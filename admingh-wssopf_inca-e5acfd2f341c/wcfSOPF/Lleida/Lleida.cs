﻿using Newtonsoft.Json;
using SOPF.Core;
using SOPF.Core.BusinessLogic.Lleida;
using SOPF.Core.BusinessLogic.Sistema;
using SOPF.Core.BusinessLogic.Solicitudes;
using SOPF.Core.Entities.Sistema;
using SOPF.Core.Model.Request.Lleida;
using SOPF.Core.Model.Response.Lleida;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using Lleida = SOPF.Core.BusinessLogic.Lleida;

namespace wcfSOPF
{
    public partial class SOPF : ILleida
    {
        #region Configuraciones
        public ConfiguracionesResponse Plantillas()
        {
            return ConfiguracionBLL.Configuraciones();
        }

        public GeneraPlantillaResponse AsignarPlantilla(GeneraPlantillaRequest peticion)
        {
            return ConfiguracionBLL.AsignarPlantilla(peticion);
        }

        public DesactivaPlantillaResponse DesactivarPlantilla(DesactivaPlantillaRequest peticion)
        {
            return ConfiguracionBLL.DesactivarPlantilla(peticion);
        }

        public ActivaPlantillaResponse ActivarPlantilla(ActivaPlantillaRequest peticion)
        {
            return ConfiguracionBLL.ActivarPlantilla(peticion);
        }
        #endregion

        #region Firmas
        public ObtenerFirmasResponse ObtenerFirmas(ObtenerFirmasRequest peticion)
        {
            return FirmaBLL.ObtenerFirmas(peticion);
        }

        public ActualizarFirmaResponse ActualizarFirma(string estatusfirma)
        {
            ActualizarFirmaRequest peticion = new ActualizarFirmaRequest();
            if (!string.IsNullOrEmpty(estatusfirma))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ActualizarFirmaRequest));
                    StringReader reader = new StringReader(estatusfirma);
                    peticion = (ActualizarFirmaRequest)serializer.Deserialize(reader);
                }
                catch (Exception ex)
                {
                    Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                        $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(estatusfirma)}", JsonConvert.SerializeObject(ex));
                }
            }

            return FirmaBLL.ActualizarFirma(peticion);
        }

        public CancelarFirmaResponse CancelarFirma(CancelarFirmaRequest peticion)
        {
            if (peticion == null) peticion = new CancelarFirmaRequest();
            if (peticion.Accion <= 0)
            {
                return FirmaBLL.CancelarFirma(peticion);
            }
            else
            {
                return FirmaBLL.CancelarFirmaLocal(peticion);
            }
        }
        #endregion

        #region Prueba Vida
        public ActualizarEstatusPruebaVidaResponse ActualizarPruebaVida(string estatusvideo)
        {
            ActualizarEstatusPruebaVidaRequest peticion = new ActualizarEstatusPruebaVidaRequest();
            if (!string.IsNullOrEmpty(estatusvideo))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ActualizarEstatusPruebaVidaRequest));
                    StringReader reader = new StringReader(estatusvideo);
                    peticion = (ActualizarEstatusPruebaVidaRequest)serializer.Deserialize(reader);
                    peticion.IdUsuario = 21;
                    var usuario = SeguridadBL.FiltrarUsuario(new UsuarioEntity { Usuario = AppSettings.WsPfLleidaUsuairo });
                    if (usuario != null && usuario.ResultObject != null && usuario.ResultObject.Count > 0)
                    {
                        var usr = usuario.ResultObject.FirstOrDefault();
                        if (usr != null) peticion.IdUsuario = usr.IdUsuario;
                    }
                }
                catch (Exception ex)
                {
                    Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                        $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(estatusvideo)}", JsonConvert.SerializeObject(ex));
                }
            }

            return SolicitudPruebaVidaBLL.ActualizarEstatus(peticion);
        }
        #endregion

        #region Notificacion
        public ObtenerFirmasResponse GenerarNotificacion(GenerarNotificacionRequest peticion)
        {
            return Lleida.NotificacionBLL.GenerarNotificacion(peticion);
        }
        #endregion
    }
}
