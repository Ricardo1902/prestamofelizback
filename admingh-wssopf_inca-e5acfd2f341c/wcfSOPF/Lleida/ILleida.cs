﻿using SOPF.Core.Model.Request.Lleida;
using SOPF.Core.Model.Response.Lleida;
using System.ServiceModel;

namespace wcfSOPF
{
    [ServiceContract]
    public interface ILleida
    {
        #region Configuraciones
        [OperationContract]
        ConfiguracionesResponse Plantillas();

        [OperationContract]
        GeneraPlantillaResponse AsignarPlantilla(GeneraPlantillaRequest peticion);

        [OperationContract]
        DesactivaPlantillaResponse DesactivarPlantilla(DesactivaPlantillaRequest peticion);

        [OperationContract]
        ActivaPlantillaResponse ActivarPlantilla(ActivaPlantillaRequest peticion);
        #endregion

        #region Firmas
        [OperationContract]
        ActualizarFirmaResponse ActualizarFirma(string estatusfirma);

        [OperationContract]
        ObtenerFirmasResponse ObtenerFirmas(ObtenerFirmasRequest peticion);

        [OperationContract]
        CancelarFirmaResponse CancelarFirma(CancelarFirmaRequest peticion);
        #endregion

        #region Prueba Vida
        [OperationContract]
        ActualizarEstatusPruebaVidaResponse ActualizarPruebaVida(string estatusvideo);
        #endregion

        #region Notificacion
        [OperationContract]
        ObtenerFirmasResponse GenerarNotificacion(GenerarNotificacionRequest peticion);
        #endregion
    }
}
