﻿using Google.Apis.Drive.v2.Data;
using SOPF.Core.BusinessLogic.Catalogo;
using SOPF.Core.BusinessLogic.Solcitudes;
using SOPF.Core.BusinessLogic.Solicitudes;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Model;
using SOPF.Core.Model.Dbo;
using SOPF.Core.Model.Request.Lleida;
using SOPF.Core.Model.Request.Reportes;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response;
using SOPF.Core.Model.Response.Lleida;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Model.Response.Sistema;
using SOPF.Core.Poco.Solicitudes;
using System.Collections.Generic;

namespace wcfSOPF
{
    partial class SOPF : ISolicitud
    {
        #region Impresion de documentos
        public ImpresionDocumentoResponse ImpresionContrato(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionContrato(peticion);
        }

        public ImpresionDocumentoResponse ImpresionPagare(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionPagare(peticion);
        }

        public ImpresionDocumentoResponse ImpresionAcuerdoPagare(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionAcuerdoPagare(peticion);
        }

        public ImpresionDocumentoResponse ImpresionAcuerdoDomiciliacionCliente(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionAcuerdoDomiciliacionContinental(peticion);
        }

        public ImpresionDocumentoResponse ImpresionAcuerdoDomiciliacionVISA(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionAcuerdoDomiciliacionVISA(peticion);
        }

        public ImpresionDocumentoResponse ImpresionAcuerdoDomiciliacionInterbank(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionAcuerdoDomiciliacionInterbank(peticion);
        }

        public EnviarCronogramaResponse EnviarCronograma(EnviarCronogramaRequest peticion)
        {
            return TBSolicitudesBll.EnviarCronograma(peticion);
        }

        public ImpresionDocumentoResponse ImpresionReestructura(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionReestructura(peticion);
        }

        public ImpresionDocumentoResponse ImpresionTransaccionExtrajudicial(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionTransaccionExtrajudicial(peticion);
        }

        public ImpresionDocumentoResponse ImpresionSolicitud(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionSolicitud(peticion);
        }

        public ImpresionDocumentoResponse ImpresionCartaPoder(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionCartaPoder(peticion);
        }

        public ImpresionDocumentoResponse ImpresionDeclaracionJurada(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionDeclaracionJurada(peticion);
        }

        public ImpresionDocumentoResponse ImpresionEstadoCuenta(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionEstadoCuenta(peticion);
        }

        public ImpresionDocumentoResponse ImpresionSolicitudRPT(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionSolicitudRPT(peticion);
        }


        #endregion

        #region Firma Digital
        public SolicitudFirmaDigitalResponse SolicitudFirmaDigital(SolicitudFirmaDigitalRequest peticion)
        {
            return SolicitudFirmaDigitalBLL.SolicitudFirmaDigital(peticion);
        }

        public List<EstatusVM> EstatusProcesoFirmaDigital()
        {
            return TB_CATEstatusBLL.EstatusProcesoFirmaDigital();
        }
        #endregion

        #region Proceso Digital
        public void AltaSolicitudProcesoDigital(NuevoSolicitudProcesoDigitalRequest peticion)
        {
            SolicitudProcesoDigitalBLL.Nuevo(peticion);
        }

        public InicarSolicitudProcesoDigitalResponse IniciarProcesoOriginacionD(InicarSolicitudProcesoDigitalRequest peticion)
        {
            return SolicitudProcesoDigitalBLL.ProcesoOriginacion(peticion);
        }

        public InicarSolicitudProcesoDigitalResponse IniciarProcesoPeticionReestructura(InicarSolicitudProcesoDigitalRequest peticion)
        {
            return SolicitudProcesoDigitalBLL.ProcesoOriginacionPeticionReestructura(peticion);
        }

        public void AltaSolicitudDocDigital(int idSolicitud, int idProceso)
        {
            SolicitudDocDigitalBLL.AltaSolicitudDocDigital(idSolicitud, idProceso);
        }

        public EstatusSolicitudProcesoDigitalResponse EstatusProcesoOriginacionD(int idSolicitud)
        {
            TB_Proceso proceso = ProcesoBLL.Originacion();
            int idProceso = 0;
            if (proceso != null) idProceso = proceso.IdProceso;
            return SolicitudProcesoDigitalBLL.Estatus(idSolicitud, idProceso);
        }

        public LogProcesoResponse LogProcesoOriginacion(LogProcesoRequest peticion)
        {
            TB_Proceso proceso = ProcesoBLL.Originacion();
            if (peticion != null && proceso != null) peticion.IdProceso = proceso.IdProceso;
            return SolicitudProcesoDigitalBLL.LogProceso(peticion);
        }

        public ReiniciarProcesoResponse ReiniciarProceso(ReiniciarProcesoRequest peticion)
        {
            if (peticion != null)
            {
                peticion.InicioAutomatico = true;
                peticion.ReinicioManual = true;
            }
            return SolicitudProcesoDigitalBLL.ReiniciarProceso(peticion);
        }
        #endregion

        #region Prueba Vida
        public SolicitudPruebaVidaResponse PruebasVida(SolicitudPruebaVidaRequest peticion)
        {
            return SolicitudPruebaVidaBLL.PruebasVida(peticion);
        }

        public ActualizarEstatusPruebaVidaResponse ActualizarPeticionEstatus(ActualizarPeticionEstatusRequest peticion)
        {
            return SolicitudPruebaVidaBLL.ActualizarPeticionEstatus(peticion);
        }

        public ValidarPruebaVidaResponse ValidarPruebaVida(ValidarPruebaVidaRequest peticion)
        {
            return SolicitudPruebaVidaBLL.ValidarPruebaVida(peticion);
        }

        public ReiniciarPruebaVidaResponse ReiniciarPruebaVida(ReiniciarPruebaVidaRequest peticion)
        {
            return SolicitudPruebaVidaBLL.ReiniciarPruebaVida(peticion);
        }
        #endregion

        #region Solicitudes
        #region GET
        public ObtenerSolicitudResponse ObtenerSolicitud(int IDUsuario, int IDSolicitud)
        {
            return TBSolicitudesBll.ObtenerSolicitud(IDUsuario, IDSolicitud);
        }

        public DescargarArchivoGDResponse DescargaDocumentoGDrive(DescargarArchivoGDRequest peticion)
        {
            return TBSolicitudesBll.DescargaArchivoGDrive(peticion);
        }

        public ExpedienteResponse Expediente(ExpedienteRequest peticion)
        {
            return ExpedienteBLL.Expediente(peticion);
        }

        public List<TBSolicitudes.Documentos> DocumentosCondicionados(DocumentosCondicionadosRequest peticion)
        {
            return TBSolicitudesBll.DocumentosCondicionados(peticion);
        }
        #endregion

        #region POST
        public CargarExpedienteResponse CargarExpediente(CargarExpedienteRequest peticion)
        {
            return ExpedienteBLL.CargarExpediente(peticion);
        }

        public CargarDocumentoArchivoResponse CargarDocumentoArchivo(CargarDocumentoArchivoRequest peticion)
        {
            return ExpedienteBLL.CargarDocumentoArchivo(peticion);
        }

        public Respuesta EliminarDocumentoArchivo(EliminarDocumentoArchivoRequest peticion)
        {
            return ExpedienteBLL.EliminarDocumentoArchivo(peticion);
        }

        public DocumentosArchivosResponse DocumentosArchivos(DocumentosArchivosRequest peticion)
        {
            return ExpedienteBLL.DocumentosArchivos(peticion);
        }

        public Respuesta GeneraDocumentoArchivoExpediente(GeneraDocumentoArchivoExpedienteRequest peticion)
        {
            return ExpedienteBLL.GeneraDocumentoArchivoExpediente(peticion);
        }
        #endregion

        #endregion

        #region Formatos
        #region GET
        public ObtenerPaginasFormatosResponse ObtenerPaginasFormatos()
        {
            return TBSolicitudesBll.ObtenerPaginasFormatos();
        }
        #endregion
        #endregion

        #region Buzones
        public CargarBuzonCancelacionResponse CargarBuzonCancelacion(CargarBuzonCancelacionRequest peticion)
        {
            return TBSolicitudesBll.CargarBuzonCancelacion(peticion);
        }

        public ObtenerEstatusBuzonCancelacionResponse ObtenerEstatusBuzonCancelacion(ObtenerEstatusBuzonCancelacionRequest peticion)
        {
            return TBSolicitudesBll.ObtenerEstatusBuzonCancelacion(peticion);
        }

        public AtenderBuzonCancelacionResponse AtenderBuzonCancelacion(AtenderBuzonCancelacionRequest peticion)
        {
            return TBSolicitudesBll.AtenderBuzonCancelacion(peticion);
        }

        public EnviarCodigoCancelacionResponse EnviarCodigoCancelacion(EnviarCodigoCancelacionRequest peticion)
        {
            return TBSolicitudesBll.EnviarCodigoCancelacion(peticion);
        }

        public ConfirmarRecaudoCancelacionResponse ConfirmarRecaudoCancelacion(ConfirmarRecaudoCancelacionRequest peticion)
        {
            return TBSolicitudesBll.ConfirmarRecaudoCancelacion(peticion);
        }

        public RechazarBuzonCancelacionResponse RechazarBuzonCancelacion(RechazarBuzonCancelacionRequest peticion)
        {
            return TBSolicitudesBll.RechazarBuzonCancelacion(peticion);
        }
        #endregion

        public ImpresionDocumentoResponse DEBUG_ImprimeHojaResumen(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionResumenCredito(peticion);
        }
    }
}
