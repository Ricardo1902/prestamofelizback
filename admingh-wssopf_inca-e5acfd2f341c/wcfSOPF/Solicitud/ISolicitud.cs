﻿using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Model;
using SOPF.Core.Model.Dbo;
using SOPF.Core.Model.Request.Lleida;
using SOPF.Core.Model.Request.Reportes;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response;
using SOPF.Core.Model.Response.Lleida;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Model.Response.Sistema;
using System.Collections.Generic;
using System.ServiceModel;

namespace wcfSOPF
{
    public partial interface ISolicitud
    {
        #region Impresion de documentos
        [OperationContract]
        ImpresionDocumentoResponse ImpresionContrato(ImpresionDocumentoRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionPagare(ImpresionDocumentoRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionAcuerdoPagare(ImpresionDocumentoRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionAcuerdoDomiciliacionCliente(ImpresionDocumentoRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionAcuerdoDomiciliacionVISA(ImpresionDocumentoRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionAcuerdoDomiciliacionInterbank(ImpresionDocumentoRequest peticion);

        [OperationContract]
        EnviarCronogramaResponse EnviarCronograma(EnviarCronogramaRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionReestructura(ImpresionDocumentoRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionTransaccionExtrajudicial(ImpresionDocumentoRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionSolicitud(ImpresionDocumentoRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionCartaPoder(ImpresionDocumentoRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionDeclaracionJurada(ImpresionDocumentoRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionEstadoCuenta(ImpresionDocumentoRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionSolicitudRPT(ImpresionDocumentoRequest peticion);

        #endregion

        #region Proceso Digital
        [OperationContract]
        void AltaSolicitudProcesoDigital(NuevoSolicitudProcesoDigitalRequest peticion);

        [OperationContract]
        InicarSolicitudProcesoDigitalResponse IniciarProcesoOriginacionD(InicarSolicitudProcesoDigitalRequest peticion);

        [OperationContract]
        InicarSolicitudProcesoDigitalResponse IniciarProcesoPeticionReestructura(InicarSolicitudProcesoDigitalRequest peticion);

        [OperationContract]
        void AltaSolicitudDocDigital(int idSolicitud, int idProceso);

        [OperationContract]
        EstatusSolicitudProcesoDigitalResponse EstatusProcesoOriginacionD(int idSolicitud);

        [OperationContract]
        LogProcesoResponse LogProcesoOriginacion(LogProcesoRequest peticion);

        [OperationContract]
        ReiniciarProcesoResponse ReiniciarProceso(ReiniciarProcesoRequest peticion);
        #endregion

        #region Firma Digital
        [OperationContract]
        SolicitudFirmaDigitalResponse SolicitudFirmaDigital(SolicitudFirmaDigitalRequest peticion);

        [OperationContract]
        List<EstatusVM> EstatusProcesoFirmaDigital();

        [OperationContract]
        SolicitudPruebaVidaResponse PruebasVida(SolicitudPruebaVidaRequest peticion);

        [OperationContract]
        ActualizarEstatusPruebaVidaResponse ActualizarPeticionEstatus(ActualizarPeticionEstatusRequest peticion);

        [OperationContract]
        ValidarPruebaVidaResponse ValidarPruebaVida(ValidarPruebaVidaRequest peticion);

        [OperationContract]
        ReiniciarPruebaVidaResponse ReiniciarPruebaVida(ReiniciarPruebaVidaRequest peticion);
        #endregion

        #region Solicitudes
        #region GET
        [OperationContract]
        ObtenerSolicitudResponse ObtenerSolicitud(int IDUsuario, int IDSolicitud);

        [OperationContract]
        DescargarArchivoGDResponse DescargaDocumentoGDrive(DescargarArchivoGDRequest peticion);

        [OperationContract]
        ExpedienteResponse Expediente(ExpedienteRequest peticion);

        [OperationContract]
        List<TBSolicitudes.Documentos> DocumentosCondicionados(DocumentosCondicionadosRequest peticion);
        #endregion

        #region POST
        [OperationContract]
        CargarExpedienteResponse CargarExpediente(CargarExpedienteRequest peticion);

        [OperationContract]
        CargarDocumentoArchivoResponse CargarDocumentoArchivo(CargarDocumentoArchivoRequest peticion);

        [OperationContract]
        Respuesta EliminarDocumentoArchivo(EliminarDocumentoArchivoRequest peticion);

        [OperationContract]
        DocumentosArchivosResponse DocumentosArchivos(DocumentosArchivosRequest peticion);

        [OperationContract]
        Respuesta GeneraDocumentoArchivoExpediente(GeneraDocumentoArchivoExpedienteRequest peticion);
        #endregion

        #endregion

        #region Formatos
        #region GET
        [OperationContract]
        ObtenerPaginasFormatosResponse ObtenerPaginasFormatos();
        #endregion
        #endregion

        #region Buzones
        [OperationContract]
        CargarBuzonCancelacionResponse CargarBuzonCancelacion(CargarBuzonCancelacionRequest peticion);

        [OperationContract]
        ObtenerEstatusBuzonCancelacionResponse ObtenerEstatusBuzonCancelacion(ObtenerEstatusBuzonCancelacionRequest peticion);

        [OperationContract]
        AtenderBuzonCancelacionResponse AtenderBuzonCancelacion(AtenderBuzonCancelacionRequest peticion);

        [OperationContract]
        EnviarCodigoCancelacionResponse EnviarCodigoCancelacion(EnviarCodigoCancelacionRequest peticion);

        [OperationContract]
        ConfirmarRecaudoCancelacionResponse ConfirmarRecaudoCancelacion(ConfirmarRecaudoCancelacionRequest peticion);

        [OperationContract]
        RechazarBuzonCancelacionResponse RechazarBuzonCancelacion(RechazarBuzonCancelacionRequest peticion);
        #endregion

        [OperationContract]
        ImpresionDocumentoResponse DEBUG_ImprimeHojaResumen(ImpresionDocumentoRequest peticion);
    }
}
