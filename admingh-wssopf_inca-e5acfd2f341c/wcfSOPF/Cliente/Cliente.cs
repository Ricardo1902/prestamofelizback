﻿using SOPF.Core.BusinessLogic.Clientes;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Clientes;
using SOPF.Core.Model.Request.Cliente;
using SOPF.Core.Model.Response.Cliente;
using SOPF.Core.Poco.Clientes;
using System;
using System.Collections.Generic;

namespace wcfSOPF
{
    partial class SOPF : IClientes
    {
        #region GET
        public List<TBClientes> ObtenerTBClientes(int Accion, int IdCliente)
        {
            return TBClientesBll.ObtenerTBClientes(Accion, IdCliente);
        }

        public List<TBClientes.BusquedaClienteRecomienda> ObtenerTBClienteRecomendador(int Accion, int IdCliente)
        {
            return TBClientesBll.ObtenerTBClienteRecomendador(Accion, IdCliente);
        }

        public List<TBClientes.BusquedaCliente> BusquedaCliente(int Accion, int IdSolicitud, int idCliente, string Cliente, DateTime FechaInicial, DateTime FechaFinal, int idSucursal)
        {
            return TBClientesBll.BusquedaCliente(Accion, IdSolicitud, idCliente, Cliente, FechaInicial, FechaFinal, idSucursal);
        }

        public List<TBClabes.ClabeCliente> ObtenerClabesCliente(int Accion, int idCliente, int idsolicitud, string clabe)
        {
            return TBClabesBll.ObtenerClabesCliente(Accion, idCliente, idsolicitud, clabe);
        }

        public Resultado<int> ObtenerSolicitudActivaDeCliente(int idCliente)
        {
            return TBClientesBll.ObtenerSolicitudActivaDeCliente(idCliente);
        }

        public List<ClientesReferencia> ObtenerReferenciasCliente(int idCliente)
        {
            return ReferenciasBll.ObtenerReferenciasCliente(idCliente);
        }

        public List<DireccionCliente> ObtenerDireccionesCliente(int idCliente)
        {
            return TBClientesBll.ObtenerDireccionesCliente(idCliente);
        }

        public BuscarClienteResponse BuscarCliente(BuscarClienteRequest peticion)
        {
            return TBClientesBll.BuscarCliente(peticion);
        }

        public string ObtenerCategoriaCliente(int idCliente)
        {
            return TBClientesBll.ObtenerCategoriaCliente(idCliente);
        }
        #endregion

        #region POST
        public Resultado<bool> InsertarTBClientes(TBClientes entidad)
        {
            return TBClientesBll.InsertarTBClientes(entidad);
        }

        public void ActualizarTBClientes()
        {
            TBClientesBll.ActualizarTBClientes();
        }

        public TBClabes.DataResultClabe InsertarTBClabes(int Accion, TBClabes entidad)
        {
            return TBClabesBll.InsertarTBClabes(Accion, entidad);
        }

        public Resultado<bool> InsertarClienteRecomendador(int Accion, int IdCliente, int IdClienteRec, int IdClabeRec, int IdCampania, int IdUsuario)
        {
            return TBClientesBll.InsertarClienteRecomendador(Accion, IdCliente, IdClienteRec, IdClabeRec, IdCampania, IdUsuario);
        }

        public Resultado<bool> ActualizarClienteGestiones(TBClientes.ActualizaClienteGestiones cliente)
        {
            return TBClientesBll.ActualizarClienteGestiones(cliente);
        }

        public Resultado<int> InsertarClientesReferencia(ClientesReferencia referencia, int idUsuario)
        {
            return ReferenciasBll.InsertarReferencia(referencia, true, idUsuario);
        }

        public Resultado<ClientesReferencia> ActualizarClientesReferencia(ClientesReferencia referencia, int idUsuario)
        {
            return ReferenciasBll.ActualizarReferencia(referencia, true, idUsuario);
        }

        public Resultado<int> ActualizarClientesReferenciaMasivo(int idUsuario, List<ClientesReferencia> referencias)
        {
            return ReferenciasBll.ActualizarReferenciaMasivo(idUsuario, referencias);
        }

        public Resultado<bool> EliminarClientesReferencia(int idReferencia, int idCliente, int idUsuario)
        {
            return ReferenciasBll.EliminarReferencia(idReferencia, idCliente, true, idUsuario);
        }

        public Resultado<bool> AgregarGestionActCliente(int IdSolicitud, int IdUsuario)
        {
            return TBClientesBll.AgregarGestionActCliente(IdSolicitud, IdUsuario);
        }

        #endregion
    }
}
