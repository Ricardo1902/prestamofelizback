﻿using SOPF.Core.Entities;
using SOPF.Core.Entities.Clientes;
using SOPF.Core.Model.Request.Cliente;
using SOPF.Core.Model.Response.Cliente;
using SOPF.Core.Poco.Clientes;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace wcfSOPF
{
    [ServiceContract]
    partial interface IClientes
    {
        #region GET
        [OperationContract]
        List<TBClientes> ObtenerTBClientes(int Accion, int IdCliente);

        [OperationContract]
        List<TBClientes.BusquedaClienteRecomienda> ObtenerTBClienteRecomendador(int Accion, int IdCliente);

        [OperationContract]
        List<TBClientes.BusquedaCliente> BusquedaCliente(int Accion, int IdSolicitud, int idCliente, string Cliente, DateTime FechaInicial, DateTime FechaFinal, int idSucursal);

        [OperationContract]
        List<TBClabes.ClabeCliente> ObtenerClabesCliente(int Accion, int idCliente, int idsolicitud, string clabe);

        [OperationContract]
        Resultado<int> ObtenerSolicitudActivaDeCliente(int idCliente);

        [OperationContract]
        List<ClientesReferencia> ObtenerReferenciasCliente(int idCliente);

        [OperationContract]
        List<DireccionCliente> ObtenerDireccionesCliente(int idCliente);

        [OperationContract]
        BuscarClienteResponse BuscarCliente(BuscarClienteRequest peticion);

        [OperationContract]
        string ObtenerCategoriaCliente(int idCliente);

        #endregion

        #region POST
        [OperationContract]
        Resultado<bool> InsertarTBClientes(TBClientes entidad);

        [OperationContract]
        void ActualizarTBClientes();

        [OperationContract]
        TBClabes.DataResultClabe InsertarTBClabes(int Accion, TBClabes entidad);

        [OperationContract]
        Resultado<bool> InsertarClienteRecomendador(int Accion, int IdCliente, int IdClienteRec, int IdClabeRec, int IdCampania, int IdUsuario);

        [OperationContract]
        Resultado<bool> ActualizarClienteGestiones(TBClientes.ActualizaClienteGestiones cliente);

        [OperationContract]
        Resultado<int> InsertarClientesReferencia(ClientesReferencia referencia, int idUsuario);

        [OperationContract]
        Resultado<ClientesReferencia> ActualizarClientesReferencia(ClientesReferencia referencia, int idUsuario);

        [OperationContract]
        Resultado<int> ActualizarClientesReferenciaMasivo(int idUsuario, List<ClientesReferencia> referencias);

        [OperationContract]
        Resultado<bool> EliminarClientesReferencia(int idReferencia, int idCliente, int idUsuario);

        [OperationContract]
        Resultado<bool> AgregarGestionActCliente(int IdSolicitud, int IdUsuario);

        #endregion
    }
}
