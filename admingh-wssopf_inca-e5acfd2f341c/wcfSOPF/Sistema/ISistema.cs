﻿using SOPF.Core.Entities;
using SOPF.Core.Entities.Sistema;
using SOPF.Core.Model;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response.Reportes;
using SOPF.Core.Model.Response.Sistema;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace wcfSOPF
{
    [ServiceContract]
    public interface ISistema
    {
        [OperationContract]
        Resultado<CuadreDatosSolicitudEntity> Sistema_ObtenerDatosSolicitud(CuadreDatosSolicitudEntity entity);

        [OperationContract]
        Resultado<bool> Sistema_ActualizarDatosSolicitud(CuadreDatosSolicitudEntity entity);

        [OperationContract]
        Resultado<bool> Sistema_HRCobranza(CuadreDatosSolicitudEntity entity);

        [OperationContract]
        Resultado<bool> Sistema_ReprocesarTablaAmortizacion(int IdSolicitud, string TipoTabla);

        [OperationContract]
        Resultado<bool> Sistema_CambioFechaTablaAmortizacion(int IdSolicitud, DateTime FechaInicio);

        [OperationContract]
        Resultado<DatosAmpliacionEntity> Sistema_ObtenerDatosAmpliacion(int IdSolicitud, int IdSolicitudAmplia, DateTime FechaAmpliacion);

        [OperationContract]
        Resultado<UsuarioEntity> Sistema_GuardarUsuario(UsuarioEntity entity);

        [OperationContract]
        Resultado<List<UsuarioEntity>> Sistema_FiltrarUsuario(UsuarioEntity entity);

        [OperationContract]
        Resultado<UsuarioEntity> Sistema_ObtenerUsuario(UsuarioEntity entity);

        [OperationContract]
        List<TipoUsuarioEntity> Sistema_TipoUsuario_Obtener_Activos();

        [OperationContract]
        List<SucursalEntity> Sistema_Sucursales_Obtener_Activos();

        [OperationContract]
        List<DepartamentoEntity> Sistema_Departamentos_Obtener_Activos();

        [OperationContract]
        List<MenuEntity> Sistema_FiltrarMenu(MenuEntity entity);

        [OperationContract]
        List<MenuEntity> Sistema_Menu_Obtener_Activos();

        [OperationContract]
        List<ReporteEntity> Sistema_FiltrarReporte(ReporteEntity entity);

        [OperationContract]
        List<ReporteEntity> Sistema_Reporte_Obtener_Activos();

        [OperationContract]
        List<AccionEntity> Sistema_Accion_Obtener_Activos();

        [OperationContract]
        void Sistema_InsertarAccesoMenuUsuario(int IdUsuario, int IdMenu);

        [OperationContract]
        void Sistema_InsertarAccesoReporteUsuario(int IdUsuario, int IdMenu);

        [OperationContract]
        void Sistema_InsertarAccesoAccionUsuario(int IdUsuario, int IdMenuAccion);

        [OperationContract]
        void Sistema_QuitarAccesoMenuUsuario(int IdUsuario, int IdMenu);

        [OperationContract]
        void Sistema_QuitarAccesoReporteUsuario(int IdUsuario, int IdMenu);

        [OperationContract]
        void Sistema_QuitarAccesoAccionUsuario(int IdUsuario, int IdMenuAccion);

        [OperationContract]
        NotificacionesPendientesResponse Sistema_NotificacionesPendientes(NotificacionesPendientesRequest peticion);

        [OperationContract]
        ActualizarNotificacionResponse Sistema_NotificacionesActualizar(ActualizarNotificacionRequest peticion);

        [OperationContract]
        CuentaCorreoResponse Sistema_CuentaCorreo(CuentaCorreoRequest peticion);

        [OperationContract]
        DescargarArchivoGDResponse DescargarArchivoGD(DescargarArchivoGDRequest peticion);

        [OperationContract]
        NotificacionesSolNotPendientesResponse Sistema_NotificacionesSolNotPendientes(NotificacionesSolNotPendientesRequest peticion);

        [OperationContract]
        ConexionSipGestionResponse ConexionSipGestion(ConexionSipGestionRequest peticion);

        [OperationContract]
        AutenticarResponse Autenticar(string usuario, string password);

        [OperationContract]
        Respuesta FinalizarSesion(int idUsuario);

        [OperationContract]
        bool EsTipoUsuario(string tipoUsuario, int idUsuario);

        [OperationContract]
        void NotificacionProgramadoReset();

        [OperationContract]
        ValidaRecursoAccesoResponse ValidaRecursoAcceso(ValidaRecursoAccesoRequest peticion);

        [OperationContract]
        TipoNotificacionResponse TipoNotificacion(TipoNotificacionRequest peticion);

        [OperationContract]
        EnviarOtpResponse EnviarOtp(EnviarOtpRequest peticion);

        [OperationContract]
        VerificarOtpResponse VerificarOtp(VerificarOtpRequest peticion);
    }
}
