﻿using SOPF.Core;
using SOPF.Core.BusinessLogic.Gestiones;
using SOPF.Core.BusinessLogic.Sistema;
using SOPF.Core.BusinessLogic.Usuario;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Sistema;
using SOPF.Core.Model;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response.Reportes;
using SOPF.Core.Model.Response.Sistema;
using System;
using System.Collections.Generic;

namespace wcfSOPF
{
    public partial class SOPF : ISistema
    {
        public Resultado<CuadreDatosSolicitudEntity> Sistema_ObtenerDatosSolicitud(CuadreDatosSolicitudEntity entity)
        {
            return CuadreBL.ObtenerDatosSolicitud(entity);
        }

        public Resultado<bool> Sistema_ActualizarDatosSolicitud(CuadreDatosSolicitudEntity entity)
        {
            return CuadreBL.ActualizarDatosSolicitud(entity);
        }

        public Resultado<bool> Sistema_HRCobranza(CuadreDatosSolicitudEntity entity)
        {
            return CuadreBL.HRCobranza(entity);
        }

        public Resultado<bool> Sistema_ReprocesarTablaAmortizacion(int IdSolicitud, string TipoTabla)
        {
            return CuadreBL.ReprocesarTablaAmortizacion(IdSolicitud, TipoTabla);
        }

        public Resultado<bool> Sistema_CambioFechaTablaAmortizacion(int IdSolicitud, DateTime FechaInicio)
        {
            return CuadreBL.CambioFechaTablaAmortizacion(IdSolicitud, FechaInicio);
        }

        public Resultado<DatosAmpliacionEntity> Sistema_ObtenerDatosAmpliacion(int IdSolicitud, int IdSolicitudAmplia, DateTime FechaAmpliacion)
        {
            return CuadreBL.ObtenerDatosAmpliacion(IdSolicitud, IdSolicitudAmplia, FechaAmpliacion);
        }

        public Resultado<UsuarioEntity> Sistema_GuardarUsuario(UsuarioEntity entity)
        {
            return SeguridadBL.GuardarUsuario(entity);
        }

        public Resultado<List<UsuarioEntity>> Sistema_FiltrarUsuario(UsuarioEntity entity)
        {
            return SeguridadBL.FiltrarUsuario(entity);
        }

        public Resultado<UsuarioEntity> Sistema_ObtenerUsuario(UsuarioEntity entity)
        {
            return SeguridadBL.ObtenerUsuario(entity);
        }

        public List<TipoUsuarioEntity> Sistema_TipoUsuario_Obtener_Activos()
        {
            return SeguridadBL.TipoUsuario_Obtener_Activos();
        }

        public List<SucursalEntity> Sistema_Sucursales_Obtener_Activos()
        {
            return SeguridadBL.Sucursales_Obtener_Activos();
        }

        public List<DepartamentoEntity> Sistema_Departamentos_Obtener_Activos()
        {
            return SeguridadBL.Departamentos_Obtener_Activos();
        }

        public List<MenuEntity> Sistema_FiltrarMenu(MenuEntity entity)
        {
            return SeguridadBL.FiltrarMenu(entity);
        }

        public List<MenuEntity> Sistema_Menu_Obtener_Activos()
        {
            return SeguridadBL.FiltrarMenu(new MenuEntity() { Activo = true });
        }

        public List<ReporteEntity> Sistema_FiltrarReporte(ReporteEntity entity)
        {
            return SeguridadBL.FiltrarReporte(entity);
        }

        public List<AccionEntity> Sistema_Accion_Obtener_Activos()
        {
            return SeguridadBL.FiltrarAccion(new AccionEntity() { Activo = true });
        }

        public List<ReporteEntity> Sistema_Reporte_Obtener_Activos()
        {
            return SeguridadBL.FiltrarReporte(new ReporteEntity() { Activo = true });
        }

        public void Sistema_InsertarAccesoMenuUsuario(int IdUsuario, int IdMenu)
        {
            SeguridadBL.InsertarAccesoMenuUsuario(IdUsuario, IdMenu);
        }

        public void Sistema_InsertarAccesoReporteUsuario(int IdUsuario, int IdMenu)
        {
            SeguridadBL.InsertarAccesoReporteUsuario(IdUsuario, IdMenu);
        }

        public void Sistema_InsertarAccesoAccionUsuario(int IdUsuario, int IdMenuAccion)
        {
            SeguridadBL.InsertarAccesoAccionUsuario(IdUsuario, IdMenuAccion);
        }

        public void Sistema_QuitarAccesoMenuUsuario(int IdUsuario, int IdMenu)
        {
            SeguridadBL.QuitarAccesoMenuUsuario(IdUsuario, IdMenu);
        }

        public void Sistema_QuitarAccesoReporteUsuario(int IdUsuario, int IdMenu)
        {
            SeguridadBL.QuitarAccesoReporteUsuario(IdUsuario, IdMenu);
        }

        public void Sistema_QuitarAccesoAccionUsuario(int IdUsuario, int IdMenuAccion)
        {
            SeguridadBL.QuitarAccesoAccionUsuario(IdUsuario, IdMenuAccion);
        }

        public NotificacionesPendientesResponse Sistema_NotificacionesPendientes(NotificacionesPendientesRequest peticion)
        {
            return NotificacionBLL.Pendientes(peticion);
        }

        public ActualizarNotificacionResponse Sistema_NotificacionesActualizar(ActualizarNotificacionRequest peticion)
        {
            return NotificacionBLL.Actualizar(peticion);
        }

        public CuentaCorreoResponse Sistema_CuentaCorreo(CuentaCorreoRequest peticion)
        {
            return CuentaCorreoBLL.CuentaCorreo(peticion);
        }

        public DescargarArchivoGDResponse DescargarArchivoGD(DescargarArchivoGDRequest peticion)
        {
            string error = string.Empty;
            DescargarArchivoGDResponse respuesta = new DescargarArchivoGDResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válida.");

                using (ServidorGoogleDrive wsGo = new ServidorGoogleDrive())
                {
                    ArchivoGoogleDrive descarga = wsGo.DescargarArchivo(peticion.IdArchivoGD, ref error);
                    if (!string.IsNullOrEmpty(error)) throw new ValidacionExcepcion(error);
                    respuesta.ArchivoGoogle = descarga;
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = ex.Message;
            }
            return respuesta;
        }

        public NotificacionesSolNotPendientesResponse Sistema_NotificacionesSolNotPendientes(NotificacionesSolNotPendientesRequest peticion)
        {
            return SolicitudNotificacionBLL.ObtenerNotificacionesSolNotPendientes(peticion);
        }

        public ConexionSipGestionResponse ConexionSipGestion(ConexionSipGestionRequest peticion)
        {
            return ConexionSipBLL.ConexionSipGestion(peticion);
        }

        public AutenticarResponse Autenticar(string usuario, string password)
        {
            return SeguridadBL.Autenticar(usuario, password);
        }

        public Respuesta FinalizarSesion(int idUsuario)
        {
            return SeguridadBL.FinalizarSesion(idUsuario);
        }

        public bool EsTipoUsuario(string tipoUsuario, int idUsuario)
        {
            return TbCatUsuarioBll.EsTipoUsuario(tipoUsuario, idUsuario);
        }

        public void NotificacionProgramadoReset()
        {
            NotificacionBLL.NotificacionProgramadoReset();
        }

        public ValidaRecursoAccesoResponse ValidaRecursoAcceso(ValidaRecursoAccesoRequest peticion)
        {
            return SeguridadBL.ValidaRecursoAcceso(peticion);
        }

        public TipoNotificacionResponse TipoNotificacion(TipoNotificacionRequest peticion)
        {
            return NotificacionBLL.TipoNotificacion(peticion);
        }

        public EnviarOtpResponse EnviarOtp(EnviarOtpRequest peticion)
        {
            return OTPBLL.Enviar(peticion);
        }

        public VerificarOtpResponse VerificarOtp(VerificarOtpRequest peticion)
        {
            return OTPBLL.Verificar(peticion);
        }
    }
}
