using SOPF.Core.BusinessLogic.Cobranza;
using SOPF.Core.BusinessLogic.Gestiones;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Model;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Request.Reportes;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Response;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Poco.Gestiones;
using System.Collections.Generic;

namespace wcfSOPF
{
    public partial class SOPF : IGestion
    {
        #region Gestiones
        public List<TipoGestion> ObtenerCatalogoTipoGestion(int accion)
        {
            return TipoGestionBLL.ObtenerTipoGestion(accion);
        }

        public ObtenerDatosClienteGestionResponse ObtenerDatosClienteGestion(ObtenerDatosClienteGestionRequest peticion)
        {
            return GestionBll.ObtenerDatosClienteGestion(peticion);
        }

        public ImpresionNotificacionResponse ImpresionAvisoCobranza(ImpresionAvisoCobranzaRequest peticion)
        {
            return GestionBll.ImpresionAvisoCobranza(peticion);
        }

        public ImpresionNotificacionResponse ImpresionAvisoPago(ImpresionAvisoPagoRequest peticion)
        {
            return GestionBll.ImpresionAvisoPago(peticion);
        }

        public ImpresionNotificacionResponse ProcesarAvisoCobranza(ImpresionAvisoCobranzaRequest peticion)
        {
            return GestionBll.ProcesarAvisoCobranza(peticion);
        }

        public ImpresionNotificacionResponse ProcesarAvisoPago(ImpresionAvisoPagoRequest peticion)
        {
            return GestionBll.ProcesarAvisoPago(peticion);
        }

        public ImpresionNotificacionResponse ImpresionNotificacionPrejudicial(ImpresionNotificacionPrejudicialRequest peticion)
        {
            return GestionBll.ImpresionNotificacionPrejudicial(peticion);
        }

        public ImpresionNotificacionResponse ProcesarNotificacionPrejudicial(ImpresionNotificacionPrejudicialRequest peticion)
        {
            return GestionBll.ProcesarNotificacionPrejudicial(peticion);
        }

        public ImpresionNotificacionResponse ImpresionUltimaNotificacionPrejudicial(ImpresionUltimaNotificacionPrejudicialRequest peticion)
        {
            return GestionBll.ImpresionUltimaNotificacionPrejudicial(peticion);
        }

        public ImpresionNotificacionResponse ProcesarUltimaNotificacionPrejudicial(ImpresionUltimaNotificacionPrejudicialRequest peticion)
        {
            return GestionBll.ProcesarUltimaNotificacionPrejudicial(peticion);
        }

        public ImpresionNotificacionResponse ImpresionCitacionPrejudicial(ImpresionCitacionPrejudicialRequest peticion)
        {
            return GestionBll.ImpresionCitacionPrejudicial(peticion);
        }

        public ImpresionNotificacionResponse ProcesarCitacionPrejudicial(ImpresionCitacionPrejudicialRequest peticion)
        {
            return GestionBll.ProcesarCitacionPrejudicial(peticion);
        }

        public ImpresionNotificacionResponse ImpresionPromocionReduccionMora(ImpresionPromocionReduccionMoraRequest peticion)
        {
            return GestionBll.ImpresionPromocionReduccionMora(peticion);
        }

        public ImpresionNotificacionResponse ProcesarPromocionReduccionMora(ImpresionPromocionReduccionMoraRequest peticion)
        {
            return GestionBll.ProcesarPromocionReduccionMora(peticion);
        }

        public ObtenerCarteraAsignadaResponse ObtenerCarteraAsignada(ObtenerCarteraAsignadaRequest peticion)
        {
            return GestionBll.ObtenerCarteraAsignada(peticion);
        }

        public ObtenerGestionesResponse ObtenerGestionesPorIdCuenta(ObtenerGestionRequest peticion)
        {
            return GestionBll.ObtenerGestionesPorIdCuenta(peticion);
        }

        public ObtenerDatosClienteAcuerdoResponse ObtenerDatosClienteAcuerdo(ObtenerDatosClienteAcuerdoRequest peticion)
        {
            return AcuerdoPagoBll.ObtenerDatosClienteAcuerdo(peticion);
        }

        public ObtenerAcuerdoPagoConDetalleResponse ObtenerAcuerdoPagoConDetalle(ObtenerAcuerdoPagoRequest peticion)
        {
            return AcuerdoPagoBll.ObtenerAcuerdoPagoConDetalle(peticion);
        }

        public ObtenerAcuerdoPagoConDetalleResponse ObtenerAcuerdoPagoConDetallePorSolicitud(ObtenerAcuerdoPagoRequest peticion)
        {
            return AcuerdoPagoBll.ObtenerAcuerdoPagoConDetallePorSolicitud(peticion);
        }

        public ObtenerAcuerdoPagoResponse ObtenerAcuerdoPago(ObtenerAcuerdoPagoRequest peticion)
        {
            return AcuerdoPagoBll.ObtenerAcuerdoPago(peticion);
        }

        public ObtenerAcuerdosPagoResponse ObtenerAcuerdosPago(ObtenerAcuerdoPagoRequest peticion)
        {
            return AcuerdoPagoBll.ObtenerAcuerdosPago(peticion);
        }

        public ObtenerAcuerdoPagoResponse ObtenerAcuerdoPagoSolicitud(ObtenerAcuerdoPagoRequest peticion)
        {
            return AcuerdoPagoBll.ObtenerAcuerdoPagoSolicitud(peticion);
        }

        public ObtenerAcuerdoPagoDetalleResponse ObtenerAcuerdoPagoDetalle(ObtenerAcuerdoPagoDetalleRequest peticion)
        {
            return AcuerdoPagoBll.ObtenerAcuerdoPagoDetalle(peticion);
        }

        public AltaAcuerdoPagoResponse AltaAcuerdoPago(AltaAcuerdoPagoRequest peticion)
        {
            return AcuerdoPagoBll.AltaAcuerdoPago(peticion);
        }

        public CancelarAcuerdoPagoResponse CancelarAcuerdoPago(CancelarAcuerdoPagoRequest peticion)
        {
            return AcuerdoPagoBll.CancelarAcuerdoPago(peticion);
        }

        public ObtenerPagosGestionesResponse ObtenerPagosGestiones(ObtenerPagosGestionesRequest peticion)
        {
            return GestionBll.ObtenerPagosGestiones(peticion);
        }

        public GestoresResponse Gestores(GestoresRequest peticion)
        {
            return TBCatGestoresBll.Gestores(peticion);
        }

        public ObtenerTipoContactosResponse TipoContactosGestTelefonico(Peticion peticion)
        {
            return CatTipoContactoBLL.TipoContactosGestTelefonico(peticion);
        }

        public ObtenerTipoContactosResponse ObtenerTipoContactos(TipoContactosRequest peticion)
        {
            return CatTipoContactoBLL.ObtenerTipoContactos(peticion);
        }

        public TelefonosTipoContactoResponse TelefonosTipoContacto(TelefonosTipoContactoRequest peticion)
        {
            return GestionBll.TelefonosTipoContacto(peticion);
        }

        public ResultadosGestionResponse ResultadoGestionTelefonica(ResultadosGestionPeticion peticion)
        {
            return GestionBll.ResultadoGestionTelefonica(peticion);
        }

        public ResultadosGestionResponse ResultadosGestion(ResultadosGestionPeticion peticion)
        {
            return GestionBll.ResultadosGestion(peticion);
        }

        public ConfiguracionNotificacionResponse ConfiguracionNotificacion(ConfiguracionNotificacionRequest peticion)
        {
            return GestionBll.ConfiguracionNotificacion(peticion);
        }

        public DevolverLlamadaResponse DevolverLlamada(DevolverLlamadaRequest peticion)
        {
            return GestionBll.DevolverLlamada(peticion);
        }

        public CargarEnviosCourierResponse CargarEnviosCourier()
        {
            return GestionBll.CargarEnviosCourier();
        }

        public VerEnvioCourierDetalleResponse VerEnvio(VerEnvioCourierDetalleRequest peticion)
        {
            return GestionBll.VerEnvio(peticion);
        }

        public ObtenerSolicitudNotificacionesCourierResponse ObtenerSolicitudNotificacionesCourier()
        {
            return GestionBll.ObtenerSolicitudNotificacionesCourier();
        }

        public ImprimirNotificacionesCourierResponse ImprimirNotificacionesCourier(ImprimirNotificacionesCourierRequest peticion)
        {
            return GestionBll.ImprimirNotificacionesCourier(peticion);
        }

        public TipoGestionDomiciliacionResponse TipoGestionDomiciliacion()
        {
            return GestionBll.TipoGestionDomiciliacion();
        }
        public TipoGestionPromesaResponse TipoGestionPromesa()
        {
            return GestionBll.TipoGestionPromesa();
        }
        public TipoGestionTipoNotificacionReponse TipoGestionTipoNotificacion()
        {
            return GestionBll.TipoGestionTipoNotificacion();
        }

        public GestionDomiciacionesResponse GestionDomiciaciones(GestionDomiciacionesRequest peticion)
        {
            return GestionBll.GestionDomiciaciones(peticion);
        }

        public PeticionDomicilacionResponse PeticionDomicilacion(PeticionDomicilacionRequest peticion)
        {
            return GestionBll.PeticionDomicilacion(peticion);
        }

        public ConexionTelefonicaResponse ConexionTelefonica(ConexionTelefonicaRequest peticion)
        {
            return GestionBll.ConexionTelefonica(peticion);
        }

        public Respuesta FinalizarSesionTelefonica(ActualizarSesionTelefonicaRequest peticion)
        {
            return GestionBll.FinalizarSesionTelefonica(peticion);
        }

        public Respuesta ActualizarSesionTelefonica(ActualizarSesionTelefonicaRequest peticion)
        {
            return GestionBll.ActualizarSesionTelefonica(peticion);
        }

        public DevolucionLlamadaGestionResponse DevolucionLlamadaGestion(DevolucionLlamadaGestionRequest peticion)
        {
            return GestionBll.DevolucionLlamadaGestion(peticion);
        }

        public GestionNotificacionResponse GestionNotificacion(GestionNotificacionRequest peticion)
        {
            return GestionBll.GestionNotificacion(peticion);
        }

        public ValidarPeticionDomicilacionResponse ValidarPeticionDomicilacion(ValidarPeticionDomicilacionRequest peticion)
        {
            return GestionBll.ValidarPeticionDomicilacion(peticion);
        }
        #endregion

        #region AdministrarAsignacion
        #region GET
        public CargaAsignacionResponse CargaAsignacion(CargaAsignacionRequest peticion)
        {
            return AsignacionesBLL.CargaAsignacion(peticion);
        }

        public CargaAsignacionesResponse CargaAsignaciones(CargaAsignacionesRequest peticion)
        {
            return AsignacionesBLL.CargaAsignaciones(peticion);
        }

        public CargaAsignacionDetallesResponse CargaAsignacionDetalles(CargaAsignacionDetallesRequest peticion)
        {
            return AsignacionesBLL.CargaAsignacionDetalles(peticion);
        }
        #endregion

        #region POST
        public CargarAsignacionResponse CargarAsignacion(CargarAsignacionRequest peticion)
        {
            return AsignacionesBLL.CargarAsignacion(peticion);
        }

        public ActualizarCargaAsignacionResponse ActualizarCargaAsignacion(ActualizarCargaAsignacionRequest peticion)
        {
            return AsignacionesBLL.ActualizarCargaAsignacion(peticion);
        }

        public AsignarCargaAsignacionResponse AsignarCargaAsignacion(AsignarCargaAsignacionRequest peticion)
        {
            return AsignacionesBLL.AsignarCargaAsignacion(peticion);
        }
        #endregion

        #endregion

        #region Gestores
        #region GET
        public GestoresAsignadosResponse GestoresAsignados(GestoresAsignadosRequest peticion)
        {
            return GestorBLL.GestoresAsignados(peticion);
        }
        #endregion
        #endregion

        #region PeticionReestructura
        public List<Combo> ObtenerCombo_Bucket_PE()
        {
            return BucketBll_PE.ObtenerCATCombo();
        }

        public List<Combo> ObtenerCombo_MotivoPeticionReestructura_PE()
        {
            return MotivoPeticionReestructuraBll_PE.ObtenerCATCombo();
        }

        public List<Combo> ObtenerCombo_TipoReestructura_PE()
        {
            return TipoReestructuraBll_PE.ObtenerCATCombo();
        }

        public List<PeticionReestructura.BusquedaClienteSolicitudPRRes> Clientes_Filtrar(string Accion, PeticionReestructura.BusquedaClienteSolicitudPRReq entity)
        {
            return PeticionReestructuraBll.Clientes_Filtrar(Accion, entity);
        }

        public Resultado<bool> InsertarPeticionReestructura(string Accion, InsertarPeticionReestructuraRequest entity)
        {
            return PeticionReestructuraBll.InsertarPeticionReestructura(Accion, entity);
        }

        public Resultado<List<PeticionReestructura>> ObtenerPeticionReestructura(string Accion, PeticionReestructura entity)
        {
            return PeticionReestructuraBll.ObtenerPeticionReestructura(Accion, entity);
        }

        public Resultado<TBCreditos.CalculoLiquidacion> CalcularLiquidacionPeticionReestructura(TBCreditos.Liquidacion eOwner)
        {
            return PeticionReestructuraBll.CalcularLiquidacionPeticionReestructura(eOwner);
        }

        public Resultado<bool> AutorizarPeticionReestructura(AutorizarPeticionReestructuraRequest entity)
        {
            return PeticionReestructuraBll.AutorizarPeticionReestructura(entity);
        }

        public Resultado<bool> RechazarPeticionReestructura(RechazarPeticionReestructuraRequest entity)
        {
            return PeticionReestructuraBll.RechazarPeticionReestructura(entity);
        }

        public List<Combo> ObtenerCombo_PlazosReestructura(ObtenerComboPlazosReestructuraRequest entity)
        {
            return PeticionReestructuraBll.ObtenerCombo_PlazosReestructura(entity);
        }

        public PeticionReestDocDigitalesResponse PeticionReestDocDigitales(PeticionReestDocDigitalesRequest peticion)
        {
            return PeticionReestructuraBll.PeticionReestDocDigitales(peticion);
        }

        public ValidarPeticionReestructuraResponse ValidarPeticionReestructura(ValidarPeticionReestructuraRequest peticion)
        {
            return PeticionReestructuraBll.ValidarPeticionReestructura(peticion);
        }

        public EstatusPeticionReestResponse EstatusPeticionReestEtapaVal()
        {
            return PeticionReestructuraBll.EstatusPeticionReestEtapaVal();
        }

        public ReiniciarProcesoDigitalPetReestResponse ReiniciarProcesoDigitalPetReest(ReiniciarProcesoDigitalPetReestRequest peticion)
        {
            return PeticionReestructuraBll.ReiniciarProceso(peticion);
        }
        #endregion
    }
}
