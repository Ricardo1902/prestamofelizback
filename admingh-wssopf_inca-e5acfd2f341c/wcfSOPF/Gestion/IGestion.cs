using SOPF.Core.Entities;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Model;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Request.Reportes;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Response;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Poco.Gestiones;
using System.Collections.Generic;
using System.ServiceModel;

namespace wcfSOPF
{
    public partial interface IGestion
    {
        #region GestionesNuevo
        [OperationContract]
        List<TipoGestion> ObtenerCatalogoTipoGestion(int accion);

        [OperationContract]
        ObtenerDatosClienteGestionResponse ObtenerDatosClienteGestion(ObtenerDatosClienteGestionRequest peticion);

        [OperationContract]
        ImpresionNotificacionResponse ImpresionAvisoCobranza(ImpresionAvisoCobranzaRequest peticion);

        [OperationContract]
        ImpresionNotificacionResponse ProcesarAvisoCobranza(ImpresionAvisoCobranzaRequest peticion);

        [OperationContract]
        ImpresionNotificacionResponse ImpresionAvisoPago(ImpresionAvisoPagoRequest peticion);

        [OperationContract]
        ImpresionNotificacionResponse ProcesarAvisoPago(ImpresionAvisoPagoRequest peticion);

        [OperationContract]
        ImpresionNotificacionResponse ImpresionNotificacionPrejudicial(ImpresionNotificacionPrejudicialRequest peticion);

        [OperationContract]
        ImpresionNotificacionResponse ProcesarNotificacionPrejudicial(ImpresionNotificacionPrejudicialRequest peticion);

        [OperationContract]
        ImpresionNotificacionResponse ImpresionUltimaNotificacionPrejudicial(ImpresionUltimaNotificacionPrejudicialRequest peticion);

        [OperationContract]
        ImpresionNotificacionResponse ProcesarUltimaNotificacionPrejudicial(ImpresionUltimaNotificacionPrejudicialRequest peticion);

        [OperationContract]
        ImpresionNotificacionResponse ImpresionCitacionPrejudicial(ImpresionCitacionPrejudicialRequest peticion);

        [OperationContract]
        ImpresionNotificacionResponse ProcesarCitacionPrejudicial(ImpresionCitacionPrejudicialRequest peticion);

        [OperationContract]
        ImpresionNotificacionResponse ImpresionPromocionReduccionMora(ImpresionPromocionReduccionMoraRequest peticion);

        [OperationContract]
        ImpresionNotificacionResponse ProcesarPromocionReduccionMora(ImpresionPromocionReduccionMoraRequest peticion);

        [OperationContract]
        ObtenerCarteraAsignadaResponse ObtenerCarteraAsignada(ObtenerCarteraAsignadaRequest peticion);

        [OperationContract]
        ObtenerGestionesResponse ObtenerGestionesPorIdCuenta(ObtenerGestionRequest peticion);

        [OperationContract]
        ObtenerDatosClienteAcuerdoResponse ObtenerDatosClienteAcuerdo(ObtenerDatosClienteAcuerdoRequest peticion);

        [OperationContract]
        ObtenerAcuerdoPagoConDetalleResponse ObtenerAcuerdoPagoConDetalle(ObtenerAcuerdoPagoRequest peticion);

        [OperationContract]
        ObtenerAcuerdoPagoConDetalleResponse ObtenerAcuerdoPagoConDetallePorSolicitud(ObtenerAcuerdoPagoRequest peticion);

        [OperationContract]
        ObtenerAcuerdoPagoResponse ObtenerAcuerdoPago(ObtenerAcuerdoPagoRequest peticion);

        [OperationContract]
        ObtenerAcuerdosPagoResponse ObtenerAcuerdosPago(ObtenerAcuerdoPagoRequest peticion);

        [OperationContract]
        ObtenerAcuerdoPagoResponse ObtenerAcuerdoPagoSolicitud(ObtenerAcuerdoPagoRequest peticion);

        [OperationContract]
        ObtenerAcuerdoPagoDetalleResponse ObtenerAcuerdoPagoDetalle(ObtenerAcuerdoPagoDetalleRequest peticion);

        [OperationContract]
        AltaAcuerdoPagoResponse AltaAcuerdoPago(AltaAcuerdoPagoRequest peticion);

        [OperationContract]
        CancelarAcuerdoPagoResponse CancelarAcuerdoPago(CancelarAcuerdoPagoRequest peticion);

        [OperationContract]
        ObtenerPagosGestionesResponse ObtenerPagosGestiones(ObtenerPagosGestionesRequest peticion);
        #endregion

        #region Gestiones
        #region GET
        [OperationContract]
        GestoresResponse Gestores(GestoresRequest peticion);

        [OperationContract]
        ObtenerTipoContactosResponse TipoContactosGestTelefonico(Peticion peticion);

        [OperationContract]
        ObtenerTipoContactosResponse ObtenerTipoContactos(TipoContactosRequest peticion);

        [OperationContract]
        ResultadosGestionResponse ResultadoGestionTelefonica(ResultadosGestionPeticion peticion);

        [OperationContract]
        TelefonosTipoContactoResponse TelefonosTipoContacto(TelefonosTipoContactoRequest peticion);

        [OperationContract]
        ResultadosGestionResponse ResultadosGestion(ResultadosGestionPeticion peticion);

        [OperationContract]
        ConfiguracionNotificacionResponse ConfiguracionNotificacion(ConfiguracionNotificacionRequest peticion);

        [OperationContract]
        DevolverLlamadaResponse DevolverLlamada(DevolverLlamadaRequest peticion);

        [OperationContract]
        CargarEnviosCourierResponse CargarEnviosCourier();

        [OperationContract]
        ObtenerSolicitudNotificacionesCourierResponse ObtenerSolicitudNotificacionesCourier();

        [OperationContract]
        ConexionTelefonicaResponse ConexionTelefonica(ConexionTelefonicaRequest peticion);

        [OperationContract]
        DevolucionLlamadaGestionResponse DevolucionLlamadaGestion(DevolucionLlamadaGestionRequest peticion);

        [OperationContract]
        GestionNotificacionResponse GestionNotificacion(GestionNotificacionRequest peticion);

        [OperationContract]
        ValidarPeticionDomicilacionResponse ValidarPeticionDomicilacion(ValidarPeticionDomicilacionRequest peticion);
        #endregion

        #region POST
        [OperationContract]
        VerEnvioCourierDetalleResponse VerEnvio(VerEnvioCourierDetalleRequest peticion);

        [OperationContract]
        ImprimirNotificacionesCourierResponse ImprimirNotificacionesCourier(ImprimirNotificacionesCourierRequest peticion);

        [OperationContract]
        TipoGestionDomiciliacionResponse TipoGestionDomiciliacion();

        [OperationContract]
        TipoGestionPromesaResponse TipoGestionPromesa();
        [OperationContract]
        TipoGestionTipoNotificacionReponse TipoGestionTipoNotificacion();

        [OperationContract]
        GestionDomiciacionesResponse GestionDomiciaciones(GestionDomiciacionesRequest peticion);

        [OperationContract]
        PeticionDomicilacionResponse PeticionDomicilacion(PeticionDomicilacionRequest peticion);

        [OperationContract]
        Respuesta FinalizarSesionTelefonica(ActualizarSesionTelefonicaRequest peticion);

        [OperationContract]
        Respuesta ActualizarSesionTelefonica(ActualizarSesionTelefonicaRequest peticion);
        #endregion
        #endregion

        #region AdministrarAsignacion
        #region GET
        [OperationContract]
        CargaAsignacionResponse CargaAsignacion(CargaAsignacionRequest peticion);

        [OperationContract]
        CargaAsignacionesResponse CargaAsignaciones(CargaAsignacionesRequest peticion);

        [OperationContract]
        CargaAsignacionDetallesResponse CargaAsignacionDetalles(CargaAsignacionDetallesRequest peticion);

        #endregion

        #region POST
        [OperationContract]
        CargarAsignacionResponse CargarAsignacion(CargarAsignacionRequest peticion);

        [OperationContract]
        ActualizarCargaAsignacionResponse ActualizarCargaAsignacion(ActualizarCargaAsignacionRequest peticion);

        [OperationContract]
        AsignarCargaAsignacionResponse AsignarCargaAsignacion(AsignarCargaAsignacionRequest peticion);

        #endregion
        #endregion

        #region Gestores
        #region GET
        [OperationContract]
        GestoresAsignadosResponse GestoresAsignados(GestoresAsignadosRequest peticion);
        #endregion
        #endregion

        #region PeticionReestructura
        [OperationContract]
        List<Combo> ObtenerCombo_Bucket_PE();

        [OperationContract]
        List<Combo> ObtenerCombo_MotivoPeticionReestructura_PE();

        [OperationContract]
        List<Combo> ObtenerCombo_TipoReestructura_PE();

        [OperationContract]
        List<PeticionReestructura.BusquedaClienteSolicitudPRRes> Clientes_Filtrar(string Accion, PeticionReestructura.BusquedaClienteSolicitudPRReq entity);

        [OperationContract]
        Resultado<bool> InsertarPeticionReestructura(string Accion, InsertarPeticionReestructuraRequest entity);

        [OperationContract]
        Resultado<List<PeticionReestructura>> ObtenerPeticionReestructura(string Accion, PeticionReestructura entity);

        [OperationContract]
        Resultado<TBCreditos.CalculoLiquidacion> CalcularLiquidacionPeticionReestructura(TBCreditos.Liquidacion eOwner);

        [OperationContract]
        Resultado<bool> AutorizarPeticionReestructura(AutorizarPeticionReestructuraRequest entity);

        [OperationContract]
        Resultado<bool> RechazarPeticionReestructura(RechazarPeticionReestructuraRequest entity);

        [OperationContract]
        List<Combo> ObtenerCombo_PlazosReestructura(ObtenerComboPlazosReestructuraRequest entity);

        [OperationContract]
        PeticionReestDocDigitalesResponse PeticionReestDocDigitales(PeticionReestDocDigitalesRequest peticion);

        [OperationContract]
        ValidarPeticionReestructuraResponse ValidarPeticionReestructura(ValidarPeticionReestructuraRequest peticion);

        [OperationContract]
        EstatusPeticionReestResponse EstatusPeticionReestEtapaVal();

        [OperationContract]
        ReiniciarProcesoDigitalPetReestResponse ReiniciarProcesoDigitalPetReest(ReiniciarProcesoDigitalPetReestRequest peticion);
        #endregion
    }
}
