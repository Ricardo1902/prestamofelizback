﻿using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;

using SOPF.Core.Entities;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Model.Request.Creditos;
using SOPF.Core.Model.Response.Creditos;

namespace wcfSOPF
{
    [ServiceContract]
    public partial interface ICredito
    {
        [OperationContract]
        List<TBCreditos> ObtenerCredito(int accion, int cuenta);

        [OperationContract]
        DataSet ObtenerCreditoListado(int Accion, int IdSolicitud, int IdCuenta, int IdCliente, string Cliente, string FechaInicial, string FechaFinal, int IdSucursal, int IdUsuario, int comodin1, string comodin2);

        [OperationContract]
        DataSet ObtenerTBCreditosFiniquitos(int accion, int cuenta, int Usuario);

        [OperationContract]
        List<DataResultCredito.Usp_ObtenerTablaAmortizacion> ObtenerRecibos(int idCuenta);

        [OperationContract]
        List<DataResultCredito.Usp_HistorialPagos> ObtenerHistorialPagos(int idCuenta);

        [OperationContract]
        List<DataResultCredito.Usp_SelCredito> ObtenerCreditosFechas(DateTime fechaIni, DateTime fechaFin, int Accion, int IdPromotor);

        [OperationContract]
        double ObtenerCat(int idCuenta);

        [OperationContract]
        DataSet ObtenerEdoCta(int idCuenta, int idCompania, DateTime FecIni, DateTime FecFin);

        [OperationContract]
        List<DataResultCredito.Usp_Error> CompDispersion(int idcuenta, decimal monto);

        [OperationContract]
        void CreaXMLCirculo(int Accion, DateTime Fecha);

        [OperationContract]
        List<CortesDispersion> ObtenerCortesPendientes(int Accion, int idcorte);

        [OperationContract]
        List<DataResultCredito.Usp_SelDetalleCorteDispersion> ObtenerDetalleCorte(int Accion, int idcorte);

        [OperationContract]
        void ActualizarCorteDispersionDetalle(int Accion, int idCorte, int idsolicitud);


        [OperationContract]
        List<HistorialCobranza> ObtenerHistorialCobranza(int idBanco, int TipoConvenio, int Convenio, DateTime FecIni, DateTime Fecfin);

        [OperationContract]
        List<HistorialCobranza> ObtenerHistorialCobranzaMontos(int idBanco, int TipoConvenio, int Convenio, DateTime FecIni, DateTime Fecfin);

        [OperationContract]
        void InsertaPago(int idsolicitud, decimal monto, DateTime fecha, decimal Comision, int id, int Accion, int Origen, DateTime Quincena, int GrupoRelacion, string TipoPago, int resultHeaderId);

        [OperationContract]
        int ObtenerGrupoRelacion(int Accion, int idCobranzaBanco, int idReciboCobranza);

        [OperationContract]
        int GeneraId();

        [OperationContract]
        DataSet ConsultarPagosAplicados(int tipoConsul, int IdGrupo, string fch_pago);

        [OperationContract]
        List<AutorizaPagosEfectivo> ConsultarPagoEfectivo(DateTime FchIni, DateTime FchFin, string Tip_Consul);

        [OperationContract]
        void AutorizarPagoEfectivo(int IdFolio, decimal MontoAplicado, int Accion, int idGpo, int IdUser, int IdUserAut, DateTime Fecha, int IdSolicitud);

        [OperationContract]
        DataResultCredito.Usp_SelDatosPago ObtenerDatosPago(int idproducto, decimal capital, int tipocredito, decimal deducciones, decimal ingreso);

        [OperationContract]
        List<CatProducto> ObtenerCatProducto(int Accion, int idProducto, string producto);

        [OperationContract]
        TBCreditos.GeneraReferenciaCIE GeneraReferenciaCIE(DateTime FechaAutoriza, int idsolicitud, int idusuario, int origen);

        [OperationContract]
        Resultado<TBCreditos.AmpliacionEntity> ObtenerInfoAmpliacionAnterior(int IdSolicitud);

        [OperationContract]
        Resultado<TBCreditos.AmpliacionEntity> ObtenerInfoAmpliacionPosterior(int IdSolicitud);

        [OperationContract]
        Resultado<TBCreditos.CalculoLiquidacion> CalcularLiquidacion(TBCreditos.Liquidacion eOwner);

        [OperationContract]
        Resultado<TBCreditos.Liquidacion> RegistrarLiquidacion(TBCreditos.Liquidacion eOwner);

        [OperationContract]
        TBCreditos.Liquidacion ObtenerLiquidacionActiva(TBCreditos.Liquidacion eOwner);

        [OperationContract]
        TBCreditos.Liquidacion ObtenerLiquidacionCredito(TBCreditos.Liquidacion eOwner);

        [OperationContract]
        Resultado<TBCreditos.Liquidacion> ConfirmarLiquidacion(TBCreditos.Liquidacion eOwner);

        [OperationContract]
        Resultado<TBCreditos.Liquidacion> CancelarLiquidacion(TBCreditos.Liquidacion eOwner);

        [OperationContract]
        List<LotesImpresion> ConsultarLotesImpresion(int Accion, int IdLote, DateTime Fecha, int IdSucursal, int IdUsuario, int FolioInicial, int FolioFinal, int IdEstatus, int IdFormato);

        [OperationContract]
        void InsertarLote(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdEstatus, int IdFolio);

        [OperationContract]
        void ActualizarLote(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdEstatus, int IdFolio);

        [OperationContract]
        void InsertarReimpresion(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdFolio, string Comentario, int IdEstatus);

        [OperationContract]
        void EliminarReimpresion(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdFolio, string Comentario, int IdEstatus);

        [OperationContract]
        List<ReimpresionFormatos> ConsultarReimpresion(int Accion, int IdFolio, DateTime Fecha, int IdSucursal, int IdUsuario, int FolioInicial, int FolioFinal, int IdEstatus, int IdFormato);

        [OperationContract]
        void ActualizarReimpresion(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdFolio, string Comentario, int IdEstatus);

        [OperationContract]
        List<PromoRetenciones> ObtenerPromoRetenciones(int Accion, int IdCliente, int IdSucursal, int IdCuenta, int IdUsuario);

        [OperationContract]
        List<TBCreditos.CreditoRelacionado> ObtenerCreditosRelacionados(int Accion, int idcliente);

        [OperationContract]
        List<TBCreditos.CreditoRelacionado> ObtenerCreditosRelacionadosBySol(int Accion, int idSolicitud);

        [OperationContract]
        void ActualizarCreditosRenovacion(int Accion, int idsolicitud, int idcuenta, decimal LiqCapital, decimal InteresDev, int idcliente);

        [OperationContract]
        Resultado<ObtenerSimulacionCreditoResponse> ObtenerSimulacionCredito(ObtenerSimulacionCreditoRequest entity);

        [OperationContract]
        Resultado<List<ConsultaExpediente>> ConsultaExpediente(string Busqueda);

        [OperationContract]
        TipoCreditoCliente ObtenerTipoCreditoCliente(int idCliente);
    }
}
