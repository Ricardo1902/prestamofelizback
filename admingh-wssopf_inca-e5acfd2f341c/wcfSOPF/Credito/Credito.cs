﻿using SOPF.Core.BusinessLogic.Creditos;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Creditos;
using System;
using System.Collections.Generic;
using System.Data;

namespace wcfSOPF
{
    partial class SOPF : ICredito
    {
        public List<TBCreditos> ObtenerCredito(int accion, int cuenta)
        {
            List<TBCreditos> resultado = new List<TBCreditos>();
            return TBCreditosBll.ObtenerTBCreditos(accion, cuenta);
        }

        public DataSet ObtenerCreditoListado(int Accion, int IdSolicitud, int IdCuenta, int IdCliente, string Cliente, string FechaInicial, string FechaFinal, int IdSucursal, int IdUsuario, int comodin1, string comodin2)
        {            
            return TBCreditosBll.ObtenerTBCreditosListado(Accion, IdSolicitud, IdCuenta, IdCliente, Cliente, FechaInicial, FechaFinal, IdSucursal, IdUsuario, comodin1, comodin2);
        }

        public DataSet ObtenerTBCreditosFiniquitos(int accion, int cuenta, int usuario)
        {
            return TBCreditosBll.ObtenerTBCreditosFiniquitos(accion, cuenta, usuario);
        }

        public List<DataResultCredito.Usp_ObtenerTablaAmortizacion> ObtenerRecibos(int idCuenta)
        {
            return TBRecibosBll.ObtenerTBRecibos(idCuenta);
        }

        public List<DataResultCredito.Usp_HistorialPagos> ObtenerHistorialPagos(int idCuenta)
        {
            return TBCobranzaBll.ObtenerHistorialPagos(idCuenta);
        }

        public List<DataResultCredito.Usp_SelCredito> ObtenerCreditosFechas(DateTime fechaIni, DateTime fechaFin, int Accion, int IdPromotor)
        {
            return TBCreditosBll.ObtenerTBCreditosFechas(fechaIni, fechaFin, Accion, IdPromotor);
        }

        public double ObtenerCat(int idCuenta)
        {
            return TBCreditosBll.ObtenerCat(idCuenta);
        }

        public DataSet ObtenerEdoCta(int idCuenta, int idCompania, DateTime FecIni, DateTime FecFin)
        {
            DataSet ds = new DataSet();
            ds = EdoCtaBll.ObtenerEdoCta(idCuenta, idCompania, FecIni, FecFin);
            return ds;
        }

        public List<DataResultCredito.Usp_Error> CompDispersion(int idcuenta, decimal monto)
        {
            return CompDispersionBll.CompDispersion(idcuenta, monto);
        }

        public void CreaXMLCirculo(int Accion, DateTime Fecha)
        {
            RepCirculoBll.ObtenerRepCirculo(Accion, Fecha);
        }

        public List<CortesDispersion> ObtenerCortesPendientes(int Accion, int idcorte)
        {
            return CortesDispersionBll.ObtenerCortesDispersion(Accion, idcorte);
        }

        public List<DataResultCredito.Usp_SelDetalleCorteDispersion> ObtenerDetalleCorte(int Accion, int idcorte)
        {
            return CorteDispersionDetalleBll.ObtenerCorteDispersionDetalle(Accion, idcorte);
        }

        public void ActualizarCorteDispersionDetalle(int Accion, int idCorte, int idsolicitud)
        {
            CorteDispersionDetalleBll.ActualizarCorteDispersionDetalle(Accion, idCorte, idsolicitud);
        }

        public List<HistorialCobranza> ObtenerHistorialCobranza(int idBanco, int TipoConvenio, int Convenio, DateTime FecIni, DateTime Fecfin)
        {
            return TBCreditosBll.ObtenerHistorial(idBanco, TipoConvenio, Convenio, FecIni, Fecfin);
        }

        public List<HistorialCobranza> ObtenerHistorialCobranzaMontos(int idBanco, int TipoConvenio, int Convenio, DateTime FecIni, DateTime Fecfin)
        {
            return TBCreditosBll.ObtenerHistorialMontos(idBanco, TipoConvenio, Convenio, FecIni, Fecfin);
        }

        public void InsertaPago(int idsolicitud, decimal monto, DateTime Fecha, decimal Comision, int id, int Accion, int Origen, DateTime Quincena, int GrupoRelacion, string TipoPago, int resultHeaderId)
        {
            PagosMasivosBll.InsertarPago(idsolicitud, monto, Fecha, Comision, id, Accion, Origen, Quincena, GrupoRelacion, TipoPago, resultHeaderId);

        }

        public int ObtenerGrupoRelacion(int Accion, int idCobranzaBanco, int idReciboCobranza)
        {
            return PagosMasivosBll.ObtenerGrupoRelacion(Accion, idCobranzaBanco, idReciboCobranza);
        }

        public int GeneraId()
        {
            return PagosMasivosBll.generaId();
        }

        public DataSet ConsultarPagosAplicados(int tipoConsul, int IdGrupo, string fch_pago)
        {
            return PagosMasivosBll.ConsultarPagosAplicados(tipoConsul, IdGrupo, fch_pago);
        }

        public List<AutorizaPagosEfectivo> ConsultarPagoEfectivo(DateTime FchIni, DateTime FchFin, String Tip_Consul)
        {
            return PagosMasivosBll.ConsultarPagoEfectivo(FchIni, FchFin, Tip_Consul);
        }

        public void AutorizarPagoEfectivo(int IdFolio, decimal MontoAplicado, int Accion, int idGpo, int IdUser, int IdUserAut, DateTime Fecha, int IdSolicitud)
        {
            PagosMasivosBll.AutorizarPagoEfectivo(IdFolio, MontoAplicado, Accion, idGpo, IdUser, IdUserAut, Fecha, IdSolicitud);
        }

        public DataResultCredito.Usp_SelDatosPago ObtenerDatosPago(int idproducto, decimal capital, int tipocredito, decimal deducciones, decimal ingreso)
        {
            return TBCreditosBll.SelDatosPago(idproducto, capital, tipocredito, deducciones, ingreso);
        }

        public List<CatProducto> ObtenerCatProducto(int Accion, int idProducto, string producto)
        {
            return CatProductoBll.ObtenerCatProducto(Accion, idProducto, producto);
        }

        public TBCreditos.GeneraReferenciaCIE GeneraReferenciaCIE(DateTime FechaAutoriza, int idsolicitud, int idusuario, int origen)
        {
            return TBCreditosBll.GeneraReferenciaCIE(FechaAutoriza, idsolicitud, idusuario, origen);
        }

        public Resultado<TBCreditos.AmpliacionEntity> ObtenerInfoAmpliacionAnterior(int IdSolicitud)
        {
            return TBCreditosBll.ObtenerInfoAmpliacionAnterior(IdSolicitud);
        }

        public Resultado<TBCreditos.AmpliacionEntity> ObtenerInfoAmpliacionPosterior(int IdSolicitud)
        {
            return TBCreditosBll.ObtenerInfoAmpliacionPosterior(IdSolicitud);
        }

        public Resultado<TBCreditos.CalculoLiquidacion> CalcularLiquidacion(TBCreditos.Liquidacion eOwner)
        {
            return TBCreditosBll.CalcularLiquidacion(eOwner);
        }

        public Resultado<TBCreditos.Liquidacion> RegistrarLiquidacion(TBCreditos.Liquidacion eOwner)
        {
            return TBCreditosBll.RegistrarLiquidacion(eOwner);
        }

        public TBCreditos.Liquidacion ObtenerLiquidacionActiva(TBCreditos.Liquidacion eOwner)
        {
            return TBCreditosBll.ObtenerLiquidacionActiva(eOwner);
        }

        public TBCreditos.Liquidacion ObtenerLiquidacionCredito(TBCreditos.Liquidacion eOwner)
        {
            return TBCreditosBll.ObtenerLiquidacionCredito(eOwner);
        }

        public Resultado<TBCreditos.Liquidacion> ConfirmarLiquidacion(TBCreditos.Liquidacion eOwner)
        {
            return TBCreditosBll.ConfirmarLiquidacion(eOwner);
        }

        public Resultado<TBCreditos.Liquidacion> CancelarLiquidacion(TBCreditos.Liquidacion eOwner)
        {
            return TBCreditosBll.CancelarLiquidacion(eOwner);
        }

        public List<LotesImpresion> ConsultarLotesImpresion(int Accion, int IdLote, DateTime Fecha, int IdSucursal, int IdUsuario, int FolioInicial, int FolioFinal, int IdEstatus, int IdFormato)
        {
            return LotesImpresionBll.ConsultarLotesImpresion(Accion, IdLote, Fecha, IdSucursal, IdUsuario, FolioInicial, FolioFinal, IdEstatus, IdFormato);
        }

        public void InsertarLote(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdEstatus, int IdFolio)
        {
            LotesImpresionBll.InsertarLote(Accion, FechaAlta, IdUsuarioAlta, IdFormato, IdSucursal, FolioInicial, FolioFinal, IdEstatus, IdFolio);
        }

        public void ActualizarLote(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdEstatus, int IdFolio)
        {
            LotesImpresionBll.ActualizarLote(Accion, FechaAlta, IdUsuarioAlta, IdFormato, IdSucursal, FolioInicial, FolioFinal, IdEstatus, IdFolio);
        }

        public void InsertarReimpresion(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdFolio, string Comentario, int IdEstatus)
        {
            ReimpresionFormatosBll.InsertarReimpresion(Accion, FechaAlta, IdUsuarioAlta, IdFormato, IdSucursal, FolioInicial, FolioFinal, IdFolio, Comentario, IdEstatus);
        }

        public void EliminarReimpresion(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdFolio, string Comentario, int IdEstatus)
        {
            ReimpresionFormatosBll.EliminarReimpresion(Accion, FechaAlta, IdUsuarioAlta, IdFormato, IdSucursal, FolioInicial, FolioFinal, IdFolio, Comentario, IdEstatus);
        }

        public List<ReimpresionFormatos> ConsultarReimpresion(int Accion, int IdFolio, DateTime Fecha, int IdSucursal, int IdUsuario, int FolioInicial, int FolioFinal, int IdEstatus, int IdFormato)
        {
            return ReimpresionFormatosBll.ConsultarReimpresion(Accion, IdFolio, Fecha, IdSucursal, IdUsuario, FolioInicial, FolioFinal, IdEstatus, IdFormato);
        }

        public void ActualizarReimpresion(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdFolio, string Comentario, int IdEstatus)
        {
            ReimpresionFormatosBll.ActualizarReimpresion(Accion, FechaAlta, IdUsuarioAlta, IdFormato, IdSucursal, FolioInicial, FolioFinal, IdFolio, Comentario, IdEstatus);
        }

        public List<PromoRetenciones> ObtenerPromoRetenciones(int Accion, int IdCliente, int IdSucursal, int IdCuenta, int IdUsuario)
        {
            return PromoRetencionesBll.ObtenerPromoRetenciones(Accion, IdCliente, IdSucursal, IdCuenta, IdUsuario);
        }

        public List<TBCreditos.CreditoRelacionado> ObtenerCreditosRelacionados(int Accion, int idcliente)
        {
            return TBCreditosBll.ObtenerCreditosRelacionados(Accion, idcliente);
        }

        public List<TBCreditos.CreditoRelacionado> ObtenerCreditosRelacionadosBySol(int Accion, int idSolicitud)
        {
            return TBCreditosBll.ObtenerCreditosRelacionadosBySol(Accion, idSolicitud);
        }

        public void ActualizarCreditosRenovacion(int Accion, int idsolicitud, int idcuenta, decimal LiqCapital, decimal InteresDev, int idcliente)
        {
            TBCreditosBll.ActualizarCreditosRenovacion(Accion, idsolicitud, idcuenta, LiqCapital, InteresDev, idcliente);
        }

        public Resultado<List<ConsultaExpediente>> ConsultaExpediente(string Busqueda)
        {
            return TBCreditosBll.ConsultaExpediente(Busqueda);
        }

        public TipoCreditoCliente ObtenerTipoCreditoCliente(int idCliente)
        {
            return TBCreditosBll.ObtenerTipoCreditoCliente(idCliente);
        }
    }
}
