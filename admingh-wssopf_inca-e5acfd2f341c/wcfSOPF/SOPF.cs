﻿using SOPF.Core.BusinessLogic;
using SOPF.Core.BusinessLogic.Adelantofeliz;
using SOPF.Core.BusinessLogic.AhorroVoluntario;
using SOPF.Core.BusinessLogic.Catalogo;
using SOPF.Core.BusinessLogic.Clientes;
using SOPF.Core.BusinessLogic.Cobranza;
using SOPF.Core.BusinessLogic.CobranzaAdministrativa;
using SOPF.Core.BusinessLogic.Creditos;
using SOPF.Core.BusinessLogic.Gestiones;
using SOPF.Core.BusinessLogic.Hubble;
using SOPF.Core.BusinessLogic.Menu;
using SOPF.Core.BusinessLogic.Pulsus;
using SOPF.Core.BusinessLogic.Sistema;
using SOPF.Core.BusinessLogic.Solicitudes;
using SOPF.Core.BusinessLogic.Tesoreria;
using SOPF.Core.BusinessLogic.Usuario;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Adelantofeliz;
using SOPF.Core.Entities.AhorroVoluntario;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.Entities.Clientes;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.Entities.Hubble;
using SOPF.Core.Entities.Menu;
using SOPF.Core.Entities.Pulsus;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Entities.Tesoreria;
using SOPF.Core.Entities.Usuario;
using SOPF.Core.Model.Catalogo;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Catalogo;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Request.Creditos;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Request.Reportes;
using SOPF.Core.Model.Response;
using SOPF.Core.Model.Response.Catalogo;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Model.Response.Creditos;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Poco.Catalogo;
using SOPF.Core.Poco.Clientes;
using SOPF.Core.Poco.Gestiones;
using SOPF.Core.Poco.Hubble;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Xml;

namespace wcfSOPF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public partial class SOPF : IUsuario, IMenu, IGestion, ICredito, ICatalogo, ITesoreria, IAdelantoFeliz, IAhorroVoluntario, IPulsus, ICobranzaAdministrativa, ISolicitud, ISistema, IHubble
    {
        public void GuardarUsuario(TBCATUsuario Usuario)
        {
            TbCatUsuarioBll.InsertarTbCatUsuario(Usuario);
        }

        public string ValidarUsuario(string usuario, string contraseña, string IPAddress, string HostName, string MACAddress)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            respuesta = TbCatUsuarioBll.ValidaUsuario(usuario, contraseña, IPAddress, HostName, MACAddress);
            XmlDocument xmlRespuesta = new XmlDocument();
            StringBuilder xml = new StringBuilder();

            if (respuesta != null)
            {
                xml.Append("<Resultado>");
                xml.AppendFormat("<Error>{0}</Error>", respuesta.Codigo);
                xml.Append("<Mensajes>");
                xml.AppendFormat("<{0}>{1}</{0}>", "Mensaje", respuesta.Mensaje);
                xml.Append("</Mensajes>");
                xml.AppendFormat("<Usuario>{0}</Usuario>", respuesta.IdUsuario);
                xml.AppendFormat("<FechaEntro>{0}</FechaEntro>", respuesta.FechaEntro);
                xml.Append("</Resultado>");
            }

            xmlRespuesta.LoadXml(xml.ToString());
            return xmlRespuesta.InnerXml;



        }

        public List<TBCATUsuario> ObtenerUsuario(int Accion, int IdUsuario, string Usuario)
        {
            List<TBCATUsuario> resultado = new List<TBCATUsuario>();
            return TbCatUsuarioBll.ObtenerTbCatUsuario(Accion, IdUsuario, Usuario);
        }

        public List<TBCATUsuario> BuscarUsuario(int Accion, int IdUsuario, string Usuario, int idsucursal)
        {
            //List<TBCATUsuario> resultado = new List<TBCATUsuario>();
            return TbCatUsuarioBll.BuscarUsuario(Accion, IdUsuario, Usuario, idsucursal);
        }

        public bool EsAdministrador(int accion, int idUsuario)
        {
            return TbCatUsuarioBll.EsAdministrador(accion, idUsuario);
        }

        public List<TBCATMenu> ObtenerMenu(int accion, int idTipoUsuario, int idMenu, int nivel, int idModulo, int tipo)
        {
            List<TBCATMenu> resultado = new List<TBCATMenu>();
            return TBCATMenuBll.ObtenerTBCATMenu(accion, idTipoUsuario, idMenu, nivel, idModulo, tipo);
        }

        public List<TBCatTipoGestor> ObtenerTipoGestor()
        {
            List<TBCatTipoGestor> resultado = new List<TBCatTipoGestor>();
            return TBCatTipoGestorBll.ObtenerTBCatTipoGestor();
        }

        public List<TBCatGestores> ObtenerGestor(int accion, int idGestor, int idTipo, int usuario)
        {
            List<TBCatTipoGestor> resultado = new List<TBCatTipoGestor>();
            return TBCatGestoresBll.ObtenerTBCatGestores(accion, idGestor, idTipo, usuario);
        }

        public void GuardarGestor(int idTipo, int idUsuarioGes, int idUsuario)
        {
            TBCatGestoresBll.InsertarTBCatGestores(idTipo, idUsuarioGes, idUsuario);
        }

        public void EliminarGestor(int idUsuarioGes, int idUsuario)
        {
            TBCatGestoresBll.EliminarTBCatGestores(idUsuarioGes, idUsuario);
        }                        

        public DataSet ObtenerTBSolicitudes(int accion, int cuenta, int idcliente, string cliente, string fchInicial, string fchFinal, int idsucursal, int idconvenio, int idlinea)
        {
            return TBSolicitudesBll.ObtenerTBSolicitudes(accion, cuenta, idcliente, cliente, fchInicial, fchFinal, idsucursal, idconvenio, idlinea);
        }

        public void Asignar(TBAsignacion asignacion, int usuarioIdAsigna)
        {
            TBAsignacionBll.InsertarTBAsignacion(asignacion, usuarioIdAsigna);
        }

        public void Desasignar(TBAsignacion asignacion)
        {
            TBAsignacionBll.ActualizarTBAsignacion(asignacion);
        }

        public void AsignarMasivo(List<AsignarCuentasTMP> cuentas)
        {
            TBAsignacionBll.InsertarAsignacionMasiva(cuentas);
        }

        public void DesasignarMasivo(List<AsignarCuentasTMP> cuentas)
        {
            TBAsignacionBll.DesasignacionMasiva(cuentas);
        }

        public List<DataResult.Usp_ObtenerAsignacion> ObtenerAsignacion(int idGestor, int accion, int idCuenta, string cliente, int idSucursal, string DNI, string convenio)
        {
            return TBAsignacionBll.ObtenerTBAsignacion(idGestor, accion, idCuenta, cliente, idSucursal, DNI, convenio);
        }

        public ObtenerAsignacionAccionResponse ObtenerAsignacionAccion(ObtenerAsignacionAccionRequest peticion)
        {
            return TBAsignacionBll.ObtenerAsignacionAccion(peticion);
        }

        public DetalleCuenta ObtenerDetalleCuenta(int idCuenta)
        {


            return DetalleCuentaBll.ObtenerDetalleCuenta(idCuenta);
        }

        public List<Referencias> ObtenerReferencias(int idCuenta)
        {
            return ReferenciasBll.ObtenerReferencias(idCuenta);
        }

        public List<DataResult.Usp_ObtenerGestion> ObtenerGestion(int idCuenta)
        {
            return GestionBll.ObtenerGestion(idCuenta);
        }

        public ObtenerGestionPordIdGestionResponse ObtenerGestionPordIdGestion(ObtenerGestionRequest peticion)
        {
            return GestionBll.ObtenerGestionPordIdGestion(peticion);
        }

        public List<DataResult.Usp_ObtenerGestion> ObtenerGestionAccion(int Accion, int idCuenta, int idGestor, DateTime FecIni, DateTime FecFin, int idgestor)
        {
            return GestionBll.ObtenerGestion(Accion, idCuenta, idGestor, FecIni, FecFin, idgestor);
        }

        public List<CatResultadoGestion> ObtenerCatalogoResultado()
        {
            return CatResultadoGestionBll.ObtenerCatResultadoGestion();
        }
        public List<CatResultadoGestion> ObtenerCatalogoResultadoAccion(int accion, string resultado)
        {
            return CatResultadoGestionBll.ObtenerCatResultadoGestion(accion, resultado);
        }
        public int insertarGestion(Gestion gest)
        {
            return GestionBll.InsertarGestion(gest);
        }
        public InsertarGestionConResultadoResponse InsertarGestionConResultado(Gestion gestion)
        {
            return GestionBll.InsertarGestionConResultado(gestion);
        }
        public void insertarPromesaPago(PromesaPago promesa)
        {
            PromesaPagoBll.InsertarPromesaPago(promesa);
        }
        public List<PromesaPago> ObtenerPromesaPago(int idCuenta)
        {
            return PromesaPagoBll.ObtenerPromesaPago(idCuenta);
        }
        public List<TBCreditos> ObtenerOtrasCuentas(int idCuenta)
        {
            return GestionBll.ObtenerOtrasCuentas(idCuenta);
        }                
        public string ActualizaGestion(Gestion entidad)
        {
            return GestionBll.ActualizarGestion(entidad);
        }                
        public List<CatCNT> ObtenerCatCNT(int Accion, string Descripcion)
        {
            return CatCNTBll.ObtenerCatCNT(Accion, Descripcion);
        }        
        public string GeneraArchivoAfiliacion(int idBanco)
        {
            return CatArchivosBll.GeneraArchivoAfiliacion(idBanco);
        }
        public string GeneraArchivoAfiliacionBancomer(int idBanco)
        {
            return CatArchivosBll.GeneraArchivoAfiliacionBancomer(idBanco);
        }        
        public List<TBCATPromotor> ObtenerPromotores(int Accion, int Sucursal)
        {
            return TBCATPromotorBll.ObtenerTBCATPromotor(Accion, Sucursal);
        }
        public List<TBCATPromotor> ObtenerPromotoresPorCanalVenta(int Sucursal, int IdCanalVenta)
        {
            return TBCATPromotorBll.ObtenerTBCATPromotorPorCanalVenta(Sucursal, IdCanalVenta);
        }
        public List<TBCATBanco> ObtenerBancos(int Accion, int idBanco)
        {
            return TBCATBancoBll.ObtenerTBCATBanco(Accion, idBanco);
        }
        public List<CatCampana> ObtenerCampanas(int Accion, int idCampana)
        {
            return CatCampanaBll.ObtenerCatCampana(Accion, idCampana);
        }        
        public string GeneraArchivoDispersion(int Accion, int idBanco, int idCorte)
        {
            return CatArchivosBll.GeneraArchivoDispersion(Accion, idBanco, idCorte);
        }
        public List<ArchivosDispersion> ObtenerArchivoDispersion(int Accion, int IdArchivo)
        {
            return ArchivosDispersionBll.ObtenerArchivosDispersion(Accion, IdArchivo);
        }
        public string ActArchivoDispersion(ArchivosDispersion Archivo)
        {
            return ArchivosDispersionBll.ActualizarArchivosDispersion(Archivo);
        }
        public List<DataResult.RepGestiones> ReporteGestiones(int Accion, DateTime FecIni, DateTime FecFin, int idgestor)
        {
            return GestionBll.ReporteGestiones(Accion, FecIni, FecFin, idgestor);
        }
        public void InsetaEmpleado(Empleados Empleado)
        {
            EmpleadosBll.InsertarEmpleados(Empleado);
        }
        public int ConsultarEmpresa(int idusuario)
        {
            return EmpleadosBll.ConsultarEmpresa(idusuario);
        }
        public List<Empleados> ObtenerEmpleados(int Accion, int IdEmpresa, int NumEmpleado, string Nombre, string RFC, string CURP, int Id)
        {
            return EmpleadosBll.ObtenerEmpleados(Accion, IdEmpresa, NumEmpleado, Nombre, RFC, CURP, Id);
        }
        public List<Empresa> ObtenerEmpreda(int Accion, int IdEmpresa)
        {
            return EmpresaBll.ObtenerEmpresa(Accion, IdEmpresa);
        }
        public TB_CATEmpresa ObtenerEmpresa(int IdEmpresa)
        {
            return TBCATEmpresaBll_PE.Obtener(IdEmpresa);
        }        
        public List<TBCATTipoConvenio> ObtenerTipoConvenio(int Accion, int tipoconvenio, string convenio, int IdEstatus, int producto)
        {
            return TBCATTipoConvenioBll.ObtenerTBCATTipoConvenio(Accion, tipoconvenio, convenio, IdEstatus, producto);
        }
        public DataResultAhorro InsertarCredito(Credito Credito, Cliente Cliente, DateTime FechaPrimerPago)
        {
            return CreditoBll.InsertarCredito(Cliente, Credito, FechaPrimerPago);
        }
        public List<Cliente> ObtenerCliente(int Accion, int idCliente, string Nombre)
        {
            return ClienteBll.ObtenerCliente(Accion, idCliente, Nombre);
        }
        public void InsertarDevolverLlamada(int IdGestion, int IdCuenta, DateTime FchDevLlamada, int IdCliente, int IdGestor)
        {
            TBDevolverLlamadaBll.InsertarTBDevolverLlamada(IdGestion, IdCuenta, FchDevLlamada, IdCliente, IdGestor);
            //return 
        }

        public void InsertarAvisos(int IdCuenta, int Carteo, int sms, int wats, int mail, int circulo, int fisico)
        {
            AvisosBll.InsertarAvisos(IdCuenta, Carteo, sms, wats, mail, circulo, fisico);
            //return 
        }

        public Avisos ObtenerAvisos(int idCuenta)
        {
            return AvisosBll.ObtenerAvisos(idCuenta);
        }
        public List<TBDevolverLlamada> ObtenerTBDevolverLlamada(int idcuenta)
        {
            return TBDevolverLlamadaBll.ObtenerTBDevolverLlamada(idcuenta);
        }

        public List<PromesaPago> ReportePromesaPago(int idcuenta, DateTime Fec_Inicial, DateTime Fec_Final, int Accion)
        {
            return PromesaPagoBll.ReportePromesaPago(idcuenta, Fec_Inicial, Fec_Final, Accion);
        }
        public string InsertaDesarrolladora(Desarrolladora entidad, int Accion)
        {
            return DesarrolladoraBll.InsertarDesarrolladora(entidad, Accion);
        }
        public List<TBCATEstado> ObtenerTBCatEstado(int accion, int idEstado, string estado, int idEstatus)
        {
            return TBCATEstadoBll.ObtenerTBCATEstado(accion, idEstado, estado, idEstatus);
        }
        public List<TBCATCiudad> ObtenerTbCatCiudad(int accion, int idCiudad, string ciudad, int idEstatus, int IdEstado)
        {
            return TBCATCiudadBll.ObtenerTBCATCiudad(accion, idCiudad, ciudad, idEstatus, IdEstado);
        }
        public List<TBCATCiudad> ObtenerTbCatCiudadOrig(int accion, int IdEstado, int idCiudad)
        {
            return TBCATCiudadBll.ObtenerTBCATCiudadOrig(accion, IdEstado, idCiudad);
        }
        public List<TBCATColonia> ObtenerTbCatColonia(int accion, int ciudad)
        {
            return TBCATColoniaBll.ObtenerTBCATColonia(accion, ciudad, 0);
        }
        public List<TBCATColonia> ObtenerTbCatColoniaId(int accion, int ciudad, int idcolonia)
        {
            return TBCATColoniaBll.ObtenerTBCATColonia(accion, ciudad, idcolonia);
        }
        public DetalleColoniaVM ObtenerDetalleColonia(int accion, int idColonia)
        {
            return TBCATColoniaBll.ObtenerDetalleColonia(accion, idColonia);
        }
        public List<Promotor> ObtenerPromotor(int Accion, int idDesarrolladora, int idpromotor, int idusuario)
        {
            return PromotorBll.ObtenerPromotor(Accion, idDesarrolladora, idpromotor, idusuario);
        }
        public List<DataResultAhorro.SelCreditos> ObtenerCreditosPendientes(int Accion, int idEstatus, int Credito, int idDesarrolladora)
        {
            return CreditoBll.ObtenerCreditosPendientes(Accion, idEstatus, Credito, idDesarrolladora);
        }
        //public List<DataResultAhorro.SelCheques> ObtenerCreditoCheques(int Accion, int idEstatus, int Credito, int idDesarrolladora)
        //{
        //    return ChequeBll.ObtenerCreditoCheques(Accion, idEstatus, Credito, idDesarrolladora);
        //}
        //public void InsertarCheque(int idcuenta, string cheque, decimal monto, int idestatus)
        //{
        //    ChequeBll.InsertarCheque(idcuenta, cheque, monto, idestatus);
        //}
        public Credito ObtenerCreditoAhorro(int idcuenta)
        {
            return CreditoBll.ObtenerCredito(idcuenta);
        }
        public string SubirGestionesCampo(List<GestionesMovil> gestiones)
        {
            return GestionBll.InsertarGestionesMovil(gestiones);
        }
        public void ActualizarEstatusCredito(int idcuenta, int idestatus)
        {
            CreditoBll.ActualizarEstatusCredito(idcuenta, idestatus);
        }
        public List<Recibo> ObtenerRecibosAhorro(int idcuenta)
        {
            return ReciboBll.ObtenerRecibos(idcuenta);
        }
        public List<DataResultAhorro.SelRecibos> ObtenerRecibosAhorroRep(int idcuenta)
        {
            return ReciboBll.ObtenerRecibosAhorro(idcuenta);
        }
        public string GeneraArchivoAfiliacionPulsus(int idBanco, string EMail)
        {
            return AfiliacionEmpleadosBll.GeneraArchivoAfiliacion(idBanco, EMail);
        }
        public List<AfiliacionEmpleados> ObtenerCuentas(int accion)
        {
            return AfiliacionEmpleadosBll.ObtenerCuentas(accion);
        }
        public void insertaEmpleados(List<AfiliacionEmpleados> Empleados)
        {
            AfiliacionEmpleadosBll.insertaEmpleados(Empleados);
        }
        public List<CatTipoCredito> ObtenerCatTipoCredito(int Accion, int idTipo)
        {
            return CatTipoCreditoBll.ObtenerCatTipoCredito(Accion, idTipo);
        }
        //public List<HistorialCobranza> ObtenerHistorialCobranzaMontos(int idBanco, int TipoConvenio, int Convenio, DateTime FecIni, DateTime Fecfin)
        //{
        //    return TBCreditosBll.ObtenerHistorialMontos(idBanco, TipoConvenio, Convenio, FecIni, Fecfin);
        //}
        public List<CatEstatusCancelacion> ObtenerCatEstatusCancelacion(int Accion, int TipoEstatus)
        {
            return CatEstatusCancelacionBll.ObtenerCatEstatusCancelacion(Accion, TipoEstatus);
        }
        public void InsertarArchivoDispersion(ArchivoDispersion entidad)
        {
            ArchivoDispersionBll.InsertarArchivoDispersion(entidad);
        }
        public void GeneraArchivoDispersionEmp(int idBanco, string EMail)
        {
            ArchivoDispersionBll.GeneraArchivoDispersion(idBanco, EMail);
        }
        public List<AnalistaNomina> ObtenerAnalistaNomina(int Accion, int idUsuario, int IdAnalista)
        {
            return AnalistaNominaBll.ObtenerAnalistaNomina(Accion, idUsuario, IdAnalista);
        }
        public void InsertarRegistros(Registros entidad)
        {
            RegistrosBll.InsertarRegistros(entidad);
        }
        
        public void InsertarReferencia(Referencia Entidad)
        {
            ReferenciaBll.InsertarReferencia(Entidad);
        }

        public List<Referencia> ObtenerReferenciaAhorro(int accion, int idcuenta)
        {
            return ReferenciaBll.ObtenerReferencia(accion, idcuenta);
        }

        public string GeneraArchivoAfilDomiciliacion(int idBanco)
        {
            return CatArchivosBll.GeneraArchivoAfilDomiciliacion(idBanco);
        }                        

        public Resultado<List<TBSolicitudes.ListaNegra>> ValidaListaNegra(TBSolicitudes.ListaNegra entidad)
        {
            return TBSolicitudesBll.ValidaListaNegra(entidad);
        }
        
        public string GeneraArchivoDomiciliacionBancomer(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart, string idEmisora, string idconvenio, int AplicarGastos, int idPlantilla)
        {
            return CatArchivosBll.GeneraArchivoDomiciliacionBancomer(GpoEnvio, DiasVenini, DiasVenFin, IdBanco, Porcent, TotalArchivos, TotPart, idEmisora, idconvenio, AplicarGastos, idPlantilla);
        }

        public string GeneraArchivoDomiciliacionBanamex(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart, string idEmisora, string idconvenio, int AplicarGastos, int idPlantilla)
        {
            return CatArchivosBll.GeneraArchivoDomiciliacionBanamex(GpoEnvio, DiasVenini, DiasVenFin, IdBanco, Porcent, TotalArchivos, TotPart, idEmisora, idconvenio, AplicarGastos, idPlantilla);
        }

        public string GeneraArchivoDomiciliacionBanorte(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart, string IdEmisora, string idconvenio, int AplicarGastos, int idPlantilla)
        {
            return CatArchivosBll.GeneraArchivoDomiciliacionBanorte(GpoEnvio, DiasVenini, DiasVenFin, IdBanco, Porcent, TotalArchivos, TotPart, IdEmisora, idconvenio, AplicarGastos, idPlantilla);
        }

        public string EnvioCobro()
        {
            return EnvioCobroBll.EnivoCobro();
        }

        public List<SIPais> ObtenerSIPais()
        {
            return SIPaisBll.ObtenerSIPais();
        }

        public List<TipoPension> ObtenerTipoPension(int TipoConsulta)
        {
            return TipoPensionBll.ObtenerTipoPension(TipoConsulta);
        }

        public List<TBCATTipoVade> ObtenerTBCATTipoVade(int Tipo)
        {
            return TBCATTipoVadeBll.ObtenerTBCATTipoVade(Tipo);
        }

        public List<TBCATGiro> ObtenerTBCATGiro(int Accion, int idGiro, string Giro, int idestatus)
        {
            return TBCATGiroBll.ObtenerTBCATGiro(Accion, idGiro, Giro, idestatus);
        }

        public DataSet ObtenerTBCATMedios(int Accion, int idMedio, string Medio, int idestatus)
        {
            return TBCATMediosBll.ObtenerTBCATMedios(Accion, idMedio, Medio, idestatus);
        }

        public string GeneraArchivoAfiliacionRec(int idBanco)
        {
            return CatArchivosBll.GeneraArchivoAfiliacionRec(idBanco);
        }

        public void GeneraArchivoDispersionRecomienda(int Accion, int idBanco, int idCorte)
        {
            CatArchivosBll.GeneraArchivoDispersionRecomienda(Accion, idBanco, idCorte);
        }
        
        public List<TbCatFrecuencia> ObtenerTBCatFrecuencia(int usuarioSwitch, string frecs)
        {
            return TbCatFrecuenciaBll.ObtenerTBCatFrecuencia(usuarioSwitch, frecs);
        }

  
        //public void InsertaDetArchivoEfectivos(AutorizaPagosEfectivo.InsertaDetArchivoEfectivos entidad)
        //        {
        //            PagosMasivosBll.InsertaDetArchivoEfectivos(entidad);
        //        }

        //        public List<TBCreditos.TipoProducto> ObtenerTipoProductoCreditos (int Accion, int Cuenta)
        //        {
        //            return TBCreditosBll.ObtenerTipoProductoCreditos(Accion, Cuenta);
        //        }

        //        public List<AutorizaPagosEfectivo.ConsultaArchivoEfe> ConsultaArchivoEfe(int Accion, int IdSolicitud, int IdGrupo, int EsNomina)
        //        {
        //            return PagosMasivosBll.ConsultaArchivoEfe(Accion, IdSolicitud, IdGrupo, EsNomina);
        //        }

        //        public List<AutorizaPagosEfectivo.ConsultaDetArchivoEfe> ConsultaDetArchivoEfe(int Accion, int IdSolicitud, int IdGrupo, int EsNomina)
        //        {
        //            return PagosMasivosBll.ConsultaDetArchivoEfe(Accion, IdSolicitud, IdGrupo, EsNomina);
        //        }

        /* public DataSet ConsultaFormatos(int Accion, int idFormato, string Formato, int idestatus, int idsucursal)
         {
             return TBCATFormatosBll.ConsultaFormatos(Accion, idFormato, Formato, idestatus, idsucursal);
         }
         */

        public List<TBCATFormatos> ConsultaFormatos(int Accion, int idFormato, string Formato, int idestatus, int idsucursal)
        {
            return TBCATFormatosBll.ConsultaFormatos(Accion, idFormato, Formato, idestatus, idsucursal);
        }

        public List<TBCATEstatusLotes> ConsultaEstatusLotes(int Accion, int IdEstatusLote, string EstatusLote, int idestatus)
        {
            return TBCATEstatusLotesBll.ConsultaEstatusLotes(Accion, IdEstatusLote, EstatusLote, idestatus);
        }
        
        public List<TBCATSucursal> ObtenerSucursal(TBCATSucursal entidad)
        {
            return TBCATSucursalBll.ObtenerSucursal(entidad);
        }

        public List<TBCATCampanias> ConsultaCampania(int Accion, int idCampania, string Campania, int idestatus)
        {
            return TBCATCampaniaBll.ConsultaCampania(Accion, idCampania, Campania, idestatus);
        }
        
        public List<TBCATConvenio> ObtenerTBCATConvenio(int Accion, int idconvenio, string convenio, int idEstatus)
        {
            return TBCATConvenioBll.ObtenerTBCATConvenio(Accion, idconvenio, convenio, idEstatus);
        }
        public List<TBCATProducto> ObtenerTBCATProducto(int Accion, int idProducto, string Producto, int idEstatus, int TipoReno, int IdTipoCredito)
        {
            return TBCATProductoBll.ObtenerTBCATProducto(Accion, idProducto, Producto, idEstatus, TipoReno, IdTipoCredito);
        }                
        public DataResultSolicitudes InsertarTBSolicitudes(int Accion, int idUsuario, TBSolicitudes entidad)
        {
            return TBSolicitudesBll.InsertarTBSolicitudes(Accion, idUsuario, entidad);
        }
                
        public Resultado<ObtenerSimulacionCreditoResponse> ObtenerSimulacionCredito(ObtenerSimulacionCreditoRequest entity)
        {
            return TBCreditosBll.ObtenerSimulacionCredito(entity);
        }

        public string GeneraArchivoDomiciliacionBancomerEspecifica(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart)
        {
            return CatArchivosBll.GeneraArchivoDomiciliacionBancomerEspecifica(GpoEnvio, DiasVenini, DiasVenini, IdBanco, Porcent, TotalArchivos, TotPart);
        }
        public int UpdEstProceso(int Accion, int idSolicitud, int EstProceso, int idUsuario)
        {
            return TBSolicitudesBll.UpdEstProceso(Accion, idSolicitud, EstProceso, idUsuario);
        }        
        public List<TBSolicitudes.DatosSolicitud> ObtenerDatosSolicitud(int idsolicitud)
        {
            return TBSolicitudesBll.ObtenerDatosSolicitud(idsolicitud);
        }

        // JRVB - Tipo de Archivo
        public List<TipoArchivo> TipoArchivo_Filtrar(TipoArchivo Entity)
        {
            return TipoArchivoBL.Filtrar(Entity);
        }

        // JRVB - ArchivoMasivo        
        public List<ArchivoMasivo> DomiciliacionArchivo_ObtenerProcesados()
        {
            return ArchivoMasivoBL.ObtenerProcesados_Domiciliacion();
        }

        public List<ArchivoMasivo> DomiciliacionArchivo_ObtenerRechazados()
        {
            return ArchivoMasivoBL.ObtenerRechazados_Domiciliacion();
        }

        public List<ArchivoMasivo> DomiciliacionArchivo_ObtenerConError()
        {
            return ArchivoMasivoBL.ObtenerConError_Domiciliacion();
        }

        public List<ArchivoMasivo> PagosDirectosArchivo_ObtenerProcesados()
        {
            return ArchivoMasivoBL.ObtenerProcesados_PagosDirectos();
        }

        public List<ArchivoMasivo> PagosDirectosArchivo_ObtenerConError()
        {
            return ArchivoMasivoBL.ObtenerConError_PagosDirectos();
        }

        public ArchivoMasivo DomiciliacionArchivo_ObtenerDetalle(int IdArchivo)
        {
            return ArchivoMasivoBL.ObtenerDetalle(IdArchivo);
        }

        public Resultado<bool> DomiciliacionArchivo_Registrar(ArchivoMasivo Entity)
        {
            return ArchivoMasivoBL.Registrar(Entity);
        }

        public List<ArchivoMasivo> DomiciliacionArchivo_Filtrar(ArchivoMasivo Entity)
        {
            return ArchivoMasivoBL.Filtrar(Entity);
        }

        public ArchivoMasivo PagosDirectosArchivo_ObtenerDetalle(int IdArchivo)
        {
            return ArchivoMasivoBL.ObtenerDetalle(IdArchivo);
        }

        public Resultado<bool> PagosDirectosArchivo_Registrar(ArchivoMasivo Entity)
        {
            return ArchivoMasivoBL.Registrar_PagosDirectos(Entity);
        }

        public List<ArchivoMasivo> PagosDirectosArchivo_Filtrar(ArchivoMasivo Entity)
        {
            return ArchivoMasivoBL.Filtrar_PagosDirectos(Entity);
        }

        public List<TipoArchivo.LayoutArchivo> PagosDirectosLayout_Obtener()
        {
            return TipoArchivoBL.ObtenerLayoutPagosDirectos();
        }

        public Resultado<bool> EnvioDomiBBVA_Registrar(ArchivoMasivo Entity)
        {
            return ArchivoMasivoBL.Registrar_EnvioDomiBBVA(Entity);
        }

        public Resultado<bool> RespuestaDomiBBVA_Registrar(string nombresRespuestas, string nombreArchivo, int numArchivos)
        {
            return ArchivoMasivoBL.Registrar_RespuestaDomiBBVA(nombresRespuestas, nombreArchivo, numArchivos);
        }

        public List<ArchivoMasivo> Filtrar_DomiBBVA(ArchivoMasivo Entity)
        {
            return ArchivoMasivoBL.Filtrar_DomiBBVA(Entity);
        }

        public List<ArchivoMasivo> ObtenerArchivos_SubidosBBVA()
        {
            return ArchivoMasivoBL.ObtenerArchivos_SubidosBBVA();
        }

        public List<ArchivoMasivo> ObtenerArchivos_GeneradosBBVA()
        {
            return ArchivoMasivoBL.ObtenerArchivos_GeneradosBBVA();
        }

        public List<EnvioDomiBBVA> DescragaTXTDomiBBVA(string nombreArchivo)
        {
            return ArchivoMasivoBL.DescragaTXTDomiBBVA(nombreArchivo);
        }

        public List<HistorialCobros> ObtenerHistorialSeguimientoDomiBBVA(int idArchivo)
        {
            return ArchivoMasivoBL.ObtenerHistorialSeguimientoDomiBBVA(idArchivo);
        }

        public List<FitrarRespuestaBBVA> FiltrarRespuestaDomiBBVA(string nombreArchivo)
        {
            return ArchivoMasivoBL.FiltrarRespuestaDomiBBVA(nombreArchivo);
        }

        public List<CAT_Parametros> ObtenerParametros(int IdParametro, string ClaveParametro)
        {
            return ArchivoMasivoBL.ObtenerParametros(IdParametro,  ClaveParametro);
        }

        public void AltaArchivosDomiciliacion(string NombreArchivo, int IdBanco, int IdGrupo, int IdPlantilla, string Convenio, string Emisora, int DiasVenIni, int DiasVenFin, int AplicarGastos, int IdUsuario)
        {
            CatArchivosBll.AltaArchivosDomiciliacion(NombreArchivo, IdBanco, IdGrupo, IdPlantilla, Convenio, Emisora, DiasVenIni, DiasVenFin, AplicarGastos, IdUsuario);
        }

        public string GeneraArchivoDomiciliacionBancomerRemanente(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart, string idEmisora, string idConvenio, int AplicarGastos, int idPlantilla, int idGrupoEnvioOrigen)
        {
            return CatArchivosBll.GeneraArchivoDomiciliacionBancomerRemanente(GpoEnvio, DiasVenini, DiasVenFin, IdBanco, Porcent, TotalArchivos, TotPart, idEmisora, idConvenio, AplicarGastos, idPlantilla, idGrupoEnvioOrigen);
        }

        public List<TBCatCatalogos> ObtenerTBCatCatalogos(int Accion, int IdTipo, string valor, int idestatus, string Tipo)
        {
            return TBCatCatalogosBll.ObtenerTBCatCatalogos(Accion, IdTipo, valor, idestatus, Tipo);
        }

        public int ActualizaComentariosSol(int IdSolicitud, int RolUsuario, int Accion)
        {
            return TBSolicitudesBll.ActualizaComentariosSol(IdSolicitud, RolUsuario, Accion);
        }

        public int AltaComentariosSolicitud(int IdSolicitud, int IdComentarioPadre, string Comentarios, int UsuarioAlta, int RolUsuario)
        {
            return TBSolicitudesBll.AltaComentariosSolicitud(IdSolicitud, IdComentarioPadre, Comentarios, UsuarioAlta, RolUsuario);
        }

        public DataSet ConComentariosSolicitud(int IdSolicitud, int IdComentarioPadre, int TipoConsulta)
        {
            return TBSolicitudesBll.ConComentariosSolicitud(IdSolicitud, IdComentarioPadre, TipoConsulta);
        }

        public int ValidaClabeBll(string clabe)
        {
            return TBSolicitudesBll.ValidaClabeBll(clabe);
        }

        public Resultado<bool> ValidaTarjetaVISADomiciliacion(string NumeroTarjeta)
        {
            return TBSolicitudesBll.ValidaTarjetaVISADomiciliacion(NumeroTarjeta);
        }

        public CronogramaPagos Solicitudes_ObtenerCronograma(int IdSolicitud, DateTime? FechaDesembolso)
        {
            return TBSolicitudesBll.ObtenerCronograma(IdSolicitud, FechaDesembolso);
        }

        public void InsertarDetSolicitud(int Accion, int idsolicitud, int secuencia, string descripcion, decimal cantidad, decimal p_unitario, decimal capital, decimal interes, decimal Iva, string Pedido, string FinancieraPedido, int IdUsuario)
        {
            TBSolicitudesBll.InsertarDetSolicitud(Accion, idsolicitud, secuencia, descripcion, cantidad, p_unitario, capital, interes, Iva, Pedido, FinancieraPedido, IdUsuario);
        }
        
        public Resultado<bool> ActualizaSolicitud(TBSolicitudes.ModificaSolicitud Solicitud, int IDUsuario_Modifica)
        {
            return TBSolicitudesBll.ActualizaSolicitud(Solicitud, IDUsuario_Modifica);
        }

        public Resultado<bool> AsignaSolicitud(int IDSolicitud, int IDUsuario, int Validacion)
        {
            return TBSolicitudesBll.AsignaAnalista(IDSolicitud, IDUsuario, Validacion);
        }
        public Resultado<bool> InsertarCuentaEmergente(TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente, int idUsuario)
        {
            return TBSolicitudesBll.InsertarCuentaEmergente(cuentaEmergente, idUsuario);
        }
        public Resultado<bool> ActualizarCuentaEmergente(TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente, int idUsuario)
        {
            return TBSolicitudesBll.ActualizarCuentaEmergente(cuentaEmergente, idUsuario);
        }
        public List<Combo> ObtenerCombo_TipoConvenio()
        {
            return TBCATTipoConvenioBll.ObtenerCATCombo();
        }

        public List<Combo> ObtenerCombo_Convenio(int IDTipoConvenio)
        {
            return TBCATConvenioBll.ObtenerCATCombo(IDTipoConvenio);
        }

        public List<Combo> Sistema_ObtenerCombo_Convenio(int IDTipoConvenio)
        {
            return TBCATConvenioBll.Sistema_ObtenerCATCombo(IDTipoConvenio);
        }

        public List<Combo> ObtenerCombo_UsuariosRegional()
        {
            return TbCatUsuarioBll.ObtenerCATCombo();
        }

        public List<Combo> ObtenerCombo_SucursalRegional(int IDUsuarioRegional)
        {
            return TBCATSucursalBll.ObtenerCATCombo(IDUsuarioRegional);
        }

        public List<Combo> ObtenerCombo_Promotores(int IDSucursal)
        {
            return TBCATPromotorBll.ObtenerCATCombo(IDSucursal);
        }

        public List<Combo> ObtenerCATEst_Proceso()
        {
            return TBSolicitudesBll.ObtenerCATEst_Proceso();
        }
        //##### ##### ##### ##### #####
        public DataResultSolicitudes ValidaDatosBll(int Accion, string Parametro, int idSolicitud, int TipoResultado)
        {
            return TBSolicitudesBll.ValidaDatosBll(Accion, Parametro, idSolicitud, TipoResultado);
        }
        public List<TBCATTipoCredito> ObtenerTBCatTipoCredito(int idcliente)
        {
            return TBCATTipoCreditoBll.ObtenerTBCATTipoCredito(idcliente);
        }

        //PERU
        //##### ##### ##### ##### #####
        public List<Combo> ObtenerCombo_Producto_PE()
        {
            return TBCATProductoBll_PE.ObtenerCATCombo();
        }

        public List<Combo> ObtenerCombo_TipoCredito_PE()
        {
            return TBCATTipoCreditoBll_PE.ObtenerCATCombo();
        }

        public List<Combo> ObtenerCombo_CanalVentas_PE()
        {
            return TBCATCanalVentasBll_PE.ObtenerCATCombo();
        }

        public List<Combo> ObtenerCombo_TipoEvaluacion_PE()
        {
            return TBCATTipoEvaluacionBll_PE.ObtenerCATCombo();
        }

        public List<Combo> ObtenerCombo_TipoZona_PE()
        {
            return TBCATTipoZonaBll_PE.ObtenerCATCombo();
        }

        public List<Combo> ObtenerCombo_Direccion_PE()
        {
            return TBCATDireccionBll_PE.ObtenerCATCombo();
        }

        public List<Combo> ObtenerCombo_SituacionLaboral_PE()
        {
            return TBCATSituacionLaboralBll_PE.ObtenerCATCombo();
        }

        public List<Combo> ObtenerCombo_TipoDocumento_PE()
        {
            return TBCATTipoDocumentoBll_PE.ObtenerCATCombo();
        }

        #region ServidorArchivos
        public ServidorArchivosVM ObtenerServidorArchivosPorId(int idServidorArchivos)
        {
            return ServidorArchivosBll.ObtenerServidorArchivosPorId(idServidorArchivos);
        }
        public List<ServidorArchivosVM> ObtenerServidoresArchivos()
        {
            return ServidorArchivosBll.ObtenerServidoresArchivos();
        }
        public Resultado<bool> InsertarServidorArchivos(ServidorArchivos archivoGestion)
        {
            return ServidorArchivosBll.InsertarServidorArchivos(archivoGestion);
        }
        public Resultado<bool> ActualizarServidorArchivos(ServidorArchivos archivoGestion)
        {
            return ServidorArchivosBll.ActualizarServidorArchivos(archivoGestion);
        }
        public Resultado<bool> EliminarServidorArchivos(int idServidorArchivos)
        {
            return ServidorArchivosBll.EliminarServidorArchivos(idServidorArchivos);
        }
        #endregion
        public ObtenerTiposCobroResponse ObtenerTiposCobro()
        {
            return TBTipoCobroBll.ObtenerTiposCobro();
        }

        public ObtenerOrigenClienteEdicionResponse ObtenerOrigenClienteEdicion()
        {
            return TBCatCatalogosBll.ObtenerOrigenClienteEdicion();
        }

        public DataResultSolicitudes InsertarSolicitud_Peru(int Accion, int idUsuario, TBSolicitudes.SolicitudPeru entidad)
        {
            return TBSolicitudesBll.InsertarSolicitud_Peru(Accion, idUsuario, entidad);
        }        

        public List<Combo> ObtenerCombo_Empresa_PE()
        {
            return TBCATEmpresaBll_PE.ObtenerCATCombo();
        }

        public List<Combo> ObtenerCombo_OrganoPago_PE()
        {
            return TBCATOrganoPagoBll_PE.ObtenerCATCombo();
        }

        public ObtenerConfiguracionEmpresaResponse ObtenerConfiguracionEmpresa(ObtenerConfiguracionEmpresaRequest peticion)
        {
            return TBClientesBll.ObtenerConfiguracionEmpresa(peticion);
        }

        public List<CapturaPlanillaVirtualConfig> ObtenerConfiguracionCapturaPlanillaVirtual(int IdEmpresa)
        {
            return TBClientesBll.ObtenerConfiguracionCapturaPlanillaVirtual(IdEmpresa);
        }

        //##### ##### ##### ##### ##### PERU

        public string GuardaExpediente(TBSolicitudes.Expediente _Expediente)
        {
            return TBSolicitudesBll.GuardaExpediente(_Expediente);
        }

        public TBSolicitudes.ExpedienteB64 DescargaExpediente(TBSolicitudes.Expediente _Expediente)
        {
            return TBSolicitudesBll.DescargaExpediente(_Expediente);
        }

        public List<TBSolicitudes.Documentos> ObtenerDocumentos(int IDSolicitud)
        {
            return TBSolicitudesBll.ObtenerDocumentos(IDSolicitud);
        }

        public SolicitudReestructura ObtenerReestructura(SolicitudReestructuraCondiciones condiciones)
        {
            return TBSolicitudesBll.ObtenerReestructura(condiciones);
        }

        public int InsertarDireccionSolicitud(SolicitudDireccion direccion)
        {
            return TBSolicitudesBll.InsertarDireccionSolicitud(direccion);
        }

        public int ActualizarDireccionSolicitud(SolicitudDireccion direccion)
        {
            return TBSolicitudesBll.ActualizarDireccionSolicitud(direccion);
        }

        public List<SolicitudDireccion> ConsultarDireccionSolicitud(SolicitudDireccionCondiciones condiciones)
        {
            return TBSolicitudesBll.ConsultarDireccionSolicitud(condiciones);
        }

        public PoliticaAmpliacion PoliticaAmplicacionParaCliente(int idCliente)
        {
            return TBSolicitudesBll.PoliticaAmplicacionParaCliente(idCliente);
        }

        public Decimal ObtenerGAT()
        {
            return TBSolicitudesBll.ObtenerGAT();
        }

        public Resultado<bool> ActualizarDatosSolicitudGestiones(int idUsuario, TBSolicitudes.ActualizarSolicitudGestiones solicitud)
        {
            return TBSolicitudesBll.ActualizarDatosSolicitudGestiones(idUsuario, solicitud);
        }

        public ImpresionDocumentoResponse ImpresionTablaAmortizacion(ImpresionTablaAmortizacionRequest peticion)
        {
            return TBSolicitudesBll.ImpresionTablaAmortizacion(peticion);
        }

        public ImpresionDocumentoResponse ImpresionAmpliacionCredito(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionAmpliacionCredito(peticion);
        }

        public ImpresionDocumentoResponse ImpresionResumenCredito(ImpresionDocumentoRequest peticion)
        {
            return TBSolicitudesBll.ImpresionResumenCredito(peticion);
        }

        public ObtenerTipoGestorContactosResponse ObtenerTipoGestorContactos()
        {
            return TipoGestorContactoBLL.ObtenerTipoGestorContactos();
        }

        public ObtenerEstadoNotificacionResponse ObtenerEstadoNotificacion(ObtenerEstadoNotificacionRequest peticion)
        {
            return SolicitudNotificacionBLL.ObtenerNotificacion(peticion);
        }

        public ObtenerNotificacionesResponse ObtenerNotificaciones()
        {
            return SolicitudNotificacionBLL.ObtenerNotificaciones();
        }

        public ObtenerNombreArchivoPorClaveResponse ObtenerNombreArchivoPorClave(ObtenerNombreArchivoPorClaveRequest peticion)
        {
            return SolicitudNotificacionBLL.ObtenerNombreArchivoPorClave(peticion);
        }

        public ObtenerDestinosResponse ObtenerDestinos()
        {
            return SolicitudNotificacionBLL.ObtenerDestinos();
        }

        public DatosAvisoCobranzaResponse DatosAvisoCobranza(DatosAvisoCobranzaRequest peticion)
        {
            return SolicitudNotificacionBLL.DatosAvisoCobranza(peticion);
        }

        public DatosAvisoPagoResponse DatosAvisoPago(DatosAvisoPagoRequest peticion)
        {
            return SolicitudNotificacionBLL.DatosAvisoPago(peticion);
        }

        public DatosNotificacionPrejudicialResponse DatosNotificacionPrejudicial(DatosNotificacionPrejudicialRequest peticion)
        {
            return SolicitudNotificacionBLL.DatosNotificacionPrejudicial(peticion);
        }

        public DatosUltimaNotificacionPrejudicialResponse DatosUltimaNotificacionPrejudicial(DatosUltimaNotificacionPrejudicialRequest peticion)
        {
            return SolicitudNotificacionBLL.DatosUltimaNotificacionPrejudicial(peticion);
        }

        public DatosCitacionPrejudicialResponse DatosCitacionPrejudicial(DatosCitacionPrejudicialRequest peticion)
        {
            return SolicitudNotificacionBLL.DatosCitacionPrejudicial(peticion);
        }

        public DatosPromocionReduccionMoraResponse DatosPromocionReduccionMora(DatosPromocionReduccionMoraRequest peticion)
        {
            return SolicitudNotificacionBLL.DatosPromocionReduccionMora(peticion);
        }

        public ObtenerNotificacionesResponse ObtenerNotificacionPromociones()
        {
            return SolicitudNotificacionBLL.ObtenerNotificacionPromociones();
        }

        public ObtenerBitacoraNotificacionResponse ObtenerBitacoraNotificacion(ObtenerBitacoraNotificacionRequest peticion)
        {
            return SolicitudNotificacionBLL.ObtenerBitacoraNotificacion(peticion);
        }

        public ObtenerResultadoGestionNotificacionResponse ObtenerResultadoGestionNotificacion()
        {
            return SolicitudNotificacionBLL.ObtenerResultadoGestionNotificacion();
        }

        public List<TBCatOrigen> ObtenerTBCatOrigen(int Accion)
        {
            return TBCatOrigenBll.ObtenerTBCatOrigen(Accion);
        }

        public List<TB_CATParametro> ObtenerCATParametros()
        {
            return TB_CATParametroBll.ObtenerCATParametros();
        }

        public bool ActualizarCATParametros(int IdParametro, string Parametro, string Valor, string Descripcion, int IdEstatus)
        {
            return TB_CATParametroBll.ActualizarCATParametros(IdParametro, Parametro, Valor, Descripcion, IdEstatus);
        }
        public List<Template> GetAllTemplates()
        {
            return TemplateBll.GetAllTemplates();
        }

        public List<TB_EmailAccounts> GetAllAccounts()
        {
            return AccountBll.GetAllAccounts();
        }

        public List<TB_TemplateParamInputTypes> GetAllTemplateParamInputTypes()
        {
            return TemplateBll.GetAllTemplateParamInputTypes();
        }

        public bool CreateEmailSchedule(string nombre, string descripcion, string asunto, DateTime fechaProgramacion, bool emailUnico, bool archivosEnZip, int emailTemplateId, int emailAccountId, int usuarioId, List<string> To, List<string> CC, List<string> BCC, List<ReqTemplateParam> emailParams, List<string> attachments)
        {
            return EmailScheduleBll.CreateEmailSchedule(nombre, descripcion, asunto, fechaProgramacion, emailUnico, archivosEnZip, emailTemplateId, emailAccountId, usuarioId, To, CC, BCC, emailParams, attachments);
        }

        public bool CreateSmartEmailSchedule(string nombre, string descripcion, string asunto, DateTime horarioGeneracion, DateTime horarioEnvio, DateTime fechaUltimaGeneracion, bool archivosEnZip, int emailTemplateId, int emailAccountId, int smartType, int usuarioId, List<ReqTemplateParam> emailParams, List<string> attachments)
        {
            return SmartEmailScheduleBll.CreateSmartEmailSchedule(nombre, descripcion, asunto, horarioGeneracion, horarioEnvio, fechaUltimaGeneracion, archivosEnZip, emailTemplateId, emailAccountId, smartType, usuarioId, emailParams, attachments);
        }

        public List<TB_SmartTypes> GetAllSmartTypes()
        {
            return TemplateBll.GetAllSmartTypes();
        }

        public List<TBCATSubProducto> ObtenerSubProductos(TBCATSubProducto entidad)
        {
            return TBCATSubProductoBLL.ObtenerSubProductos(entidad);
        }

        public List<TBCATSubProducto> ObtenerSubProductosPorConvenio(int IdConvenio)
        {
            return TBCATSubProductoBLL.ObtenerSubProductosPorConvenio(IdConvenio);
        }

        public int UnsubscribeEmail(string email, string emailTemplate)
        {
            return EmailScheduleBll.UnsubscribeEmail(email, emailTemplate);
        }

        #region TipoArchivoGestion
        public TipoArchivoGestionVM ObtenerTipoArchivoGestionPorId(int idTipoArchivoGestion)
        {
            return TipoArchivoGestionBll.ObtenerTipoArchivoGestionPorId(idTipoArchivoGestion);
        }
        public TipoArchivoGestionVM ObtenerTipoArchivoGestionPorMime(string tipoMime)
        {
            return TipoArchivoGestionBll.ObtenerTipoArchivoGestionPorMime(tipoMime);
        }
        public List<TipoArchivoGestionVM> ObtenerTiposArchivoGestion()
        {
            return TipoArchivoGestionBll.ObtenerTiposArchivoGestion();
        }
        public Resultado<bool> InsertarTipoArchivoGestion(TipoArchivoGestion tipoArchivoGestion)
        {
            return TipoArchivoGestionBll.InsertarTipoArchivoGestion(tipoArchivoGestion);
        }
        public Resultado<bool> ActualizarTipoArchivoGestion(TipoArchivoGestion tipoArchivoGestion)
        {
            return TipoArchivoGestionBll.ActualizarTipoArchivoGestion(tipoArchivoGestion);
        }

        #endregion

        #region Notificaciones
        public List<NotificacionVM> ObtenerNotificacionesPorTipo(int idTipoNotificacion)
        {
            return NotificacionBll.ObtenerNotificacionesPorTipo(idTipoNotificacion);
        }
        public ObtenerDocumentosDestinoResponse ObtenerDocumentosDestino()
        {
            return DocumentoDestinoBll.ObtenerDocumentosDestino();
        }

        #endregion

        #region TipoNoficicacion
        public List<TipoNotificacionVM> ObtenerTiposNotificacion()
        {
            return TipoNotificacionBll.ObtenerTiposNotificacion();
        }

        #endregion

        #region ArchivoGestion
        public ArchivoGestionVM ObtenerArchivoGestionPorId(int idArchivoGestion)
        {
            return ArchivoGestionBll.ObtenerArchivoGestionPorId(idArchivoGestion);
        }
        public List<ArchivoGestionVM> ObtenerArchivoGestionPorSolicitud(int idSolicitud)
        {
            return ArchivoGestionBll.ObtenerArchivoGestionPorSolicitud(idSolicitud);
        }
        public Resultado<bool> InsertarArchivoGestion(ArchivoGestion archivoGestion)
        {
            return ArchivoGestionBll.InsertarArchivoGestion(archivoGestion);
        }
        public Resultado<bool> ActualizarArchivoGestion(ArchivoGestion archivoGestion)
        {
            return ArchivoGestionBll.ActualizarArchivoGestion(archivoGestion);
        }
        public Resultado<bool> EliminarArchivoGestion(int idArchivoGestion, int idSolicitud)
        {
            return ArchivoGestionBll.EliminarArchivoGestion(idArchivoGestion, idSolicitud);
        }
        public Resultado<bool> SubirArchivoGoogleDrive(int idSolicitud, int idUsuario, CargaArchivoGoogleDrive peticion)
        {
            return ArchivoGestionBll.SubirArchivoGoogleDrive(idSolicitud, idUsuario, peticion);
        }
        public Resultado<ArchivoGoogleDrive> DescargarArchivoGoogleDrive(string idArchivo)
        {
            return ArchivoGestionBll.DescargarArchivoGoogleDrive(idArchivo);
        }

        #endregion        


        #region "Ampliaciones App"

        public Resultado<TB_SolicitudAmpliacionExpress> AmpliacionesApp_ObtenerSolicitud(int IdSolicitudAmpliacionExpress)
        {
            return AmpliacionesAppBL.ObtenerSolicitudAmpliacion(IdSolicitudAmpliacionExpress);
        }

        public Resultado<List<TB_SolicitudAmpliacionExpress>> AmpliacionesApp_FiltrarSolicitudes(TB_SolicitudAmpliacionExpress entity)
        {
            return AmpliacionesAppBL.FiltrarSolicitudes(entity);
        }

        public void AmpliacionesApp_EliminarDocumento(int IdExpedienteAmpliacion, int IdUsuario, string Comentario)
        {
            AmpliacionesAppBL.EliminarDocumento(IdExpedienteAmpliacion, IdUsuario, Comentario);
        }

        public void AmpliacionesApp_AutorizarCredito(int IdSolicitudAmpliacion, int IdUsuario)
        {
            AmpliacionesAppBL.AutorizarCredito(IdSolicitudAmpliacion, IdUsuario);
        }

        public void AmpliacionesApp_DenegarCredito(int IdSolicitudAmpliacion, int IdUsuario, string Comentario)
        {
            AmpliacionesAppBL.DenegarCredito(IdSolicitudAmpliacion, IdUsuario, Comentario);
        }

        public List<TBCATEstatus> AmpliacionesApp_RevisionDocs_CboEstatus()
        {
            return AmpliacionesAppBL.RevisionDocs_CboEstatus();
        }

        #endregion

        #region "Cobranza - Layouts"
        public List<LayoutCobro.LayoutGenerado> LayoutGenerado_Filtrar(LayoutCobro.LayoutGenerado eFiltrar)
        {
            return LayoutCobroBL.LayoutGenerado_Filtrar(eFiltrar);
        }

        public LayoutCobro.LayoutGenerado LayoutGenerado_ObtenerDetalle(int Id)
        {
            return LayoutCobroBL.LayoutGenerado_ObtenerDetalle(Id);
        }

        #endregion

        #region "Cobranza - Plantillas"
        public List<Bucket> Plantilla_ListarBucketsLayout()
        {
            return BucketBL.ListarLayout();
        }

        public Resultado<List<int>> Plantilla_ObtenerDiasEjecucion(int IdPlantilla)
        {
            return PlantillaBL.ObtenerDiasEjecucion(IdPlantilla);
        }

        #endregion

        #region "Cobranza - Grupo Layouts"
        public Resultado<bool> GrupoLayout_Procesar(GrupoLayout.PeticionProcesar entity)
        {
            return GrupoLayoutBL.Procesar(entity);
        }
        #endregion
    }

}