﻿using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.Model;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Poco.dbo;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace wcfSOPF
{
    [ServiceContract]
    public partial interface ICobranzaAdministrativa
    {
        #region "Metodos Mexico"

        [OperationContract]
        int InsertarGrupoEnvio(GrupoEnvio entidad, int Accion);

        [OperationContract]
        string InsertarGrupoEnvioDetalle(int accion, GrupoEnvioDetalle entidad);

        List<GrupoEnvio> ObtenerGrupoEnvio(int Accion, int idGrupoenvio, string GrupoEnvio);

        #endregion

        #region "Metodos Peru Esquema COBRANZA"

        [OperationContract]
        List<CanalPago> CanalPago_ListarTodos();

        [OperationContract]
        List<CanalPago> CanalPago_ListarActivos();

        [OperationContract]
        List<CanalPago> CanalPago_ListarInactivos();

        [OperationContract]
        Resultado<bool> Cuenta_HardResetCobranza(int IdSolicitud);

        [OperationContract]
        List<BalanceCuenta> Balance_Filtrar(BalanceCuenta entity);

        [OperationContract]
        BalanceCuenta Balance_Obtener(BalanceCuenta entity);

        [OperationContract]
        List<BalanceCuenta.Recibos> Balance_Obtener_Recibos(BalanceCuenta entity);

        [OperationContract]
        List<Pagos.ChartCartera> Pagos_ChartCartera(int Anio, int MesInicio, int MesFin);

        [OperationContract]
        ResultadoDetallePagoMasivo ConsultarPagosMasivos(DetallePagoMasivoCondiciones condiciones);

        [OperationContract]
        List<ProcesoPagosAplicacion> ConsultarProcesosPagosAplicacion(ProcesoPagosAplicacionCondiciones condiciones);

        [OperationContract]
        List<ProcesoPagosAplicacionDetalle> ConsultarProcesoPagosAplicacionDetalle(ProcesoPagosAplicacionDetalleCondiciones condiciones);

        [OperationContract]
        int ProcesoPagosAplicacionAlta(ProcesoPagosAplicacion proceso);

        [OperationContract]
        Resultado<bool> Proceso_AplicarPago(AplicarPagoParametros parametros);

        [OperationContract]
        Resultado<bool> Proceso_DesaplicarPago(DesaplicarPagoParametros parametros);

        [OperationContract]
        List<Pago> Pagos_Filtrar(Pago entity);

        [OperationContract]
        Resultado<bool> Pagos_AplicarPago(Pago entity, bool AplicarUnico);

        [OperationContract]
        Resultado<bool> Pagos_DesaplicarPago(Pago entity);

        [OperationContract]
        Resultado<bool> Pagos_CancelarPago(Pago entity);

        [OperationContract]
        Resultado<bool> Pagos_DevolverPago(Pago entity);

        [OperationContract]
        Resultado<bool> Pagos_LiquidarPago(Pago entity, int IdUsuario);

        [OperationContract]
        Resultado<bool> Pagos_RegistrarPago(Pago entity, int IdUsuario);

        [OperationContract]
        Resultado<bool> Pagos_ActualizarCanalPago(Pago entity);

        [OperationContract]
        GeneraPagoReponse GenerarPago(GeneraPagoRequest pago);

        [OperationContract]
        GeneraPagoMavisoResponse GenerarPagoMasivo(GeneraPagoMavisoRequest pagos);

        [OperationContract]
        PagosMasivosResponse PagosMasivos(PagosMasivosRequest peticion);

        [OperationContract]
        PagosMasivosDetalleResponse PagosMasivosDetalle(PagosMasivosDetalleRequest peticion);

        [OperationContract]
        PagoMasivoEstatusEnviosResponse PagoMasivoEstatusEnvios(PagoMasivoEstatusEnviosRequest peticion);

        [OperationContract]
        DescargarPagosFallidosVisanetResponse DescargarPagosFallidosVisanet(int IdPagoMasivo);

        [OperationContract]
        List<PagosNoIdentificados> ObtenerPagosNoIdentificados(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta);

        [OperationContract]
        List<Devolucion> ObtenerDevoluciones(DevolucionCondiciones condiciones);

        [OperationContract]
        GeneraPagoMavisoResponse GenPagosNoIdentificadosMasivo(GeneraPagoMavisoRequest pagos);

        [OperationContract]
        GeneraDevolucionResponse GenDevolucionesMasivo(GeneraDevolucionRequest devoluciones);

        [OperationContract]
        Resultado<bool> GuardaPagoNoIdentificadoIndiv(PagosNoIdentificados pagos);

        [OperationContract]
        Resultado<bool> GuardaDevolucion(Devolucion devolucion);

        [OperationContract]
        Resultado<bool> AplicarDevolucionesPendientes();

        [OperationContract]
        Resultado<bool> ActuPagoNoIdentificadoIndiv(PagosNoIdentificados pagos);

        [OperationContract]
        Resultado<bool> Devoluciones_EliminarDevolucion(Devolucion entity, int IdUsuario);

        [OperationContract]
        VerificacionRUCResponse VerificarRUC(VerificacionRUCRequest verificar);

        [OperationContract]
        VerificacionRUCResponse GuardarRUC(VerificacionRUCRequest guardar);

        #endregion

        #region "Metodos Kushki"

        [OperationContract]
        PagosMasivosKushkiResponse PagosMasivosKushki(PagosMasivosKushkiRequest peticion);

        [OperationContract]
        PagosMasivosDetalleKushkiResponse PagosMasivosDetalleKushki(PagosMasivosDetalleKushkiRequest peticion);

        [OperationContract]
        string GenerarSuscripcionMasivoKushki(GeneraPagoMavisokushkiRequest pago);

        [OperationContract]
        string GenerarPagoMasivoKushki(GeneraPagoMavisokushkiRequest pago); 

        [OperationContract]
        PagoKushkiResponse GenerarPagoKushki(GenerarPagoKushkiRequest pago);

        [OperationContract]
        PagoKushkiResponse GenerarPagoConErrKushki(GenerarPagoKushkiRequest pago);

        [OperationContract]
        PagoKushkiResponse GenerarSuscripcionKushki(GenerarPagoKushkiRequest pago);

        [OperationContract]
        PagoKushkiResponse GenerarSuscripcionConErrKushki(GenerarPagoKushkiRequest pago);

        [OperationContract]
        PagoKushkiResponse GuardarPagoMasivoSuscripcion(GeneraPagoMavisokushkiRequest peticion);

        [OperationContract]
        PagoKushkiResponse GuardarPagoMasivoPagar(GeneraPagoMavisokushkiRequest peticion);

        [OperationContract]
        PagoKushkiResponse TerminoProcesoPago(int IdPagoMasivo);

        [OperationContract]
        PagoKushkiResponse TerminoProcesoSuscripcion(int IdPagoMasivo);

        [OperationContract]
        PagosMasivosKushkiResponse SuscripcionMasivosKushki(PagosMasivosKushkiRequest peticion);

        [OperationContract]
        PagosMasivosDetalleKushkiResponse SuscripcionMasivosDetalleKushki(PagosMasivosDetalleKushkiRequest peticion);

        [OperationContract]
        string RecuperarIdSuscripcionKushki(string tarjeta);
        
        #endregion

        #region "Layouts"

        [OperationContract]
        List<TipoLayoutCobro> TipoLayoutCobro_ObtenerTodos();

        [OperationContract]
        List<TipoLayoutCobro> TipoLayoutCobro_ObtenerActivos();

        [OperationContract]
        List<LayoutCobro.LayoutGenerado> LayoutGenerado_Filtrar(LayoutCobro.LayoutGenerado eFiltrar);

        [OperationContract]
        LayoutCobro.LayoutGenerado LayoutGenerado_ObtenerDetalle(int Id);

        [OperationContract]
        List<LayoutCobro.LayoutSumatoria> LayoutCobro_Suma(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago);

        [OperationContract]
        List<LayoutCobro.LayoutNetCash> LayoutCobro_ObtenerNetCash(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago);

        [OperationContract]
        List<LayoutCobro.LayoutVisaNet> LayoutCobro_ObtenerVisaNet(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago);

        [OperationContract]
        List<LayoutCobro.LayoutInterbank> LayoutCobro_ObtenerInterbank(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago);

        #endregion

        #region "Plantillas"

        [OperationContract]
        Resultado<Plantilla> Plantilla_Obtener(int IdPlantilla);

        [OperationContract]
        Resultado<List<Plantilla>> Plantilla_Buscar(Plantilla entity);

        [OperationContract]
        Resultado<Plantilla> Plantilla_Guardar(Plantilla entity);

        [OperationContract]
        List<Bucket> Plantilla_ListarBuckets();

        [OperationContract]
        Resultado<List<int>> Plantilla_ObtenerDiasEjecucion(int IdPlantilla);

        [OperationContract]
        List<Bucket> Plantilla_ListarBucketsLayout();

        [OperationContract]
        GenerarPlantillaEnvioCobroResponse GenerarPlantillaEnvioCobro(GenerarPlantillaEnvioCobroRequest peticion);

        [OperationContract]
        DescargaArchivoDomiciliacionResponse Plantilla_DescargaArchivoDomiciliacion(GenerarArchivoDomiciliacionRequest peticion);

        #endregion

        #region "Grupos Layout"

        [OperationContract]
        Resultado<GrupoLayout> GrupoLayout_Obtener(int IdGrupoLayout);

        [OperationContract]
        Resultado<List<GrupoLayout>> GrupoLayout_Buscar(GrupoLayout entity);

        [OperationContract]
        Resultado<GrupoLayout> GrupoLayout_Guardar(GrupoLayout entity);

        [OperationContract]
        Resultado<bool> GrupoLayout_Procesar(GrupoLayout.PeticionProcesar entity);

        #endregion

        #region Programaciones
        #region GET
        [OperationContract]
        Resultado<GrupoLayout.ProgramacionGrupoLayout> GrupoLayoutProgramacion_Obtener(int IdProgramacion);

        [OperationContract]
        List<TipoProgramacion> TipoProgramacion_ObtenerActivos();

        [OperationContract]
        List<ProgramacionIntervalo> Intervalos_ObtenerActivos();

        [OperationContract]
        TB_CATParametro ObtenerMaximoCobrosProgramaciones(string Clave);

        [OperationContract]
        TipoProgramacionesResponse TipoProgramaciones(TipoProgramacionesRequest peticion);

        [OperationContract]
        ProgramacionesProximasResponse ProgramacionesProximas(ProgramacionesProximasRequest peticion);

        [OperationContract]
        ProgramacionEstatusResponse ProgramacionEstatus(ProgramacionEstatusRequest peticion);
        #endregion

        #region POST
        [OperationContract]
        Respuesta ValidaVigencias(int idUsuario);

        [OperationContract]
        ActualizarEstatusProgramacionResponse ActualizarEstatusProgramacion(ActualizarEstatusProgramacionRequest peticion);

        [OperationContract]
        RegistrarProgramacionDetalleProResponse RegistrarProgramacionDetallePro(RegistrarProgramacionDetalleProRequest peticion);

        [OperationContract]
        Respuesta ActualizaProgramacionDetallePro(ActualizaProgramacionDetalleProRequest peticion);

        [OperationContract]
        Resultado<GrupoLayout.ProgramacionGrupoLayout> Programacion_Guardar(GrupoLayout.ProgramacionGrupoLayout entity);

        [OperationContract]
        ProgramacionGeneraMandoCobroResponse ProgramacionGeneraMandoCobro(ProgramacionGeneraMandoCobroRequest peticion);

        [OperationContract]
        ProgramacionGeneraMandoCobroResponse ProgramacionReintentarMandoCobro(ProgramacionReintentarMandoCobroRequest peticion);
        #endregion
        #endregion

        #region Domiciliacion
        #region GET
        [OperationContract]
        CuentasExcluidasResponse Domiciliacion_CuentasExcluidas(CuentasExcluidasRequest peticion);

        [OperationContract]
        MotivosExclusionResponse Domiciliacion_MotivosExclusion(MotivosExclusionRequest peticion);

        [OperationContract]
        BancosDomiciliacionCuentaResponse BancosDomiciliacionCuenta();
        #endregion

        #region POST
        [OperationContract]
        ExcluirCuentasResponse Domiciliacion_ExcluirCuentas(ExcluirCuentasRequest peticion);
        #endregion
        #endregion
    }
}
