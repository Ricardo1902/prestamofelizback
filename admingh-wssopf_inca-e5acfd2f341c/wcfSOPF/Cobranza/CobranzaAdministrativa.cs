﻿using SOPF.Core.BusinessLogic.Cobranza;
using SOPF.Core.BusinessLogic.CobranzaAdministrativa;
using SOPF.Core.BusinessLogic.Creditos;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.Model;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Poco.dbo;
using SOPF.Core.Poco.Catalogo;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace wcfSOPF
{

    public partial class SOPF : ICobranzaAdministrativa
    {
        #region Metodos Mexico
        public int InsertarGrupoEnvio(GrupoEnvio entidad, int Accion)
        {
            return GrupoEnvioBll.InsertarGrupoEnvio(entidad, Accion);
        }

        public string InsertarGrupoEnvioDetalle(int accion, GrupoEnvioDetalle entidad)
        {
            return GrupoEnvioDetalleBll.InsertarGrupoEnvioDetalle(accion, entidad);
        }

        public List<GrupoEnvio> ObtenerGrupoEnvio(int Accion, int idGrupoenvio, string GrupoEnvio)
        {
            return GrupoEnvioBll.ObtenerGrupoEnvio(Accion, idGrupoenvio, GrupoEnvio);
        }

        #endregion

        #region "Metodos Peru Esquema COBRANZA"
        public List<CanalPago> CanalPago_ListarTodos()
        {
            return CanalPagoBL.ListarTodos();
        }

        public List<CanalPago> CanalPago_ListarActivos()
        {
            return CanalPagoBL.ListarActivos();
        }

        public List<CanalPago> CanalPago_ListarInactivos()
        {
            return CanalPagoBL.ListarInactivos();
        }

        public Resultado<bool> Cuenta_HardResetCobranza(int IdSolicitud)
        {
            return PagosBL.HardResetCobranzaCuenta(IdSolicitud);
        }

        public List<BalanceCuenta> Balance_Filtrar(BalanceCuenta entity)
        {
            return PagosBL.Balance_Filtrar(entity);
        }

        public BalanceCuenta Balance_Obtener(BalanceCuenta entity)
        {
            return PagosBL.Balance_Obtener(entity);
        }

        public List<BalanceCuenta.Recibos> Balance_Obtener_Recibos(BalanceCuenta entity)
        {
            return PagosBL.Balance_Obtener_Recibos(entity);
        }

        public List<Pagos.ChartCartera> Pagos_ChartCartera(int Anio, int MesInicio, int MesFin)
        {
            return PagosBL.ChartCartera(Anio, MesInicio, MesFin);
        }

        public ResultadoDetallePagoMasivo ConsultarPagosMasivos(DetallePagoMasivoCondiciones condiciones)
        {
            return PagosBL.ConsultarPagosMasivos(condiciones);
        }

        public List<ProcesoPagosAplicacion> ConsultarProcesosPagosAplicacion(ProcesoPagosAplicacionCondiciones condiciones)
        {
            return PagosBL.ConsultarProcesosPagosAplicacion(condiciones);
        }

        public List<ProcesoPagosAplicacionDetalle> ConsultarProcesoPagosAplicacionDetalle(ProcesoPagosAplicacionDetalleCondiciones condiciones)
        {
            return PagosBL.ConsultarProcesoPagosAplicacionDetalle(condiciones);
        }

        public int ProcesoPagosAplicacionAlta(ProcesoPagosAplicacion proceso)
        {
            return PagosBL.ProcesoPagosAplicacionAlta(proceso);
        }

        public Resultado<bool> Proceso_AplicarPago(AplicarPagoParametros parametros)
        {
            return PagosBL.Proceso_AplicarPago(parametros);
        }

        public Resultado<bool> Proceso_DesaplicarPago(DesaplicarPagoParametros parametros)
        {
            return PagosBL.Proceso_DesaplicarPago(parametros);
        }

        public List<Pago> Pagos_Filtrar(Pago entity)
        {
            return PagosBL.Filtrar(entity);
        }

        public Resultado<bool> Pagos_AplicarPago(Pago entity, bool AplicarUnico)
        {
            return PagosBL.AplicarPago(entity, AplicarUnico);
        }

        public Resultado<bool> Pagos_DesaplicarPago(Pago entity)
        {
            return PagosBL.DesaplicarPago(entity);
        }

        public Resultado<bool> Pagos_CancelarPago(Pago entity)
        {
            return PagosBL.CancelarPago(entity);
        }

        public Resultado<bool> Pagos_DevolverPago(Pago entity)
        {
            return PagosBL.DevolverPago(entity);
        }

        public Resultado<bool> Pagos_LiquidarPago(Pago entity, int IdUsuario)
        {
            return PagosBL.LiquidarPago(entity, IdUsuario);
        }

        public Resultado<bool> Pagos_RegistrarPago(Pago entity, int IdUsuario)
        {
            return PagosBL.RegistrarPago(entity, IdUsuario);
        }

        public Resultado<bool> Pagos_ActualizarCanalPago(Pago entity)
        {
            return PagosBL.ActualizarCanalPago(entity);
        }

        public GeneraPagoReponse GenerarPago(GeneraPagoRequest pago)
        {
            return VisanetBLL.GenerarPago(pago);
        }

        public GeneraPagoMavisoResponse GenerarPagoMasivo(GeneraPagoMavisoRequest pagos)
        {
            return VisanetBLL.GenerarPagoMasivo(pagos);
        }

        public PagosMasivosResponse PagosMasivos(PagosMasivosRequest peticion)
        {
            return VisanetBLL.PagosMasivos(peticion);
        }

        public PagosMasivosDetalleResponse PagosMasivosDetalle(PagosMasivosDetalleRequest peticion)
        {
            return VisanetBLL.PagosMasivosDetalle(peticion);
        }

        public PagoMasivoEstatusEnviosResponse PagoMasivoEstatusEnvios(PagoMasivoEstatusEnviosRequest peticion)
        {
            return VisanetBLL.PagoMasivoEstatusEnvios(peticion);
        }

        public DescargarPagosFallidosVisanetResponse DescargarPagosFallidosVisanet(int IdPagoMasivo)
        {
            return DescargarPagosFallidosVisanetBLL.DescargarPagosFallidosVisanet(IdPagoMasivo);
        }

        public List<PagosNoIdentificados> ObtenerPagosNoIdentificados(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta)
        {
            return TBCobranzaBll.ObtenerPagosNoIdentificados(fchCreditoDesde, fchCreditoHasta);
        }

        public List<Devolucion> ObtenerDevoluciones(DevolucionCondiciones condiciones)
        {
            return TBCobranzaBll.ObtenerDevoluciones(condiciones);
        }

        public GeneraPagoMavisoResponse GenPagosNoIdentificadosMasivo(GeneraPagoMavisoRequest pagos)
        {
            return PagosNoIdentificadosBLL.GenPagosNoIdentificadosMasivo(pagos);
        }

        public GeneraDevolucionResponse GenDevolucionesMasivo(GeneraDevolucionRequest devoluciones)
        {
            return DevolucionesBLL.GenDevolucionesMasivo(devoluciones);
        }

        public Resultado<bool> GuardaPagoNoIdentificadoIndiv(PagosNoIdentificados pagos)
        {
            return TBCobranzaBll.GuardaPagoNoIdentificadoIndiv(pagos);
        }

        public Resultado<bool> GuardaDevolucion(Devolucion devolucion)
        {
            return TBCobranzaBll.GuardaDevolucion(devolucion);
        }

        public Resultado<bool> AplicarDevolucionesPendientes()
        {
            return TBCobranzaBll.AplicarDevolucionesPendientes();
        }

        public Resultado<bool> ActuPagoNoIdentificadoIndiv(PagosNoIdentificados pagos)
        {
            return TBCobranzaBll.ActuPagoNoIdentificadoIndiv(pagos);
        }

        public Resultado<bool> Devoluciones_EliminarDevolucion(Devolucion entity, int IdUsuario)
        {
            return DevolucionesBLL.EliminarDevolucion(entity, IdUsuario);
        }

        public PagosMasivosKushkiResponse PagosMasivosKushki(PagosMasivosKushkiRequest peticion)
        {
            return KushkiBLL.PagosMasivosKushki(peticion);
        }

        public PagosMasivosDetalleKushkiResponse PagosMasivosDetalleKushki(PagosMasivosDetalleKushkiRequest peticion)
        {
            return KushkiBLL.PagosMasivosDetalleKushki(peticion);
        }
        

        public string GenerarSuscripcionMasivoKushki(GeneraPagoMavisokushkiRequest pago)
        { 
            return KushkiBLL.GenerarSuscripcionMasivoKushki(pago);
        }

        public string GenerarPagoMasivoKushki(GeneraPagoMavisokushkiRequest pago)
        {
            return KushkiBLL.GenerarPagoMasivoKushki(pago);
        }

       public PagoKushkiResponse GenerarPagoKushki(GenerarPagoKushkiRequest pago)
        {
            return KushkiBLL.GenerarPagoKushki(pago);
        }

        public PagoKushkiResponse GenerarPagoConErrKushki(GenerarPagoKushkiRequest pago)
        {
            return KushkiBLL.GenerarPagoConErrKushki(pago);
        }

        public PagoKushkiResponse GenerarSuscripcionKushki(GenerarPagoKushkiRequest pago)
        {
            return KushkiBLL.GenerarSuscripcionKushki(pago);
        }

        public PagoKushkiResponse GenerarSuscripcionConErrKushki(GenerarPagoKushkiRequest pago)
        {
            return KushkiBLL.GenerarSuscripcionConErrKushki(pago);
        }

        public  PagoKushkiResponse GuardarPagoMasivoSuscripcion(GeneraPagoMavisokushkiRequest peticion)
        {
            return KushkiBLL.GuardarPagoMasivoSuscripcion(peticion, peticion.mensaje, 0);
        }

        public PagoKushkiResponse GuardarPagoMasivoPagar(GeneraPagoMavisokushkiRequest peticion)
        {
            return KushkiBLL.GuardarPagoMasivoPagar(peticion, peticion.mensaje, 0);
        }

        public PagoKushkiResponse TerminoProcesoPago(int IdPagoMasivo)
        {
            return KushkiBLL.TerminoProcesoPago(IdPagoMasivo);
        }

        public PagoKushkiResponse TerminoProcesoSuscripcion(int IdPagoMasivo)
        {
            return KushkiBLL.TerminoProcesoSuscripcion(IdPagoMasivo);
        }

        public PagosMasivosKushkiResponse SuscripcionMasivosKushki(PagosMasivosKushkiRequest peticion)
        {
            return KushkiBLL.SuscripcionMasivosKushki(peticion);
        }

        public PagosMasivosDetalleKushkiResponse SuscripcionMasivosDetalleKushki(PagosMasivosDetalleKushkiRequest peticion)
        {
            return KushkiBLL.SuscripcionMasivosDetalleKushki(peticion);
        }

        public string RecuperarIdSuscripcionKushki(string tarjeta)
        {
            return KushkiBLL.RecuperarIdSuscripcionKushki(tarjeta);
        }
        public VerificacionRUCResponse VerificarRUC(VerificacionRUCRequest verificar)
        {
            return VerificacionRUCBL.VerificarRUC(verificar);
        }

        public VerificacionRUCResponse GuardarRUC(VerificacionRUCRequest verificar)
        {
            return VerificacionRUCBL.GuardarRUC(verificar);
        }
        #endregion

        #region Layouts

        public List<TipoLayoutCobro> TipoLayoutCobro_ObtenerTodos()
        {
            return LayoutCobroBL.TipoLayoutCobro_ListarTodos();
        }

        public List<TipoLayoutCobro> TipoLayoutCobro_ObtenerActivos()
        {
            return LayoutCobroBL.TipoLayoutCobro_ListarActivos();
        }

        public List<LayoutCobro.LayoutSumatoria> LayoutCobro_Suma(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago)
        {
            return LayoutCobroBL.LayoutCobro_Suma(fchCreditoDesde, fchCreditoHasta, ddlCanalPago);
        }

        public List<LayoutCobro.LayoutNetCash> LayoutCobro_ObtenerNetCash(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago)
        {
            return LayoutCobroBL.Obtener_LayoutCobroCobro_NetCash(fchCreditoDesde, fchCreditoHasta, ddlCanalPago);
        }

        public List<LayoutCobro.LayoutVisaNet> LayoutCobro_ObtenerVisaNet(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago)
        {
            return LayoutCobroBL.Obtener_LayoutCobro_VisaNet(fchCreditoDesde, fchCreditoHasta, ddlCanalPago);
        }

        public List<LayoutCobro.LayoutInterbank> LayoutCobro_ObtenerInterbank(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago)
        {
            return LayoutCobroBL.Obtener_LayoutCobro_Interbank(fchCreditoDesde, fchCreditoHasta, ddlCanalPago);
        }

        #endregion

        #region Plantillas
        public Resultado<Plantilla> Plantilla_Obtener(int IdPlantilla)
        {
            return PlantillaBL.Obtener(IdPlantilla);
        }

        public Resultado<List<Plantilla>> Plantilla_Buscar(Plantilla entity)
        {
            return PlantillaBL.Buscar(entity);
        }

        public Resultado<Plantilla> Plantilla_Guardar(Plantilla entity)
        {
            return PlantillaBL.Guardar(entity);
        }

        public List<Bucket> Plantilla_ListarBuckets()
        {
            return BucketBL.Listar();
        }

        public GenerarPlantillaEnvioCobroResponse GenerarPlantillaEnvioCobro(GenerarPlantillaEnvioCobroRequest peticion)
        {
            return PlantillaBL.GenerarPlantillaEnvioCobro(peticion);
        }

        public DescargaArchivoDomiciliacionResponse Plantilla_DescargaArchivoDomiciliacion(GenerarArchivoDomiciliacionRequest peticion)
        {
            return PlantillaBL.DescargaArchivoDomiciliacion(peticion);
        }
        #endregion

        #region Grupo Layouts
        public Resultado<GrupoLayout> GrupoLayout_Obtener(int IdGrupoLayout)
        {
            return GrupoLayoutBL.Obtener(IdGrupoLayout);
        }

        public Resultado<List<GrupoLayout>> GrupoLayout_Buscar(GrupoLayout entity)
        {
            return GrupoLayoutBL.Buscar(entity);
        }

        public Resultado<GrupoLayout> GrupoLayout_Guardar(GrupoLayout entity)
        {
            return GrupoLayoutBL.Guardar(entity);
        }

        #endregion

        #region Programaciones
        #region GET
        public List<TipoProgramacion> TipoProgramacion_ObtenerActivos()
        {
            return ProgramacionBL.TipoProgramacion_ObtenerActivos();
        }

        public List<ProgramacionIntervalo> Intervalos_ObtenerActivos()
        {
            return ProgramacionBL.Intervalos_ObtenerActivos();
        }

        public TB_CATParametro ObtenerMaximoCobrosProgramaciones(string Clave)
        {
            return ProgramacionBL.ObtenerMaximoCobrosProgramaciones(Clave);
        }

        public Resultado<GrupoLayout.ProgramacionGrupoLayout> GrupoLayoutProgramacion_Obtener(int IdProgramacion)
        {
            return ProgramacionBL.Obtener(IdProgramacion);
        }

        public TipoProgramacionesResponse TipoProgramaciones(TipoProgramacionesRequest peticion)
        {
            return ProgramacionBL.TipoProgramaciones(peticion);
        }

        public ProgramacionesProximasResponse ProgramacionesProximas(ProgramacionesProximasRequest peticion)
        {
            return ProgramacionBL.ProgramacionesProximas(peticion);
        }

        public ProgramacionEstatusResponse ProgramacionEstatus(ProgramacionEstatusRequest peticion)
        {
            return ProgramacionBL.ProgramacionEstatus(peticion);
        }

        #endregion

        #region POST
        public Respuesta ValidaVigencias(int idUsuario)
        {
            return ProgramacionBL.ValidaVigencias(idUsuario);
        }

        public ActualizarEstatusProgramacionResponse ActualizarEstatusProgramacion(ActualizarEstatusProgramacionRequest peticion)
        {
            return ProgramacionBL.ActualizarEstatusProgramacion(peticion);
        }

        public RegistrarProgramacionDetalleProResponse RegistrarProgramacionDetallePro(RegistrarProgramacionDetalleProRequest peticion)
        {
            return ProgramacionBL.RegistrarProgramacionDetallePro(peticion);
        }

        public Respuesta ActualizaProgramacionDetallePro(ActualizaProgramacionDetalleProRequest peticion)
        {
            return ProgramacionBL.ActualizaProgramacionDetallePro(peticion);
        }

        public Resultado<GrupoLayout.ProgramacionGrupoLayout> Programacion_Guardar(GrupoLayout.ProgramacionGrupoLayout entity)
        {
            return ProgramacionBL.Guardar(entity);
        }

        public ProgramacionGeneraMandoCobroResponse ProgramacionGeneraMandoCobro(ProgramacionGeneraMandoCobroRequest peticion)
        {
            return ProgramacionBL.ProgramacionGeneraMandoCobro(peticion);
        }

        public ProgramacionGeneraMandoCobroResponse ProgramacionReintentarMandoCobro(ProgramacionReintentarMandoCobroRequest peticion)
        {
            return ProgramacionBL.ProgramacionReintentarMandoCobro(peticion);
        }
        #endregion
        #endregion

        #region Domiciliacion
        #region GET
        public CuentasExcluidasResponse Domiciliacion_CuentasExcluidas(CuentasExcluidasRequest peticion)
        {
            return DomiciliacionBLL.CuentasExcluidas(peticion);
        }

        public MotivosExclusionResponse Domiciliacion_MotivosExclusion(MotivosExclusionRequest peticion)
        {
            return DomiciliacionBLL.MotivosExclusion(peticion);
        }

        public BancosDomiciliacionCuentaResponse BancosDomiciliacionCuenta()
        {
            return DomiciliacionBLL.BancosDomiciliacionCuenta();
        }
        #endregion

        #region POST 
        public ExcluirCuentasResponse Domiciliacion_ExcluirCuentas(ExcluirCuentasRequest peticion)
        {
            return DomiciliacionBLL.ExcluirCuentas(peticion);
        }
        #endregion
        #endregion

      
    }
}
