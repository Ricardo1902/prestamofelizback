﻿using SOPF.Core.BusinessLogic.Sistema;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Adelantofeliz;
using SOPF.Core.Entities.AhorroVoluntario;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.Entities.Clientes;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.Entities.Hubble;
using SOPF.Core.Entities.Menu;
using SOPF.Core.Entities.Pulsus;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Entities.Tesoreria;
using SOPF.Core.Entities.Usuario;
using SOPF.Core.Model.Catalogo;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Catalogo;
using SOPF.Core.Model.Request.Creditos;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Request.Reportes;
using SOPF.Core.Model.Response;
using SOPF.Core.Model.Response.Catalogo;
using SOPF.Core.Model.Response.Creditos;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Poco.Catalogo;
using SOPF.Core.Poco.Clientes;
using SOPF.Core.Poco.Gestiones;
using SOPF.Core.Poco.Hubble;
using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;

namespace wcfSOPF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.

    [ServiceContract]
    public interface IUsuario
    {
        [OperationContract]
        void GuardarUsuario(TBCATUsuario Usuario);

        [OperationContract]
        string ValidarUsuario(string usuario, string contraseña, string IPAddress, string HostName, string MACAddress);

        [OperationContract]
        List<TBCATUsuario> ObtenerUsuario(int Accion, int IdUsuario, string Usuario);

        [OperationContract]
        List<TBCATUsuario> BuscarUsuario(int Accion, int IdUsuario, string Usuario, int idsucursal);

        [OperationContract]
        List<TBCATSucursal> ObtenerSucursal(TBCATSucursal entidad);

        [OperationContract]
        List<Combo> ObtenerCombo_UsuariosRegional();
        // TODO: Add your service operations here

        [OperationContract]
        bool EsAdministrador(int accion, int idUsuario);
    }

    [ServiceContract]
    public interface IMenu
    {
        [OperationContract]
        List<TBCATMenu> ObtenerMenu(int accion, int idTipoUsuario, int idMenu, int nivel, int idModulo, int tipo);
        // TODO: Add your service operations here
    }

    [ServiceContract]
    public interface IHubble
    {
        [OperationContract]
        List<Template> GetAllTemplates();

        [OperationContract]
        List<TB_EmailAccounts> GetAllAccounts();

        [OperationContract]
        List<TB_TemplateParamInputTypes> GetAllTemplateParamInputTypes();

        [OperationContract]
        bool CreateEmailSchedule(string nombre, string descripcion, string asunto, DateTime fechaProgramacion, bool emailUnico, bool archivosEnZip, int emailTemplateId, int emailAccountId, int usuarioId, List<string> To, List<string> CC, List<string> BCC, List<ReqTemplateParam> emailParams, List<string> attachments);

        [OperationContract]
        bool CreateSmartEmailSchedule(string nombre, string descripcion, string asunto, DateTime horarioGeneracion, DateTime horarioEnvio, DateTime fechaUltimaGeneracion, bool archivosEnZip, int emailTemplateId, int emailAccountId, int smartType, int usuarioId, List<ReqTemplateParam> emailParams, List<string> attachments);

        [OperationContract]
        List<TB_SmartTypes> GetAllSmartTypes();

        [OperationContract]
        int UnsubscribeEmail(string email, string emailTemplate);
    }

    [ServiceContract]
    public partial interface IGestion
    {
        [OperationContract]
        void GuardarGestor(int idTipo, int idUsuarioGes, int idUsuario);

        [OperationContract]
        void EliminarGestor(int idUsuarioGes, int idUsuario);

        [OperationContract]
        List<TBCatGestores> ObtenerGestor(int accion, int idGestor, int idTipo, int usuario);
        [OperationContract]
        List<TBCatTipoGestor> ObtenerTipoGestor();

        [OperationContract]
        void Asignar(TBAsignacion asignacion, int usuarioAsigna);

        [OperationContract]
        void Desasignar(TBAsignacion asignacion);

        [OperationContract]
        void AsignarMasivo(List<AsignarCuentasTMP> cuentas);

        [OperationContract]
        void DesasignarMasivo(List<AsignarCuentasTMP> cuentas);

        [OperationContract]
        List<DataResult.Usp_ObtenerAsignacion> ObtenerAsignacion(int idGestor, int accion, int idCuenta, string cliente, int idSucursal, string DNI, string convenio);

        [OperationContract]
        ObtenerAsignacionAccionResponse ObtenerAsignacionAccion(ObtenerAsignacionAccionRequest peticion);

        [OperationContract]
        DetalleCuenta ObtenerDetalleCuenta(int idCuenta);

        [OperationContract]
        List<Referencias> ObtenerReferencias(int idCuenta);
        [OperationContract]
        List<DataResult.Usp_ObtenerGestion> ObtenerGestion(int idCuenta);

        [OperationContract]
        List<DataResult.Usp_ObtenerGestion> ObtenerGestionAccion(int Accion, int idCuenta, int idGestor, DateTime FecIni, DateTime FecFin, int idgestor);

        [OperationContract]
        ObtenerGestionPordIdGestionResponse ObtenerGestionPordIdGestion(ObtenerGestionRequest peticion);

        [OperationContract]
        List<CatResultadoGestion> ObtenerCatalogoResultado();

        [OperationContract]
        List<CatResultadoGestion> ObtenerCatalogoResultadoAccion(int accion, string reultado);

        [OperationContract]
        int insertarGestion(Gestion gest);

        [OperationContract]
        InsertarGestionConResultadoResponse InsertarGestionConResultado(Gestion gestion);

        [OperationContract]
        void insertarPromesaPago(PromesaPago promesa);

        [OperationContract]
        List<PromesaPago> ObtenerPromesaPago(int idCuenta);

        [OperationContract]
        List<TBCreditos> ObtenerOtrasCuentas(int idCuenta);

        [OperationContract]
        string ActualizaGestion(Gestion entidad);


        [OperationContract]
        List<CatCampana> ObtenerCampanas(int Accion, int idCampana);


        [OperationContract]
        List<DataResult.RepGestiones> ReporteGestiones(int Accion, DateTime FecIni, DateTime FecFin, int idgestor);

        [OperationContract]
        void InsertarDevolverLlamada(int IdGestion, int IdCuenta, DateTime FchDevLlamada, int IdCliente, int IdGestor);

        [OperationContract]
        void InsertarAvisos(int IdCuenta, int Carteo, int sms, int wats, int mail, int circulo, int fisico);

        [OperationContract]
        Avisos ObtenerAvisos(int idCuenta);

        // DetalleCuenta ObtenerDetalleCuenta(int idCuenta);

        [OperationContract]
        List<TBDevolverLlamada> ObtenerTBDevolverLlamada(int idcuenta);

        [OperationContract]
        List<PromesaPago> ReportePromesaPago(int idcuenta, DateTime Fec_Inicial, DateTime Fec_Final, int Accion);
        [OperationContract]
        string SubirGestionesCampo(List<GestionesMovil> gestiones);

        [OperationContract]
        ObtenerTipoGestorContactosResponse ObtenerTipoGestorContactos();

        #region TipoArchivoGestion
        [OperationContract]
        TipoArchivoGestionVM ObtenerTipoArchivoGestionPorId(int idTipoArchivoGestion);
        [OperationContract]
        TipoArchivoGestionVM ObtenerTipoArchivoGestionPorMime(string tipoMime);
        [OperationContract]
        List<TipoArchivoGestionVM> ObtenerTiposArchivoGestion();
        [OperationContract]
        Resultado<bool> InsertarTipoArchivoGestion(TipoArchivoGestion tipoArchivoGestion);
        [OperationContract]
        Resultado<bool> ActualizarTipoArchivoGestion(TipoArchivoGestion tipoArchivoGestion);

        #endregion

        #region Notificaciones
        [OperationContract]
        List<NotificacionVM> ObtenerNotificacionesPorTipo(int idTipoNotificacion);
        [OperationContract]
        ObtenerDocumentosDestinoResponse ObtenerDocumentosDestino();

        #endregion

        #region TipoNoficicacion
        [OperationContract]
        List<TipoNotificacionVM> ObtenerTiposNotificacion();

        #endregion

        #region ArchivoGestion
        [OperationContract]
        ArchivoGestionVM ObtenerArchivoGestionPorId(int idArchivoGestion);
        [OperationContract]
        List<ArchivoGestionVM> ObtenerArchivoGestionPorSolicitud(int idSolicitud);
        [OperationContract]
        Resultado<bool> InsertarArchivoGestion(ArchivoGestion archivoGestion);
        [OperationContract]
        Resultado<bool> ActualizarArchivoGestion(ArchivoGestion archivoGestion);
        [OperationContract]
        Resultado<bool> EliminarArchivoGestion(int idArchivoGestion, int idSolicitud);
        [OperationContract]
        Resultado<bool> SubirArchivoGoogleDrive(int idSolicitud, int idUsuario, CargaArchivoGoogleDrive peticion);
        [OperationContract]
        Resultado<ArchivoGoogleDrive> DescargarArchivoGoogleDrive(string idArchivo);

        #endregion


        [OperationContract]
        ObtenerEstadoNotificacionResponse ObtenerEstadoNotificacion(ObtenerEstadoNotificacionRequest peticion);

        [OperationContract]
        ObtenerNotificacionesResponse ObtenerNotificaciones();

        [OperationContract]
        ObtenerNombreArchivoPorClaveResponse ObtenerNombreArchivoPorClave(ObtenerNombreArchivoPorClaveRequest peticion);

        [OperationContract]
        ObtenerDestinosResponse ObtenerDestinos();

        [OperationContract]
        DatosAvisoCobranzaResponse DatosAvisoCobranza(DatosAvisoCobranzaRequest peticion);

        [OperationContract]
        DatosAvisoPagoResponse DatosAvisoPago(DatosAvisoPagoRequest peticion);

        [OperationContract]
        DatosNotificacionPrejudicialResponse DatosNotificacionPrejudicial(DatosNotificacionPrejudicialRequest peticion);

        [OperationContract]
        DatosUltimaNotificacionPrejudicialResponse DatosUltimaNotificacionPrejudicial(DatosUltimaNotificacionPrejudicialRequest peticion);

        [OperationContract]
        DatosCitacionPrejudicialResponse DatosCitacionPrejudicial(DatosCitacionPrejudicialRequest peticion);

        [OperationContract]
        DatosPromocionReduccionMoraResponse DatosPromocionReduccionMora(DatosPromocionReduccionMoraRequest peticion);

        [OperationContract]
        ObtenerNotificacionesResponse ObtenerNotificacionPromociones();

        [OperationContract]
        ObtenerBitacoraNotificacionResponse ObtenerBitacoraNotificacion(ObtenerBitacoraNotificacionRequest peticion);

        [OperationContract]
        ObtenerResultadoGestionNotificacionResponse ObtenerResultadoGestionNotificacion();
    }

    [ServiceContract]
    public partial interface ISolicitud
    {
        [OperationContract]
        DataResultSolicitudes InsertarTBSolicitudes(int Accion, int idUsuario, TBSolicitudes entidad);

        [OperationContract]
        int UpdEstProceso(int Accion, int idSolicitud, int EstProceso, int idUsuario);

        [OperationContract]
        DataSet ObtenerTBSolicitudes(int accion, int cuenta, int idcliente, string cliente, string fchInicial, string fchFinal, int idsucursal, int idconvenio, int idlinea);

        [OperationContract]
        List<TBSolicitudes.DatosSolicitud> ObtenerDatosSolicitud(int idsolicitud);

        [OperationContract]
        int ActualizaComentariosSol(int IdSolicitud, int RolUsuario, int Accion);

        [OperationContract]
        int AltaComentariosSolicitud(int IdSolicitud, int IdComentarioPadre, string Comentarios, int UsuarioAlta, int RolUsuario);

        [OperationContract]
        DataSet ConComentariosSolicitud(int IdSolicitud, int IdComentarioPadre, int TipoConsulta);

        [OperationContract]
        int ValidaClabeBll(string clabe);

        [OperationContract]
        Resultado<List<TBSolicitudes.ListaNegra>> ValidaListaNegra(TBSolicitudes.ListaNegra entidad);

        [OperationContract]
        Resultado<bool> ValidaTarjetaVISADomiciliacion(string NumeroTarjeta);

        [OperationContract]
        CronogramaPagos Solicitudes_ObtenerCronograma(int IdSolicitud, DateTime? FechaDesembolso);

        [OperationContract]
        void InsertarDetSolicitud(int Accion, int idsolicitud, int secuencia, string descripcion, decimal cantidad, decimal p_unitario, decimal capital, decimal interes, decimal Iva, string Pedido, string FinancieraPedido, int IdUsuario);
        
        [OperationContract]
        Resultado<bool> ActualizaSolicitud(TBSolicitudes.ModificaSolicitud Solicitud, int IDUsuario_Modifica);

        [OperationContract]
        Resultado<bool> AsignaSolicitud(int IDSolicitud, int IDUsuario, int Validacion);
        [OperationContract]
        Resultado<bool> InsertarCuentaEmergente(TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente, int idUsuario);
        [OperationContract]
        Resultado<bool> ActualizarCuentaEmergente(TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente, int idUsuario);

        [OperationContract]
        List<Combo> ObtenerCATEst_Proceso();
        //##### ##### ##### ##### #####

        [OperationContract]
        DataResultSolicitudes ValidaDatosBll(int Accion, string Parametro, int idSolicitud, int TipoResultado);

        //PERU
        //##### ##### ##### ##### #####
        [OperationContract]
        DataResultSolicitudes InsertarSolicitud_Peru(int Accion, int idUsuario, TBSolicitudes.SolicitudPeru entidad);
        //##### ##### ##### ##### ##### PERU


        [OperationContract]
        String GuardaExpediente(TBSolicitudes.Expediente _Expediente);

        [OperationContract]
        TBSolicitudes.ExpedienteB64 DescargaExpediente(TBSolicitudes.Expediente _Expediente);

        [OperationContract]
        List<TBSolicitudes.Documentos> ObtenerDocumentos(int IDSolicitud);

        [OperationContract]
        SolicitudReestructura ObtenerReestructura(SolicitudReestructuraCondiciones condiciones);

        [OperationContract]
        int InsertarDireccionSolicitud(SolicitudDireccion direccion);

        [OperationContract]
        int ActualizarDireccionSolicitud(SolicitudDireccion direccion);

        [OperationContract]
        List<SolicitudDireccion> ConsultarDireccionSolicitud(SolicitudDireccionCondiciones condiciones);

        [OperationContract]
        PoliticaAmpliacion PoliticaAmplicacionParaCliente(int idCliente);

        [OperationContract]
        Decimal ObtenerGAT();
        [OperationContract]
        Resultado<bool> ActualizarDatosSolicitudGestiones(int idUsuario, TBSolicitudes.ActualizarSolicitudGestiones solicitud);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionAmpliacionCredito(ImpresionDocumentoRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionTablaAmortizacion(ImpresionTablaAmortizacionRequest peticion);

        [OperationContract]
        ImpresionDocumentoResponse ImpresionResumenCredito(ImpresionDocumentoRequest peticion);

        #region "Ampliaciones App"

        [OperationContract]
        Resultado<TB_SolicitudAmpliacionExpress> AmpliacionesApp_ObtenerSolicitud(int IdSolicitudAmpliacionExpress);

        [OperationContract]
        Resultado<List<TB_SolicitudAmpliacionExpress>> AmpliacionesApp_FiltrarSolicitudes(TB_SolicitudAmpliacionExpress entity);

        [OperationContract]
        void AmpliacionesApp_EliminarDocumento(int IdExpedienteAmpliacion, int IdUsuario, string Comentario);

        [OperationContract]
        void AmpliacionesApp_AutorizarCredito(int IdSolicitudAmpliacion, int IdUsuario);

        [OperationContract]
        void AmpliacionesApp_DenegarCredito(int IdSolicitudAmpliacion, int IdUsuario, string Comentario);

        [OperationContract]
        List<TBCATEstatus> AmpliacionesApp_RevisionDocs_CboEstatus();

        #endregion
    }
    
    [ServiceContract]
    public interface ICatalogo
    {

        [OperationContract]
        List<CatCNT> ObtenerCatCNT(int Accion, string Descripcion);


        [OperationContract]
        List<TBCATPromotor> ObtenerPromotores(int Accion, int Sucursal);

        [OperationContract]
        List<TBCATPromotor> ObtenerPromotoresPorCanalVenta(int Sucursal, int IdCanalVenta);

        [OperationContract]
        List<TBCATBanco> ObtenerBancos(int Accion, int idBanco);

        [OperationContract]
        List<TBCATTipoConvenio> ObtenerTipoConvenio(int Accion, int tipoconvenio, string convenio, int IdEstatus, int producto);

        [OperationContract]
        List<TBCATEstado> ObtenerTBCatEstado(int accion, int idEstado, string estado, int idEstatus);

        [OperationContract]
        List<TBCATCiudad> ObtenerTbCatCiudad(int accion, int idCiudad, string ciudad, int idEstatus, int IdEstado);
        [OperationContract]
        List<TBCATCiudad> ObtenerTbCatCiudadOrig(int accion, int IdEstado, int idCiudad);

        [OperationContract]
        List<TBCATColonia> ObtenerTbCatColonia(int accion, int ciudad);

        [OperationContract]
        List<TBCATColonia> ObtenerTbCatColoniaId(int accion, int ciudad, int idcolonia);
        [OperationContract]
        DetalleColoniaVM ObtenerDetalleColonia(int accion, int idColonia);

        [OperationContract]
        List<SIPais> ObtenerSIPais();

        [OperationContract]
        List<TipoPension> ObtenerTipoPension(int TipoConsulta);

        [OperationContract]
        List<TBCATTipoVade> ObtenerTBCATTipoVade(int Tipo);

        [OperationContract]
        List<TBCATGiro> ObtenerTBCATGiro(int Accion, int idGiro, string Giro, int idestatus);

        [OperationContract]
        DataSet ObtenerTBCATMedios(int Accion, int idMedio, string Medio, int idestatus);
        [OperationContract]
        List<TbCatFrecuencia> ObtenerTBCatFrecuencia(int usuarioSwitch, string frecs);

        [OperationContract]
        List<TBCATFormatos> ConsultaFormatos(int Accion, int idFormato, string Formato, int idestatus, int idsucursal);

        [OperationContract]
        List<TBCATEstatusLotes> ConsultaEstatusLotes(int Accion, int IdEstatusLote, string EstatusLote, int idestatus);

        // TODO: Add your service operations here
        [OperationContract]
        List<TBCATConvenio> ObtenerTBCATConvenio(int Accion, int idconvenio, string convenio, int idEstatus);
        [OperationContract]
        List<TBCATProducto> ObtenerTBCATProducto(int Accion, int idProducto, string Producto, int idEstatus, int TipoReno, int IdTipoCredito);

        [OperationContract]
        List<TBCATCampanias> ConsultaCampania(int Accion, int idCampania, string Campania, int idestatus);

        [OperationContract]
        List<TBCatCatalogos> ObtenerTBCatCatalogos(int Accion, int IdTipo, string valor, int idestatus, string Tipo);
        //ModificarSolicitud-LP
        //##### ##### ##### ##### #####
        [OperationContract]
        List<Combo> ObtenerCombo_TipoConvenio();

        [OperationContract]
        List<Combo> Sistema_ObtenerCombo_Convenio(int IDTipoConvenio);

        [OperationContract]
        List<Combo> ObtenerCombo_Convenio(int IDTipoConvenio);

        [OperationContract]
        List<Combo> ObtenerCombo_SucursalRegional(int IDUsuarioRegional);

        [OperationContract]
        List<Combo> ObtenerCombo_Promotores(int IDSucursal);

        [OperationContract]
        List<TBCATTipoCredito> ObtenerTBCatTipoCredito(int idcliente);
        //##### ##### ##### ##### ##### ModificarSolicitud

        //PERU
        //##### ##### ##### ##### #####
        [OperationContract]
        List<Combo> ObtenerCombo_Producto_PE();

        [OperationContract]
        List<Combo> ObtenerCombo_TipoCredito_PE();

        [OperationContract]
        List<Combo> ObtenerCombo_CanalVentas_PE();

        [OperationContract]
        List<Combo> ObtenerCombo_TipoEvaluacion_PE();

        [OperationContract]
        List<Combo> ObtenerCombo_TipoZona_PE();

        [OperationContract]
        List<Combo> ObtenerCombo_Direccion_PE();

        [OperationContract]
        List<Combo> ObtenerCombo_SituacionLaboral_PE();

        [OperationContract]
        List<Combo> ObtenerCombo_TipoDocumento_PE();

        [OperationContract]
        List<Combo> ObtenerCombo_Empresa_PE();

        [OperationContract]
        TB_CATEmpresa ObtenerEmpresa(int IdEmpresa);

        [OperationContract]
        List<Combo> ObtenerCombo_OrganoPago_PE();

        [OperationContract]
        ObtenerConfiguracionEmpresaResponse ObtenerConfiguracionEmpresa(ObtenerConfiguracionEmpresaRequest peticion);

        [OperationContract]
        List<CapturaPlanillaVirtualConfig> ObtenerConfiguracionCapturaPlanillaVirtual(int IdEmpresa);


        //##### ##### ##### ##### #####
        #region ServidorArchivos
        [OperationContract]
        ServidorArchivosVM ObtenerServidorArchivosPorId(int idServidorArchivos);
        [OperationContract]
        List<ServidorArchivosVM> ObtenerServidoresArchivos();
        [OperationContract]
        Resultado<bool> InsertarServidorArchivos(ServidorArchivos archivoGestion);
        [OperationContract]
        Resultado<bool> ActualizarServidorArchivos(ServidorArchivos archivoGestion);
        [OperationContract]
        Resultado<bool> EliminarServidorArchivos(int idServidorArchivos);
        #endregion

        [OperationContract]
        ObtenerTiposCobroResponse ObtenerTiposCobro();

        [OperationContract]
        ObtenerOrigenClienteEdicionResponse ObtenerOrigenClienteEdicion();

        [OperationContract]
        List<TBCatOrigen> ObtenerTBCatOrigen(int Accion);

        [OperationContract]
        List<TB_CATParametro> ObtenerCATParametros();

        [OperationContract]
        bool ActualizarCATParametros(int IdParametro, string Parametro, string Valor, string Descripcion, int IdEstatus);

        [OperationContract]
        List<TBCATSubProducto> ObtenerSubProductos(TBCATSubProducto entidad);

        [OperationContract]
        List<TBCATSubProducto> ObtenerSubProductosPorConvenio(int IdConvenio);
    }

    [ServiceContract]
    public interface ITesoreria
    {
        [OperationContract]
        List<TipoArchivo> TipoArchivo_Filtrar(TipoArchivo Entity);

        [OperationContract]
        List<ArchivoMasivo> DomiciliacionArchivo_ObtenerProcesados();

        [OperationContract]
        List<ArchivoMasivo> DomiciliacionArchivo_ObtenerRechazados();

        [OperationContract]
        List<ArchivoMasivo> DomiciliacionArchivo_ObtenerConError();

        [OperationContract]
        List<ArchivoMasivo> PagosDirectosArchivo_ObtenerProcesados();

        [OperationContract]
        List<ArchivoMasivo> PagosDirectosArchivo_ObtenerConError();

        [OperationContract]
        ArchivoMasivo DomiciliacionArchivo_ObtenerDetalle(int IdArchivo);

        [OperationContract]
        List<ArchivoMasivo> DomiciliacionArchivo_Filtrar(ArchivoMasivo Entity);

        [OperationContract]
        Resultado<bool> DomiciliacionArchivo_Registrar(ArchivoMasivo Entity);

        [OperationContract]
        ArchivoMasivo PagosDirectosArchivo_ObtenerDetalle(int IdArchivo);

        [OperationContract]
        List<ArchivoMasivo> PagosDirectosArchivo_Filtrar(ArchivoMasivo Entity);

        [OperationContract]
        Resultado<bool> PagosDirectosArchivo_Registrar(ArchivoMasivo Entity);

        [OperationContract]
        List<TipoArchivo.LayoutArchivo> PagosDirectosLayout_Obtener();

        [OperationContract]
        string GeneraArchivoAfiliacion(int idBanco);

        [OperationContract]
        string GeneraArchivoAfiliacionBancomer(int idBanco);

        [OperationContract]
        string GeneraArchivoAfiliacionRec(int idBanco);

        [OperationContract]
        string GeneraArchivoDispersion(int Accion, int idBanco, int idCorte);

        [OperationContract]
        void GeneraArchivoDispersionRecomienda(int Accion, int idBanco, int idCorte);

        [OperationContract]
        List<ArchivosDispersion> ObtenerArchivoDispersion(int Accion, int IdArchivo);

        [OperationContract]
        string ActArchivoDispersion(ArchivosDispersion Archivo);

        [OperationContract]
        string GeneraArchivoAfilDomiciliacion(int idBanco);

        [OperationContract]
        string GeneraArchivoDomiciliacionBancomer(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart, string idEmisora, string idconvenio, int AplicarGastos, int idPlantilla);
        [OperationContract]
        string GeneraArchivoDomiciliacionBancomerEspecifica(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart);

        [OperationContract]
        string GeneraArchivoDomiciliacionBanamex(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart, string idEmisora, string idconvenio, int AplicarGastos, int idPlantilla);

        [OperationContract]
        string GeneraArchivoDomiciliacionBanorte(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart, string IdEmisora, string idconvenio, int AplicarGastos, int idPlantilla);

        [OperationContract]
        string EnvioCobro();

        [OperationContract]
        void AltaArchivosDomiciliacion(string NombreArchivo, int IdBanco, int IdGrupo, int IdPlantilla, string Convenio, string Emisora, int DiasVenIni, int DiasVenFin, int AplicarGastos, int IdUsuario);

        [OperationContract]
        string GeneraArchivoDomiciliacionBancomerRemanente(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart, string idEmisora, string idConvenio, int AplicarGastos, int idPlantilla, int idGrupoEnvioOrigen);

        #region "Domiciliacion Net Cash BBVA"

        [OperationContract]
        List<ArchivoMasivo> ObtenerArchivos_SubidosBBVA();

        [OperationContract]
        List<ArchivoMasivo> ObtenerArchivos_GeneradosBBVA();

        [OperationContract]
        List<ArchivoMasivo> Filtrar_DomiBBVA(ArchivoMasivo Entity);

        [OperationContract]
        Resultado<bool> RespuestaDomiBBVA_Registrar(string nombresRespuestas, string nombreArchivo, int numArchivos);

        [OperationContract]
        List<EnvioDomiBBVA> DescragaTXTDomiBBVA(string nombreArchivo);

        [OperationContract]
        List<FitrarRespuestaBBVA> FiltrarRespuestaDomiBBVA(string nombreArchivo);

        [OperationContract]
        List<CAT_Parametros> ObtenerParametros(int IdParametro, string ClaveParametro);

        [OperationContract]
        Resultado<bool> EnvioDomiBBVA_Registrar(ArchivoMasivo Entity);

        [OperationContract]
        List<HistorialCobros> ObtenerHistorialSeguimientoDomiBBVA(int idArchivo);

        #endregion
    }

    [ServiceContract]
    public interface IAdelantoFeliz
    {
        [OperationContract]
        void InsetaEmpleado(Empleados emplado);
        [OperationContract]
        int ConsultarEmpresa(int idUsuario);
        [OperationContract]
        List<Empleados> ObtenerEmpleados(int Accion, int IdEmpresa, int NumEmpleado, string Nombre, string RFC, string CURP, int Id);
        [OperationContract]
        List<Empresa> ObtenerEmpreda(int Accion, int IdEmpresa);

    }

    [ServiceContract]
    public interface IAhorroVoluntario
    {
        [OperationContract]
        DataResultAhorro InsertarCredito(Credito Credito, Cliente Cliente, DateTime FechaPrimerPago);
        [OperationContract]
        List<Cliente> ObtenerCliente(int Accion, int idCliente, string Nombre);
        [OperationContract]
        string InsertaDesarrolladora(Desarrolladora entidad, int Accion);
        [OperationContract]
        List<Promotor> ObtenerPromotor(int Accion, int idDesarrolladora, int idpromotor, int idusuario);
        [OperationContract]
        List<DataResultAhorro.SelCreditos> ObtenerCreditosPendientes(int Accion, int idEstatus, int Credito, int idDesarrolladora);
        //[OperationContract]
        //List<DataResultAhorro.SelCheques> ObtenerCreditoCheques(int Accion, int idEstatus, int Credito, int idDesarrolladora);
        [OperationContract]
        Credito ObtenerCreditoAhorro(int idcuenta);
        [OperationContract]
        void ActualizarEstatusCredito(int idcuenta, int idestatus);
        //[OperationContract]
        //void InsertarCheque(int idcuenta, string cheque, decimal monto, int idestatus);

        [OperationContract]
        List<Recibo> ObtenerRecibosAhorro(int idcuenta);

        [OperationContract]
        List<DataResultAhorro.SelRecibos> ObtenerRecibosAhorroRep(int idcuenta);

        [OperationContract]
        List<CatTipoCredito> ObtenerCatTipoCredito(int Accion, int idTipo);
        [OperationContract]
        List<CatEstatusCancelacion> ObtenerCatEstatusCancelacion(int Accion, int TipoEstatus);
        [OperationContract]
        void InsertarReferencia(Referencia Entidad);

        [OperationContract]
        List<Referencia> ObtenerReferenciaAhorro(int accion, int idcuenta);

    }

    [ServiceContract]
    public interface IPulsus
    {
        [OperationContract]
        string GeneraArchivoAfiliacionPulsus(int idBanco, string EMail);
        [OperationContract]
        List<AfiliacionEmpleados> ObtenerCuentas(int accion);
        [OperationContract]
        void insertaEmpleados(List<AfiliacionEmpleados> Empleados);
        [OperationContract]
        void InsertarArchivoDispersion(ArchivoDispersion entidad);
        [OperationContract]
        void GeneraArchivoDispersionEmp(int idBanco, string EMail);
        [OperationContract]
        List<AnalistaNomina> ObtenerAnalistaNomina(int Accion, int idUsuario, int IdAnalista);
        [OperationContract]
        void InsertarRegistros(Registros entidad);

    }
}
