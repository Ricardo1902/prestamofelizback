﻿using System.ComponentModel;

namespace SOPF.Core.Solicitudes
{
    public enum DocumentoDigital
    {
        [Description("Ninguno")]
        Ninguno = 0,
        [Description("Solicitud.pdf")]
        Solicitud = 1,
        [Description("Contrato.pdf")]
        Contrato = 2,
        [Description("Pagare.pdf")]
        Pagare = 3,
        [Description("AcuerdoPagare.pdf")]
        AcuerdoLlenadoPagare = 4,
        [Description("HojaResumen.pdf")]
        HojaResumen = 5,
        [Description("Cronograma.pdf")]
        Cronograma = 6,
        [Description("AcuerdoDomiciliacion.pdf")]
        AcuerdoDomiciliacion = 8,
        [Description("FormatoVisa.pdf")]
        FormatoVisa = 16,
        [Description("FormularioAmpliacion.pdf")]
        FormularioAmpliacion = 19,
        [Description("Reestructura.pdf")]
        Reestructura = 23,
        [Description("ReestructuraExtrajudicial.pdf")]
        ReestructuraExtrajudicial = 24,
        [Description("ReestructuraExtrajudicialDev.pdf")]
        ReestructuraExtrajudicialDev = 25,
        [Description("CartaPoder.pdf")]
        CartaPoder = 27,
        [Description("DeclaracionJurada.pdf")]
        DeclaracionJurada = 28,
        [Description("EstadoCuenta.pdf")]
        EstadoCuenta = 29,
       [Description("SolicitudRPT.pdf")]
        SolicitudRPT = 30
    }

    public enum EstatusSolicitud
    {
        [Description("Activa")]
        Activa = 1,
        [Description("Cancelada por Cliente")]
        CanceladaCliente = 2,
        [Description("Declinada por Analista")]
        DeclinadaAnalista = 3,
        [Description("Pendiente")]
        Pendiente = 4,
        [Description("Operada")]
        Operada = 5,
        [Description("Condicionada")]
        Condicionada = 6,
        [Description("Declinada por Sistema")]
        DeclinadaSistema = 7
    }

    public enum EstatusProcesoDigital
    {
        [Description("Nuevo")]
        Nuevo = 1,
        [Description("En proceso")]
        EnProceso = 2,
        [Description("Aprobado")]
        Aprobado = 3,
        [Description("Rechazado")]
        Rechazado = 4
    }

    public enum ProcesoDigital
    {
        PruebaVida = 1,
        FirmaDocumentos = 2,
        Analisis = 3
    }

    public enum OrigenEnvioCronograma
    {
        Gestion,
        Creditos
    }

    public enum Proceso
    {
        Ninguno,
        [Description("Originacion")]
        Originacion,
        [Description("PeticionReestructura")]
        PeticionReestructura
    }

    public enum EstatusPago
    {
        [Description("Registrado")]
        Registrado = 1,
        [Description("Liquidación")]
        Liquidacion = 2
    }
}
