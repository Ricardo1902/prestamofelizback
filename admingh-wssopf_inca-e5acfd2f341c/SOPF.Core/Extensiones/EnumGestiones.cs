﻿using System.ComponentModel;

namespace SOPF.Core.Gestiones
{
    public enum TipoGestionDom
    {
        [Description("GenDomBanco")]
        Banco,
        [Description("GenDomVisanet")]
        Visanet,
        [Description("GenPagoRef")]
        PagoReferenciado
    }

    public enum EstatusGestionPeticionDom
    {
        Registrado = 1,
        Autorizado = 2,
        Cancelado = 3,
        ArchivoGenerado = 4
    }
}
