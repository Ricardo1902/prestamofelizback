﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace SOPF.Core.Lleida
{
    public enum EstatusFirma
    {
        Otro = 0,
        New = 1,
        Ready = 2,
        Signed = 3,
        Expired = 4,
        Failed = 5,
        Cancelled = 6,
        Otp_max_retries = 7,
        Severally_level_completed = 8,
        Evidence_generated = 9
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum EkycMediaType
    {
        [EnumMember(Value = "NONE")]
        None,
        [EnumMember(Value = "FACE")]
        Face,
        [EnumMember(Value = "ID_FRONT")]
        IdFront,
        [EnumMember(Value = "ID_BACK")]
        IdBack,
        [EnumMember(Value = "VIDEO")]
        Video,
        [EnumMember(Value = "CERTIFICATE")]
        Cetificate,
        [EnumMember(Value = "VALIDATION_INFO")]
        ValidationInfo,
        [EnumMember(Value = "VALIDATION_CHECK")]
        ValidatonCheck
    }
}