﻿using System.ComponentModel;

namespace SOPF.Core.APP
{
    public enum Buzon
    {
        [Description("QuieroAmpliacion")]        
        QuieroAmpliacion = 1,
        [Description("QuieroCancelacion")]
        QuieroCancelacion = 2,
        [Description("QuieroAmortizacion")]
        QuieroAmortizacion = 3
    }
}
