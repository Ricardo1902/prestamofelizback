﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace SOPF.Core
{
    public enum TipoNotificacion
    {
        Ninguno,
        [Description("CEDL")]
        EnvioDocumentos,
        [Description("CEPV")]
        PruebaVida,
        [Description("CESN")]
        SolicitudNotificacion,
        [Description("CEGDL")]
        DevolucionLlamada,
        [Description("CEGCP")]
        CompromisoPago,
        [Description("CEGPR")]
        PagoReferenciado,
        [Description("CEGCC")]
        CitaCliente,
        [Description("CEGCD")]
        CanceDeuda,
        [Description("CEGRC")]
        ReimpresionCronogramaGe,
        [Description("CECRC")]
        ReimpresionCronogramaCred,
        [Description("CEPVPR")]
        PruebaVidaPetRes,
        [Description("CEPRCD")]
        CargaDocPetReest
    }

    public enum MenuSistema
    {
        Ninguno,
        [Description("Site/Gestiones/PeticionReestructuraValidacion.aspx?")]
        Gestiones_ValidacionPeticionReestrucura
    }

    [DataContract]
    public enum MedioEnvioOTP
    {
        [EnumMember(Value = "Sms")]
        Sms,
        [EnumMember(Value = "Correo")]
        Correo,
        [EnumMember(Value = "Ambos")]
        Ambos
    }

    [DataContract]
    public enum TipoOTP
    {
        [Description("OTPR")]
        [EnumMember(Value = "OTPR")]
        OtpRegistro
    }
}
