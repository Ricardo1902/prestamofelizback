﻿using System.ComponentModel;

namespace SOPF.Core
{
    public enum Firmante
    {
        Cliente = 1,
        Conyuge = 2,
        Empresa = 3,
        Aval = 4
    }

    public enum BancoDomicilacion
    {
        [Description("Otro")]
        Otro = 0,
        [Description("Continental")]
        Continental = 1,
        [Description("Interbank")]
        Interbank = 2,
    }

    public enum UsuarioSistema
    {
        Administrador = 21
    }
}