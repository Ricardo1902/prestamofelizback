﻿using System;
using System.IO;

namespace SOPF.Core.Extensiones.Sistema
{
    public static class ExtensionesStream
    {
        public static byte[] ToByteArray(this Stream stream)
        {
            byte[] buffer = null;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                int count = 0;
                do
                {
                    byte[] tempBuffer = new byte[1024];
                    count = stream.Read(tempBuffer, 0, 1024);
                    memoryStream.Write(tempBuffer, 0, count);
                } while (stream.CanRead && count > 0);
                buffer = memoryStream.ToArray();
            }
            return buffer;
        }
    }
}
