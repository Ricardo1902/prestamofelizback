#pragma warning disable 150412
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Adelantofeliz;
using SOPF.Core.DataAccess.Adelantofeliz;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: EmpresaBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase EmpresaDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-12 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Adelantofeliz
{
    public static class EmpresaBll
    {
        private static EmpresaDal dal;

        static EmpresaBll()
        {
            dal = new EmpresaDal();
        }

        public static void InsertarEmpresa(Empresa entidad)
        {
            dal.InsertarEmpresa(entidad);			
        }
        
        public static List<Empresa> ObtenerEmpresa(int Accion, int IdEmpresa)
        {
            return dal.ObtenerEmpresa(Accion, IdEmpresa);
        }
        
        public static void ActualizarEmpresa()
        {
            dal.ActualizarEmpresa();
        }

        public static void EliminarEmpresa()
        {
            dal.EliminarEmpresa();
        }
    }
}