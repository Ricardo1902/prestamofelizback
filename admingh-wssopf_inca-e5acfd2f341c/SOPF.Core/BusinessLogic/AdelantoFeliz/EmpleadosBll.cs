#pragma warning disable 150408
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Adelantofeliz;
using SOPF.Core.DataAccess.Adelantofeliz;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: EmpleadosBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase EmpleadosDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Adelantofeliz
{
    public static class EmpleadosBll
    {
        private static EmpleadosDal dal;

        static EmpleadosBll()
        {
            dal = new EmpleadosDal();
        }

        public static void InsertarEmpleados(Empleados entidad)
        {
            dal.InsertarEmpleados(entidad);			
        }
        
        public static List<Empleados> ObtenerEmpleados(int Accion, int IdEmpresa, int NumEmpleado, string Nombre, string RFC, string CURP, int Id)
        {
            return dal.ObtenerEmpleados(Accion, IdEmpresa, NumEmpleado, Nombre, RFC, CURP, Id);
        }
        
        public static void ActualizarEmpleados()
        {
            dal.ActualizarEmpleados();
        }

        public static void EliminarEmpleados()
        {
            dal.EliminarEmpleados();
        }
        public static int ConsultarEmpresa(int idUsuario)
        {
            return dal.ConsultarEmpresa(idUsuario);
        }

    }
}