#pragma warning disable 141030
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using Newtonsoft.Json;
using SOPF.Core.BusinessLogic.Gestiones;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Clientes;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Clientes;
using SOPF.Core.Model.Clientes;
using SOPF.Core.Model.Request.Catalogo;
using SOPF.Core.Model.Request.Cliente;
using SOPF.Core.Model.Response.Catalogo;
using SOPF.Core.Model.Response.Cliente;
using SOPF.Core.Poco.Clientes;
using SOPF.Core.Poco.dbo;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;

#region Copyright
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TBClientesBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBClientesDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-10-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Clientes
{
    public static class TBClientesBll
    {
        private static TBClientesDal dal;

        static TBClientesBll()
        {
            dal = new TBClientesDal();
        }

        #region GET
        public static List<TBClientes> ObtenerTBClientes(int Accion, int IdCliente)
        {
            return dal.ObtenerTBClientes(Accion, IdCliente);
        }

        public static List<TBClientes.BusquedaClienteRecomienda> ObtenerTBClienteRecomendador(int Accion, int IdCliente)
        {
            return dal.ObtenerTBClienteRecomendador(Accion, IdCliente);
        }

        public static List<TBClientes.BusquedaCliente> BusquedaCliente(int Accion, int IdSolicitud, int idCliente, string Cliente, DateTime FechaInicial, DateTime FechaFinal, int idSucursal)
        {
            return dal.BusquedaCliente(Accion, IdSolicitud, idCliente, Cliente, FechaInicial, FechaFinal, idSucursal);
        }

        public static Resultado<int> ObtenerSolicitudActivaDeCliente(int idCliente)
        {
            return dal.ObtenerSolicitudActivaDeCliente(idCliente);
        }

        public static List<DireccionCliente> ObtenerDireccionesCliente(int idCliente)
        {
            return dal.ObtenerDireccionesCliente(idCliente);
        }

        public static ObtenerConfiguracionEmpresaResponse ObtenerConfiguracionEmpresa(ObtenerConfiguracionEmpresaRequest peticion)
        {
            return dal.ObtenerConfiguracionEmpresa(peticion);
        }

        public static List<CapturaPlanillaVirtualConfig> ObtenerConfiguracionCapturaPlanillaVirtual(int IdEmpresa)
        {
            return dal.ObtenerConfiguracionCapturaPlanillaVirtual(IdEmpresa);
        }

        public static BuscarClienteResponse BuscarCliente(BuscarClienteRequest peticion)
        {
            BuscarClienteResponse respuesta = new BuscarClienteResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idCliente = 0;
                    int idSolicitud = 0;

                    if (peticion.IdCliente > 0)
                    {
                        idCliente = peticion.IdCliente;
                        idSolicitud = (from s in con.dbo_TB_Solicitudes
                                       where s.IdCliente == idCliente
                                       select new { s.IdSolicitud, s.fch_Solicitud }
                                       )
                                       .OrderByDescending(o => o.fch_Solicitud)
                                       .Select(s => (int)s.IdSolicitud)
                                       .DefaultIfEmpty(0)
                                       .FirstOrDefault();
                    }
                    else if (peticion.IdSolicitud > 0)
                    {
                        idCliente = con.dbo_TB_Solicitudes
                            .Where(s => s.IdSolicitud == peticion.IdSolicitud)
                            .Select(s => (int)s.IdCliente)
                            .DefaultIfEmpty(0)
                            .FirstOrDefault();
                        idSolicitud = peticion.IdSolicitud;
                    }
                    else if (peticion.IdCuenta > 0)
                    {
                        var datosSol = (from c in con.dbo_TB_Creditos
                                        join s in con.dbo_TB_Solicitudes on c.IdSolicitud equals s.IdSolicitud
                                        where c.IdCuenta == peticion.IdCuenta
                                        select new { s.IdCliente, s.IdSolicitud })
                                     .FirstOrDefault();
                        if (datosSol != null)
                        {
                            idCliente = (int)datosSol.IdCliente;
                            idSolicitud = (int)datosSol.IdSolicitud;
                        }
                    }

                    if (idCliente > 0 || !string.IsNullOrEmpty(peticion.DNI))
                    {
                        IQueryable<TB_Clientes> filtro;
                        if (idCliente > 0)
                            filtro = con.dbo_TB_Clientes.Where(c => c.IdCliente == idCliente);
                        else
                            filtro = con.dbo_TB_Clientes.Where(c => c.NumeroDocumento == peticion.DNI);

                        respuesta.Cliente = filtro
                            .Select(c => new DatosClienteVM
                            {
                                Nombres = c.Nombres,
                                ApellidoPaterno = c.apellidop,
                                ApellidoMaterno = c.apellidom,
                                Dni = c.NumeroDocumento,
                                Email = c.Correo,
                                Celular = c.Celular
                            })
                            .FirstOrDefault();

                        if (respuesta.Cliente != null && idSolicitud > 0)
                        {
                            var datosDir = (from cd in con.Clientes_TB_ClienteDirecciones
                                            join col in con.dbo_TB_CATColonia on new { IdColonia = cd.distrito ?? 0 } equals new { IdColonia = (int)col.IdColonia } into cdCol
                                            from lCol in cdCol.DefaultIfEmpty()
                                            where cd.idCliente == idCliente
                                                && (idSolicitud == 0 || cd.idSolicitud == idSolicitud)
                                            select new
                                            {
                                                cd.nombreVia,
                                                cd.numeroExterno,
                                                cd.manzana,
                                                cd.lote,
                                                cd.nombreZona,
                                                Colonia = lCol == null ? "" : lCol.Colonia
                                            })
                                            .FirstOrDefault();
                            if (datosDir != null)
                            {
                                respuesta.Cliente.Direccion = $"{(!string.IsNullOrEmpty(datosDir.nombreVia) ? datosDir.nombreVia : $"MZ: {datosDir.manzana}")}" +
                                    $"{(!string.IsNullOrEmpty(datosDir.nombreVia) ? $" {datosDir.numeroExterno}" : $" LT: {datosDir.lote}")}";
                                respuesta.Cliente.Direccion.Trim();

                                respuesta.Cliente.Direccion +=
                                    $"{(!string.IsNullOrEmpty(datosDir.nombreZona) ? $", ZONA - {datosDir.nombreZona}" : "")}{(!string.IsNullOrEmpty(datosDir.Colonia) ? $", {datosDir.Colonia.Trim()}" : "")}";
                                respuesta.Cliente.Direccion.Trim();
                            }

                        }
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static string ObtenerCategoriaCliente(int idCliente)
        {
            return dal.ObtenerCategoriaCliente(idCliente);
        }
        #endregion

        #region POST
        public static Resultado<bool> InsertarTBClientes(TBClientes entidad)
        {
            return dal.InsertarTBClientes(entidad);
        }

        public static Resultado<bool> InsertarClienteRecomendador(int Accion, int IdCliente, int IdClienteRec, int IdClabeRec, int IdCampania, int IdUsuario)
        {
            return dal.InsertarClienteRecomendador(Accion, IdCliente, IdClienteRec, IdClabeRec, IdCampania, IdUsuario);
        }
        #endregion

        #region PUT
        public static void ActualizarTBClientes()
        {
            dal.ActualizarTBClientes();
        }

        public static Resultado<bool> ActualizarReferenciasCliente(int idCliente, int idUsuario, List<ClientesReferencia> referencias)
        {
            return dal.ActualizarReferenciasCliente(idCliente, idUsuario, referencias);
        }

        public static Resultado<bool> ActualizarClienteGestiones(TBClientes.ActualizaClienteGestiones cliente)
        {
            return dal.ActualizarClienteGestiones(cliente);
        }

        public static Resultado<bool> AgregarGestionActCliente(int IdSolicitud, int IdUsuario)
        {
            var respuesta = new Resultado<bool>();
            try
            {
                if (IdSolicitud > 0 && IdUsuario > 0)
                {
                    // Se busca el resultado de gestion correspondiente para alta de acuerdo de pago
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        var tipoGestionActCliente = con.Gestiones_TipoGestion.SingleOrDefault(tg => tg.Clave == "ActualizaDatos" && tg.Activo);
                        var resGestionActCliente = con.Gestiones_TB_CATResultadoGestion.SingleOrDefault(rg => rg.TipoGestion == tipoGestionActCliente.IdTipoGestion && rg.Descripcion == "Actualización enviada" && rg.Estatus.Value);
                        var cuenta = con.dbo_TB_Creditos.SingleOrDefault(c => c.IdSolicitud == IdSolicitud);
                        var gestor = con.Gestiones_TB_CatGestores.SingleOrDefault(g => g.Usuarioid == IdUsuario);
                        if (resGestionActCliente != null && resGestionActCliente.IdResultado > 0)
                        {
                            if (cuenta != null && cuenta.IdCuenta > 0 && gestor != null && gestor.IdGestor > 0)
                            {
                                var gestion = new Entities.Gestiones.Gestion
                                {
                                    IdCuenta = cuenta.IdCuenta,
                                    IdGestor = gestor.IdGestor,
                                    IdResultadoGestion = resGestionActCliente.IdResultado,
                                    IdTipoGestion = tipoGestionActCliente.IdTipoGestion
                                };
                                int idGestion = GestionBll.InsertarGestion(gestion);
                                if (idGestion > 0)
                                {
                                    respuesta.Codigo = 1;
                                    respuesta.Mensaje = idGestion.ToString();
                                    return respuesta;
                                }
                                else
                                {
                                    respuesta.Codigo = 0;
                                    respuesta.Mensaje = "No se pudo insertar la gestión.";
                                }
                            }
                        }
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Codigo = 0;
                respuesta.Mensaje = "No se pudo insertar la gestión. " + vex.InnerException.Message ?? "";
            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.Mensaje = "Ocurrió un error al momento de dar de alta la gestión.";
                respuesta.CodigoStr = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: " + IdUsuario.ToString() + IdSolicitud.ToString(), JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region DELETE
        public static void EliminarTBClientes()
        {
            dal.EliminarTBClientes();
        }
        #endregion
    }
}