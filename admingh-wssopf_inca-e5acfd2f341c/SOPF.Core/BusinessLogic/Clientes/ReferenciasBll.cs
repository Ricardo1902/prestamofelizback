﻿using System;
using System.Collections.Generic;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.Entities.Clientes;
using SOPF.Core.DataAccess.Gestiones;
using SOPF.Core.Poco.Clientes;
using SOPF.Core.DataAccess.Clientes;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess;

namespace SOPF.Core.BusinessLogic.Clientes
{
    public static class ReferenciasBll
    {
        private static DetalleCuentaDal dalDetalleCuenta;
        private static ReferenciaDal dalReferencia;

        static ReferenciasBll()
        {
            dalDetalleCuenta = new DetalleCuentaDal();
            dalReferencia = new ReferenciaDal(new CreditOrigContext());
        }

        public static List<Referencias> ObtenerReferencias(int idCuenta)
        {
            DetalleCuenta Detcuenta = new DetalleCuenta();
            Detcuenta = dalDetalleCuenta.ObtenerDetalleCuenta(idCuenta);

            List<Referencias> Ref = new List<Referencias>();

            Referencias referencia = new Referencias();

            referencia.Referencia = Detcuenta.Referencia1;
            referencia.LadaRef = Detcuenta.LadaRef1;
            referencia.TeleRef = Detcuenta.TeleRef1;
            referencia.Parentesco = Detcuenta.parantesco1;
            referencia.Celular = Detcuenta.CelRef1;


            Ref.Add(referencia);

            referencia = new Referencias();

            referencia.Referencia = Detcuenta.Referencia2;
            referencia.LadaRef = Detcuenta.LadaRef2;
            referencia.TeleRef = Detcuenta.TeleRef2;
            referencia.Parentesco = Detcuenta.parantesco2;
            referencia.Celular = Detcuenta.CelRef2;

            Ref.Add(referencia);

            return Ref;

        }
        public static List<ClientesReferencia> ObtenerReferenciasCliente(int idCliente)
        {
            return dalReferencia.ObtenerReferenciasCliente(idCliente);
        }
        public static Resultado<int> InsertarReferencia(ClientesReferencia referencia, bool actualizarClientes = true, int idUsuario = 0)
        {
            var respuestaInsertar = dalReferencia.InsertarReferencia(referencia);
            try
            {
                if (actualizarClientes && idUsuario <= 0) throw new Exception("La clave del usuario es incorrecta.");
                if (actualizarClientes)
                {
                    if (respuestaInsertar != null && respuestaInsertar.Codigo > 0 && respuestaInsertar.ResultObject > 0)
                    {
                        var respActClientes = AplicarCambiosTBClientes(referencia.IdCliente, idUsuario);

                        if (respActClientes != null && respActClientes.Codigo <= 0)
                        {
                            // Se revierten los cambios
                            EliminarReferenciaPermamente(respuestaInsertar.ResultObject, referencia.IdCliente);
                        }
                    }
                    else
                    {
                        throw new Exception("Ha ocurrido un error al actualizar las referencias del cliente.");
                    }
                }
            }
            catch (Exception ex)
            {
                respuestaInsertar.Codigo = 0;
                respuestaInsertar.ResultObject = 0;
                respuestaInsertar.Mensaje = ex.Message;
            }
            return respuestaInsertar;
        }
        public static Resultado<ClientesReferencia> ActualizarReferencia(ClientesReferencia referencia, bool actualizarClientes = true, int idUsuario = 0)
        {
            var respuestaActualizar = dalReferencia.ActualizarReferencia(referencia);
            try
            {
                if (actualizarClientes && idUsuario <= 0) throw new Exception("La clave del usuario es incorrecta.");
                if (actualizarClientes)
                {
                    if (respuestaActualizar != null && respuestaActualizar.Codigo > 0)
                    {
                        AplicarCambiosTBClientes(referencia.IdCliente, idUsuario);
                    }
                    else
                    {
                        throw new Exception("Ha ocurrido un error al actualizar las referencias del cliente.");
                    }
                }
            }
            catch (Exception ex)
            {
                respuestaActualizar.Codigo = 0;
                respuestaActualizar.ResultObject = null;
                respuestaActualizar.Mensaje = ex.Message;
            }
            return respuestaActualizar;
        }
        public static Resultado<int> ActualizarReferenciaMasivo(int idUsuario, List<ClientesReferencia> referencias)
        {
            var respuesta = new Resultado<int>();
            List<int> idsError = new List<int>();
            try
            {
                if (referencias != null && referencias.Count > 0)
                {
                    int idCliente = referencias[0].IdCliente;

                    List<ClientesReferencia> referenciasCliente = ObtenerReferenciasCliente(idCliente);
                    if (referenciasCliente != null && referenciasCliente.Count > 0)
                    {
                        //Comparar de BDD cuales referencias ya no existen en la lista nueva para inactivarse.
                        foreach (var referenciaCliente in referenciasCliente)
                        {
                            ClientesReferencia tempReferenciaEncontrada = null;
                            tempReferenciaEncontrada = referencias.Find(r => r.IdReferencia == referenciaCliente.IdReferencia);
                            if (tempReferenciaEncontrada == null)
                            {
                                var respEliminar = EliminarReferencia(referenciaCliente.IdReferencia, referenciaCliente.IdCliente, false);
                                if (respEliminar != null && respEliminar.Codigo <= 0)
                                    idsError.Add(referenciaCliente.IdReferencia);
                            }
                        }
                    }
                    foreach (var referencia in referencias)
                    {
                        if (referencia.IdCliente != idCliente) throw new Exception("Todas las referencias deben pertenecer al mismo cliente.");
                        if (referencia.IdReferencia > 0)
                        {
                            var respActualizar = ActualizarReferencia(referencia, false);
                            if (respActualizar != null && respActualizar.Codigo <= 0 && respActualizar.ResultObject != null)
                                idsError.Add(referencia.IdReferencia);
                        }
                        else
                        {
                            var respInsertar = InsertarReferencia(referencia, false);
                            if (respInsertar != null && respInsertar.Codigo <= 0)
                                idsError.Add(respInsertar.ResultObject);
                        }
                    }

                    //Aplicar solo una vez los cambios
                    AplicarCambiosTBClientes(idCliente, idUsuario);

                    respuesta.Codigo = (idsError.Count > 0) ? 0 : 1;
                    respuesta.Mensaje = (idsError.Count > 0) ? $"Ocurrieron {idsError.Count} errores en la operación, correspondientes a los IDs: {idsError.ToString()}" : "La operación se ejecutó con éxito";
                    respuesta.ResultObject = idsError.Count;
                }

            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.Mensaje = ex.Message;
                respuesta.ResultObject = 1;
            }
            return respuesta;
        }
        public static Resultado<bool> EliminarReferencia(int idReferencia, int idCliente, bool actualizarClientes = true, int idUsuario = 0)
        {
            var respuestaEliminar = dalReferencia.EliminarReferencia(idReferencia, idCliente);
            try
            {
                if (actualizarClientes && idUsuario <= 0) throw new Exception("La clave del usuario es incorrecta.");
                if (actualizarClientes)
                {
                    if (respuestaEliminar != null && respuestaEliminar.Codigo > 0)
                    {
                        var respActClientes = AplicarCambiosTBClientes(idCliente, idUsuario);

                        if (respActClientes != null && respActClientes.Codigo <= 0)
                        {
                            // Se revierten los cambios
                            var respReactivacion = ReactivarReferencia(idReferencia, idCliente);
                            if (respReactivacion != null && respReactivacion.Codigo > 0)
                            {
                                throw new Exception("Ha ocurrido un error al actualizar las referencias del cliente.");
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Ha ocurrido un error al actualizar las referencias del cliente.");
                    }
                }
            }
            catch (Exception ex)
            {
                respuestaEliminar.Codigo = 0;
                respuestaEliminar.ResultObject = false;
                respuestaEliminar.Mensaje = ex.Message;
            }
            return respuestaEliminar;
        }


        #region Private methods
        private static Resultado<int> ReactivarReferencia(int idReferencia, int idCliente)
        { 
            return dalReferencia.ReactivarReferenia(idReferencia, idCliente);
        }
        private static void EliminarReferenciaPermamente(int idReferencia, int idCliente)
        {
            dalReferencia.EliminarReferenciaPermanente(idReferencia, idCliente);
        }
        private static Resultado<bool> AplicarCambiosTBClientes(int idCliente, int idUsuario)
        {
            Resultado<bool> respuesta = null;
            try
            {
                var encontradas = dalReferencia.ObtenerUltimasReferenciasCliente(idCliente);
                if (encontradas != null)
                {
                    //Revertir orden de las referencias
                    encontradas.Reverse();
                    //ultimasReferencias = encontradas;
                    respuesta = TBClientesBll.ActualizarReferenciasCliente(idCliente, idUsuario, encontradas);
                }
            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.ResultObject = false;
                respuesta.Mensaje = ex.Message;
            }
            return respuesta;
        }
        #endregion
    }
}