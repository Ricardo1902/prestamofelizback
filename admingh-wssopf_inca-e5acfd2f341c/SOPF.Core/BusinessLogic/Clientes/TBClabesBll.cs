#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Clientes;
using SOPF.Core.DataAccess.Clientes;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBClabesBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBClabesDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Clientes
{
    public static class TBClabesBll
    {
        private static TBClabesDal dal;

        static TBClabesBll()
        {
            dal = new TBClabesDal();
        }

        public static TBClabes.DataResultClabe InsertarTBClabes(int Accion, TBClabes entidad)
        {
            return dal.InsertarTBClabes(Accion, entidad);
        }
       
        public static List<TBClabes.ClabeCliente> ObtenerClabesCliente(int Accion, int idCliente, int idsolicitud, string clabe)
        {
            return dal.ObtenerClabesCliente(Accion, idCliente, idsolicitud,clabe);
        }
        
        public static void ActualizarTBClabes()
        {
            dal.ActualizarTBClabes();
        }

        public static void EliminarTBClabes()
        {
            dal.EliminarTBClabes();
        }
    }
}