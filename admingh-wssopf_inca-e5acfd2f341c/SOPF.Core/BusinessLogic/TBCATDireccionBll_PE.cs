﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SOPF.Core.Entities;
using SOPF.Core.DataAccess.Catalogo;

namespace SOPF.Core.BusinessLogic
{
  public class TBCATDireccionBll_PE
  {
    private static TBCATDireccionDal_PE dal;

    static TBCATDireccionBll_PE()
    {
      dal = new TBCATDireccionDal_PE();
    }

    public static List<Combo> ObtenerCATCombo()
    {
      return dal.ObtenerCATCombo();
    }
  }
}
