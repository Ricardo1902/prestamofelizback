﻿using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using OfficeOpenXml;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Creditos;
using SOPF.Core.DataAccess.Cobranza;
using SOPF.Core.Entities.Cobranza;

namespace SOPF.Core.BusinessLogic.Creditos
{
    public class PagosNoIdentificadosBLL
    {
        public static GeneraPagoMavisoResponse GenPagosNoIdentificadosMasivo(GeneraPagoMavisoRequest pagos)
        {
            string[] extValidas = new string[] { ".xls", ".xlsx" };

            GeneraPagoMavisoResponse respuesta = new GeneraPagoMavisoResponse();
            try
            {
                string extension = Path.GetExtension(pagos.NombreArchivo);
                if (pagos.IdUsuario <= 0)
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "El usuario no es válido para realizar la carga de pagos.";
                    return respuesta;
                }
                if (!extValidas.Contains(extension))
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "El archivo no cuenta con el formato válido para realizar la carga de pagos masivos.";
                    return respuesta;
                }
                if (pagos.ContenidoArchivo == null && pagos.ContenidoArchivo.Length <= 0)
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "El archivo no contiene información para procesar pagos.";
                    return respuesta;
                }

                new Thread(() =>
                {
                    ProcesarArchivoPagoMasivo(pagos);
                })
                {
                    IsBackground = true
                }.Start();
            }
            catch (Exception e)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "EI02";
                respuesta.MensajeOperacion = "Ocurrió un error al momento de procesar los datos del pago.";
            }
            return respuesta;
        }
        private static void ProcesarArchivoPagoMasivo(GeneraPagoMavisoRequest pagos)
        {
            PagosNoIdentificados nuevoPagDetalle;
            List<string> validaciones = new List<string>();
            decimal monto = 0;            
            int registroMinimo = 0;
            int registroMaximo = 0;            
            DateTime fecha = Convert.ToDateTime("1900/01/01");
            DateTime FechaRegistro = DateTime.Now;
            int Idusuario = pagos.IdUsuario;
            
            PagoNoIdentificadosMasivo pagoMasivo = new PagoNoIdentificadosMasivo() { };            
           
            List<PagosNoIdentificados> pagoMasivoDetalle = new List<PagosNoIdentificados>();
           
            CanalPagoDAL dal = new CanalPagoDAL();
            List<CanalPago> canalesPago = new List<CanalPago>();
            canalesPago = dal.ListarActivos();
            TBCobranzaDal dal2 = new TBCobranzaDal();

            //Procesamiento de la carga del archivo.
            #region "ProcesoCargaArchivo"

            try
            {
                var ms = new MemoryStream(pagos.ContenidoArchivo);
                var excel = new ExcelPackage(ms);
                registroMinimo = 0;
                registroMaximo = 0;                
                fecha = Convert.ToDateTime("1900/01/01");
                FechaRegistro = DateTime.Now;               

                if (excel != null)
                {
                    var hoja1 = excel.Workbook.Worksheets.First();
                    if (hoja1 != null)
                    {
                        registroMinimo = hoja1.Dimension.Start.Row;
                        registroMaximo = hoja1.Dimension.End.Row;
                        //Para saltar los titulos
                        registroMinimo++;

                        int id_canal = 0;
                        
                        for (int i = registroMinimo; i <= registroMaximo; i++)
                        {
                            monto = 0;                            
                            fecha = Convert.ToDateTime("1900/01/01");
                            FechaRegistro = DateTime.Now;
                            validaciones = new List<string>();
                            id_canal = 0;
                            nuevoPagDetalle = new PagosNoIdentificados
                            {
                                Error = false
                            };
                            try
                            {                                
                               foreach(CanalPago cp in canalesPago)
                                {
                                    if(hoja1.Cells[i, 1].Text.ToString() == cp.Nombre)
                                    {
                                        id_canal = cp.IdCanalPago;
                                    }
                                }

                                nuevoPagDetalle.IdCanalPAgo = id_canal;
                                
                                DateTime.TryParseExact(hoja1.Cells[i, 3].Text.ToString(),"dd/MM/yyyy",null, System.Globalization.DateTimeStyles.None, out fecha);
                                nuevoPagDetalle.Fch_Pago = fecha;
                                decimal.TryParse(hoja1.Cells[i, 2].Text.ToString(), out monto);
                                nuevoPagDetalle.Monto = monto;
                                nuevoPagDetalle.Observaciones = hoja1.Cells[i, 4].Text.ToString();
                                nuevoPagDetalle.Fch_Resgistro = FechaRegistro;
                                
                                nuevoPagDetalle.IdUsuario = Idusuario;

                                //Validaciones de los campos
                                #region Validaciones de campos
                                //if (nuevoPagDetalle.IdSolicitud <= 0) validaciones.Add("La solicitud no es válida.");
                                if (nuevoPagDetalle.Monto <= 0) validaciones.Add("El valor del pago no es válido.");
                                if (nuevoPagDetalle.IdCanalPAgo <=0)
                                    validaciones.Add("El numero de canal de pago no es válido");

                                #endregion
                            }
                            catch (Exception e)
                            {
                                validaciones.Add("Ocurrió un error al cargar el registro." + e.Message);
                                nuevoPagDetalle.Error = true;
                            }
                            if (validaciones.Count > 0)
                            {
                                pagoMasivo.Error = true;
                            }
                            pagoMasivoDetalle.Add(nuevoPagDetalle);
                        }
                    }
                }
                else
                {
                    pagoMasivo.Error = true;
                    pagoMasivo.Mensaje = "No se econtró información de pagos a procesar.";
                }
            }
            catch (Exception e)
            {
                pagoMasivo.Error = true;
                pagoMasivo.Mensaje += "Ocurrió un error al procesar la carga del archivo de pagos. " + e.Message;
            }
            bool errorPagos = pagoMasivoDetalle.Any(p => p.Error.Value == true);

            if (errorPagos) pagoMasivo.Mensaje = "Ocurrió uno o varios errores al procesar la carga del archivo de pagos."
                    + pagoMasivo.Mensaje;

            bool procesaPagos = true;
            //Guardar el contenido del archivo de carga
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {                   
                    //Guardar detalle del pago masivo
                    foreach (var pago in pagoMasivoDetalle)
                    {                       
                        dal2.GuardaPagoNoIdentificadoIndiv(pago);
                    }
                }
            }
            catch (Exception e)
            {
                procesaPagos = false;
            }

            #endregion
        }
    }
}
