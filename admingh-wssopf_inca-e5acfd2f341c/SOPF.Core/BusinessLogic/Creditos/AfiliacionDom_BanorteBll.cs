﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.DataAccess.Creditos;
using SOPF.Core.Entities.Creditos;


namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class AfiliacionDom_BanorteBll
    {
        private static AfiliacionDom_BanorteDal dal;

        static AfiliacionDom_BanorteBll()
        {
            dal = new AfiliacionDom_BanorteDal();
        }

        public static List<AfiliacionDom_Banorte> ObtenerCuentas(int Accion)
        {
            return dal.CuentasAfiliar(Accion);
        }
    }
}
