﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.DataAccess.Creditos;
using Microsoft.VisualBasic;
using System.Data;
using System.ComponentModel;

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class PagosMasivosBll
    {
        private static PagosMasivosDal dal;

        static PagosMasivosBll()
        {
            dal = new PagosMasivosDal();
        }

        public static void InsertarPago(int idsolicitud, decimal monto, DateTime Fecha, decimal Comision, int id, int Accion, int Origen, DateTime Quincena, int GrupoRelacion, string TipoPago, int resultHeaderId)

        {
            dal.InsertarPago(idsolicitud, monto, Fecha, Comision, id, Accion, Origen, Quincena, GrupoRelacion, TipoPago, resultHeaderId);
        }

        public static int generaId()
        {
            int id = 0;
            id = dal.GeneraId();
            return id;
        }

        public static List<AutorizaPagosEfectivo> ConsultarPagoEfectivo(DateTime FchIni, DateTime FchFin, string Tip_Consul)
        {
            return dal.ConsultarPagoEfectivo(FchIni, FchFin, Tip_Consul);
        }

        public static void AutorizarPagoEfectivo(int IdFolio, decimal MontoAplicado, int Accion, int idGpo, int IdUser, int IdUserAut, DateTime Fecha, int IdSolicitud)
        {
            dal.AutorizarPagoEfectivo(IdFolio, MontoAplicado, Accion, idGpo, IdUser, IdUserAut, Fecha, IdSolicitud);
        }

        public static int ObtenerGrupoRelacion(int Accion, int idCobranzaBanco, int idReciboCobranza)
        {
            return dal.ObtenerGrupoRelacion( Accion, idCobranzaBanco, idReciboCobranza);
        }

        public static DataSet ConsultarPagosAplicados(int tipoConsul, int IdGrupo, string fch_pago)
        {
            DataSet dsFiniquito = new DataSet();
                DataTable dtFiniquito = new DataTable();
                List<PagosAplicados> listFiniquito = new List<PagosAplicados>();
                listFiniquito = dal.ConsultarPagosAplicados(tipoConsul, IdGrupo, fch_pago);

                dtFiniquito = ConvertToDatatable(listFiniquito);
                dsFiniquito.Tables.Add(dtFiniquito);

                return dsFiniquito;
            
            //return dal.ConsultarPagosAplicados(tipoConsul, IdGrupo, fch_pago);
        }

        //public static DataSet ObtenerTBCreditosFiniquitos(int accion, int cuenta, int Usuario)
        //{
        //    DataSet dsFiniquito = new DataSet();
        //    DataTable dtFiniquito = new DataTable();
        //    List<TBCreditos.Finiquitos> listFiniquito = new List<TBCreditos.Finiquitos>();
        //    listFiniquito = dal.ObtenerTBCreditosFiniquitos(accion, cuenta, Usuario);

        //    dtFiniquito = ConvertToDatatable(listFiniquito);
        //    dsFiniquito.Tables.Add(dtFiniquito);

        //    return dsFiniquito;
        //}
        public static DataTable ConvertToDatatable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

    }
}
