#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Creditos;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatProductoBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase CatProductoDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class CatProductoBll
    {
        private static CatProductoDal dal;

        static CatProductoBll()
        {
            dal = new CatProductoDal();
        }

        public static void InsertarCatProducto(CatProducto entidad)
        {
            dal.InsertarCatProducto(entidad);			
        }
        public static List<CatProducto> ObtenerCatProducto(int Accion, int idProducto, string producto)
        {
            return dal.ObtenerCatProducto(Accion,   idProducto, producto);
        }
        
        public static void ActualizarCatProducto()
        {
            dal.ActualizarCatProducto();
        }

        public static void EliminarCatProducto()
        {
            dal.EliminarCatProducto();
        }
    }
}