﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.DataAccess.Creditos;
using SOPF.Core.Entities.Creditos;

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class PromoRetencionesBll
    {
         private static PromoRetencionesDal dal;

         static PromoRetencionesBll()
        {
            dal = new PromoRetencionesDal();                    
        }
         public static List<PromoRetenciones> ObtenerPromoRetenciones(int Accion, int IdCliente, int IdSucursal, int IdCuenta, int IdUsuario)
         {
             return dal.ObtenerPromoRetenciones(Accion, IdCliente, IdSucursal, IdCuenta, IdUsuario);
         }
    }
}
