﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Creditos;
using System.ComponentModel;

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class EdoCtaBll
    {
          private static EdoCtaDal dal;
          private static TBCreditosDal dalCreditos;

          static EdoCtaBll()
        {
            dal = new EdoCtaDal();
            dalCreditos = new TBCreditosDal();           
              
        }

        
        public static DataSet ObtenerEdoCta(int idCuenta, int idCompania, DateTime FecIni, DateTime FecFin)
        {
            DataSet dsEdoCta = new DataSet();
            DataTable dtRazonSocial = new DataTable();            
            List<EdoCuenta.RazonSocial> listRazonSocial = new List<EdoCuenta.RazonSocial>();
            List<EdoCuenta.InfoCreditos> listDatosCredito = new List<EdoCuenta.InfoCreditos>();
            List<EdoCuenta.totalAbonos> listAbonos = new List<EdoCuenta.totalAbonos>();
            List<EdoCuenta.ResumenSaldos> listResumen = new List<EdoCuenta.ResumenSaldos>();
            List<EdoCuenta.Pagos> listPagos = new List<EdoCuenta.Pagos>();
            List<EdoCuenta.Vencidos> listVencidos = new List<EdoCuenta.Vencidos>();
            List<EdoCuenta.ProxVencimientos> listProximos = new List<EdoCuenta.ProxVencimientos>();

            double cat = 0.00;


            cat = TBCreditosBll.ObtenerCat(idCuenta);
            listRazonSocial.Add(dal.DatosRazonSocial(idCompania));
            dtRazonSocial = ConvertToDatatable(listRazonSocial);
            dsEdoCta.Tables.Add(dtRazonSocial);

            listDatosCredito.Add(dal.InfoCreditos(idCuenta, FecIni, FecFin, cat));
            dsEdoCta.Tables.Add(ConvertToDatatable(listDatosCredito));

            listAbonos.Add(dal.TotalAbonos(idCuenta, FecIni, FecFin, cat));
            
            
                dsEdoCta.Tables.Add(ConvertToDatatable(listAbonos));
            

            listResumen.Add(dal.Saldos(idCuenta, FecIni, FecFin, cat));
            
            
                dsEdoCta.Tables.Add(ConvertToDatatable(listResumen));
            

            listPagos   = dal.Pagos(idCuenta, FecIni, FecFin, cat);
            
            
                dsEdoCta.Tables.Add(ConvertToDatatable(listPagos));
            

            listVencidos = dal.RecibosVencidos(idCuenta, FecIni, FecFin, cat);
            
            
                dsEdoCta.Tables.Add(ConvertToDatatable(listVencidos));
            

            listProximos = dal.RecibosPorVencer(idCuenta, FecIni, FecFin, cat);
           
                dsEdoCta.Tables.Add(ConvertToDatatable(listProximos));
        


            return dsEdoCta;
        }

        public static DataTable ConvertToDatatable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        
    }
}