﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Creditos;
using Microsoft.VisualBasic;

namespace SOPF.Core.BusinessLogic.Creditos
{
    public class LotesImpresionBll
    {
        private static LotesImpresionDal dal;

        static LotesImpresionBll()
        {
            dal = new LotesImpresionDal();
        }

        public static List<LotesImpresion> ConsultarLotesImpresion(int Accion, int IdLote, DateTime Fecha, int IdSucursal, int IdUsuario, int FolioInicial, int FolioFinal, int IdEstatus, int IdFormato)
        {
            return dal.ConsultarLotesImpresion(Accion, IdLote, Fecha, IdSucursal, IdUsuario, FolioInicial, FolioFinal, IdEstatus, IdFormato);
        }

        public static void InsertarLote(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdEstatus, int IdFolio)
        {
            dal.InsertarLote(Accion, FechaAlta, IdUsuarioAlta, IdFormato,  IdSucursal,  FolioInicial,  FolioFinal,  IdEstatus, IdFolio);
        }

        public static void ActualizarLote(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdEstatus, int IdFolio)
        {
            dal.ActualizarLote(Accion, FechaAlta, IdUsuarioAlta, IdFormato, IdSucursal, FolioInicial, FolioFinal, IdEstatus, IdFolio);
        }
    }
}
