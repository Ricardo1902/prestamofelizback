﻿using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using OfficeOpenXml;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Creditos;
using SOPF.Core.DataAccess.Cobranza;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.Entities;

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class DevolucionesBLL
    {
        private static DevolucionesDal dal;

        static DevolucionesBLL()
        {
            dal = new DevolucionesDal();
        }

        public static GeneraDevolucionResponse GenDevolucionesMasivo(GeneraDevolucionRequest devoluciones)
        {
            string[] extValidas = new string[] { ".xls", ".xlsx" };

            GeneraDevolucionResponse respuesta = new GeneraDevolucionResponse();
            try
            {
                string extension = Path.GetExtension(devoluciones.nombreArchivo);
                if (devoluciones.idUsuario <= 0)
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "El usuario no es válido para realizar la carga de pagos.";
                    return respuesta;
                }
                if (!extValidas.Contains(extension))
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "El archivo no cuenta con el formato válido para realizar la carga de pagos masivos.";
                    return respuesta;
                }
                if (devoluciones.contenidoArchivo == null && devoluciones.contenidoArchivo.Length <= 0)
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "El archivo no contiene información para procesar pagos.";
                    return respuesta;
                }

                new Thread(() =>
                {
                    ProcesarArchivoDevolucionesMasivo(devoluciones);
                })
                {
                    IsBackground = true
                }.Start();
            }
            catch (Exception e)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "EI02";
                respuesta.MensajeOperacion = "Ocurrió un error al momento de procesar los datos del pago.";
            }
            return respuesta;
        }

        private static void ProcesarArchivoDevolucionesMasivo(GeneraDevolucionRequest devoluciones)
        {
            Devolucion devolucionDetalle;
            List<string> validaciones = new List<string>();
            decimal monto = 0;
            int idSolicitud = 0;
            int registroMinimo = 0;
            int registroMaximo = 0;            
            DateTime fecha = Convert.ToDateTime("1900/01/01");
            DateTime FechaRegistro = DateTime.Now;
            int Idusuario = devoluciones.idUsuario;
            
            DevolucionMasivo pagoMasivo = new DevolucionMasivo() { };            
           
            List<Devolucion> devolucionesDetalle = new List<Devolucion>();
           
            CanalPagoDAL dal = new CanalPagoDAL();
            List<CanalPago> canalesPago = new List<CanalPago>();
            canalesPago = dal.ListarActivos();
            TBCobranzaDal dal2 = new TBCobranzaDal();

            //Procesamiento de la carga del archivo.
            #region "ProcesoCargaArchivo"

            try
            {
                var ms = new MemoryStream(devoluciones.contenidoArchivo);
                var excel = new ExcelPackage(ms);
                registroMinimo = 0;
                registroMaximo = 0;                
                fecha = Convert.ToDateTime("1900/01/01");
                FechaRegistro = DateTime.Now;                

                if (excel != null)
                {
                    var hoja1 = excel.Workbook.Worksheets.First();
                    if (hoja1 != null)
                    {
                        registroMinimo = hoja1.Dimension.Start.Row;
                        registroMaximo = hoja1.Dimension.End.Row;
                        //Para saltar los titulos
                        registroMinimo++;
                        
                        for (int i = registroMinimo; i <= registroMaximo; i++)
                        {
                            monto = 0;                            
                            fecha = Convert.ToDateTime("1900/01/01");
                            FechaRegistro = DateTime.Now;
                            validaciones = new List<string>();                            
                            devolucionDetalle = new Devolucion
                            {
                                error = false
                            };
                            try
                            {   
                                DateTime.TryParseExact(hoja1.Cells[i, 3].Text.ToString(),"dd/MM/yyyy",null, System.Globalization.DateTimeStyles.None, out fecha);
                                TimeSpan ts = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                                fecha = fecha.Date + ts;
                                devolucionDetalle.fechaPago = fecha;
                                decimal.TryParse(hoja1.Cells[i, 2].Text.ToString(), out monto);                                
                                devolucionDetalle.montoPago = monto;
                                int.TryParse(hoja1.Cells[i, 1].Text.ToString(), out idSolicitud);
                                devolucionDetalle.idSolicitud = idSolicitud;
                                devolucionDetalle.comentarios = hoja1.Cells[i, 4].Text.ToString();                                
                                
                                devolucionDetalle.idUsuario = Idusuario;

                                //Validaciones de los campos
                                #region Validaciones de campos
                                //if (nuevoPagDetalle.IdSolicitud <= 0) validaciones.Add("La solicitud no es válida.");
                                if (devolucionDetalle.montoPago <= 0) validaciones.Add("El valor del pago no es válido.");
                                
                                #endregion
                            }
                            catch (Exception e)
                            {
                                validaciones.Add("Ocurrió un error al cargar el registro." + e.Message);
                                devolucionDetalle.error = true;
                            }
                            if (validaciones.Count > 0)
                            {
                                pagoMasivo.error = true;
                            }
                            devolucionesDetalle.Add(devolucionDetalle);
                        }
                    }
                }
                else
                {
                    pagoMasivo.error = true;
                    pagoMasivo.mensaje = "No se econtró información de pagos a procesar.";
                }
            }
            catch (Exception e)
            {
                pagoMasivo.error = true;
                pagoMasivo.mensaje += "Ocurrió un error al procesar la carga del archivo de devoluciones. " + e.Message;
            }
            bool errorPagos = devolucionesDetalle.Any(p => p.error.Value == true);

            if (errorPagos) pagoMasivo.mensaje = "Ocurrió uno o varios errores al procesar la carga del archivo de devoluciones."
                    + pagoMasivo.mensaje;

            bool procesaPagos = true;
            //Guardar el contenido del archivo de carga
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {                   
                    //Guardar detalle del pago masivo
                    foreach (var devolucion in devolucionesDetalle)
                    {                   
                        dal2.GuardaDevolucion(devolucion);                        
                    }
                }

                dal2.AplicarDevolucionesPendientes();
            }
            catch (Exception e)
            {
                procesaPagos = false;
            }

            #endregion
        }

        public static Resultado<bool> EliminarDevolucion(Devolucion Entity, int IdUsuario)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            objReturn = dal.EliminarDevolucion(Entity, IdUsuario);

            return objReturn;
        }
    }
}
