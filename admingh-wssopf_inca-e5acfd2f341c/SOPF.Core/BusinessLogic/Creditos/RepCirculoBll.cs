#pragma warning disable 141208
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Creditos;
using System.Xml;
using System.IO;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: RepCirculoBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase RepCirculoDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-12-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class RepCirculoBll
    {
        private static RepCirculoDal dal;

        static RepCirculoBll()
        {
            dal = new RepCirculoDal();
        }

        public static void InsertarRepCirculo(RepCirculo entidad)
        {
            dal.InsertarRepCirculo(entidad);			
        }
        
        public static void ObtenerRepCirculo(int Accion, DateTime Fecha)
        {

            List<RepCirculo> repCirculo = new List<RepCirculo>();
            repCirculo =  dal.ObtenerRepCirculo(Accion, Fecha);

            XmlDocument xmlRespuesta = new XmlDocument();
            StringBuilder xml = new StringBuilder();
            string NombreArchivo = "";

            NombreArchivo = repCirculo[0].NombreOtorgante + repCirculo[0].FechaExtraccion + ".xml";
            

           

            //xml.Append("<?xmlversion=\"1.0\" encoding = \"ISO-8859-1\"?>");
            xml.Append("<Carga>");

                xml.Append("<Enabezado>");
                    xml.Append("<ClaveOtorgante>");
                        xml.Append(repCirculo[0].ClaveOtorgante.ToString());
                    xml.Append("</ClaveOtorgante>");
                    xml.Append("<NombreOtorgante>");
                        xml.Append(repCirculo[0].NombreOtorgante.ToString());
                    xml.Append("</NombreOtorgante>");
                    xml.Append("<IdentificadorDeMedio>");
                        
                    xml.Append("</IdentificadorDeMedio>");
                    xml.Append("<FechaExtraccion>");
                        xml.Append(repCirculo[0].FechaExtraccion.ToString());
                    xml.Append("</FechaExtraccion>");
                    xml.Append("<NotaOtorgante>");
                        
                    xml.Append("</NotaOtorgante>");
                    xml.Append("<Version>");
                    xml.Append(repCirculo[0].Versi.ToString());
                    xml.Append("</Version>");
                xml.Append("</Enabezado>");
                xml.Append("<Personas>");
                foreach (RepCirculo reporte in repCirculo)
                {
                    
                        xml.Append("<Persona>");

                            xml.Append("<Nombre>");
                                xml.Append("<ApellidoPaterno>");
                                    xml.Append(reporte.ApellidPaterno);
                                xml.Append("</ApellidoPaterno>");
                                xml.Append("<ApellidoMaterno>");
                                    xml.Append(reporte.ApellidoMaterno);
                                xml.Append("</ApellidoMaterno>");
                                xml.Append("<ApellidoAdicional>");
                                xml.Append("</ApellidoAdicional>");
                                xml.Append("<Nombres>");
                                    xml.Append(reporte.Nombres);
                                xml.Append("</Nombres>");
                                xml.Append("<FechaNacimiento>");
                                    xml.Append(reporte.FechaNacimiento);               
                                xml.Append("</FechaNacimiento>");
                                xml.Append("<RFC>");
                                    xml.Append(reporte.RFC);
                                xml.Append("</RFC>");
                                xml.Append("<CURP>");
                                    xml.Append(reporte.CURP);
                                xml.Append("</CURP>");
                                xml.Append("<Nacionalidad>");
                                    xml.Append(reporte.NACIONALIDAD);
                                xml.Append("</Nacionalidad>");
                                xml.Append("<Residencia>");
                                xml.Append("</Residencia>");
                                xml.Append("<NumeroLicenciaConducir>");
                                xml.Append("</NumeroLicenciaConducir>");
                                xml.Append("<EstadoCivil>");
                                xml.Append("</EstadoCivil>");
                                xml.Append("<Sexo>");
                                xml.Append("</Sexo>");
                                xml.Append("<ClaveElectorIFE>");
                                xml.Append("</ClaveElectorIFE>");
                                xml.Append("<NumeroDependientes>");
                                xml.Append("</NumeroDependientes>");
                                xml.Append("<FechaDefuncion>");
                                xml.Append("</FechaDefuncion>");
                                xml.Append("<IndicadorDefuncion>");
                                xml.Append("</IndicadorDefuncion>");
                                xml.Append("<TipoPersona>");
                                xml.Append("</TipoPersona>");
                            xml.Append("</Nombre>");

                            xml.Append("<Domicilios>");
                                xml.Append("<Domicilio>");
                                    xml.Append("<Direccion>");
                                        xml.Append(reporte.Direccion);
                                    xml.Append("</Direccion>");
                                    xml.Append("<ColoniaPoblacion>");
                                        
                                    xml.Append("</ColoniaPoblacion>");
                                    xml.Append("<DelegacionMunicipio>");
                                        
                                    xml.Append("</DelegacionMunicipio>");
                                    xml.Append("<Ciudad>");
                                        xml.Append(reporte.Ciudad);
                                    xml.Append("</Ciudad>");
                                    xml.Append("<Estado>");
                                        xml.Append(reporte.Estado);
                                    xml.Append("</Estado>");
                                    xml.Append("<CP>");
                                        xml.Append(reporte.CP);
                                    xml.Append("</CP>");
                                    xml.Append("<FechaResidencia>");
                                    xml.Append("</FechaResidencia>");
                                    xml.Append("<NumeroTelefono>");
                                    xml.Append("</NumeroTelefono>");
                                    xml.Append("<TipoDomicilio>");
                                    xml.Append("</TipoDomicilio>");
                                    xml.Append("<TipoAsentamiento>");
                                    xml.Append("</TipoAsentamiento>");
                                    
                                xml.Append("</Domicilio>");
                            xml.Append("</Domicilios>");

                            xml.Append("<Empleos>");
                                xml.Append("<Empleo>");
                                    xml.Append("<NombreEmpresa>");
                                    xml.Append(reporte.NombreEmpresa);
                                    xml.Append("</NombreEmpresa>");
                                    xml.Append("<Direccion>");
                                    xml.Append(reporte.DireccionEmpresa);
                                    xml.Append("</Direccion>");
                                    xml.Append("<ColoniaPoblacion>");
                                    xml.Append(reporte.ColoniaEmpresa);
                                    xml.Append("</ColoniaPoblacion>");
                                    xml.Append("<DelagacionMuncipio>");
                                    xml.Append(reporte.CiudadEmpresa);
                                    xml.Append("</DelagacionMuncipio>");
                                    xml.Append("<Ciudad>");
                                    xml.Append(reporte.CiudadEmpresa);
                                    xml.Append("</Ciudad>");
                                    xml.Append("<Estado>");
                                    xml.Append(reporte.EstadoEmpresa);
                                    xml.Append("</Estado>");
                                    xml.Append("<CP>");
                                    xml.Append(reporte.CPEmpresa);
                                    xml.Append("</CP>");
                                    xml.Append("<NumeroTelefono>");
                                    xml.Append(reporte.TelefonoEmpresa);
                                    xml.Append("</NumeroTelefono>");
                                    xml.Append("<Extension>");
                                    xml.Append(reporte.Extension);
                                    xml.Append("</Extension>");
                                    xml.Append("<Fax>");
                                    
                                    xml.Append("</Fax>");
                                    xml.Append("<Puesto>");
                                    xml.Append(reporte.Puesto);
                                    xml.Append("</Puesto>");
                                    xml.Append("<FechaContratacion>");
                                    xml.Append(reporte.FechaContratacion);
                                    xml.Append("</FechaContratacion>");
                                    xml.Append("<ClaveMoneda>");
                                    xml.Append(reporte.ClaveMonedaEmp);
                                    xml.Append("</ClaveMoneda>");
                                    xml.Append("<SalarioMensual>");
                                    xml.Append(reporte.Salario);
                                    xml.Append("</SalarioMensual>");
                                    xml.Append("<FechaUltimoDiaEmpleo>");

                                    xml.Append("</FechaUltimoDiaEmpleo>");
                                    xml.Append("<FechaVerificacionEmpleo>");
                                    xml.Append(reporte.FechaVerificacion);
                                    xml.Append("</FechaVerificacionEmpleo>");
                                xml.Append("</Empleo>");
                            xml.Append("</Empleos>");

                            xml.Append("<Cuenta>");
                                xml.Append("<ClaveActualOtorgante>");
                                    xml.Append(reporte.ClaveOtorgante);
                                xml.Append("</ClaveActualOtorgante>");
                                xml.Append("<NombreOtorgante>");
                                    xml.Append(reporte.NombreOtorgante);
                                xml.Append("</NombreOtorgante>");
                                xml.Append("<CuentaActual>");
                                    xml.Append(reporte.CuentaActual);
                                xml.Append("</CuentaActual>");
                                xml.Append("<TipoResponsabilidad>");
                                    xml.Append(reporte.TipoResponsabilidad);
                                xml.Append("</TipoResponsabilidad>");
                                xml.Append("<TipoCuenta>");
                                    xml.Append(reporte.TipoCuenta);
                                xml.Append("</TipoCuenta>");
                                xml.Append("<TipoContrato>");
                                    xml.Append(reporte.TipoContrato);
                                xml.Append("</TipoContrato>");
                                xml.Append("<ClaveUnidadMonetaria>");
                                    xml.Append(reporte.CveUnicaMoneda);
                                xml.Append("</ClaveUnidadMonetaria>");
                                xml.Append("<ValorActivoValuacion>");
                                xml.Append("</ValorActivoValuacion>");
                                xml.Append("<NumeroPagos>");
                                    xml.Append(reporte.NumeroPagos);
                                xml.Append("</NumeroPagos>");
                                xml.Append("<FrecuenciaPagos>");
                                    xml.Append(reporte.FrecuenciaPagos);
                                xml.Append("</FrecuenciaPagos>");
                                xml.Append("<MontoPagar>");
                                    xml.Append(reporte.MontoPagar);
                                xml.Append("</MontoPagar>");
                                xml.Append("<FechaAperturaCuenta>");
                                    xml.Append(reporte.FchApertura);
                                xml.Append("</FechaAperturaCuenta>");
                                xml.Append("<FechaUltimoPago>");
                                    xml.Append(reporte.FchUltimoPago);
                                xml.Append("</FechaUltimoPago>");
                                xml.Append("<FechaUltimaCompra>");
                                    xml.Append(reporte.FchUltimaCompra);
                                xml.Append("</FechaUltimaCompra>");
                                xml.Append("<FechaCierreCuenta>");
                                    xml.Append(reporte.FechaCierre);
                                xml.Append("</FechaCierreCuenta>");
                                xml.Append("<FechaCorte>");
                                    xml.Append(reporte.FechaCorte);
                                xml.Append("</FechaCorte>");
                                xml.Append("<Garantia>");
                                xml.Append("</Garantia>");
                                xml.Append("<CreditoMaximo>");
                                    xml.Append(reporte.CreditoMaximo);
                                xml.Append("</CreditoMaximo>");
                                xml.Append("<SaldoActual>");
                                    xml.Append(reporte.SaldoActual);
                                xml.Append("</SaldoActual>");
                                xml.Append("<LimiteCredito>");
                                    xml.Append(reporte.LimiteCredito);
                                xml.Append("</LimiteCredito>");
                                xml.Append("<SaldoVencido>");
                                    xml.Append(reporte.Vencido);
                                xml.Append("</SaldoVencido>");
                                xml.Append("<NumeroPagosVencidos>");
                                    xml.Append(reporte.RecibosVencidos);
                                xml.Append("</NumeroPagosVencidos>");
                                xml.Append("<PagoActual>");
                                    xml.Append(reporte.PagoActual);
                                xml.Append("</PagoActual>");
                                xml.Append("<HistoricoPagos>");
                                xml.Append("</HistoricoPagos>");
                                xml.Append("<ClavePrevencion>");
                                xml.Append("</ClavePrevencion>");
                                xml.Append("<TotalPagosReportados>");
                                xml.Append("</TotalPagosReportados>");
                                xml.Append("<ClaveAnteriorOtorgante>");
                                xml.Append("</ClaveAnteriorOtorgante>");
                                xml.Append("<NombreAnteriorOtorgante>");
                                xml.Append("</NombreAnteriorOtorgante>");
                                xml.Append("<NumeroCuentaAnteriro>");
                                xml.Append("</NumeroCuentaAnteriro>");
                                xml.Append("<FechaPrimerIncumplimiento>");
                                xml.Append("</FechaPrimerIncumplimiento>");
                                xml.Append("<SaldoInsoluto>");
                                    xml.Append(reporte.SaldoInsoluto);
                                xml.Append("</SaldoInsoluto>");
                                xml.Append("<MontoUltimoPago>");
                                xml.Append("</MontoUltimoPago>");
                            xml.Append("</Cuenta>");
                        xml.Append("</Persona>");
                    
                }
                xml.Append("</Personas>");
                xml.Append("<CifrasControl>");
                    xml.Append("<TotalSaldosActuales>");
                        xml.Append(repCirculo[0].TotalSaldoAct.ToString());
                    xml.Append("</TotalSaldosActuales>");
                    xml.Append("<TotalSaldosVencidos>");
                        xml.Append(repCirculo[0].TotalSaldoVen.ToString());
                    xml.Append("</TotalSaldosVencidos>");
                    xml.Append("<TotalElementosNombreReportados>");
                        xml.Append(repCirculo[0].TotalNombRepo);
                    xml.Append("</TotalElementosNombreReportados>");
                    xml.Append("<TotalElementosDireccionReportados>");
                        xml.Append(repCirculo[0].TotalDireRepo);
                    xml.Append("</TotalElementosDireccionReportados>");
                    xml.Append("<TotalElementosEmpleoReportados>");
                        xml.Append(repCirculo[0].TotalEmplRepo);
                    xml.Append("</TotalElementosEmpleoReportados>");
                    xml.Append("<TotalElementosCuentaReportados>");
                        xml.Append(repCirculo[0].TotalCuenRepo);
                    xml.Append("</TotalElementosCuentaReportados>");
                    xml.Append("<NombreOtorgante>");
                        xml.Append(repCirculo[0].NombreOtorgante);
                    xml.Append("</NombreOtorgante>");
                    xml.Append("<DomicilioDevolucion>");
                        xml.Append(repCirculo[0].DomicilioDev);
                    xml.Append("</DomicilioDevolucion>");
                xml.Append("</CifrasControl>");
            xml.Append("</Carga>");
            try
            {
                

                string linea;
                //using (var ms = new MemoryStream())
                //{
                //    using (var tw = new StreamWriter(ms, Encoding.UTF8))
                //    {
                //        xml.(tw);
                //    }
                //    linea = Encoding.UTF8.GetString(ms.GetBuffer(), 0, (int)ms.Length);
                //}

                //Console.Write(xml);
                linea = xml.ToString();
                List<string> lineas = new List<string>();
                lineas.Add(linea);
                System.IO.File.WriteAllLines(@"C:\AfiliacionBanorte_SOPF\" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                xmlRespuesta.LoadXml(xml.ToString());
                xmlRespuesta.Save("C:\\Users\\Juan Carlos\\Documents\\JCGarcia\\" + NombreArchivo);
            }
            catch (Exception ex)
            {
                string Nombre;
                Nombre = (repCirculo[0].Nombres);
            }

        }
        
        public static void ActualizarRepCirculo()
        {
            dal.ActualizarRepCirculo();
        }

        public static void EliminarRepCirculo()
        {
            dal.EliminarRepCirculo();
        }
    }
}