#pragma warning disable 141229
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Creditos;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CortesDispersionBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase CortesDispersionDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-12-29 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class CortesDispersionBll
    {
        private static CortesDispersionDal dal;

        static CortesDispersionBll()
        {
            dal = new CortesDispersionDal();
        }

        public static void InsertarCortesDispersion(CortesDispersion entidad)
        {
            dal.InsertarCortesDispersion(entidad);			
        }
        
        public static List<CortesDispersion> ObtenerCortesDispersion(int Accion, int idcorte)
        {
            return dal.ObtenerCortesDispersion(Accion, idcorte);
        }
        
        public static void ActualizarCortesDispersion()
        {
            dal.ActualizarCortesDispersion();
        }

        public static void EliminarCortesDispersion()
        {
            dal.EliminarCortesDispersion();
        }
    }
}