﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Creditos;
using System.ComponentModel;

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class CompDispersionBll
    {
         private static CompDispersionDal dal;


         static CompDispersionBll()
        {
            dal = new CompDispersionDal();                    
        }
         public static List<DataResultCredito.Usp_Error> CompDispersion(int idcuenta, decimal monto)
         {
             return dal.VerificarDiespersion(idcuenta, monto);
         }

    }
}
