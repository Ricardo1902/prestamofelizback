﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Creditos;
using Microsoft.VisualBasic;

namespace SOPF.Core.BusinessLogic.Creditos
{
    public class ReimpresionFormatosBll
    {
        private static ReimpresionFormatosDal dal;

        static ReimpresionFormatosBll()
        {
            dal = new ReimpresionFormatosDal();
        }

        public static List<ReimpresionFormatos> ConsultarReimpresion(int Accion, int IdFolio, DateTime Fecha, int IdSucursal, int IdUsuario, int FolioInicial, int FolioFinal, int IdEstatus, int IdFormato)
        {
            return dal.ConsultarReimpresion(Accion, IdFolio, Fecha, IdSucursal, IdUsuario, FolioInicial, FolioFinal, IdEstatus, IdFormato);
        }

        public static void InsertarReimpresion(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdFolio, string Comentario, int IdEstatus)
        {
            dal.InsertarReimpresion(Accion,  FechaAlta,  IdUsuarioAlta,  IdFormato,  IdSucursal,  FolioInicial,  FolioFinal,  IdFolio,  Comentario,  IdEstatus);
        }

        public static void EliminarReimpresion(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdFolio, string Comentario, int IdEstatus)
        {
            dal.EliminarReimpresion(Accion, FechaAlta, IdUsuarioAlta, IdFormato, IdSucursal, FolioInicial, FolioFinal, IdFolio, Comentario, IdEstatus);
        }

        public static void ActualizarReimpresion(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdFolio, string Comentario, int IdEstatus)
        {
            dal.ActualizarReimpresion(Accion, FechaAlta, IdUsuarioAlta, IdFormato, IdSucursal, FolioInicial, FolioFinal, IdFolio, Comentario, IdEstatus);
        }

    }
}
