#pragma warning disable 140827
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Creditos;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.Entities;

#region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TBCobranzaBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCobranzaDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class TBCobranzaBll
    {
        private static TBCobranzaDal dal;

        static TBCobranzaBll()
        {
            dal = new TBCobranzaDal();
        }

        public static void InsertarTBCobranza(TBCobranza entidad)
        {
            dal.InsertarTBCobranza(entidad);			
        }
        
        public static TBCobranza ObtenerTBCobranza()
        {
            return dal.ObtenerTBCobranza();
        }

        public static List<DataResultCredito.Usp_HistorialPagos> ObtenerHistorialPagos(int idCuenta)
        {
            return dal.ObtenerHistorialPagos(idCuenta);
        }      

        public static List<PagosNoIdentificados> ObtenerPagosNoIdentificados(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta)
        {
           return dal.ObtenerPagosNoIdentificados(fchCreditoDesde, fchCreditoHasta);            
        }

        public static List<Devolucion> ObtenerDevoluciones(DevolucionCondiciones condiciones)
        {
            return dal.ObtenerDevoluciones(condiciones);
        }

        public static Resultado<bool> GuardaPagoNoIdentificadoIndiv(PagosNoIdentificados pagos)
        {
            return dal.GuardaPagoNoIdentificadoIndiv( pagos);
        }

        public static Resultado<bool> GuardaDevolucion(Devolucion devolucion)
        {
            return dal.GuardaDevolucion(devolucion);
        }

        public static Resultado<bool> AplicarDevolucionesPendientes()
        {
            return dal.AplicarDevolucionesPendientes();
        }

        public static Resultado<bool> ActuPagoNoIdentificadoIndiv(PagosNoIdentificados pagos)
        {
            return dal.ActuPagoNoIdentificadoIndiv(pagos);
        }
    }
}