﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.DataAccess.Creditos;
using SOPF.Core.Entities.Creditos;

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class ClientesAfiliacionBll
    {
        private static ClientesAfiliacionDal dal;

        static  ClientesAfiliacionBll()
        {
            dal = new ClientesAfiliacionDal();
        }

        public static List<ClientesAfiliacion> ObtenerCuentas(int Accion)
        {
            return dal.CuentasAfiliar(Accion);
        }
        public static List<ClientesAfiliacion> ObtenerCuentasBancomer(int Accion)
        {
            return dal.CuentasAfiliarBancomer(Accion);
        }
        public static List<ClientesAfiliacion> ObtenerCuentasRec(int Accion)
        {
            return dal.CuentasAfiliarRec(Accion);
        }
    }
}
