#pragma warning disable 140827
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Creditos;

# region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBRecibosBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBRecibosDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class TBRecibosBll
    {
        private static TBRecibosDal dal;

        static TBRecibosBll()
        {
            dal = new TBRecibosDal();
        }

        public static void InsertarTBRecibos(TBRecibos entidad)
        {
            dal.InsertarTBRecibos(entidad);			
        }
        
        public static List<DataResultCredito.Usp_ObtenerTablaAmortizacion> ObtenerTBRecibos(int idCuenta)
        {
            return dal.ObtenerTBRecibos(idCuenta);
        }
        
        public static void ActualizarTBRecibos()
        {
            dal.ActualizarTBRecibos();
        }

        public static void EliminarTBRecibos()
        {
            dal.EliminarTBRecibos();
        }
    }
}