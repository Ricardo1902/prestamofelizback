#pragma warning disable 141229
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Creditos;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CorteDispersionDetalleBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase CorteDispersionDetalleDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-12-29 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class CorteDispersionDetalleBll
    {
        private static CorteDispersionDetalleDal dal;

        static CorteDispersionDetalleBll()
        {
            dal = new CorteDispersionDetalleDal();
        }

        public static void InsertarCorteDispersionDetalle(CorteDispersionDetalle entidad)
        {
            dal.InsertarCorteDispersionDetalle(entidad);			
        }
        
        public static List<DataResultCredito.Usp_SelDetalleCorteDispersion> ObtenerCorteDispersionDetalle(int Accion, int idCorte)
        {
            return dal.ObtenerCorteDispersionDetalle(Accion,idCorte);
        }
        
        public static void ActualizarCorteDispersionDetalle(int Accion, int IdCorte, int IdSolicitud)
        {
            dal.ActCorteDispersionDetalle(Accion,IdCorte,IdSolicitud);
        }

        public static void EliminarCorteDispersionDetalle()
        {
            dal.EliminarCorteDispersionDetalle();
        }
    }
}