#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.DataAccess.Creditos;
using Microsoft.VisualBasic;
using System.Data;
using System.ComponentModel;
using SOPF.Core.Model.Response.Creditos;
using SOPF.Core.Model.Request.Creditos;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.BusinessLogic.Solicitudes;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.BusinessLogic.Catalogo;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.BusinessLogic.Gestiones;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Model.Request.Gestiones;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TBCreditosBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCreditosDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Creditos
{
    public static class TBCreditosBll
    {
        private static TBCreditosDal dal;
        private static CreditOrigContext con;

        static TBCreditosBll()
        {
            dal = new TBCreditosDal();
            con = new CreditOrigContext();
        }

        public static DataResultCredito.Usp_SelDatosPago SelDatosPago(int idproducto, decimal capital, int tipocredito, decimal deducciones, decimal ingreso)
        {
            return dal.ObtenerDatosPago(idproducto,capital,tipocredito,deducciones,ingreso);
        }
           
        public static void InsertarTBCreditos(TBCreditos entidad)
        {
            dal.InsertarTBCreditos(entidad);
        }

        public static List<TBCreditos> ObtenerTBCreditos(int accion, int cuenta)
        {
            return dal.ObtenerTBCreditos(accion, cuenta);
        }

        public static DataSet ObtenerTBCreditosListado(int Accion, int IdSolicitud, int IdCuenta, int IdCliente, string Cliente, string FechaInicial, string FechaFinal, int IdSucursal, int IdUsuario, int comodin1, string comodin2)
        {
            DataSet dsCreditos = new DataSet();
            DataTable dtCreditos= new DataTable();
            List<TBCreditos> listCreditos = new List<TBCreditos>();
            listCreditos = dal.ObtenerTBCreditosListado(Accion, IdSolicitud, IdCuenta, IdCliente, Cliente, FechaInicial, FechaFinal, IdSucursal, IdUsuario, comodin1, comodin2);
            dtCreditos = ConvertToDatatable(listCreditos);
            dsCreditos.Tables.Add(dtCreditos);
            return dsCreditos;
        }   
        
        public static DataSet ObtenerTBCreditosFiniquitos(int accion, int cuenta, int Usuario)
        {
            DataSet dsFiniquito = new DataSet();
            DataTable dtFiniquito = new DataTable();
            List<TBCreditos.Finiquitos> listFiniquito = new List<TBCreditos.Finiquitos>();
            listFiniquito = dal.ObtenerTBCreditosFiniquitos(accion, cuenta, Usuario);
            dtFiniquito = ConvertToDatatable(listFiniquito);
            dsFiniquito.Tables.Add(dtFiniquito);
            return dsFiniquito;
        }

        public static DataTable ConvertToDatatable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static List<DataResultCredito.Usp_SelCredito> ObtenerTBCreditosFechas(DateTime fechaIni, DateTime fechaFin, int Accion, int idPromotor)
        {
            return dal.ObtenerTBCreditosFecha(Accion, fechaIni, fechaFin, idPromotor);
        }

        public static void ActualizarTBCreditos()
        {
            dal.ActualizarTBCreditos();
        }

        public static void EliminarTBCreditos()
        {
            dal.EliminarTBCreditos();
        }

        public static double ObtenerCat(int idcuenta)
        {
            double CAT = 0.00;
            double TIR = 0.00;

            double guess = 0.01;
            DetalleCuenta cuenta = new DetalleCuenta();
            List<DataResultCredito.Usp_ObtenerTablaAmortizacion> recibos = new List<DataResultCredito.Usp_ObtenerTablaAmortizacion>();
            cuenta = Gestiones.DetalleCuentaBll.ObtenerDetalleCuenta(idcuenta);
            recibos = Creditos.TBRecibosBll.ObtenerTBRecibos(idcuenta);
            int totalPagos = 0;
            switch (cuenta.frecuencia)
            {
                case "M":
                    totalPagos = 12;
                    break;

                case "Q":
                    totalPagos = 24;
                    break;
                default:
                    totalPagos = 12;
                    break;
            }

            decimal Capital = cuenta.Capital;
            decimal Ergoacion = cuenta.Erogacion;
            decimal valorinicial = -1 * Capital;
            int RecibosTotales = cuenta.Rectot;
            double[] valores = new double[RecibosTotales + 1];
            for (int i = 0; i <= RecibosTotales; i++)
            {
                if (i > 0)
                {
                    valorinicial = recibos[i - 1].Capital + recibos[i - 1].Interes;
                    valores[i] = Convert.ToDouble(valorinicial);
                }
                else
                {
                    valores[i] = Convert.ToDouble(valorinicial);
                }
            }

            TIR = Financial.IRR(ref valores, guess);
            CAT = Math.Round((Math.Pow(1 + TIR, totalPagos) - 1) * 100, 2);
            return CAT;
        }

        public static List<HistorialCobranza> ObtenerHistorial(int IdBanco, int Tipoconvenio, int convenio, DateTime Fecini, DateTime FecFin)
        {
            return dal.ObtenerHistorialCobranza(IdBanco, Tipoconvenio, convenio, Fecini, FecFin);
        }

        public static List<HistorialCobranza> ObtenerHistorialMontos(int IdBanco, int Tipoconvenio, int convenio, DateTime Fecini, DateTime FecFin)
        {
            return dal.ObtenerHistorialCobranzaMontos(IdBanco, Tipoconvenio, convenio, Fecini, FecFin);
        }

        public static TBCreditos.GeneraReferenciaCIE GeneraReferenciaCIE(DateTime FechaAutoriza, int idsolicitud, int idusuario, int origen)
        {
            return dal.GeneraReferenciaCIE(FechaAutoriza, idsolicitud, idusuario, origen);
        }

        public static Resultado<TBCreditos.AmpliacionEntity> ObtenerInfoAmpliacionAnterior(int IdSolicitud)
        {
            return dal.ObtenerInfoAmpliacion(IdSolicitud, 35);
        }

        public static Resultado<TBCreditos.AmpliacionEntity> ObtenerInfoAmpliacionPosterior(int IdSolicitud)
        {
            return dal.ObtenerInfoAmpliacion(IdSolicitud, 36);
        }

        public static Resultado<TBCreditos.CalculoLiquidacion> CalcularLiquidacion(TBCreditos.Liquidacion eOwner)
        {
            return dal.CalcularLiquidacion(eOwner);
        }

        public static Resultado<TBCreditos.Liquidacion> RegistrarLiquidacion(TBCreditos.Liquidacion eOwner)
        {
            return dal.RegistrarLiquidacion(eOwner);
        }

        public static TBCreditos.Liquidacion ObtenerLiquidacionActiva(TBCreditos.Liquidacion eOwner)
        {
            return dal.ObtenerLiquidacionActiva(eOwner);
        }

        public static TBCreditos.Liquidacion ObtenerLiquidacionCredito(TBCreditos.Liquidacion eOwner)
        {
            return dal.ObtenerLiquidacionCredito(eOwner);
        }

        public static Resultado<TBCreditos.Liquidacion> ConfirmarLiquidacion(TBCreditos.Liquidacion eOwner)
        {
            return dal.ConfirmarLiquidacion(eOwner);
        }

        public static Resultado<TBCreditos.Liquidacion> CancelarLiquidacion(TBCreditos.Liquidacion eOwner)
        {
            return dal.CancelarLiquidacion(eOwner);
        }

        public static List<TBCreditos.CreditoRelacionado> ObtenerCreditosRelacionados(int Accion, int idcliente)
        {
            return dal.ObtenerCreditosRelacionados(Accion, idcliente);
        }

        public static List<TBCreditos.CreditoRelacionado> ObtenerCreditosRelacionadosBySol(int Accion, int idsolicitud)
        {
            return dal.ObtenerCreditosRelacionadosBySol(Accion,idsolicitud);
        }

        public static void ActualizarCreditosRenovacion(int Accion, int idsolicitud, int idcuenta, decimal LiqCapital, decimal InteresDev, int idcliente)
        {
            dal.ActualizarCreditosRenovacion(Accion,idsolicitud,idcuenta,LiqCapital,InteresDev,idcliente);
        }

        public static Resultado<ObtenerSimulacionCreditoResponse> ObtenerSimulacionCredito(ObtenerSimulacionCreditoRequest entity)
        {
            Resultado<ObtenerSimulacionCreditoResponse> respuesta = new Resultado<ObtenerSimulacionCreditoResponse>();
            try
            {
                List<TBSolicitudes.DatosSolicitud> Datos = TBSolicitudesBll.ObtenerDatosSolicitud(entity.IdSolicitud);
                if (Datos.Count > 0)
                {
                    List<TBCATProducto> DatosProducto = TBCATProductoBll.ObtenerTBCATProducto(1, Datos[0].IDProducto, string.Empty, 1, 0, 0).ToList();
                    if (DatosProducto.Count <= 0)
                        throw new Exception("No se pudo recuperar la información del producto.");
                    //t.TipoReestructura == "Reprogramación" && t.Activo &&
                    var resTipoReestReprog = con.Solicitudes_TB_CATTipoReestructuras
                        .Where(t => t.Id_TipoReestructura == entity.IdTipoReestructura && t.TipoReestructura == "Reprogramación"
                        && t.Activo).ToList();
                    if (resTipoReestReprog.Count <= 0)
                    {
                        ObtenerProductoReestructuraResponse resProd = PeticionReestructuraBll.ObtenerProductoReestructura(
                            new ObtenerProductoReestructuraRequest() { Accion = "OBTENER_PRODUCTO_REESTRUCTURA_POR_IDSOLICITUD_PLAZO"
                                , IdSolicitud = entity.IdSolicitud, Plazo = entity.Plazo });
                        if(resProd != null && !resProd.Error && resProd.Resultado != null)
                        {
                            entity.IdProducto = resProd.Resultado.IdProducto;
                            entity.Interes = decimal.Round(resProd.Resultado.Tasa, 2, MidpointRounding.AwayFromZero);
                        }
                    }
                    else
                    {
                        entity.Interes = decimal.Round(DatosProducto.FirstOrDefault().Tasa, 2, MidpointRounding.AwayFromZero);
                        entity.IdProducto = DatosProducto[0].IdProducto;
                    }
                    entity.TipoSeguro = Datos[0].TipoSeguroDesgravamen;
                    switch (Datos[0].Banco.ToString().ToUpper())
                    {
                        case "CONTINENTAL":
                            entity.Banco = 1;
                            break;
                        case "INTERBANK":
                            entity.Banco = 2;
                            break;
                        default:
                            entity.Banco = 0;
                            break;
                    }
                    respuesta = dal.ObtenerSimulacionCredito(entity);
                }
                else
                {
                    throw new Exception("No se encontró información de la solicitud.");
                }
            }
            catch (Exception e)
            {
                respuesta.ResultObject = null;
                respuesta.Codigo = 1;
                respuesta.Mensaje = "Ocurrió un error al obtener los datos de solicitud. Error: " + e.Message ?? "no controlado.";
                Utils.EscribirLog($"{e.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(null)}", JsonConvert.SerializeObject(e));
            }
            return respuesta;
        }

        public static Resultado<List<ConsultaExpediente>> ConsultaExpediente(string Busqueda)
        {
            Resultado<List<ConsultaExpediente>> respuesta = new Resultado<List<ConsultaExpediente>>();

            try
            {
                return dal.ConsultaExpediente(Busqueda);
            }
            catch (Exception e)
            {
                respuesta.ResultObject = null;
                respuesta.Codigo = 1;
                respuesta.Mensaje = "Ocurrió un error al obtener los datos de solicitud. Error: " + e.Message ?? "no controlado.";
                Utils.EscribirLog($"{e.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(null)}", JsonConvert.SerializeObject(e));
            }

            return respuesta;
        }

        public static TipoCreditoCliente ObtenerTipoCreditoCliente(int idCliente)
        {
            return dal.ObtenerTipoCreditoCliente(idCliente);
        }
    }
}
