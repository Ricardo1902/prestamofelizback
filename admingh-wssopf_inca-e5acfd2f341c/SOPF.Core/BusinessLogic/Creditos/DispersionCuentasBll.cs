﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.DataAccess.Creditos;
using SOPF.Core.Entities.Creditos;


namespace SOPF.Core.BusinessLogic.Creditos
{
    public class DispersionCuentasBll
    {
        private static CuentasDispersionDal dal;

        static DispersionCuentasBll()
        {
            dal = new CuentasDispersionDal();
        }

        public static List<CuentasDispersion> ObtenerCuentas(int Accion, int IdBanco, int idCorte)
        {
            return dal.CuentasDispersion(Accion, IdBanco, idCorte);
        }

    }
}
