#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Menu;
using SOPF.Core.DataAccess.Menu;

# region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATMenuBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATMenuDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Menu
{
    public static class TBCATMenuBll
    {
        private static TBCATMenuDal dal;

        static TBCATMenuBll()
        {
            dal = new TBCATMenuDal();
        }

        public static void InsertarTBCATMenu(TBCATMenu entidad)
        {
            dal.InsertarTBCATMenu(entidad);			
        }

        public static List<TBCATMenu> ObtenerTBCATMenu(int accion, int idTipoUsuario, int idMenu, int nivel, int idModulo, int tipo)
        {
            
            return dal.ObtenerTBCATMenu(accion,idTipoUsuario,idMenu,nivel,idModulo,tipo);
        }
        
        public static void ActualizarTBCATMenu()
        {
            dal.ActualizarTBCATMenu();
        }

        public static void EliminarTBCATMenu()
        {
            dal.EliminarTBCATMenu();
        }
    }
}