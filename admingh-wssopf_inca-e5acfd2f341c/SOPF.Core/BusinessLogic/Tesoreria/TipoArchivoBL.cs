﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Tesoreria;
using SOPF.Core.DataAccess.Tesoreria;

namespace SOPF.Core.BusinessLogic.Tesoreria
{
    public static class TipoArchivoBL
    {
        private static TipoArchivoDAL dal;

        static TipoArchivoBL()
        {
            dal = new TipoArchivoDAL();
        }

        public static List<TipoArchivo> Filtrar(TipoArchivo Entity)
        {
            List<TipoArchivo> objReturn = new List<TipoArchivo>();

            objReturn = dal.Filtrar(Entity);

            return objReturn;
        }

        public static List<TipoArchivo.LayoutArchivo> ObtenerLayoutPagosDirectos()
        {
            TipoArchivo tipoPagosDirectos = dal.Filtrar(new TipoArchivo() { Descripcion = "CARGA PAGOS DIRECTOS" }).FirstOrDefault();

            return dal.ObtenerLayoutArchivo(tipoPagosDirectos);
        }
    }
}
