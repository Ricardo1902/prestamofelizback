#pragma warning disable 141124
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.Configuration;
using System.Threading;
using SOPF.Core.Entities.Tesoreria;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Tesoreria;
using SOPF.Core.DataAccess.Creditos;
using SOPF.Core.DataAccess.CobranzaAdministrativa;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatArchivosBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase CatArchivosDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-11-24 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Tesoreria
{
    public static class CatArchivosBll
    {
        private static CatArchivosDal dal;
        private static ArchivosDispersionDal dalA;
        private static CuentasDomiciliacionDal dalDom;
        private static GpoEnvio_MoraDal dalEnMo;
        private static MoraConfgDal dalConfig;

        static CatArchivosBll()
        {
            dal = new CatArchivosDal();
            dalA = new ArchivosDispersionDal();
            dalDom = new CuentasDomiciliacionDal();
            dalEnMo = new GpoEnvio_MoraDal();
            dalConfig = new MoraConfgDal();
        }

        public static void InsertarCatArchivos(CatArchivos entidad)
        {
            dal.InsertarCatArchivos(entidad);
        }

        public static CatArchivos ObtenerCatArchivos(int idBanco, string tipoTran, string tipo)
        {
            return dal.ObtenerCatArchivos(idBanco, tipoTran, tipo);
        }

        public static void ActualizarCatArchivos()
        {
            dal.ActualizarCatArchivos();
        }

        public static void EliminarCatArchivos()
        {
            dal.EliminarCatArchivos();
        }

        public static string GeneraArchivoAfiliacion(int idBanco)
        {
            string Mensaje = "";
            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutSDet = new List<CatArchivosDetalle>();
            List<ClientesAfiliacion> cuentasAfiliar = new List<ClientesAfiliacion>();

            LayoutGral = ObtenerCatArchivos(idBanco, "A", "S");
            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");
            LayoutSDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "S");

            cuentasAfiliar = Creditos.ClientesAfiliacionBll.ObtenerCuentas(1);
            List<string> lineas = new List<string>();

            string linea = "";
            string campo = "";
            string Relleno = "";
            int longitud = 0;
            int longitudCampo = 0;

            foreach (ClientesAfiliacion cuenta in cuentasAfiliar)
            {
                linea = String.Empty;
                foreach (CatArchivosDetalle detalle in LayoutDet)
                {

                    switch (detalle.IdCampo)
                    {
                        case 0:
                            campo = detalle.TextiFijo;
                            break;
                        case 1:
                            campo = cuenta.ClaveId;
                            break;
                        case 3:
                            campo = cuenta.Nombre;
                            break;
                        case 4:
                            campo = cuenta.RFC;
                            break;
                        case 5:
                            campo = cuenta.Telefono;
                            break;

                    }
                    Relleno = detalle.Relleno;
                    longitud = detalle.Longitud;

                    if (campo != null)
                    {
                        longitudCampo = campo.Length;
                    }
                    else
                    {
                        longitudCampo = 0;
                    }

                    if (detalle.IdTipoCampo == "N")
                    {
                        campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                        if (detalle.Incluyepunto == false)
                        {
                            campo.Replace(".", "");

                        }
                        if (detalle.Alineacion == "I")
                        {
                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                            {
                                campo = campo + Relleno;
                            }
                        }
                        else
                        {
                            for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                            {
                                campo = Relleno + campo;
                            }
                        }
                    }
                    if (detalle.IdTipoCampo == "T")
                    {


                        if (detalle.Alineacion == "I")
                        {
                            if (detalle.Longitud > 0)
                            {

                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                        }
                        else
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                    }
                    campo = String.Format("{0}\t", campo);
                    linea = linea + campo;




                }
                linea = linea.Substring(0, linea.Length - 1);
                lineas.Add(linea);
                linea = String.Empty;
                foreach (CatArchivosDetalle detalle in LayoutSDet)
                {

                    switch (detalle.IdCampo)
                    {
                        case 0:
                            campo = detalle.TextiFijo;
                            break;
                        case 1:
                            campo = cuenta.ClaveId;
                            break;
                        case 6:
                            campo = cuenta.TipoCuenta;
                            break;
                        case 7:
                            campo = cuenta.Moneda;
                            break;
                        case 8:
                            campo = cuenta.Banco;
                            break;
                        case 9:
                            campo = cuenta.Clabe;
                            break;

                    }
                    Relleno = detalle.Relleno;

                    if (detalle.IdCampo == 9 && cuenta.TipoCuenta == "001")
                    {
                        detalle.Longitud = 10;
                    }

                    longitud = detalle.Longitud;

                    if (campo != null)
                    {


                        longitudCampo = campo.Length;

                    }
                    else
                    {
                        longitudCampo = 0;
                    }
                    //asterisco para RFC
                    if (detalle.IdTipoCampo == "N")
                    {
                        campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                        if (detalle.Incluyepunto == false)
                        {
                            campo.Replace(".", "");

                        }
                        if (detalle.Alineacion == "I")
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                        }
                        else
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                    }
                    if (detalle.IdTipoCampo == "T")
                    {


                        if (detalle.Alineacion == "I")
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                        }
                        else
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                    }
                    campo = String.Format("{0}\t", campo);
                    linea = linea + campo;




                }
                linea = linea.Substring(0, linea.Length - 1);
                lineas.Add(linea);
            }
            System.IO.File.WriteAllLines(@"F:\AfiliacionBanorte_SOPF\" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);




            MailMessage mail = new MailMessage();
            SmtpClient client = new SmtpClient("smtpout.secureserver.net");
            client.Port = 3535;
            client.EnableSsl = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("notificaciones@prestamofeliz.com.mx", "pf2015");
            mail.From = new MailAddress("notificaciones@prestamofeliz.com.mx");
            mail.To.Add(new MailAddress("stefany.leyva@prestamofeliz.com.mx"));
            mail.CC.Add(new MailAddress("arelis@prestamofeliz.com.mx"));
            mail.Subject = "Archivo de Afiliacion.";
            mail.Body = "Archivo de Afiliacion";

            Attachment Data = new Attachment(@"F:\AfiliacionBanorte_SOPF\" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
            mail.Attachments.Add(Data);

            client.Send(mail);
            //GeneraArchivoAfiliacionRec(idBanco);
            return Mensaje;
        }
        public static string GeneraArchivoAfiliacionBancomer(int idBanco)
        {
            string Mensaje = "";
            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutSDet = new List<CatArchivosDetalle>();
            List<ClientesAfiliacion> cuentasAfiliar = new List<ClientesAfiliacion>();

            LayoutGral = ObtenerCatArchivos(idBanco, "A", "S");
            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");
            LayoutSDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "S");

            cuentasAfiliar = Creditos.ClientesAfiliacionBll.ObtenerCuentasBancomer(1);
            List<string> lineas = new List<string>();

            string linea = "";
            string campo = "";
            string Relleno = "";
            int longitud = 0;
            int longitudCampo = 0;

            foreach (ClientesAfiliacion cuenta in cuentasAfiliar)
            {
                linea = String.Empty;
                foreach (CatArchivosDetalle detalle in LayoutDet)
                {

                    switch (detalle.IdCampo)
                    {
                        case 0:
                            campo = detalle.TextiFijo;
                            break;
                        case 1:
                            campo = cuenta.ClaveId;
                            break;
                        case 3:
                            campo = cuenta.Nombre;
                            break;
                        case 4:
                            campo = cuenta.RFC;
                            break;
                        case 5:
                            campo = cuenta.Telefono;
                            break;
                        case 6:
                            campo = cuenta.TipoCuenta;
                            break;
                        case 7:
                            campo = cuenta.Moneda;
                            break;
                        case 8:
                            campo = cuenta.Banco;
                            break;
                        case 9:
                            campo = cuenta.Clabe;
                            break;
                        
                    }
                    Relleno = detalle.Relleno;
                    longitud = detalle.Longitud;

                    if (campo != null)
                    {
                        longitudCampo = campo.Length;
                    }
                    else
                    {
                        longitudCampo = 0;
                    }

                    if (detalle.IdTipoCampo == "N")
                    {
                        campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                        if (detalle.Incluyepunto == false)
                        {
                            campo.Replace(".", "");

                        }
                        if (detalle.Alineacion == "I")
                        {
                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                            {
                                campo = campo + Relleno;
                            }
                        }
                        else
                        {
                            for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                            {
                                campo = Relleno + campo;
                            }
                        }
                    }
                    if (detalle.IdTipoCampo == "T")
                    {


                        if (detalle.Alineacion == "I")
                        {
                            if (detalle.Longitud > 0)
                            {

                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                        }
                        else
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                    }
                    //campo = String.Format("{0}\t", campo);
                    linea = linea + campo;




                }
               
            
                linea = linea.Substring(0, linea.Length - 1);
                lineas.Add(linea);
            }
            System.IO.File.WriteAllLines(@"F:\AfiliacionBancomer_SOPF\" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);




            MailMessage mail = new MailMessage();
            SmtpClient client = new SmtpClient("smtpout.secureserver.net");
            client.Port = 3535;
            client.EnableSsl = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("notificaciones@prestamofeliz.com.mx", "pf2015");
            mail.From = new MailAddress("notificaciones@prestamofeliz.com.mx");
            mail.To.Add(new MailAddress("stefany.leyva@prestamofeliz.com.mx"));
            mail.CC.Add(new MailAddress("arelis@prestamofeliz.com.mx"));
            mail.Subject = "Archivo de Afiliacion.";
            mail.Body = "Archivo de Afiliacion";

            Attachment Data = new Attachment(@"C:\AfiliacionBancomer_SOPF\" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
            mail.Attachments.Add(Data);

            client.Send(mail);
            //GeneraArchivoAfiliacionRec(idBanco);
            return Mensaje;
        }

        public static string GeneraArchivoAfiliacionRec(int idBanco)
        {
            string Mensaje = "";
            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutSDet = new List<CatArchivosDetalle>();
            List<ClientesAfiliacion> cuentasAfiliar = new List<ClientesAfiliacion>();

            LayoutGral = ObtenerCatArchivos(idBanco, "A", "S");
            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");
            LayoutSDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "S");

            cuentasAfiliar = Creditos.ClientesAfiliacionBll.ObtenerCuentasRec(1);
            List<string> lineas = new List<string>();

            string linea = "";
            string campo = "";
            string Relleno = "";
            int longitud = 0;
            int longitudCampo = 0;

            if (cuentasAfiliar.Count > 0)
            {

                foreach (ClientesAfiliacion cuenta in cuentasAfiliar)
                {
                    linea = String.Empty;
                    foreach (CatArchivosDetalle detalle in LayoutDet)
                    {

                        switch (detalle.IdCampo)
                        {
                            case 0:
                                campo = detalle.TextiFijo;
                                break;
                            case 1:
                                campo = cuenta.ClaveId;
                                break;
                            case 3:
                                campo = cuenta.Nombre;
                                break;
                            case 4:
                                campo = cuenta.RFC;
                                break;
                            case 5:
                                campo = cuenta.Telefono;
                                break;

                        }
                        Relleno = detalle.Relleno;
                        longitud = detalle.Longitud;

                        if (campo != null)
                        {
                            longitudCampo = campo.Length;
                        }
                        else
                        {
                            longitudCampo = 0;
                        }

                        if (detalle.IdTipoCampo == "N")
                        {
                            campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                            if (detalle.Incluyepunto == false)
                            {
                                campo.Replace(".", "");

                            }
                            if (detalle.Alineacion == "I")
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                            else
                            {
                                for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                        if (detalle.IdTipoCampo == "T")
                        {


                            if (detalle.Alineacion == "I")
                            {
                                if (detalle.Longitud > 0)
                                {

                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                            }
                            else
                            {
                                if (detalle.Longitud > 0)
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                        }
                        campo = String.Format("{0}\t", campo);
                        linea = linea + campo;




                    }
                    linea = linea.Substring(0, linea.Length - 1);
                    lineas.Add(linea);
                    linea = String.Empty;
                    foreach (CatArchivosDetalle detalle in LayoutSDet)
                    {

                        switch (detalle.IdCampo)
                        {
                            case 0:
                                campo = detalle.TextiFijo;
                                break;
                            case 1:
                                campo = cuenta.ClaveId;
                                break;
                            case 6:
                                campo = cuenta.TipoCuenta;
                                break;
                            case 7:
                                campo = cuenta.Moneda;
                                break;
                            case 8:
                                campo = cuenta.Banco;
                                break;
                            case 9:
                                campo = cuenta.Clabe;
                                break;

                        }
                        Relleno = detalle.Relleno;

                        if (detalle.IdCampo == 9 && cuenta.TipoCuenta == "001")
                        {
                            detalle.Longitud = 10;
                        }

                        longitud = detalle.Longitud;

                        if (campo != null)
                        {


                            longitudCampo = campo.Length;

                        }
                        else
                        {
                            longitudCampo = 0;
                        }

                        if (detalle.IdTipoCampo == "N")
                        {
                            campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                            if (detalle.Incluyepunto == false)
                            {
                                campo.Replace(".", "");

                            }
                            if (detalle.Alineacion == "I")
                            {
                                if (detalle.Longitud > 0)
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                            }
                            else
                            {
                                if (detalle.Longitud > 0)
                                {
                                    for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                        }

                        if (detalle.IdTipoCampo == "T")
                        {


                            if (detalle.Alineacion == "I")
                            {
                                if (detalle.Longitud > 0)
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                            }
                            else
                            {
                                if (detalle.Longitud > 0)
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    { 

                                        campo = Relleno + campo;
                                    }
                                }
                            }
                        }
                        campo = String.Format("{0}\t", campo);
                        linea = linea + campo;




                    }
                    linea = linea.Substring(0, linea.Length - 1);
                    lineas.Add(linea);
                }
                System.IO.File.WriteAllLines(@"F:\AfiliacionBanorte_SOPF\" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + "rec.txt", lineas);




                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient("smtpout.secureserver.net");
                client.Port = 3535;
                client.EnableSsl = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("notificaciones@prestamofeliz.com.mx", "pf2015");
                mail.From = new MailAddress("notificaciones@prestamofeliz.com.mx");
                mail.To.Add(new MailAddress("afiliaciones@pulsusempresariales.com"));
                mail.CC.Add(new MailAddress("adriana.martinez@prestamofeliz.com.mx"));
                mail.Subject = "Archivo de Afiliacion Recomienda y Gana.";
                mail.Body = "Archivo de Afiliacion Recomienda y Gana";

                Attachment Data = new Attachment(@"F:\AfiliacionBanorte_SOPF\" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + "rec.txt");
                mail.Attachments.Add(Data);

                client.Send(mail);
                
            }
            return Mensaje;
            
        }

        public static string GeneraArchivoDispersion(int Accion, int idBanco, int idCorte)
        {
            string Mensaje = "";
            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutSDet = new List<CatArchivosDetalle>();
            List<CuentasDispersion> cuentasDispersar = new List<CuentasDispersion>();

            LayoutGral = ObtenerCatArchivos(idBanco, "T", "S");
            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");
            //LayoutSDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "S");

            cuentasDispersar = Creditos.DispersionCuentasBll.ObtenerCuentas(Accion, idBanco, idCorte);
            List<string> lineas = new List<string>();

            string linea = "";
            string campo = "";
            string Relleno = "";
            int longitud = 0;
            int longitudCampo = 0;
            try
            {
                foreach (CuentasDispersion cuenta in cuentasDispersar)
                {
                    linea = String.Empty;
                    foreach (CatArchivosDetalle detalle in LayoutDet)
                    {

                        switch (detalle.IdCampo)
                        {
                            case 0:
                                campo = detalle.TextiFijo;
                                break;
                            case 1:
                                campo = cuenta.ClaveId;
                                break;
                            case 4:
                                campo = cuenta.RFC;
                                break;
                            case 10:
                                campo = cuenta.Operacion;
                                break;
                            case 11:
                                campo = cuenta.CuentaOrigen;
                                break;
                            case 12:
                                campo = cuenta.CuentaDestino;
                                break;
                            case 13:
                                campo = cuenta.MontoTransferencia.ToString();
                                break;
                            case 14:
                                campo = cuenta.Referencia;
                                break;
                            case 15:
                                campo = cuenta.Descripcion;
                                break;
                            case 16:
                                campo = cuenta.RFC;
                                break;
                            case 17:
                                campo = cuenta.iva.ToString();
                                break;
                            case 18:
                                campo = cuenta.fecha;
                                break;
                            case 19:
                                campo = cuenta.instruccion;
                                break;
                        }
                        Relleno = detalle.Relleno;


                        if (detalle.IdCampo == 12 && cuenta.Operacion == "02")
                        {
                            detalle.Longitud = 10;
                        }

                        longitud = detalle.Longitud;

                        if (campo != null)
                        {
                            longitudCampo = campo.Length;
                        }
                        else
                        {
                            longitudCampo = 0;
                        }

                        if (detalle.IdTipoCampo == "N")
                        {
                            campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                            if (detalle.Incluyepunto == false)
                            {
                                campo.Replace(".", "");
                                

                            }
                            
                            if (detalle.Alineacion == "I")
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                            else
                            {
                                for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                        if (detalle.IdTipoCampo == "T")
                        {


                            if (detalle.Alineacion == "I")
                            {
                                if (detalle.Longitud > 0)
                                {

                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                            }
                            else
                            {
                                if (detalle.Longitud > 0)
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                        }
                        campo = String.Format("{0}\t", campo);
                        linea = linea + campo;




                    }
                    linea = linea.Substring(0, linea.Length - 1);
                    lineas.Add(linea);
                    linea = String.Empty;


                }
                //System.IO.File.WriteAllLines(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                System.IO.File.WriteAllLines(@"F:\AfiliacionBanorte_SOPF\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                Mensaje = "1-Archivo Generado con Exito";

                ArchivosDispersion archivo = new ArchivosDispersion();

                archivo.NombreArchivo = "Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt";
                archivo.IdCorte = idCorte;
                archivo.Estatus = 0;

                dalA.InsertarArchivosDispersion(archivo);

                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient("smtpout.secureserver.net");
                client.Port = 3535;
                client.EnableSsl = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("notificaciones@prestamofeliz.com.mx", "pf2015");
                mail.From = new MailAddress("notificaciones@prestamofeliz.com.mx");
                mail.To.Add(new MailAddress("stefany.leyva@prestamofeliz.com.mx"));
                mail.CC.Add(new MailAddress("arelis@prestamofeliz.com.mx"));
                mail.Subject = "Archivo de Dispersion.";
                mail.Body = "Archivo de Dispersion";

                ////Attachment Data = new Attachment(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
                Attachment Data = new Attachment(@"F:\AfiliacionBanorte_SOPF\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
                mail.Attachments.Add(Data);

                client.Send(mail);
                //System.IO.File.Delete(@"F:\Users\Juan Carlos\Documents\DispersionBanorte\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");


                GeneraArchivoDispersionRecomienda(2, idBanco, idCorte);
                return Mensaje;
            }
            catch (Exception ex)
            {
                Mensaje = ex.Message;
                return Mensaje;

            }

        }

        public static void GeneraArchivoDispersionRecomienda(int Accion, int idBanco, int idCorte)
        {
            string Mensaje = "";
            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutSDet = new List<CatArchivosDetalle>();
            List<CuentasDispersion> cuentasDispersar = new List<CuentasDispersion>();

            LayoutGral = ObtenerCatArchivos(idBanco, "T", "S");
            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");
            //LayoutSDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "S");

            cuentasDispersar = Creditos.DispersionCuentasBll.ObtenerCuentas(Accion, idBanco, idCorte);
            List<string> lineas = new List<string>();

            string linea = "";
            string campo = "";
            string Relleno = "";
            int longitud = 0;
            int longitudCampo = 0;
            try
            {
                foreach (CuentasDispersion cuenta in cuentasDispersar)
                {
                    linea = String.Empty;
                    foreach (CatArchivosDetalle detalle in LayoutDet)
                    {

                        switch (detalle.IdCampo)
                        {
                            case 0:
                                campo = detalle.TextiFijo;
                                break;
                            case 1:
                                campo = cuenta.ClaveId;
                                break;
                            case 4:
                                campo = cuenta.RFC;
                                break;
                            case 10:
                                campo = cuenta.Operacion;
                                break;
                            case 11:
                                campo = cuenta.CuentaOrigen;
                                break;
                            case 12:
                                campo = cuenta.CuentaDestino;
                                break;
                            case 13:
                                campo = cuenta.MontoTransferencia.ToString();
                                break;
                            case 14:
                                campo = cuenta.Referencia;
                                break;
                            case 15:
                                campo = cuenta.Descripcion;
                                break;
                            case 16:
                                campo = cuenta.RFC;
                                break;
                            case 17:
                                campo = cuenta.iva.ToString();
                                break;
                            case 18:
                                campo = cuenta.fecha;
                                break;
                            case 19:
                                campo = cuenta.instruccion;
                                break;
                        }
                        Relleno = detalle.Relleno;


                        if (detalle.IdCampo == 12 && cuenta.Operacion == "02")
                        {
                            detalle.Longitud = 10;
                        }

                        longitud = detalle.Longitud;

                        if (campo != null)
                        {
                            longitudCampo = campo.Length;
                        }
                        else
                        {
                            longitudCampo = 0;
                        }

                        if (detalle.IdTipoCampo == "N")
                        {
                            campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                            if (detalle.Incluyepunto == false)
                            {
                                campo.Replace(".", "");

                            }
                            if (detalle.Alineacion == "I")
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                            else
                            {
                                for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                        if (detalle.IdTipoCampo == "T")
                        {


                            if (detalle.Alineacion == "I")
                            {
                                if (detalle.Longitud > 0)
                                {

                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                            }
                            else
                            {
                                if (detalle.Longitud > 0)
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                        }
                        campo = String.Format("{0}\t", campo);
                        linea = linea + campo;




                    }
                    linea = linea.Substring(0, linea.Length - 1);
                    lineas.Add(linea);
                    linea = String.Empty;


                }
                //System.IO.File.WriteAllLines(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                System.IO.File.WriteAllLines(@"F:\AfiliacionBanorte_SOPF\DispersionRec" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                Mensaje = "1-Archivo Generado con Exito";

                ArchivosDispersion archivo = new ArchivosDispersion();

                archivo.NombreArchivo = "Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt";
                archivo.IdCorte = idCorte;
                archivo.Estatus = 0;

                dalA.InsertarArchivosDispersion(archivo);

                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient("smtpout.secureserver.net");
                client.Port = 3535;
                client.EnableSsl = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("notificaciones@prestamofeliz.com.mx", "pf2015");
                mail.From = new MailAddress("notificaciones@prestamofeliz.com.mx");
                mail.To.Add(new MailAddress("gabriela@pulsusempresariales.com"));
                mail.CC.Add(new MailAddress("adriana.martinez@prestamofeliz.com.mx"));
                mail.Subject = "Archivo de Dispersion.";
                mail.Body = "Archivo de Dispersion";

                ////Attachment Data = new Attachment(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
                Attachment Data = new Attachment(@"F:\AfiliacionBanorte_SOPF\DispersionRec" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
                mail.Attachments.Add(Data);

                client.Send(mail);
                //System.IO.File.Delete(@"F:\Users\Juan Carlos\Documents\DispersionBanorte\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");



                
            }
            catch (Exception ex)
            {
                Mensaje = ex.Message;
                

            }

        }

        public static string GenerarLayOutBancomer(CatArchivos LayoutGral, List<CatArchivosDetalle> LayoutDet, List<CatArchivosDetalle> LayoutSDet, List<CatArchivosDetalle> LayoutHeader, List<CuentasDomiciliacion.Bancomer> cuentasDispersar, List<CuentasDomiciliacion.Bancomer> cuentaOk, List<GpoEnvio_Mora> GpoEnvio_Mora, List<MoraConfig> MorConfig, int Archivo, string consecutivoArchivo)
        {
            string Mensaje = "";
            List<string> lineas = new List<string>();

            string linea = "";
            string campo = "";
            string Relleno = "";
            int longitud = 0;
            int longitudCampo = 0;
            int Registros = 0;
            decimal ImporteTotal = 0;
            try
            {

                linea = String.Empty;
                foreach (CatArchivosDetalle detalle in LayoutHeader)
                {

                    switch (detalle.IdCampo)
                    {
                        case 0:
                            campo = detalle.TextiFijo;
                            break;
                        case 20:
                            campo = cuentaOk[0].FechaPresentacion;
                            break;
                        case 34:
                            campo = cuentaOk[0].NumeroBloque;
                            break;

                    }
                    Relleno = detalle.Relleno;

                    longitud = detalle.Longitud;

                    if (campo != null)
                    {
                        longitudCampo = campo.Length;
                    }
                    else
                    {
                        longitudCampo = 0;
                    }

                    if (detalle.IdTipoCampo == "N")
                    {
                        campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                        if (detalle.Incluyepunto == false)
                        {
                            campo.Replace(".", "");

                        }
                        if (detalle.Alineacion == "I")
                        {
                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                            {
                                campo = campo + Relleno;
                            }
                        }
                        else
                        {
                            for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                            {
                                campo = Relleno + campo;
                            }
                        }
                    }
                    if (detalle.IdTipoCampo == "T")
                    {


                        if (detalle.Alineacion == "I")
                        {
                            if (detalle.Longitud > 0)
                            {

                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                        }
                        else
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                    }
                    //campo = String.Format("{0}\t", campo);
                    linea = linea + campo;




                }
                linea = linea.Substring(0, linea.Length - 1);
                lineas.Add(linea);
                linea = String.Empty;
                int continuacion = 0;

                continuacion = cuentaOk.Count + 1;
                int numerosucuencia = 2;
                foreach (CuentasDomiciliacion.Bancomer cuenta in cuentaOk)
                {
                    linea = String.Empty;

                    foreach (CatArchivosDetalle detalle in LayoutDet)
                    {
                        switch (detalle.IdCampo)
                        {
                            case 0:
                                campo = detalle.TextiFijo;
                                break;
                            case 29:


                                campo = Convert.ToString(numerosucuencia);
                                Registros = numerosucuencia;
                                numerosucuencia = numerosucuencia + 1;

                                break;
                            case 30:
                                campo = cuenta.Importe.ToString();
                                ImporteTotal = ImporteTotal + cuenta.Importe;
                                break;
                            case 31:
                                campo = cuenta.FechaLiquidacion;
                                break;
                            case 32:
                                campo = cuenta.FechaVencimiento;
                                break;
                            case 8:
                                campo = cuenta.Banco;
                                break;
                            case 6:
                                campo = cuenta.TipoCuenta;
                                break;
                            case 9:
                                campo = cuenta.Clabe;
                                break;
                            case 3:
                                campo = cuenta.Cliente;
                                break;
                            case 14:
                                campo = cuenta.referencia.ToString();
                                break;
                            case 33:
                                campo = cuenta.referencianum.ToString();
                                break;

                        }
                        Relleno = detalle.Relleno;

                        longitud = detalle.Longitud;

                        if (campo != null)
                        {
                            longitudCampo = campo.Length;
                        }
                        else
                        {
                            longitudCampo = 0;
                        }

                        if (detalle.IdTipoCampo == "N")
                        {
                            campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                            if (detalle.Incluyepunto == false)
                            {
                                campo = campo.Replace(".", "");
                                longitudCampo = campo.Length;

                            }
                            else
                            {
                                longitudCampo = campo.Length;
                            }
                            if (detalle.Alineacion == "I")
                            {

                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                            else
                            {
                                longitudCampo = longitudCampo - 2;
                                for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                        if (detalle.IdTipoCampo == "T")
                        {


                            if (detalle.Alineacion == "I")
                            {
                                if (detalle.Longitud > 0)
                                {

                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                            }
                            else
                            {
                                if (detalle.Longitud > 0)
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                        }
                        //campo = String.Format("{0}\t", campo);
                        linea = linea + campo;
                    }
                    linea = linea.Substring(0, linea.Length - 1);
                    lineas.Add(linea);
                    linea = String.Empty;
                }
                linea = String.Empty;
                foreach (CatArchivosDetalle detalle in LayoutSDet)
                {

                    switch (detalle.IdCampo)
                    {
                        case 0:
                            campo = detalle.TextiFijo;
                            break;
                        case 29:
                            campo = Convert.ToString(Registros + 1);
                            continuacion = Registros + 1;
                            break;
                        case 34:
                            campo = cuentaOk[0].NumeroBloque;
                            break;
                        case 31:
                            campo = Convert.ToString(Registros - 1);
                            break;
                        case 32:
                            campo = ImporteTotal.ToString();
                            break;

                    }
                    Relleno = detalle.Relleno;

                    longitud = detalle.Longitud;

                    if (campo != null)
                    {
                        longitudCampo = campo.Length;
                    }
                    else
                    {
                        longitudCampo = 0;
                    }

                    if (detalle.IdTipoCampo == "N")
                    {
                        campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                        if (detalle.Incluyepunto == false)
                        {
                            campo = campo.Replace(".", "");
                            longitudCampo = campo.Length;

                        }
                        else
                        {
                            longitudCampo = campo.Length;
                        }
                        if (detalle.Alineacion == "I")
                        {
                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                            {
                                campo = campo + Relleno;
                            }
                        }
                        else
                        {
                            longitudCampo = longitudCampo - 2;
                            for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                            {
                                campo = Relleno + campo;
                            }
                        }
                    }
                    if (detalle.IdTipoCampo == "T")
                    {


                        if (detalle.Alineacion == "I")
                        {
                            if (detalle.Longitud > 0)
                            {

                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                        }
                        else
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                    }
                    //campo = String.Format("{0}\t", campo);
                    linea = linea + campo;




                }
                linea = linea.Substring(0, linea.Length - 1);
                lineas.Add(linea);
                linea = String.Empty;

                //System.IO.File.WriteAllLines(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                //System.IO.File.WriteAllLines(@"C:\AfiliacionBanorte_SOPF\DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Archivo.ToString() + ".txt", lineas, Encoding.GetEncoding(1252));

                ArchivosDispersion archivo = new ArchivosDispersion();
                //archivo.NombreArchivo = "DomiciliacionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Archivo.ToString() + ".txt";
                //archivo.NombreArchivo = "DomiciliacionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + consecutivoArchivo.ToString() + ".txt";

                string Anio = DateTime.Now.Year.ToString();
                string Mes = "0" + DateTime.Now.Month.ToString();
                string Dia = "0" + DateTime.Now.Day.ToString();
                archivo.NombreArchivo = "DomiciliacionBancomer" + Dia.Substring(Dia.Length - 2, 2) + Mes.Substring(Mes.Length - 2, 2) + Anio.Substring(2, 2) + consecutivoArchivo.ToString() + Archivo + ".txt";


                string RutaDomiciliacionBancomer = ConfigurationSettings.AppSettings["RutaDomiciliacionBancomer"].ToString();
                System.IO.File.WriteAllLines(@RutaDomiciliacionBancomer + archivo.NombreArchivo, lineas, Encoding.GetEncoding(1252));

                Mensaje = "1-Archivo Generado con Exito §" + archivo.NombreArchivo;



                string CorreoDom = ConfigurationSettings.AppSettings["CorreoDom"].ToString();

                if (CorreoDom == "1")
                {

                    string CorreoDomFrom = ConfigurationSettings.AppSettings["CorreoDomFrom"].ToString();
                    string CorreoDomTo = ConfigurationSettings.AppSettings["CorreoDomTo"].ToString();

                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient("smtpout.secureserver.net");
                client.Port = 3535;
                client.Timeout = 300000;
                client.EnableSsl = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(CorreoDomFrom, "pf2015");
                    mail.From = new MailAddress(CorreoDomFrom);
                    mail.To.Add(new MailAddress(CorreoDomTo));
                mail.Subject = "Archivo de Domiciliacion Bancomer";
                mail.Body = "Archivo de Domiciliacion Bancomer";

                //Attachment Data = new Attachment(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
                    //Attachment Data = new Attachment(@"C:\AfiliacionBanorte_SOPF\DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Archivo.ToString() + ".txt");
                    Attachment Data = new Attachment(@RutaDomiciliacionBancomer + archivo.NombreArchivo);
                mail.Attachments.Add(Data);

                client.Send(mail);
                }
                //System.IO.File.Delete(@"F:\Users\Juan Carlos\Documents\DispersionBanorte\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");

                return Mensaje;
            }
            catch (Exception ex)
            {
                Mensaje = ex.Message;
                return Mensaje;


            }
        }

        public static string GeneraArchivoDomiciliacionBancomer(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart, string idEmisora, string idConvenio, int AplicarGastos, int idPlantilla)
        {

            if(idPlantilla != 0){
                CatArchivosDetalleBll.GeneraMoraConfig(GpoEnvio, idPlantilla);
            }
            
            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutSDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutHeader = new List<CatArchivosDetalle>();
            List<CuentasDomiciliacion.Bancomer> cuentasDispersar = new List<CuentasDomiciliacion.Bancomer>();

            LayoutGral = ObtenerCatArchivos(2, "C", "S");

            string SeparadorRFC = "";
            LayoutHeader = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "H");

            switch (idEmisora)
            {
                case "T":
                    SeparadorRFC = " ";
                    break;
                case "PO":
                    SeparadorRFC = "-";
                    break;
                case "PA":
                    SeparadorRFC = "*";
                    break;
            }


            if (LayoutHeader[12].Descripcion == "RFC")
            {
                LayoutHeader[12].TextiFijo = LayoutHeader[12].TextiFijo.ToString().Replace(" ", SeparadorRFC);
            }

            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");
            LayoutSDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "T");
            List<CuentasDomiciliacion.Bancomer> cuentaOk = new List<CuentasDomiciliacion.Bancomer>();
            List<GpoEnvio_Mora> GpoEnvio_Mora = new List<Entities.CobranzaAdministrativa.GpoEnvio_Mora>();
            List<MoraConfig> MorConfig = new List<Entities.CobranzaAdministrativa.MoraConfig>();
            int TotalCuentas = 0;
            string Mensaje = "";
            int Archivo = 0;
            
            string consecutivoArchivo = ObtenerConsecutivo(2, "C", "S");

         
            int maxRegistrosArchivos = Convert.ToInt32(ConfigurationSettings.AppSettings["maxRegistrosArchivos"].ToString());

            string[] Particiones;
            if (TotalArchivos == 1)
            {

                GpoEnvio_Mora = dalEnMo.ObtenerGrupoEnvio_Mora(1, GpoEnvio);
                int contador = 1;
                int  consecutivoReg    = GpoEnvio_Mora[0].ConsecutivoReg;
                foreach (GpoEnvio_Mora GpoMora in GpoEnvio_Mora)
                {
                    MorConfig = dalConfig.ObtenerMoraConfig(1, GpoMora.IdGrupoMora);
                    
                    foreach (MoraConfig config in MorConfig)
                    {
                        cuentasDispersar = dalDom.CuentasDomiBancomer(GpoEnvio, DiasVenini, DiasVenFin, IdBanco, config.Porcentaje, config.NumAmorti, GpoMora.IdMora, config.monto, AplicarGastos, idEmisora);
                        foreach (CuentasDomiciliacion.Bancomer disp in cuentasDispersar)
                        {
                            TotalCuentas = TotalCuentas + 1;
                            disp.referencia = (disp.referencia.ToString().ToString() + disp.FechaPresentacion + disp.NumeroBloque.Substring(2, 5) + contador.ToString());
                            disp.referencianum = Convert.ToString(consecutivoReg + 1);
                            consecutivoReg = consecutivoReg + 1;
                            cuentaOk.Add(disp);
                            if (TotalCuentas == maxRegistrosArchivos)
                            {
                                Archivo = Archivo + 1;
                                Mensaje = GenerarLayOutBancomer(LayoutGral, LayoutDet, LayoutSDet, LayoutHeader, cuentasDispersar, cuentaOk, GpoEnvio_Mora, MorConfig, Archivo, consecutivoArchivo);
                                TotalCuentas = 0;
                                cuentaOk.Clear();
                                
                            }
                        }
                        contador = contador + 1;
                        
                    }

                }

                //TODO MSa: actualizar el consecutivo en tabla
                ABCCatParametro(1, "CONDOM", consecutivoReg.ToString(), 1);


                Archivo = Archivo + 1;
                Mensaje = GenerarLayOutBancomer(LayoutGral, LayoutDet, LayoutSDet, LayoutHeader, cuentasDispersar, cuentaOk, GpoEnvio_Mora, MorConfig, Archivo, consecutivoArchivo);
                return Mensaje;
               
            }
            else
            {
                Particiones = Porcent.Split(',');
                int contador = 0;
                for (int j = 0; j <= Particiones.Length - 1; j++)
                {
                    cuentasDispersar = dalDom.CuentasDomiBancomer(GpoEnvio, DiasVenini, DiasVenFin, IdBanco, Convert.ToDecimal(Particiones[j]) / 100, 0, 0, 300, AplicarGastos, idEmisora);
                    contador = contador + 1;
                    cuentaOk.Clear();

                    foreach (CuentasDomiciliacion.Bancomer disp in cuentasDispersar)
                    {
                        cuentaOk.Add(disp);
                    }

                    List<string> lineas = new List<string>();

                    string linea = "";
                    string campo = "";
                    string Relleno = "";
                    int longitud = 0;
                    int longitudCampo = 0;
                    int Registros = 0;
                    decimal ImporteTotal = 0;
                    try
                    {

                        linea = String.Empty;
                        foreach (CatArchivosDetalle detalle in LayoutHeader)
                        {

                            switch (detalle.IdCampo)
                            {
                                case 0:
                                    campo = detalle.TextiFijo;
                                    break;
                                case 20:
                                    campo = cuentaOk[0].FechaPresentacion;
                                    break;
                                case 34:
                                    campo = cuentaOk[0].NumeroBloque;
                                    break;

                            }
                            Relleno = detalle.Relleno;

                            longitud = detalle.Longitud;

                            if (campo != null)
                            {
                                longitudCampo = campo.Length;
                            }
                            else
                            {
                                longitudCampo = 0;
                            }

                            if (detalle.IdTipoCampo == "N")
                            {
                                campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                if (detalle.Incluyepunto == false)
                                {
                                    campo.Replace(".", "");

                                }
                                if (detalle.Alineacion == "I")
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                                else
                                {
                                    for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                            if (detalle.IdTipoCampo == "T")
                            {


                                if (detalle.Alineacion == "I")
                                {
                                    if (detalle.Longitud > 0)
                                    {

                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                }
                                else
                                {
                                    if (detalle.Longitud > 0)
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                            }
                            //campo = String.Format("{0}\t", campo);
                            linea = linea + campo;




                        }
                        linea = linea.Substring(0, linea.Length - 1);
                        lineas.Add(linea);
                        linea = String.Empty;
                        foreach (CuentasDomiciliacion.Bancomer cuenta in cuentaOk)
                        {
                            linea = String.Empty;
                            int numerosucuencia = 2;
                            foreach (CatArchivosDetalle detalle in LayoutDet)
                            {
                                switch (detalle.IdCampo)
                                {
                                    case 0:
                                        campo = detalle.TextiFijo;
                                        break;
                                    case 29:
                                        campo = Convert.ToString(numerosucuencia);
                                        Registros = numerosucuencia;
                                        numerosucuencia = numerosucuencia + 1;
                                        break;
                                    case 30:
                                        campo = cuenta.Importe.ToString();
                                        ImporteTotal = ImporteTotal + cuenta.Importe;
                                        break;
                                    case 31:
                                        campo = cuenta.FechaLiquidacion;
                                        break;
                                    case 32:
                                        campo = cuenta.FechaVencimiento;
                                        break;
                                    case 8:
                                        campo = cuenta.Banco;
                                        break;
                                    case 6:
                                        campo = cuenta.TipoCuenta;
                                        break;
                                    case 9:
                                        campo = cuenta.Clabe;
                                        break;
                                    case 3:
                                        campo = cuenta.Cliente;
                                        break;
                                    case 14:
                                        campo = cuenta.referencia.ToString() + contador.ToString();
                                        break;
                                    case 33:
                                        campo = cuenta.referencianum.ToString();
                                        break;

                                }
                                Relleno = detalle.Relleno;

                                longitud = detalle.Longitud;

                                if (campo != null)
                                {
                                    longitudCampo = campo.Length;
                                }
                                else
                                {
                                    longitudCampo = 0;
                                }

                                if (detalle.IdTipoCampo == "N")
                                {
                                    campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                    if (detalle.Incluyepunto == false)
                                    {
                                        campo = campo.Replace(".", "");

                                    }
                                    if (detalle.Alineacion == "I")
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                    else
                                    {
                                        longitudCampo = longitudCampo - 3;
                                        for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                                if (detalle.IdTipoCampo == "T")
                                {


                                    if (detalle.Alineacion == "I")
                                    {
                                        if (detalle.Longitud > 0)
                                        {

                                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                            {
                                                campo = campo + Relleno;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (detalle.Longitud > 0)
                                        {
                                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                            {
                                                campo = Relleno + campo;
                                            }
                                        }
                                    }
                                }
                                //campo = String.Format("{0}\t", campo);
                                linea = linea + campo;
                            }
                            linea = linea.Substring(0, linea.Length - 1);
                            lineas.Add(linea);
                            linea = String.Empty;
                        }
                        linea = String.Empty;
                        foreach (CatArchivosDetalle detalle in LayoutSDet)
                        {

                            switch (detalle.IdCampo)
                            {
                                case 0:
                                    campo = detalle.TextiFijo;
                                    break;
                                case 29:
                                    campo = Convert.ToString(Registros + 1);
                                    break;
                                case 34:
                                    campo = cuentaOk[0].NumeroBloque;
                                    break;
                                case 31:
                                    campo = Convert.ToString(Registros - 1);
                                    break;
                                case 32:
                                    campo = ImporteTotal.ToString();
                                    break;

                            }
                            Relleno = detalle.Relleno;

                            longitud = detalle.Longitud;

                            if (campo != null)
                            {
                                longitudCampo = campo.Length;
                            }
                            else
                            {
                                longitudCampo = 0;
                            }

                            if (detalle.IdTipoCampo == "N")
                            {
                                campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                if (detalle.Incluyepunto == false)
                                {
                                    campo = campo.Replace(".", "");

                                }
                                if (detalle.Alineacion == "I")
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                                else
                                {
                                    longitudCampo = longitudCampo - 3;
                                    for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                            if (detalle.IdTipoCampo == "T")
                            {


                                if (detalle.Alineacion == "I")
                                {
                                    if (detalle.Longitud > 0)
                                    {

                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                }
                                else
                                {
                                    if (detalle.Longitud > 0)
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                            }
                            //campo = String.Format("{0}\t", campo);
                            linea = linea + campo;




                        }
                        linea = linea.Substring(0, linea.Length - 1);
                        lineas.Add(linea);
                        linea = String.Empty;

                        //System.IO.File.WriteAllLines(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                        //System.IO.File.WriteAllLines(@"F:\AfiliacionBanorte_SOPF\DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Convert.ToString(j) + ".txt", lineas);



                        ArchivosDispersion archivo = new ArchivosDispersion();
                        //archivo.NombreArchivo = "DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Convert.ToString(j) + consecutivoArchivo + ".txt";


                        string Anio = DateTime.Now.Year.ToString();
                        string Mes = "0" + DateTime.Now.Month.ToString();
                        string Dia = "0" + DateTime.Now.Day.ToString();
                        archivo.NombreArchivo = "DomiciliacionBancomer" + Dia.Substring(Dia.Length - 2, 2) + Mes.Substring(Mes.Length - 2, 2) + Anio.Substring(2, 2) + consecutivoArchivo + ".txt";


                        string RutaDomiciliacionBancomer = ConfigurationSettings.AppSettings["RutaDomiciliacionBancomer"].ToString();
                        System.IO.File.WriteAllLines(@RutaDomiciliacionBancomer + archivo.NombreArchivo, lineas);

                        Mensaje = "1-Archivo Generado con Exito";

                        //archivo.NombreArchivo = "DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Convert.ToString(j) + ".txt";

                        string CorreoDomiciliacion = ConfigurationSettings.AppSettings["CorreoDom"].ToString();

                        if (CorreoDomiciliacion == "1")
                        {

                            string CorreoDomFrom = ConfigurationSettings.AppSettings["CorreoDomFrom"].ToString();
                            string CorreoDomTo = ConfigurationSettings.AppSettings["CorreoDomTo"].ToString();

                        MailMessage mail = new MailMessage();
                        SmtpClient client = new SmtpClient("smtpout.secureserver.net");
                        client.Port = 3535;
                        client.Timeout = 300000;
                        client.EnableSsl = false;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                            client.Credentials = new NetworkCredential(CorreoDomFrom, "pf2015");
                            mail.From = new MailAddress(CorreoDomFrom);
                            mail.To.Add(new MailAddress(CorreoDomTo));
                        mail.Subject = "Archivo de Domiciliacion Bancomer";
                        mail.Body = "Archivo de Domiciliacion Bancomer";


                            Attachment Data = new Attachment(@RutaDomiciliacionBancomer + archivo.NombreArchivo);
                        mail.Attachments.Add(Data);

                        client.Send(mail);
                        }

                        //System.IO.File.Delete(@"F:\Users\Juan Carlos\Documents\DispersionBanorte\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");

                        
                    }
                    catch (Exception ex)
                    {
                        Mensaje = ex.Message;
                        

                    }
                }
            }
            return Mensaje;
        }

        public static string GeneraArchivoDomiciliacionBancomerEspecifica(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart)
        {

            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutSDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutHeader = new List<CatArchivosDetalle>();
            List<CuentasDomiciliacion.Bancomer> cuentasDispersar = new List<CuentasDomiciliacion.Bancomer>();

            LayoutGral = ObtenerCatArchivos(2, "C", "S");
            LayoutHeader = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "H");
            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");
            LayoutSDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "T");
            List<CuentasDomiciliacion.Bancomer> cuentaOk = new List<CuentasDomiciliacion.Bancomer>();
            List<GpoEnvio_Mora> GpoEnvio_Mora = new List<Entities.CobranzaAdministrativa.GpoEnvio_Mora>();
            List<MoraConfig> MorConfig = new List<Entities.CobranzaAdministrativa.MoraConfig>();
            int TotalCuentas = 0;
            string Mensaje = "";
            int Archivo = 0;


            string consecutivoArchivo = ObtenerConsecutivo(2, "C", "S");


            string[] Particiones;
            if (TotalArchivos == 1)
            {

                GpoEnvio_Mora = dalEnMo.ObtenerGrupoEnvio_Mora(1, GpoEnvio);
                int contador = 1;
                int consecutivoReg = GpoEnvio_Mora[0].ConsecutivoReg;              
                    

                   
                        cuentasDispersar = dalDom.CuentasDomiBancomerEspecifica(GpoEnvio, DiasVenini, DiasVenFin, IdBanco,1, 1, 1, 50);
                        foreach (CuentasDomiciliacion.Bancomer disp in cuentasDispersar)
                        {
                            TotalCuentas = TotalCuentas + 1;
                            disp.referencia = (disp.referencia.ToString().ToString() + disp.FechaPresentacion + disp.NumeroBloque.Substring(2, 5) + contador.ToString());
                            disp.referencianum = Convert.ToString(consecutivoReg + 1);
                            consecutivoReg = consecutivoReg + 1;
                            cuentaOk.Add(disp);
                            if (TotalCuentas == 15000)
                            {
                                Archivo = Archivo + 1;
                                Mensaje = GenerarLayOutBancomer(LayoutGral, LayoutDet, LayoutSDet, LayoutHeader, cuentasDispersar, cuentaOk, GpoEnvio_Mora, MorConfig, Archivo, consecutivoArchivo);
                                TotalCuentas = 0;
                                cuentaOk.Clear();

                            }
                        }
                        contador = contador + 1;

                    

               

                Archivo = Archivo + 1;
                Mensaje = GenerarLayOutBancomer(LayoutGral, LayoutDet, LayoutSDet, LayoutHeader, cuentasDispersar, cuentaOk, GpoEnvio_Mora, MorConfig, Archivo, consecutivoArchivo);
                return Mensaje;

            }
            else
            {
                Particiones = Porcent.Split(',');
                int contador = 0;
                for (int j = 0; j <= Particiones.Length - 1; j++)
                {
                    cuentasDispersar = dalDom.CuentasDomiBancomer(GpoEnvio, DiasVenini, DiasVenFin, IdBanco, Convert.ToDecimal(Particiones[j]) / 100, 0, 0, 300, 0, "");
                    contador = contador + 1;
                    cuentaOk.Clear();

                    foreach (CuentasDomiciliacion.Bancomer disp in cuentasDispersar)
                    {
                        cuentaOk.Add(disp);
                    }

                    List<string> lineas = new List<string>();

                    string linea = "";
                    string campo = "";
                    string Relleno = "";
                    int longitud = 0;
                    int longitudCampo = 0;
                    int Registros = 0;
                    decimal ImporteTotal = 0;
                    try
                    {

                        linea = String.Empty;
                        foreach (CatArchivosDetalle detalle in LayoutHeader)
                        {

                            switch (detalle.IdCampo)
                            {
                                case 0:
                                    campo = detalle.TextiFijo;
                                    break;
                                case 20:
                                    campo = cuentaOk[0].FechaPresentacion;
                                    break;
                                case 34:
                                    campo = cuentaOk[0].NumeroBloque;
                                    break;

                            }
                            Relleno = detalle.Relleno;

                            longitud = detalle.Longitud;

                            if (campo != null)
                            {
                                longitudCampo = campo.Length;
                            }
                            else
                            {
                                longitudCampo = 0;
                            }

                            if (detalle.IdTipoCampo == "N")
                            {
                                campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                if (detalle.Incluyepunto == false)
                                {
                                    campo.Replace(".", "");

                                }
                                if (detalle.Alineacion == "I")
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                                else
                                {
                                    for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                            if (detalle.IdTipoCampo == "T")
                            {


                                if (detalle.Alineacion == "I")
                                {
                                    if (detalle.Longitud > 0)
                                    {

                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                }
                                else
                                {
                                    if (detalle.Longitud > 0)
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                            }
                            //campo = String.Format("{0}\t", campo);
                            linea = linea + campo;




                        }
                        linea = linea.Substring(0, linea.Length - 1);
                        lineas.Add(linea);
                        linea = String.Empty;
                        foreach (CuentasDomiciliacion.Bancomer cuenta in cuentaOk)
                        {
                            linea = String.Empty;
                            int numerosucuencia = 2;
                            foreach (CatArchivosDetalle detalle in LayoutDet)
                            {
                                switch (detalle.IdCampo)
                                {
                                    case 0:
                                        campo = detalle.TextiFijo;
                                        break;
                                    case 29:
                                        campo = Convert.ToString(numerosucuencia);
                                        Registros = numerosucuencia;
                                        numerosucuencia = numerosucuencia + 1;
                                        break;
                                    case 30:
                                        campo = cuenta.Importe.ToString();
                                        ImporteTotal = ImporteTotal + cuenta.Importe;
                                        break;
                                    case 31:
                                        campo = cuenta.FechaLiquidacion;
                                        break;
                                    case 32:
                                        campo = cuenta.FechaVencimiento;
                                        break;
                                    case 8:
                                        campo = cuenta.Banco;
                                        break;
                                    case 6:
                                        campo = cuenta.TipoCuenta;
                                        break;
                                    case 9:
                                        campo = cuenta.Clabe;
                                        break;
                                    case 3:
                                        campo = cuenta.Cliente;
                                        break;
                                    case 14:
                                        campo = cuenta.referencia.ToString() + contador.ToString();
                                        break;
                                    case 33:
                                        campo = cuenta.referencianum.ToString();
                                        break;

                                }
                                Relleno = detalle.Relleno;

                                longitud = detalle.Longitud;

                                if (campo != null)
                                {
                                    longitudCampo = campo.Length;
                                }
                                else
                                {
                                    longitudCampo = 0;
                                }

                                if (detalle.IdTipoCampo == "N")
                                {
                                    campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                    if (detalle.Incluyepunto == false)
                                    {
                                        campo = campo.Replace(".", "");

                                    }
                                    if (detalle.Alineacion == "I")
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                    else
                                    {
                                        longitudCampo = longitudCampo - 3;
                                        for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                                if (detalle.IdTipoCampo == "T")
                                {


                                    if (detalle.Alineacion == "I")
                                    {
                                        if (detalle.Longitud > 0)
                                        {

                                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                            {
                                                campo = campo + Relleno;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (detalle.Longitud > 0)
                                        {
                                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                            {
                                                campo = Relleno + campo;
                                            }
                                        }
                                    }
                                }
                                //campo = String.Format("{0}\t", campo);
                                linea = linea + campo;
                            }
                            linea = linea.Substring(0, linea.Length - 1);
                            lineas.Add(linea);
                            linea = String.Empty;
                        }
                        linea = String.Empty;
                        foreach (CatArchivosDetalle detalle in LayoutSDet)
                        {

                            switch (detalle.IdCampo)
                            {
                                case 0:
                                    campo = detalle.TextiFijo;
                                    break;
                                case 29:
                                    campo = Convert.ToString(Registros + 1);
                                    break;
                                case 34:
                                    campo = cuentaOk[0].NumeroBloque;
                                    break;
                                case 31:
                                    campo = Convert.ToString(Registros - 1);
                                    break;
                                case 32:
                                    campo = ImporteTotal.ToString();
                                    break;

                            }
                            Relleno = detalle.Relleno;

                            longitud = detalle.Longitud;

                            if (campo != null)
                            {
                                longitudCampo = campo.Length;
                            }
                            else
                            {
                                longitudCampo = 0;
                            }

                            if (detalle.IdTipoCampo == "N")
                            {
                                campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                if (detalle.Incluyepunto == false)
                                {
                                    campo = campo.Replace(".", "");

                                }
                                if (detalle.Alineacion == "I")
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                                else
                                {
                                    longitudCampo = longitudCampo - 3;
                                    for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                            if (detalle.IdTipoCampo == "T")
                            {


                                if (detalle.Alineacion == "I")
                                {
                                    if (detalle.Longitud > 0)
                                    {

                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                }
                                else
                                {
                                    if (detalle.Longitud > 0)
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                            }
                            //campo = String.Format("{0}\t", campo);
                            linea = linea + campo;




                        }
                        linea = linea.Substring(0, linea.Length - 1);
                        lineas.Add(linea);
                        linea = String.Empty;

                        //System.IO.File.WriteAllLines(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                        System.IO.File.WriteAllLines(@"F:\AfiliacionBanorte_SOPF\DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Convert.ToString(j) + ".txt", lineas);
                        Mensaje = "1-Archivo Generado con Exito";

                        ArchivosDispersion archivo = new ArchivosDispersion();

                        archivo.NombreArchivo = "DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Convert.ToString(j) + ".txt";




                        //MailMessage mail = new MailMessage();
                        //SmtpClient client = new SmtpClient("smtpout.secureserver.net");
                        //client.Port = 3535;
                        //client.EnableSsl = false;
                        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        //client.UseDefaultCredentials = false;
                        //client.Credentials = new NetworkCredential("notificaciones@prestamofeliz.com.mx", "pf2015");
                        //mail.From = new MailAddress("notificaciones@prestamofeliz.com.mx");
                        //mail.To.Add(new MailAddress("juan.garcia@prestamofeliz.com.mx"));
                        //mail.Subject = "Archivo de Domiciliacion Bancomer";
                        //mail.Body = "Archivo de Domiciliacion Bancomer";

                        ////Attachment Data = new Attachment(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
                        //Attachment Data = new Attachment(@"F:\AfiliacionBanorte_SOPF\DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Convert.ToString(j) + ".txt");
                        //mail.Attachments.Add(Data);

                        //client.Send(mail);
                        //System.IO.File.Delete(@"F:\Users\Juan Carlos\Documents\DispersionBanorte\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");


                    }
                    catch (Exception ex)
                    {
                        Mensaje = ex.Message;


                    }
                }
            }
            return Mensaje;
        }


        public static string GeneraArchivoDomiciliacionBanorte(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart, string idEmisora, string idConvenio, int AplicarGastos, int idPlantilla)
        {

            if (idPlantilla != 0)
            {
                CatArchivosDetalleBll.GeneraMoraConfig(GpoEnvio, idPlantilla);
            }

            string Mensaje = "";
            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutSDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutHeader = new List<CatArchivosDetalle>();
            List<CuentasDomiciliacion.Banorte> cuentasDispersar = new List<CuentasDomiciliacion.Banorte>();

            LayoutGral = ObtenerCatArchivos(16, "C", "S");
            LayoutHeader = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "H");
            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");
            LayoutSDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "T");
            List<CuentasDomiciliacion.Banorte> cuentaOk = new List<CuentasDomiciliacion.Banorte>();
            List<GpoEnvio_Mora> GpoEnvio_Mora = new List<Entities.CobranzaAdministrativa.GpoEnvio_Mora>();
            List<MoraConfig> MorConfig = new List<Entities.CobranzaAdministrativa.MoraConfig>();

            string consecutivoArchivo = ObtenerConsecutivo(16, "C", "S");





            string[] Particiones;
            if (TotalArchivos == 1)
            {

                GpoEnvio_Mora = dalEnMo.ObtenerGrupoEnvio_Mora(1, GpoEnvio);
                int contador = 1;
                decimal ImporteTotales= 0;
                foreach (GpoEnvio_Mora GpoMora in GpoEnvio_Mora)
                {
                    MorConfig = dalConfig.ObtenerMoraConfig(1, GpoMora.IdGrupoMora);

                    foreach (MoraConfig config in MorConfig)
                    {
                        cuentasDispersar = dalDom.CuentasDomiBanorte(GpoEnvio, DiasVenini, DiasVenFin, IdBanco, config.Porcentaje, config.NumAmorti, GpoMora.IdMora, config.monto, idEmisora, AplicarGastos);
                        foreach (CuentasDomiciliacion.Banorte disp in cuentasDispersar)
                        {
                           // disp.Referencia = Convert.ToInt32(disp.Referencia.ToString().ToString() + contador.ToString());
                            ImporteTotales = ImporteTotales + disp.Importe;
                            cuentaOk.Add(disp);
                        }
                        contador = contador + 1;
                    }

                }



                List<string> lineas = new List<string>();

                string linea = "";
                string campo = "";
                string Relleno = "";
                int longitud = 0;
                int longitudCampo = 0;
                int Registros = 0;
                decimal ImporteTotal = 0;
                try
                {

                    linea = String.Empty;
                    foreach (CatArchivosDetalle detalle in LayoutHeader)
                    {

                        switch (detalle.IdCampo)
                        {
                            case 0:
                                campo = detalle.TextiFijo;
                                break;
                            case 35:
                                campo = cuentaOk[0].Emisora;
                                break;
                            case 20:
                                campo = cuentaOk[0].FechaProceso;
                                break;
                            case 30:
                                campo = cuentaOk[0].consecutivo.ToString();
                                break;
                            case 31:
                                campo = cuentaOk.Count.ToString();
                                break;
                            case 32:
                                campo = ImporteTotales.ToString();
                                break;                            
                          

                        }
                        Relleno = detalle.Relleno;

                        longitud = detalle.Longitud;

                        if (campo != null)
                        {
                            longitudCampo = campo.Length;
                        }
                        else
                        {
                            longitudCampo = 0;
                        }

                        if (detalle.IdTipoCampo == "N")
                        {
                            campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                            if (detalle.Incluyepunto == false)
                            {
                                campo = campo.Replace(".", "");
                                longitudCampo = campo.Length;

                            }
                            else
                            {
                                longitudCampo = campo.Length;
                            }
                            if (detalle.Alineacion == "I")
                            {

                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                            else
                            {
                                longitudCampo = longitudCampo - 2;
                                for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                        if (detalle.IdTipoCampo == "T")
                        {


                            if (detalle.Alineacion == "I")
                            {
                                if (detalle.Longitud > 0)
                                {

                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                            }
                            else
                            {
                                if (detalle.Longitud > 0)
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                        }
                        //campo = String.Format("{0}\t", campo);
                        linea = linea + campo;




                    }
                    linea = linea.Substring(0, linea.Length - 1);
                    lineas.Add(linea);
                    linea = String.Empty;
                    int continuacion = 0;

                    continuacion = cuentaOk.Count + 1;
                    int numerosucuencia = 2;
                    foreach (CuentasDomiciliacion.Banorte cuenta in cuentaOk)
                    {
                        linea = String.Empty;

                        foreach (CatArchivosDetalle detalle in LayoutDet)
                        {
                            switch (detalle.IdCampo)
                            {
                                case 0:
                                    campo = detalle.TextiFijo;
                                    break;
                                case 20:
                                    campo = cuenta.FechaAplicacion;
                                    break;
                                case 14:
                                    campo = cuenta.Referencia.ToString();
                                    break;
                                case 29:


                                    campo = Convert.ToString(numerosucuencia);
                                    Registros = numerosucuencia;
                                    numerosucuencia = numerosucuencia + 1;

                                    break;
                                case 30:
                                    campo = cuenta.Importe.ToString();
                                    ImporteTotal = ImporteTotal + cuenta.Importe;
                                    break;

                                case 8:
                                    campo = cuenta.Banco;
                                    break;
                                case 6:
                                    campo = cuenta.TipoCuenta;
                                    break;

                                case 9:
                                    campo = cuenta.Clabe;
                                    break;

                           

                            }
                            Relleno = detalle.Relleno;

                            longitud = detalle.Longitud;

                            if (campo != null)
                            {
                                longitudCampo = campo.Length;
                            }
                            else
                            {
                                longitudCampo = 0;
                            }

                            if (detalle.IdTipoCampo == "N")
                            {
                                campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                if (detalle.Incluyepunto == false)
                                {
                                    campo = campo.Replace(".", "");
                                    longitudCampo = campo.Length;

                                }
                                else
                                {
                                    longitudCampo = campo.Length;
                                }
                                if (detalle.Alineacion == "I")
                                {

                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                                else
                                {
                                    longitudCampo = longitudCampo - 2;
                                    for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                            if (detalle.IdTipoCampo == "T")
                            {


                                if (detalle.Alineacion == "I")
                                {
                                    if (detalle.Longitud > 0)
                                    {

                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                }
                                else
                                {
                                    if (detalle.Longitud > 0)
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                            }
                            //campo = String.Format("{0}\t", campo);
                            linea = linea + campo;
                        }
                        linea = linea.Substring(0, linea.Length - 1);
                        lineas.Add(linea);
                        linea = String.Empty;
                    }
                    
                    //linea = linea.Substring(0, linea.Length - 1);
                    //lineas.Add(linea);
                    //linea = String.Empty;

                    //System.IO.File.WriteAllLines(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                    //System.IO.File.WriteAllLines(@"F:\AfiliacionBanorte_SOPF\CI"+idEmisora+"01.PAG", lineas, Encoding.GetEncoding(1252));
                    string RutaDomiciliacionBanorte = ConfigurationSettings.AppSettings["RutaDomiciliacionBanorte"].ToString();

                    ArchivosDispersion archivo = new ArchivosDispersion();
                    archivo.NombreArchivo = "CI" + idConvenio + consecutivoArchivo + ".PAG";

                    System.IO.File.WriteAllLines(@RutaDomiciliacionBanorte + archivo.NombreArchivo, lineas, Encoding.GetEncoding(1252));
                    Mensaje = "1-Archivo Generado con Exito §" + archivo.NombreArchivo;


                    //archivo.NombreArchivo = "CI7096801.PAG";



                    string CorreoDom = ConfigurationSettings.AppSettings["CorreoDom"].ToString();

                    if (CorreoDom == "1")
                    {

                        string CorreoDomFrom = ConfigurationSettings.AppSettings["CorreoDomFrom"].ToString();
                        string CorreoDomTo = ConfigurationSettings.AppSettings["CorreoDomTo"].ToString();

                    MailMessage mail = new MailMessage();
                    SmtpClient client = new SmtpClient("smtpout.secureserver.net");
                    client.Port = 3535;
                    client.Timeout = 300000;
                    client.EnableSsl = false;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential(CorreoDomFrom, "pf2015");
                        mail.From = new MailAddress(CorreoDomFrom);
                        mail.To.Add(new MailAddress(CorreoDomTo));
                    mail.Subject = "Archivo de Domiciliacion Bancomer";
                    mail.Body = "Archivo de Domiciliacion Bancomer";

                    //Attachment Data = new Attachment(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
                        Attachment Data = new Attachment(@RutaDomiciliacionBanorte + archivo.NombreArchivo);
                    mail.Attachments.Add(Data);

                    client.Send(mail);
                    }
                    //System.IO.File.Delete(@"F:\Users\Juan Carlos\Documents\DispersionBanorte\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");


                }
                catch (Exception ex)
                {
                    Mensaje = ex.Message;


                }
            }

            return Mensaje;
        }
        public static string GeneraArchivoDomiciliacionBanamex(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart, string idEmisora, string idConvenio, int AplicarGastos, int idPlantilla)
        {

            if (idPlantilla != 0)
            {
                CatArchivosDetalleBll.GeneraMoraConfig(GpoEnvio, idPlantilla);
            }

            string Mensaje = "";
            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutSDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutHeader = new List<CatArchivosDetalle>();
            List<CuentasDomiciliacion.Banamex> cuentasDispersar = new List<CuentasDomiciliacion.Banamex>();
            string FechaArchivo =  "";

            LayoutGral = ObtenerCatArchivos(1, "D", "S");
            LayoutHeader = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "H");
            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");
            LayoutSDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "T");
            List<CuentasDomiciliacion.Banamex> cuentaOk = new List<CuentasDomiciliacion.Banamex>();
            List<GpoEnvio_Mora> GpoEnvio_Mora = new List<Entities.CobranzaAdministrativa.GpoEnvio_Mora>();
            List<MoraConfig> MorConfig = new List<Entities.CobranzaAdministrativa.MoraConfig>();


            string consecutivoArchivo = ObtenerConsecutivo(1, "D", "S");



            string[] Particiones;
            if (TotalArchivos == 1)
            {

                GpoEnvio_Mora = dalEnMo.ObtenerGrupoEnvio_Mora(1, GpoEnvio);
                int contador = 1;
                foreach (GpoEnvio_Mora GpoMora in GpoEnvio_Mora)
                {
                    MorConfig = dalConfig.ObtenerMoraConfig(1, GpoMora.IdGrupoMora);

                    foreach (MoraConfig config in MorConfig)
                    {
                        cuentasDispersar = dalDom.CuentasDomiBanamex(GpoEnvio, DiasVenini, DiasVenFin, IdBanco, config.Porcentaje, config.NumAmorti, GpoMora.IdMora, config.monto, AplicarGastos);
                        if (cuentasDispersar.Count > 0)
                        {
                            FechaArchivo = cuentasDispersar[0].FechaPresentacion;
                        }
                        foreach (CuentasDomiciliacion.Banamex disp in cuentasDispersar)
                        {
                            disp.referencia = Convert.ToInt32(disp.referencia.ToString().ToString() + contador.ToString());
                            cuentaOk.Add(disp);
                        }
                        contador = contador + 1;
                    }

                }



                List<string> lineas = new List<string>();

                string linea = "";
                string campo = "";
                string Relleno = "";
                int longitud = 0;
                int longitudCampo = 0;
                int Registros = 0;
                decimal ImporteTotal = 0;
                try
                {

                    linea = String.Empty;
                    foreach (CatArchivosDetalle detalle in LayoutHeader)
                    {

                        switch (detalle.IdCampo)
                        {
                            case 0:
                                campo = detalle.TextiFijo;
                                break;
                            case 20:
                                campo = cuentaOk[0].FechaPresentacion;
                                break;
                            case 34:
                                campo = cuentaOk[0].NumeroBloque;
                                break;
                            case 36:
                                campo = consecutivoArchivo;
                                break;

                        }
                        Relleno = detalle.Relleno;

                        longitud = detalle.Longitud;

                        if (campo != null)
                        {
                            longitudCampo = campo.Length;
                        }
                        else
                        {
                            longitudCampo = 0;
                        }

                        if (detalle.IdTipoCampo == "N")
                        {
                            campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                            if (detalle.Incluyepunto == false)
                            {
                                campo.Replace(".", "");

                            }
                            if (detalle.Alineacion == "I")
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                            else
                            {
                                for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                        if (detalle.IdTipoCampo == "T")
                        {


                            if (detalle.Alineacion == "I")
                            {
                                if (detalle.Longitud > 0)
                                {

                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                            }
                            else
                            {
                                if (detalle.Longitud > 0)
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                        }
                        //campo = String.Format("{0}\t", campo);
                        linea = linea + campo;




                    }
                    linea = linea.Substring(0, linea.Length - 1);
                    lineas.Add(linea);
                    linea = String.Empty;
                    int continuacion = 0;

                    continuacion = cuentaOk.Count + 1;
                    int numerosucuencia = 2;
                    foreach (CuentasDomiciliacion.Banamex cuenta in cuentaOk)
                    {
                        linea = String.Empty;

                        foreach (CatArchivosDetalle detalle in LayoutDet)
                        {
                            switch (detalle.IdCampo)
                            {
                                case 0:
                                    campo = detalle.TextiFijo;
                                    break;
                                case 29:


                                    campo = Convert.ToString(numerosucuencia);
                                    Registros = numerosucuencia;
                                    numerosucuencia = numerosucuencia + 1;

                                    break;
                                case 30:
                                    campo = cuenta.Importe.ToString();
                                    ImporteTotal = ImporteTotal + cuenta.Importe;
                                    break;
                                case 31:
                                    campo = cuenta.FechaLiquidacion;
                                    break;
                                case 32:
                                    campo = cuenta.FechaVencimiento;
                                    break;
                                case 8:
                                    campo = cuenta.Banco;
                                    break;
                                case 6:
                                    campo = cuenta.TipoCuenta;
                                    break;
                                case 9:
                                    campo = cuenta.Clabe;
                                    break;
                                case 3:
                                    campo = cuenta.Cliente;
                                    break;
                                case 14:
                                    campo = cuenta.referencia.ToString();
                                    break;
                                case 33:
                                    campo = cuenta.referencianum.ToString();
                                    break;
                                case 36:
                                    campo = cuenta.Secuencia.ToString();
                                    break;

                            }
                            Relleno = detalle.Relleno;

                            longitud = detalle.Longitud;

                            if (campo != null)
                            {
                                longitudCampo = campo.Length;
                            }
                            else
                            {
                                longitudCampo = 0;
                            }

                            if (detalle.IdTipoCampo == "N")
                            {
                                campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                if (detalle.Incluyepunto == false)
                                {
                                    campo = campo.Replace(".", "");
                                    longitudCampo = campo.Length;

                                }
                                else
                                {
                                    longitudCampo = campo.Length;
                                }
                                if (detalle.Alineacion == "I")
                                {

                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                                else
                                {
                                    longitudCampo = longitudCampo - 2;
                                    for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                            if (detalle.IdTipoCampo == "T")
                            {


                                if (detalle.Alineacion == "I")
                                {
                                    if (detalle.Longitud > 0)
                                    {

                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                }
                                else
                                {
                                    if (detalle.Longitud > 0)
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                            }
                            //campo = String.Format("{0}\t", campo);
                            linea = linea + campo;
                        }
                        linea = linea.Substring(0, linea.Length - 1);
                        lineas.Add(linea);
                        linea = String.Empty;
                    }
                    linea = String.Empty;
                    foreach (CatArchivosDetalle detalle in LayoutSDet)
                    {

                        switch (detalle.IdCampo)
                        {
                            case 0:
                                campo = detalle.TextiFijo;
                                break;
                            case 29:
                                campo = Convert.ToString(Registros + 1);
                                continuacion = Registros + 1;
                                break;
                            case 34:
                                campo = cuentaOk[0].NumeroBloque;
                                break;
                            case 31:
                                campo = Convert.ToString(Registros - 1);
                                break;
                            case 32:
                                campo = ImporteTotal.ToString();
                                break;

                        }
                        Relleno = detalle.Relleno;

                        longitud = detalle.Longitud;

                        if (campo != null)
                        {
                            longitudCampo = campo.Length;
                        }
                        else
                        {
                            longitudCampo = 0;
                        }

                        if (detalle.IdTipoCampo == "N")
                        {
                            campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                            if (detalle.Incluyepunto == false)
                            {
                                campo = campo.Replace(".", "");
                                longitudCampo = campo.Length;

                            }
                            else
                            {
                                longitudCampo = campo.Length;
                            }
                            if (detalle.Alineacion == "I")
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                            else
                            {
                                longitudCampo = longitudCampo - 2;
                                for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                        if (detalle.IdTipoCampo == "T")
                        {


                            if (detalle.Alineacion == "I")
                            {
                                if (detalle.Longitud > 0)
                                {

                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                            }
                            else
                            {
                                if (detalle.Longitud > 0)
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                        }
                        //campo = String.Format("{0}\t", campo);
                        linea = linea + campo;




                    }
                    linea = linea.Substring(0, linea.Length - 1);
                    lineas.Add(linea);
                    linea = String.Empty;

                    //System.IO.File.WriteAllLines(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);

                    string RutaDomiciliacionBanamex = ConfigurationSettings.AppSettings["RutaDomiciliacionBanamex"].ToString();
                    string consecutivoReg = "01";
                    ArchivosDispersion archivo = new ArchivosDispersion();


                    
                    archivo.NombreArchivo = "DCB" + FechaArchivo.Substring(2, 2) + FechaArchivo.Substring(4, 2) + FechaArchivo.Substring(6, 2) + "000" + idConvenio + consecutivoArchivo + ".dom";

                    //System.IO.File.WriteAllLines(@"C:\AfiliacionBanorte_SOPF\DomicilaicionBanamex" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas, Encoding.GetEncoding(1252));
                    //System.IO.File.WriteAllLines(@"C:\Users\Juan Carlos\Documents\MarySantoy\PF\Doctos\DomiciliaconBanamex" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas, Encoding.GetEncoding(1252));
                    System.IO.File.WriteAllLines(@RutaDomiciliacionBanamex + archivo.NombreArchivo, lineas, Encoding.GetEncoding(1252));
                    Mensaje = "1-Archivo Generado con Exito §" + archivo.NombreArchivo;


                    //archivo.NombreArchivo = "DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt";


                    string CorreoDom = ConfigurationSettings.AppSettings["CorreoDom"].ToString();

                    if (CorreoDom == "1")
                    {
                        string CorreoDomFrom = ConfigurationSettings.AppSettings["CorreoDomFrom"].ToString();
                        string CorreoDomTo = ConfigurationSettings.AppSettings["CorreoDomTo"].ToString();


                    MailMessage mail = new MailMessage();
                    SmtpClient client = new SmtpClient("smtpout.secureserver.net");
                    client.Port = 3535;
                    client.Timeout = 300000;
                    client.EnableSsl = false;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential(CorreoDomFrom, "pf2015");
                        mail.From = new MailAddress(CorreoDomFrom);
                        mail.To.Add(new MailAddress(CorreoDomTo));
                    mail.Subject = "Archivo de Domiciliacion Bancomer";
                    mail.Body = "Archivo de Domiciliacion Bancomer";

                    //Attachment Data = new Attachment(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
                        Attachment Data = new Attachment(@RutaDomiciliacionBanamex + archivo.NombreArchivo);
                    mail.Attachments.Add(Data);

                    client.Send(mail);

                    }
                    //System.IO.File.Delete(@"F:\Users\Juan Carlos\Documents\DispersionBanorte\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");


                }
                catch (Exception ex)
                {
                    Mensaje = ex.Message;

                }
            }
            else
            {
                Particiones = Porcent.Split(',');
                int contador = 0;
                for (int j = 0; j <= Particiones.Length - 1; j++)
                {
                    cuentasDispersar = dalDom.CuentasDomiBanamex(GpoEnvio, DiasVenini, DiasVenFin, IdBanco, Convert.ToDecimal(Particiones[j]) / 100, 0, 0, 300, AplicarGastos);
                    contador = contador + 1;
                    cuentaOk.Clear();

                    foreach (CuentasDomiciliacion.Banamex disp in cuentasDispersar)
                    {
                        cuentaOk.Add(disp);
                    }

                    List<string> lineas = new List<string>();

                    string linea = "";
                    string campo = "";
                    string Relleno = "";
                    int longitud = 0;
                    int longitudCampo = 0;
                    int Registros = 0;
                    decimal ImporteTotal = 0;
                    try
                    {

                        linea = String.Empty;
                        foreach (CatArchivosDetalle detalle in LayoutHeader)
                        {

                            switch (detalle.IdCampo)
                            {
                                case 0:
                                    campo = detalle.TextiFijo;
                                    break;
                                case 20:
                                    campo = cuentaOk[0].FechaPresentacion;
                                    break;
                                case 34:
                                    campo = cuentaOk[0].NumeroBloque;
                                    break;

                            }
                            Relleno = detalle.Relleno;

                            longitud = detalle.Longitud;

                            if (campo != null)
                            {
                                longitudCampo = campo.Length;
                            }
                            else
                            {
                                longitudCampo = 0;
                            }

                            if (detalle.IdTipoCampo == "N")
                            {
                                campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                if (detalle.Incluyepunto == false)
                                {
                                    campo.Replace(".", "");

                                }
                                if (detalle.Alineacion == "I")
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                                else
                                {
                                    for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                            if (detalle.IdTipoCampo == "T")
                            {


                                if (detalle.Alineacion == "I")
                                {
                                    if (detalle.Longitud > 0)
                                    {

                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                }
                                else
                                {
                                    if (detalle.Longitud > 0)
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                            }
                            //campo = String.Format("{0}\t", campo);
                            linea = linea + campo;




                        }
                        linea = linea.Substring(0, linea.Length - 1);
                        lineas.Add(linea);
                        linea = String.Empty;
                        foreach (CuentasDomiciliacion.Banamex cuenta in cuentaOk)
                        {
                            linea = String.Empty;
                            int numerosucuencia = 2;
                            foreach (CatArchivosDetalle detalle in LayoutDet)
                            {
                                switch (detalle.IdCampo)
                                {
                                    case 0:
                                        campo = detalle.TextiFijo;
                                        break;
                                    case 29:
                                        campo = Convert.ToString(numerosucuencia);
                                        Registros = numerosucuencia;
                                        numerosucuencia = numerosucuencia + 1;
                                        break;
                                    case 30:
                                        campo = cuenta.Importe.ToString();
                                        ImporteTotal = ImporteTotal + cuenta.Importe;
                                        break;
                                    case 31:
                                        campo = cuenta.FechaLiquidacion;
                                        break;
                                    case 32:
                                        campo = cuenta.FechaVencimiento;
                                        break;
                                    case 8:
                                        campo = cuenta.Banco;
                                        break;
                                    case 6:
                                        campo = cuenta.TipoCuenta;
                                        break;
                                    case 9:
                                        campo = cuenta.Clabe;
                                        break;
                                    case 3:
                                        campo = cuenta.Cliente;
                                        break;
                                    case 14:
                                        campo = cuenta.referencia.ToString() + contador.ToString();
                                        break;
                                    case 33:
                                        campo = cuenta.referencianum.ToString();
                                        break;

                                }
                                Relleno = detalle.Relleno;

                                longitud = detalle.Longitud;

                                if (campo != null)
                                {
                                    longitudCampo = campo.Length;
                                }
                                else
                                {
                                    longitudCampo = 0;
                                }

                                if (detalle.IdTipoCampo == "N")
                                {
                                    campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                    if (detalle.Incluyepunto == false)
                                    {
                                        campo = campo.Replace(".", "");

                                    }
                                    if (detalle.Alineacion == "I")
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                    else
                                    {
                                        longitudCampo = longitudCampo - 3;
                                        for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                                if (detalle.IdTipoCampo == "T")
                                {


                                    if (detalle.Alineacion == "I")
                                    {
                                        if (detalle.Longitud > 0)
                                        {

                                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                            {
                                                campo = campo + Relleno;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (detalle.Longitud > 0)
                                        {
                                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                            {
                                                campo = Relleno + campo;
                                            }
                                        }
                                    }
                                }
                                //campo = String.Format("{0}\t", campo);
                                linea = linea + campo;
                            }
                            linea = linea.Substring(0, linea.Length - 1);
                            lineas.Add(linea);
                            linea = String.Empty;
                        }
                        linea = String.Empty;
                        foreach (CatArchivosDetalle detalle in LayoutSDet)
                        {

                            switch (detalle.IdCampo)
                            {
                                case 0:
                                    campo = detalle.TextiFijo;
                                    break;
                                case 29:
                                    campo = Convert.ToString(Registros + 1);
                                    break;
                                case 34:
                                    campo = cuentaOk[0].NumeroBloque;
                                    break;
                                case 31:
                                    campo = Convert.ToString(Registros - 1);
                                    break;
                                case 32:
                                    campo = ImporteTotal.ToString();
                                    break;

                            }
                            Relleno = detalle.Relleno;

                            longitud = detalle.Longitud;

                            if (campo != null)
                            {
                                longitudCampo = campo.Length;
                            }
                            else
                            {
                                longitudCampo = 0;
                            }

                            if (detalle.IdTipoCampo == "N")
                            {
                                campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                if (detalle.Incluyepunto == false)
                                {
                                    campo = campo.Replace(".", "");

                                }
                                if (detalle.Alineacion == "I")
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                                else
                                {
                                    longitudCampo = longitudCampo - 3;
                                    for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                            if (detalle.IdTipoCampo == "T")
                            {


                                if (detalle.Alineacion == "I")
                                {
                                    if (detalle.Longitud > 0)
                                    {

                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                }
                                else
                                {
                                    if (detalle.Longitud > 0)
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                            }
                            //campo = String.Format("{0}\t", campo);
                            linea = linea + campo;




                        }
                        linea = linea.Substring(0, linea.Length - 1);
                        lineas.Add(linea);
                        linea = String.Empty;

                        //System.IO.File.WriteAllLines(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                        System.IO.File.WriteAllLines(@"F:\AfiliacionBanorte_SOPF\DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Convert.ToString(j) + ".txt", lineas);
                        Mensaje = "1-Archivo Generado con Exito";

                        ArchivosDispersion archivo = new ArchivosDispersion();

                        archivo.NombreArchivo = "DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Convert.ToString(j) + ".txt";




                        //MailMessage mail = new MailMessage();
                        //SmtpClient client = new SmtpClient("smtpout.secureserver.net");
                        //client.Port = 3535;
                        //client.EnableSsl = false;
                        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        //client.UseDefaultCredentials = false;
                        //client.Credentials = new NetworkCredential("notificaciones@prestamofeliz.com.mx", "pf2015");
                        //mail.From = new MailAddress("notificaciones@prestamofeliz.com.mx");
                        //mail.To.Add(new MailAddress("juan.garcia@prestamofeliz.com.mx"));
                        //mail.Subject = "Archivo de Domiciliacion Bancomer";
                        //mail.Body = "Archivo de Domiciliacion Bancomer";

                        ////Attachment Data = new Attachment(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
                        //Attachment Data = new Attachment(@"F:\AfiliacionBanorte_SOPF\DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Convert.ToString(j) + ".txt");
                        //mail.Attachments.Add(Data);

                        //client.Send(mail);
                        //System.IO.File.Delete(@"F:\Users\Juan Carlos\Documents\DispersionBanorte\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");


                    }
                    catch (Exception ex)
                    {
                        Mensaje = ex.Message;


                    }
                }
            }
            return Mensaje;
        }

        public static string GeneraArchivoAfilDomiciliacion(int idBanco)
        {
            string Mensaje = "";
            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutHeader = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<AfiliacionDom_Banorte> cuentasAfiliar = new List<AfiliacionDom_Banorte>();

            LayoutGral = ObtenerCatArchivos(idBanco, "D", "S");
            LayoutHeader = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "H");
            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");

            cuentasAfiliar = Creditos.AfiliacionDom_BanorteBll.ObtenerCuentas(1);
            List<string> lineas = new List<string>();

            string linea = "";
            string campo = "";
            string Relleno = "";
            int longitud = 0;
            int longitudCampo = 0;


            linea = String.Empty;
            foreach (CatArchivosDetalle header in LayoutHeader)
            {

                switch (header.IdCampo)
                {
                    case 0:
                        campo = header.TextiFijo;
                        break;
                    case 20:
                        campo = Convert.ToString(cuentasAfiliar[0].Fecha);
                        break;
                    case 21:
                        campo = Convert.ToString(cuentasAfiliar[0].NumeroAfiliaciones);
                        break;

                }
                Relleno = header.Relleno;
                longitud = header.Longitud;

                if (campo != null)
                {
                    longitudCampo = campo.Length;
                }
                else
                {
                    longitudCampo = 0;
                }

                if (header.IdTipoCampo == "N")
                {
                    campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), header.Decimales));
                    if (header.Incluyepunto == false)
                    {
                        campo.Replace(".", "");

                    }
                    if (header.Alineacion == "I")
                    {
                        for (int i = 1; i <= header.Longitud - longitudCampo; i++)
                        {
                            campo = campo + Relleno;
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= header.Enteros - longitudCampo; i++)
                        {
                            campo = Relleno + campo;
                        }
                    }
                }
                if (header.IdTipoCampo == "T")
                {


                    if (header.Alineacion == "I")
                    {
                        if (header.Longitud > 0)
                        {

                            for (int i = 1; i <= header.Longitud - longitudCampo; i++)
                            {
                                campo = campo + Relleno;
                            }
                        }
                    }
                    else
                    {
                        if (header.Longitud > 0)
                        {
                            for (int i = 1; i <= header.Longitud - longitudCampo; i++)
                            {
                                campo = Relleno + campo;
                            }
                        }
                    }
                }
                //campo = String.Format("{0}\t", campo);
                linea = linea + campo;




            }
            linea = linea.Substring(0, linea.Length - 1);
            lineas.Add(linea);
            linea = String.Empty;
            foreach (AfiliacionDom_Banorte cuenta in cuentasAfiliar)
            {
                foreach (CatArchivosDetalle detalle in LayoutDet)
                {

                    switch (detalle.IdCampo)
                    {
                        case 0:
                            campo = detalle.TextiFijo;
                            break;
                        case 22:
                            campo = Convert.ToString(cuenta.ReferenciaServicio);
                            break;
                        case 23:
                            campo = cuenta.BancoReceptor;
                            break;
                        case 24:
                            campo = cuenta.nombreTitular;
                            break;
                        case 6:
                            campo = cuenta.TipoCuenta;
                            break;
                        case 9:
                            campo = cuenta.Clabe;
                            break;
                        case 26:
                            campo = cuenta.NumeroId;
                            break;
                        case 4:
                            campo = cuenta.RFC;
                            break;

                    }
                    Relleno = detalle.Relleno;

                    if (detalle.IdCampo == 9 && cuenta.TipoCuenta == "001")
                    {
                        detalle.Longitud = 10;
                    }

                    longitud = detalle.Longitud;

                    if (campo != null)
                    {


                        longitudCampo = campo.Length;

                    }
                    else
                    {
                        longitudCampo = 0;
                    }

                    if (detalle.IdTipoCampo == "N")
                    {
                        campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                        if (detalle.Incluyepunto == false)
                        {
                            campo = campo.Replace(".", "");
                            longitudCampo = campo.Length;

                        }
                        if (detalle.Alineacion == "I")
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                        }
                        else
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - (longitudCampo); i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                    }
                    if (detalle.IdTipoCampo == "T")
                    {


                        if (detalle.Alineacion == "I")
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                        }
                        else
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                    }
                    //campo = String.Format("{0}\t", campo);
                    linea = linea + campo;




                }
                linea = linea.Substring(0, linea.Length - 1);
                lineas.Add(linea);
                linea = String.Empty;
            }
            System.IO.File.WriteAllLines(@"F:\AfiliacionBanorte_SOPF\" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);




            //MailMessage mail = new MailMessage();
            //SmtpClient client = new SmtpClient("smtpout.secureserver.net");
            //client.Port = 3535;
            //client.EnableSsl = false;
            //client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //client.UseDefaultCredentials = false;
            //client.Credentials = new NetworkCredential("notificaciones@prestamofeliz.com.mx", "pf2015");
            //mail.From = new MailAddress("notificaciones@prestamofeliz.com.mx");
            //mail.To.Add(new MailAddress("arelis@prestamofeliz.com.mx"));
            //mail.CC.Add(new MailAddress("alfredo.garcia@prestamofeliz.com.mx"));
            //mail.Subject = "Archivo de Afiliacion Domi.";
            //mail.Body = "Archivo de Afiliacion Domi";

            //Attachment Data = new Attachment(@"F:\AfiliacionBanorte_SOPF\" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
            //mail.Attachments.Add(Data);

            //client.Send(mail);

            return Mensaje;
        }



        public static string GeneraArchivoDomiciliacionBancomerRemanente(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, string Porcent, int TotalArchivos, int TotPart, string idEmisora, string idConvenio, int AplicarGastos, int idPlantilla, int idGrupoEnvioOrigen)
        {

            if (idPlantilla != 0)
            {

                CatArchivosDetalleBll.GeneraMoraConfig(GpoEnvio, idPlantilla);
            }

            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutSDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutHeader = new List<CatArchivosDetalle>();
            List<CuentasDomiciliacion.Bancomer> cuentasDispersar = new List<CuentasDomiciliacion.Bancomer>();

            LayoutGral = ObtenerCatArchivos(2, "C", "S");

            string SeparadorRFC = "";
            LayoutHeader = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "H");

            switch (idEmisora)
            {
                case "T":
                    SeparadorRFC = " ";
                    break;
                case "PO":
                    SeparadorRFC = "-";
                    break;
                case "PA":
                    SeparadorRFC = "*";
                    break;
            }


            if (LayoutHeader[12].Descripcion == "RFC")
            {
                LayoutHeader[12].TextiFijo = LayoutHeader[12].TextiFijo.ToString().Replace(" ", SeparadorRFC);
            }

            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");
            LayoutSDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "T");
            List<CuentasDomiciliacion.Bancomer> cuentaOk = new List<CuentasDomiciliacion.Bancomer>();
            List<GpoEnvio_Mora> GpoEnvio_Mora = new List<Entities.CobranzaAdministrativa.GpoEnvio_Mora>();
            List<MoraConfig> MorConfig = new List<Entities.CobranzaAdministrativa.MoraConfig>();
            int TotalCuentas = 0;
            string Mensaje = "";
            int Archivo = 0;

            string consecutivoArchivo = ObtenerConsecutivo(2, "C", "S");


            int maxRegistrosArchivos = Convert.ToInt32(ConfigurationSettings.AppSettings["maxRegistrosArchivos"].ToString());
            string[] Particiones;
            if (TotalArchivos == 1)
            {

                GpoEnvio_Mora = dalEnMo.ObtenerGrupoEnvio_Mora(1, GpoEnvio);
                int contador = 1;
                int consecutivoReg = GpoEnvio_Mora[0].ConsecutivoReg;
                foreach (GpoEnvio_Mora GpoMora in GpoEnvio_Mora)
                {
                    MorConfig = dalConfig.ObtenerMoraConfig(1, GpoMora.IdGrupoMora);

                    foreach (MoraConfig config in MorConfig)
                    {
                        cuentasDispersar = dalDom.CuentasDomiBancomerRemanente(GpoEnvio, DiasVenini, DiasVenFin, IdBanco, config.Porcentaje, config.NumAmorti, GpoMora.IdMora, config.monto, AplicarGastos, idGrupoEnvioOrigen, idEmisora);
                        foreach (CuentasDomiciliacion.Bancomer disp in cuentasDispersar)
                        {
                            TotalCuentas = TotalCuentas + 1;
                            disp.referencia = (disp.referencia.ToString().ToString() + disp.FechaPresentacion + disp.NumeroBloque.Substring(2, 5) + contador.ToString());
                            disp.referencianum = Convert.ToString(consecutivoReg + 1);
                            consecutivoReg = consecutivoReg + 1;
                            cuentaOk.Add(disp);
                            if (TotalCuentas == maxRegistrosArchivos)
                            {
                                Archivo = Archivo + 1;
                                Mensaje = GenerarLayOutBancomer(LayoutGral, LayoutDet, LayoutSDet, LayoutHeader, cuentasDispersar, cuentaOk, GpoEnvio_Mora, MorConfig, Archivo, consecutivoArchivo);
                                TotalCuentas = 0;
                                cuentaOk.Clear();

                            }
                        }
                        contador = contador + 1;

                    }

                }

                //TODO MSa: actualizar el consecutivo en tabla
                ABCCatParametro(1, "CONDOM", consecutivoReg.ToString(), 1);


                Archivo = Archivo + 1;
                Mensaje = GenerarLayOutBancomer(LayoutGral, LayoutDet, LayoutSDet, LayoutHeader, cuentasDispersar, cuentaOk, GpoEnvio_Mora, MorConfig, Archivo, consecutivoArchivo);
                return Mensaje;

            }
            else
            {
                Particiones = Porcent.Split(',');
                int contador = 0;
                for (int j = 0; j <= Particiones.Length - 1; j++)
                {
                    cuentasDispersar = dalDom.CuentasDomiBancomerRemanente(GpoEnvio, DiasVenini, DiasVenFin, IdBanco, Convert.ToDecimal(Particiones[j]) / 100, 0, 0, 300, AplicarGastos, idGrupoEnvioOrigen, idEmisora);
                    contador = contador + 1;
                    cuentaOk.Clear();

                    foreach (CuentasDomiciliacion.Bancomer disp in cuentasDispersar)
                    {
                        cuentaOk.Add(disp);
                    }

                    List<string> lineas = new List<string>();

                    string linea = "";
                    string campo = "";
                    string Relleno = "";
                    int longitud = 0;
                    int longitudCampo = 0;
                    int Registros = 0;
                    decimal ImporteTotal = 0;
                    try
                    {

                        linea = String.Empty;
                        foreach (CatArchivosDetalle detalle in LayoutHeader)
                        {

                            switch (detalle.IdCampo)
                            {
                                case 0:
                                    campo = detalle.TextiFijo;
                                    break;
                                case 20:
                                    campo = cuentaOk[0].FechaPresentacion;
                                    break;
                                case 34:
                                    campo = cuentaOk[0].NumeroBloque;
                                    break;

                            }
                            Relleno = detalle.Relleno;

                            longitud = detalle.Longitud;

                            if (campo != null)
                            {
                                longitudCampo = campo.Length;
                            }
                            else
                            {
                                longitudCampo = 0;
                            }

                            if (detalle.IdTipoCampo == "N")
                            {
                                campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                if (detalle.Incluyepunto == false)
                                {
                                    campo.Replace(".", "");

                                }
                                if (detalle.Alineacion == "I")
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                                else
                                {
                                    for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                            if (detalle.IdTipoCampo == "T")
                            {


                                if (detalle.Alineacion == "I")
                                {
                                    if (detalle.Longitud > 0)
                                    {

                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                }
                                else
                                {
                                    if (detalle.Longitud > 0)
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                            }
                            //campo = String.Format("{0}\t", campo);
                            linea = linea + campo;




                        }
                        linea = linea.Substring(0, linea.Length - 1);
                        lineas.Add(linea);
                        linea = String.Empty;
                        foreach (CuentasDomiciliacion.Bancomer cuenta in cuentaOk)
                        {
                            linea = String.Empty;
                            int numerosucuencia = 2;
                            foreach (CatArchivosDetalle detalle in LayoutDet)
                            {
                                switch (detalle.IdCampo)
                                {
                                    case 0:
                                        campo = detalle.TextiFijo;
                                        break;
                                    case 29:
                                        campo = Convert.ToString(numerosucuencia);
                                        Registros = numerosucuencia;
                                        numerosucuencia = numerosucuencia + 1;
                                        break;
                                    case 30:
                                        campo = cuenta.Importe.ToString();
                                        ImporteTotal = ImporteTotal + cuenta.Importe;
                                        break;
                                    case 31:
                                        campo = cuenta.FechaLiquidacion;
                                        break;
                                    case 32:
                                        campo = cuenta.FechaVencimiento;
                                        break;
                                    case 8:
                                        campo = cuenta.Banco;
                                        break;
                                    case 6:
                                        campo = cuenta.TipoCuenta;
                                        break;
                                    case 9:
                                        campo = cuenta.Clabe;
                                        break;
                                    case 3:
                                        campo = cuenta.Cliente;
                                        break;
                                    case 14:
                                        campo = cuenta.referencia.ToString() + contador.ToString();
                                        break;
                                    case 33:
                                        campo = cuenta.referencianum.ToString();
                                        break;

                                }
                                Relleno = detalle.Relleno;

                                longitud = detalle.Longitud;

                                if (campo != null)
                                {
                                    longitudCampo = campo.Length;
                                }
                                else
                                {
                                    longitudCampo = 0;
                                }

                                if (detalle.IdTipoCampo == "N")
                                {
                                    campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                    if (detalle.Incluyepunto == false)
                                    {
                                        campo = campo.Replace(".", "");

                                    }
                                    if (detalle.Alineacion == "I")
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                    else
                                    {
                                        longitudCampo = longitudCampo - 3;
                                        for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                                if (detalle.IdTipoCampo == "T")
                                {


                                    if (detalle.Alineacion == "I")
                                    {
                                        if (detalle.Longitud > 0)
                                        {

                                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                            {
                                                campo = campo + Relleno;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (detalle.Longitud > 0)
                                        {
                                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                            {
                                                campo = Relleno + campo;
                                            }
                                        }
                                    }
                                }
                                //campo = String.Format("{0}\t", campo);
                                linea = linea + campo;
                            }
                            linea = linea.Substring(0, linea.Length - 1);
                            lineas.Add(linea);
                            linea = String.Empty;
                        }
                        linea = String.Empty;
                        foreach (CatArchivosDetalle detalle in LayoutSDet)
                        {

                            switch (detalle.IdCampo)
                            {
                                case 0:
                                    campo = detalle.TextiFijo;
                                    break;
                                case 29:
                                    campo = Convert.ToString(Registros + 1);
                                    break;
                                case 34:
                                    campo = cuentaOk[0].NumeroBloque;
                                    break;
                                case 31:
                                    campo = Convert.ToString(Registros - 1);
                                    break;
                                case 32:
                                    campo = ImporteTotal.ToString();
                                    break;

                            }
                            Relleno = detalle.Relleno;

                            longitud = detalle.Longitud;

                            if (campo != null)
                            {
                                longitudCampo = campo.Length;
                            }
                            else
                            {
                                longitudCampo = 0;
                            }

                            if (detalle.IdTipoCampo == "N")
                            {
                                campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                                if (detalle.Incluyepunto == false)
                                {
                                    campo = campo.Replace(".", "");

                                }
                                if (detalle.Alineacion == "I")
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                                else
                                {
                                    longitudCampo = longitudCampo - 3;
                                    for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                            if (detalle.IdTipoCampo == "T")
                            {


                                if (detalle.Alineacion == "I")
                                {
                                    if (detalle.Longitud > 0)
                                    {

                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = campo + Relleno;
                                        }
                                    }
                                }
                                else
                                {
                                    if (detalle.Longitud > 0)
                                    {
                                        for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                        {
                                            campo = Relleno + campo;
                                        }
                                    }
                                }
                            }
                            //campo = String.Format("{0}\t", campo);
                            linea = linea + campo;




                        }
                        linea = linea.Substring(0, linea.Length - 1);
                        lineas.Add(linea);
                        linea = String.Empty;

                        //System.IO.File.WriteAllLines(@"F:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                        //System.IO.File.WriteAllLines(@"F:\AfiliacionBanorte_SOPF\DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Convert.ToString(j) + ".txt", lineas);



                        ArchivosDispersion archivo = new ArchivosDispersion();
                        //archivo.NombreArchivo = "DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Convert.ToString(j) + consecutivoArchivo + ".txt";


                        string Anio = DateTime.Now.Year.ToString();
                        string Mes = "0" + DateTime.Now.Month.ToString();
                        string Dia = "0" + DateTime.Now.Day.ToString();
                        archivo.NombreArchivo = "DomiciliacionBancomer" + Dia.Substring(Dia.Length - 2, 2) + Mes.Substring(Mes.Length - 2, 2) + Anio.Substring(2, 2) + consecutivoArchivo + ".txt";


                        string RutaDomiciliacionBancomer = ConfigurationSettings.AppSettings["RutaDomiciliacionBancomer"].ToString();
                        System.IO.File.WriteAllLines(@RutaDomiciliacionBancomer + archivo.NombreArchivo, lineas);

                        Mensaje = "1-Archivo Generado con Exito";

                        //archivo.NombreArchivo = "DomicilaicionBancomer" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + Convert.ToString(j) + ".txt";

                        string CorreoDomiciliacion = ConfigurationSettings.AppSettings["CorreoDom"].ToString();

                        if (CorreoDomiciliacion == "1")
                        {

                            string CorreoDomFrom = ConfigurationSettings.AppSettings["CorreoDomFrom"].ToString();
                            string CorreoDomTo = ConfigurationSettings.AppSettings["CorreoDomTo"].ToString();

            MailMessage mail = new MailMessage();
            SmtpClient client = new SmtpClient("smtpout.secureserver.net");
            client.Port = 3535;
            client.Timeout = 300000;
            client.EnableSsl = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
                            client.Credentials = new NetworkCredential(CorreoDomFrom, "pf2015");
                            mail.From = new MailAddress(CorreoDomFrom);
                            mail.To.Add(new MailAddress(CorreoDomTo));
                            mail.Subject = "Archivo de Domiciliacion Bancomer";
                            mail.Body = "Archivo de Domiciliacion Bancomer";


                            Attachment Data = new Attachment(@RutaDomiciliacionBancomer + archivo.NombreArchivo);
            mail.Attachments.Add(Data);

            client.Send(mail);
                        }

                        //System.IO.File.Delete(@"F:\Users\Juan Carlos\Documents\DispersionBanorte\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");


                    }
                    catch (Exception ex)
                    {
                        Mensaje = ex.Message;


                    }
                }
            }
            return Mensaje;
        }
        
        public static string ObtenerConsecutivo(int idBanco, string tipoTran, string tipo)
        {
            return dal.ObtenerConsecutivo(idBanco, tipoTran, tipo);
        }
        public static void ABCCatParametro(int Accion, string Parametro, string valor, int idEstatus)
        {
            dal.ABCCatParametro(Accion, Parametro, valor, idEstatus);
        }

        public static void AltaArchivosDomiciliacion(string NombreArchivo, int IdBanco, int IdGrupo, int IdPlantilla, string Convenio, string Emisora, int DiasVenIni, int DiasVenFin, int AplicarGastos, int IdUsuario)
        {
            dal.AltaArchivosDomiciliacion(NombreArchivo, IdBanco, IdGrupo, IdPlantilla, Convenio, Emisora, DiasVenIni, DiasVenFin, AplicarGastos, IdUsuario);
        }
    }

}