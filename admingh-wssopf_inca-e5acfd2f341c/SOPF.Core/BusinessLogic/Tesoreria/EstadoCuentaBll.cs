#pragma warning disable 150317
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Tesoreria;
using SOPF.Core.DataAccess.Tesoreria;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: EstadoCuentaBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase EstadoCuentaDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-03-17 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Tesoreria
{
    public static class EstadoCuentaBll
    {
        private static EstadoCuentaDal dal;

        static EstadoCuentaBll()
        {
            dal = new EstadoCuentaDal();
        }

        public static void InsertarEstadoCuenta(EstadoCuenta entidad)
        {
            dal.InsertarEstadoCuenta(entidad);			
        }
        
        public static EstadoCuenta ObtenerEstadoCuenta()
        {
            return dal.ObtenerEstadoCuenta();
        }
        
        public static void ActualizarEstadoCuenta()
        {
            dal.ActualizarEstadoCuenta();
        }

        public static void EliminarEstadoCuenta()
        {
            dal.EliminarEstadoCuenta();
        }
    }
}