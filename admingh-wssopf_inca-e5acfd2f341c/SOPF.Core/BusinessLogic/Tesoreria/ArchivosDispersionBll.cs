#pragma warning disable 150126
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Tesoreria;
using SOPF.Core.DataAccess.Tesoreria;
using SOPF.Core.DataAccess.Creditos;
using SOPF.Core.BusinessLogic.Creditos;


# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: ArchivosDispersionBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase ArchivosDispersionDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-01-26 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Tesoreria
{
    public static class ArchivosDispersionBll
    {
        private static ArchivosDispersionDal dal;
        private static  CorteDispersionDetalleDal dalC;

        static ArchivosDispersionBll()
        {
            dal = new ArchivosDispersionDal();
            dalC = new CorteDispersionDetalleDal();
        }

        public static void InsertarArchivosDispersion(ArchivosDispersion entidad)
        {
            dal.InsertarArchivosDispersion(entidad);			
        }
        
        public static List<ArchivosDispersion> ObtenerArchivosDispersion(int Accion, int idArchivo)
        {
            return dal.ObtenerArchivosDispersion(Accion,idArchivo);
        }
        
        public static string ActualizarArchivosDispersion(ArchivosDispersion Archivo)
        {
            string Mensaje = "";
            int IdCorte = 0;
            string Resultado = "";
            List<ArchivosDispersion> archivos = new List<ArchivosDispersion>();
            try
            {
                dal.ActualizarArchivosDispersion(Archivo);
                archivos = dal.ObtenerArchivosDispersion(2, Archivo.IdArchivo);

                if (archivos.Count > 0)
                {
                    IdCorte = archivos[0].IdCorte;
                }
                if (Archivo.Estatus == 1)
                {
                    dalC.ActCorteDispersionDetalle(4, IdCorte, 0);
                }

                if (Archivo.Estatus == 2 || Archivo.Estatus == 3)
                {
                    dalC.ActCorteDispersionDetalle(5, IdCorte, 0);
                }

                Mensaje = "0-Actualizado con Exito";
                

                
                Resultado   = CatArchivosBll.GeneraArchivoAfilDomiciliacion(16);
                return Mensaje;
                
            }
            catch (Exception ex)
            {
                Mensaje = ex.Message;
                return Mensaje;
            }
            
        }

        public static void EliminarArchivosDispersion()
        {
            dal.EliminarArchivosDispersion();
        }
    }
}