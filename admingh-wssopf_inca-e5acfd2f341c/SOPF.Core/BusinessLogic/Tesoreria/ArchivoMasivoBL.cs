﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Tesoreria;
using SOPF.Core.DataAccess.Tesoreria;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.Entities.Catalogo;

namespace SOPF.Core.BusinessLogic.Tesoreria
{
    public static class ArchivoMasivoBL
    {
        private static ArchivoMasivoDAL dal;

        static ArchivoMasivoBL()
        {
            dal = new ArchivoMasivoDAL();
        }

        public static List<ArchivoMasivo> Filtrar(ArchivoMasivo Entity)
        {
            List<ArchivoMasivo> objReturn = new List<ArchivoMasivo>();

            objReturn = dal.Filtrar(Entity);

            return objReturn;
        }

        public static List<ArchivoMasivo> Filtrar_PagosDirectos(ArchivoMasivo Entity)
        {
            List<ArchivoMasivo> objReturn = new List<ArchivoMasivo>();

            // Obtener el Tipo Archivo para Pagos Directos
            TipoArchivoDAL taDAL = new TipoArchivoDAL();
            TipoArchivo tipoPagosDirectos = taDAL.Filtrar(new TipoArchivo() { Descripcion = "CARGA PAGOS DIRECTOS" }).FirstOrDefault();

            Entity.TipoArchivo = tipoPagosDirectos;

            objReturn = dal.Filtrar(Entity);

            return objReturn;
        }

        public static List<ArchivoMasivo> ObtenerProcesados_Domiciliacion()
        {
            List<ArchivoMasivo> objReturn = new List<ArchivoMasivo>();

            objReturn = dal.ObtenerProcesados_Domiciliacion();

            return objReturn;
        }

        public static List<ArchivoMasivo> ObtenerRechazados_Domiciliacion()
        {
            List<ArchivoMasivo> objReturn = new List<ArchivoMasivo>();

            objReturn = dal.ObtenerRechazados_Domiciliacion();

            return objReturn;
        }

        public static List<ArchivoMasivo> ObtenerConError_Domiciliacion()
        {
            List<ArchivoMasivo> objReturn = new List<ArchivoMasivo>();

            objReturn = dal.ObtenerConError_Domiciliacion();

            return objReturn;
        }

        public static List<ArchivoMasivo> ObtenerProcesados_PagosDirectos()
        {
            List<ArchivoMasivo> objReturn = new List<ArchivoMasivo>();

            objReturn = dal.ObtenerProcesados_PagosDirectos();

            return objReturn;
        }

        public static List<ArchivoMasivo> ObtenerConError_PagosDirectos()
        {
            List<ArchivoMasivo> objReturn = new List<ArchivoMasivo>();

            objReturn = dal.ObtenerConError_PagosDirectos();

            return objReturn;
        }

        public static ArchivoMasivo ObtenerDetalle(int IdArchivo)
        {
            ArchivoMasivo objReturn = new ArchivoMasivo();

            objReturn = dal.ObtenerDetalle(IdArchivo);

            return objReturn;
        }

        public static Resultado<bool> Registrar(ArchivoMasivo Entity)
        {
            return dal.Registrar(Entity);            
        }

        public static Resultado<bool> Registrar_PagosDirectos(ArchivoMasivo Entity)
        {
            // Obtener el Tipo Archivo para Pagos Directos
            TipoArchivoDAL taDAL = new TipoArchivoDAL();
            TipoArchivo tipoPagosDirectos = taDAL.Filtrar(new TipoArchivo() { Descripcion = "CARGA PAGOS DIRECTOS" }).FirstOrDefault();

            Entity.TipoArchivo = tipoPagosDirectos;

            return dal.Registrar(Entity);
        }

        #region "Domiciliacion Net Cash BBVA"

        public static List<ArchivoMasivo> Filtrar_DomiBBVA(ArchivoMasivo Entity)
        {
            List<ArchivoMasivo> objReturn = new List<ArchivoMasivo>();

            // Obtener el Tipo Archivo para Pagos Directos
            TipoArchivoDAL taDAL = new TipoArchivoDAL();
            TipoArchivo tipoPagosDirectos = taDAL.Filtrar(new TipoArchivo() { Descripcion = "ENVIO DOMICILIACION" }).FirstOrDefault();

            Entity.TipoArchivo = tipoPagosDirectos;

            objReturn = dal.Filtrar(Entity);

            return objReturn;
        }

        public static List<ArchivoMasivo> ObtenerArchivos_SubidosBBVA()
        {
            List<ArchivoMasivo> objReturn = new List<ArchivoMasivo>();

            objReturn = dal.ObtenerArchivos_SubidosBBVA();

            return objReturn;
        }

        public static List<ArchivoMasivo> ObtenerArchivos_GeneradosBBVA()
        {
            List<ArchivoMasivo> objReturn = new List<ArchivoMasivo>();

            objReturn = dal.ObtenerArchivos_GeneradosBBVA();

            return objReturn;
        }

        public static Resultado<bool> Registrar_EnvioDomiBBVA(ArchivoMasivo Entity)
        {
            // Obtener el Tipo Archivo para Pagos Directos
            TipoArchivoDAL taDAL = new TipoArchivoDAL();
            TipoArchivo tipoPagosDirectos = taDAL.Filtrar(new TipoArchivo() { Descripcion = "ENVIO DOMICILIACION DEBITO NETCASH" }).FirstOrDefault();

            Entity.TipoArchivo = tipoPagosDirectos;

            return dal.RegistrarEnvioDomiBBVA(Entity);
        }

        public static Resultado<bool> Registrar_RespuestaDomiBBVA(string nombresRespuestas, string nombreArchivo, int numArchivos)
        {
            // Obtener el Tipo Archivo para Pagos Directos
            TipoArchivoDAL taDAL = new TipoArchivoDAL();
            TipoArchivo tipoPagosDirectos = taDAL.Filtrar(new TipoArchivo() { Descripcion = "RESPUESTA DEBITO NETCASH" }).FirstOrDefault();


            int TipoArchivo = Convert.ToInt32(tipoPagosDirectos.IdTipoArchivo);

            return dal.RegistrarRespuestaDomiBBVA(nombresRespuestas, nombreArchivo, numArchivos, TipoArchivo);
        }

        public static List<EnvioDomiBBVA> DescragaTXTDomiBBVA(string nombreArchivo)
        {

            return dal.DescragaTXTDomiBBVA(nombreArchivo);

        }

        public static List<FitrarRespuestaBBVA> FiltrarRespuestaDomiBBVA(string nombreArchivo)
        {
  
            return dal.FiltrarRespuestaDomiBBVA(nombreArchivo);

        }

        public static List<CAT_Parametros> ObtenerParametros(int IdParametro, string ClaveParametro)
        {

            return dal.ObtenerParametros( IdParametro,  ClaveParametro);

        }

        

        public static List<HistorialCobros> ObtenerHistorialSeguimientoDomiBBVA(int idArchivo)
        { 

            return dal.ObtenerHistorialSeguimientoDomiBBVA(idArchivo);

        }

        #endregion
    }
}
