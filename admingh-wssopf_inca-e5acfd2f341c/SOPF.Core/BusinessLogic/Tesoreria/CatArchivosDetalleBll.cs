#pragma warning disable 141124
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Tesoreria;
using SOPF.Core.DataAccess.Tesoreria;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatArchivosDetalleBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase CatArchivosDetalleDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-11-24 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Tesoreria
{
    public static class CatArchivosDetalleBll
    {
        private static CatArchivosDetalleDal dal;

        static CatArchivosDetalleBll()
        {
            dal = new CatArchivosDetalleDal();
        }

        public static void InsertarCatArchivosDetalle(CatArchivosDetalle entidad)
        {
            dal.InsertarCatArchivosDetalle(entidad);			
        }
        
        public static List<CatArchivosDetalle> ObtenerCatArchivosDetalle(int idArchivo, string seccion)
        {
            return dal.ObtenerCatArchivosDetalle(idArchivo,seccion);
        }
        
        public static void ActualizarCatArchivosDetalle()
        {
            dal.ActualizarCatArchivosDetalle();
        }

        public static void EliminarCatArchivosDetalle()
        {
            dal.EliminarCatArchivosDetalle();
        }

        public static void GeneraMoraConfig(int idGrupo, int idPlantilla)
        {
            dal.GeneraMoraConfig(idGrupo, idPlantilla);
        }
    }
}