﻿using Microsoft.Win32.SafeHandles;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace SOPF.Core.BusinessLogic.Lleida
{
    public class WsPfLleida : IDisposable
    {
        private bool disposedValue = false; // Para detectar llamadas redundantes
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
        private HttpClient apiClient;
        private AuthenticationHeaderValue autorizacion;
        public string ConfiguracionV1 { get => "api/v1/cs/configuraciones"; }
        public string Firmav1 { get => "api/v1/cs/firmas"; }
        public string NotificacionV1 { get => "api/v1/cs/notificaciones"; }
        public string VideoV1 { get => "api/v1/ekyc/videos"; }

        public WsPfLleida()
        {
            apiClient = new HttpClient
            {
                BaseAddress = new Uri(AppSettings.WsPfLleidaUrl)
            };
            apiClient.DefaultRequestHeaders.Accept.Clear();
            var byteAuth = Encoding.ASCII.GetBytes($"{AppSettings.WsPfLleidaUsuairo}:{AppSettings.WsPfLleidaPassword}");
            autorizacion = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteAuth));
        }

        #region IDisposable Support        
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    handle.Dispose();
                }

                disposedValue = true;
            }
        }

        // Este código se agrega para implementar correctamente el patrón descartable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region GET
        internal bool Get(string metodo, string queryString, out string respuestaJson)
        {
            bool peticionCorrecta = false;
            try
            {
                apiClient.DefaultRequestHeaders.Clear();
                apiClient.DefaultRequestHeaders.Authorization = autorizacion;
                apiClient.Timeout = new TimeSpan(0, 0, AppSettings.WsPfLleidaTimeOut);
                if (!string.IsNullOrEmpty(queryString)) metodo = string.Format("{0}?{1}", metodo, queryString.Trim());
                HttpResponseMessage httpResponse = apiClient.GetAsync(metodo).Result;
                respuestaJson = httpResponse.Content.ReadAsStringAsync().Result;
                peticionCorrecta = httpResponse.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { metodo, queryString })}", JsonConvert.SerializeObject(ex));
                respuestaJson = JsonConvert.SerializeObject(ex);
            }
            return peticionCorrecta;
        }
        #endregion

        #region POST
        internal bool Post(string url, object entidadBody, out string respuestaJson)
        {
            bool peticionCorrecta = false;
            respuestaJson = string.Empty;
            try
            {
                ByteArrayContent byteContenido = null;
                apiClient.DefaultRequestHeaders.Clear();
                apiClient.DefaultRequestHeaders.Authorization = autorizacion;
                apiClient.Timeout = new TimeSpan(0, 0, AppSettings.WsPfLleidaTimeOut);
                string jsonBody = string.Empty;
                if (entidadBody != null) jsonBody = JsonConvert.SerializeObject(entidadBody);
                if (!string.IsNullOrEmpty(jsonBody))
                {
                    byteContenido = new ByteArrayContent(Encoding.UTF8.GetBytes(jsonBody));
                    byteContenido.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");
                }
                HttpResponseMessage httpResponse = apiClient.PostAsync(url, byteContenido).Result;
                respuestaJson = httpResponse.Content.ReadAsStringAsync().Result;
                peticionCorrecta = httpResponse.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { url, entidadBody, respuestaJson })}", JsonConvert.SerializeObject(ex));
                respuestaJson = JsonConvert.SerializeObject(ex);
            }
            return peticionCorrecta;
        }
        #endregion

        #region PUT
        internal bool Put(string url, object entidadBody, out string respuestaJson)
        {
            bool peticionCorrecta = false;
            respuestaJson = string.Empty;
            try
            {
                ByteArrayContent byteContenido = null;
                apiClient.DefaultRequestHeaders.Clear();
                apiClient.DefaultRequestHeaders.Authorization = autorizacion;
                apiClient.Timeout = new TimeSpan(0, 0, AppSettings.WsPfLleidaTimeOut);
                string jsonBody = string.Empty;
                if (entidadBody != null) jsonBody = JsonConvert.SerializeObject(entidadBody);
                if (!string.IsNullOrEmpty(jsonBody))
                {
                    byteContenido = new ByteArrayContent(Encoding.UTF8.GetBytes(jsonBody));
                    byteContenido.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");
                }
                HttpResponseMessage httpResponse = apiClient.PutAsync(url, byteContenido).Result;
                respuestaJson = httpResponse.Content.ReadAsStringAsync().Result;
                peticionCorrecta = httpResponse.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { url, entidadBody, respuestaJson })}", JsonConvert.SerializeObject(ex));
                respuestaJson = JsonConvert.SerializeObject(ex);
            }
            return peticionCorrecta;
        }
        #endregion
    }
}
