﻿using Newtonsoft.Json;
using SOPF.Core.BusinessLogic.Catalogo;
using SOPF.Core.DataAccess;
using SOPF.Core.Lleida;
using SOPF.Core.Model.Dbo;
using SOPF.Core.Model.Request.Lleida;
using SOPF.Core.Model.Request.WsPfLleida;
using SOPF.Core.Model.Response.Lleida;
using SOPF.Core.Model.Response.WsPfLleida;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Lleida
{
    public static class NotificacionBLL
    {
        #region GET

        #endregion

        #region POST
        public static ObtenerFirmasResponse GenerarNotificacion(GenerarNotificacionRequest peticion)
        {
            ObtenerFirmasResponse respuesta = new ObtenerFirmasResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta");
                List<EstatusVM> restatusFirma = TB_CATEstatusBLL.EstatusProcesoFirmaDigital();
                if (restatusFirma != null && restatusFirma.Count > 0)
                {
                    if (!restatusFirma.Exists(e => e.Estatus == peticion.Estatus))
                    {
                        throw new ValidacionExcepcion("El estatus no es válido para el proceso.");
                    }
                }

                string signatureId = string.Empty;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    signatureId = con.Solicitudes_TB_SolicitudFirmaDigital.Where(sf => sf.IdSolicitudFirmaDigital == peticion.IdSolicitudFirmaDigital)
                        .Where(sf => sf.IdEstatus == (int)EstatusFirma.Ready
                            && !string.IsNullOrEmpty(sf.SignatureId))
                        .Select(sf => sf.SignatureId)
                        .FirstOrDefault();
                }
                long.TryParse(signatureId, out long valSignatureId);
                if (valSignatureId <= 0)
                {
                    throw new ValidacionExcepcion("La clave de firma no existe o no es válido.");
                }

                NotificacionRequest peticionWs = new NotificacionRequest { SignatureId = valSignatureId, Status = peticion.Estatus };
                using (WsPfLleida ws = new WsPfLleida())
                {
                    bool petcionCorrecta = ws.Post($"{ws.NotificacionV1}", peticionWs, out string jsonResponse);
                    if (!petcionCorrecta)
                    {
                        respuesta.StatusCode = 404;
                        throw new ValidacionExcepcion("No fue posible realizar la petición");
                    }
                    NotificacionResponse respNotificacion = JsonConvert.DeserializeObject<NotificacionResponse>(jsonResponse);

                    if (respNotificacion == null || respNotificacion.Error || respNotificacion.CodigoEstado != 200)
                    {
                        respuesta.Error = true;
                        respuesta.StatusCode = respNotificacion.CodigoEstado;
                        respuesta.MensajeOperacion = respNotificacion.Mensaje;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        #endregion
    }
}
