﻿using Newtonsoft.Json;
using SOPF.Core.BusinessLogic.Solicitudes;
using SOPF.Core.DataAccess;
using SOPF.Core.Lleida;
using SOPF.Core.Model.Request.Lleida;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Request.WsPfLleida;
using SOPF.Core.Model.Response.Lleida;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Model.Response.WsPfLleida;
using SOPF.Core.Poco.Solicitudes;
using SOPF.Core.Solicitudes;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SOPF.Core.BusinessLogic.Lleida
{
    public static class FirmaBLL
    {
        #region GET
        public static ObtenerFirmasResponse ObtenerFirmas(ObtenerFirmasRequest peticion)
        {
            ObtenerFirmasResponse respuesta = new ObtenerFirmasResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta");
                FirmasRequest peticionWs = peticion.Convertir<FirmasRequest>();
                using (WsPfLleida ws = new WsPfLleida())
                {
                    bool petcionCorrecta = ws.Post($"{ws.Firmav1}", peticionWs, out string jsonResponse);
                    if (!petcionCorrecta)
                    {
                        respuesta.StatusCode = 404;
                        throw new ValidacionExcepcion("No fue posible realizar la petición");
                    }
                    FirmasResponse respIniciarF = JsonConvert.DeserializeObject<FirmasResponse>(jsonResponse);

                    if (respIniciarF == null || respIniciarF.Error || respIniciarF.Firmas == null || respIniciarF.CodigoEstado != 200)
                    {
                        respuesta.Error = true;
                        respuesta.StatusCode = respIniciarF.CodigoEstado;
                        respuesta.MensajeOperacion = respIniciarF.Mensaje;
                    }
                    else
                    {
                        respuesta.Firmas = respIniciarF.Firmas;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST
        public static AltaFirmaResponse AltaFirma(AltaFirmaRequest peticion)
        {
            AltaFirmaResponse respuesta = new AltaFirmaResponse();
            try
            {
                using (WsPfLleida ws = new WsPfLleida())
                {
                    bool petcionCorrecta = ws.Post($"{ws.Firmav1}/iniciarfirma", peticion, out string jsonResponse);
                    if (!petcionCorrecta)
                    {
                        respuesta.StatusCode = 404;
                        throw new ValidacionExcepcion("No fue posible realizar la petición");
                    }
                    IniciarFirmaResponse respIniciarF = JsonConvert.DeserializeObject<IniciarFirmaResponse>(jsonResponse);

                    if (respIniciarF == null || respIniciarF.Error || respIniciarF.Firma == null || respIniciarF.CodigoEstado != 200)
                    {
                        respuesta.Error = true;
                        respuesta.StatusCode = respIniciarF.CodigoEstado;
                        respuesta.MensajeOperacion = respIniciarF.Mensaje;
                    }
                    else
                    {
                        respuesta.SigantureId = respIniciarF.Firma.SignatureId;
                    }
                    if (respIniciarF != null) respuesta.IdPeticion = respIniciarF.IdPeticion;
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        public static ActualizarFirmaResponse ActualizarFirma(ActualizarFirmaRequest peticion)
        {
            ActualizarFirmaResponse respuesta = new ActualizarFirmaResponse();
            int[] idEstusActivos = new int[] { (int)EstatusFirma.New, (int)EstatusFirma.Ready };
            EstatusFirma[] rechazado = new EstatusFirma[] {
                EstatusFirma.Expired,
                EstatusFirma.Failed,
                EstatusFirma.Cancelled,
                EstatusFirma.Otp_max_retries };
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición es incorrecta.");
                if (peticion.IdPeticion <= 0) throw new ValidacionExcepcion("La clave de la petición no es correcta");
                if (peticion.IdEstatus <= 0) throw new ValidacionExcepcion("Se espera un estatus.");
                if (string.IsNullOrEmpty(peticion.IdSignature)) throw new ValidacionExcepcion("Se espera la clave del documento generado.");

                int idSolicitud = 0, idSolicitudFirmaDigital = 0, idSolicitudProcesoDigital = 0, idEstatusActual = 0;
                bool activo = false;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_SolicitudFirmaDigital solFirmaDigital = con.Solicitudes_TB_SolicitudFirmaDigital
                        .Where(s => s.Activo
                            && s.IdPeticion == peticion.IdPeticion
                            && s.SignatureId == peticion.IdSignature
                            && idEstusActivos.Contains(s.IdEstatus))
                        .FirstOrDefault();
                    if (solFirmaDigital != null)
                    {
                        idSolicitud = solFirmaDigital.IdSolicitud;
                        idSolicitudFirmaDigital = solFirmaDigital.IdSolicitudFirmaDigital;
                        idSolicitudProcesoDigital = solFirmaDigital.IdSolicitudProcesoDigital;
                        idEstatusActual = solFirmaDigital.IdEstatus;
                        activo = solFirmaDigital.Activo;
                    }
                }
                if (idSolicitudFirmaDigital > 0)
                {
                    ActualizarSolicitudFirmaDigitalRequest actualizaSolicitudF = new ActualizarSolicitudFirmaDigitalRequest
                    {
                        IdSolicitudFirmaDigital = idSolicitudFirmaDigital
                    };
                    if (activo)
                    {
                        if ((EstatusFirma)peticion.IdEstatus == EstatusFirma.Signed)
                        {
                            actualizaSolicitudF.Mensaje = "Proceso de carga de documentos en curso.";
                            actualizaSolicitudF.IdEstatus = (int)EstatusFirma.Ready;
                        }
                        else if ((EstatusFirma)peticion.IdEstatus == EstatusFirma.Evidence_generated)
                        {
                            actualizaSolicitudF.Mensaje = "Proceso de carga de documentos en curso.";
                            actualizaSolicitudF.IdEstatus = (int)EstatusFirma.Ready;
                            _ = Task.Factory.StartNew(() =>
                            {
                                ExpedienteBLL.CargarDocumentos(new CargarDocumentosRequest
                                {
                                    IdSolicitudFirmaDigital = idSolicitudFirmaDigital,
                                    IdSolicitud = idSolicitud,
                                    Archivos = peticion.Archivos
                                });
                            });
                        }
                        else
                        {
                            actualizaSolicitudF.IdEstatus = peticion.IdEstatus;
                        }
                    }
                    _ = SolicitudFirmaDigitalBLL.ActualizarSolicitudFirmaDigital(actualizaSolicitudF);
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region PUT
        public static Model.Response.Lleida.CancelarFirmaResponse CancelarFirma(CancelarFirmaRequest peticion)
        {
            Model.Response.Lleida.CancelarFirmaResponse respuesta = new Model.Response.Lleida.CancelarFirmaResponse();
            try
            {
                int[] idEstatusActivos = { (int)EstatusFirma.New, (int)EstatusFirma.Ready };
                string signatureId = string.Empty;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    signatureId = con.Solicitudes_TB_SolicitudFirmaDigital.Where(sf => sf.IdSolicitudFirmaDigital == peticion.IdSolicitudFirmaDigital)
                        .Where(sf => idEstatusActivos.Contains(sf.IdEstatus)
                            && !string.IsNullOrEmpty(sf.SignatureId))
                        .Select(sf => sf.SignatureId)
                        .FirstOrDefault();
                }
                long.TryParse(signatureId, out long valSignatureId);
                if (valSignatureId <= 0)
                {
                    throw new ValidacionExcepcion("La clave de firma no existe o no es válido.");
                }
                Model.Response.WsPfLleida.CancelarFirmaResponse respCancelar;
                using (WsPfLleida ws = new WsPfLleida())
                {
                    bool petcionCorrecta = ws.Put($"{ws.Firmav1}/{signatureId}/cancelar", peticion, out string jsonResponse);
                    if (!petcionCorrecta)
                    {
                        respuesta.StatusCode = 404;
                        throw new ValidacionExcepcion("No fue posible realizar la petición");
                    }
                    respCancelar = JsonConvert.DeserializeObject<Model.Response.WsPfLleida.CancelarFirmaResponse>(jsonResponse);
                }

                EstatusFirma estatus = EstatusFirma.Cancelled;
                if (respCancelar == null || respCancelar.Error || respCancelar.CodigoEstado != 200)
                {
                    respuesta.Error = true;
                    respuesta.StatusCode = respCancelar.CodigoEstado;
                    respuesta.MensajeOperacion = respCancelar.Mensaje;
                    estatus = EstatusFirma.Failed;
                }
                if (!respCancelar.Error && string.IsNullOrEmpty(respCancelar.Mensaje))
                {
                    respCancelar.Mensaje = $"Se ha cancelado correctamente: por IdUsario: {peticion.IdUsuario}";
                }

                _ = SolicitudFirmaDigitalBLL.ActualizarSolicitudFirmaDigital(new ActualizarSolicitudFirmaDigitalRequest
                {
                    IdSolicitudFirmaDigital = peticion.IdSolicitudFirmaDigital,
                    IdEstatus = (int)estatus,
                    Mensaje = respCancelar.Mensaje
                });
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        public static Model.Response.Lleida.CancelarFirmaResponse CancelarFirmaLocal(CancelarFirmaRequest peticion)
        {
            Model.Response.Lleida.CancelarFirmaResponse respuesta = new Model.Response.Lleida.CancelarFirmaResponse();
            try
            {
                int[] idEstatusActivos = { (int)EstatusFirma.New, (int)EstatusFirma.Ready };
                string signatureId = string.Empty;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    signatureId = con.Solicitudes_TB_SolicitudFirmaDigital.Where(sf => sf.IdSolicitudFirmaDigital == peticion.IdSolicitudFirmaDigital)
                        .Where(sf => idEstatusActivos.Contains(sf.IdEstatus)
                            && !string.IsNullOrEmpty(sf.SignatureId))
                        .Select(sf => sf.SignatureId)
                        .FirstOrDefault();
                }
                long.TryParse(signatureId, out long valSignatureId);
                if (valSignatureId <= 0)
                {
                    throw new ValidacionExcepcion("La clave de firma no existe o no es válido.");
                }

                ActualizarSolicitudFirmaDigitalResponse respCancelar = SolicitudFirmaDigitalBLL.ActualizarSolicitudFirmaDigital(new ActualizarSolicitudFirmaDigitalRequest
                {
                    IdSolicitudFirmaDigital = peticion.IdSolicitudFirmaDigital,
                    IdEstatus = (int)EstatusFirma.Cancelled,
                    Mensaje = $"Cancelado por usuario. IdUsuario: {peticion.IdUsuario}"
                });
                if (respCancelar != null)
                {
                    respuesta.Error = respCancelar.Error;
                    respuesta.MensajeOperacion = respCancelar.MensajeOperacion;
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        #endregion


        #region Metodos privados        
        #endregion
    }
}
