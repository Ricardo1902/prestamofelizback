﻿using Newtonsoft.Json;
using SOPF.Core.Model.Request.WsPfLleida.DosFA;
using SOPF.Core.Model.Response.WsPfLleida.DosFA;
using System;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Lleida
{
    public static class DosFABLL
    {
        const string endPoint = "api/v1/2fa";
        #region GET

        #endregion

        #region POST
        public static EnviarResponse Enviar(EnviarRequest peticion)
        {
            EnviarResponse respuesta = new EnviarResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                using (WsPfLleida ws = new WsPfLleida())
                {
                    bool petcionCorrecta = ws.Post($"{endPoint}/enviar", peticion, out string jsonResponse);
                    if (!petcionCorrecta)
                    {
                        respuesta.CodigoEstado = 404;
                        throw new ValidacionExcepcion("No fue posible realizar la petición");
                    }
                    respuesta = JsonConvert.DeserializeObject<EnviarResponse>(jsonResponse);
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.Mensaje = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.Mensaje = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static VerificarResponse Verificar(VerificarRequest peticion)
        {
            VerificarResponse respuesta = new VerificarResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                using (WsPfLleida ws = new WsPfLleida())
                {
                    bool petcionCorrecta = ws.Post($"{endPoint}/verificar", peticion, out string jsonResponse);
                    if (!petcionCorrecta)
                    {
                        respuesta.CodigoEstado = 404;
                        throw new ValidacionExcepcion("No fue posible realizar la petición");
                    }
                    respuesta = JsonConvert.DeserializeObject<VerificarResponse>(jsonResponse);
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.Mensaje = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.Mensaje = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion
    }
}
