﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Lleida;
using SOPF.Core.Model.Request.Lleida;
using SOPF.Core.Model.Response.Lleida;
using SOPF.Core.Poco.Lleida;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Lleida
{
    public static class ConfiguracionBLL
    {
        #region Constructor
        static ConfiguracionBLL()
        {
        }
        #endregion

        #region Metodos privados

        #endregion

        #region GET
        public static ConfiguracionesResponse Configuraciones()
        {
            ConfiguracionesResponse respuesta = new ConfiguracionesResponse();
            try
            {
                using (WsPfLleida ws = new WsPfLleida())
                {
                    bool petcionCorrecta = ws.Get($"{ws.ConfiguracionV1}", null, out string jsonResponse);
                    if (!petcionCorrecta) throw new ValidacionExcepcion("No fue posible realizar la petición");
                    ConfiguracionesResultado respSignature = JsonConvert.DeserializeObject<ConfiguracionesResultado>(jsonResponse);
                    if (respSignature != null)
                    {
                        int IdConfig = 0;
                        using (CreditOrigContext con = new CreditOrigContext())
                        {
                            IdConfig = con.Lleida_TB_Plantilla
                                .Where(f => f.Activo)
                                .Select(f => f.IdConfig)
                                .DefaultIfEmpty(0)
                                .FirstOrDefault();
                        }
                        if (IdConfig > 0)
                        {
                            ConfiguracionStatusVM respSign = respSignature.Configuraciones.Where(c => c.ConfigId == IdConfig)
                                .FirstOrDefault();
                            if (respSign != null)
                            {
                                respSign.Activo = true;
                            }
                        }
                        respuesta.MensajeOperacion = "Asignación exitosa.";
                        respuesta.Resultado = respSignature;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static EnvioProcesoFirmasResponse EnvioProcesoFirmas()
        {
            EnvioProcesoFirmasResponse respuesta = new EnvioProcesoFirmasResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idConfig = con.Lleida_TB_Plantilla.Where(p => p.Activo)
                        .OrderByDescending(o => o.IdPlantilla)
                        .Select(p => p.IdConfig)
                        .DefaultIfEmpty(0)
                        .FirstOrDefault();

                    respuesta.IdConfig = idConfig;
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST
        public static GeneraPlantillaResponse AsignarPlantilla(GeneraPlantillaRequest peticion)
        {
            GeneraPlantillaResponse respuesta = new GeneraPlantillaResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    GeneraPlantillaResult pa = (from p in con.Lleida_TB_Plantilla
                                                where p.IdConfig == peticion.IdPlantilla
                                                    && p.Activo
                                                select new GeneraPlantillaResult
                                                {
                                                    IdPlantilla = p.IdPlantilla,
                                                    IdConfig = p.IdConfig,
                                                    IdUsuario = p.IdUsuario,
                                                    Activo = p.Activo,
                                                    FechaRegistro = p.FechaRegistro
                                                })
                                                .SingleOrDefault();
                    if (pa != null)
                    {
                        throw new ValidacionExcepcion("Esta plantilla esta asignada desde el " + pa.FechaRegistro.ToString("dd/MM/yyyy"));
                    }
                    else
                    {
                        List<Plantilla> plantillasActivas = (from p in con.Lleida_TB_Plantilla
                                                                where p.Activo
                                                                select p).ToList();

                        foreach (Plantilla p in plantillasActivas)
                        {
                            p.Activo = false;
                        }
                        con.SaveChanges();

                        Plantilla plantilla = new Plantilla()
                        {
                            IdConfig = peticion.IdPlantilla,
                            IdUsuario = peticion.IdUsuario,
                            Activo = true,
                            FechaRegistro = DateTime.Now
                        };
                        con.Lleida_TB_Plantilla.Add(plantilla);
                        con.SaveChanges();
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static DesactivaPlantillaResponse DesactivarPlantilla(DesactivaPlantillaRequest peticion)
        {
            DesactivaPlantillaResponse respuesta = new DesactivaPlantillaResponse();
            try
            {
                using (WsPfLleida ws = new WsPfLleida())
                {
                    var estatusRequest = new
                    {
                        Status = "disabled"
                    };
                    bool petcionCorrecta = ws.Post($"{ws.ConfiguracionV1}/{peticion.IdPlantilla}/estatus", estatusRequest, out string jsonResponse);
                    if (!petcionCorrecta) throw new ValidacionExcepcion("No fue posible realizar la petición");
                    respuesta.MensajeOperacion = "Desactivación exitosa.";
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ActivaPlantillaResponse ActivarPlantilla(ActivaPlantillaRequest peticion)
        {
            ActivaPlantillaResponse respuesta = new ActivaPlantillaResponse();
            try
            {
                using (WsPfLleida ws = new WsPfLleida())
                {
                    var estatusRequest = new
                    {
                        Status = "enabled"
                    };
                    bool petcionCorrecta = ws.Post($"{ws.ConfiguracionV1}/{peticion.IdPlantilla}/estatus", estatusRequest, out string jsonResponse);
                    if (!petcionCorrecta) throw new ValidacionExcepcion("No fue posible realizar la petición");
                    respuesta.MensajeOperacion = "Activación exitosa.";
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion
    }
}
