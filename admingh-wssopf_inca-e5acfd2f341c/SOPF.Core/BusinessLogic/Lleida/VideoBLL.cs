﻿using Newtonsoft.Json;
using SOPF.Core.Model.Request.Lleida;
using SOPF.Core.Model.Request.WsPfLleida;
using SOPF.Core.Model.Response.WsPfLleida;
using System;
using System.Reflection;
using WsPfLleidaM = SOPF.Core.Model.Response.WsPfLleida;

namespace SOPF.Core.BusinessLogic.Lleida
{
    public static class VideoBLL
    {
        #region GET
        public static EstatusVideoResponse Estatus(EstatusVideoRequest peticion)
        {
            EstatusVideoResponse respuesta = new EstatusVideoResponse();
            try
            {
                using (WsPfLleida wsV = new WsPfLleida())
                {
                    bool peticionCorrecta = wsV.Get($"{wsV.VideoV1}/{peticion.IdPeticion}", $"IncluirArchivos={peticion.IncluirArchivos}", out string jsonResponse);
                    if (!peticionCorrecta)
                    {
                        respuesta.CodigoEstado = 404;
                        throw new ValidacionExcepcion("No fue posible realizar la petición");
                    }
                    respuesta = JsonConvert.DeserializeObject<EstatusVideoResponse>(jsonResponse);
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.Mensaje = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.Mensaje = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST
        public static Model.Response.Lleida.NuevoVideoResponse Nuevo(NuevoVideoRequest peticion)
        {
            Model.Response.Lleida.NuevoVideoResponse respuesta = new Model.Response.Lleida.NuevoVideoResponse();
            try
            {
                using (WsPfLleida ws = new WsPfLleida())
                {
                    bool petcionCorrecta = ws.Post($"{ws.VideoV1}", peticion, out string jsonResponse);
                    if (!petcionCorrecta)
                    {
                        respuesta.StatusCode = 404;
                        throw new ValidacionExcepcion("No fue posible realizar la petición");
                    }

                    WsPfLleidaM.NuevoVideoResponse nuevoVideoR = JsonConvert.DeserializeObject<WsPfLleidaM.NuevoVideoResponse>(jsonResponse);

                    if (nuevoVideoR == null || nuevoVideoR.Error || nuevoVideoR.CodigoEstado != 200)
                    {
                        respuesta.Error = true;
                        respuesta.StatusCode = nuevoVideoR.CodigoEstado;
                        respuesta.MensajeOperacion = nuevoVideoR.Mensaje;
                    }
                    else
                    {
                        respuesta = nuevoVideoR.Convertir<Model.Response.Lleida.NuevoVideoResponse>();
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }
        #endregion
    }
}
