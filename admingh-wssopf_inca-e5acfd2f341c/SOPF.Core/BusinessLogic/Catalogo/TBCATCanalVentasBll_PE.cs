﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SOPF.Core.Entities;
using SOPF.Core.DataAccess.Catalogo;

namespace SOPF.Core.BusinessLogic.Catalogo
{
  public static class TBCATCanalVentasBll_PE
  {
    private static TBCATCanalVentasDal_PE dal;

    static TBCATCanalVentasBll_PE()
    {
      dal = new TBCATCanalVentasDal_PE();
    }

    public static List<Combo> ObtenerCATCombo()
    {
      return dal.ObtenerCATCombo();
    }
  }
}
