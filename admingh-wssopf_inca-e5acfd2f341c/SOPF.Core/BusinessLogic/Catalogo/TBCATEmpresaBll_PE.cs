﻿using System.Collections.Generic;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.DataAccess.Catalogo;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATEmpresaBll_PE
    {
        private static TBCATEmpresaDal_PE dal;

        static TBCATEmpresaBll_PE()
        {
            dal = new TBCATEmpresaDal_PE();
        }

        public static List<Combo> ObtenerCATCombo()
        {
            return dal.ObtenerCATCombo();
        }

        public static TB_CATEmpresa Obtener(int IdEmpresa)
        {
            return dal.Obtener(IdEmpresa);
        }
    }
}
