#pragma warning disable 141030
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATBancoBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATBancoDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-10-30 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATBancoBll
    {
        private static TBCATBancoDal dal;

        static TBCATBancoBll()
        {
            dal = new TBCATBancoDal();
        }

        public static void InsertarTBCATBanco(TBCATBanco entidad)
        {
            dal.InsertarTBCATBanco(entidad);			
        }
        
        public static List<TBCATBanco> ObtenerTBCATBanco(int Accion, int idBanco)
        {
            return dal.ObtenerTBCATBanco(Accion,idBanco);
        }
        
        public static void ActualizarTBCATBanco()
        {
            dal.ActualizarTBCATBanco();
        }

        public static void EliminarTBCATBanco()
        {
            dal.EliminarTBCATBanco();
        }
    }
}