﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SOPF.Core.Entities;
using SOPF.Core.DataAccess.Catalogo;

namespace SOPF.Core.BusinessLogic.Catalogo
{
  public static class TBCATProductoBll_PE
  {
    private static TBCATProductoDal_PE dal;

    static TBCATProductoBll_PE()
    {
      dal = new TBCATProductoDal_PE();
    }

    public static List<Combo> ObtenerCATCombo()
    {
      return dal.ObtenerCATCombo();
    }
  }
}
