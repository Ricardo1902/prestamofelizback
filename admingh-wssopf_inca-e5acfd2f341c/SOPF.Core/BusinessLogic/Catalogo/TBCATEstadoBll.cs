#pragma warning disable 141030
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATEstadoBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATEstadoDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-10-30 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATEstadoBll
    {
        private static TBCATEstadoDal dal;

        static TBCATEstadoBll()
        {
            dal = new TBCATEstadoDal();
        }

        public static void InsertarTBCATEstado(TBCATEstado entidad)
        {
            dal.InsertarTBCATEstado(entidad);			
        }

        public static List<TBCATEstado> ObtenerTBCATEstado(int accion, int idEstado, string estado, int idEstatus)
        {
            return dal.ObtenerTBCATEstado(accion, idEstado, estado, idEstatus);
        }
        
        public static void ActualizarTBCATEstado()
        {
            dal.ActualizarTBCATEstado();
        }

        public static void EliminarTBCATEstado()
        {
            dal.EliminarTBCATEstado();
        }
    }
}