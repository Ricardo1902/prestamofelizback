#pragma warning disable 160330
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.DataAccess.Catalogo;
using SOPF.Core.Entities;

# region Copyright Dimex – 2016
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATConvenioBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATConvenioDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2016-03-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATConvenioBll
    {
        private static TBCATConvenioDal dal;

        static TBCATConvenioBll()
        {
            dal = new TBCATConvenioDal();
        }

        public static void InsertarTBCATConvenio(TBCATConvenio entidad)
        {
            dal.InsertarTBCATConvenio(entidad);			
        }

        public static List<TBCATConvenio> ObtenerTBCATConvenio(int Accion, int idconvenio, string convenio, int idEstatus)
        {
            return dal.ObtenerTBCATConvenio(Accion,idconvenio,convenio,idEstatus);
        }
        
        public static void ActualizarTBCATConvenio()
        {
            dal.ActualizarTBCATConvenio();
        }

        public static void EliminarTBCATConvenio()
        {
            dal.EliminarTBCATConvenio();
        }

        public static List<Combo> Sistema_ObtenerCATCombo(int IDTipoConvenio)
        {
            return dal.Sistema_ObtenerCATCombo(IDTipoConvenio);
        }

        public static List<Combo> ObtenerCATCombo(int IDTipoConvenio)
        {
          return dal.ObtenerCATCombo(IDTipoConvenio);
        }
    }
}