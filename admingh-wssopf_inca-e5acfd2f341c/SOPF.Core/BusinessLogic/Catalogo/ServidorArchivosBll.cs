﻿using SOPF.Core.DataAccess.Catalogo;
using SOPF.Core.Entities;
using SOPF.Core.Model.Catalogo;
using SOPF.Core.Poco.Catalogo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class ServidorArchivosBll
    {
        private static ServidorArchivosDal dal;
        static ServidorArchivosBll()
        {
            dal = new ServidorArchivosDal(new DataAccess.CreditOrigContext());
        }
        public static ServidorArchivosVM ObtenerServidorArchivosPorId(int idServidorArchivos)
        {
            return dal.ObtenerServidorArchivosPorId(idServidorArchivos);
        }
        public static List<ServidorArchivosVM> ObtenerServidoresArchivos()
        {
            return dal.ObtenerServidoresArchivos();
        }
        public static Resultado<bool> InsertarServidorArchivos(ServidorArchivos servidorArchivos)
        {
            return dal.InsertarServidorArchivos(servidorArchivos);
        }
        public static Resultado<bool> ActualizarServidorArchivos(ServidorArchivos servidorArchivos)
        {
            return dal.ActualizarServidorArchivos(servidorArchivos);
        }
        public static Resultado<bool> EliminarServidorArchivos(int idServidorArchivos)
        {
            return dal.EliminarServidorArchivos(idServidorArchivos);
        }
    }
}
