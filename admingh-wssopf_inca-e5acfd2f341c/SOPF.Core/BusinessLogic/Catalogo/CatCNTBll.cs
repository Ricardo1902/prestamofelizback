﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.DataAccess.Catalogo;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class CatCNTBll
    {
        private static CatCNTDal dal;
        static CatCNTBll()
        {
            dal = new CatCNTDal();        
        }
        public static List<CatCNT>ObtenerCatCNT(int Accion, string Descripcion)
        {
            return dal.ObtenerCatCNT(Accion,Descripcion);
        }

    }
}
