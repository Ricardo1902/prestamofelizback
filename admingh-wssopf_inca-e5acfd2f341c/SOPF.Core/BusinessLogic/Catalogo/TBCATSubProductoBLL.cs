﻿using SOPF.Core.DataAccess.Catalogo;
using SOPF.Core.Entities.Catalogo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public class TBCATSubProductoBLL
    {
        private static TBCATSubProductoDal dal;

        static TBCATSubProductoBLL()
        {
            dal = new TBCATSubProductoDal();
        }

        public static List<TBCATSubProducto> ObtenerSubProductos(TBCATSubProducto entidad)
        {
            return dal.ObtenerSubProductos(entidad);
        }

        public static List<TBCATSubProducto> ObtenerSubProductosPorConvenio(int IdConvenio)
        {
            return dal.ObtenerSubProductosPorConvenio(IdConvenio);
        }
    }
}
