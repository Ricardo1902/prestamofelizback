#pragma warning disable 160817
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.DataAccess.Catalogo;
using SOPF.Core.Model.Response.Catalogo;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Catalogo;
using System.Reflection;
using Newtonsoft.Json;

#region Copyright Dimex – 2016
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TBCatCatalogosBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCatCatalogosDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2016-08-17 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCatCatalogosBll
    {
        private static TBCatCatalogosDal dal;

        static TBCatCatalogosBll()
        {
            dal = new TBCatCatalogosDal();
        }

        public static void InsertarTBCatCatalogos(TBCatCatalogos entidad)
        {
            dal.InsertarTBCatCatalogos(entidad);			
        }

        public static List<TBCatCatalogos> ObtenerTBCatCatalogos(int Accion, int IdTipo, string valor, int idestatus, string Tipo)
        {
            return dal.ObtenerTBCatCatalogos(Accion,IdTipo,valor,idestatus,Tipo);
        }
        
        public static void ActualizarTBCatCatalogos()
        {
            dal.ActualizarTBCatCatalogos();
        }

        public static void EliminarTBCatCatalogos()
        {
            dal.EliminarTBCatCatalogos();
        }

        public static ObtenerOrigenClienteEdicionResponse ObtenerOrigenClienteEdicion()
        {
            ObtenerOrigenClienteEdicionResponse respuesta = new ObtenerOrigenClienteEdicionResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta.Origenes = con.Catalogo_TB_CATOrigenClienteEdicion.Select(oce => new OrigenClienteEdicionVM { IdOrigenClienteEdicion = oce.IdOrigenClienteEdicion, Url = oce.Url }).ToList();
                    if (respuesta.Origenes == null) throw new Exception("No fue posible obtener los origenes para editar cliente");
                }
            }

            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
    }
}