﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Catalogo;

namespace SOPF.Core.BusinessLogic
{
    public static  class TBCATTipoCreditoBll
    {
      private static TBCATTipoCreditoDal dal;

      static TBCATTipoCreditoBll()
        {
            dal = new TBCATTipoCreditoDal();
        }

      public static List<TBCATTipoCredito> ObtenerTBCATTipoCredito(int idCliente)
      {
          return dal.ObtenerTBCATTipoCredito(idCliente);
      }


    }
}
