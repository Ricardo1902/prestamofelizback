﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Poco.Catalogo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TipoProgramacionBLL
    {
        #region GET
        public static List<TB_CATTipoProgramacion> TipoProgramaciones()
        {
            return TipoProgramacionesFiltro(null);
        }

        public static List<TB_CATTipoProgramacion> TipoProgramaciones(string tipoProgramacion)
        {
            return TipoProgramacionesFiltro(tp => tp.TipoProgramacion == tipoProgramacion);
        }

        public static List<TB_CATTipoProgramacion> TipoProgramaciones(string tipoProgramacion, bool activo)
        {
            return TipoProgramacionesFiltro(tp => tp.TipoProgramacion == tipoProgramacion && tp.Activo == activo);
        }

        public static List<TB_CATTipoProgramacion> TipoProgramaciones(TipoProgramacionesRequest peticion)
        {
            if (peticion == null) return null;
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    IQueryable<TB_CATTipoProgramacion> filtro = con.Catalogo_TB_CATTipoProgramacion;
                    if (peticion.Activo != null)
                    {
                        filtro = filtro.Where(tp => tp.Activo == peticion.Activo);
                    }
                    if (!string.IsNullOrEmpty(peticion.TipoProgramacion))
                    {
                        filtro = filtro.Where(tp => tp.TipoProgramacion == peticion.TipoProgramacion);
                    }
                    return filtro.ToList();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                      $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
                return null;
            }
        }

        #endregion

        #region POST

        #endregion

        #region Metodos Privados
        private static List<TB_CATTipoProgramacion> TipoProgramacionesFiltro(Expression<Func<TB_CATTipoProgramacion, bool>> filtro)
        {
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    return filtro == null ? con.Set<TB_CATTipoProgramacion>().ToList() :
                        con.Set<TB_CATTipoProgramacion>().Where(filtro).ToList();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(filtro)}", JsonConvert.SerializeObject(ex));
                return null;
            }
        }
        #endregion
    }
}
