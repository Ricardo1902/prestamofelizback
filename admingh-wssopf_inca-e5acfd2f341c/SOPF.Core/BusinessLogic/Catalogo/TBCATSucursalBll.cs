﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.DataAccess.Catalogo;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATSucursal.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATGiroDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public class TBCATSucursalBll
    {
        private static TBCATSucursalDal dal;

        static TBCATSucursalBll()
        {
            dal = new TBCATSucursalDal();
        }

        public static List<TBCATSucursal> ObtenerSucursal(TBCATSucursal entidad)
        {
            return dal.ObtenerSucursal(entidad);
        }

        //ModificarSolicitud-LP
        public static List<Combo> ObtenerCATCombo(int IDUsuarioRegional)
        {
          return dal.ObtenerCATCombo(IDUsuarioRegional);
        }

    }
}
