#pragma warning disable 141030
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATCiudadBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATCiudadDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-10-30 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATCiudadBll
    {
        private static TBCATCiudadDal dal;

        static TBCATCiudadBll()
        {
            dal = new TBCATCiudadDal();
        }

        public static void InsertarTBCATCiudad(TBCATCiudad entidad)
        {
            dal.InsertarTBCATCiudad(entidad);			
        }

        public static List<TBCATCiudad> ObtenerTBCATCiudad(int accion, int idCiudad, string ciudad, int idEstatus, int IdEstado)
        {
            return dal.ObtenerTBCATCiudad(accion, idCiudad, ciudad, idEstatus, IdEstado);
        }
        public static List<TBCATCiudad> ObtenerTBCATCiudadOrig(int accion,  int IdEstado,int idCiudad)
        {
            return dal.ObtenerTBCATCiudadOrig(accion, IdEstado, idCiudad);
        }
        
        public static void ActualizarTBCATCiudad()
        {
            dal.ActualizarTBCATCiudad();
        }

        public static void EliminarTBCATCiudad()
        {
            dal.EliminarTBCATCiudad();
        }
    }
}