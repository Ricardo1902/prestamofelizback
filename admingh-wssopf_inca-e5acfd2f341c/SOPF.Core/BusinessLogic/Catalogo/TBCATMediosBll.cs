﻿#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATGiroBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATGiroDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATMediosBll
    {
        private static TBCATMediosDal dal;

        static TBCATMediosBll()
        {
            dal = new TBCATMediosDal();
        }

       /* public static List<TBCATMedios> ObtenerTBCATMedios(int Accion, int idMedio, string Medio, int idestatus)
        {
            return dal.ObtenerTBCATMedios(Accion, idMedio, Medio, idestatus);
        }
        */

        public static DataSet ObtenerTBCATMedios(int Accion, int idMedio, string Medio, int idestatus)
        {
            DataSet dsMedios = new DataSet();
            DataTable dtMedios = new DataTable();
            List<TBCATMedios> listMedios = new List<TBCATMedios>();
            listMedios = dal.ObtenerTBCATMedios(Accion, idMedio, Medio, idestatus);

            dtMedios = ConvertToDatatable(listMedios);
            dsMedios.Tables.Add(dtMedios);

            return dsMedios;
        }

        
        public static DataTable ConvertToDatatable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
    }
}