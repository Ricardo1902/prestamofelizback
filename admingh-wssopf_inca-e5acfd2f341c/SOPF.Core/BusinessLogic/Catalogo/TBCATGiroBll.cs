#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATGiroBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATGiroDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATGiroBll
    {
        private static TBCATGiroDal dal;

        static TBCATGiroBll()
        {
            dal = new TBCATGiroDal();
        }

        public static void InsertarTBCATGiro(TBCATGiro entidad)
        {
            dal.InsertarTBCATGiro(entidad);			
        }

        public static List<TBCATGiro> ObtenerTBCATGiro(int Accion, int idGiro, string Giro, int idestatus)
        {
            return dal.ObtenerTBCATGiro(Accion,idGiro,Giro,idestatus);
        }
        
        public static void ActualizarTBCATGiro()
        {
            dal.ActualizarTBCATGiro();
        }

        public static void EliminarTBCATGiro()
        {
            dal.EliminarTBCATGiro();
        }
    }
}