﻿using SOPF.Core.DataAccess.Catalogo;
using SOPF.Core.Entities.Catalogo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TB_CATParametroBll
    {
        private static TB_CATParametroDal dal;

        static TB_CATParametroBll()
        {
            dal = new TB_CATParametroDal();
        }

        public static List<TB_CATParametro> ObtenerCATParametros()
        {
            return dal.ObtenerCATParametros();
        }

        public static bool ActualizarCATParametros(int IdParametro, string Parametro, string Valor, string Descripcion, int IdEstatus)
        {
            return dal.ActualizarCATParametros(IdParametro, Parametro, Valor, Descripcion, IdEstatus);
        }
    }
}
