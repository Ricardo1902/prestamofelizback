#pragma warning disable 150412
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATTipoConvenioBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATTipoConvenioDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-12 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATTipoConvenioBll
    {
        private static TBCATTipoConvenioDal dal;

        static TBCATTipoConvenioBll()
        {
            dal = new TBCATTipoConvenioDal();
        }

        public static void InsertarTBCATTipoConvenio(TBCATTipoConvenio entidad)
        {
            dal.InsertarTBCATTipoConvenio(entidad);			
        }

        public static List<TBCATTipoConvenio> ObtenerTBCATTipoConvenio(int Accion, int TipoConvenio, string convenio, int IdEstatus, int producto)
        {
            return dal.ObtenerTBCATTipoConvenio(Accion, TipoConvenio, convenio, IdEstatus, producto);
        }
        
        public static void ActualizarTBCATTipoConvenio()
        {
            dal.ActualizarTBCATTipoConvenio();
        }

        public static void EliminarTBCATTipoConvenio()
        {
            dal.EliminarTBCATTipoConvenio();
        }

        //ModificarSolicitud-LP
        public static List<Combo> ObtenerCATCombo()
        {
          return dal.ObtenerCATCombo();
        }
    }
}