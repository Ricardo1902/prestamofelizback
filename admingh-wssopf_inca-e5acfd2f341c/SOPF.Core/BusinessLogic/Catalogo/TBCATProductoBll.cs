#pragma warning disable 160330
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.DataAccess.Catalogo;

# region Copyright Dimex – 2016
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATProductoBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATProductoDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2016-03-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATProductoBll
    {
        private static TBCATProductoDal dal;

        static TBCATProductoBll()
        {
            dal = new TBCATProductoDal();
        }

        public static void InsertarTBCATProducto(TBCATProducto entidad)
        {
            dal.InsertarTBCATProducto(entidad);			
        }

        public static List<TBCATProducto> ObtenerTBCATProducto(int Accion, int idProducto, string Producto, int idEstatus, int TipoReno, int IdTipoCredito)
        {
            return dal.ObtenerTBCATProducto(Accion,idProducto,Producto,idEstatus,TipoReno, IdTipoCredito);
        }
        
        public static void ActualizarTBCATProducto()
        {
            dal.ActualizarTBCATProducto();
        }

        public static void EliminarTBCATProducto()
        {
            dal.EliminarTBCATProducto();
        }
    }
}