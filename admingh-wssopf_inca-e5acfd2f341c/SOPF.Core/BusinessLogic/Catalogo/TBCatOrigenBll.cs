﻿using SOPF.Core.DataAccess.Catalogo;
using SOPF.Core.Entities.Catalogo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCatOrigenBll
    {
        private static TBCatOrigenDal dal;

        static TBCatOrigenBll()
        {
            dal = new TBCatOrigenDal();
        }

        public static List<TBCatOrigen> ObtenerTBCatOrigen(int Accion)
        {
            return dal.ObtenerTBCatOrigen(Accion);
        }
    }
}
