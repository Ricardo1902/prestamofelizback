﻿using System.Collections.Generic;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess.Catalogo;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATOrganoPagoBll_PE
    {
        private static TBCATOrganoPagoaDal_PE dal;

        static TBCATOrganoPagoBll_PE()
        {
            dal = new TBCATOrganoPagoaDal_PE();
        }

        public static List<Combo> ObtenerCATCombo()
        {
            return dal.ObtenerCATCombo();
        }
    }
}
