﻿using SOPF.Core.DataAccess;
using SOPF.Core.Model.Dbo;
using SOPF.Core.Poco.dbo;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TB_CATEstatusBLL
    {
        public static List<TB_CATEstatus> ObtenerEstatusAmpliacionExpress()
        {
            List<TB_CATEstatus> estatus = new List<TB_CATEstatus>();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    estatus = con.dbo_TB_CATEstatus
                        .Where(e => e.IdTipoEstatus == 14
                            && e.Ayuda.Equals("Para RR de Solicitud Ampliación Express", StringComparison.InvariantCultureIgnoreCase))
                        .ToList();
                }
            }
            catch (Exception ex)
            {
            }
            return estatus;
        }

        public static List<EstatusVM> EstatusProcesoFirmaDigital()
        {
            List<EstatusVM> estatus = new List<EstatusVM>();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idTipoEsatus = con.Catalogo_TB_TipoEstatus
                        .Where(te => te.Tipo == "Firma Digital")
                        .Select(te => te.IdTipoEstatus)
                        .DefaultIfEmpty(15)
                        .FirstOrDefault();

                    List<EstatusVM> respEstaus = con.dbo_TB_CATEstatus
                        .Where(e => e.IdTipoEstatus == idTipoEsatus)
                        .Select(e => new EstatusVM
                        {
                            IdEstatus = e.IdEstatus,
                            Estatus = e.Estatus,
                            Descripcion = e.Descripcion
                        })
                        .ToList();
                    if (respEstaus != null && respEstaus.Count > 0)
                    {
                        estatus = respEstaus;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return estatus;
        }

        public static List<EstatusVM> EstatusProcesoDigital()
        {
            List<EstatusVM> estatus = new List<EstatusVM>();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idTipoEsatus = con.Catalogo_TB_TipoEstatus
                        .Where(te => te.Tipo == AppSettings.TipoEstatusProcesoDigital)
                        .Select(te => te.IdTipoEstatus)
                        .DefaultIfEmpty(16)
                        .FirstOrDefault();

                    List<EstatusVM> respEstaus = con.dbo_TB_CATEstatus
                        .Where(e => e.IdTipoEstatus == idTipoEsatus)
                        .Select(e => new EstatusVM
                        {
                            IdEstatus = e.IdEstatus,
                            Estatus = e.Estatus,
                            Descripcion = e.Descripcion
                        })
                        .ToList();
                    if (respEstaus != null && respEstaus.Count > 0)
                    {
                        estatus = respEstaus;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return estatus;
        }

        public static List<EstatusVM> EstatusGestionPeticionDom()
        {
            List<EstatusVM> estatus = new List<EstatusVM>();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idTipoEsatus = con.Catalogo_TB_TipoEstatus
                        .Where(te => te.Tipo == AppSettings.TipoEstatusGestionPeticionDom)
                        .Select(te => te.IdTipoEstatus)
                        .DefaultIfEmpty(20)
                        .FirstOrDefault();

                    List<EstatusVM> respEstaus = con.dbo_TB_CATEstatus
                        .Where(e => e.IdTipoEstatus == idTipoEsatus)
                        .Select(e => new EstatusVM
                        {
                            IdEstatus = e.IdEstatus,
                            Estatus = e.Estatus,
                            Descripcion = e.Descripcion
                        })
                        .ToList();
                    if (respEstaus != null && respEstaus.Count > 0)
                    {
                        estatus = respEstaus;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return estatus;
        }

        public static List<EstatusVM> EstatusLayoutsGenerados()
        {
            List<EstatusVM> estatus = new List<EstatusVM>();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idTipoEsatus = con.Catalogo_TB_TipoEstatus
                        .Where(te => te.Tipo == AppSettings.TipoEstatusLayoutsGenerados)
                        .Select(te => te.IdTipoEstatus)
                        .DefaultIfEmpty(21)
                        .FirstOrDefault();

                    List<EstatusVM> respEstaus = con.dbo_TB_CATEstatus
                        .Where(e => e.IdTipoEstatus == idTipoEsatus)
                        .Select(e => new EstatusVM
                        {
                            IdEstatus = e.IdEstatus,
                            Estatus = e.Estatus,
                            Descripcion = e.Descripcion
                        })
                        .ToList();
                    if (respEstaus != null && respEstaus.Count > 0)
                    {
                        estatus = respEstaus;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return estatus;
        }
    }
}
