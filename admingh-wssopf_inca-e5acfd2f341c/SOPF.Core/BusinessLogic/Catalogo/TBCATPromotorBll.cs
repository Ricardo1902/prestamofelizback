#pragma warning disable 141229
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATPromotorBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATPromotorDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-12-29 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATPromotorBll
    {
        private static TBCATPromotorDal dal;

        static TBCATPromotorBll()
        {
            dal = new TBCATPromotorDal();
        }

        public static void InsertarTBCATPromotor(TBCATPromotor entidad)
        {
         //   dal.InsertarTBCATPromotor(entidad);			
        }
        
        public static List<TBCATPromotor> ObtenerTBCATPromotor(int Accion,  int Sucursal)
        {
            return dal.ObtenerTBCATPromotor(Accion, Sucursal);
        }

        public static List<TBCATPromotor> ObtenerTBCATPromotorPorCanalVenta(int Sucursal, int IdCanalVenta)
        {
            return dal.ObtenerTBCATPromotorPorCanalVenta(Sucursal, IdCanalVenta);
        }

        public static void ActualizarTBCATPromotor()
        {
            dal.ActualizarTBCATPromotor();
        }

        public static void EliminarTBCATPromotor()
        {
            dal.EliminarTBCATPromotor();
        }

        //ModificarSolicitud-LP
        public static List<Combo> ObtenerCATCombo(int IDSucursal)
        {
          return dal.ObtenerCATCombo(IDSucursal);
        }
    }
}