﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SOPF.Core.Entities;
using SOPF.Core.DataAccess.Catalogo;

namespace SOPF.Core.BusinessLogic.Catalogo
{
  public static class TBCATSituacionLaboralBll_PE
  {
    private static TBCATSituacionLaboralDal_PE dal;

    static TBCATSituacionLaboralBll_PE()
    {
      dal = new TBCATSituacionLaboralDal_PE();
    }

    public static List<Combo> ObtenerCATCombo()
    {
      return dal.ObtenerCATCombo();
    }
  }
}
