﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SOPF.Core.Entities;
using SOPF.Core.DataAccess.Catalogo;

namespace SOPF.Core.BusinessLogic.Catalogo
{
  public static class TBCATTipoCreditoBll_PE
  {
    private static TBCATTipoCreditoDal_PE dal;

    static TBCATTipoCreditoBll_PE()
    {
      dal = new TBCATTipoCreditoDal_PE();
    }

    public static List<Combo> ObtenerCATCombo()
    {
      return dal.ObtenerCATCombo();
    }
  }

}
