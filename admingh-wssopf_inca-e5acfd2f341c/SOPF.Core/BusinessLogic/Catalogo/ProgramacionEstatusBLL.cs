﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Poco.Catalogo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public class ProgramacionEstatusBLL
    {
        public static List<TB_ProgramacionEstatus> ProgramacionEstatus(ProgramacionEstatusRequest peticion)
        {
            try
            {
                if (peticion == null) return null;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    IQueryable<TB_ProgramacionEstatus> filtro = con.Catalogo_TB_ProgramacionEstatus;
                    if (!string.IsNullOrEmpty(peticion.ClaveProgramacion))
                    {
                        filtro = filtro.Where(p => p.ClaveProgramacion == peticion.ClaveProgramacion);
                    }

                    return filtro.ToList();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
                return null;
            }
        }
    }
}
