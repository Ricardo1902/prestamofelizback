﻿using SOPF.Core.DataAccess;
using SOPF.Core.Model.Dbo;
using SOPF.Core.Poco.dbo;
using System;
using System.Linq;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TipoEstatusBLL
    {
        /// <summary>
        /// Obtiene el tipo estatus del catálogo de Estaus
        /// </summary>
        /// <param name="ayuda"></param>
        /// <returns></returns>
        public static TB_CATEstatus ObtenerTipoEstatus(string ayuda)
        {
            TB_CATEstatus tipoEstatus = new TB_CATEstatus();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var tiEs = con.dbo_TB_CATEstatus
                            .Where(te => te.Ayuda.Equals(ayuda, StringComparison.CurrentCultureIgnoreCase))
                            .FirstOrDefault();
                    if (tiEs != null)
                    {
                        tipoEstatus = tiEs;
                    }
                }
            }
            catch (Exception e)
            {
            }
            return tipoEstatus;
        }

        /// <summary>
        /// Devuelte el tipo de estatus
        /// Devuelve nulo si el tipo no existe
        /// </summary>
        /// <param name="tipo">tipo estatus a buscar</param>
        /// <returns>TipoEstatusVM</returns>
        /// <exception cref="System.Exception">Ocurre una excepcion si no se logra una conexion a datos o cualquier otro error. 
        /// El objeto resultante puede ser nulo.
        /// </exception>
        public static TipoEstatusVM TipoEstatus(string tipo)
        {
            TipoEstatusVM respuesta = null;
            if (string.IsNullOrEmpty(tipo)) return respuesta;
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Catalogo_TB_TipoEstatus
                        .Where(te => te.Tipo == tipo)
                        .FirstOrDefault()
                        .Convertir<TipoEstatusVM>();
                }
            }
            catch (Exception)
            {
            }
            return respuesta;
        }
    }
}
