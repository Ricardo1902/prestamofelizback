﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SOPF.Core.Entities;
using SOPF.Core.DataAccess.Catalogo;

namespace SOPF.Core.BusinessLogic.Catalogo
{
  public static class TBCATTipoEvaluacionBll_PE
  {
    private static TBCATTipoEvaluacionDal_PE dal;

    static TBCATTipoEvaluacionBll_PE()
    {
      dal = new TBCATTipoEvaluacionDal_PE();
    }

    public static List<Combo> ObtenerCATCombo()
    {
      return dal.ObtenerCATCombo();
    }
  }
}
