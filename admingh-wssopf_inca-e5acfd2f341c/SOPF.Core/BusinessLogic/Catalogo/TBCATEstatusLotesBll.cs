﻿#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.DataAccess;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATGiroBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATGiroDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATEstatusLotesBll
    {
        private static TBCATEstatusLotesDal dal;

        static TBCATEstatusLotesBll()
        {
            dal = new TBCATEstatusLotesDal();
        }

        public static List<TBCATEstatusLotes> ConsultaEstatusLotes(int Accion, int IdEstatusLote, string EstatusLote, int idestatus)
        {
            return dal.ConsultaEstatusLotes(Accion, IdEstatusLote, EstatusLote, idestatus);
        }

        /*public static DataSet ConsultaEstatusLotes(int Accion, int IdEstatusLote, string EstatusLote, int idestatus)
        {
            DataSet dsEstatusLotes = new DataSet();
            DataTable dtEstatusLotes = new DataTable();
            List<TBCATEstatusLotes> listEstatusLotes = new List<TBCATEstatusLotes>();
            listEstatusLotes = dal.ConsultaEstatusLotes(Accion, IdEstatusLote, EstatusLote, idestatus);

            dtEstatusLotes = ConvertToDatatable(listEstatusLotes);
            dsEstatusLotes.Tables.Add(dtEstatusLotes);

            return dsEstatusLotes;
        }

        
        public static DataTable ConvertToDatatable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }*/
    }
}