#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.DataAccess.Catalogo;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATTipoVadeBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATTipoVadeDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATTipoVadeBll
    {
        private static TBCATTipoVadeDal dal;

        static TBCATTipoVadeBll()
        {
            dal = new TBCATTipoVadeDal();
        }

        public static void InsertarTBCATTipoVade(TBCATTipoVade entidad)
        {
            dal.InsertarTBCATTipoVade(entidad);			
        }
        
        public static List<TBCATTipoVade> ObtenerTBCATTipoVade(int Tipo)
        {
            return dal.ObtenerTBCATTipoVade(Tipo);
        }
        
        public static void ActualizarTBCATTipoVade()
        {
            dal.ActualizarTBCATTipoVade();
        }

        public static void EliminarTBCATTipoVade()
        {
            dal.EliminarTBCATTipoVade();
        }
    }
}