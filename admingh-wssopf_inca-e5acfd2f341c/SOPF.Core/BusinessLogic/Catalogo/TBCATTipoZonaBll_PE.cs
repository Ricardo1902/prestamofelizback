﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SOPF.Core.Entities;
using SOPF.Core.DataAccess.Catalogo;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATTipoZonaBll_PE
    {
        private static TBCATTipoZonaDal_PE dal;

        static TBCATTipoZonaBll_PE()
        {
            dal = new TBCATTipoZonaDal_PE();
        }

        public static List<Combo> ObtenerCATCombo()
        {
            return dal.ObtenerCATCombo();
        }
    }
}
