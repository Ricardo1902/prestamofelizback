﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Catalogo;
using SOPF.Core.Model.Response.Catalogo;
using System;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBTipoCobroBll
    {
        public static ObtenerTiposCobroResponse ObtenerTiposCobro()
        {
            ObtenerTiposCobroResponse respuesta = new ObtenerTiposCobroResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta.Tipos = con.Catalogo_TB_TipoCobro.Select(tc => new TipoCobroVM { IdTipoCobro = tc.id, TipoCobro = tc.TipoCobro }).ToList();
                    if (respuesta.Tipos == null) throw new Exception("No fue posible obtener los tipos de cobro");
                }
            }

            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
    }
}
