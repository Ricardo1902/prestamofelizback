﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SOPF.Core.Entities;
using SOPF.Core.DataAccess.Catalogo;

namespace SOPF.Core.BusinessLogic.Catalogo
{
  public class TBCATTipoDocumentoBll_PE
  {
    private static TBCATTipoDocumentoDal_PE dal;

    static TBCATTipoDocumentoBll_PE()
    {
      dal = new TBCATTipoDocumentoDal_PE();
    }

    public static List<Combo> ObtenerCATCombo()
    {
      return dal.ObtenerCATCombo();
    }
  }
}
