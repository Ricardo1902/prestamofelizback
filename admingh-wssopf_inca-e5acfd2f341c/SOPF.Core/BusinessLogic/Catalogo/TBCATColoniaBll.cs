#pragma warning disable 141030
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Catalogo;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TBCATColoniaBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCATColoniaDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-10-30 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class TBCATColoniaBll
    {
        private static TBCATColoniaDal dal;

        static TBCATColoniaBll()
        {
            dal = new TBCATColoniaDal();
        }

        public static void InsertarTBCATColonia(TBCATColonia entidad)
        {
            dal.InsertarTBCATColonia(entidad);			
        }
        
        public static List<TBCATColonia> ObtenerTBCATColonia(int Accion, int Ciudad, int idcolonia)
        {
            return dal.ObtenerTBCATColonia(Accion,Ciudad,idcolonia);
        }
        
        public static void ActualizarTBCATColonia()
        {
            dal.ActualizarTBCATColonia();
        }

        public static void EliminarTBCATColonia()
        {
            dal.EliminarTBCATColonia();
        }
        public static DetalleColoniaVM ObtenerDetalleColonia(int accion, int idColonia)
        {
            return dal.ObtenerDetalleColonia(accion, idColonia);
        }
    }
}