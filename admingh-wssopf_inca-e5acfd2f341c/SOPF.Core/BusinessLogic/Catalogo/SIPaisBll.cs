#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.DataAccess.Catalogo;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: SIPaisBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase SIPaisDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Catalogo
{
    public static class SIPaisBll
    {
        private static SIPaisDal dal;

        static SIPaisBll()
        {
            dal = new SIPaisDal();
        }

        public static void InsertarSIPais(SIPais entidad)
        {
            dal.InsertarSIPais(entidad);			
        }
        
        public static List<SIPais> ObtenerSIPais()
        {
            return dal.ObtenerSIPais();
        }
        
        public static void ActualizarSIPais()
        {
            dal.ActualizarSIPais();
        }

        public static void EliminarSIPais()
        {
            dal.EliminarSIPais();
        }
    }
}