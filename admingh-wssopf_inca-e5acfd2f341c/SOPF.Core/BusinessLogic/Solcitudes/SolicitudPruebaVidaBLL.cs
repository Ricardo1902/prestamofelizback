﻿using Newtonsoft.Json;
using OfficeOpenXml;
using SOPF.Core.BusinessLogic.Lleida;
using SOPF.Core.BusinessLogic.Sistema;
using SOPF.Core.BusinessLogic.Solcitudes;
using SOPF.Core.DataAccess;
using SOPF.Core.Lleida;
using SOPF.Core.Model;
using SOPF.Core.Model.Lleida;
using SOPF.Core.Model.Request.Lleida;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Request.WsPfLleida;
using SOPF.Core.Model.Response.Lleida;
using SOPF.Core.Model.Response.Sistema;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Poco.Solicitudes;
using SOPF.Core.Solicitudes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SOPF.Core.BusinessLogic.Solicitudes
{
    public class SolicitudPruebaVidaBLL
    {
        static readonly int idProcesoOriginacion = 0;
        static readonly int idProcesoReestructura = 0;

        static SolicitudPruebaVidaBLL()
        {
            TB_Proceso proceso = ProcesoBLL.Originacion();
            if (proceso != null)
            {
                idProcesoOriginacion = proceso.IdProceso;
            }

            proceso = ProcesoBLL.PetionReestructura();
            if (proceso != null)
            {
                idProcesoReestructura = proceso.IdProceso;
            }
        }

        #region GET
        public static SolicitudPruebaVidaResponse PruebasVida(SolicitudPruebaVidaRequest peticion)
        {
            SolicitudPruebaVidaResponse respuesta = new SolicitudPruebaVidaResponse();
            try
            {
                #region validaciones
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                #endregion

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Solicitudes_SP_SolicitudPruebaVidaCON(peticion);
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST
        public static ValidarEnvioProcesoDigitalResponse ValidarEnvio(ValidarEnvioProcesoDigitalRequest peticion)
        {
            ValidarEnvioProcesoDigitalResponse respuesta = new ValidarEnvioProcesoDigitalResponse();
            try
            {
                if (peticion == null || peticion.Solicitud == null || peticion.ProcesoDigital == null)
                    throw new ValidacionExcepcion("Los datos proporcionados no son válidos.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válida.");

                List<TB_SolicitudPruebaVida> pruebaVida = null;
                string claveEnvio = peticion.Solicitud.IdSolicitud.ToString();

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    pruebaVida = (from pd in con.Solicitudes_TB_SolicitudProcesoDigital
                                  join pv in con.Solicitudes_TB_SolicitudPruebaVida on pd.IdSolicitudProcesoDigital equals pv.IdSolicitudProcesoDigital
                                  where pd.IdProceso == peticion.ProcesoDigital.IdProceso
                                    && pd.IdSolicitud == peticion.Solicitud.IdSolicitud
                                  select pv)
                                  .ToList();

                    if (pruebaVida != null && pruebaVida.Count > 0)
                    {
                        claveEnvio += $"_{pruebaVida.Count + 1}";
                        if (peticion.ProcesoDigital.IdProceso == idProcesoOriginacion)
                        {
                            respuesta.ProcesoIniciado = pruebaVida.Where(pv => pv.Activo).Any();
                        }
                        else if (peticion.ProcesoDigital.IdProceso == idProcesoReestructura)
                        {
                            //Cancelar proceso activos
                            foreach (var pruebaV in pruebaVida)
                            {
                                if (pruebaV.Activo)
                                {
                                    pruebaV.Activo = false;
                                }
                            }
                            con.SaveChanges();
                        }
                    }
                }

                if (!respuesta.ProcesoIniciado)
                {
                    peticion.ClaveEnvio = claveEnvio;
                    GenerarPruebaVidaResponse generaPVResp = GenerarPruebaVida(peticion);
                    respuesta.Error = generaPVResp.Error;
                    if (generaPVResp.Error)
                    {
                        respuesta.ProcesoIniciado = false;
                        respuesta.MensajeOperacion = generaPVResp.MensajeOperacion;
                    }
                    else
                    {
                        respuesta.ProcesoIniciado = true;
                        respuesta.MensajeOperacion = "Se ha iniciado el proceso de prueba de vida con el cliente.";
                    }
                }
                else
                {
                    respuesta.MensajeOperacion = "El cliente cuenta con un proceso de prueba de vida en curso.";
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static GenerarPruebaVidaResponse GenerarPruebaVida(ValidarEnvioProcesoDigitalRequest peticion)
        {
            GenerarPruebaVidaResponse respuesta = new GenerarPruebaVidaResponse();
            bool enviarCorreo = false;
            int idSolicitudPruebaVida = 0;
            try
            {
                if (peticion == null || peticion.Solicitud == null || peticion.ProcesoDigital == null)
                    throw new ValidacionExcepcion("Los datos proporcionados no son válidos.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válida.");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_SolicitudProcesoDigital proceso = con.Solicitudes_TB_SolicitudProcesoDigital
                       .Where(sp => sp.IdSolicitudProcesoDigital == peticion.ProcesoDigital.IdSolicitudProcesoDigital)
                       .FirstOrDefault();
                    if (proceso != null)
                    {
                        TB_SolicitudPruebaVida nuevo = new TB_SolicitudPruebaVida
                        {
                            IdSolicitudProcesoDigital = proceso.IdSolicitudProcesoDigital,
                            IdSolicitud = peticion.Solicitud.IdSolicitud,
                            IdUsuarioRegistro = peticion.IdUsuario,
                            ClaveEnvio = peticion.ClaveEnvio,
                            FechaRegistro = DateTime.Now,
                            Activo = false
                        };

                        con.Solicitudes_TB_SolicitudPruebaVida.Add(nuevo);
                        con.SaveChanges();

                        NuevoVideoResponse videoResp = VideoBLL.Nuevo(new NuevoVideoRequest
                        {
                            ClavePeticion = peticion.ClaveEnvio
                        });

                        nuevo.Mensaje = videoResp.MensajeOperacion;
                        respuesta.Error = videoResp.Error;

                        if (!videoResp.Error && !string.IsNullOrEmpty(videoResp.UrlSesion))
                        {
                            nuevo.Activo = true;
                            nuevo.IdPeticion = videoResp.IdPeticion;
                            nuevo.UrlSesion = videoResp.UrlSesion;
                            proceso.IdEstatus = (int)EstatusProcesoDigital.EnProceso;
                            enviarCorreo = true;
                            idSolicitudPruebaVida = nuevo.IdSolicitudPruebaVida;
                        }
                        else
                        {
                            respuesta.Error = true;
                            respuesta.MensajeOperacion = "No fue posible generar el proceso de vida.";
                        }

                        con.SaveChanges();
                    }

                    if (enviarCorreo)
                    {
                        _ = Task.Factory.StartNew(() =>
                        {
                            if (proceso.IdProceso == idProcesoOriginacion)
                            {
                                GenerarEnvioCorreo(peticion.Solicitud.IdSolicitud, peticion.IdUsuario);
                            }
                            else if (proceso.IdProceso == idProcesoReestructura)
                            {
                                GenerarEnvioCorreoPeticionReest(peticion.Solicitud.IdSolicitud, peticion.IdUsuario, idSolicitudPruebaVida);
                            }
                        });
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
                Utils.EscribirLog($"{vex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                 $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(vex));
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ValidarPruebaVidaResponse ValidarPruebaVida(ValidarPruebaVidaRequest peticion)
        {
            ValidarPruebaVidaResponse respuesta = new ValidarPruebaVidaResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave de usuario no es válida.");
                if (peticion.IdPeticion <= 0) throw new ValidacionExcepcion("La clave de la petición no es correcta.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_SolicitudPruebaVida pruebaVida = con.Solicitudes_TB_SolicitudPruebaVida
                        .Where(
                            s => s.IdSolicitud == peticion.IdSolicitud
                            && s.IdPeticion == peticion.IdPeticion
                            && s.Activo
                            && s.SesionRealizada)
                        .FirstOrDefault();
                    if (pruebaVida == null) throw new ValidacionExcepcion("No se encontró el proceso asociado a la petición.");

                    pruebaVida.Aprobado = peticion.Aprobado;
                    pruebaVida.Mensaje = peticion.Mensaje;
                    pruebaVida.FechaAprobacion = DateTime.Now;
                    con.SaveChanges();

                    con.Solicitudes_TB_SolicitudPruebaVidaEstatus.Add(new TB_SolicitudPruebaVidaEstatus
                    {
                        IdSolicitudPruebaVida = pruebaVida.IdSolicitudPruebaVida,
                        IdUsuario = peticion.IdUsuario,
                        ValidacionCorrecta = peticion.Aprobado,
                        FechaRegistro = DateTime.Now,
                        Mensaje = peticion.Mensaje
                    });
                    con.SaveChanges();

                    //Actualizar el estatus del proceso principal solo cuando el estatus definie una aprobacion o rechazo.
                    ActualizarProcesoDigitalResponse resp = SolicitudProcesoDigitalBLL.Actualizar(new ActualizarProcesoDigitalRequest
                    {
                        IdSolicitudProcesoDigital = pruebaVida.IdSolicitudProcesoDigital,
                        EstatusProcesoDigital = peticion.Aprobado ? EstatusProcesoDigital.Aprobado : EstatusProcesoDigital.Rechazado
                    });
                    respuesta = resp.Convertir<ValidarPruebaVidaResponse>();
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso de actualización.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        [Obsolete]
        //DEV: Revisar si se seguirá usando
        public static ReiniciarPruebaVidaResponse ReiniciarPruebaVida(ReiniciarPruebaVidaRequest peticion)
        {
            ReiniciarPruebaVidaResponse respuesta = new ReiniciarPruebaVidaResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave de usuario no es válida.");
                if (peticion.IdSolicitudPruebaVida <= 0) throw new ValidacionExcepcion("La clave de la petición no es correcta.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_SolicitudPruebaVida pruebaVida = con.Solicitudes_TB_SolicitudPruebaVida.Where(pv => pv.IdSolicitudPruebaVida == peticion.IdSolicitudPruebaVida
                            && pv.Activo)
                        .FirstOrDefault();
                    if (pruebaVida == null) throw new ValidacionExcepcion("No se encontró una prueba de vida activa para reiniciar el proceso.");
                    if (!pruebaVida.SesionRealizada && !string.IsNullOrEmpty(pruebaVida.UrlSesion)) throw new ValidacionExcepcion("No es posible reiniciar el proceso debido a que se encuentra en proceso.");
                    if ((pruebaVida.Aprobado ?? false)) throw new ValidacionExcepcion("La prueba de vida no puede ser reiniciado.");

                    TB_SolicitudProcesoDigital procesoDigital = con.Solicitudes_TB_SolicitudProcesoDigital
                        .Where(pd => pd.IdSolicitudProcesoDigital == pruebaVida.IdSolicitudProcesoDigital).FirstOrDefault();
                    if (procesoDigital == null) throw new ValidacionExcepcion("No se encontró una proceso activo.");
                    if (con.Solicitudes_TB_SolicitudProcesoDigital
                        .Any(sp => sp.IdSolicitud == procesoDigital.IdSolicitud
                            && sp.IdEstatus != (int)EstatusProcesoDigital.Nuevo
                            && sp.Orden > procesoDigital.Orden
                        ))
                    {
                        throw new ValidacionExcepcion("No es posible reinicar ya que existe un proceso activo.");
                    }

                    pruebaVida.Activo = false;
                    pruebaVida.Mensaje = peticion.Comentario;
                    procesoDigital.IdEstatus = (int)EstatusProcesoDigital.Nuevo;
                    con.SaveChanges();

                    con.Solicitudes_TB_SolicitudPruebaVidaEstatus.Add(new TB_SolicitudPruebaVidaEstatus
                    {
                        IdSolicitudPruebaVida = pruebaVida.IdSolicitudPruebaVida,
                        IdUsuario = peticion.IdUsuario,
                        ValidacionCorrecta = pruebaVida.ValidacionCorrecta,
                        FechaRegistro = DateTime.Now,
                        Mensaje = pruebaVida.Mensaje
                    });
                    con.SaveChanges();

                    SolicitudProcesoDigitalBLL.SiguienteProceso(new InicarSolicitudProcesoDigitalRequest
                    {
                        IdSolicitud = pruebaVida.IdSolicitud,
                        IdUsuario =
                        peticion.IdUsuario
                    });
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region PUT
        public static ActualizarEstatusPruebaVidaResponse ActualizarEstatus(ActualizarEstatusPruebaVidaRequest peticion)
        {
            ActualizarEstatusPruebaVidaResponse respuesta = new ActualizarEstatusPruebaVidaResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdPeticion <= 0) throw new ValidacionExcepcion("La clave de la petición no es correcta.");

                int idSolicitudProcesoDigital = 0;
                int idSoliciutPruebaVida = 0;
                int idSolicitud;
                int idProceso = 0;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_SolicitudPruebaVida solicitudPrueba = con.Solicitudes_TB_SolicitudPruebaVida.Where(sp => sp.IdPeticion == peticion.IdPeticion)
                        .FirstOrDefault();
                    if (solicitudPrueba == null) throw new ValidacionExcepcion("No se encontró el proceso asociado a la petición.");
                    TB_SolicitudProcesoDigital proceso = con.Solicitudes_TB_SolicitudProcesoDigital
                        .Where(sp => sp.IdSolicitudProcesoDigital == solicitudPrueba.IdSolicitudProcesoDigital)
                        .FirstOrDefault();
                    if (proceso == null) throw new ValidacionExcepcion("No se encontró el proceso asociado a la petición.");

                    idProceso = proceso.IdProceso ?? 0;
                    idSolicitudProcesoDigital = solicitudPrueba.IdSolicitudProcesoDigital;
                    idSoliciutPruebaVida = solicitudPrueba.IdSolicitudPruebaVida;
                    idSolicitud = solicitudPrueba.IdSolicitud;

                    if (solicitudPrueba.ValidacionCorrecta != peticion.Aprobado)
                    {
                        solicitudPrueba.ValidacionCorrecta = peticion.Aprobado;
                    }
                    if (!string.IsNullOrEmpty(peticion.Mensaje))
                    {
                        solicitudPrueba.Mensaje = peticion.Mensaje;
                    }
                    if (peticion.Aprobado)
                    {
                        solicitudPrueba.Aprobado = true;
                        solicitudPrueba.FechaAprobacion = DateTime.Now;
                    }
                    solicitudPrueba.FechaValidacion = DateTime.Now;
                    solicitudPrueba.SesionRealizada = true;
                    con.SaveChanges();

                    con.Solicitudes_TB_SolicitudPruebaVidaEstatus.Add(new TB_SolicitudPruebaVidaEstatus
                    {
                        IdSolicitudPruebaVida = idSoliciutPruebaVida,
                        IdUsuario = peticion.IdUsuario,
                        ValidacionCorrecta = solicitudPrueba.ValidacionCorrecta,
                        FechaRegistro = DateTime.Now,
                        Mensaje = solicitudPrueba.Mensaje
                    });
                    con.SaveChanges();
                }

                if (peticion.Archivos != null && peticion.Archivos.Count > 0)
                {
                    if (peticion.EsperarResultado)
                    {
                        CargarArchivosPruebaVida(idSolicitud, idSoliciutPruebaVida, peticion.Archivos);
                    }
                    else
                    {
                        _ = Task.Factory.StartNew(() =>
                          {
                              CargarArchivosPruebaVida(idSolicitud, idSoliciutPruebaVida, peticion.Archivos);
                          });
                    }
                }

                if (idProcesoOriginacion == idProceso && peticion.Aprobado)
                {
                    //Actualizar el estatus del proceso principal solo cuando el estatus definie una aprobacion
                    _ = Task.Factory.StartNew(() =>
                    {
                        SolicitudProcesoDigitalBLL.Actualizar(new ActualizarProcesoDigitalRequest
                        {
                            IdSolicitudProcesoDigital = idSolicitudProcesoDigital,
                            EstatusProcesoDigital = EstatusProcesoDigital.Aprobado
                        });
                    });
                }
                else if (idProceso == idProcesoReestructura)
                {
                    //Se aprueban los procesos para avanzar al siguiente.
                    _ = Task.Factory.StartNew(() =>
                    {
                        SolicitudProcesoDigitalBLL.Actualizar(new ActualizarProcesoDigitalRequest
                        {
                            IdSolicitudProcesoDigital = idSolicitudProcesoDigital,
                            EstatusProcesoDigital = EstatusProcesoDigital.Aprobado
                        });
                    });
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ActualizarEstatusPruebaVidaResponse ActualizarPeticionEstatus(ActualizarPeticionEstatusRequest peticion)
        {
            ActualizarEstatusPruebaVidaResponse respuesta = new ActualizarEstatusPruebaVidaResponse();
            try
            {
                if (peticion == null || peticion.IdPeticion <= 0) throw new ValidacionExcepcion("La petición no es correcta.");
                Model.Response.WsPfLleida.EstatusVideoResponse estatus = VideoBLL.Estatus(peticion.Convertir<EstatusVideoRequest>());
                if (estatus == null || estatus.Error || estatus.Peticion == null || estatus.Peticion.IdPeticion != peticion.IdPeticion)
                    throw new ValidacionExcepcion("No fue posible obtener los datos de la petición.");
                respuesta = ActualizarEstatus(new ActualizarEstatusPruebaVidaRequest
                {
                    EsperarResultado = true,
                    IdPeticion = estatus.Peticion.IdPeticion,
                    Aprobado = estatus.Peticion.Correcto ?? false,
                    Archivos = estatus.Archivos,
                    Mensaje = estatus.Mensaje,
                    IdUsuario = peticion.IdUsuario
                });
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region Metodos Privados
        private static void GenerarEnvioCorreo(int idSolicitud, int idUsuario)
        {
            try
            {
                int idTipoNotificacion = 0;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    idTipoNotificacion = con.Sistema_TB_TipoNotificacion.Where(tn => tn.Clave == AppSettings.ClaveNotificacionPruebaVida)
                        .Select(tn => tn.IdTipoNotificacion)
                        .DefaultIfEmpty(0)
                        .FirstOrDefault();
                }
                if (idTipoNotificacion == 0) throw new ValidacionExcepcion("No existe el tipo de notificación para envios de correos de pruebas de vida.");

                AltaNotificacionResponse notificacion = Sistema.NotificacionBLL.Alta(new AltaNotificacionRequest
                {
                    IdUsuario = idUsuario,
                    IdSolicitud = idSolicitud,
                    IdTipoNotificacion = idTipoNotificacion
                });
            }
            catch (ValidacionExcepcion vex)
            {
                Utils.EscribirLog($"{vex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                 $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { idSolicitud, idUsuario })}", JsonConvert.SerializeObject(vex));
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { idSolicitud, idUsuario })}", JsonConvert.SerializeObject(ex));
            }
        }

        public static void GenerarEnvioCorreoPeticionReest(int idSolicitud, int idUsuario, int idSolicitudPruebaVida)
        {
            try
            {
                Poco.Sistema.TB_TipoNotificacion tipoNotificacion = Sistema.TipoNotificacionBLL.TipoNotificacion(TipoNotificacion.PruebaVidaPetRes.GetDescription());
                if (tipoNotificacion == null || tipoNotificacion.IdTipoNotificacion <= 0) throw new ValidacionExcepcion("No fue posible realizar el envio. Consulte a soporte técnico.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var datoCte = (from s in con.dbo_TB_Solicitudes
                                   join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                   where s.IdSolicitud == idSolicitud
                                   select new { c.Cliente, c.Nombres, c.apellidop, c.apellidom, c.Correo }
                                            ).FirstOrDefault();

                    if (datoCte == null || string.IsNullOrEmpty(datoCte.Correo)) throw new ValidacionExcepcion("No se pudo obtener la cuenta de coreo del cliente.");

                    TB_SolicitudPruebaVida pruebaVida = con.Solicitudes_TB_SolicitudPruebaVida
                        .Where(spv => spv.IdSolicitudPruebaVida == idSolicitudPruebaVida).FirstOrDefault();
                    if (pruebaVida == null || string.IsNullOrEmpty(pruebaVida.UrlSesion)) throw new ValidacionExcepcion("No se pudo obtener los datos para el envio del correo al cliente.");

                    NuevoRequest nuevaNotif = new NuevoRequest
                    {
                        IdUsuarioRegistro = idUsuario,
                        IdTipoNotificacion = tipoNotificacion.IdTipoNotificacion,
                        IdSolicitud = idSolicitud,
                        Titulo = string.IsNullOrEmpty(tipoNotificacion.TituloCorreo) ? "Prueba de Vida" : tipoNotificacion.TituloCorreo,
                        NombreCliente = $"{datoCte.Nombres} {datoCte.apellidop} {datoCte.apellidom}",
                        CorreoElectronico = datoCte.Correo,
                        Url = pruebaVida.UrlSesion
                    };

                    NuevoResponse respNoti = Sistema.NotificacionBLL.Nuevo(nuevaNotif);
                }
            }
            catch (ValidacionExcepcion vex)
            {
                Utils.EscribirLog($"{vex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                 $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { idSolicitud, idUsuario })}", JsonConvert.SerializeObject(vex));
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { idSolicitud, idUsuario })}", JsonConvert.SerializeObject(ex));
            }
        }

        private static void CargarArchivosPruebaVida(int idSolicitud, int idSoliciutdPruebaVida, List<ArchivoDigitalVM> archivos)
        {
            try
            {
                if (idSolicitud > 0 && idSoliciutdPruebaVida > 0 && archivos != null)
                {
                    string expedientes = System.Configuration.ConfigurationManager.AppSettings["GDExpedientes"];
                    if (string.IsNullOrEmpty(expedientes)) throw new Exception("No fue posible asignar una carpeta para el archivo, favor de revisar la configuracion");
                    string idFolderSolicitud = string.Empty;
                    string urlArchivo = string.Empty;
                    using (ServidorGoogleDrive servGoogle = new ServidorGoogleDrive())
                    {
                        Respuesta crearCarpeta = servGoogle.CrearCarpeta(new CargaArchivoGoogleDrive
                        {
                            IdCarpeta = expedientes,
                            Comentario = "Archivos prueba de vida solicitud " + idSolicitud,
                            NombreArchivo = idSolicitud.ToString()
                        }, ref idFolderSolicitud, ref urlArchivo);
                        if (crearCarpeta.Error || string.IsNullOrEmpty(idFolderSolicitud)) throw new Exception("No fue posible crear el directorio");
                    }

                    List<TB_SolicitudPruebaVidaArchivo> archivosCargar = new List<TB_SolicitudPruebaVidaArchivo>();
                    #region Cargar de archivos
                    foreach (ArchivoDigitalVM archivo in archivos)
                    {
                        string idArchivo = string.Empty;
                        string nombreArchivo = $"{archivo.MediaType}{ObtenerExtensionArchivo(archivo.MediaType)}";
                        string tipoArchivo = ObtenerTipoMimeType(archivo.MediaType);
                        urlArchivo = string.Empty;
                        try
                        {
                            using (ServidorGoogleDrive servGoogle = new ServidorGoogleDrive())
                            {
                                byte[] contenido = Convert.FromBase64String(archivo.Data);
                                if (contenido != null && contenido.Length > 0)
                                {
                                    Entities.Resultado<bool> sbuirArchivo = servGoogle.SubirArchivo(new CargaArchivoGoogleDrive
                                    {
                                        IdCarpeta = idFolderSolicitud,
                                        NombreArchivo = archivo.MediaType.ToString(),
                                        Contenido = contenido,
                                        TipoArchivo = tipoArchivo
                                    }, ref idArchivo, ref urlArchivo);

                                    if (sbuirArchivo == null || !sbuirArchivo.ResultObject || string.IsNullOrEmpty(idArchivo))
                                    {
                                        throw new Exception($"No fue posible realizar la carga del archivo: {nombreArchivo} para la solicitud: {idSolicitud}");
                                    }
                                    archivosCargar.Add(new TB_SolicitudPruebaVidaArchivo
                                    {
                                        IdSolicitudPruebaVida = idSoliciutdPruebaVida,
                                        NombreArchivo = nombreArchivo,
                                        IdGDrive = idArchivo,
                                        FechaRegistro = DateTime.Now,
                                        Actual = true
                                    });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                                $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { idSoliciutdPruebaVida, archivos })}", JsonConvert.SerializeObject(ex));
                        }
                    }
                    #endregion

                    #region Registro de archivos
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        List<TB_SolicitudPruebaVidaArchivo> actuales = con.Solicitudes_TB_SolicitudPruebaVidaArchivo
                            .Where(sa => sa.IdSolicitudPruebaVida == idSoliciutdPruebaVida
                                && sa.Actual)
                            .ToList();

                        List<TB_SolicitudPruebaVidaArchivo> existentes = actuales
                            .Where(a => archivosCargar
                                .Select(ac => ac.NombreArchivo)
                                .ToList().Contains(a.NombreArchivo))
                            .ToList();

                        //Desactivar los actuales
                        if (existentes != null && existentes.Count > 0)
                        {
                            foreach (TB_SolicitudPruebaVidaArchivo existente in existentes)
                            {
                                existente.Actual = false;
                            }
                        }

                        //Registro de los nuevos
                        con.Solicitudes_TB_SolicitudPruebaVidaArchivo.AddRange(archivosCargar);
                        con.SaveChanges();
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { idSoliciutdPruebaVida, archivos })}", JsonConvert.SerializeObject(ex));
            }
        }

        private static string ObtenerExtensionArchivo(EkycMediaType tipoArchivo)
        {
            string extension;
            switch (tipoArchivo)
            {
                case EkycMediaType.Face:
                case EkycMediaType.IdFront:
                case EkycMediaType.IdBack:
                    extension = ".jpg";
                    break;
                case EkycMediaType.Video:
                    extension = ".mp4";
                    break;
                case EkycMediaType.Cetificate:
                case EkycMediaType.ValidationInfo:
                case EkycMediaType.ValidatonCheck:
                    extension = ".pdf";
                    break;
                default:
                    extension = ".txt";
                    break;
            }
            return extension;
        }

        private static string ObtenerTipoMimeType(EkycMediaType tipoArchivo)
        {
            string tipoMime;
            switch (tipoArchivo)
            {
                case EkycMediaType.Face:
                case EkycMediaType.IdFront:
                case EkycMediaType.IdBack:
                    tipoMime = "image/jpeg";
                    break;
                case EkycMediaType.Video:
                    tipoMime = "video/mp4";
                    break;
                case EkycMediaType.Cetificate:
                case EkycMediaType.ValidationInfo:
                case EkycMediaType.ValidatonCheck:
                    tipoMime = "application/pdf";
                    break;
                default:
                    tipoMime = "text/plain";
                    break;
            }
            return tipoMime;
        }
        #endregion
    }
}
