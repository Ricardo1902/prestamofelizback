﻿using Newtonsoft.Json;
using SOPF.Core.BusinessLogic.Catalogo;
using SOPF.Core.BusinessLogic.Clientes;
using SOPF.Core.BusinessLogic.Lleida;
using SOPF.Core.BusinessLogic.Solcitudes;
using SOPF.Core.DataAccess;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Lleida;
using SOPF.Core.Model.Clientes;
using SOPF.Core.Model.Dbo;
using SOPF.Core.Model.Reportes;
using SOPF.Core.Model.Request.Lleida;
using SOPF.Core.Model.Request.Reportes;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Response;
using SOPF.Core.Model.Response.Cliente;
using SOPF.Core.Model.Response.Lleida;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Model.Solicitudes;
using SOPF.Core.Model.WsPfLleida;
using SOPF.Core.Poco.Solicitudes;
using SOPF.Core.Solicitudes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SOPF.Core.BusinessLogic.Solicitudes
{
    public static class SolicitudFirmaDigitalBLL
    {
        static readonly int[] idEstusActivos = new int[] { (int)EstatusFirma.New, (int)EstatusFirma.Ready };
        static int idProcesoOriginacion = 0;
        static int idProcesoPetRes = 0;

        static SolicitudFirmaDigitalBLL()
        {
            TB_Proceso proceso = ProcesoBLL.Originacion();
            if (proceso != null) idProcesoOriginacion = proceso.IdProceso;
            proceso = ProcesoBLL.PetionReestructura();
            if (proceso != null) idProcesoPetRes = proceso.IdProceso;
        }

        #region GET
        public static bool ProcesoActivo(int idSolicitud)
        {
            bool procesoActivo = false;
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<TB_SolicitudFirmaDigital> peticionfirmas = con.Solicitudes_TB_SolicitudFirmaDigital
                        .Where(s => s.IdSolicitud == idSolicitud)
                        .ToList();

                    if (peticionfirmas != null)
                    {
                        procesoActivo = peticionfirmas.Exists(s => idEstusActivos.Contains(s.IdEstatus));
                    }
                }
            }
            catch (Exception)
            {
                procesoActivo = false;
            }
            return procesoActivo;
        }

        public static List<SolicitudFirmaDigitalArchivoVM> SolicitudFirmaDigitalArchivos(int idSolicitudFirmaDigital)
        {
            List<SolicitudFirmaDigitalArchivoVM> archivos = new List<SolicitudFirmaDigitalArchivoVM>();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<TB_SolicitudFirmaDigitalArchivo> archivosDb = con.Solicitudes_TB_SolicitudFirmaDigitalArchivo
                        .Where(sa => sa.IdSolicitudFirmaDigital == idSolicitudFirmaDigital)
                        .ToList();
                    if (archivosDb != null && archivosDb.Count > 0)
                    {
                        archivos = archivosDb.Select(da => new SolicitudFirmaDigitalArchivoVM
                        {
                            IdSolicitudFirmaDigital = da.IdSolicitudFirmaDigital,
                            IdDocumento = da.IdDocumento,
                            Orden = da.Orden,
                            NombreDocumentoDigital = da.NombreDocumentoDigital
                        }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(idSolicitudFirmaDigital)}", JsonConvert.SerializeObject(ex));
            }
            return archivos;
        }

        public static SolicitudFirmaDigitalResponse SolicitudFirmaDigital(SolicitudFirmaDigitalRequest peticion)
        {
            SolicitudFirmaDigitalResponse respuesta = new SolicitudFirmaDigitalResponse();
            try
            {
                #region validaciones
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                #endregion

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    SolicitudFirmaDigitalResponse resp = con.Solicitudes_SP_SolicitudFirmaDigitalCON(peticion);
                    if (resp != null)
                    {
                        respuesta.Firmas = resp.Firmas;
                        respuesta.TotalRegistros = resp.TotalRegistros;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static SolicitudProcesoDigitalVM FirmaDigitalProceso(int idSolicitudFirmaDigital)
        {
            SolicitudProcesoDigitalVM respuesta = null;
            try
            {
                if (idSolicitudFirmaDigital <= 0) return respuesta;
                int idTipoEstatus = 0;
                TipoEstatusVM tipoEsatus = TipoEstatusBLL.TipoEstatus(AppSettings.TipoEstatusProcesoDigital);
                if (tipoEsatus != null) idTipoEstatus = tipoEsatus.IdTipoEstatus;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = (from f in con.Solicitudes_TB_SolicitudFirmaDigital
                                 join sp in con.Solicitudes_TB_SolicitudProcesoDigital on f.IdSolicitudProcesoDigital equals sp.IdSolicitudProcesoDigital
                                 join p in con.Solicitudes_TB_ProcesoDigital on sp.IdProcesoDigital equals p.IdProcesoDigital
                                 join es in con.dbo_TB_CATEstatus on new { sp.IdEstatus, IdTipoEstatus = idTipoEstatus } equals new { es.IdEstatus, es.IdTipoEstatus }
                                 where f.IdSolicitudFirmaDigital == idSolicitudFirmaDigital
                                 select new SolicitudProcesoDigitalVM
                                 {
                                     IdSolicitudProcesoDigital = sp.IdSolicitudProcesoDigital,
                                     IdSolicitud = sp.IdSolicitud,
                                     IdEstatus = sp.IdEstatus,
                                     IdProcesoDigital = p.IdProcesoDigital,
                                     ProcesoDigital = p.Proceso,
                                     Orden = sp.Orden,
                                     Estatus = es.Estatus,
                                     ProcesoActual = sp.ProcesoActual,
                                     IdProceso = sp.IdProceso
                                 }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(idSolicitudFirmaDigital)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST
        public static ValidarEnvioProcesoDigitalResponse VerificaEnvioFirmaDigital(ValidarEnvioProcesoDigitalRequest peticion)
        {
            ValidarEnvioProcesoDigitalResponse respuesta = new ValidarEnvioProcesoDigitalResponse();
            try
            {
                if (peticion == null || peticion.Solicitud == null || peticion.ProcesoDigital == null)
                    throw new ValidacionExcepcion("Los datos proporcionados no son válidos.");

                bool iniciarProceso = false;
                bool existenCambiosEnDocs = false;
                string claveEnvio = peticion.Solicitud.IdSolicitud.ToString();
                List<TB_SolicitudFirmaDigital> peticionfirmas = null;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    //Todas las peticiones de este proceso
                    peticionfirmas = (from pd in con.Solicitudes_TB_SolicitudProcesoDigital
                                      join pv in con.Solicitudes_TB_SolicitudFirmaDigital on pd.IdSolicitudProcesoDigital equals pv.IdSolicitudProcesoDigital
                                      where pd.IdProceso == peticion.ProcesoDigital.IdProceso
                                        && pd.IdSolicitud == peticion.Solicitud.IdSolicitud
                                      select pv)
                                      .ToList();

                    //Revisamos si ya existe un proceso en curso para no volver a enviar a firmar
                    TB_SolicitudFirmaDigital procesoSolFirma = null;

                    if (peticionfirmas != null)
                    {
                        procesoSolFirma = peticionfirmas
                            .Where(fd => fd.Activo
                                && idEstusActivos.Contains(fd.IdEstatus))
                            .OrderByDescending(o => o.IdSolicitudFirmaDigital)
                            .FirstOrDefault();
                    }

                    if (idProcesoOriginacion == peticion.ProcesoDigital.IdProceso)
                    {
                        if (peticion.Solicitud.IdEstatus == (int)EstatusSolicitud.Activa
                        && peticion.Solicitud.EstProceso == 2
                        )
                        {
                            if (procesoSolFirma != null)
                            {
                                respuesta.MensajeOperacion = "La solicitud cuenta con un proceso de firma de documentos vigente.";
                            }
                            else
                            {
                                iniciarProceso = true;
                            }
                        }
                        else if (peticion.Solicitud.IdEstatus == (int)EstatusSolicitud.Condicionada)
                        {
                            if (procesoSolFirma != null)
                            {
                                respuesta.MensajeOperacion = "La solicitud cuenta con un proceso de firma de documentos vigente.";
                            }
                            else
                            {
                                //Validar si existen cambios, activar proceso para envio de firmas
                                existenCambiosEnDocs = ExisteCambiosDatosDocumentos(new ExisteCambiosDatosDocumentosRequest
                                {
                                    IdSolicitud = peticion.Solicitud.IdSolicitud,
                                    IdUsuario = peticion.IdUsuario,
                                    IdProceso = peticion.ProcesoDigital.IdProceso ?? 0
                                });
                                iniciarProceso = existenCambiosEnDocs;
                            }
                        }
                    }
                    else if (idProcesoPetRes == peticion.ProcesoDigital.IdProceso)
                    {
                        if (procesoSolFirma != null)
                        {
                            respuesta.MensajeOperacion = "La solicitud cuenta con un proceso de firma de documentos vigente.";
                        }
                        iniciarProceso = procesoSolFirma == null;
                    }

                    if (iniciarProceso && peticionfirmas != null && peticionfirmas.Count > 0)
                    {
                        claveEnvio += $"_{peticionfirmas.Count + 1}";
                    }
                }

                if (iniciarProceso)
                {
                    _ = Task.Factory.StartNew(() =>
                    {
                        GenerarSolicitudFirmaDigital(new GenerarSolicitudFirmaDigitalRequest
                        {
                            IdUsuario = peticion.IdUsuario,
                            IdSolicitudProcesoDigital = peticion.ProcesoDigital.IdSolicitudProcesoDigital,
                            IdSolicitud = peticion.Solicitud.IdSolicitud,
                            ClaveEnvio = claveEnvio,
                            ValidarCambios = existenCambiosEnDocs
                        });
                    });
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        public static AltaSolicitudFirmaDigitalResponse AltaSolicitudFirmaDigital(AltaSolicitudFirmaDigitalRequest peticion)
        {
            AltaSolicitudFirmaDigitalResponse respuesta = new AltaSolicitudFirmaDigitalResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válida.");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_SolicitudFirmaDigital nuevoProcesoFirma = new TB_SolicitudFirmaDigital
                    {
                        IdUsuarioRegistro = peticion.IdUsuario,
                        IdSolicitudProcesoDigital = peticion.IdSolicitudProcesoDigital,
                        IdSolicitud = peticion.IdSolicitud,
                        IdEstatus = (int)EstatusFirma.New,
                        FechaRegistro = DateTime.Now,
                        ClaveEnvio = peticion.ClaveEnvio,
                        Activo = true
                    };
                    con.Solicitudes_TB_SolicitudFirmaDigital.Add(nuevoProcesoFirma);
                    con.SaveChanges();

                    respuesta.IdSolicitudFirmaDigital = nuevoProcesoFirma.IdSolicitudFirmaDigital;

                    TB_SolicitudProcesoDigital proceso = con.Solicitudes_TB_SolicitudProcesoDigital
                        .Where(sp => sp.IdSolicitudProcesoDigital == peticion.IdSolicitudProcesoDigital)
                        .FirstOrDefault();

                    if (proceso != null)
                    {
                        respuesta.IdProceso = proceso.IdProceso ?? 0;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ActualizarSolicitudFirmaDigitalResponse ActualizarSolicitudFirmaDigital(ActualizarSolicitudFirmaDigitalRequest peticion)
        {
            ActualizarSolicitudFirmaDigitalResponse respuesta = new ActualizarSolicitudFirmaDigitalResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdSolicitudFirmaDigital <= 0) throw new ValidacionExcepcion("La clave de petición es incorrecta.");
                int idSolicitudProcesoDigital = 0;
                int idEstatusActual = 0;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_SolicitudFirmaDigital solicitudDigital = con.Solicitudes_TB_SolicitudFirmaDigital
                        .Where(s => s.IdSolicitudFirmaDigital == peticion.IdSolicitudFirmaDigital)
                        .FirstOrDefault();
                    if (solicitudDigital != null)
                    {
                        idEstatusActual = solicitudDigital.IdEstatus;
                        if (peticion.IdEstatus >= 0 && peticion.IdEstatus != solicitudDigital.IdEstatus) solicitudDigital.IdEstatus = peticion.IdEstatus;
                        if (peticion.CodigoEstado > 0) solicitudDigital.CodigoEstado = peticion.CodigoEstado;
                        if (!string.IsNullOrEmpty(peticion.Mensaje)) solicitudDigital.Mensaje = peticion.Mensaje;
                        if (!string.IsNullOrEmpty(peticion.SignatureId)) solicitudDigital.SignatureId = peticion.SignatureId;
                        if (peticion.IdPeticion > 0) solicitudDigital.IdPeticion = peticion.IdPeticion;
                        if (peticion.FechaFirmado != null) solicitudDigital.FechaFirmado = peticion.FechaFirmado;
                        solicitudDigital.FechaActualizacion = DateTime.Now;
                        con.SaveChanges();

                        idSolicitudProcesoDigital = solicitudDigital.IdSolicitudProcesoDigital;

                        TB_SolicitudProcesoDigital proceso = con.Solicitudes_TB_SolicitudProcesoDigital
                                .Where(sp => sp.IdSolicitudProcesoDigital == idSolicitudProcesoDigital)
                                .FirstOrDefault();

                        if (proceso != null)
                        {
                            if (
                                (idEstatusActual == (int)EstatusFirma.New
                                && peticion.IdEstatus == (int)EstatusFirma.Ready
                                && peticion.IdPeticion > 0
                                && !string.IsNullOrEmpty(peticion.SignatureId))
                                ||
                                (idEstatusActual == (int)EstatusFirma.New
                                && peticion.IdEstatus == (int)EstatusFirma.Signed
                                && solicitudDigital.IdPeticion == null
                                && !string.IsNullOrEmpty(peticion.Mensaje)))
                            {
                                proceso.IdEstatus = (int)EstatusProcesoDigital.EnProceso;
                                con.SaveChanges();
                            }
                        }
                    }
                }

                EstatusFirma[] rechazado = new EstatusFirma[] {
                EstatusFirma.Expired,
                EstatusFirma.Failed,
                EstatusFirma.Cancelled,
                EstatusFirma.Otp_max_retries };

                if (idSolicitudProcesoDigital > 0)
                {
                    if (peticion.IdEstatus == (int)EstatusFirma.Signed)
                    {
                        _ = Task.Factory.StartNew(() =>
                        {
                            SolicitudProcesoDigitalBLL.Actualizar(new ActualizarProcesoDigitalRequest
                            {
                                IdSolicitudProcesoDigital = idSolicitudProcesoDigital,
                                EstatusProcesoDigital = EstatusProcesoDigital.Aprobado
                            });
                        });
                    }
                    if (rechazado.Contains((EstatusFirma)peticion.IdEstatus))
                    {
                        _ = Task.Factory.StartNew(() =>
                        {
                            SolicitudProcesoDigitalBLL.Actualizar(new ActualizarProcesoDigitalRequest
                            {
                                IdSolicitudProcesoDigital = idSolicitudProcesoDigital,
                                EstatusProcesoDigital = EstatusProcesoDigital.Rechazado
                            });
                        });
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static GenerarSolicitudFirmaDigitalResponse GenerarSolicitudFirmaDigital(GenerarSolicitudFirmaDigitalRequest peticion)
        {
            GenerarSolicitudFirmaDigitalResponse respuesta = new GenerarSolicitudFirmaDigitalResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("Peticion incorrecta.");

                AltaSolicitudFirmaDigitalResponse altaSolicitudF = AltaSolicitudFirmaDigital(new AltaSolicitudFirmaDigitalRequest
                {
                    IdSolicitud = peticion.IdSolicitud,
                    IdSolicitudProcesoDigital = peticion.IdSolicitudProcesoDigital,
                    IdUsuario = peticion.IdUsuario,
                    ClaveEnvio = peticion.ClaveEnvio
                });
                if (altaSolicitudF.Error) throw new ValidacionExcepcion(altaSolicitudF.MensajeOperacion);
                peticion.IdSolicitudFirmaDigital = altaSolicitudF.IdSolicitudFirmaDigital;

                SolicitudDocDigitalesResponse resp = SolicitudDocDigitalBLL.SolicitudDocDigitales(new SolicitudDocDigitalesRequest
                {
                    IdSolicitud = peticion.IdSolicitud,
                    Activo = true,
                    IdProceso = altaSolicitudF.IdProceso
                });
                Dictionary<DocumentoDigital, List<PosicionFirmaVM>> documentoFirma = new Dictionary<DocumentoDigital, List<PosicionFirmaVM>>();

                if (resp.Error) throw new ValidacionExcepcion(resp.MensajeOperacion);

                if (resp.Documentos != null && resp.Documentos.Count > 0)
                {
                    #region Datos Cliente
                    DatosClienteVM cliente = new DatosClienteVM();
                    SolicitudVM solicitud = new SolicitudVM();

                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        var datos = (from s in con.dbo_TB_Solicitudes
                                     join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         c.Nombres,
                                         c.apellidop,
                                         c.apellidom,
                                         c.lada,
                                         c.telefono,
                                         c.Celular,
                                         c.Correo,
                                         s.IdSolicitud,
                                         s.idTipoCredito,
                                         s.Banco,
                                         IdEstatus = s.IdEstatus ?? 2
                                     })
                                     .FirstOrDefault();
                        if (datos != null)
                        {
                            cliente.Nombres = datos.Nombres;
                            cliente.ApellidoPaterno = datos.apellidop;
                            cliente.ApellidoMaterno = datos.apellidom;
                            cliente.Email = datos.Correo;
                            cliente.Telefono = $"+51{datos.lada}{datos.telefono}";
                            cliente.Celular = $"+51{datos.Celular}";
                            solicitud.IdSolicitud = (int)datos.IdSolicitud;
                            solicitud.IdTipoCredito = datos.idTipoCredito ?? 0;
                            solicitud.Banco = datos.Banco;
                            solicitud.IdEstatus = datos.IdEstatus;
                        }
                    }

                    if (!AppSettings.EsProductivo || Debugger.IsAttached)
                    {
                        cliente.Email = AppSettings.CorreoEnvioPruebas;
                        cliente.Celular = AppSettings.TelefonoCtePruebas;
                    }
                    #endregion

                    #region Generar Documentos
                    resp.Documentos = resp.Documentos.OrderBy(o => o.Orden).ToList();
                    foreach (SolicitudDocDigitalVM doc in resp.Documentos)
                    {
                        ImpresionDocumentoResponse impresionDoc = null;
                        doc.ContenidoB64 = string.Empty;
                        ImpresionDocumentoRequest impresionDocReq = new ImpresionDocumentoRequest
                        {
                            IdUsuario = 21,
                            IdSolicitud = peticion.IdSolicitud,
                            Version = doc.Version
                        };
                        switch ((DocumentoDigital)doc.IdDocumento)
                        {
                            case DocumentoDigital.Contrato:
                                impresionDoc = TBSolicitudesBll.ImpresionContrato(impresionDocReq);
                                IncluirDocumento(DocumentoDigital.Contrato, doc, impresionDoc, peticion.ValidarCambios, documentoFirma);
                                break;
                            case DocumentoDigital.Pagare:
                                impresionDoc = TBSolicitudesBll.ImpresionPagare(impresionDocReq);
                                IncluirDocumento(DocumentoDigital.Pagare, doc, impresionDoc, peticion.ValidarCambios, documentoFirma);
                                break;
                            case DocumentoDigital.AcuerdoLlenadoPagare:
                                impresionDoc = TBSolicitudesBll.ImpresionAcuerdoPagare(impresionDocReq);
                                IncluirDocumento(DocumentoDigital.AcuerdoLlenadoPagare, doc, impresionDoc, peticion.ValidarCambios, documentoFirma);
                                break;
                            case DocumentoDigital.HojaResumen:
                                impresionDoc = TBSolicitudesBll.ImpresionResumenCredito(impresionDocReq);
                                IncluirDocumento(DocumentoDigital.HojaResumen, doc, impresionDoc, peticion.ValidarCambios, documentoFirma);
                                break;
                            case DocumentoDigital.Cronograma:
                                impresionDoc = TBSolicitudesBll.ImpresionTablaAmortizacion(new ImpresionTablaAmortizacionRequest
                                {
                                    IdUsuario = 21,
                                    IdSolicitud = peticion.IdSolicitud,
                                    Version = doc.Version
                                });
                                IncluirDocumento(DocumentoDigital.Cronograma, doc, impresionDoc, peticion.ValidarCambios, documentoFirma);
                                break;
                            case DocumentoDigital.AcuerdoDomiciliacion:
                                if (solicitud.BancoDomiciliacion == BancoDomicilacion.Continental)
                                {
                                    impresionDoc = TBSolicitudesBll.ImpresionAcuerdoDomiciliacionContinental(impresionDocReq);
                                }
                                else if (solicitud.BancoDomiciliacion == BancoDomicilacion.Interbank)
                                {
                                    impresionDoc = TBSolicitudesBll.ImpresionAcuerdoDomiciliacionInterbank(impresionDocReq);
                                }
                                IncluirDocumento(DocumentoDigital.AcuerdoDomiciliacion, doc, impresionDoc, peticion.ValidarCambios, documentoFirma);
                                break;
                            case DocumentoDigital.FormatoVisa:
                                impresionDoc = TBSolicitudesBll.ImpresionAcuerdoDomiciliacionVISA(impresionDocReq);
                                IncluirDocumento(DocumentoDigital.FormatoVisa, doc, impresionDoc, peticion.ValidarCambios, documentoFirma);
                                break;
                            case DocumentoDigital.Reestructura:
                                impresionDoc = TBSolicitudesBll.ImpresionReestructura(impresionDocReq);
                                IncluirDocumento(DocumentoDigital.Reestructura, doc, impresionDoc, peticion.ValidarCambios, documentoFirma);
                                break;
                            case DocumentoDigital.ReestructuraExtrajudicial:
                                impresionDoc = TBSolicitudesBll.ImpresionTransaccionExtrajudicial(impresionDocReq);
                                IncluirDocumento(DocumentoDigital.ReestructuraExtrajudicial, doc, impresionDoc, peticion.ValidarCambios, documentoFirma);
                                break;
                            case DocumentoDigital.FormularioAmpliacion:
                                impresionDoc = TBSolicitudesBll.ImpresionAmpliacionCredito(impresionDocReq);
                                IncluirDocumento(DocumentoDigital.FormularioAmpliacion, doc, impresionDoc, peticion.ValidarCambios, documentoFirma);
                                break;
                            case DocumentoDigital.CartaPoder:
                                impresionDoc = TBSolicitudesBll.ImpresionCartaPoder(impresionDocReq);
                                IncluirDocumento(DocumentoDigital.CartaPoder, doc, impresionDoc, peticion.ValidarCambios, documentoFirma);
                                break;
                            case DocumentoDigital.DeclaracionJurada:
                                impresionDoc = TBSolicitudesBll.ImpresionDeclaracionJurada(impresionDocReq);
                                IncluirDocumento(DocumentoDigital.DeclaracionJurada, doc, impresionDoc, peticion.ValidarCambios, documentoFirma);
                                break;
                        }
                    }
                    #endregion

                    #region Seleccion de plantilla
                    EnvioProcesoFirmasResponse ConfigResp = ConfiguracionBLL.EnvioProcesoFirmas();
                    if (ConfigResp == null || ConfigResp.Error || ConfigResp.IdConfig <= 0)
                    {
                        throw new Exception("No existe una plantilla o configuración seleccionada para " +
                        "realizar el proceso de envio de firmas con el cliente.");
                    }

                    AltaFirmaRequest procesoFirma = new AltaFirmaRequest()
                    {
                        IdPlantilla = ConfigResp.IdConfig,
                        ClaveDocumento = peticion.ClaveEnvio,
                        OrdenFirma = new List<OrdenFirmaVM>(),
                        Archivos = new List<ArchivoVM>()
                    };

                    List<SolicitudFirmaDigitalArchivoVM> documentoArchivo = new List<SolicitudFirmaDigitalArchivoVM>();
                    List<Firmante> firmantes = new List<Firmante>();
                    List<DatosFirmanteVM> datosFirmante = new List<DatosFirmanteVM>();
                    #endregion

                    #region Datos de los firmantes
                    foreach (var d in documentoFirma.Values)
                    {
                        var firm = d.Select(f => f.Firmante).ToList();
                        firmantes.AddRange(firm);
                    }
                    firmantes = firmantes.Distinct().ToList();

                    foreach (var f in firmantes)
                    {
                        switch (f)
                        {
                            case Firmante.Cliente:
                                datosFirmante.Add(new DatosFirmanteVM
                                {
                                    Nombre = cliente.Nombres,
                                    Apellidos = cliente.Apellidos,
                                    Telefono = cliente.Celular,
                                    Email = cliente.Email,
                                    Firmante = f
                                });
                                break;
                        }
                    }
                    #endregion

                    #region Documento-firma
                    int orden = 0;
                    OrdenFirmaVM ordenFirma = new OrdenFirmaVM
                    {
                        Orden = orden,
                        Firmantes = new List<FirmanteVM>()
                    };
                    orden = 1;
                    foreach (SolicitudDocDigitalVM d in resp.Documentos.Where(d => !string.IsNullOrEmpty(d.ContenidoB64)))
                    {
                        List<PosicionFirmaVM> firmasPos = new List<PosicionFirmaVM>();
                        if (documentoFirma.TryGetValue((DocumentoDigital)d.IdDocumento, out firmasPos))
                        {
                            #region Buscar Datos del firmante por documento                            
                            foreach (var firmante in firmantes)
                            {
                                if (firmasPos.Exists(f => f.Firmante == firmante))
                                {
                                    DatosFirmanteVM datFirmante = datosFirmante.Where(df => df.Firmante == firmante).FirstOrDefault();
                                    var existe = ordenFirma.Firmantes.Exists(f => f.NumeroFirmante == ((int)datFirmante.Firmante).ToString());
                                    if (datosFirmante != null && !existe)
                                    {
                                        ordenFirma.Firmantes.Add(new FirmanteVM
                                        {
                                            Nombres = datFirmante.Nombre,
                                            Apellidos = datFirmante.Apellidos,
                                            NumeroFirmante = ((int)datFirmante.Firmante).ToString(),
                                            Email = datFirmante.Email,
                                            Telefono = datFirmante.Telefono
                                        });
                                    }
                                }
                            }
                            #endregion

                            #region Incluir archivo con posiciones de firmas                            
                            procesoFirma.Archivos.Add(new ArchivoVM()
                            {
                                Contenido = d.ContenidoB64,
                                NombreArchivo = d.Documento,
                                Firmas = firmasPos.Select(f => new FirmaArchivoVM
                                {
                                    FirmaNumeroFirmante = ((int)f.Firmante).ToString(),
                                    Pagina = f.Pagina.ToString(),
                                    X = f.PosX,
                                    Y = f.PosY,
                                    Alto = f.Alto,
                                    Ancho = f.Ancho
                                }).ToList()
                            });

                            documentoArchivo.Add(new SolicitudFirmaDigitalArchivoVM { IdDocumento = d.IdDocumento, Orden = orden, NombreDocumentoDigital = d.Documento });
                            orden++;
                            #endregion
                        }
                    }

                    procesoFirma.OrdenFirma.Add(ordenFirma);
                    #endregion

                    #region Envio a firmar
                    ActualizarSolicitudFirmaDigitalRequest resultadoFirma = new ActualizarSolicitudFirmaDigitalRequest() { IdSolicitudFirmaDigital = peticion.IdSolicitudFirmaDigital };
                    AltaFirmaResponse respFirma = null;
                    bool sindDocsCondicionado = false;

                    if (procesoFirma.Archivos == null || procesoFirma.Archivos.Count == 0)
                    {
                        sindDocsCondicionado = solicitud.IdEstatus == (int)EstatusSolicitud.Condicionada;
                        respuesta.Error = true;
                        respuesta.MensajeOperacion = "No existen documentos para enviar a firmar.";
                    }
                    else
                    {
                        AltaSolicitudFirmaDigitalArchivo(peticion.IdSolicitudFirmaDigital, documentoArchivo);
                        respFirma = FirmaBLL.AltaFirma(procesoFirma);
                        if (respFirma == null)
                        {
                            respuesta.Error = true;
                            respuesta.MensajeOperacion = "Ocurrió un error al momento de procesar la información.";
                        }
                        else if (respFirma.Error)
                        {
                            respuesta.Error = true;
                            respuesta.MensajeOperacion = string.IsNullOrEmpty(respFirma.MensajeOperacion) ? "Error al enviar la información al servicio."
                                : respFirma.MensajeOperacion;
                            resultadoFirma.IdPeticion = respFirma.IdPeticion;
                            resultadoFirma.CodigoEstado = respFirma.StatusCode;
                        }
                        else
                        {
                            resultadoFirma.IdPeticion = respFirma.IdPeticion;
                            resultadoFirma.SignatureId = respFirma.SigantureId;
                            resultadoFirma.CodigoEstado = 200;
                        }
                    }

                    //Invalidar error si no existen documentos y la solicitud se encuntra condicionada y es del proceso de originacion
                    if (sindDocsCondicionado && altaSolicitudF.IdProceso == idProcesoOriginacion)
                    {
                        resultadoFirma.IdEstatus = (int)EstatusFirma.Signed;
                    }
                    else
                    {
                        resultadoFirma.IdEstatus = respuesta.Error ? (int)EstatusFirma.Failed : (int)EstatusFirma.Ready;
                    }
                    resultadoFirma.Mensaje = respuesta.MensajeOperacion;

                    _ = ActualizarSolicitudFirmaDigital(resultadoFirma);
                    #endregion
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
                _ = ActualizarSolicitudFirmaDigital(new ActualizarSolicitudFirmaDigitalRequest
                {
                    IdSolicitudFirmaDigital = peticion.IdSolicitudFirmaDigital,
                    IdEstatus = (int)EstatusFirma.Failed,
                    Mensaje = ex.Message
                });
            }
            return respuesta;
        }

        public static bool ExisteCambiosDatosDocumentos(ExisteCambiosDatosDocumentosRequest peticion)
        {
            ExisteCambiosDatosDocumentosResponse respuesta = new ExisteCambiosDatosDocumentosResponse();
            bool existenCambios = false;
            try
            {
                //TODO:Mejorar esta forma para obtener todos los datos que se ocupan en todos los documentos y tenerlos disponibles.
                SolicitudDocDigitalesResponse respDocumentos = SolicitudDocDigitalBLL.SolicitudDocDigitales(new SolicitudDocDigitalesRequest
                {
                    IdSolicitud = peticion.IdSolicitud,
                    Activo = true,
                    IdProceso = peticion.IdProceso
                });

                if (respDocumentos != null && respDocumentos.Documentos != null && respDocumentos.Documentos.Count > 0)
                {
                    CronogramaPagos cronograma = TBSolicitudesBll.ObtenerCronograma(peticion.IdSolicitud, null);
                    if (cronograma == null) cronograma = new CronogramaPagos() { Recibos = new List<CronogramaPagos.Recibo>() };
                    CultureInfo cultPeru = new CultureInfo("es-PE");

                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        var datos = (from s in con.dbo_TB_Solicitudes
                                     join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                     join es in con.Catalogo_TB_CATTipoVade on new { IdTipoVade = c.esta_civil ?? 0, Tipo = 1 } equals new { es.IdTipoVade, es.Tipo } into tEs
                                     from les in tEs.DefaultIfEmpty()
                                     join co in con.dbo_TB_CATColonia on c.IdColonia ?? 0 equals co.IdColonia into cco
                                     from lco in cco.DefaultIfEmpty()
                                     join cd in con.Clientes_TB_ClienteDirecciones on s.IdSolicitud equals cd.idSolicitud ?? 0 into scd
                                     from lcd in scd.DefaultIfEmpty()
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         c.Cliente,
                                         c.Nombres,
                                         c.apellidop,
                                         c.apellidom,
                                         IdSolicitud = ((int)s.IdSolicitud).ToString(),
                                         DNI = c.NumeroDocumento,
                                         EstadoCivil = les != null ? les.TipoVade : string.Empty,
                                         c.Celular,
                                         Telefono = c.telefono,
                                         c.Correo,
                                         c.direccion,
                                         SolicitudDireccion = lcd == null ? "" : lcd.nombreVia,
                                         Distrito = lco == null ? "" : lco.Colonia,
                                         s.fch_Solicitud,
                                         s.BCEntidad,
                                         s.BCOficina,
                                         s.BCCuenta,
                                         s.BCDC,
                                         s.CuentaDesembolso,
                                         s.CuentaCCI,
                                         s.NumeroTarjetaVisa,
                                         s.MesExpiraTarjetaVisa,
                                         s.AnioExpiraTarjetaVisa
                                     }).FirstOrDefault();
                        if (datos != null)
                        {
                            foreach (var doc in respDocumentos.Documentos)
                            {
                                List<string> datosDocumento = new List<string>();
                                string contenido = string.Empty;

                                switch ((DocumentoDigital)doc.IdDocumento)
                                {
                                    case DocumentoDigital.Contrato:
                                        datosDocumento.Add(datos.Cliente);
                                        break;
                                    case DocumentoDigital.Pagare:
                                        datosDocumento.Add("Pagare");
                                        break;
                                    case DocumentoDigital.AcuerdoLlenadoPagare:
                                        datosDocumento.Add(datos.BCOficina);
                                        datosDocumento.Add(datos.BCCuenta);
                                        datosDocumento.Add(datos.BCDC);
                                        datosDocumento.Add(cronograma.NombreCliente);
                                        datosDocumento.Add(cronograma.DireccionCliente);
                                        datosDocumento.Add(datos.DNI);
                                        datosDocumento.Add(datos.Celular);
                                        datosDocumento.Add(datos.IdSolicitud);
                                        datosDocumento.Add(cronograma.Recibos.Count.ToString());
                                        datosDocumento.Add(cronograma.Cuota.ToString("C", cultPeru));
                                        break;
                                    case DocumentoDigital.HojaResumen:
                                        datosDocumento.Add(cronograma.MontoDesembolsar.ToString("C", cultPeru));
                                        datosDocumento.Add(cronograma.Cuota.ToString("C", cultPeru));
                                        datosDocumento.Add($"{cronograma.TasaAnual:00.00} %");
                                        datosDocumento.Add($"{cronograma.TasaInteresMoratoria:00.00} %");
                                        datosDocumento.Add($"{cronograma.TasaCostoAnual:00.00} %");
                                        datosDocumento.Add(cronograma.TotalInteres.ToString("C", cultPeru));
                                        datosDocumento.Add(cronograma.NombreBancoDesembolso);
                                        datosDocumento.Add(cronograma.CuentaBancariaDesembolso);
                                        datosDocumento.Add(cronograma.NoTarjetaVisa);
                                        break;
                                    case DocumentoDigital.Cronograma:
                                        datosDocumento.Add($"{cronograma.NombreCliente} {cronograma.ApPaternoCliente} {cronograma.ApMaternoCliente}");
                                        datosDocumento.Add(cronograma.DNICliente);
                                        datosDocumento.Add(cronograma.DireccionCliente);
                                        datosDocumento.Add(cronograma.MontoDesembolsar.ToString("C", cultPeru));
                                        datosDocumento.Add($"{cronograma.TasaAnual:00.00} %");
                                        datosDocumento.Add($"{cronograma.TasaCostoAnual:00.00} %");
                                        datosDocumento.Add(cronograma.TotalInteres.ToString("C", cultPeru));
                                        datosDocumento.Add(cronograma.PrimerFechaPago.ToString("dd/MM/yyyy"));
                                        datosDocumento.Add(cronograma.UltimaFechaPago.ToString("dd/MM/yyyy"));
                                        datosDocumento.Add(cronograma.FechaDesembolso.ToString("dd/MM/yyyy"));
                                        break;
                                    case DocumentoDigital.AcuerdoDomiciliacion:
                                        datosDocumento.Add(datos.BCOficina);
                                        datosDocumento.Add(datos.BCCuenta);
                                        datosDocumento.Add(datos.BCDC);
                                        datosDocumento.Add($"{cronograma.NombreCliente} {cronograma.ApPaternoCliente} {cronograma.ApMaternoCliente}");
                                        datosDocumento.Add(cronograma.DireccionCliente);
                                        datosDocumento.Add(cronograma.DNICliente);
                                        datosDocumento.Add(datos.Celular);
                                        datosDocumento.Add(datos.IdSolicitud);
                                        datosDocumento.Add(cronograma.Recibos.Count.ToString());
                                        datosDocumento.Add(cronograma.Cuota.ToString("C", cultPeru));
                                        break;
                                    case DocumentoDigital.FormatoVisa:
                                        datosDocumento.Add($"{cronograma.NombreCliente} {cronograma.ApPaternoCliente} {cronograma.ApMaternoCliente}");
                                        datosDocumento.Add(cronograma.DNICliente);
                                        datosDocumento.Add(datos.Telefono);
                                        datosDocumento.Add(datos.Celular);
                                        datosDocumento.Add(datos.Correo);
                                        datosDocumento.Add(datos.NumeroTarjetaVisa ?? "");
                                        datosDocumento.Add(datos.MesExpiraTarjetaVisa != null ? datos.MesExpiraTarjetaVisa.Value.ToString("D2") : "");
                                        datosDocumento.Add(datos.AnioExpiraTarjetaVisa != null ? datos.AnioExpiraTarjetaVisa.Value.ToString() : "");
                                        break;
                                    case DocumentoDigital.CartaPoder:
                                        BuscarClienteResponse datosCliente = TBClientesBll.BuscarCliente(new Model.Request.Cliente.BuscarClienteRequest { IdSolicitud = peticion.IdSolicitud });
                                        if (datosCliente == null || datosCliente.Cliente == null) datosCliente = new BuscarClienteResponse { Cliente = new DatosClienteVM() };
                                        DatosClienteVM cliente = datosCliente.Cliente;
                                        CartaPoderVM datosCartaPoder = new CartaPoderVM()
                                        {
                                            NombreCliente = $"{cliente.Nombres} {cliente.Apellidos}",
                                            DNI = cliente.Dni,
                                            FechaSolicitud = datos?.fch_Solicitud ?? new DateTime(),
                                            Direccion = cliente.Direccion ?? "".ToUpper(),
                                            NoCuentaDom = datos == null ? "" :
                                                !string.IsNullOrWhiteSpace(datos.CuentaDesembolso) ? datos.CuentaDesembolso : datos.CuentaCCI
                                        };
                                        datosDocumento.Add(datosCartaPoder.NombreCliente);
                                        datosDocumento.Add(datosCartaPoder.DNI);
                                        datosDocumento.Add(datosCartaPoder.Direccion);
                                        datosDocumento.Add(datosCartaPoder.NoCuentaDom);
                                        datosDocumento.Add($"{datosCartaPoder.FechaSolicitud:dd/MM/yyyy}");
                                        break;
                                    case DocumentoDigital.DeclaracionJurada:
                                        datosDocumento.Add(datos.Cliente);
                                        datosDocumento.Add(datos.DNI);
                                        break;
                                    case DocumentoDigital.FormularioAmpliacion:
                                        datosDocumento.Add($"{datos.Nombres} {datos.apellidop} {datos.apellidop}");
                                        datosDocumento.Add($"{datos.DNI}");
                                        break;
                                }
                                contenido = string.Join("|", datosDocumento);
                                existenCambios = !contenido.Equals(doc.Contenido ?? "", StringComparison.InvariantCultureIgnoreCase);
                                if (existenCambios) break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return existenCambios;
        }
        #endregion

        #region Metodos privados
        private static void AltaSolicitudFirmaDigitalArchivo(int idSolicitudFirmaDigital, List<SolicitudFirmaDigitalArchivoVM> archivos)
        {
            try
            {
                if (idSolicitudFirmaDigital > 0 && archivos != null && archivos.Count > 0)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        con.Solicitudes_TB_SolicitudFirmaDigitalArchivo.AddRange(archivos
                            .Select(a => new TB_SolicitudFirmaDigitalArchivo
                            {
                                IdSolicitudFirmaDigital = idSolicitudFirmaDigital,
                                IdDocumento = a.IdDocumento,
                                Orden = a.Orden,
                                NombreDocumentoDigital = a.NombreDocumentoDigital
                            }).ToList());
                        con.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {idSolicitudFirmaDigital} - {JsonConvert.SerializeObject(archivos)}", JsonConvert.SerializeObject(ex));
            }
        }

        private static string ContenidoBase64(ImpresionDocumentoResponse documento)
        {
            string contenido = string.Empty;
            if (documento == null || documento.Error || documento.Resultado == null || documento.Resultado.ContenidoArchivo == null) return contenido;
            try
            {
                contenido = Convert.ToBase64String(documento.Resultado.ContenidoArchivo);
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(documento)}", JsonConvert.SerializeObject(ex));
            }
            return contenido;
        }

        private static void IncluirDocumento(DocumentoDigital docDigital, SolicitudDocDigitalVM documento, ImpresionDocumentoResponse archivo, bool validarCambios,
            Dictionary<DocumentoDigital, List<PosicionFirmaVM>> documentoFirma)
        {
            bool agregar = true;
            if (documento != null && archivo != null && documentoFirma != null)
            {
                documento.ContenidoB64 = ContenidoBase64(archivo);
                documento.Documento = docDigital.GetDescription();

                if (!string.IsNullOrEmpty(documento.ContenidoB64) && archivo.Resultado.Firmas != null && archivo.Resultado.Firmas.Count > 0)
                {
                    if (validarCambios)
                    {
                        agregar = !archivo.Resultado.DatosDocumento.Equals(documento.Contenido, StringComparison.InvariantCultureIgnoreCase);
                    }
                    if (agregar)
                    {
                        documentoFirma.Add(docDigital, archivo.Resultado.Firmas);
                        SolicitudDocDigitalBLL.ActualizarSolicitudDocDigital(new SolicitudDocDigitalVM
                        {
                            IdSolicitudDocDigital = documento.IdSolicitudDocDigital,
                            Contenido = archivo.Resultado.DatosDocumento
                        });
                    }
                }
            }
        }

        #endregion
    }
}
