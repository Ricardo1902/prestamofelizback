﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Poco.Solicitudes;
using SOPF.Core.Solicitudes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Solcitudes
{
    public static class DocumentoDigitalBLL
    {
        public static TB_DocumentoDigital SolicitudDocumentoDigital(int idSoliciutd, DocumentoDigital documento)
        {
            TB_DocumentoDigital documentoDigital = null;
            try
            {
                if (idSoliciutd <= 0) return documentoDigital;
                int idDocumentoDigital = (int)documento;
                if (idDocumentoDigital <= 0) return documentoDigital;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var solicitud = con.dbo_TB_Solicitudes.Where(s => s.IdSolicitud == idSoliciutd).Select(s => new { idSoliciutd, s.fch_Solicitud })
                        .FirstOrDefault();
                    if (solicitud == null) return documentoDigital;

                    List<TB_DocumentoDigital> documentosD = con.Solicitudes_TB_DocumentoDigital
                        .Where(d => d.IdDocumento == idDocumentoDigital
                            && d.Activo)
                        .OrderByDescending(o => o.FechaRegistro)
                        .ToList();

                    if (documentosD != null && documentosD.Count > 0)
                    {
                        documentoDigital = documentosD.Where(d =>
                            solicitud.fch_Solicitud >= (d.InicioVigencia ?? solicitud.fch_Solicitud)
                            && solicitud.fch_Solicitud <= (d.FinVigencia ?? DateTime.Now))
                            .OrderByDescending(o => o.FechaRegistro)
                            .FirstOrDefault();
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { idSoliciutd, documento })}", JsonConvert.SerializeObject(ex));
            }
            return documentoDigital;
        }
    }
}
