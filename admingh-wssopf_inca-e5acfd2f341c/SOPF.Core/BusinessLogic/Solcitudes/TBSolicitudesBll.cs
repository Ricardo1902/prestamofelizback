using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v2;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Newtonsoft.Json;
using SOPF.Core.BusinessLogic.Catalogo;
using SOPF.Core.BusinessLogic.Clientes;
using SOPF.Core.BusinessLogic.Cobranza;
using SOPF.Core.BusinessLogic.Creditos;
using SOPF.Core.BusinessLogic.Dbo;
using SOPF.Core.BusinessLogic.Reportes;
using SOPF.Core.BusinessLogic.Solcitudes;
using SOPF.Core.BusinessLogic.Sistema;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Solicitudes;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Model.Clientes;
using SOPF.Core.Model.Reportes;
using SOPF.Core.Model.Request.Creditos;
using SOPF.Core.Model.Request.Reportes;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response;
using SOPF.Core.Model.Response.Cliente;
using SOPF.Core.Model.Response.Creditos;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Model.Response.Sistema;
using SOPF.Core.Model.Solicitudes;
using SOPF.Core.Poco.Clientes;
using SOPF.Core.Poco.dbo;
using SOPF.Core.Poco.Solicitudes;
using SOPF.Core.Solicitudes;
using SOPF.Core.APP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;

namespace SOPF.Core.BusinessLogic.Solicitudes
{
    public static class TBSolicitudesBll
    {
        private static TBSolicitudesDal dal;

        static TBSolicitudesBll()
        {
            dal = new TBSolicitudesDal();
        }

        public static DataResultSolicitudes InsertarTBSolicitudes(int Accion, int idUsuario, TBSolicitudes entidad)
        {
            return dal.InsertarTBSolicitudes(Accion, idUsuario, entidad);
        }

        public static int UpdEstProceso(int Accion, int idSolicitud, int EstProceso, int idUsuario)
        {
            int resultado = dal.UpdEstProceso(Accion, idSolicitud, EstProceso, idUsuario);

            //Reactivaci�n de solicitud
            if (Accion == 4 && EstProceso == 8 && resultado == 0)
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    con.MesaControl_SP_DocumentosObservarMCACT(new Model.Request.MesaControl.SP_DocumentosObservarMCACTRequest { IdSolicitud = idSolicitud });
                }
            }
            return resultado;
        }


        public static DataSet ObtenerTBSolicitudes(int Accion, int cuenta, int idcliente, string cliente, string fchInicial, string fchFinal, int idsucursal, int idconvenio, int idlinea)
        {
            DataSet dsSolicitudes = new DataSet();
            DataTable dtSolicitudes = new DataTable();
            List<TBSolicitudes> listFiniquito = new List<TBSolicitudes>();
            listFiniquito = dal.ObtenerTBSolicitudes(Accion, cuenta, idcliente, cliente, fchInicial, fchFinal, idsucursal, idconvenio, idlinea);

            dtSolicitudes = ConvertToDatatable(listFiniquito);
            dsSolicitudes.Tables.Add(dtSolicitudes);

            return dsSolicitudes;
            //return dal.ObtenerTBSolicitudes(Accion,cuenta);
        }

        public static DataTable ConvertToDatatable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static void ActualizarTBSolicitudes()
        {
            dal.ActualizarTBSolicitudes();
        }

        public static void EliminarTBSolicitudes()
        {
            dal.EliminarTBSolicitudes();
        }

        public static List<TBSolicitudes.DatosSolicitud> ObtenerDatosSolicitud(int idsolicitud)
        {
            return dal.ObtenerDatosSolicitud(idsolicitud);
        }

        public static int ValidaClabeBll(string clabe)
        {
            return dal.ValidaClabeDal(clabe);
        }

        public static Resultado<List<TBSolicitudes.ListaNegra>> ValidaListaNegra(TBSolicitudes.ListaNegra entidad)
        {
            return dal.ValidaListaNegra(entidad);
        }

        public static Resultado<bool> ValidaTarjetaVISADomiciliacion(string NumeroTarjeta)
        {
            return dal.ValidaTarjetaVISADomiciliacion(NumeroTarjeta);
        }

        public static int ValidaTipoRenovacionDal(int idCliente)
        {
            return dal.ValidaTipoRenovacionDal(idCliente);
        }

        public static void InsertarDetSolicitud(int Accion, int idsolicitud, int secuencia, string descripcion, decimal cantidad, decimal p_unitario, decimal capital, decimal interes, decimal Iva, string Pedido, string FinancieraPedido, int IdUsuario)
        {
            dal.InsertarDetSolicitud(Accion, idsolicitud, secuencia, descripcion, cantidad, p_unitario, capital, interes, Iva, Pedido, FinancieraPedido, IdUsuario);
        }

        public static int ActualizaComentariosSol(int IdSolicitud, int RolUsuario, int Accion)
        {
            int respuesta = dal.ActualizaComentariosSol(IdSolicitud, RolUsuario, Accion);

            return respuesta;
        }

        public static int AltaComentariosSolicitud(int IdSolicitud, int IdComentarioPadre, string Comentarios, int UsuarioAlta, int RolUsuario)
        {
            int respuesta = dal.AltaComentariosSolicitud(IdSolicitud, IdComentarioPadre, Comentarios, UsuarioAlta, RolUsuario);

            return respuesta;
        }

        public static DataSet ConComentariosSolicitud(int IdSolicitud, int IdComentarioPadre, int TipoConsulta)
        {
            DataSet dsComentarios = new DataSet();
            DataTable dtComentarios = new DataTable();
            List<TBSolicitudes.ComentariosSolicitud> listComentarios = new List<TBSolicitudes.ComentariosSolicitud>();
            listComentarios = dal.ConComentariosSolicitud(IdSolicitud, IdComentarioPadre, TipoConsulta);

            dtComentarios = ConvertToDatatable(listComentarios);
            dsComentarios.Tables.Add(dtComentarios);

            return dsComentarios;
        }

        public static ObtenerSolicitudResponse ObtenerSolicitud(int IDUsuario, int IDSolicitud)
        {
            ObtenerSolicitudResponse respuesta = new ObtenerSolicitudResponse();
            try
            {
                #region Validaciones
                if (IDUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es v�lido.");
                if (IDSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es v�lido.");
                #endregion

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta.Solicitud = con.dbo_TB_Solicitudes.Where(s => s.IdSolicitud == IDSolicitud)
                        .Select(s => new SolicitudVM
                        {
                            IdSolicitud = (int)s.IdSolicitud,
                            IdCliente = (int)s.IdCliente,
                            FechaSolicitud = s.fch_Solicitud,
                            IdTipoCredito = s.idTipoCredito ?? 0,
                            IdEstatus = s.IdEstatus ?? 0,
                            EstProceso = s.Est_Proceso ?? 0,
                            IdAsignado = s.IdAsignado ?? 0,
                            Banco = s.Banco
                        })
                        .FirstOrDefault();
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "No fue posible realizar la carga de los documentos.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { IDUsuario, IDSolicitud })}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static SOPF.Core.Entities.Resultado<bool> ActualizaSolicitud(TBSolicitudes.ModificaSolicitud Solicitud, int IDUsuario_Modifica)
        {
            return dal.ActualizaSolicitud(Solicitud, IDUsuario_Modifica);
        }

        public static SOPF.Core.Entities.Resultado<bool> AsignaAnalista(int IDSolicitud, int IDUsuario, int Validacion)
        {
            return dal.AsignaAnalista(IDSolicitud, IDUsuario, Validacion);
        }

        public static List<Combo> ObtenerCATEst_Proceso()
        {
            return dal.ObtenerCATEst_Proceso();
        }
        //##### ##### ##### ##### #####

        public static DataResultSolicitudes ValidaDatosBll(int Accion, string Parametro, int idSolicitud, int TipoResultado)
        {
            DataResultSolicitudes resultado = new DataResultSolicitudes();

            resultado = dal.ValidaDatosDal(Accion, Parametro, idSolicitud, TipoResultado);

            return resultado;
        }

        //PERU
        //##### ##### ##### ##### #####
        public static DataResultSolicitudes InsertarSolicitud_Peru(int Accion, int idUsuario, TBSolicitudes.SolicitudPeru entidad)
        {
            return dal.InsertarSolicitud_Peru(Accion, idUsuario, entidad);
        }

        public static CronogramaPagos ObtenerCronograma(int IdSolicitud, DateTime? FechaDesembolso)
        {
            return dal.ObtenerCronograma(IdSolicitud, FechaDesembolso);
        }
        //##### ##### ##### ##### ##### PERU

        #region Expedientes
        public static string GuardaExpediente(TBSolicitudes.Expediente _Expediente)
        {
            return dal.GuardaExpediente(_Expediente);
        }

        public static TBSolicitudes.ExpedienteB64 DescargaExpediente(TBSolicitudes.Expediente _Expediente)
        {
            TBSolicitudes.ExpedienteB64 _ExpedienteB64 = new TBSolicitudes.ExpedienteB64();
            _ExpedienteB64.IDResultado = 1;

            try
            {
                int intTipoBusqueda = 0;
                string strPathArchivo = "";
                string strError = "";

                int auxIDSolicitud = 0;
                auxIDSolicitud = dal.getIDSolicitud(_Expediente.IDSolicitud);
                if (auxIDSolicitud > 0)
                {
                    _Expediente.IDSolicitud = auxIDSolicitud;
                }

                intTipoBusqueda = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["PDF_TipoBusqueda"]);
                if (intTipoBusqueda == 0)
                {
                    _Expediente.IDGoogleDrive = "";
                    _Expediente = dal.ConsultaExpediente(_Expediente);
                    if (_Expediente.IDSolicitud != -1 && _Expediente.IDGoogleDrive != "")
                    {
                        if (descargarArchivo(_Expediente.IDGoogleDrive, ref strPathArchivo, ref strError))
                        {
                            byte[] arrayByte = System.IO.File.ReadAllBytes(strPathArchivo);
                            _ExpedienteB64.B64 = Convert.ToBase64String(arrayByte);
                            if (System.IO.File.Exists(strPathArchivo))
                                System.IO.File.Delete(strPathArchivo);
                        }
                        else
                        {
                            intTipoBusqueda = 1;
                        }
                    }
                    else
                    {
                        intTipoBusqueda = 1;
                    }
                }
                else if (intTipoBusqueda == 1)
                {
                    strPathArchivo = System.Configuration.ConfigurationManager.AppSettings["PDF_DirectorioPDF"];
                    strPathArchivo += _Expediente.IDSolicitud.ToString();
                    if (_Expediente.IDDocumento != 0)
                        strPathArchivo += "-" + _Expediente.IDDocumento.ToString();
                    strPathArchivo += ".pdf";
                    if (System.IO.File.Exists(strPathArchivo))
                    {
                        byte[] arrayByte = System.IO.File.ReadAllBytes(strPathArchivo);
                        _ExpedienteB64.B64 = Convert.ToBase64String(arrayByte);
                    }
                    else
                    {
                        _ExpedienteB64.B64 = "No existe el archivo";
                        return _ExpedienteB64;
                    }
                }
                _ExpedienteB64.IDResultado = 0;
            }
            catch (Exception ex)
            {
                _ExpedienteB64.B64 = ex.Message;

                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(_Expediente)}", JsonConvert.SerializeObject(ex));
            }
            return _ExpedienteB64;
        }

        public static DescargarArchivoGDResponse DescargaArchivoGDrive(DescargarArchivoGDRequest peticion)
        {
            DescargarArchivoGDResponse respuesta = new DescargarArchivoGDResponse();

            try
            {
                // Validaciones
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (string.IsNullOrEmpty(peticion.IdArchivoGD)) throw new ValidacionExcepcion("ID de Google Drive no puede estar vac�o.");

                // Depurar directorio temporal de archivos
                string dirEC = AppSettings.DirDocumentoECTemp;
                if (peticion.RegresaURL)
                {
                    if (!Directory.Exists(dirEC)) { throw new ValidacionExcepcion("No fue posible realizar la descarga porque no se encontr� la ruta de guardado para el archivo. Favor de contactar a soporte t�cnico."); }

                    DirectoryInfo d = new DirectoryInfo(dirEC);

                    int limitDays = 0;
                    List<SOPF.Core.Poco.dbo.TB_CATParametro> parametros = ParametrosBLL.Parametro("DEC");
                    if (parametros != null && parametros.Count > 0)
                        limitDays = Convert.ToInt32(parametros[0].valor); //5 default
                    else
                        limitDays = 5;
                    DateTime pasada = DateTime.Now.AddDays(-limitDays);
                    DateTime limite = new DateTime(pasada.Year, pasada.Month, pasada.Day, 23, 59, 59);
                    FileInfo[] files = d.GetFiles("*").Where(f => f.CreationTime <= limite).ToArray();

                    foreach (FileInfo fil in files)
                    {
                        fil.Delete();
                    }
                }

                // Descargar archivo desde Google Drive
                DocumentoVM archivo = new DocumentoVM();

                string error = string.Empty;
                using (ServidorGoogleDrive wsGo = new ServidorGoogleDrive())
                {
                    ArchivoGoogleDrive descarga = wsGo.DescargarArchivo(peticion.IdArchivoGD, ref error);
                    if (string.IsNullOrEmpty(error) && descarga.Contenido != null && descarga.Contenido.Length > 0)
                    {
                        respuesta.ArchivoGoogle = descarga;

                        if (peticion.RegresaURL)
                        {
                            try
                            {                               
                                Utils.GuardarArchivo($"{respuesta.ArchivoGoogle.NombreArchivo}", respuesta.ArchivoGoogle.Contenido, dirEC);
                            }
                            catch
                            {
                                throw new ValidacionExcepcion("El archivo no es v�lido, favor de verificarlo.");
                            }
                            respuesta.ArchivoGoogle.Contenido = null;                          
                            respuesta.TotalRegistros = 1;
                            respuesta.ArchivoGoogle.URL = System.ServiceModel.OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.OriginalString.Replace(System.ServiceModel.OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.PathAndQuery, "")
                                                    + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings.Get("DirDocumentoECTemp")
                                                    + "/" + respuesta.ArchivoGoogle.NombreArchivo;
                        }
                    }
                    else
                        throw new Exception(error);
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al descargar el archivo de Google Drive.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }
        #endregion

        #region GoogleDrive
        static string[] Scopes = { DriveService.Scope.Drive };
        static UserCredential credential;

        public static bool cnxGoogle(ref string Error)
        {
            Boolean resultado = false;
            Error = "";
            try
            {
                string strAuthPath = "";
                strAuthPath = System.Configuration.ConfigurationManager.AppSettings["Auth_GoogleDrive"];

                if (!System.IO.File.Exists(strAuthPath + "SistemasOA.json"))
                {
                    Error = "No existe archivo json";

                    Utils.EscribirLog($"Error en {Error}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                        $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject("No se encontro archivo json de autenticacion"));
                    return false;
                }

                using (var stream = new FileStream(strAuthPath + "SistemasOA.json", FileMode.Open, FileAccess.Read))
                {
                    string credPath = strAuthPath + "user_json";

                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        Scopes,
                        "infraGH",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;

                Utils.EscribirLog($"Error en {ex.Message + ", " + Error}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
            }
            return resultado;
        }

        public static bool descargarArchivo(string IDArchivo, ref string URLArchivo, ref string Error)
        {
            bool blnResultado = false;
            Error = "";
            if (cnxGoogle(ref Error))
            {
                try
                {
                    var service = new DriveService(new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "appDrive",
                    });

                    Google.Apis.Drive.v2.Data.File fileGoogle = service.Files.Get(IDArchivo).Execute();
                    var stream = DownloadFile(fileGoogle, ref Error);
                    if (stream != null)
                    {
                        string PathArchivo = "";
                        PathArchivo = System.Configuration.ConfigurationManager.AppSettings["PDF_TMP"];
                        URLArchivo = PathArchivo + fileGoogle.Id + "." + fileGoogle.FileExtension;
                        FileStream FS = new FileStream(PathArchivo + fileGoogle.Id + "." + fileGoogle.FileExtension, FileMode.Create);
                        stream.CopyTo(FS);
                        FS.Close();
                        blnResultado = true;
                    }
                }
                catch (Exception ex)
                {
                    Error = ex.Message;

                    Utils.EscribirLog($"Error en {ex.Message + ", " + Error}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                        $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
                }
            }
            return blnResultado;
        }

        public static System.IO.Stream DownloadFile(Google.Apis.Drive.v2.Data.File file, ref string Error)
        {
            Error = "";
            if (!String.IsNullOrEmpty(file.DownloadUrl))
            {
                try
                {
                    var service = new DriveService(new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "appDrive",
                    });

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(file.DownloadUrl));
                    //El Header Authorization lo agregamos de forma manual
                    //authenticator.ApplyAuthenticationToRequest(request);           
                    request.Headers.Add("Authorization: Bearer " + credential.Token.AccessToken);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        return response.GetResponseStream();
                    }
                    else
                    {
                        Error = response.StatusDescription;
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    Error = ex.Message;

                    Utils.EscribirLog($"Error en {ex.Message + ", " + Error}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                        $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));

                    return null;
                }
            }
            else
            {
                Error = "No hay URL de descarga";
                return null;
            }
        }
        #endregion

        public static List<TBSolicitudes.Documentos> ObtenerDocumentos(int IDSolicitud)
        {
            return dal.ObtenerDocumentos(IDSolicitud);
        }

        public static SolicitudReestructura ObtenerReestructura(SolicitudReestructuraCondiciones condiciones)
        {
            return dal.ObtenerReestructura(condiciones);
        }

        public static int InsertarDireccionSolicitud(SolicitudDireccion direccion)
        {
            return dal.InsertarDireccionSolicitud(direccion);
        }

        public static int ActualizarDireccionSolicitud(SolicitudDireccion direccion)
        {
            return dal.ActualizarDireccionSolicitud(direccion);
        }

        public static List<SolicitudDireccion> ConsultarDireccionSolicitud(SolicitudDireccionCondiciones condiciones)
        {
            return dal.ConsultarDireccionSolicitud(condiciones);
        }

        public static PoliticaAmpliacion PoliticaAmplicacionParaCliente(int idCliente)
        {
            return dal.PoliticaAmplicacionParaCliente(idCliente);
        }

        public static Decimal ObtenerGAT()
        {
            return dal.ObtenerGAT();
        }
        public static Resultado<bool> InsertarCuentaEmergente(TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente, int idUsuario)
        {
            return dal.InsertarCuentaEmergente(cuentaEmergente, idUsuario);
        }
        public static Resultado<bool> ActualizarCuentaEmergente(TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente, int idUsuario)
        {
            return dal.ActualizarCuentaEmergente(cuentaEmergente, idUsuario);
        }
        public static Resultado<bool> ActualizarDatosSolicitudGestiones(int idUsuario, TBSolicitudes.ActualizarSolicitudGestiones solicitud)
        {
            return dal.ActualizarDatosSolicitudGestiones(idUsuario, solicitud);
        }
        public static ValidarEnvioProcesoDigitalResponse ValidarProcesoAnalisis(ValidarEnvioProcesoDigitalRequest peticion)
        {
            ValidarEnvioProcesoDigitalResponse respuesta = new ValidarEnvioProcesoDigitalResponse();
            try
            {
                if (peticion == null || peticion.Solicitud == null || peticion.ProcesoDigital == null)
                    throw new ValidacionExcepcion("Los datos proporcionados no son v�lidos.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es v�lida.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_SolicitudProcesoDigital procesoDigital = con.Solicitudes_TB_SolicitudProcesoDigital
                        .Where(sp => sp.IdSolicitudProcesoDigital == peticion.ProcesoDigital.IdSolicitudProcesoDigital)
                        .FirstOrDefault();
                    if (procesoDigital != null)
                    {
                        procesoDigital.IdEstatus = (int)EstatusProcesoDigital.EnProceso;
                        con.SaveChanges();
                    }
                }

                #region Envio a Mesa de control
                if (peticion.Solicitud.IdEstatus == (int)EstatusSolicitud.Condicionada)
                {
                    UpdEstProceso(4, peticion.Solicitud.IdSolicitud, 8, (int)UsuarioSistema.Administrador);
                    UpdEstProceso(3, peticion.Solicitud.IdSolicitud, 8, (int)UsuarioSistema.Administrador);
                }
                else
                {
                    UpdEstProceso(3, peticion.Solicitud.IdSolicitud, 8, (int)UsuarioSistema.Administrador);
                }

                SolicitudProcesoDigitalBLL.Actualizar(new ActualizarProcesoDigitalRequest
                {
                    EstatusProcesoDigital = EstatusProcesoDigital.Aprobado,
                    IdSolicitudProcesoDigital = peticion.ProcesoDigital.IdSolicitudProcesoDigital
                });
                #endregion
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static EnviarCronogramaResponse EnviarCronograma(EnviarCronogramaRequest peticion)
        {
            EnviarCronogramaResponse respuesta = new EnviarCronogramaResponse();
            try
            {
                if (peticion == null || peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La petici�n no es correcta.");
                int idSolicitud = 0;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    if (peticion.IdSolicitud > 0)
                    {
                        idSolicitud = peticion.IdSolicitud;
                    }
                    else if (peticion.IdCuenta > 0)
                    {
                        idSolicitud = con.dbo_TB_Creditos.Where(c => c.IdCuenta == peticion.IdCuenta)
                            .Select(c => (int)c.IdSolicitud)
                            .DefaultIfEmpty(0)
                            .FirstOrDefault();
                    }

                    if (idSolicitud > 0)
                    {
                        string clavePeticion = string.Empty;
                        switch (peticion.Origen)
                        {
                            case OrigenEnvioCronograma.Gestion: clavePeticion = "CEGRC"; break;
                            case OrigenEnvioCronograma.Creditos: clavePeticion = "CECRC"; break;
                        }

                        var datoCte = (from s in con.dbo_TB_Solicitudes
                                       join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                       where s.IdSolicitud == idSolicitud
                                       select new { c.Cliente, c.Nombres, c.apellidop, c.apellidom, c.Correo }
                                            ).FirstOrDefault();

                        Poco.Sistema.TB_TipoNotificacion tipoNotificacion = Sistema.TipoNotificacionBLL.TipoNotificacion(clavePeticion);
                        if (tipoNotificacion == null || tipoNotificacion.IdTipoNotificacion <= 0) throw new ValidacionExcepcion("No fue posible realizar el envio. Consulte a soporte t�cnico.");
                        if (datoCte == null || string.IsNullOrEmpty(datoCte.Correo)) throw new ValidacionExcepcion("No se pudo obtener la cuenta de coreo del cliente.");
                        Model.Request.Sistema.NuevoRequest nuevaNotif = new Model.Request.Sistema.NuevoRequest
                        {
                            IdUsuarioRegistro = peticion.IdUsuario,
                            IdTipoNotificacion = tipoNotificacion.IdTipoNotificacion,
                            IdSolicitud = idSolicitud,
                            Titulo = string.IsNullOrEmpty(tipoNotificacion.TituloCorreo) ? "Reimpresi�n de Cronograma" : tipoNotificacion.TituloCorreo,
                            NombreCliente = $"{datoCte.Nombres} {datoCte.apellidop} {datoCte.apellidom}",
                            CorreoElectronico = datoCte.Correo,
                        };
                        if (!string.IsNullOrEmpty(peticion.CopiaCC)) nuevaNotif.CorreoCopia = peticion.CopiaCC;

                        Model.Response.Sistema.NuevoResponse respNoti = Sistema.NotificacionBLL.Nuevo(nuevaNotif);
                        if (respNoti != null)
                        {
                            respuesta.Error = respNoti.Error;
                            respuesta.MensajeOperacion = respNoti.MensajeOperacion;
                        }
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static List<TBSolicitudes.Documentos> DocumentosCondicionados(DocumentosCondicionadosRequest peticion)
        {
            List<TBSolicitudes.Documentos> respuesta = new List<TBSolicitudes.Documentos>();
            try
            {
                #region Validaciones
                if (peticion == null) throw new ValidacionExcepcion("La petici�n no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es correcta.");
                #endregion

                ObtenerSolicitudResponse sol = ObtenerSolicitud(peticion.IdUsuario, peticion.IdSolicitud);
                if (sol.Solicitud == null && sol.Solicitud.IdSolicitud <= 0) return respuesta;
                DateTime? fechaCargaV2 = ParametrosBLL.FechaCargaExpedienteV2();

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var documentos = (from dob in con.MesaControl_TB_DocumentosObservar
                                      join cd in con.Solicitudes_TB_CATDocumentos on
                                        new { IdSolicitud = (int)dob.Solicitud_Id, IDDocumento = dob.Documento_Id } equals new { peticion.IdSolicitud, cd.IDDocumento }
                                      where dob.RegistroActual == true
                                      select new
                                      {
                                          cd.IDDocumento,
                                          cd.Documento,
                                          dob.Comentarios,
                                          dob.Fch_Registro
                                      })
                                   .ToList();

                    if (documentos != null && documentos.Count > 0)
                    {
                        respuesta = documentos
                            .Select(d => new TBSolicitudes.Documentos
                            {
                                idDocumento = d.IDDocumento,
                                Documento = d.Documento,
                                Condicionado = true,
                                MotivoCondicionado = d.Comentarios
                            })
                            .ToList();

                    }
                    else if (fechaCargaV2 == null || sol.Solicitud.FechaSolicitud < fechaCargaV2)
                    {
                        respuesta = ObtenerDocumentos(peticion.IdSolicitud);
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                Utils.EscribirLog($"{vex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(vex));
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        #region Impresion de Documentos
        public static ImpresionDocumentoResponse ImpresionTablaAmortizacion(ImpresionTablaAmortizacionRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");
                if (peticion.Version == 0)
                {
                    TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.Cronograma);
                    if (docD != null)
                    {
                        peticion.Version = docD.Version;
                    }
                }
                TablaAmortizacion tablaAmortizacion = new TablaAmortizacion(peticion.Version);
                if (peticion.EsReimpresion ?? false)
                {
                    tablaAmortizacion.Titulo = "Reimpresi�n de Cronograma de Pagos";
                }
                else if (peticion.EsSimulado ?? false)
                {
                    tablaAmortizacion.Titulo = "Cronograma de Pagos por Reestructura";
                    tablaAmortizacion.EsExtrajudicial = true;
                    tablaAmortizacion.IdSolicitudNuevo = peticion.IdSolicitud;
                }
                CronogramaPagos amortizacion = new CronogramaPagos();
                amortizacion = ObtenerCronograma(peticion.IdSolicitud, peticion.FechaDesembolso);
                if (amortizacion == null || amortizacion.Recibos == null || amortizacion.Recibos.Count == 0) throw new ValidacionExcepcion("No existe informaci�n de la cuenta.");

                if (peticion.EsSimulado ?? false)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        int[] estatusPetResImpresion = { 5 };
                        var reest = (from p in con.Solicitudes_TB_PeticionReestructuras
                                     join tr in con.Solicitudes_TB_CATTipoReestructuras on p.TipoReestructura_Id equals tr.Id_TipoReestructura
                                     join f in con.Solicitudes_TB_FotoPeticionReestructuras on p.Id_PeticionReestructura equals f.PeticionReestructura_Id
                                     join s in con.dbo_TB_Solicitudes on p.Solicitud_Id equals s.IdSolicitud
                                     join t in con.dbo_TB_CATTipoCredito_PE on s.IDCredito_PE equals t.IDCredito
                                     where tr.TipoReestructura == "Extrajudicial" && p.Actual && p.IdSolicitudNuevo == peticion.IdSolicitud && estatusPetResImpresion.Contains(p.EstatusPeticionReestructuras_Id)
                                     select new
                                     {
                                         Plazo = p.Pagos,
                                         IdTipoReestructura = p.TipoReestructura_Id,
                                         FechaDesembolso = p.FechaRegistro,
                                         Importe = p.TotalLiquidacionRestructura.HasValue ? p.TotalLiquidacionRestructura :
                                             f.TotalLiquidacion - (con.Solicitudes_TB_FotoReciboPetRes.Where(r => r.FotoPeticionReestructura_Id == f.Id_FotoPeticionReestructura && r.FotoTipoReciboPetRes_Id == 2).GroupBy(s => 1).Select(s => new
                                             {
                                                 Total = s.Sum(x => x.Liquida_Capital) + s.Sum(x => x.Liquida_Interes) + s.Sum(x => x.Liquida_IGV) + s.Sum(x => x.Liquida_Seguro) + s.Sum(x => x.Liquida_GAT)
                                             }).FirstOrDefault().Total)
                                     }).FirstOrDefault();
                        if (reest == null)
                            throw new ValidacionExcepcion("No se encontr� informaci�n de restructura en obtener simulacion de cronograma para formato.");

                        string fechaSimula = string.Concat(reest.FechaDesembolso.Year.ToString(), reest.FechaDesembolso.Month.ToString(), reest.FechaDesembolso.Day.ToString());

                        Resultado<ObtenerSimulacionCreditoResponse> res = TBCreditosBll.ObtenerSimulacionCredito(new ObtenerSimulacionCreditoRequest()
                        {
                            IdSolicitud = peticion.IdSolicitud,
                            IdTipoReestructura = reest.IdTipoReestructura,
                            Importe = reest.Importe.Value,
                            Plazo = reest.Plazo,
                            FechaDesembolso = fechaSimula
                        });

                        if (res.Codigo > 0)
                            throw new ValidacionExcepcion("Ocurri� un error al obtener simulaci�n " + res.Mensaje ?? "");

                        if (res != null && res.ResultObject != null && res.ResultObject.Resultado != null
                            && res.ResultObject.Resultado.Count > 0)
                        {
                            res.ResultObject.Resultado.RemoveAt(res.ResultObject.Resultado.Count - 1);
                            amortizacion.IdSolicitud = peticion.IdSolicitud;
                            amortizacion.Recibos = res.ResultObject.Resultado;
                        }
                    }
                }

                byte[] tAmortizacion = tablaAmortizacion.Generar(amortizacion);
                if (tAmortizacion == null || tAmortizacion.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del cronograma.");

                respuesta.Resultado.ContenidoArchivo = tAmortizacion;
                respuesta.Resultado.TipoContenido = "application/pdf";
                respuesta.Resultado.NombreArchivo = $"Cronograma_{peticion.IdSolicitud}";
                respuesta.Resultado.Extension = ".pdf";
                respuesta.Resultado.Firmas = tablaAmortizacion.Firmas;
                respuesta.Resultado.DatosDocumento = tablaAmortizacion.DatosDocumento;
                respuesta.TotalRegistros = 1;

                if (!peticion.EsSimulado ?? false)
                {
#if DEBUG
                    Utils.GuardarArchivo($"Cronograma_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el cronograma.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionAmpliacionCredito(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    CronogramaPagos datosCredito = null;
                    var solicitud = (from s in con.dbo_TB_Solicitudes
                                     join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         s.IdSolicitud,
                                         s.IDCredito_PE,
                                         c.Correo,
                                         s.articulos
                                     }).FirstOrDefault();
                    AmpliacionCreditoVM datosAmpliacion = new AmpliacionCreditoVM();

                    if (solicitud == null) throw new ValidacionExcepcion("No existe informaci�n sobre la cuenta.");
                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.FormularioAmpliacion);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }

                    if (solicitud.IDCredito_PE == 3)
                    {
                        var estatus = TB_CATEstatusBLL.ObtenerEstatusAmpliacionExpress();
                        int[] idEstatusValidos = null;
                        if (estatus != null)
                        {
                            idEstatusValidos = estatus.Where(e => new string[] { "P", "PD", "RD" }.Contains(e.Estatus))
                                .Select(e => e.IdEstatus)
                                .ToArray();
                        }
                        if (idEstatusValidos == null) idEstatusValidos = new int[] { 1 };

                        Poco.Solicitudes.TB_SolicitudAmpliacionExpress solAmpliacion = con.Solicitudes_SolicitudAmpliacionExpress
                            .Where(sa => sa.IdSolicitudAmpliacion == peticion.IdSolicitud
                                && idEstatusValidos.Contains(sa.IdEstatus))
                            .OrderByDescending(o => o.FechaRegistro)
                            .FirstOrDefault();
                        if (solAmpliacion == null) throw new ValidacionExcepcion("La solicitud no cuenta con informaci�n de la ampliaci�n.");
                        datosAmpliacion.MontoLiquidacion = solAmpliacion.MontoLiquidacion;
                        datosAmpliacion.MontoCredito = solAmpliacion.MontoOtorgado;
                        datosAmpliacion.MontoDesembolso = solAmpliacion.MontoDesembolsar;
                        datosCredito = ObtenerCronograma(peticion.IdSolicitud, solAmpliacion.FechaAmpliacion);
                        datosAmpliacion.MontoPrestamoConAmpliacion = solAmpliacion.MontoOtorgado;
                        datosAmpliacion.TotalRecibos = solAmpliacion.PlazoOtorgado;
                        int idSolicitudAnterior = Convert.ToInt32(solAmpliacion.IdSolicitud);
                        var solicitudA = (from s in con.dbo_TB_Solicitudes
                                          join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                          where s.IdSolicitud == idSolicitudAnterior
                                          select new
                                          {
                                              s.articulos,
                                              s.capital,
                                              s.ImporteCredito
                                          }).FirstOrDefault();
                        if (solicitudA == null) throw new ValidacionExcepcion("No existe informaci�n sobre la cuenta anterior.");
                        datosAmpliacion.MontoAnterior = (solicitudA.ImporteCredito ?? 0) > 0 ? solicitudA.ImporteCredito ?? 0 : solicitudA.capital ?? 0;
                    }
                    else
                    {
                        datosCredito = ObtenerCronograma(peticion.IdSolicitud, null);
                    }

                    if (datosCredito == null || datosCredito.Recibos == null || datosCredito.Recibos.Count == 0)
                        throw new ValidacionExcepcion("No se encontr� informaci�n de la cuenta.");
                    datosAmpliacion.IdSolicitud = datosCredito.IdSolicitud;
                    datosAmpliacion.Nombres = datosCredito.NombreCliente;
                    datosAmpliacion.ApellidoPaterno = datosCredito.ApPaternoCliente;
                    datosAmpliacion.ApellidoMaterno = datosCredito.ApMaternoCliente;
                    datosAmpliacion.NumeroDocumento = datosCredito.DNICliente;
                    datosAmpliacion.TasaInteres = datosCredito.TasaAnual;
                    datosAmpliacion.TotalInteres = datosCredito.TotalInteres;
                    datosAmpliacion.TasaCostoEfectivoAnual = datosCredito.TasaCostoAnual;
                    datosAmpliacion.Erogacion = datosCredito.Cuota;
                    datosAmpliacion.CorreoElectronico = solicitud.Correo;

                    AmpliacionCreditoSimple ampliacionCto = new AmpliacionCreditoSimple(peticion.Version);
                    byte[] tAmortizacion = ampliacionCto.Generar(datosAmpliacion);

                    if (tAmortizacion == null || tAmortizacion.Length == 0)
                        throw new ValidacionExcepcion("Ocurri� un error dutante la generaci�n del documento Ampliacion de cr�dito simple.");

                    respuesta.Resultado.ContenidoArchivo = tAmortizacion;
                    respuesta.Resultado.TipoContenido = "application/pdf";
                    respuesta.Resultado.NombreArchivo = $"AmpliacionCredSimp_{peticion.IdSolicitud}";
                    respuesta.Resultado.Extension = ".pdf";
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"AmpliacionCredSimp_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el archivo el documento de Ampliaci�n de cr�dito simple.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionResumenCredito(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");

                CronogramaPagos datosCredito = ObtenerCronograma(peticion.IdSolicitud, null);
                if (datosCredito == null || datosCredito.Recibos == null || datosCredito.Recibos.Count == 0) throw new ValidacionExcepcion("No existe informaci�n de la cuenta.");
                if (peticion.Version == 0)
                {
                    TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.HojaResumen);
                    if (docD != null)
                    {
                        peticion.Version = docD.Version;
                    }
                }

                ResumenCredito resumenCredito = new ResumenCredito(peticion.Version);

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    // Validar si es Campa�a PF y obtener tipo de Credito
                    var solicitud = (from s in con.dbo_TB_Solicitudes
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         s.Origen_Id,
                                         s.idTipoCredito                                         
                                     }).FirstOrDefault();
                    if (solicitud == null) throw new ValidacionExcepcion("No existe informaci�n sobre la cuenta.");

                    resumenCredito.esCampaniaPF = (solicitud.Origen_Id == 2 || solicitud.Origen_Id == 5);
                    resumenCredito.esAmpliacion = (solicitud.idTipoCredito == 3);

                    // Si es una ampliacion, obtener el monto de desembolso                    
                    if (resumenCredito.esAmpliacion)
                    {
                        var solicitudReestructura = (from s in con.dbo_TB_Solicitudes_Reestructura
                                         where s.IdSolicitud == peticion.IdSolicitud
                                         select new
                                         {                                             
                                             MontoDesembolso = datosCredito.MontoDesembolsar - s.TotalLiquidar
                                         }).FirstOrDefault();

                        resumenCredito.MontoDesembolso = solicitudReestructura.MontoDesembolso ?? 0;
                    }
                }

                byte[] byteResumenCredito = resumenCredito.Generar(datosCredito);
                if (byteResumenCredito == null || byteResumenCredito.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del resumen de cr�dito.");

                respuesta.Resultado.ContenidoArchivo = byteResumenCredito;
                respuesta.Resultado.TipoContenido = "application/pdf";
                respuesta.Resultado.NombreArchivo = $"ResumenCredito_{peticion.IdSolicitud}";
                respuesta.Resultado.Extension = ".pdf";
                respuesta.Resultado.Firmas = resumenCredito.Firmas;
                respuesta.Resultado.DatosDocumento = resumenCredito.DatosDocumento;
                respuesta.TotalRegistros = 1;
#if DEBUG
                Utils.GuardarArchivo($"ResumenCredito_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el resumen del cr�dito.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionContrato(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var solicitud = (from s in con.dbo_TB_Solicitudes
                                     join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                     join su in con.dbo_TB_CATSucursal on s.IdSucursal equals su.IdSucursal
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         s.IdSolicitud,
                                         su.Sucursal,
                                         s.fch_Solicitud,
                                         c.Cliente
                                     }).FirstOrDefault();
                    if (solicitud == null) throw new ValidacionExcepcion("No fue posible encontrar los datos de la soliciutd.");
                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.Contrato);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }

                    Contrato contrato = new Contrato(peticion.Version);
                    ContratoVM datosContrato = new ContratoVM
                    {
                        Folio = Convert.ToInt32(solicitud.IdSolicitud),
                        Lugar = solicitud.Sucursal,
                        Fecha = solicitud.fch_Solicitud,
                        NombreCliente = solicitud.Cliente
                    };

                    byte[] bContrato = contrato.Generar(datosContrato);
                    if (bContrato == null || bContrato.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del contrato.");

                    respuesta.Resultado.ContenidoArchivo = bContrato;
                    respuesta.Resultado.TipoContenido = "application/pdf";
                    respuesta.Resultado.NombreArchivo = $"Contrato_{peticion.IdSolicitud}";
                    respuesta.Resultado.Extension = ".pdf";
                    respuesta.Resultado.Firmas = contrato.Firmas;
                    respuesta.Resultado.DatosDocumento = contrato.DatosDocumento;
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"Contrato_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el contrato.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionPagare(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var solicitud = (from s in con.dbo_TB_Solicitudes
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         s.IdSolicitud,
                                         s.ImporteCredito
                                     }).FirstOrDefault();
                    if (solicitud == null) throw new ValidacionExcepcion("No fue posible encontrar los datos de la soliciutd.");
                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.Pagare);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }
                    Pagare pagare = new Pagare(peticion.Version);
                    PagareVM datosPagare = new PagareVM
                    {
                        Folio = Convert.ToInt32(solicitud.IdSolicitud),
                        Importe = solicitud.ImporteCredito.Value
                    };

                    byte[] bPagare = pagare.Generar(datosPagare);
                    if (bPagare == null || bPagare.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del pagar�.");

                    respuesta.Resultado.ContenidoArchivo = bPagare;
                    respuesta.Resultado.TipoContenido = "application/pdf";
                    respuesta.Resultado.NombreArchivo = $"Pagare_{peticion.IdSolicitud}";
                    respuesta.Resultado.Extension = ".pdf";
                    respuesta.Resultado.Firmas = pagare.Firmas;
                    respuesta.Resultado.DatosDocumento = pagare.DatosDocumento;
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"Pagare_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el pagare.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionAcuerdoPagare(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");

                CronogramaPagos cronograma = ObtenerCronograma(peticion.IdSolicitud, null);
                if (cronograma == null) cronograma = new CronogramaPagos();
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    AcuerdoPagareVM datosAcuerdoPagare = (from s in con.dbo_TB_Solicitudes
                                                          join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                                          join es in con.Catalogo_TB_CATTipoVade on new { IdTipoVade = c.esta_civil ?? 0, Tipo = 1 } equals new { es.IdTipoVade, es.Tipo } into tEs
                                                          from les in tEs.DefaultIfEmpty()
                                                          where s.IdSolicitud == peticion.IdSolicitud
                                                          select new AcuerdoPagareVM
                                                          {
                                                              Cliente = c.Cliente,
                                                              IdSolicitud = (int)s.IdSolicitud,
                                                              DNI = c.NumeroDocumento,
                                                              EstadoCivil = les != null ? les.TipoVade : string.Empty
                                                          }).FirstOrDefault();
                    if (datosAcuerdoPagare == null) datosAcuerdoPagare = new AcuerdoPagareVM();
                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.AcuerdoLlenadoPagare);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }

                    AcuerdoPagare acuerdo = new AcuerdoPagare(peticion.Version);
                    datosAcuerdoPagare.Domicilio = cronograma.DireccionCliente;
                    byte[] bAcuerdoPagare = acuerdo.Generar(datosAcuerdoPagare);
                    if (bAcuerdoPagare == null || bAcuerdoPagare.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del acuerdo de pagar�.");

                    respuesta.Resultado.ContenidoArchivo = bAcuerdoPagare;
                    respuesta.Resultado.TipoContenido = "application/pdf";
                    respuesta.Resultado.NombreArchivo = $"AcuerdoPagare_{peticion.IdSolicitud}";
                    respuesta.Resultado.Extension = ".pdf";
                    respuesta.Resultado.Firmas = acuerdo.Firmas;
                    respuesta.Resultado.DatosDocumento = acuerdo.DatosDocumento;
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"AcuerdoPagare_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el acuerdo de pagar�.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionAcuerdoDomiciliacionContinental(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");
                CronogramaPagos crono = ObtenerCronograma(peticion.IdSolicitud, null);
                if (crono == null || crono.Recibos == null || crono.Recibos.Count == 0) throw new ValidacionExcepcion("No existe informaci�n de la cuenta.");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var solicitud = (from s in con.dbo_TB_Solicitudes
                                     join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         s.fch_Solicitud,
                                         c.Celular,
                                         c.compania,
                                         s.BCEntidad,
                                         s.BCOficina,
                                         s.BCCuenta,
                                         s.BCDC
                                     }).FirstOrDefault();

                    if (solicitud == null) throw new ValidacionExcepcion("No fue posible encontrar los datos de la soliciutd.");

                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.AcuerdoDomiciliacion);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }

                    AcuerdoDomiciliacionContinental acuerdo = new AcuerdoDomiciliacionContinental(peticion.Version);
                    int totalRecibos = crono.Recibos.Count;
                    AcuerdoDomiciliacionClienteVM datosAcuerdoDomiciliacionCliente = new AcuerdoDomiciliacionClienteVM()
                    {
                        FechaSolicitud = solicitud.fch_Solicitud,
                        NombreCliente = crono.NombreCliente,
                        ApPaternoCliente = crono.ApPaternoCliente,
                        ApMaternoCliente = crono.ApMaternoCliente,
                        DireccionCliente = crono.DireccionCliente,
                        DNICliente = crono.DNICliente,
                        CelularCliente = solicitud.Celular,
                        EmpresaCliente = solicitud.compania,
                        IdSolicitud = crono.IdSolicitud,
                        CantidadRecibos = totalRecibos,
                        ImporteMaximoCuota = crono.Cuota,
                        BCEntidad = solicitud.BCEntidad,
                        BCOficina = solicitud.BCOficina,
                        BCCuenta = solicitud.BCCuenta,
                        BCDC = solicitud.BCDC
                    };

                    byte[] bAcuerdoDomiciliacionCliente = acuerdo.Generar(datosAcuerdoDomiciliacionCliente);
                    if (bAcuerdoDomiciliacionCliente == null || bAcuerdoDomiciliacionCliente.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del acuerdo de domiciliaci�n cliente.");

                    respuesta.Resultado.ContenidoArchivo = bAcuerdoDomiciliacionCliente;
                    respuesta.Resultado.TipoContenido = "application/pdf";
                    respuesta.Resultado.NombreArchivo = $"AcuerdoDomiciliacionClienteCont_{peticion.IdSolicitud}";
                    respuesta.Resultado.Extension = ".pdf";
                    respuesta.Resultado.Firmas = acuerdo.Firmas;
                    respuesta.Resultado.DatosDocumento = acuerdo.DatosDocumento;
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"AcuerdoDomiciliacionClienteCont_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el acuerdo de domiciliaci�n.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionAcuerdoDomiciliacionVISA(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");
                CronogramaPagos crono = ObtenerCronograma(peticion.IdSolicitud, null);
                if (crono == null || crono.Recibos == null || crono.Recibos.Count == 0) throw new ValidacionExcepcion("No existe informaci�n de la cuenta.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var solicitud = (from s in con.dbo_TB_Solicitudes
                                     join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         s.fch_Solicitud,
                                         c.telefono,
                                         c.Celular,
                                         c.Correo,
                                         s.NumeroTarjetaVisa,
                                         s.MesExpiraTarjetaVisa,
                                         s.AnioExpiraTarjetaVisa
                                     }).FirstOrDefault();

                    if (solicitud == null) throw new ValidacionExcepcion("No fue posible encontrar los datos de la soliciutd.");

                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.FormatoVisa);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }

                    AcuerdoDomiciliacionVISA acuerdo = new AcuerdoDomiciliacionVISA(peticion.Version);

                    AcuerdoDomiciliacionVISAVM datosAcuerdoDomiciliacionVISA = new AcuerdoDomiciliacionVISAVM()
                    {
                        FechaSolicitud = solicitud.fch_Solicitud,
                        NombreCliente = crono.NombreCliente,
                        ApPaternoCliente = crono.ApPaternoCliente,
                        ApMaternoCliente = crono.ApMaternoCliente,
                        DNICliente = crono.DNICliente,
                        TelefonoCliente = solicitud.telefono,
                        CelularCliente = solicitud.Celular,
                        CorreoCliente = solicitud.Correo,
                        IdSolicitud = crono.IdSolicitud.ToString(),
                        NumeroTarjetaVisa = solicitud.NumeroTarjetaVisa,
                        MesExpiraTarjetaVisa = solicitud.MesExpiraTarjetaVisa != null ? solicitud.MesExpiraTarjetaVisa.Value.ToString("D2") : "",
                        AnioExpiraTarjetaVisa = solicitud.AnioExpiraTarjetaVisa != null ? solicitud.AnioExpiraTarjetaVisa.Value.ToString() : ""
                    };

                    byte[] bAcuerdoDomiciliacionVISA = acuerdo.Generar(datosAcuerdoDomiciliacionVISA);
                    if (bAcuerdoDomiciliacionVISA == null || bAcuerdoDomiciliacionVISA.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del acuerdo de domiciliaci�n VISA.");

                    respuesta.Resultado.ContenidoArchivo = bAcuerdoDomiciliacionVISA;
                    respuesta.Resultado.TipoContenido = "application/pdf";
                    respuesta.Resultado.NombreArchivo = $"AcuerdoDomiciliacionVISA_{peticion.IdSolicitud}";
                    respuesta.Resultado.Extension = ".pdf";
                    respuesta.Resultado.Firmas = acuerdo.Firmas;
                    respuesta.Resultado.DatosDocumento = acuerdo.DatosDocumento;
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"AcuerdoDomiciliacionVISA_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el acuerdo de domiciliaci�n VISA.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionAcuerdoDomiciliacionInterbank(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");
                CronogramaPagos cronograma = ObtenerCronograma(peticion.IdSolicitud, null);
                if (cronograma == null) cronograma = new CronogramaPagos { Recibos = new List<CronogramaPagos.Recibo>() };

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var solicitud = (from s in con.dbo_TB_Solicitudes
                                     join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         s.fch_Solicitud,
                                         c.Celular,
                                         c.compania,
                                         s.CuentaDesembolso
                                     }).FirstOrDefault();
                    if (solicitud == null) throw new ValidacionExcepcion("No fue posible encontrar los datos de la soliciutd.");

                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.AcuerdoDomiciliacion);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }
                    AcuerdoDomiciliacionInterbank acuerdo = new AcuerdoDomiciliacionInterbank(peticion.Version);

                    int totalRecibos = cronograma.Recibos.Count;
                    AcuerdoDomiciliacionClienteVM datosAcuerdoDomiciliacionCliente = new AcuerdoDomiciliacionClienteVM()
                    {
                        FechaSolicitud = solicitud.fch_Solicitud,
                        NombreCliente = cronograma.NombreCliente,
                        ApPaternoCliente = cronograma.ApPaternoCliente,
                        ApMaternoCliente = cronograma.ApMaternoCliente,
                        DireccionCliente = cronograma.DireccionCliente,
                        DNICliente = cronograma.DNICliente,
                        CelularCliente = solicitud.Celular,
                        EmpresaCliente = solicitud.compania,
                        IdSolicitud = cronograma.IdSolicitud,
                        CantidadRecibos = totalRecibos,
                        ImporteMaximoCuota = cronograma.Cuota,
                        CuentaDesembolso = solicitud.CuentaDesembolso
                    };

                    byte[] bAcuerdoDomiciliacionCliente = acuerdo.Generar(datosAcuerdoDomiciliacionCliente);
                    if (bAcuerdoDomiciliacionCliente == null || bAcuerdoDomiciliacionCliente.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del acuerdo de domiciliaci�n cliente.");

                    respuesta.Resultado.ContenidoArchivo = bAcuerdoDomiciliacionCliente;
                    respuesta.Resultado.TipoContenido = "application/pdf";
                    respuesta.Resultado.NombreArchivo = $"AcuerdoDomiciliacionClienteInt_{peticion.IdSolicitud}";
                    respuesta.Resultado.Extension = ".pdf";
                    respuesta.Resultado.Firmas = acuerdo.Firmas;
                    respuesta.Resultado.DatosDocumento = acuerdo.DatosDocumento;
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"AcuerdoDomiciliacionClienteInt_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el acuerdo de domiciliaci�n.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionReestructura(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    decimal nuevaCuota = decimal.Zero;
                    //IdSolicitud de la nueva solicitud generada
                    decimal idSol = Convert.ToDecimal(peticion.IdSolicitud);
                    int[] estatusPetResImpresion = { 5 };
                    var solicitud = (from p in con.Solicitudes_TB_PeticionReestructuras
                                     join tr in con.Solicitudes_TB_CATTipoReestructuras on p.TipoReestructura_Id equals tr.Id_TipoReestructura
                                     join f in con.Solicitudes_TB_FotoPeticionReestructuras on p.Id_PeticionReestructura equals f.PeticionReestructura_Id
                                     join s in con.dbo_TB_Solicitudes on p.Solicitud_Id equals s.IdSolicitud
                                     join sn in con.dbo_TB_Solicitudes on p.IdSolicitudNuevo equals sn.IdSolicitud
                                     join t in con.dbo_TB_CATTipoCredito_PE on s.IDCredito_PE equals t.IDCredito
                                     where tr.TipoReestructura == "Reestructura" && p.Actual && p.IdSolicitudNuevo == idSol && estatusPetResImpresion.Contains(p.EstatusPeticionReestructuras_Id)
                                     select new
                                     {
                                         NombreCliente = f.Cliente,
                                         TipoCredito = t.Credito,
                                         IdSolicitud = p.Solicitud_Id,
                                         ImporteCredito = s.capital.HasValue ? s.capital : 0,
                                         PlazoActual = s.Plazo.HasValue ? s.Plazo : 0,
                                         Condonado = (con.Solicitudes_TB_FotoReciboPetRes.Where(r => r.FotoPeticionReestructura_Id == f.Id_FotoPeticionReestructura && r.FotoTipoReciboPetRes_Id == 2).GroupBy(s => 1).Select(s => new
                                         {
                                             Total = s.Sum(x => x.Liquida_Capital) + s.Sum(x => x.Liquida_Interes) + s.Sum(x => x.Liquida_IGV) + s.Sum(x => x.Liquida_Seguro) + s.Sum(x => x.Liquida_GAT)
                                         }).FirstOrDefault().Total),
                                         Vencidas = con.Solicitudes_TB_FotoReciboPetRes.Where(r => r.FotoPeticionReestructura_Id == f.Id_FotoPeticionReestructura && r.FotoTipoReciboPetRes_Id == 1 && r.TipoRecibo == "VENCIDO").Count(),
                                         NuevoPlazo = p.Pagos,
                                         p.TipoReestructura_Id,
                                         p.FechaRegistro,
                                         TotalLiquidacionRestructura = p.TotalLiquidacionRestructura.HasValue ? p.TotalLiquidacionRestructura :
                                             f.TotalLiquidacion - (con.Solicitudes_TB_FotoReciboPetRes.Where(r => r.FotoPeticionReestructura_Id == f.Id_FotoPeticionReestructura && r.FotoTipoReciboPetRes_Id == 2).GroupBy(s => 1).Select(s => new
                                             {
                                                 Total = s.Sum(x => x.Liquida_Capital) + s.Sum(x => x.Liquida_Interes) + s.Sum(x => x.Liquida_IGV) + s.Sum(x => x.Liquida_Seguro) + s.Sum(x => x.Liquida_GAT)
                                             }).FirstOrDefault().Total),
                                         NuevaCuota = sn.erogacion.HasValue ? s.erogacion : (sn.MontoCuota.HasValue ? sn.MontoCuota : 0)
                                     }).FirstOrDefault();

                    if (solicitud == null)
                        throw new ValidacionExcepcion("No se encontr� informaci�n de reestructura.");

                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.Reestructura);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }
                    Reestructura reestructura = new Reestructura(peticion.Version);
                    ReestructuraVM datosReestructura = new ReestructuraVM
                    {
                        NombreCliente = solicitud.NombreCliente,
                        TipoCredito = solicitud.TipoCredito,
                        IdSolicitud = Convert.ToInt32(solicitud.IdSolicitud),
                        ImporteCredito = solicitud.ImporteCredito.Value,
                        PlazoActual = solicitud.PlazoActual.Value,
                        Condonado = solicitud.Condonado,
                        Vencidas = solicitud.Vencidas,
                        NuevoPlazo = solicitud.NuevoPlazo,
                        NuevaCuota = solicitud.NuevaCuota.Value
                    };

                    if (solicitud.NuevaCuota.HasValue && solicitud.NuevaCuota.Value <= 0)
                    {
                        Resultado<ObtenerSimulacionCreditoResponse> res = TBCreditosBll.ObtenerSimulacionCredito(
                            new ObtenerSimulacionCreditoRequest()
                            {
                                IdSolicitud = Convert.ToInt32(solicitud.IdSolicitud),
                                IdTipoReestructura = solicitud.TipoReestructura_Id,
                                Importe = solicitud.TotalLiquidacionRestructura.HasValue ? solicitud.TotalLiquidacionRestructura.Value : 0,
                                Plazo = solicitud.NuevoPlazo,
                                FechaDesembolso = solicitud.FechaRegistro.ToString("yyyyMMdd")
                            });
                        if (res.Codigo > 0)
                        {
                            throw new ValidacionExcepcion(res.Mensaje);
                        }
                        if (res != null && res.ResultObject != null && res.ResultObject.Resultado != null
                            && res.ResultObject.Resultado.Count > 0)
                        {
                            nuevaCuota = res.ResultObject.Resultado.FirstOrDefault().Cuota;
                        }
                        if (solicitud == null || nuevaCuota <= 0)
                        {
                            throw new ValidacionExcepcion("Ocurri� un error al obtener la informaci�n de la petici�n de reestructura.");
                        }
                        else
                        {
                            datosReestructura.NuevaCuota = nuevaCuota;
                        }
                    }

                    byte[] breestructura = reestructura.Generar(datosReestructura);
                    if (breestructura == null || breestructura.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del documento de reestructura.");

                    respuesta.Resultado.ContenidoArchivo = breestructura;
                    respuesta.Resultado.TipoContenido = "application/pdf";
                    respuesta.Resultado.NombreArchivo = $"Reestructura_{peticion.IdSolicitud}";
                    respuesta.Resultado.Extension = ".pdf";
                    respuesta.Resultado.Firmas = reestructura.Firmas;
                    respuesta.Resultado.DatosDocumento = reestructura.DatosDocumento;
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"Reestructura_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el documento de reestructura.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionTransaccionExtrajudicial(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    decimal idSol = Convert.ToDecimal(peticion.IdSolicitud);
                    int[] estatusPetResImpresion = { 5 };
                    var solicitud = (from p in con.Solicitudes_TB_PeticionReestructuras
                                     join tr in con.Solicitudes_TB_CATTipoReestructuras on p.TipoReestructura_Id equals tr.Id_TipoReestructura
                                     join s in con.dbo_TB_Solicitudes on p.Solicitud_Id equals s.IdSolicitud
                                     join sn in con.dbo_TB_Solicitudes on p.IdSolicitudNuevo equals sn.IdSolicitud
                                     join cr in con.dbo_TB_Creditos on s.IdSolicitud equals cr.IdSolicitud
                                     join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                     join co in con.dbo_TB_CATColonia on c.IdColonia.Value equals co.IdColonia
                                     where tr.TipoReestructura == "Extrajudicial" && p.IdSolicitudNuevo == idSol && p.Actual && estatusPetResImpresion.Contains(p.EstatusPeticionReestructuras_Id)
                                     select new
                                     {
                                         NombreCliente = c.Cliente,
                                         DNI = c.NumeroDocumento,
                                         Calle = c.direccion,
                                         Distrito = co.Colonia,
                                         FechaCredito = cr.fch_Credito,
                                         ImporteCredito = s.capital.HasValue ? s.capital : 0,
                                         PlazoActual = s.Plazo.HasValue ? s.Plazo : 0,
                                         CuotaActual = s.erogacion.HasValue ? s.erogacion.Value : 0,
                                         IdSolicitudAnterior = p.Solicitud_Id
                                     }).FirstOrDefault();
                    if (solicitud == null)
                        throw new ValidacionExcepcion("No se encontr� informaci�n de transaccion.");
                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.ReestructuraExtrajudicial);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }
                    TransaccionExtrajudicial transaccion = new TransaccionExtrajudicial(peticion.Version);

                    var recibo = (from c in con.dbo_TB_Creditos
                                  join r in con.dbo_TB_Recibos on c.IdCuenta equals r.IdCuenta
                                  where c.IdSolicitud == solicitud.IdSolicitudAnterior
                                  orderby r.fch_recibo.Value
                                  select new
                                  {
                                      FechaPrimerRecibo = r.fch_recibo
                                  }).FirstOrDefault();
                    if (recibo == null)
                        throw new ValidacionExcepcion("No se encontr� informaci�n de recibo.");

                    TransaccionExtrajudicialVM datosTransaccion = new TransaccionExtrajudicialVM
                    {
                        NombreCliente = solicitud.NombreCliente,
                        DNI = solicitud.DNI,
                        Calle = solicitud.Calle,
                        Distrito = solicitud.Distrito,
                        FechaCredito = solicitud.FechaCredito,
                        ImporteCredito = solicitud.ImporteCredito.Value,
                        PlazoActual = solicitud.PlazoActual.Value,
                        FechaPrimerRecibo = recibo.FechaPrimerRecibo.Value,
                        CuotaActual = solicitud.CuotaActual
                    };

                    byte[] bTran = transaccion.Generar(datosTransaccion);
                    if (bTran == null || bTran.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del documento de transacci�n.");

                    ImpresionDocumentoResponse impresionTabAmorti = ImpresionTablaAmortizacion(new ImpresionTablaAmortizacionRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        IdSolicitud = peticion.IdSolicitud,
                        Version = 0,
                        EsSimulado = true
                    });

                    if (impresionTabAmorti == null || impresionTabAmorti.Error || impresionTabAmorti.Resultado.ContenidoArchivo.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del nuevo cronograma de transacci�n.");
                    List<byte[]> archivos = new List<byte[]>();
                    archivos.Add(bTran);
                    archivos.Add(impresionTabAmorti.Resultado.ContenidoArchivo);

                    if (archivos.Count > 0)
                    {
                        respuesta.Resultado.ContenidoArchivo = Utils.CombinarPdfs(archivos);
                        respuesta.TotalRegistros = 1;
                    }

                    if (transaccion.Firmas != null && impresionTabAmorti.Resultado.Firmas != null)
                    {
                        int paginaMax = transaccion.Firmas.Max(p => p.Pagina);
                        if (paginaMax > 0)
                        {
                            foreach (var f in impresionTabAmorti.Resultado.Firmas)
                            {
                                f.Pagina += paginaMax;
                            }
                            transaccion.Firmas.AddRange(impresionTabAmorti.Resultado.Firmas);
                        }
                    }

                    //respuesta.Resultado.ContenidoArchivo = bTran;
                    respuesta.Resultado.TipoContenido = "application/pdf";
                    respuesta.Resultado.NombreArchivo = $"TransaccionExtrajudicial_{peticion.IdSolicitud}";
                    respuesta.Resultado.Extension = ".pdf";
                    respuesta.Resultado.Firmas = transaccion.Firmas;
                    respuesta.Resultado.DatosDocumento = transaccion.DatosDocumento + impresionTabAmorti.Resultado.DatosDocumento;
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"TranExtra_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el documento de transaccion.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionSolicitud(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La peticion es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El numero de cuenta no es correcto.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    decimal idSol = Convert.ToDecimal(peticion.IdSolicitud);
                    var Csolicitud = (from s in con.dbo_TB_Solicitudes
                                      join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                      join p in con.dbo_TB_CATPromotor on s.IdPromotor equals p.IdPromotor
                                      join sx in con.Catalogo_TB_CATTipoVade on new { IdTipoVade = c.sexo ?? 0, Tipo = 2 } equals new { sx.IdTipoVade, sx.Tipo } into tsx
                                      from lsx in tsx.DefaultIfEmpty()
                                      join e in con.dbo_TB_CATEstado on c.IdC_Nacimiento equals e.IdEstado into se
                                      from le in se.DefaultIfEmpty()
                                      join pa in con.dbo_SI_Pais on c.idPaisNacimiento equals pa.Clave into spa
                                      from lpa in spa.DefaultIfEmpty()
                                      join ec in con.Catalogo_TB_CATEmpresaConfiguracion on c.IdConfiguracionEmpresa equals ec.IdConfiguracion into sec
                                      from lec in sec.DefaultIfEmpty()
                                      join ecsl in con.dbo_TB_CATSituacionLaboral_PE on (lec != null ? new { IDSituacion = lec.IdSituacionLaboral } : new { IDSituacion = 0 }) equals new { ecsl.IDSituacion } into secsl
                                      from lecsl in secsl.DefaultIfEmpty()
                                      join ecrp in con.Catalogo_TipoPension on (lec != null && lec.IdRegimenPension.HasValue ? new { idTipoPension = lec.IdRegimenPension.Value } : new { idTipoPension = 0 }) equals new { ecrp.idTipoPension } into secrp
                                      from lecrp in secrp.DefaultIfEmpty()
                                      join td in con.dbo_TB_CATTipoDocumento_PE on c.TipoDocumento equals td.IDTipoDocumento into std
                                      from ltd in std.DefaultIfEmpty()
                                      join tdi in con.dbo_TB_CATDireccion_PE on c.TipoDireccion equals tdi.IDDireccion into stdi
                                      from ltdi in stdi.DefaultIfEmpty()
                                      join tz in con.dbo_TB_CATTipoZona_PE on c.TipoZona equals tz.IDTipoZona into stz
                                      from ltz in stz.DefaultIfEmpty()
                                      join co in con.dbo_TB_CATColonia on c.IdColonia.Value equals co.IdColonia
                                      join ci in con.dbo_TB_CATCiudad on co.IdCiudad equals ci.IdCiudad
                                      //join co in con.dbo_TB_CATColonia on c.IdColonia.Value equals co.IdColonia
                                      //join es in con.Catalogo_TB_CATTipoVade on new { IdTipoVade = c.esta_civil ?? 0, Tipo = 1 } equals new { es.IdTipoVade, es.Tipo } into tEs
                                      //from les in tEs.DefaultIfEmpty()
                                      where s.IdSolicitud == idSol
                                      select new
                                      {
                                          s.IdSolicitud,
                                          Vendedor = p.Promotor,
                                          NombresCliente = c.Nombres,
                                          ApPaternoCliente = c.apellidop,
                                          ApMaternoCliente = c.apellidom,
                                          Sexo = lsx != null ? lsx.TipoVade : "NA",
                                          FechaNacimiento = c.fch_Nacimiento.Value,
                                          LugarNacimiento = le != null ? le.Estado : "Sin Informacion",
                                          PaisNacimiento = lpa != null ? lpa.Pais : "Sin Informacion",
                                          Ocupacion = c.ocupacion,
                                          ECSituacionLaboral = lecsl != null ? lecsl.Situacion : "Sin Informacion",
                                          ECRegimenPension = lecrp != null ? lecrp.Descripcion : "Sin Informacion",
                                          TipoDocumento = ltd != null ? ltd.TipoDocumento : "Sin Informacion",
                                          c.NumeroDocumento,
                                          c.Correo,
                                          TipoDireccion = ltdi != null ? ltdi.Direccion : "Sin Informacion",
                                          c.NombreVia,
                                          c.NumExterior,
                                          c.NumInterior,
                                          c.manzana,
                                          c.lote,
                                          TipoZona = ltz != null ? ltz.TipoZona : "Sin Informacion",
                                          c.NombreZona,
                                          Distrito = co.Colonia,
                                          Departamento = "",
                                          Provincia = "",
                                          ImporteCredito = s.capital.HasValue ? s.capital : 0,
                                          Plazo = s.Plazo.HasValue ? s.Plazo : 0,
                                          FechaSolicitud = s.fch_Solicitud
                                      }).FirstOrDefault();

                    if (Csolicitud == null)
                        throw new ValidacionExcepcion("No se encontro informacion de solicitud.");
                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.Solicitud);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }

                    SolicitudFormatoVM datosSolicitud = new SolicitudFormatoVM
                    {
                        IdSolicitud = Convert.ToInt32(Csolicitud.IdSolicitud),
                        Vendedor = Csolicitud.Vendedor,
                        NombresCliente = Csolicitud.NombresCliente,
                        ApellidoPaternoCliente = Csolicitud.ApPaternoCliente,
                        ApellidoMaternoCliente = Csolicitud.ApMaternoCliente,
                        Sexo = Csolicitud.Sexo,
                        FechaNacimiento = Csolicitud.FechaNacimiento,
                        LugarNacimiento = Csolicitud.LugarNacimiento,
                        PaisNacimiento = Csolicitud.PaisNacimiento,
                        Ocupacion = Csolicitud.Ocupacion,
                        SituacionLaboral = Csolicitud.ECSituacionLaboral,
                        RegimenPension = Csolicitud.ECRegimenPension,
                        TipoDocumento = Csolicitud.TipoDocumento,
                        NumeroDocumento = Csolicitud.NumeroDocumento,
                        Correo = Csolicitud.Correo,
                        TipoDireccion = Csolicitud.TipoDireccion,
                        NombreVia = Csolicitud.NombreVia,
                        NumeroExterior = Csolicitud.NumExterior,
                        NumeroInterior = Csolicitud.NumInterior,
                        Manzana = Csolicitud.manzana,
                        Lote = Csolicitud.lote,
                        TipoZona = Csolicitud.TipoZona,
                        NombreZona = Csolicitud.NombreZona,
                        Departamento = Csolicitud.Departamento,
                        Provincia = Csolicitud.Provincia,
                        Distrito = Csolicitud.Distrito,

                        ImporteCredito = Csolicitud.ImporteCredito.Value,
                        Plazo = Csolicitud.Plazo.Value,
                        FechaSolicitud = Csolicitud.FechaSolicitud
                    };

                    Solicitud solicitud = new Solicitud(peticion.Version);
                    byte[] bsolicitud = solicitud.Generar(datosSolicitud);
                    if (bsolicitud == null || bsolicitud.Length == 0) throw new ValidacionExcepcion("Ocurrio un error durante la generacion del documento de solicitud.");

                    respuesta.Resultado.ContenidoArchivo = bsolicitud;
                    respuesta.Resultado.TipoContenido = "application/pdf";
                    respuesta.Resultado.NombreArchivo = $"Solicitud_{peticion.IdSolicitud}";
                    respuesta.Resultado.Extension = ".pdf";
                    respuesta.Resultado.Firmas = solicitud.Firmas;
                    respuesta.Resultado.DatosDocumento = solicitud.DatosDocumento;
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"Solicitud_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrio un error al momento de generar el documento de solicitud.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Metodo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionCartaPoder(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var solicitud = (from s in con.dbo_TB_Solicitudes
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         s.IdSolicitud,
                                         s.fch_Solicitud,
                                         s.CuentaDesembolso,
                                         s.CuentaCCI,
                                         s.NumeroTarjetaVisa
                                     }).FirstOrDefault();
                    BuscarClienteResponse datosCliente = TBClientesBll.BuscarCliente(new Model.Request.Cliente.BuscarClienteRequest { IdSolicitud = peticion.IdSolicitud, IdUsuario = peticion.IdUsuario });
                    if (solicitud == null || datosCliente == null || datosCliente.Cliente == null) throw new ValidacionExcepcion("No fue posible encontrar los datos de la soliciutd.");
                    DatosClienteVM cliente = datosCliente.Cliente;
                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.CartaPoder);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }
                    CartaPoder contrato = new CartaPoder(peticion.Version);

                    CartaPoderVM datosCartaPoder = new CartaPoderVM()
                    {
                        NombreCliente = $"{cliente.Nombres} {cliente.Apellidos}",
                        DNI = cliente.Dni,
                        FechaSolicitud = solicitud?.fch_Solicitud ?? new DateTime(),
                        Direccion = cliente.Direccion ?? "".ToUpper(),
                        NoCuentaDom = solicitud == null ? "" :
                            !string.IsNullOrWhiteSpace(solicitud.CuentaDesembolso) ? solicitud.CuentaDesembolso : solicitud.CuentaCCI
                    };

                    byte[] bCartaP = contrato.Generar(datosCartaPoder);
                    if (bCartaP == null || bCartaP.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n de la Carta Poder.");

                    respuesta.Resultado.ContenidoArchivo = bCartaP;
                    respuesta.Resultado.TipoContenido = "application/pdf";
                    respuesta.Resultado.NombreArchivo = $"CartaPoder_{peticion.IdSolicitud}";
                    respuesta.Resultado.Extension = ".pdf";
                    respuesta.Resultado.Firmas = contrato.Firmas;
                    respuesta.Resultado.DatosDocumento = contrato.DatosDocumento;
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"CartaPoder_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar la Carta Poder.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionDeclaracionJurada(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var solicitud = (from s in con.dbo_TB_Solicitudes
                                     join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                     join co in con.dbo_TB_CATColonia on c.IdColonia ?? 0 equals co.IdColonia into cco
                                     from lco in cco.DefaultIfEmpty()
                                     join cd in con.Clientes_TB_ClienteDirecciones on s.IdSolicitud equals cd.idSolicitud ?? 0 into scd
                                     from lcd in scd.DefaultIfEmpty()
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         s.IdSolicitud,
                                         s.fch_Solicitud,
                                         c.Cliente,
                                         c.NumeroDocumento,
                                         c.direccion,
                                         SolicitudDireccion = lcd == null ? "" : lcd.nombreVia,
                                         Distrito = lco == null ? "" : lco.Colonia,
                                         s.CuentaDesembolso,
                                         s.CuentaCCI,
                                         s.NumeroTarjetaVisa
                                     }).FirstOrDefault();
                    if (solicitud == null) throw new ValidacionExcepcion("No fue posible encontrar los datos de la soliciutd.");
                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.DeclaracionJurada);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }

                    DeclaracionJurada declaracion = new DeclaracionJurada(peticion.Version);
                    DeclaracionJuradaVM datosDeclaracionJ = new DeclaracionJuradaVM()
                    {
                        NombreCliente = solicitud?.Cliente,
                        DNI = solicitud?.NumeroDocumento,
                        IdSolicitud = (int)solicitud?.IdSolicitud
                    };

                    byte[] bCartaP = declaracion.Generar(datosDeclaracionJ);
                    if (bCartaP == null || bCartaP.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del documento.");

                    respuesta.Resultado.ContenidoArchivo = bCartaP;
                    respuesta.Resultado.TipoContenido = "application/pdf";
                    respuesta.Resultado.NombreArchivo = $"DeclaracionJurada_{peticion.IdSolicitud}";
                    respuesta.Resultado.Extension = ".pdf";
                    respuesta.Resultado.Firmas = declaracion.Firmas;
                    respuesta.Resultado.DatosDocumento = declaracion.DatosDocumento;
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"DeclaracionJurada_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el documento.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionEstadoCuenta(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");

                string dirEC = AppSettings.DirDocumentoECTemp;

                if (peticion.RegresaUrl)
                {
                    if (!Directory.Exists(dirEC)) { throw new ValidacionExcepcion("No fue posible realizar la impresi�n porque no se encontr� la ruta de guardado para el archivo. Favor de contactar a soporte t�cnico."); }

                    DirectoryInfo d = new DirectoryInfo(dirEC);

                    int limitDays = 0;
                    List<SOPF.Core.Poco.dbo.TB_CATParametro> parametros = ParametrosBLL.Parametro("DEC");
                    if (parametros != null && parametros.Count > 0)
                        limitDays = Convert.ToInt32(parametros[0].valor); //5 default
                    else
                        limitDays = 5;
                    DateTime pasada = DateTime.Now.AddDays(-limitDays);
                    DateTime limite = new DateTime(pasada.Year, pasada.Month, pasada.Day, 23, 59, 59);
                    FileInfo[] files = d.GetFiles("*.pdf").Where(f => f.CreationTime <= limite).ToArray();
                    
                    foreach (FileInfo fil in files)
                    {
                        fil.Delete();
                    }
                }

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var solicitud = (from s in con.dbo_TB_Solicitudes
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         s.IdSolicitud
                                     }).FirstOrDefault();
                    if (solicitud == null) throw new ValidacionExcepcion("No fue posible encontrar los datos de la soliciutd.");
                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.EstadoCuenta);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }
                    EstadoCuenta estadoCuenta = new EstadoCuenta(peticion.Version);

                    BalanceCuenta eCuenta = new BalanceCuenta() { IdSolicitud = peticion.IdSolicitud };
                    eCuenta = PagosBL.Balance_Obtener(eCuenta);

                    List<BalanceCuenta.Recibos> eRecibos = PagosBL.Balance_Obtener_Recibos(eCuenta).ToList();

                    EstadoCuentaVM datosEstadoCuenta = new EstadoCuentaVM
                    {
                        EstatusCredito = eCuenta.EstatusCredito.EstatusDesc,
                        IdSolicitud = peticion.IdSolicitud,
                        Cliente = string.Format("{0} {1} {2}", eCuenta.ClienteNombre, eCuenta.ClienteApPaterno, eCuenta.ClienteApMaterno),
                        FechaCredito = eCuenta.FechaCredito.Value.GetValueOrDefault().ToString("dd/MM/yyyy"),
                        MontoUsoCanal = eCuenta.MontoUsoCanal,
                        MontoGAT = eCuenta.MontoGAT,
                        Cuota = eCuenta.MontoCuota,
                        Capital = eCuenta.Capital,
                        Producto = eCuenta.ProductoDesc,
                        SaldoVencido = eCuenta.SaldoVencido,
                        CostoTotalCredito = eCuenta.CostoTotalCredito,
                        TipoCredito = eCuenta.TipoCreditoDesc.ToUpper(),
                        MontoComisionUsoCanal = eCuenta.ComisionOrigen,
                        Recibos = eRecibos
                    };

                    byte[] bEstadoCuenta = estadoCuenta.Generar(datosEstadoCuenta);
                    if (bEstadoCuenta == null || bEstadoCuenta.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del estado de cuenta.");

                    if(peticion.RegresaUrl)
                    {
                        try
                        {
                            string extension = ".pdf";
                            Utils.GuardarArchivo($"EstadoCuenta_{peticion.IdSolicitud}{extension}", bEstadoCuenta, dirEC);
                        }
                        catch
                        {
                            throw new ValidacionExcepcion("El archivo no es v�lido, favor de verificarlo.");
                        }
                        respuesta.Resultado.ContenidoArchivo = null;
                        respuesta.Resultado.TipoContenido = "application/pdf";
                        respuesta.Resultado.NombreArchivo = $"EstadoCuenta_{peticion.IdSolicitud}.pdf";
                        respuesta.Resultado.Extension = ".pdf";
                        respuesta.TotalRegistros = 1;                        
                        respuesta.Resultado.Url = System.ServiceModel.OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.OriginalString.Replace(System.ServiceModel.OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.PathAndQuery, "")
                                                + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings.Get("DirDocumentoECTemp")
                                                + "/" + respuesta.Resultado.NombreArchivo;
                    }
                    else
                    {
                        respuesta.Resultado.ContenidoArchivo = bEstadoCuenta;
                        respuesta.Resultado.TipoContenido = "application/pdf";
                        respuesta.Resultado.NombreArchivo = $"EstadoCuenta_{peticion.IdSolicitud}";
                        respuesta.Resultado.Extension = ".pdf";
                        respuesta.TotalRegistros = 1;
#if DEBUG
                        Utils.GuardarArchivo($"EstadoCuenta_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                    }

                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el estado de cuenta.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionDocumentoResponse ImpresionSolicitudRPT(ImpresionDocumentoRequest peticion)
        {
            ImpresionDocumentoResponse respuesta = new ImpresionDocumentoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petici�n es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El n�mero de cuenta no es correcto.");

                string dirEC = AppSettings.DirDocumentoECTemp;

                if (peticion.RegresaUrl)
                {
                    if (!Directory.Exists(dirEC)) { throw new ValidacionExcepcion("No fue posible realizar la impresi�n porque no se encontr� la ruta de guardado para el archivo. Favor de contactar a soporte t�cnico."); }

                    DirectoryInfo d = new DirectoryInfo(dirEC);

                    int limitDays = 0;
                    List<SOPF.Core.Poco.dbo.TB_CATParametro> parametros = ParametrosBLL.Parametro("DEC");
                    if (parametros != null && parametros.Count > 0)
                        limitDays = Convert.ToInt32(parametros[0].valor); //5 default
                    else
                        limitDays = 5;
                    DateTime pasada = DateTime.Now.AddDays(-limitDays);
                    DateTime limite = new DateTime(pasada.Year, pasada.Month, pasada.Day, 23, 59, 59);
                    FileInfo[] files = d.GetFiles("*.pdf").Where(f => f.CreationTime <= limite).ToArray();

                    foreach (FileInfo fil in files)
                    {
                        fil.Delete();
                    }
                }

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var solicitud = (from s in con.dbo_TB_Solicitudes
                                     where s.IdSolicitud == peticion.IdSolicitud
                                     select new
                                     {
                                         s.IdSolicitud
                                     }).FirstOrDefault();
                    if (solicitud == null) throw new ValidacionExcepcion("No fue posible encontrar los datos de la soliciutd.");
                    if (peticion.Version == 0)
                    {
                        TB_DocumentoDigital docD = DocumentoDigitalBLL.SolicitudDocumentoDigital(peticion.IdSolicitud, DocumentoDigital.SolicitudRPT);
                        if (docD != null)
                        {
                            peticion.Version = docD.Version;
                        }
                    }
                    SolicitudRPT solicitudRPT = new SolicitudRPT(peticion.Version);

                    SolicitudRPTPeru eCuenta = new SolicitudRPTPeru();
                    //{ IdSolicitud = peticion.IdSolicitud };
                    //eCuenta = PagosBL.Balance_Obtener(eCuenta);

                    eCuenta = dal.SolcitudRPT_Obtener(peticion.IdSolicitud);

                    //List<BalanceCuenta.Recibos> eRecibos = PagosBL.Balance_Obtener_Recibos(eCuenta).ToList();

                    SolicitudRptVM solicitudRptVM = new SolicitudRptVM
                    {
                        IdSolicitud = peticion.IdSolicitud,
                        FechaSolicitud = eCuenta.FechaSolicitud.ToString("dd/MM/yyyy"),
                        ClienteNombre = eCuenta.ClienteNombre,
                        ClienteApePat = eCuenta.ClienteApePat,
                        ClienteApeMat = eCuenta.ClienteApeMat,
                        FechaNacimiento = eCuenta.FechaNacimiento.ToString("dd/MM/yyyy"),
                        Edad = eCuenta.Edad,
                        EstadoCivil = eCuenta.EstadoCivil,
                        Sexo = eCuenta.Sexo,
                        Promotor = eCuenta.Promotor,
                        CanalVenta = eCuenta.CanalVenta,
                        ScoreExperian = eCuenta.ScoreExperian,
                        Origen = eCuenta.Origen,
                        TipoDocumento = eCuenta.TipoDocumento,
                        NumeroDocumento = eCuenta.NumeroDocumento,
                        Celular = eCuenta.Celular,
                        Email = eCuenta.Email,
                        UbicacionOfic = eCuenta.UbicacionOfic,
                        IngresoBruto = eCuenta.IngresoBruto,
                        Otros_Ingr = eCuenta.Otros_Ingr,
                        DescuentosLey = eCuenta.DescuentosLey,
                        SituacionLaboral = eCuenta.SituacionLaboral,
                        Puesto = eCuenta.Puesto,
                        PEP = eCuenta.PEP,
                        LugNac = eCuenta.LugNac,
                        Nacion = eCuenta.Nacion,
                        Distrito = eCuenta.Distrito,
                        Provincia = eCuenta.Provincia,
                        Departamento = eCuenta.Departamento,
                        TipRes = eCuenta.TipRes,
                        RUC = eCuenta.RUC,
                        conyuge = eCuenta.conyuge,
                        cony_tele = eCuenta.cony_tele,
                        Ref1Nom = eCuenta.Ref1Nom,
                        Ref1Pare = eCuenta.Ref1Pare,
                        Ref1Tel = eCuenta.Ref1Tel,
                        Ref2Nom = eCuenta.Ref2Nom,
                        Ref2Pare = eCuenta.Ref2Pare,
                        Ref2Tel = eCuenta.Ref2Tel,
                        NumDepen = eCuenta.NumDepen,
                        Direccion = eCuenta.Direccion,
                        RefDom = eCuenta.RefDom,
                        Empresa = eCuenta.Empresa,
                        Dependencia = eCuenta.Dependencia,
                        TelefonoLaboral = eCuenta.TelefonoLaboral,
                        AnexoLaboral = eCuenta.AnexoLaboral,
                        PuestoLaboral = eCuenta.PuestoLaboral,
                        AntiLaboral = eCuenta.AntiLaboral,
                        OtrosDsctos = eCuenta.OtrosDsctos,
                        IngresoNeto = eCuenta.IngresoNeto,
                        ProOIng = eCuenta.ProOIng,
                        ConyTDoc = eCuenta.ConyTDoc,
                        ConyNDoc = eCuenta.ConyNDoc,
                        ImpCred = eCuenta.ImpCred,
                        PlaCred = eCuenta.PlaCred,
                        MtoCuota = eCuenta.MtoCuota,
                    };

                    byte[] bSolicitudRPT = solicitudRPT.Generar(solicitudRptVM);
                    if (bSolicitudRPT == null || bSolicitudRPT.Length == 0) throw new ValidacionExcepcion("Ocurri� un error durante la generaci�n del estado de cuenta.");

                    if (peticion.RegresaUrl)
                    {
                        try
                        {
                            string extension = ".pdf";
                            Utils.GuardarArchivo($"Solicitud_{peticion.IdSolicitud}{extension}", bSolicitudRPT, dirEC);
                        }
                        catch
                        {
                            throw new ValidacionExcepcion("El archivo no es v�lido, favor de verificarlo.");
                        }
                        respuesta.Resultado.ContenidoArchivo = null;
                        respuesta.Resultado.TipoContenido = "application/pdf";
                        respuesta.Resultado.NombreArchivo = $"Solicitud_{peticion.IdSolicitud}.pdf";
                        respuesta.Resultado.Extension = ".pdf";
                        respuesta.TotalRegistros = 1;
                        respuesta.Resultado.Url = System.ServiceModel.OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.OriginalString.Replace(System.ServiceModel.OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.PathAndQuery, "")
                                                + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings.Get("DirDocumentoECTemp")
                                                + "/" + respuesta.Resultado.NombreArchivo;
                    }
                    else
                    {
                        respuesta.Resultado.ContenidoArchivo = bSolicitudRPT;
                        respuesta.Resultado.TipoContenido = "application/pdf";
                        respuesta.Resultado.NombreArchivo = $"Solicitud_{peticion.IdSolicitud}";
                        respuesta.Resultado.Extension = ".pdf";
                        respuesta.TotalRegistros = 1;
#if DEBUG
                        Utils.GuardarArchivo($"Solicitud_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                    }

                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurri� un error al momento de generar el estado de cuenta.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }



        #endregion

        #region Formatos
        public static ObtenerPaginasFormatosResponse ObtenerPaginasFormatos()
        {
            ObtenerPaginasFormatosResponse res = new ObtenerPaginasFormatosResponse();

            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<FormatoPaginaVM> pags = (from f in con.dbo_TB_CATFormatos
                                                  join fp in con.Catalogo_TB_CATFormatoPaginas on f.idFormato equals fp.IdFormato
                                                  where f.IdEstatus == 1
                                                  select new FormatoPaginaVM
                                                  {
                                                      IdFormatoPagina = fp.IdFormatoPagina,
                                                      IdFormato = fp.IdFormato,
                                                      NombrePagina = fp.NombrePagina,
                                                      PaginaInicial = fp.PaginaInicial,
                                                      PaginaFinal = fp.PaginaFinal,
                                                      CantidadCopias = fp.CantidadCopias,
                                                      NombreDescarga = f.NombreDescarga,
                                                      NombrePlantilla = f.NombrePlantilla
                                                  }).ToList();
                    if (pags != null && pags.Count > 0)
                    {
                        res.formatos = pags;
                    }
                    else
                    {
                        throw new ValidacionExcepcion("No se encontraron p�ginas de formatos activos v�lidas.");
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                res.Error = true;
                res.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                res.Error = true;
                res.MensajeOperacion = "No fue posible obtener las p�ginas de formatos.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}", JsonConvert.SerializeObject(ex));
            }

            return res;
        }
        #endregion

        #region Buzones
        public static CargarBuzonCancelacionResponse CargarBuzonCancelacion(CargarBuzonCancelacionRequest peticion)
        {
            CargarBuzonCancelacionResponse res = new CargarBuzonCancelacionResponse();

            try
            {
                string usuariosVenTodoBuzon = string.Empty;
                if(peticion.IdUsuario > 0)
                {
                    List<Poco.dbo.TB_CATParametro> parametros = ParametrosBLL.Parametro("UBC");
                    if (parametros != null && parametros.Count > 0)
                        usuariosVenTodoBuzon = parametros[0].valor; //21, 24, 95, 41 default
                }
                int[] usuariosVenTodoBuzonArr = !string.IsNullOrEmpty(usuariosVenTodoBuzon) ? usuariosVenTodoBuzon.Split(',').Select(x => int.Parse(x)).ToArray() : null;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    DateTime hoy = DateTime.Now;
                    bool buscaUnico = usuariosVenTodoBuzonArr != null && !usuariosVenTodoBuzonArr.Contains(peticion.IdUsuario);
                    List<ProcesoBuzonCancelacionVM> procs = (from p in con.Clientes_TB_Proceso
                                                             join s in con.dbo_TB_Solicitudes on p.Solicitud_Id equals s.IdSolicitud
                                                             join t in con.dbo_TB_CATTipoCredito_PE on s.IDCredito_PE equals t.IDCredito
                                                             join c in con.dbo_TB_Clientes on p.Cliente_Id equals c.IdCliente
                                                             where p.Estatus_Id == peticion.IdEstatus
                                                             && p.Activo
                                                             && p.TipoProceso_Id == (int)APP.Buzon.QuieroCancelacion
                                                             && p.IdUsuarioAtendio == (buscaUnico ? peticion.IdUsuario : p.IdUsuarioAtendio)
                                                             select new ProcesoBuzonCancelacionVM
                                                             {
                                                                 IdProceso = p.Id_Proceso,
                                                                 Solicitud = p.Solicitud_Id,
                                                                 TipoCredito = t.Credito,
                                                                 Cliente = c.Cliente,
                                                                 SaldoVigente = p.SaldoVigente,
                                                                 PagosRealizados = p.PagosRealizados,
                                                                 Registro = p.FecAlta
                                                             })
                                                             .OrderByDescending(x => x.Registro)
                                                             .ToList();
                    if (procs != null)
                    {
                        res.Procesos = procs;
                    }
                    else
                    {
                        throw new ValidacionExcepcion("Hubo un problema para obtener procesos con ese estatus.");
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                res.Error = true;
                res.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                res.Error = true;
                res.MensajeOperacion = "No fue posible obtener procesos con ese estatus.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}", JsonConvert.SerializeObject(ex));
            }

            return res;
        }

        public static ObtenerEstatusBuzonCancelacionResponse ObtenerEstatusBuzonCancelacion(ObtenerEstatusBuzonCancelacionRequest peticion)
        {
            ObtenerEstatusBuzonCancelacionResponse res = new ObtenerEstatusBuzonCancelacionResponse();
            try
            {
                List<TB_CATEstatus> estatus = new List<TB_CATEstatus>();
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    estatus = con.dbo_TB_CATEstatus
                        .Where(e => e.Ayuda.Equals("Para Buzon Cancelacion", StringComparison.InvariantCultureIgnoreCase)
                            && e.Descripcion.Equals(peticion.Estatus, StringComparison.InvariantCultureIgnoreCase))
                        .ToList();
                    if (estatus != null && estatus.Count > 0)
                    {
                        res.IdEstatus = estatus[0].IdEstatus;
                    }
                    else
                    {
                        throw new ValidacionExcepcion("No se encontraron IdEstatus con esa descripcion.");
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                res.Error = true;
                res.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                res.Error = true;
                res.MensajeOperacion = "No fue posible obtener IdEstatus.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}", JsonConvert.SerializeObject(ex));
            }

            return res;
        }

        public static AtenderBuzonCancelacionResponse AtenderBuzonCancelacion(AtenderBuzonCancelacionRequest peticion)
        {
            AtenderBuzonCancelacionResponse res = new AtenderBuzonCancelacionResponse();
            try
            {
                #region Validaciones
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es v�lida.");

                ObtenerEstatusBuzonCancelacionResponse resEstatus = ObtenerEstatusBuzonCancelacion(new ObtenerEstatusBuzonCancelacionRequest()
                {
                    Estatus = "En proceso"
                });
                if (resEstatus == null || resEstatus.IdEstatus <= 0) throw new ValidacionExcepcion("El estatus en proceso no se ha encontrado.");
                #endregion

                int estatusAnterior = 0;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var proceso = con.Clientes_TB_Proceso.Where(p => p.Id_Proceso == peticion.IdProceso).SingleOrDefault();
                    if (proceso != null && proceso.Id_Proceso > 0)
                    {
                        estatusAnterior = proceso.Estatus_Id;
                        proceso.Estatus_Id = resEstatus.IdEstatus;
                        proceso.IdUsuarioAtendio = peticion.IdUsuario;
                        proceso.FecAtendido = DateTime.Now;
                        con.SaveChanges();

                        if(!RegistrarBuzonCancelacionAccion(proceso.Id_Proceso, estatusAnterior, resEstatus.IdEstatus, "Se atendio el proceso pendiente, ahora esta en estatus en proceso.", peticion.IdUsuario))
                            throw new ValidacionExcepcion("No se pudo registrar movimiento de proceso en bitacora de acciones.");
                        res.MensajeOperacion = "Esta cancelaci�n se mover� a la secci�n \"En proceso\" para su seguimiento.";
                    }
                    else
                    {
                        throw new ValidacionExcepcion("No se encontraron registros con ese idproceso.");
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                res.Error = true;
                res.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                res.Error = true;
                res.MensajeOperacion = "No fue posible atender el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}", JsonConvert.SerializeObject(ex));
            }

            return res;
        }

        private static bool RegistrarBuzonCancelacionAccion(int idProceso, int idEstatusOrigen, int idEstatusDestino, string comentario, int idUsuario)
        {
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    con.Clientes_TB_BuzonAcciones.Add(new TB_BuzonAcciones
                    {
                        IdProceso = idProceso,
                        EstatusOrigen = idEstatusOrigen,
                        EstatusDestino = idEstatusDestino,
                        Comentario = comentario,
                        IdUsuario = idUsuario,
                        FechaRegistro = DateTime.Now
                    });
                    con.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static EnviarCodigoCancelacionResponse EnviarCodigoCancelacion(EnviarCodigoCancelacionRequest peticion)
        {
            EnviarCodigoCancelacionResponse res = new EnviarCodigoCancelacionResponse();

            try
            {
                #region Validaciones
                ObtenerEstatusBuzonCancelacionResponse resEstatus = ObtenerEstatusBuzonCancelacion(new ObtenerEstatusBuzonCancelacionRequest()
                {
                    Estatus = "Recaudo enviado"
                });
                if (resEstatus == null || resEstatus.IdEstatus <= 0) throw new ValidacionExcepcion("El estatus recaudo enviado no se ha encontrado.");
                #endregion

                int estatusAnterior = 0;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var proceso = con.Clientes_TB_Proceso.Where(p => p.Id_Proceso == peticion.IdProceso).SingleOrDefault();
                    if (proceso != null && proceso.Id_Proceso > 0)
                    {
                        estatusAnterior = proceso.Estatus_Id;
                        proceso.Estatus_Id = resEstatus.IdEstatus;
                        proceso.CodigoRecaudo = peticion.Codigo;
                        proceso.FecLimitePago = peticion.FechaLimitePago;
                        con.SaveChanges();

                        if (!RegistrarBuzonCancelacionAccion(proceso.Id_Proceso, estatusAnterior, resEstatus.IdEstatus, "Se envi� c�digo para el proceso en proceso, ahora esta en estatus recaudo enviado.", peticion.IdUsuario))
                            throw new ValidacionExcepcion("No se pudo registrar movimiento de proceso en bitacora de acciones.");
                        res.MensajeOperacion = "Codigo de recaudo enviado correctamente.";
                    }
                    else
                    {
                        throw new ValidacionExcepcion("No se encontraron registros con ese idproceso.");
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                res.Error = true;
                res.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                res.Error = true;
                res.MensajeOperacion = "No fue posible enviar codigo del proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}", JsonConvert.SerializeObject(ex));
            }

            return res;
        }

        public static ConfirmarRecaudoCancelacionResponse ConfirmarRecaudoCancelacion(ConfirmarRecaudoCancelacionRequest peticion)
        {
            ConfirmarRecaudoCancelacionResponse res = new ConfirmarRecaudoCancelacionResponse();

            try
            {
                #region Validaciones
                ObtenerEstatusBuzonCancelacionResponse resEstatus = ObtenerEstatusBuzonCancelacion(new ObtenerEstatusBuzonCancelacionRequest()
                {
                    Estatus = "Terminado"
                });
                if (resEstatus == null || resEstatus.IdEstatus <= 0) throw new ValidacionExcepcion("El estatus terminado no se ha encontrado.");
                #endregion

                int estatusAnterior = 0;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var proceso = con.Clientes_TB_Proceso.Where(p => p.Id_Proceso == peticion.IdProceso).SingleOrDefault();

                    if (proceso != null && proceso.Id_Proceso > 0)
                    {
                        DateTime FechaInicioHora = new DateTime(proceso.FecAlta.Year, proceso.FecAlta.Month, proceso.FecAlta.Day, 0, 0, 0);
                        DateTime FechaFinHora = new DateTime(proceso.FecLimitePago.Value.Year, proceso.FecLimitePago.Value.Month, proceso.FecLimitePago.Value.Day, 23, 59, 59);

                        //VALIDAR QUE EXISTA PAGO EXACTO
                        var pago = con.Cobranza_Pago.Where(p => p.IdSolicitud == proceso.Solicitud_Id
                            && (p.IdEstatus == (int)EstatusPago.Registrado || p.IdEstatus == (int)EstatusPago.Liquidacion)
                            && p.MontoAplicado == 0 && p.MontoCobrado == proceso.SaldoVigente
                            && p.FechaPago >= FechaInicioHora && p.FechaPago <= FechaFinHora)
                            .FirstOrDefault();
                        if (pago == null)
                        {
                            throw new ValidacionExcepcion("Pago de recaudo no registrado en el sistema");
                        }

                        // Obtener el token para mandar notificacion push
                        string token = string.Empty;

                        var usuarioToken = con.Clientes_TB_UsuarioFCMToken.Where(t => t.Usuario_Id == proceso.Usuario_Id && t.Activo).FirstOrDefault();

                        if (usuarioToken != null && usuarioToken.Id_UsuarioFCMToken > 0)
                        {
                            res.token = usuarioToken.Token;
                        }
                        else
                        {
                            throw new Exception("No se pudo encontrar token para notificaci�n push de terminado.");
                        }

                        DateTime fecAlta = new DateTime(proceso.FecAlta.Year, proceso.FecAlta.Month, proceso.FecAlta.Day, 0, 0, 0);
                        TBCreditos.Liquidacion eLiquidacion = new TBCreditos.Liquidacion()
                        {
                            IdSolicitud = Convert.ToInt32(proceso.Solicitud_Id),
                            IdUsuarioRegistro = peticion.IdUsuario,
                            FechaLiquidar = new Utils.DateTimeR() { Value = fecAlta }
                        };

                        // Registrar la Liquidacion
                        Resultado<TBCreditos.Liquidacion> resInsLiquidacion = TBCreditosBll.RegistrarLiquidacion(eLiquidacion);

                        if (resInsLiquidacion.Codigo != 0)
                            throw new Exception(resInsLiquidacion.Mensaje);

                        // Si todo salio bien, hace liquidacion manual
                        Resultado<bool> resLiq = PagosBL.LiquidarPago(new Pago() { IdPago = pago.IdPago }, peticion.IdUsuario);

                        if (resLiq.Codigo != 0)
                            throw new Exception(resLiq.Mensaje);

                        estatusAnterior = proceso.Estatus_Id;
                        proceso.Estatus_Id = resEstatus.IdEstatus;
                        proceso.Comentario = peticion.Comentario;
                        con.SaveChanges();

                        if (!RegistrarBuzonCancelacionAccion(proceso.Id_Proceso, estatusAnterior, resEstatus.IdEstatus, string.Concat("Se confirmo el recaudo para el proceso en recaudo enviado, ahora esta en estatus terminado. Comentario capturado: ", peticion.Comentario), peticion.IdUsuario))
                            throw new Exception("No se pudo registrar movimiento de proceso en bitacora de acciones.");

                        res.MensajeOperacion = "Se ha confirmado el recaudo correctamente.";
                    }
                    else
                    {
                        throw new ValidacionExcepcion("No se encontraron registros con ese idproceso.");
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                res.Error = true;
                res.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                res.Error = true;
                res.MensajeOperacion = string.Concat("No fue posible confirmar recaudo del proceso. ", ex.Message ?? "").Trim();
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}", JsonConvert.SerializeObject(ex));
            }

            return res;
        }

        public static RechazarBuzonCancelacionResponse RechazarBuzonCancelacion(RechazarBuzonCancelacionRequest peticion)
        {
            RechazarBuzonCancelacionResponse res = new RechazarBuzonCancelacionResponse();
            try
            {
                #region Validaciones
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es v�lida.");

                ObtenerEstatusBuzonCancelacionResponse resEstatus = ObtenerEstatusBuzonCancelacion(new ObtenerEstatusBuzonCancelacionRequest()
                {
                    Estatus = "Rechazado"
                });
                if (resEstatus == null || resEstatus.IdEstatus <= 0) throw new ValidacionExcepcion("El estatus rechazado no se ha encontrado.");
                #endregion

                int estatusAnterior = 0;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var proceso = con.Clientes_TB_Proceso.Where(p => p.Id_Proceso == peticion.IdProceso).SingleOrDefault();
                    if (proceso != null && proceso.Id_Proceso > 0)
                    {
                        DateTime ahora = DateTime.Now;
                        estatusAnterior = proceso.Estatus_Id;
                        proceso.Estatus_Id = resEstatus.IdEstatus;
                        proceso.IdUsuarioRechazo = peticion.IdUsuario;
                        proceso.FecRechazo = ahora;
                        con.SaveChanges();
                        if (!RegistrarBuzonCancelacionAccion(proceso.Id_Proceso, estatusAnterior, resEstatus.IdEstatus, "Se rechazo el proceso, ahora esta en estatus en rechazado.", peticion.IdUsuario))
                            throw new ValidacionExcepcion("No se pudo registrar movimiento de proceso en bitacora de acciones.");
                        res.MensajeOperacion = "Esta cancelaci�n se mover� a la secci�n \"Rechazado\".";
                    }
                    else
                    {
                        throw new ValidacionExcepcion("No se encontraron registros con ese idproceso.");
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                res.Error = true;
                res.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                res.Error = true;
                res.MensajeOperacion = "No fue posible rechazar el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}M�todo: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}", JsonConvert.SerializeObject(ex));
            }

            return res;
        }
        #endregion
    }
}