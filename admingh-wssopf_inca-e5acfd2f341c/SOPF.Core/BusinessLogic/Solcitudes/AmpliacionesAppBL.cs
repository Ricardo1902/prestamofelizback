using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.DataAccess.Solicitudes;

namespace SOPF.Core.BusinessLogic.Solicitudes
{
    public static class AmpliacionesAppBL
    {
        private static AmpliacionesAppDal dal;

        static AmpliacionesAppBL()
        {
            dal = new AmpliacionesAppDal();
        }

        public static Resultado<TB_SolicitudAmpliacionExpress> ObtenerSolicitudAmpliacion(int IdSolicitudAmpliacionExpress)
        {
            Resultado<TB_SolicitudAmpliacionExpress> res = dal.ObtenerSolicitudAmpliacion(IdSolicitudAmpliacionExpress);

            if (res.Codigo <= 0)
            {
                // Obtener Expedientes de la AMPEX
                Resultado<List<TB_SolicitudAmpliacionExpress.ExpedienteDocumento>> resExp = dal.ObtenerDocumentacion(res.ResultObject.IdSolicitudAmpliacion);

                if (resExp.Codigo > 0)
                {
                    res.Codigo = resExp.Codigo;
                    res.Mensaje = resExp.Mensaje;
                }
                else
                    res.ResultObject.ExpedienteDocumentos = resExp.ResultObject;


                // Obtener el historico de los estatus de la AMPEX
                Resultado<List<TB_SolicitudAmpliacionExpress.EstatusHistorico>> resHis = dal.ObtenerEstatusHistorico(res.ResultObject.IdSolicitudAmpliacion);

                if (resExp.Codigo > 0)
                {
                    res.Codigo = resExp.Codigo;
                    res.Mensaje = resExp.Mensaje;
                }
                else
                    res.ResultObject.EstatusHistoricos = resHis.ResultObject;
            }

            return res;
        }

        public static Resultado<List<TB_SolicitudAmpliacionExpress>> FiltrarSolicitudes(TB_SolicitudAmpliacionExpress entity)
        {            
            return dal.FiltrarSolicitudes(entity);          
        }
        
        public static void EliminarDocumento(int IdExpedienteAmpliacion, int IdUsuario, string Comentario)
        {
            dal.EliminarDocumento(IdExpedienteAmpliacion, IdUsuario, Comentario);
        }

        public static void AutorizarCredito(int IdSolicitudAmpliacion, int IdUsuario)
        {
            dal.AutorizarCredito(IdSolicitudAmpliacion, IdUsuario);
        }

        public static void DenegarCredito(int IdSolicitudAmpliacion, int IdUsuario, string Comentario)
        {
            dal.DenegarCredito(IdSolicitudAmpliacion, IdUsuario, Comentario);
        }

        public static List<TBCATEstatus> RevisionDocs_CboEstatus()
        {
            return dal.RevisionDocs_CboEstatus();
        }
    }
}