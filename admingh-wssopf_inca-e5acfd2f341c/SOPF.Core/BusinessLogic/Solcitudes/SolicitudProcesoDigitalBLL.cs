﻿using Newtonsoft.Json;
using SOPF.Core.BusinessLogic.Catalogo;
using SOPF.Core.BusinessLogic.Solcitudes;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Dbo;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Model.Solicitudes;
using SOPF.Core.Poco.Solicitudes;
using SOPF.Core.Solicitudes;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Mapping;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SOPF.Core.BusinessLogic.Solicitudes
{
    public static class SolicitudProcesoDigitalBLL
    {
        static readonly int idProcesoOriginacion = 0;
        static readonly int idProcesoReestructura = 0;

        static SolicitudProcesoDigitalBLL()
        {
            TB_Proceso proceso = ProcesoBLL.Originacion();
            if (proceso != null)
            {
                idProcesoOriginacion = proceso.IdProceso;
            }

            proceso = ProcesoBLL.PetionReestructura();
            if (proceso != null)
            {
                idProcesoReestructura = proceso.IdProceso;
            }
        }

        #region GET        
        public static EstatusSolicitudProcesoDigitalResponse Estatus(int idSolicitud, int idProceso)
        {
            EstatusSolicitudProcesoDigitalResponse respuesta = new EstatusSolicitudProcesoDigitalResponse();
            try
            {
                if (idSolicitud > 0)
                {
                    int idTipoEstatus = 0;
                    TipoEstatusVM tipoEsatus = TipoEstatusBLL.TipoEstatus(AppSettings.TipoEstatusProcesoDigital);
                    if (tipoEsatus != null) idTipoEstatus = tipoEsatus.IdTipoEstatus;

                    List<SolicitudProcesoDigitalVM> procesos = null;
                    SolicitudVM solicitud = null;
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        procesos = (from sp in con.Solicitudes_TB_SolicitudProcesoDigital
                                    join p in con.Solicitudes_TB_ProcesoDigital on sp.IdProcesoDigital equals p.IdProcesoDigital
                                    join es in con.dbo_TB_CATEstatus on new { sp.IdEstatus, IdTipoEstatus = idTipoEstatus } equals new { es.IdEstatus, es.IdTipoEstatus }
                                    where sp.Activo
                                        && sp.IdProceso == idProceso
                                        && p.Visible
                                        && sp.IdEstatus != (int)EstatusProcesoDigital.Nuevo
                                        && sp.IdSolicitud == idSolicitud
                                    select new SolicitudProcesoDigitalVM
                                    {
                                        IdSolicitudProcesoDigital = sp.IdSolicitudProcesoDigital,
                                        IdSolicitud = sp.IdSolicitud,
                                        IdEstatus = sp.IdEstatus,
                                        IdProcesoDigital = p.IdProcesoDigital,
                                        ProcesoDigital = p.Proceso,
                                        Orden = sp.Orden,
                                        Estatus = es.Estatus,
                                        ProcesoActual = sp.ProcesoActual
                                    })
                                    .ToList();
                        solicitud = con.dbo_TB_Solicitudes
                            .Where(s => s.IdSolicitud == idSolicitud)
                            .Select(s => new SolicitudVM
                            {
                                IdSolicitud = (int)s.IdSolicitud,
                                IdEstatus = s.IdEstatus ?? 1,
                                EstProceso = s.Est_Proceso ?? 2,
                                IdAsignado = s.IdAsignado ?? 0
                            })
                            .FirstOrDefault();

                        respuesta.ProcesosDigital = procesos;
                        if (procesos != null && procesos.Count > 0 && solicitud != null)
                        {
                            respuesta.ExisteProceso = true;
                            SolicitudProcesoDigitalVM ultimoProcEjecutado = procesos.Where(p => p.ProcesoActual).FirstOrDefault();

                            //Si recien se ha generado la solicitud.                            
                            if (idProceso == idProcesoOriginacion
                                && solicitud.IdEstatus == (int)EstatusSolicitud.Activa
                                && new int[] { 1, 2 }.Contains(solicitud.EstProceso)
                                && solicitud.IdAsignado == 0
                                && ultimoProcEjecutado == null
                                )
                            {
                                respuesta.ExisteProceso = false;
                                return respuesta;
                            }

                            foreach (SolicitudProcesoDigitalVM proceso in procesos)
                            {
                                switch ((ProcesoDigital)proceso.IdProcesoDigital)
                                {
                                    case ProcesoDigital.PruebaVida:
                                        proceso.Mensaje = con.Solicitudes_TB_SolicitudPruebaVida
                                            .Where(pv => pv.Activo
                                                && pv.IdSolicitudProcesoDigital == proceso.IdSolicitudProcesoDigital)
                                        .OrderByDescending(opv => opv.IdSolicitudPruebaVida)
                                        .Select(pv => pv.Mensaje)
                                        .FirstOrDefault();
                                        break;
                                    case ProcesoDigital.FirmaDocumentos:
                                        proceso.Mensaje = con.Solicitudes_TB_SolicitudFirmaDigital
                                                .Where(sp => sp.IdSolicitudProcesoDigital == proceso.IdSolicitudProcesoDigital)
                                                .OrderByDescending(o => o.IdSolicitudFirmaDigital)
                                                .Select(sf => sf.Mensaje)
                                                .FirstOrDefault();
                                        break;
                                    case ProcesoDigital.Analisis:
                                        //Implemenar seguimiento de Análisis
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(idSolicitud)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }


        public static LogProcesoResponse LogProceso(LogProcesoRequest peticion)
        {
            LogProcesoResponse respuesta = new LogProcesoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válido.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El número de solicitud no es válido.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");

                int idTipoEstatus = 0;
                TipoEstatusVM tipoEsatus = TipoEstatusBLL.TipoEstatus(AppSettings.TipoEstatusProcesoDigital);
                if (tipoEsatus != null) idTipoEstatus = tipoEsatus.IdTipoEstatus;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<SolicitudProcesoDigitalVM> procesos = (from sp in con.Solicitudes_TB_SolicitudProcesoDigital
                                                                join p in con.Solicitudes_TB_ProcesoDigital on sp.IdProcesoDigital equals p.IdProcesoDigital
                                                                join es in con.dbo_TB_CATEstatus on new { sp.IdEstatus, IdTipoEstatus = idTipoEstatus } equals new { es.IdEstatus, es.IdTipoEstatus }
                                                                where sp.Activo
                                                                    && sp.IdProceso == peticion.IdProceso
                                                                    && p.Visible
                                                                    && sp.IdEstatus != (int)EstatusProcesoDigital.Nuevo
                                                                    && sp.IdSolicitud == peticion.IdSolicitud
                                                                select new SolicitudProcesoDigitalVM
                                                                {
                                                                    IdSolicitudProcesoDigital = sp.IdSolicitudProcesoDigital,
                                                                    IdSolicitud = sp.IdSolicitud,
                                                                    IdEstatus = sp.IdEstatus,
                                                                    IdProcesoDigital = p.IdProcesoDigital,
                                                                    ProcesoDigital = p.Proceso,
                                                                    Orden = sp.Orden,
                                                                    Estatus = es.Estatus,
                                                                    ProcesoActual = sp.ProcesoActual
                                                                })
                                                                .ToList();

                    if (procesos != null && procesos.Count > 0)
                    {
                        SolicitudProcesoDigitalVM ultimoProcEjecutado = procesos.Where(p => p.ProcesoActual).FirstOrDefault();
                        List<LogProcesoVM> logProcesos = new List<LogProcesoVM>();

                        foreach (SolicitudProcesoDigitalVM proceso in procesos)
                        {
                            switch ((ProcesoDigital)proceso.IdProcesoDigital)
                            {
                                case ProcesoDigital.PruebaVida:
                                    List<LogProcesoVM> logPv = con.Solicitudes_TB_SolicitudPruebaVida
                                        .Where(pv => pv.Activo
                                            && pv.IdSolicitudProcesoDigital == proceso.IdSolicitudProcesoDigital)
                                        .Select(pv => new LogProcesoVM
                                        {
                                            Proceso = proceso.ProcesoDigital,
                                            FechaRegistro = pv.FechaValidacion ?? pv.FechaRegistro,
                                            Accion = pv.Mensaje ?? proceso.Estatus
                                        })
                                        .ToList();
                                    if (logPv != null && logPv.Count > 0)
                                    {
                                        logProcesos.AddRange(logPv);
                                        string claveNotificacion = string.Empty;
                                        if (peticion.IdProceso == idProcesoOriginacion) claveNotificacion = AppSettings.ClaveNotificacionPruebaVida;
                                        List<LogProcesoVM> notifiaciones = (from n in con.Sistema_TB_Notificacion
                                                                            join tn in con.Sistema_TB_TipoNotificacion on n.IdTipoNotificacion equals tn.IdTipoNotificacion
                                                                            where tn.Clave == claveNotificacion
                                                                                && n.IdSolicitud == peticion.IdSolicitud
                                                                            select new LogProcesoVM
                                                                            {
                                                                                Proceso = proceso.ProcesoDigital,
                                                                                FechaRegistro = n.FechaRegistro,
                                                                                Accion = n.Enviado ? "Envio de correo al cliente realizado."
                                                                                    : n.Intentos >= n.MaxIntentos ? "Falló el envio de correo al cliente." : "Enviando correo al cliente."
                                                                            })
                                                             .ToList();
                                        if (notifiaciones != null && notifiaciones.Count > 0)
                                        {
                                            logProcesos.AddRange(notifiaciones);
                                        }
                                    }
                                    break;
                                case ProcesoDigital.FirmaDocumentos:
                                    List<LogProcesoVM> logFirmas = con.Solicitudes_TB_SolicitudFirmaDigital
                                        .Where(fd => fd.Activo
                                            && fd.IdSolicitudProcesoDigital == proceso.IdSolicitudProcesoDigital)
                                        .Select(sf => new LogProcesoVM
                                        {
                                            Proceso = proceso.ProcesoDigital,
                                            FechaRegistro = sf.FechaActualizacion ?? sf.FechaRegistro,
                                            Accion = sf.Mensaje ?? proceso.Estatus
                                        }).ToList();
                                    if (logFirmas != null && logFirmas.Count > 0)
                                    {
                                        logProcesos.AddRange(logFirmas);
                                    }
                                    break;
                                case ProcesoDigital.Analisis:
                                    //Implemenar seguimiento de Análisis
                                    break;
                            }
                        }

                        if (logProcesos != null && logProcesos.Count > 0)
                        {
                            respuesta.LogProcesos = logProcesos.OrderByDescending(o => o.FechaRegistro).ToList();
                        }
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST
        public static void Nuevo(NuevoSolicitudProcesoDigitalRequest peticion)
        {
            try
            {
                #region Validacion del proceso
                if (peticion != null && peticion.Proceso != Proceso.Ninguno)
                {
                    if (peticion.Proceso == Proceso.Originacion)
                    {
                        peticion.IdProceso = idProcesoOriginacion;
                    }
                    else if (peticion.Proceso == Proceso.PeticionReestructura)
                    {
                        peticion.IdProceso = idProcesoReestructura;
                    }
                }
                #endregion

                if (peticion != null && peticion.IdSolicitud > 0 && peticion.IdUsuario > 0 && peticion.IdProceso > 0)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        int idOrigenDef = 0;
                        Poco.Creditos.TB_CATOrigen origenDef = con.Creditos_TB_CATOrigen.Where(o => o.Origen == "PRESENCIAL").FirstOrDefault();
                        if (origenDef != null) idOrigenDef = origenDef.IdOrigen;

                        List<TB_SolicitudProcesoDigital> procesosDigitales = con.Solicitudes_TB_SolicitudProcesoDigital
                            .Where(s => s.IdSolicitud == peticion.IdSolicitud
                                && s.IdProceso == peticion.IdProceso).ToList();

                        var datosSolicitud = con.dbo_TB_Solicitudes.Where(s => s.IdSolicitud == peticion.IdSolicitud)
                                .Select(s => new { s.IdSolicitud, IdTipoCredito = s.idTipoCredito ?? 0, IdOrigen = s.Origen_Id ?? 0 })
                                .FirstOrDefault();

                        var procesos = (from p in con.Solicitudes_TB_ProcesoDigitalConfig
                                        join pd in con.Solicitudes_TB_ProcesoDigitalConfigDet on p.IdProcesoDigitalConfig equals pd.IdProcesoDigitalConfig
                                        where p.Activo
                                             && pd.Activo
                                             && p.IdProceso == peticion.IdProceso
                                        orderby pd.Orden
                                        select new { p.IdProceso, p.IdOrigen, p.IdTipoCredito, pd.IdProcesoDigitalConfigDet, pd.IdProcesoDigital, pd.Orden })
                                               .ToList();

                        if (peticion.IdProceso == idProcesoOriginacion
                            && procesosDigitales != null
                            && procesosDigitales.Count > 0)
                        {
                            /*Originacion solo existe un proceso para originar, se reinicia el mismo proceso.*/
                            procesos = null;
                        }
                        else if (peticion.IdProceso == idProcesoReestructura)
                        {
                            //Desactivar los procesos activos para iniciar un proceso nuevo
                            if (procesosDigitales.Any(p => p.Activo))
                            {
                                foreach (var p in procesosDigitales)
                                {
                                    if (p.Activo)
                                    {
                                        p.Activo = false;
                                    }
                                }
                                con.SaveChanges();
                            }
                        }

                        //Filtrar por Origen segun el proceso
                        if (procesos != null && procesos.Count > 0 && peticion.IdProceso == idProcesoOriginacion)
                        {
                            int idOrigenSol = datosSolicitud.IdOrigen == 0 ? idOrigenDef : datosSolicitud.IdOrigen;
                            procesos = procesos.Where(p => p.IdOrigen == idOrigenSol).ToList();
                        }

                        if (procesos != null && procesos.Count > 0)
                        {
                            List<TB_SolicitudProcesoDigital> solicitudProceso = null;

                            if (procesos.Where(p => p.IdTipoCredito == datosSolicitud.IdTipoCredito).Any())
                            {
                                solicitudProceso = procesos
                                    .Where(p => p.IdTipoCredito == datosSolicitud.IdTipoCredito)
                                    .Select(p => new TB_SolicitudProcesoDigital
                                    {
                                        IdSolicitud = peticion.IdSolicitud,
                                        IdProcesoDigitalConfigDet = p.IdProcesoDigitalConfigDet,
                                        IdProcesoDigital = p.IdProcesoDigital,
                                        Orden = p.Orden,
                                        FechaRegistro = DateTime.Now,
                                        IdEstatus = (int)EstatusProcesoDigital.Nuevo,
                                        Activo = true,
                                        ProcesoActual = false,
                                        IdProceso = p.IdProceso
                                    }).ToList();
                            }
                            else
                            {
                                solicitudProceso = procesos
                                    .Where(p => p.IdTipoCredito == 0)
                                    .Select(p => new TB_SolicitudProcesoDigital
                                    {
                                        IdSolicitud = peticion.IdSolicitud,
                                        IdProcesoDigitalConfigDet = p.IdProcesoDigitalConfigDet,
                                        IdProcesoDigital = p.IdProcesoDigital,
                                        Orden = p.Orden,
                                        FechaRegistro = DateTime.Now,
                                        IdEstatus = (int)EstatusProcesoDigital.Nuevo,
                                        Activo = true,
                                        ProcesoActual = false,
                                        IdProceso = p.IdProceso
                                    }).ToList();
                            }

                            if (solicitudProceso != null && solicitudProceso.Count > 0)
                            {
                                con.Solicitudes_TB_SolicitudProcesoDigital.AddRange(solicitudProceso);
                                con.SaveChanges();
                            }

                            #region Fijar el inicio del proceso
                            TB_SolicitudProcesoDigital procesoInicio = con.Solicitudes_TB_SolicitudProcesoDigital
                                .Where(sp => sp.IdSolicitud == peticion.IdSolicitud
                                    && sp.Activo
                                    && sp.IdProceso == peticion.IdProceso)
                                .OrderBy(o => o.Orden)
                                .FirstOrDefault();

                            if (procesoInicio != null)
                            {
                                procesoInicio.ProcesoActual = true;
                                con.SaveChanges();
                            }
                            #endregion

                            if (peticion.IdProceso == idProcesoOriginacion)
                            {
                                SolicitudDocDigitalBLL.AltaSolicitudDocDigital(peticion.IdSolicitud, peticion.IdProceso);
                            }
                            else if (peticion.IdProceso == idProcesoReestructura)
                            {
                                //Iniciar documentos
                                SolicitudDocDigitalBLL.AltaSolicitudDocDigitalPetReest(peticion.IdSolicitud, peticion.IdProceso);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
        }

        public static InicarSolicitudProcesoDigitalResponse ProcesoOriginacion(InicarSolicitudProcesoDigitalRequest peticion)
        {
            InicarSolicitudProcesoDigitalResponse respuesta = new InicarSolicitudProcesoDigitalResponse();
            try
            {
                if (idProcesoOriginacion <= 0)
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "No existe un proceso para iniciar.";
                }
                else
                {
                    peticion.IdProceso = idProcesoOriginacion;
                    respuesta = SiguienteProceso(peticion);
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static InicarSolicitudProcesoDigitalResponse ProcesoOriginacionPeticionReestructura(InicarSolicitudProcesoDigitalRequest peticion)
        {
            InicarSolicitudProcesoDigitalResponse respuesta = new InicarSolicitudProcesoDigitalResponse();
            try
            {
                if (idProcesoReestructura <= 0)
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "No existe un proceso para iniciar.";
                }
                else
                {
                    peticion.IdProceso = idProcesoReestructura;
                    respuesta = SiguienteProceso(peticion);
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static InicarSolicitudProcesoDigitalResponse SiguienteProceso(InicarSolicitudProcesoDigitalRequest peticion)
        {
            InicarSolicitudProcesoDigitalResponse respuesta = new InicarSolicitudProcesoDigitalResponse();
            try
            {
                EstatusProcesoDigital[] estatusInicio = { EstatusProcesoDigital.Nuevo, EstatusProcesoDigital.EnProceso };
                SolicitudProcesoDigitalVM procesoActual = null;
                SolicitudVM datosSol = null;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    datosSol = con.dbo_TB_Solicitudes
                        .Where(s => s.IdSolicitud == peticion.IdSolicitud)
                        .Select(s => new
                        {
                            IdSolicitud = (int)s.IdSolicitud,
                            IdEstatus = s.IdEstatus ?? 1,
                            EstProceso = s.Est_Proceso ?? 2
                        })
                        .FirstOrDefault()
                        .Convertir<SolicitudVM>();

                    if (datosSol != null &&
                        con.Solicitudes_TB_SolicitudProcesoDigital
                        .Where(s => s.IdSolicitud == peticion.IdSolicitud
                            && s.Activo
                            && s.IdProceso == peticion.IdProceso)
                        .Any())
                    {
                        respuesta.EnviaProcesoDigital = true;
                        List<TB_SolicitudProcesoDigital> procesosDigitales = con.Solicitudes_TB_SolicitudProcesoDigital
                                                    .Where(s => s.IdSolicitud == peticion.IdSolicitud
                                                        && s.Activo
                                                        && s.IdProceso == peticion.IdProceso)
                                                    .ToList();

                        TB_SolicitudProcesoDigital pActual = procesosDigitales.Where(s => s.ProcesoActual
                            && estatusInicio.Contains((EstatusProcesoDigital)s.IdEstatus))
                            .OrderBy(s => s.Orden)
                            .FirstOrDefault();
                        if (pActual != null)
                            procesoActual = pActual.Convertir<SolicitudProcesoDigitalVM>();

                        if (peticion.IdProceso == idProcesoOriginacion)
                        {
                            //Si el proceso digital se completó, pero se condicionó y se solicita reactivar. 
                            //validar si hay cambios llamando el ultimo proceso de firma de documentos.
                            if (procesoActual == null && datosSol.IdEstatus == (int)EstatusSolicitud.Condicionada && peticion.Reactivar)
                            {
                                procesoActual = procesosDigitales
                                    .Where(sp => sp.IdProcesoDigital == (int)ProcesoDigital.FirmaDocumentos)
                                    .OrderByDescending(o => o.IdSolicitudProcesoDigital)
                                    .FirstOrDefault()
                                    .Convertir<SolicitudProcesoDigitalVM>();

                                _ = ReiniciarProceso(new ReiniciarProcesoRequest
                                {
                                    IdUsuario = peticion.IdUsuario,
                                    IdSolicitud = peticion.IdSolicitud,
                                    IdSolicitudProcesoDigital = procesoActual.IdSolicitudProcesoDigital,
                                    Comentario = "Reiniciado por Condicionamiento de solicitud.",
                                    InicioAutomatico = false
                                });

                                //Obtener el proceso nuevo generado
                                procesoActual = con.Solicitudes_TB_SolicitudProcesoDigital
                                    .Where(s => s.IdSolicitud == peticion.IdSolicitud
                                        && s.Activo
                                        && s.ProcesoActual
                                        && estatusInicio.Contains((EstatusProcesoDigital)s.IdEstatus))
                                    .OrderBy(s => s.Orden)
                                    .FirstOrDefault()
                                    .Convertir<SolicitudProcesoDigitalVM>();
                            }
                        }
                    }
                }

                //Validar el proceso digital actual
                if (procesoActual != null)
                {
                    ValidarEnvioProcesoDigitalRequest validarProceso = new ValidarEnvioProcesoDigitalRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        Solicitud = datosSol,
                        ProcesoDigital = procesoActual
                    };

                    ValidarEnvioProcesoDigitalResponse validarEnvioR = null;
                    switch ((ProcesoDigital)procesoActual.IdProcesoDigital)
                    {
                        case ProcesoDigital.PruebaVida:
                            validarEnvioR = SolicitudPruebaVidaBLL.ValidarEnvio(validarProceso);
                            break;
                        case ProcesoDigital.FirmaDocumentos:
                            validarEnvioR = SolicitudFirmaDigitalBLL.VerificaEnvioFirmaDigital(validarProceso);
                            break;
                        case ProcesoDigital.Analisis:
                            validarEnvioR = TBSolicitudesBll.ValidarProcesoAnalisis(validarProceso);
                            break;
                    }

                    if (validarEnvioR != null)
                    {
                        respuesta.Error = validarEnvioR.Error;
                        respuesta.ProcesoIniciado = validarEnvioR.ProcesoIniciado;
                        respuesta.MensajeOperacion = validarEnvioR.MensajeOperacion;
                        if (!validarEnvioR.Error && string.IsNullOrEmpty(respuesta.MensajeOperacion))
                        {
                            respuesta.MensajeOperacion = "El proceso se ha iniciado correctamente.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ReiniciarProcesoResponse ReiniciarProceso(ReiniciarProcesoRequest peticion)
        {
            ReiniciarProcesoResponse respuesta = new ReiniciarProcesoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La solicitud no es correcta.");
                if (peticion.IdSolicitudProcesoDigital <= 0) throw new ValidacionExcepcion("El proceso no es correcto.");
                if (string.IsNullOrEmpty(peticion.Comentario)) throw new ValidacionExcepcion("Ingrese un comentario para realizar la acción.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_SolicitudProcesoDigital procesoReiniciar = con.Solicitudes_TB_SolicitudProcesoDigital
                        .Where(sp => sp.IdSolicitudProcesoDigital == peticion.IdSolicitudProcesoDigital
                            && sp.Activo)
                        .FirstOrDefault();

                    List<TB_SolicitudProcesoDigital> procesos = null;
                    if (procesoReiniciar != null)
                    {
                        procesos = con.Solicitudes_TB_SolicitudProcesoDigital
                            .Where(sp => sp.IdSolicitud == peticion.IdSolicitud
                                && sp.Activo
                                && sp.IdProceso == procesoReiniciar.IdProceso).ToList();
                    }


                    if (procesos == null || procesoReiniciar == null) throw new ValidacionExcepcion("VD01. El proceso no es válido.");

                    List<TB_SolicitudProcesoDigital> nuevosProceso = new List<TB_SolicitudProcesoDigital>();
                    foreach (TB_SolicitudProcesoDigital proceso in procesos)
                    {
                        //Dar de baja los procesos subsecuentes
                        if (proceso.IdSolicitudProcesoDigital == procesoReiniciar.IdSolicitudProcesoDigital
                            || (proceso.Orden >= procesoReiniciar.Orden && !peticion.ReinicioInvidual))
                        {
                            proceso.Activo = false;
                            proceso.Comentario = peticion.Comentario;
                            proceso.IdUsuarioActualiza = peticion.IdUsuario;
                            proceso.FechaActualizacion = DateTime.Now;

                            //Registrar uno nuevo por cada registro dado de baja.
                            nuevosProceso.Add(new TB_SolicitudProcesoDigital
                            {
                                IdSolicitud = proceso.IdSolicitud,
                                IdProcesoDigitalConfigDet = proceso.IdProcesoDigitalConfigDet,
                                IdProcesoDigital = proceso.IdProcesoDigital,
                                Orden = proceso.Orden,
                                FechaRegistro = DateTime.Now,
                                IdEstatus = (int)EstatusProcesoDigital.Nuevo,
                                Activo = true,
                                ProcesoActual = proceso.IdSolicitudProcesoDigital == procesoReiniciar.IdSolicitudProcesoDigital,
                                IdProceso = proceso.IdProceso
                            });

                            //Cancelar los procesos.
                            switch ((ProcesoDigital)proceso.IdProcesoDigital)
                            {
                                case ProcesoDigital.PruebaVida:
                                    TB_SolicitudPruebaVida pruebaVida = con.Solicitudes_TB_SolicitudPruebaVida
                                        .Where(pv => pv.IdSolicitudProcesoDigital == proceso.IdSolicitudProcesoDigital
                                            && pv.Activo)
                                        .OrderByDescending(o => o.IdSolicitudPruebaVida)
                                        .FirstOrDefault();
                                    if (pruebaVida != null)
                                    {
                                        pruebaVida.Activo = false;
                                        pruebaVida.Mensaje = peticion.Comentario;
                                    }
                                    break;
                                case ProcesoDigital.FirmaDocumentos:
                                    TB_SolicitudFirmaDigital firmaDoc = con.Solicitudes_TB_SolicitudFirmaDigital.
                                        Where(sf => sf.IdSolicitudProcesoDigital == proceso.IdSolicitudProcesoDigital
                                            && sf.Activo)
                                        .OrderByDescending(o => o.IdSolicitudFirmaDigital)
                                        .FirstOrDefault();
                                    if (firmaDoc != null)
                                    {
                                        firmaDoc.Activo = false;
                                        firmaDoc.Mensaje = peticion.Comentario;
                                    }

                                    if (peticion.ReinicioManual)
                                    {
                                        SolicitudDocDigitalesResponse docDigitales = SolicitudDocDigitalBLL.SolicitudDocDigitales(new SolicitudDocDigitalesRequest
                                        {
                                            IdProceso = procesoReiniciar.IdProceso ?? 0,
                                            IdSolicitud = peticion.IdSolicitud,
                                            Activo = true
                                        });

                                        if (docDigitales != null && docDigitales.Documentos != null)
                                        {
                                            int[] idDocs = docDigitales.Documentos
                                                .Select(s => s.IdSolicitudDocDigital).ToArray();
                                            List<TB_SolicitudDocDigital> docReiniciar = con.Solicitudes_TB_SolicitudDocDigital
                                                .Where(sdd => idDocs.Contains(sdd.IdSolicitudDocDigital))
                                                .ToList();

                                            foreach (var doc in docReiniciar)
                                            {
                                                doc.Activo = false;
                                                con.Solicitudes_TB_SolicitudDocDigital.Add(new TB_SolicitudDocDigital
                                                {
                                                    IdSolicitud = doc.IdSolicitud,
                                                    IdDocumentoDigital = doc.IdDocumentoDigital,
                                                    FechaRegistro = DateTime.Now,
                                                    Activo = true
                                                });
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }

                    con.Solicitudes_TB_SolicitudProcesoDigital.AddRange(nuevosProceso);
                    con.SaveChanges();

                    if (peticion.InicioAutomatico)
                    {
                        InicarSolicitudProcesoDigitalResponse iniciarResp = SiguienteProceso(new InicarSolicitudProcesoDigitalRequest
                        {
                            IdSolicitud = peticion.IdSolicitud,
                            IdUsuario = peticion.IdUsuario,
                            IdProceso = procesoReiniciar.IdProceso ?? 0
                        });
                        respuesta.Error = iniciarResp.Error;
                        respuesta.MensajeOperacion = iniciarResp.MensajeOperacion;
                    }

                    if (!respuesta.Error)
                    {
                        respuesta.MensajeOperacion = "Se ha reiniciado el proceso. Los procesos posteriores al actual también fueron reiniciados. " + respuesta.MensajeOperacion;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region PUT
        public static ActualizarProcesoDigitalResponse Actualizar(ActualizarProcesoDigitalRequest peticion)
        {
            int idSolicitud = 0;
            int idProceso = 0;

            ActualizarProcesoDigitalResponse respuesta = new ActualizarProcesoDigitalResponse();
            try
            {
                if (peticion == null) throw new Exception("La petición no es correcta.");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_SolicitudProcesoDigital solicitudProceso = con.Solicitudes_TB_SolicitudProcesoDigital
                        .Where(sp => sp.IdSolicitudProcesoDigital == peticion.IdSolicitudProcesoDigital
                            && sp.IdEstatus == (int)EstatusProcesoDigital.EnProceso)
                        .FirstOrDefault();
                    if (solicitudProceso != null)
                    {
                        solicitudProceso.IdEstatus = (int)peticion.EstatusProcesoDigital;
                        idSolicitud = solicitudProceso.IdSolicitud;
                        idProceso = solicitudProceso.IdProceso ?? 0;

                        if (peticion.EstatusProcesoDigital == EstatusProcesoDigital.Aprobado)
                        {
                            TB_SolicitudProcesoDigital procesoSiguiente = con.Solicitudes_TB_SolicitudProcesoDigital
                                .Where(sp => sp.IdSolicitud == solicitudProceso.IdSolicitud
                                    && sp.Activo
                                    && sp.IdProceso == idProceso
                                    && sp.Orden >= solicitudProceso.Orden
                                    && !sp.ProcesoActual)
                                .FirstOrDefault();

                            if (procesoSiguiente != null)
                            {
                                solicitudProceso.ProcesoActual = false;
                                procesoSiguiente.ProcesoActual = true;
                            }
                        }
                        con.SaveChanges();
                    }
                }

                //Verificar si puede continuar el siguiente proceso.
                if (idSolicitud > 0 && peticion.EstatusProcesoDigital == EstatusProcesoDigital.Aprobado)
                {
                    _ = Task.Factory.StartNew(() =>
                    {
                        SiguienteProceso(new InicarSolicitudProcesoDigitalRequest
                        {
                            IdSolicitud = idSolicitud,
                            IdUsuario = (int)UsuarioSistema.Administrador,
                            IdProceso = idProceso
                        });
                    });
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region Metodos privados

        #endregion
    }
}
