#pragma warning disable 160829
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.DataAccess.Solicitudes;

# region Copyright Dimex – 2016
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBBlogComentariosBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBBlogComentariosDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2016-08-29 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Solicitudes
{
    public static class TBBlogComentariosBll
    {
        private static TBBlogComentariosDal dal;

        static TBBlogComentariosBll()
        {
            dal = new TBBlogComentariosDal();
        }

        public static string InsertarTBBlogComentarios(TBBlogComentarios entidad)
        {
            string Respuesta = "";
            Resultado<bool> resultado = dal.InsertarTBBlogComentarios(entidad);
            Respuesta = resultado.Mensaje;
            return Respuesta;

        }
        
        public static TBBlogComentarios ObtenerTBBlogComentarios()
        {
            return dal.ObtenerTBBlogComentarios();
        }
        
        public static void ActualizarTBBlogComentarios()
        {
            dal.ActualizarTBBlogComentarios();
        }

        public static void EliminarTBBlogComentarios()
        {
            dal.EliminarTBBlogComentarios();
        }
    }
}