﻿using SOPF.Core.DataAccess;
using SOPF.Core.Poco.Solicitudes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SOPF.Core.BusinessLogic.Solcitudes
{
    public static class ProcesoBLL
    {
        #region Métodos publicos
        public static TB_Proceso Originacion()
        {
            TB_Proceso originacion = null;
            List<TB_Proceso> procesos = Procesos("Originacion");
            if (procesos != null && procesos.Count > 0)
            {
                originacion = procesos[0];
            }
            return originacion;
        }
        public static TB_Proceso PetionReestructura()
        {
            TB_Proceso originacion = null;
            List<TB_Proceso> procesos = Procesos("PetionReestructura");
            if (procesos != null && procesos.Count > 0)
            {
                originacion = procesos[0];
            }
            return originacion;
        }

        #endregion

        #region metodos privados
        private static List<TB_Proceso> Procesos(string nombreProceso)
        {
            List<TB_Proceso> procesos = null;
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    IQueryable<TB_Proceso> filtro = con.Solicitudes_TB_Proceso;
                    if (!string.IsNullOrEmpty(nombreProceso))
                        filtro = con.Solicitudes_TB_Proceso.Where(p => p.NombreProceso == nombreProceso);
                    procesos = filtro.ToList();
                }
            }
            catch (Exception ex)
            {
                procesos = null;
            }
            return procesos;
        }

        #endregion
    }
}
