﻿using Newtonsoft.Json;
using SOPF.Core.BusinessLogic.Gestiones;
using SOPF.Core.BusinessLogic.Sistema;
using SOPF.Core.BusinessLogic.Solcitudes;
using SOPF.Core.DataAccess;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Lleida;
using SOPF.Core.Model;
using SOPF.Core.Model.Lleida;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Model.Solicitudes;
using SOPF.Core.Poco.dbo;
using SOPF.Core.Poco.Solicitudes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace SOPF.Core.BusinessLogic.Solicitudes
{
    public static class ExpedienteBLL
    {
        static readonly string[] tiposMedioPermitidos = new string[] { "image/png", "image/jpeg", "application/pdf" };
        static readonly string[] mediosImagen = new string[] { "image/png", "image/jpeg" };

        #region GET
        public static ExpedienteResponse Expediente(ExpedienteRequest peticion)
        {
            ExpedienteResponse respuesta = new ExpedienteResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es válida.");

                #region Obtener documentos cargados
                List<TBSolicitudes.Expediente> documentos;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    bool existe = con.dbo_TB_Solicitudes.Any(s => s.IdSolicitud == peticion.IdSolicitud);
                    if (!existe) throw new ValidacionExcepcion("VD01. La clave de la solicitud no es válida.");

                    IQueryable<Tb_Expedientes> expedientes = con.Solicitudes_Tb_Expedientes
                        .Where(e => e.Solicitud_Id == peticion.IdSolicitud
                            && (e.ExpedienteFideicomiso ?? false) == false
                            && (e.Actual ?? false) == true);

                    if (peticion.IdDocumento != null && peticion.IdDocumento >= 0)
                    {
                        //0. Expediente completo
                        expedientes = expedientes.Where(e => e.Documento_Id == peticion.IdDocumento);
                    }
                    else if (peticion.IdDocumento == null)
                    {
                        //Todos los documentos individuales excepto el completo
                        expedientes = expedientes.Where(e => e.Documento_Id > 0);
                    }

                    documentos = expedientes.Select(ex => new TBSolicitudes.Expediente
                    {
                        IdExpediente = ex.Id_DExpediente,
                        IDSolicitud = ex.Solicitud_Id ?? 0,
                        IDDocumento = ex.Documento_Id ?? 0,
                        IDGoogleDrive = ex.GDArchivo_Id,
                        NombreArchivo = ex.Nombre_Archivo
                    }).ToList();

                    if (documentos != null && documentos.Count > 0)
                    {
                        respuesta.Documentos = (from e in expedientes
                                                join d in con.Solicitudes_TB_CATDocumentos on e.Documento_Id equals d.IDDocumento into lef
                                                from lefDoc in lef.DefaultIfEmpty()
                                                select new DocumentoVM
                                                {
                                                    IdDocumento = lefDoc == null ? 0 : lefDoc.IDDocumento,
                                                    NombreDocumento = lefDoc == null ? "Expediente" : lefDoc.Documento
                                                }).ToList();

                        respuesta.TotalRegistros = documentos.Count;
                    }
                }
                #endregion

                #region Descarga de documentos
                if (respuesta.Documentos != null && respuesta.Documentos.Count > 0 && peticion.ConArchivos)
                {
                    List<byte[]> expCompletoN = new List<byte[]>();
                    foreach (DocumentoVM doc in respuesta.Documentos)
                    {
                        TBSolicitudes.Expediente datosExp = documentos.Where(d => d.IDDocumento == doc.IdDocumento).FirstOrDefault();
                        string error = string.Empty;
                        using (ServidorGoogleDrive wsGo = new ServidorGoogleDrive())
                        {
                            ArchivoGoogleDrive descarga = wsGo.DescargarArchivo(datosExp.IDGoogleDrive, ref error);
                            if (string.IsNullOrEmpty(error) && descarga.Contenido != null && descarga.Contenido.Length > 0)
                            {
                                doc.Contenido = Convert.ToBase64String(descarga.Contenido);
                                doc.TipoMedio = descarga.Mime;
#if DEBUG                                
                                expCompletoN.Add(descarga.Contenido);
#endif
                            }
                        }
                    }

#if DEBUG
                    if (expCompletoN != null && expCompletoN.Count > 0)
                    {
                        byte[] archivoPdf = Utils.CombinarPdfs(expCompletoN);
                        if (archivoPdf != null && archivoPdf.Length > 0)
                        {
                            Utils.GuardarArchivo($"{peticion.IdSolicitud}.pdf", archivoPdf);
                        }
                    }
#endif
                }
                #endregion
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "No fue posible obtener los documentos.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST
        public static void CargarDocumentos(CargarDocumentosRequest peticion)
        {
            if (peticion == null) return;
            List<SolicitudFirmaDigitalArchivoVM> respDocumentosAr = SolicitudFirmaDigitalBLL.SolicitudFirmaDigitalArchivos(peticion.IdSolicitudFirmaDigital);
            if (peticion.Archivos != null && peticion.Archivos.Count > 0 && respDocumentosAr != null && respDocumentosAr.Count > 0)
            {
                try
                {
                    List<TBSolicitudes.Expediente> documentos = new List<TBSolicitudes.Expediente>();

                    #region Encontrar IdDocumento
                    foreach (DocumentoFirmaVM doc in peticion.Archivos)
                    {
                        string lowNomDoc = doc.NombreDocumento.ToLower();
                        int posSeparadoNom = lowNomDoc.IndexOf("_");
                        if (posSeparadoNom > 0)
                        {
                            lowNomDoc = lowNomDoc.Substring(0, posSeparadoNom);
                            if (!string.IsNullOrEmpty(lowNomDoc))
                            {
                                int idDocumento = respDocumentosAr.Where(fa => (fa.NombreDocumentoDigital ?? "").Replace(".pdf", "").ToLower() == lowNomDoc)
                                    .Select(fa => fa.IdDocumento)
                                    .DefaultIfEmpty(0)
                                    .FirstOrDefault();
                                if (idDocumento > 0)
                                {
                                    doc.IdDocumento = idDocumento;
                                }
                            }
                        }
                    }

                    peticion.Archivos = peticion.Archivos.Where(a => a.IdDocumento > 0).ToList();
                    #endregion

                    //Documentos cargados
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        documentos = con.Solicitudes_Tb_Expedientes
                            .Where(e => e.Solicitud_Id == peticion.IdSolicitud
                                && (e.ExpedienteFideicomiso ?? false) == false
                                && (e.Actual ?? false) == true)
                            .Select(ex => new TBSolicitudes.Expediente
                            {
                                IdExpediente = ex.Id_DExpediente,
                                IDSolicitud = ex.Solicitud_Id ?? 0,
                                IDDocumento = ex.Documento_Id ?? 0,
                                IDGoogleDrive = ex.GDArchivo_Id,
                                NombreArchivo = ex.Nombre_Archivo,
                            })
                            .ToList();

                        if (documentos == null) documentos = new List<TBSolicitudes.Expediente>();
                    }

                    List<TBSolicitudes.Expediente> digitalExistente = documentos
                        .Where(e => peticion.Archivos
                            .Select(a => a.IdDocumento).ToList()
                            .Contains(e.IDDocumento))
                        .ToList();

                    #region Quitar existentes y registrar los nuevos
                    //Expediente completo
                    TBSolicitudes.Expediente completo = documentos.Where(d => d.IDDocumento == 0).FirstOrDefault();
                    if (completo != null) digitalExistente.Add(completo);

                    //Eliminar existentes digitales(firmados)
                    foreach (TBSolicitudes.Expediente de in digitalExistente)
                    {
                        int pos = documentos.FindIndex(d => d.IDDocumento == de.IDDocumento);
                        if (pos >= 0)
                        {
                            documentos.RemoveAt(pos);
                        }
                    }

                    documentos.AddRange(peticion.Archivos.Select(a => new TBSolicitudes.Expediente
                    {
                        IDSolicitud = peticion.IdSolicitud,
                        IDDocumento = a.IdDocumento,
                        NombreArchivo = a.NombreDocumento,
                        IDGoogleDrive = a.Url
                    }).ToList());
                    documentos = documentos.OrderBy(o => o.IDDocumento).ToList();
                    #endregion

                    //Generar expediente
                    #region GenerarExpediente
                    List<byte[]> expCompletoN = new List<byte[]>();
                    foreach (TBSolicitudes.Expediente d in documentos)
                    {
                        if (d.IdExpediente > 0)
                        {
                            //Descargar
                            using (ServidorGoogleDrive googleDrive = new ServidorGoogleDrive())
                            {
                                string error = string.Empty;
                                ArchivoGoogleDrive archivoDrive = googleDrive.DescargarArchivo(d.IDGoogleDrive, ref error);
                                if (archivoDrive != null)
                                {
                                    if (string.IsNullOrWhiteSpace(error))
                                    {
                                        expCompletoN.Add(archivoDrive.Contenido);
                                    }
                                }
                            }
                        }
                        else
                        {
                            //Descargar
                            byte[] archivo = null;
                            using (var webC = new WebClient())
                            {
                                // Cambiar TSL a 1.2 para conexion a Lleida
                                Utils.EscribirLog("Certificado TSL Actual: " + System.Net.ServicePointManager.SecurityProtocol.ToString(), "TSL ACTUAL");

                                try
                                {
                                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                    Utils.EscribirLog("Certificado TSL Actual: " + System.Net.ServicePointManager.SecurityProtocol.ToString(), "TSL CAMBIO");
                                }
                                catch (Exception ex)
                                {
                                    Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
                                }

                                archivo = webC.DownloadData(d.IDGoogleDrive);
                            }

                            #region Cargar                                
                            if (archivo != null && archivo.Length > 0)
                            {
                                expCompletoN.Add(archivo);
                                using (ServidorGoogleDrive googleDrive = new ServidorGoogleDrive())
                                {
                                    string idArchivoDrive = null, urlArchivoDrive = null;
                                    var expedientes = System.Configuration.ConfigurationManager.AppSettings["GDExpedientes"];
                                    if (string.IsNullOrEmpty(expedientes)) throw new Exception("No fue posible asignar una carpeta para el archivo, favor de revisar la configuracion");

                                    var respuestaArchivo = googleDrive.SubirArchivo(new CargaArchivoGoogleDrive
                                    {
                                        NombreArchivo = $"{d.IDSolicitud}-{d.IDDocumento}.pdf",
                                        IdCarpeta = expedientes,
                                        TipoArchivo = "application/pdf",
                                        Contenido = archivo
                                    }, ref idArchivoDrive, ref urlArchivoDrive);

                                    if (respuestaArchivo != null && respuestaArchivo.ResultObject && !String.IsNullOrEmpty(idArchivoDrive))
                                    {
                                        d.IDGoogleDrive = idArchivoDrive;
                                        d.NombreArchivo = $"{d.IDSolicitud}-{d.IDDocumento}.pdf";
                                    }
                                    else
                                    {
                                        throw new Exception($"No fue posible realizar la carga de un documento: {d.NombreArchivo}-{d.IDGoogleDrive}");
                                    }
                                }
                            }
                            else
                            {
                                throw new Exception($"No fue posible la Descarga de un documento: {d.NombreArchivo}-{d.IDGoogleDrive}");
                            }
                            #endregion
                        }
                    }
                    #endregion

                    #region GenerarExpediente Completo
                    if (expCompletoN != null && expCompletoN.Count > 0)
                    {
                        byte[] nuevoExpCom = Utils.CombinarPdfs(expCompletoN);
                        if (nuevoExpCom != null && nuevoExpCom.Length > 0)
                        {
#if DEBUG
                            Utils.GuardarArchivo($"ExpedienteCompleto_{Path.GetRandomFileName()}.pdf", nuevoExpCom);
#endif
                            Entities.Resultado<bool> respuestaArchivo = new Entities.Resultado<bool>();
                            string idArchivoDrive = null, urlArchivoDrive = null;
                            string expedientes = System.Configuration.ConfigurationManager.AppSettings["GDExpedientes"];

                            if (string.IsNullOrEmpty(expedientes)) throw new Exception("No fue posible asignar una carpeta para el archivo, favor de revisar la configuracion");
                            using (ServidorGoogleDrive googleDrive = new ServidorGoogleDrive())
                            {
                                respuestaArchivo = googleDrive.SubirArchivo(new CargaArchivoGoogleDrive
                                {
                                    NombreArchivo = $"{peticion.IdSolicitud}-0.pdf",
                                    IdCarpeta = expedientes,
                                    TipoArchivo = "application/pdf",
                                    Contenido = nuevoExpCom
                                }, ref idArchivoDrive, ref urlArchivoDrive);
                            }

                            if (respuestaArchivo != null && respuestaArchivo.ResultObject && !string.IsNullOrEmpty(idArchivoDrive))
                            {
                                documentos.Add(new TBSolicitudes.Expediente
                                {
                                    IDSolicitud = peticion.IdSolicitud,
                                    IDDocumento = 0,
                                    NombreArchivo = $"{peticion.IdSolicitud}-0.pdf",
                                    IDGoogleDrive = idArchivoDrive
                                });
                            }
                            else
                            {
                                throw new Exception($"No fue posible realizar la carga de un documento: {peticion.IdSolicitud}-0");
                            }
                        }
                        documentos = documentos.OrderBy(o => o.IDDocumento).ToList();
                    }
                    #endregion

                    #region Guardar Asignacion de nuevo Expediente
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        List<int> idExpExistente = digitalExistente
                            .Select(se => se.IdExpediente).ToList();
                        List<Tb_Expedientes> existentes = (from ex in con.Solicitudes_Tb_Expedientes
                                                           where idExpExistente
                                                            .Contains(ex.Id_DExpediente)
                                                           select ex).ToList();
                        if (existentes != null)
                        {
                            foreach (Tb_Expedientes e in existentes)
                            {
                                e.Actual = false;
                            }
                            con.SaveChanges();
                        }

                        con.Solicitudes_Tb_Expedientes.AddRange(documentos
                            .Where(d => d.IdExpediente == 0)
                            .Select(d => new Tb_Expedientes
                            {
                                Solicitud_Id = peticion.IdSolicitud,
                                Documento_Id = d.IDDocumento,
                                Actual = true,
                                ExpedienteFideicomiso = false,
                                FechaSys = DateTime.Now,
                                GDArchivo_Id = d.IDGoogleDrive,
                                Nombre_Archivo = d.NombreArchivo
                            }));
                        con.SaveChanges();
                    }
                    #endregion
                    #region Actualizar proceso

                    //Actualizar proceso principal
                    _ = SolicitudFirmaDigitalBLL.ActualizarSolicitudFirmaDigital(new ActualizarSolicitudFirmaDigitalRequest
                    {
                        IdSolicitudFirmaDigital = peticion.IdSolicitudFirmaDigital,
                        IdEstatus = (int)EstatusFirma.Signed,
                        FechaFirmado = DateTime.Now,
                        Mensaje = "Carga de documentos firmados completado."
                    });

                    //Lanzar notificacion
                    _ = Task.Factory.StartNew(() =>
                    {
                        ValidarEnvioNotificacion(peticion.IdSolicitudFirmaDigital);
                    });
                    #endregion
                }
                catch (Exception ex)
                {
                    Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                        $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
                    _ = SolicitudFirmaDigitalBLL.ActualizarSolicitudFirmaDigital(new ActualizarSolicitudFirmaDigitalRequest
                    {
                        IdSolicitudFirmaDigital = peticion.IdSolicitudFirmaDigital,
                        IdEstatus = (int)EstatusFirma.Ready,
                        Mensaje = "Ocurrió un error en la carga de documentos firmados."
                    });
                }
            }
            else
            {
                _ = SolicitudFirmaDigitalBLL.ActualizarSolicitudFirmaDigital(new ActualizarSolicitudFirmaDigitalRequest
                {
                    IdSolicitudFirmaDigital = peticion.IdSolicitudFirmaDigital,
                    IdEstatus = (int)EstatusFirma.Ready,
                    Mensaje = "No se recibieron archivos para cargar al expediente."
                });
            }
        }

        /// <summary>
        /// Realiza la carga de documentos al expediente de una solicitud a partir de una lista de documentos en formato base64
        /// </summary>
        /// <param name="peticion"></param>
        /// <returns></returns>
        public static CargarExpedienteResponse CargarExpediente(CargarExpedienteRequest peticion)
        {
            CargarExpedienteResponse respuesta = new CargarExpedienteResponse();

            try
            {
                #region Validaciones
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es válida.");
                if (peticion.Documentos == null || peticion.Documentos.Count == 0) throw new ValidacionExcepcion("Carga de documentos inválido");

                bool vacios = peticion.Documentos.Any(p => string.IsNullOrEmpty(p.Contenido));
                if (vacios) throw new ValidacionExcepcion("Favor de revisar que todos los documentos estén completos.");

                bool idsInvalidos = peticion.Documentos.Any(p => p.IdDocumento == 0);
                if (idsInvalidos) throw new ValidacionExcepcion("Existen documentos con claves incorrectas, favor de validarlos.");

                bool tipoMediosVal = peticion.Documentos.Any(d => !tiposMedioPermitidos.Contains(d.TipoMedio));
                if (tipoMediosVal) throw new ValidacionExcepcion("Existen documentos que no pueden cargarse al expediente, favor de verificarlos.");

                byte[] archivoByte = null;
                foreach (var doc in peticion.Documentos.Where(d => d.IdDocumentoArchivo == 0))
                {
                    try
                    {
                        archivoByte = Convert.FromBase64String(doc.Contenido);
                        if (archivoByte == null || archivoByte.Length == 0) throw new ValidacionExcepcion("Uno o más archivos no son válidos, favor de verificarlo.");
#if DEBUG
                        string extension = ".pdf";
                        if (doc.TipoMedio == "image/png") extension = ".png";
                        else if (doc.TipoMedio == "image/jpeg") extension = ".jpeg";
                        else if (doc.TipoMedio == "application/pdf") extension = ".pdf";
                        Utils.GuardarArchivo($"{peticion.IdSolicitud}_{doc.IdDocumento}{extension}", archivoByte);
#endif
                    }
                    catch
                    {
                        throw new ValidacionExcepcion("Uno o más archivos no son válidos, favor de verificarlo.");
                    }
                }
                #endregion

                Dictionary<int, byte[]> documentosCargar = new Dictionary<int, byte[]>();

                #region Unificar archivos en Tipo de Documentos
                List<int> idDocumentos = peticion.Documentos.Select(d => d.IdDocumento).Distinct().ToList();
                foreach (int idDocumento in idDocumentos)
                {
                    List<DocumentoVM> docsPdf = peticion.Documentos.Where(d => d.IdDocumento == idDocumento && d.TipoMedio == "application/pdf").ToList();
                    List<DocumentoVM> docsImagenes = peticion.Documentos.Where(d => d.IdDocumento == idDocumento && mediosImagen.Contains(d.TipoMedio)).ToList();
                    List<byte[]> byteDocumento = new List<byte[]>();

                    if (docsPdf != null && docsPdf.Count > 0)
                    {
                        foreach (DocumentoVM docPdf in docsPdf)
                        {
                            archivoByte = Convert.FromBase64String(docPdf.Contenido);
                            if (archivoByte == null) throw new ValidacionExcepcion("No fue posible generar el documento archivos pdf proporcionadas.");
                            byteDocumento.Add(archivoByte);
                        }
                    }

                    if (docsImagenes != null && docsImagenes.Count > 0)
                    {
                        archivoByte = Utils.ImagenB64PdfByte(docsImagenes.Select(d => d.Contenido).ToList());
                        if (archivoByte == null) throw new ValidacionExcepcion("No fue posible generar el documento con las imágenes proporcionadas.");
                        byteDocumento.Add(archivoByte);
                    }

                    if (byteDocumento != null && byteDocumento.Count > 0)
                    {
                        archivoByte = Utils.CombinarPdfs(byteDocumento);
                        if (archivoByte == null) throw new ValidacionExcepcion("Ocurrió un error al momento de generar el documento.");
                    }

                    documentosCargar.Add(idDocumento, archivoByte);
#if DEBUG
                    Utils.GuardarArchivo($"{peticion.IdSolicitud}_{idDocumento}_{DateTime.Now:yyMMddHHmmss}.pdf", archivoByte);
#endif
                }
                #endregion

                List<Tb_Expedientes> documentosBD = null;

                #region Documentos Cargados
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    bool existe = con.dbo_TB_Solicitudes.Any(s => s.IdSolicitud == peticion.IdSolicitud);
                    if (!existe) throw new ValidacionExcepcion("VD01. La clave de la solicitud no es válida.");

                    documentosBD = con.Solicitudes_Tb_Expedientes
                        .Where(e => e.Solicitud_Id == peticion.IdSolicitud
                            && (e.ExpedienteFideicomiso ?? false) == false
                            && (e.Actual ?? false) == true)
                        .ToList();

                    if (documentosBD == null) documentosBD = new List<Tb_Expedientes>();
                }
                #endregion

                #region Quitar los existentes en BD
                List<Tb_Expedientes> existentes = documentosBD.Where(d => documentosCargar
                    .Select(doc => doc.Key).ToList()
                    .Contains(d.Documento_Id ?? 0))
                    .ToList();
                if (existentes == null) existentes = new List<Tb_Expedientes>();

                //Quita el expediente completo (porque se generará uno nuevo)
                Tb_Expedientes completo = documentosBD.Where(d => d.Documento_Id == 0).FirstOrDefault();
                if (completo != null) existentes.Add(completo);

                //Quitar archivos que reemplezaremos
                foreach (Tb_Expedientes e in existentes)
                {
                    int pos = documentosBD.FindIndex(d => d.Documento_Id == e.Documento_Id);
                    if (pos >= 0)
                    {
                        documentosBD.RemoveAt(pos);
                    }
                }
                #endregion

                #region Agregar los documentos a cargar
                documentosBD.AddRange(documentosCargar.Select(d => new Tb_Expedientes
                {
                    Solicitud_Id = peticion.IdSolicitud,
                    Documento_Id = d.Key,
                    Nombre_Archivo = $"{peticion.IdSolicitud}-{d.Key}.pdf"
                }));
                #endregion

                #region Descarga de documentos que no se modificaron y Carga de los nuevos documentos
                string expedientes = ConfigurationManager.AppSettings["GDExpedientes"];
                if (string.IsNullOrEmpty(expedientes)) throw new Exception("No fue posible asignar una carpeta para el archivo, favor de revisar la configuracion");

                foreach (Tb_Expedientes doc in documentosBD)
                {
                    if (doc.Id_DExpediente > 0)
                    {
                        using (ServidorGoogleDrive googleDrive = new ServidorGoogleDrive())
                        {
                            string error = string.Empty;
                            ArchivoGoogleDrive archivoDrive = googleDrive.DescargarArchivo(doc.GDArchivo_Id, ref error);
                            if (archivoDrive == null || !string.IsNullOrEmpty(error)) throw new Exception("No fue posible obtener el contenido del archivo en Drive");
                            documentosCargar.Add(doc.Documento_Id ?? 0, archivoDrive.Contenido);
                        }
                    }

                    if (doc.Id_DExpediente == 0)
                    {
                        using (ServidorGoogleDrive googleDrive = new ServidorGoogleDrive())
                        {
                            string idArchivoDrive = null, urlArchivoDrive = null;
                            Respuesta respuestaArchivo = googleDrive.CargarArchivo(new CargaArchivoGoogleDrive
                            {
                                NombreArchivo = doc.Nombre_Archivo,
                                IdCarpeta = expedientes,
                                TipoArchivo = "application/pdf",
                                Contenido = documentosCargar[doc.Documento_Id ?? 0]
                            }, ref idArchivoDrive, ref urlArchivoDrive);

                            if (respuestaArchivo.Error) throw new Exception($"No fue posible realizar la carga de un documento: {doc.Nombre_Archivo}");
                            doc.GDArchivo_Id = idArchivoDrive;
                        }
                    }
                }
                #endregion

                #region Generar Expediente Completo nuevo
                archivoByte = Utils.CombinarPdfs(documentosCargar.OrderBy(o => o.Key)
                    .Select(d => d.Value).ToList());
                if (archivoByte == null | archivoByte.Length == 0) throw new Exception($"No fue posible generar el expediente: {peticion.IdSolicitud}-0.pdf");

#if DEBUG
                Utils.GuardarArchivo($"{peticion.IdSolicitud}_{DateTime.Now:yyMMddHHmmss}.pdf", archivoByte);
#endif

                using (ServidorGoogleDrive googleDrive = new ServidorGoogleDrive())
                {
                    string idArchivoDrive = null, urlArchivoDrive = null;
                    Respuesta respuestaArchivo = googleDrive.CargarArchivo(new CargaArchivoGoogleDrive
                    {
                        NombreArchivo = $"{peticion.IdSolicitud}-0.pdf",
                        IdCarpeta = expedientes,
                        TipoArchivo = "application/pdf",
                        Contenido = archivoByte
                    }, ref idArchivoDrive, ref urlArchivoDrive);

                    if (respuestaArchivo.Error) throw new Exception($"No fue posible realizar la carga del expediente: {peticion.IdSolicitud}-0.pdf");
                    documentosBD.Add(new Tb_Expedientes
                    {
                        Solicitud_Id = peticion.IdSolicitud,
                        Documento_Id = 0,
                        Nombre_Archivo = $"{peticion.IdSolicitud}-0.pdf",
                        GDArchivo_Id = idArchivoDrive
                    });
                }

                documentosBD = documentosBD.OrderBy(o => o.Documento_Id).ToList();
                #endregion

                #region Guardar la asignación del nuevo Expediente
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<int> idExpExistentes = existentes.Select(se => se.Id_DExpediente).ToList();
                    existentes = (from ex in con.Solicitudes_Tb_Expedientes
                                  where idExpExistentes
                                    .Contains(ex.Id_DExpediente)
                                  select ex).ToList();

                    //Desactivar en BD los que serán reemplazados
                    if (existentes != null)
                    {
                        foreach (Tb_Expedientes e in existentes)
                        {
                            e.Actual = false;
                        }
                        con.SaveChanges();
                    }

                    //Guardar la asignacion del nuevo expediente
                    con.Solicitudes_Tb_Expedientes.AddRange(documentosBD
                        .Where(d => d.Id_DExpediente == 0)
                        .Select(d => new Tb_Expedientes
                        {
                            Solicitud_Id = d.Solicitud_Id,
                            Documento_Id = d.Documento_Id,
                            Actual = true,
                            ExpedienteFideicomiso = false,
                            FechaSys = DateTime.Now,
                            GDArchivo_Id = d.GDArchivo_Id,
                            Nombre_Archivo = d.Nombre_Archivo,
                            IdUsuarioRegistra = peticion.IdUsuario
                        }));
                    con.SaveChanges();
                }
                #endregion

                respuesta.MensajeOperacion = "La carga de los documentos se ha realizado correctamente.";

                //Actualizar el Origen de los DocumentoArchivo, que fueron anexados a Expediente
                _ = Task.Factory.StartNew(() =>
                {
                    if (peticion.Documentos.Any(d => d.IdDocumentoArchivo > 0))
                    {
                        CargarDocumentoArchivosMedio(peticion.IdUsuario, peticion.IdSolicitud, peticion.Documentos.Where(d => d.IdDocumentoArchivo > 0).ToList());
                    }
                });
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "No fue posible realizar la carga de los documentos.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        public static CargarDocumentoArchivoResponse CargarDocumentoArchivo(CargarDocumentoArchivoRequest peticion)
        {
            CargarDocumentoArchivoResponse respuesta = new CargarDocumentoArchivoResponse();
            try
            {
                #region Validaciones
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es válida.");
                if (peticion.Documentos == null || peticion.Documentos.Count == 0) throw new ValidacionExcepcion("Carga de documentos inválido");

                bool vacios = peticion.Documentos.Any(p => string.IsNullOrEmpty(p.Contenido));
                if (vacios) throw new ValidacionExcepcion("Favor de revisar que todos los documentos estén completos.");

                bool idsInvalidos = peticion.Documentos.Any(p => p.IdDocumento == 0);
                if (idsInvalidos) throw new ValidacionExcepcion("Existen documentos con claves incorrectas, favor de validarlos.");

                bool tipoMediosVal = peticion.Documentos.Any(d => !tiposMedioPermitidos.Contains(d.TipoMedio));
                if (tipoMediosVal) throw new ValidacionExcepcion("Existen documentos que no pueden cargarse al expediente, favor de verificarlos.");

                string dirDocArchivo = AppSettings.DirDocumentoArchivo;
                if (!Directory.Exists(dirDocArchivo)) { throw new ValidacionExcepcion("No fue posible realizar la carga de los documentos. Favor de contactar a soporte técnico."); }
                #endregion

                #region Registrar los archivos
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var datosSolicitud = con.dbo_TB_Solicitudes.Where(s => s.IdSolicitud == peticion.IdSolicitud)
                        .Select(s => new { s.IdSolicitud, s.fch_Solicitud })
                        .FirstOrDefault();
                    if (datosSolicitud == null) throw new ValidacionExcepcion("Verifique que la clave de la solicitud sea correcta.");

                    string dirSolicitud = Path.Combine(dirDocArchivo, $"{datosSolicitud.fch_Solicitud.Year}", $"{datosSolicitud.fch_Solicitud.Month:00}",
                        $"{datosSolicitud.fch_Solicitud.Day:00}", $"{peticion.IdSolicitud}");
                    if (!Directory.Exists(dirSolicitud))
                    {
                        Directory.CreateDirectory(dirSolicitud);
                    }

                    List<TB_DocumentoArchivo> docArchivos = new List<TB_DocumentoArchivo>();
                    byte[] archivoByte = null;
                    foreach (var doc in peticion.Documentos.Where(d => d.IdDocumentoArchivo == 0))
                    {
                        try
                        {
                            archivoByte = Convert.FromBase64String(doc.Contenido);
                            if (archivoByte == null || archivoByte.Length == 0) throw new ValidacionExcepcion("Uno o más archivos no son válidos, favor de verificarlo.");
                            string idArchivoDrive = $"{Guid.NewGuid()}.b64";
                            string pathArchivoB64 = Path.Combine(dirSolicitud, idArchivoDrive);
                            File.WriteAllText(pathArchivoB64, doc.Contenido);
                            docArchivos.Add(new TB_DocumentoArchivo
                            {
                                IdDocumentoArchivo = doc.IdDocumentoArchivo,
                                IdSolicitud = peticion.IdSolicitud,
                                IdDocumento = doc.IdDocumento,
                                NombreArchivo = doc.NombreDocumento,
                                TipoMedio = doc.TipoMedio,
                                IdArchivoDrive = idArchivoDrive,
                                FechaRegistro = DateTime.Now,
                                IdUsuarioRegistro = peticion.IdUsuario,
                                Actual = true,
                                EnLocal = true
                            });
#if DEBUG
                            string extension = ".pdf";
                            if (doc.TipoMedio == "image/png") extension = ".png";
                            else if (doc.TipoMedio == "image/jpeg") extension = ".jpeg";
                            else if (doc.TipoMedio == "application/pdf") extension = ".pdf";
                            Utils.GuardarArchivo($"{peticion.IdSolicitud}_{doc.IdDocumento}{extension}", archivoByte);
#endif
                        }
                        catch
                        {
                            throw new ValidacionExcepcion("Uno o más archivos no son válidos, favor de verificarlo.");
                        }
                    }

                    con.Solicitudes_TB_DocumentoArchivo.AddRange(docArchivos);
                    con.SaveChanges();

                    respuesta.DocumentoArchivo = docArchivos.Select(d => new DocumentoArchivoVM
                    {
                        IdDocumentoArchivo = d.IdDocumentoArchivo,
                        IdDocumento = d.IdDocumento,
                        NombreArchivo = d.NombreArchivo
                    }).ToList();
                }
                #endregion

                respuesta.MensajeOperacion = "Los archivos se cargaron correctamente al documento.";
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "No fue posible realizar la carga de los documentos. Consulte a soporte técnico.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static Respuesta EliminarDocumentoArchivo(EliminarDocumentoArchivoRequest peticion)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                #region Validaciones
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (peticion.IdDocumentoArchivo <= 0) throw new ValidacionExcepcion("La clave del documento asociado no es válida.");

                string dirDocArchivo = AppSettings.DirDocumentoArchivo;
                if (!Directory.Exists(dirDocArchivo)) { throw new ValidacionExcepcion("No fue posible encontrar la ubicación del archivo. Favor de contactar a soporte técnico."); }
                #endregion

                #region Desactivar DocumentoArchivo
                TB_DocumentoArchivo documentoAr = null;
                TB_Solicitudes solicitud = null;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    documentoAr = con.Solicitudes_TB_DocumentoArchivo.Where(d => d.IdDocumentoArchivo == peticion.IdDocumentoArchivo)
                        .FirstOrDefault();
                    if (documentoAr != null)
                    {
                        var solicitudDB = con.dbo_TB_Solicitudes.Where(s => s.IdSolicitud == documentoAr.IdSolicitud)
                            .Select(s => new { s.IdSolicitud, s.fch_Solicitud })
                            .FirstOrDefault();
                        if (solicitudDB != null) solicitud = new TB_Solicitudes
                        {
                            IdSolicitud = solicitudDB.IdSolicitud,
                            fch_Solicitud = solicitudDB.fch_Solicitud
                        };
                        documentoAr.IdUsuarioActualiza = peticion.IdUsuario;
                        documentoAr.FechaActualizacion = DateTime.Now;
                        documentoAr.Actual = false;
                        con.SaveChanges();
                    }
                }
                #endregion
                respuesta.MensajeOperacion = "El archivo se ha eliminado correctamente.";
                _ = Task.Factory.StartNew(() =>
                {
                    EliminarDocumentoArchivoMedios(documentoAr, solicitud);
                });
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "No fue posible eliminar el documento.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static DocumentosArchivosResponse DocumentosArchivos(DocumentosArchivosRequest peticion)
        {
            DocumentosArchivosResponse respuesta = new DocumentosArchivosResponse();
            try
            {
                #region Validaciones
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es válida.");

                string dirDocArchivo = AppSettings.DirDocumentoArchivo;
                if (!Directory.Exists(dirDocArchivo)) { throw new ValidacionExcepcion("No fue posible encontrar la ubicación del archivo. Favor de contactar a soporte técnico."); }
                #endregion

                List<TB_DocumentoArchivo> documentoArchivos = null;
                DateTime fechaSolicitud;

                #region Obtener Registros de archivos
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    IQueryable<TB_DocumentoArchivo> filtro = con.Solicitudes_TB_DocumentoArchivo
                        .Where(d => d.IdSolicitud == peticion.IdSolicitud
                            && d.Actual);

                    if (peticion.IdsDocumento != null && peticion.IdsDocumento.Count > 0)
                    {
                        filtro = filtro.Where(d => peticion.IdsDocumento.Contains(d.IdDocumento));
                    }

                    if (peticion.IdsDocumentoArchivo != null && peticion.IdsDocumentoArchivo.Count > 0)
                    {
                        filtro = filtro.Where(d => peticion.IdsDocumentoArchivo.Contains(d.IdDocumentoArchivo));
                    }

                    documentoArchivos = filtro.ToList();
                    fechaSolicitud = con.dbo_TB_Solicitudes.Where(s => s.IdSolicitud == peticion.IdSolicitud)
                        .Select(s => s.fch_Solicitud)
                        .FirstOrDefault();
                }
                #endregion

                #region Obtener DocumentoArchivo
                if (documentoArchivos != null && documentoArchivos.Count > 0)
                {
                    string dirSolicitud = Path.Combine(dirDocArchivo, $"{fechaSolicitud.Year}", $"{fechaSolicitud.Month:00}", $"{fechaSolicitud.Day:00}", $"{peticion.IdSolicitud}");

                    if (!peticion.ConArchivo)
                    {
                        respuesta.Documentos = documentoArchivos.Select(d => new DocumentoVM
                        {
                            IdDocumentoArchivo = d.IdDocumentoArchivo,
                            IdDocumento = d.IdDocumento,
                            NombreDocumento = d.NombreArchivo,
                            TipoMedio = d.TipoMedio
                        }).ToList();
                    }
                    else
                    {
                        respuesta.Documentos = new List<DocumentoVM>();

                        foreach (TB_DocumentoArchivo docArchivo in documentoArchivos)
                        {
                            DocumentoVM documentoArchivo = new DocumentoVM
                            {
                                IdDocumentoArchivo = docArchivo.IdDocumentoArchivo,
                                IdDocumento = docArchivo.IdDocumento,
                                NombreDocumento = docArchivo.NombreArchivo,
                                TipoMedio = docArchivo.TipoMedio
                            };

                            #region Contenidos
                            if (docArchivo.EnLocal)
                            {
                                string dirArchivo = Path.Combine(dirSolicitud, docArchivo.IdArchivoDrive);
                                if (File.Exists(dirArchivo))
                                {
                                    documentoArchivo.Contenido = File.ReadAllText(dirArchivo);
                                }
                            }
                            else
                            {
                                //Desde GoogleDrive
                                string error = string.Empty;
                                using (ServidorGoogleDrive wsGo = new ServidorGoogleDrive())
                                {
                                    ArchivoGoogleDrive descarga = wsGo.DescargarArchivo(docArchivo.IdArchivoDrive, ref error);
                                    if (string.IsNullOrEmpty(error) && descarga.Contenido != null && descarga.Contenido.Length > 0)
                                    {
                                        documentoArchivo.Contenido = Convert.ToBase64String(descarga.Contenido);
                                    }
                                }
                            }
                            #endregion
                            respuesta.Documentos.Add(documentoArchivo);
                        }
                    }
                }
                #endregion
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "No fue posible obtener los documentos solicitados.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static Respuesta GeneraDocumentoArchivoExpediente(GeneraDocumentoArchivoExpedienteRequest peticion)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                #region Validaciones
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es válida.");
                #endregion

                #region Obtener Documentos a generar
                ObtenerSolicitudResponse resp = TBSolicitudesBll.ObtenerSolicitud(peticion.IdUsuario, peticion.IdSolicitud);
                if (resp.Solicitud == null || resp.Solicitud.IdSolicitud == 0) throw new ValidacionExcepcion("No fue posible encontrar los datos de la solicitud.");
                SolicitudVM solicitud = resp.Solicitud;
                List<TB_DocumentoArchivo> documentoArchivos = null;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    documentoArchivos = con.Solicitudes_TB_DocumentoArchivo.Where(d => d.IdSolicitud == peticion.IdSolicitud && d.Actual).ToList();
                }
                if (documentoArchivos == null && documentoArchivos.Count == 0) throw new ValidacionExcepcion("No se encontraron documentos para generar el expeidente.");
                #endregion

                #region Filtrar Documentos a enviar
                DocumentosArchivosRequest peticionArchivos = new DocumentosArchivosRequest
                {
                    IdUsuario = peticion.IdUsuario,
                    IdSolicitud = peticion.IdSolicitud,
                    ConArchivo = true
                };

                if ((solicitud.IdEstatus == (int)Core.Solicitudes.EstatusSolicitud.Activa && solicitud.EstProceso == 2)
                    || (solicitud.IdEstatus == (int)Core.Solicitudes.EstatusSolicitud.Condicionada))
                {
                    if (solicitud.IdEstatus == (int)Core.Solicitudes.EstatusSolicitud.Condicionada)
                    {
                        List<TBSolicitudes.Documentos> docCondicionados = TBSolicitudesBll.DocumentosCondicionados(new DocumentosCondicionadosRequest
                        {
                            IdSolicitud = peticion.IdSolicitud,
                            IdUsuario = peticion.IdUsuario
                        });

                        List<int> idsDocumentoArchivos = null;
                        if (docCondicionados != null && docCondicionados.Count > 0)
                        {
                            idsDocumentoArchivos = documentoArchivos
                                .Where(da => docCondicionados
                                    .Select(dc => dc.idDocumento)
                                    .Distinct().ToList()
                                .Contains(da.IdDocumento))
                                .Select(da => da.IdDocumentoArchivo).ToList();
                        }

                        if (idsDocumentoArchivos == null || idsDocumentoArchivos.Count == 0)
                        {
                            respuesta.MensajeOperacion = "No se encontraron documentos condicionados para generar el expediente.";
                            return respuesta;
                        }

                        peticionArchivos.IdsDocumentoArchivo = idsDocumentoArchivos;
                    }

                    DocumentosArchivosResponse respDocumentosArchivos = DocumentosArchivos(peticionArchivos);

                    if (respDocumentosArchivos.Error) throw new ValidacionExcepcion(respDocumentosArchivos.MensajeOperacion);
                    if (respDocumentosArchivos.Documentos == null) throw new ValidacionExcepcion("Verifique se hayan cargado previamente los archivos para la documentación.");


                    CargarExpedienteResponse respCargaExpediente = CargarExpediente(new CargarExpedienteRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        IdSolicitud = peticion.IdSolicitud,
                        Documentos = respDocumentosArchivos.Documentos
                    });

                    respuesta.Error = respCargaExpediente.Error;
                    respuesta.MensajeOperacion = respCargaExpediente.MensajeOperacion;
                }
                else
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "No es posible generar el expediente en este proceso de la solicitud.";
                }
                #endregion
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "No fue posible Generar el Expediente.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region metodos privados
        private static void ValidarEnvioNotificacion(int idSolicitudFirmaDigital)
        {
            try
            {
                int idProcesoOriginacion = 0;
                int idProcesoPetRes = 0;
                TB_Proceso proceso = ProcesoBLL.Originacion();
                if (proceso != null) idProcesoOriginacion = proceso.IdProceso;
                proceso = ProcesoBLL.PetionReestructura();
                if (proceso != null) idProcesoPetRes = proceso.IdProceso;

                SolicitudProcesoDigitalVM solicitudProceso = SolicitudFirmaDigitalBLL.FirmaDigitalProceso(idSolicitudFirmaDigital);
                if (solicitudProceso != null && solicitudProceso.IdProceso > 0)
                {
                    if ((solicitudProceso.IdProceso ?? 0) == idProcesoPetRes)
                    {
                        PeticionReestructuraBll.EnviarCorreoCargaDocs(idSolicitudFirmaDigital);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                        $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(idSolicitudFirmaDigital)}", JsonConvert.SerializeObject(ex));
            }
        }

        public static void EliminarDocumentoArchivoMedios(TB_DocumentoArchivo documentoArchivo, TB_Solicitudes solicitud)
        {
            try
            {
                if (documentoArchivo != null && solicitud != null)
                {
                    string dirDocArchivo = AppSettings.DirDocumentoArchivo;
                    if (!Directory.Exists(dirDocArchivo)) { throw new Exception("No fue posible encontrar la ubicación del archivo. Favor de contactar a soporte técnico."); }

                    string dirSolicitud = Path.Combine(dirDocArchivo, $"{solicitud.fch_Solicitud.Year}", $"{solicitud.fch_Solicitud.Month:00}",
                        $"{solicitud.fch_Solicitud.Day:00}", $"{solicitud.IdSolicitud}");

                    if (documentoArchivo.EnLocal)
                    {
                        dirSolicitud = Path.Combine(dirSolicitud, documentoArchivo.IdArchivoDrive);
                        if (File.Exists(dirSolicitud))
                        {
                            File.Delete(dirSolicitud);
                        }
                    }
                    else
                    {
                        //DEV: Revisar la posibilidad de eliminar desde el Drive para documentos que fueron cargados a Drive
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { documentoArchivo, solicitud })}", JsonConvert.SerializeObject(ex));
            }
        }

        public static void CargarDocumentoArchivosMedio(int idUsuario, int idSolicitud, List<DocumentoVM> documentoArchivos)
        {
            try
            {
                if (idUsuario > 0 && idSolicitud > 0 && documentoArchivos != null && documentoArchivos.Count > 0)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        List<TB_DocumentoArchivo> docsArchivoSol = con.Solicitudes_TB_DocumentoArchivo
                                .Where(d => d.IdSolicitud == idSolicitud
                                    && d.EnLocal)
                                .ToList();
                        var solicitud = con.dbo_TB_Solicitudes.Where(s => s.IdSolicitud == idSolicitud)
                            .Select(s => new { s.IdSolicitud, s.fch_Solicitud })
                            .First();

                        if (docsArchivoSol != null)
                        {
                            string expedientes = ConfigurationManager.AppSettings["GDExpedientes"];
                            if (string.IsNullOrEmpty(expedientes)) throw new Exception("No fue posible asignar una carpeta para el archivo, favor de revisar la configuracion");

                            string dirDocArchivo = AppSettings.DirDocumentoArchivo;
                            if (!Directory.Exists(dirDocArchivo)) { throw new Exception("No fue posible encontrar la ubicación del archivo. Favor de contactar a soporte técnico."); }

                            string dirSolicitud = Path.Combine(dirDocArchivo, $"{solicitud.fch_Solicitud.Year}", $"{solicitud.fch_Solicitud.Month:00}",
                                        $"{solicitud.fch_Solicitud.Day:00}", $"{solicitud.IdSolicitud}");

                            foreach (DocumentoVM docArchivo in documentoArchivos)
                            {
                                TB_DocumentoArchivo documentoArchivo = docsArchivoSol.Where(d => d.IdDocumentoArchivo == docArchivo.IdDocumentoArchivo).FirstOrDefault();
                                if (documentoArchivo != null)
                                {
                                    byte[] archivoByte = Convert.FromBase64String(docArchivo.Contenido);
                                    if (archivoByte == null || archivoByte.Length == 0) throw new ValidacionExcepcion("Uno o más archivos no son válidos, favor de verificarlo.");
                                    string extension = ".pdf";
                                    if (docArchivo.TipoMedio == "image/png") extension = ".png";
                                    else if (docArchivo.TipoMedio == "image/jpeg") extension = ".jpeg";
                                    else if (docArchivo.TipoMedio == "application/pdf") extension = ".pdf";

                                    string idArchivoDrive = null, urlArchivoDrive = null;
                                    using (ServidorGoogleDrive googleDrive = new ServidorGoogleDrive())
                                    {
                                        Respuesta respuestaArchivo = googleDrive.CargarArchivo(new CargaArchivoGoogleDrive
                                        {
                                            NombreArchivo = $"{idSolicitud}_{documentoArchivo.IdDocumentoArchivo}.{extension}",
                                            IdCarpeta = expedientes,
                                            TipoArchivo = docArchivo.TipoMedio,
                                            Contenido = archivoByte
                                        }, ref idArchivoDrive, ref urlArchivoDrive);

                                        if (respuestaArchivo.Error) throw new Exception($"No fue posible realizar la carga del expediente: {idSolicitud}_{documentoArchivo.IdDocumentoArchivo}.{extension}");
                                    }


                                    #region Eliinar Archivo Local
                                    dirSolicitud = Path.Combine(dirSolicitud, documentoArchivo.IdArchivoDrive);
                                    if (File.Exists(dirSolicitud))
                                    {
                                        File.Delete(dirSolicitud);
                                    }
                                    #endregion

                                    documentoArchivo.IdArchivoDrive = idArchivoDrive;
                                    documentoArchivo.EnLocal = false;
                                    documentoArchivo.IdUsuarioActualiza = idUsuario;
                                    documentoArchivo.FechaActualizacion = DateTime.Now;
                                }
                            }
                            con.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { idUsuario, idSolicitud, documentoArchivos })}", JsonConvert.SerializeObject(ex));
            }
        }
        #endregion
    }
}
