﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Model.Solicitudes;
using SOPF.Core.Poco.Solicitudes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SOPF.Core.BusinessLogic.Solicitudes
{
    public static class SolicitudDocDigitalBLL
    {
        #region GET
        public static VerificarSolicitudDocDigitalResponse VerificarSolicitudDocDigital(VerificarSolicitudDocDigitalRequest peticion)
        {
            VerificarSolicitudDocDigitalResponse respuesta = new VerificarSolicitudDocDigitalResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición es incorrecta");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idsolicitud = con.Solicitudes_TB_SolicitudDocDigital.Where(s => s.IdSolicitud == peticion.IdSolicitud)
                        .Select(s => s.IdSolicitud)
                        .DefaultIfEmpty(0)
                        .FirstOrDefault();
                    if (idsolicitud > 0)
                    {
                        respuesta.EnviarProcesoFirmaDigital = true;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static SolicitudDocDigitalesResponse SolicitudDocDigitales(SolicitudDocDigitalesRequest peticion)
        {
            SolicitudDocDigitalesResponse respuesta = new SolicitudDocDigitalesResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<SolicitudDocDigitalVM> documentos = (from sd in con.Solicitudes_TB_SolicitudDocDigital
                                                              join dd in con.Solicitudes_TB_DocumentoDigital on sd.IdDocumentoDigital equals dd.IdDocumentoDigital
                                                              join d in con.Solicitudes_TB_CATDocumentos on dd.IdDocumento equals d.IDDocumento
                                                              where sd.IdSolicitud == peticion.IdSolicitud
                                                                && dd.IdProceso == peticion.IdProceso
                                                                && (peticion.Activo == null || sd.Activo == peticion.Activo)
                                                              select new SolicitudDocDigitalVM
                                                              {
                                                                  IdSolicitudDocDigital = sd.IdSolicitudDocDigital,
                                                                  IdDocumento = dd.IdDocumento,
                                                                  Documento = d.Documento,
                                                                  Version = dd.Version,
                                                                  Contenido = sd.Contenido,
                                                                  Orden = d.Orden ?? 0
                                                              })
                                                              .ToList();
                    if (documentos != null && documentos.Count > 0)
                    {
                        respuesta.Documentos = documentos;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST
        public static void AltaSolicitudDocDigital(int idSolicitud, int idProceso)
        {
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idOrigenDef = 0;
                    Poco.Creditos.TB_CATOrigen origenDef = con.Creditos_TB_CATOrigen.Where(o => o.Origen == "PRESENCIAL").FirstOrDefault();
                    if (origenDef != null) idOrigenDef = origenDef.IdOrigen;

                    var soliciutd = con.dbo_TB_Solicitudes.Where(s => s.IdSolicitud == idSolicitud)
                        .Select(s => new { idSolicitud, IdOrigen = s.Origen_Id ?? idOrigenDef, IdTipoCredito = s.idTipoCredito ?? 0 })
                        .FirstOrDefault();
                    if (soliciutd == null) throw new Exception("No fue posible encontrar la solcitud");

                    List<int> idDocDigitales = (from dd in con.Solicitudes_TB_DocumentoDigital
                                                join sd in con.Solicitudes_TB_SolicitudDocDigital on
                                                    new { dd.IdDocumentoDigital, IdSolicitud = idSolicitud } equals new { sd.IdDocumentoDigital, sd.IdSolicitud } into l1
                                                from lsd in l1.DefaultIfEmpty()
                                                join ds in con.Solicitudes_TB_DocumentoXSolicitud on
                                                    new { dd.IdDocumento, EnvioDigital = true } equals new { ds.IdDocumento, ds.EnvioDigital }
                                                where dd.Activo
                                                    && dd.IdProceso == idProceso
                                                    && lsd == null
                                                    && ds.Activo
                                                    && ds.IdOrigen == soliciutd.IdOrigen
                                                    && ds.IdTipoCredito == soliciutd.IdTipoCredito
                                                select dd.IdDocumentoDigital)
                                                .ToList();

                    if (idDocDigitales != null && idDocDigitales.Count > 0)
                    {
                        con.Solicitudes_TB_SolicitudDocDigital.AddRange(idDocDigitales.Select(dd => new Poco.Solicitudes.TB_SolicitudDocDigital
                        {
                            IdSolicitud = idSolicitud,
                            IdDocumentoDigital = dd,
                            FechaRegistro = DateTime.Now,
                            Activo = true
                        }));
                        con.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {idSolicitud}", JsonConvert.SerializeObject(ex));
            }
        }

        public static void AltaSolicitudDocDigitalPetReest(int idSolicitud, int idProceso)
        {
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<TB_SolicitudDocDigital> solicitudDocumentosD = (from sd in con.Solicitudes_TB_SolicitudDocDigital
                                                                         join dd in con.Solicitudes_TB_DocumentoDigital on sd.IdDocumentoDigital equals dd.IdDocumentoDigital
                                                                         where dd.Activo
                                                                          && dd.IdProceso == idProceso
                                                                          && sd.IdSolicitud == idSolicitud
                                                                         select sd).ToList();

                    //Desactivamos los documentos del proceso activo
                    foreach (var documento in solicitudDocumentosD)
                    {
                        documento.Activo = false;
                    }
                    con.SaveChanges();


                    List<int> idDocDigitales = (from sp in con.Solicitudes_TB_PeticionReestructuras
                                                join tr in con.Solicitudes_TB_CATTipoReestructuras on sp.TipoReestructura_Id equals tr.Id_TipoReestructura
                                                join dd in con.Solicitudes_TB_DocumentoDigital on tr.IdDocumento equals dd.IdDocumento
                                                where sp.IdSolicitudNuevo == idSolicitud
                                                    && dd.IdProceso == idProceso
                                                select dd.IdDocumentoDigital).ToList();

                    if (idDocDigitales != null && idDocDigitales.Count > 0)
                    {
                        con.Solicitudes_TB_SolicitudDocDigital.AddRange(idDocDigitales.Select(dd => new Poco.Solicitudes.TB_SolicitudDocDigital
                        {
                            IdSolicitud = idSolicitud,
                            IdDocumentoDigital = dd,
                            FechaRegistro = DateTime.Now,
                            Activo = true
                        }));
                        con.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {idSolicitud}", JsonConvert.SerializeObject(ex));
            }
        }

        public static ActualizarSolicitudDocDigitalResponse ActualizarSolicitudDocDigital(ActualizarSolicitudDocDigitalRequest peticion)
        {
            ActualizarSolicitudDocDigitalResponse respuesta = new ActualizarSolicitudDocDigitalResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdSolicitudDocDigital < 0 || string.IsNullOrWhiteSpace(peticion.Contenido)) throw new ValidacionExcepcion("Contenido inválido.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_SolicitudDocDigital solicitudDoc = con.Solicitudes_TB_SolicitudDocDigital
                        .Where(sd => sd.IdSolicitudDocDigital == peticion.IdSolicitudDocDigital)
                        .FirstOrDefault();
                    if (solicitudDoc != null)
                    {
                        solicitudDoc.FechaActualizacion = DateTime.Now;
                        if (string.IsNullOrEmpty(solicitudDoc.Contenido))
                        {
                            solicitudDoc.Contenido = peticion.Contenido;
                        }
                        else
                        {
                            solicitudDoc.Activo = false;
                            con.Solicitudes_TB_SolicitudDocDigital.Add(new TB_SolicitudDocDigital
                            {
                                IdSolicitud = solicitudDoc.IdSolicitud,
                                IdDocumentoDigital = solicitudDoc.IdDocumentoDigital,
                                FechaRegistro = DateTime.Now,
                                Contenido = peticion.Contenido,
                                Activo = true
                            });
                        }
                        con.SaveChanges();
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static void ActualizarSolicitudDocDigital(SolicitudDocDigitalVM peticion)
        {
            _ = Task.Factory.StartNew(() =>
            {
                if (peticion != null && peticion.IdSolicitudDocDigital > 0 && !string.IsNullOrEmpty(peticion.Contenido))
                {
                    ActualizarSolicitudDocDigital(new ActualizarSolicitudDocDigitalRequest
                    {
                        IdSolicitudDocDigital = peticion.IdSolicitudDocDigital,
                        Contenido = peticion.Contenido
                    });
                }
            });
        }
        #endregion
    }
}
