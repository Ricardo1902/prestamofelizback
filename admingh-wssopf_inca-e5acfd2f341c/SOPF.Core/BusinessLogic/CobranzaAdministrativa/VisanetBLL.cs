﻿using Newtonsoft.Json;
using OfficeOpenXml;
using SOPF.Core.BusinessLogic.Catalogo;
using SOPF.Core.DataAccess;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.Model.Cobranza;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Poco.dbo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading;

namespace SOPF.Core.BusinessLogic.CobranzaAdministrativa
{
    public class VisanetBLL
    {
        #region Constructor

        public VisanetBLL()
        {
        }
        #endregion

        #region Metodos publicos
        /// <summary>
        /// Metodo para genara una pago mediante el servicio Visanet
        /// </summary>
        /// <param name="pago">Entidad con los datos requeridos para realizar un pago</param>
        /// <returns></returns>
        public static GeneraPagoReponse GenerarPago(GeneraPagoRequest pago)
        {
            GeneraPagoReponse respuesta = new GeneraPagoReponse();
            try
            {
                var respPago = PagoVisanet(new PagoRequest
                {
                    IdUsuario = pago.IdUsuario,
                    IdProducto = pago.Cuenta,
                    Monto = pago.Monto,
                    NumeroTarjeta = pago.NumeroTarjeta,
                    MesExpiracion = pago.MesExpiracion,
                    AnioExpiracion = pago.AnioExpiracion,
                    TipoMonena = PagoRequest.Moneda.PEN
                });
                if (respPago != null)
                {
                    respuesta.Error = respPago.Error;
                    respuesta.CodigoError = respPago.CodigoError;
                    respuesta.MensajeOperacion = respPago.MensajeOperacion;
                    respuesta.Resultado = respPago.Resultado;
                }
                else
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "Ocurrió un error al momento de generar el pago, puede que el servicio de pagos no se encuentre disponible.";
                }
            }
            catch (Exception e)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de iniciar el proceso del pago.";
            }
            return respuesta;
        }

        /// <summary>
        /// Realiza la aplicación de pagos masivos.
        /// </summary>
        /// <param name="pagos"></param>
        /// <returns></returns>
        public static GeneraPagoMavisoResponse GenerarPagoMasivo(GeneraPagoMavisoRequest pagos)
        {
            string[] extValidas = new string[] { ".xls", ".xlsx" };

            GeneraPagoMavisoResponse respuesta = new GeneraPagoMavisoResponse();
            try
            {
                string extension = Path.GetExtension(pagos.NombreArchivo);
                if (pagos.IdUsuario <= 0)
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "El usuario no es válido para realizar la carga de pagos.";
                    return respuesta;
                }
                if (!extValidas.Contains(extension))
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "El archivo no cuenta con el formato válido para realizar la carga de pagos masivos.";
                    return respuesta;
                }
                if (pagos.ContenidoArchivo == null && pagos.ContenidoArchivo.Length <= 0)
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "El archivo no contiene información para procesar pagos.";
                    return respuesta;
                }

                new Thread(() =>
                {
                    _ = ProcesarArchivoPagoMasivo(pagos);
                })
                {
                    IsBackground = true
                }.Start();
            }
            catch (Exception e)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "EI02";
                respuesta.MensajeOperacion = "Ocurrió un error al momento de procesar los datos del pago.";
            }
            return respuesta;
        }

        public static PagosMasivosResponse PagosMasivos(PagosMasivosRequest peticion)
        {
            PagosMasivosResponse respuesta = new PagosMasivosResponse();
            TB_CATEstatus estatus = TipoEstatusBLL.ObtenerTipoEstatus("Para Carga Masiva PagosVisanet");
            if (estatus == null) estatus = new TB_CATEstatus() { IdTipoEstatus = 13 };

            DateTime fIni = new DateTime(peticion.FechaDesde.Year, peticion.FechaDesde.Month, peticion.FechaDesde.Day, 0, 0, 0);
            DateTime fFin = new DateTime(peticion.FechaHasta.Year, peticion.FechaHasta.Month, peticion.FechaHasta.Day, 23, 59, 59);

            //if (peticion.FechaHasta.Equals(new DateTime())) { peticion.FechaHasta }
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respCarga = (from pm in con.Cobranza_PagoMasivo
                                     join es in con.dbo_TB_CATEstatus on pm.IdEstatusProceso equals es.IdEstatus
                                     where es.IdTipoEstatus == estatus.IdTipoEstatus
                                        && (string.IsNullOrEmpty(peticion.NombreArchivo.Trim()) || pm.NombreArchivo.Equals(peticion.NombreArchivo, StringComparison.CurrentCultureIgnoreCase))
                                        && pm.FechaRegistro >= fIni && pm.FechaRegistro <= fFin
                                     select new PagoMasivoVM
                                     {
                                         IdPagoMasivo = pm.IdPagoMasivo,
                                         NombreArchivo = pm.NombreArchivo,
                                         FechaRegistro = pm.FechaRegistro,
                                         TotalRegistros = pm.TotalRegistros,
                                         TotalProcesado = pm.TotalProcesado,
                                         TotalAprobados = pm.TotalAprobados,
                                         MontoTotal = pm.MontoTotal,
                                         Mensaje = pm.Mensaje,
                                         IdEstatusProceso = pm.IdEstatusProceso,
                                         EstatusProceso = es.Descripcion,
                                         UltimaActualizacion = pm.UltimaActualizacion,
                                         FechaFin = pm.FechaFin,
                                         IdUsuarioRegistro = pm.IdUsuarioRegistro
                                     })
                                     .ToList();
                    if (respCarga != null)
                    {
                        respuesta.Resultado.PagosMasivos = respCarga;
                    }
                }
            }
            catch (Exception e)
            {
                //TODO: Enviar error a log de aplicaciones
            }
            return respuesta;
        }

        public static PagosMasivosDetalleResponse PagosMasivosDetalle(PagosMasivosDetalleRequest peticion)
        {
            PagosMasivosDetalleResponse respuesta = new PagosMasivosDetalleResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respDetalle = (from pmd in con.Cobranza_PagoMasivoDetalle
                                       join dataMap in con.Cobranza_VisanetDataMap
                                            on new { IdPeticion = pmd.IdPeticion.Value, IdAccionVisanet = 2 }
                                                equals new { IdPeticion = dataMap.IdPeticionVisanet, dataMap.IdAccionVisanet } into pdata
                                       from result in pdata.DefaultIfEmpty()
                                       where (peticion.IdPagoMasivo == 0 || pmd.IdPagoMasivo == peticion.IdPagoMasivo)
                                       select new PagoMasivoDetalleVM
                                       {
                                           IdPagoMasivoDetalle = pmd.IdPagoMasivoDetalle,
                                           IdCuenta = pmd.IdCuenta,
                                           Monto = pmd.Monto,
                                           NoTarjeta = pmd.NoTarjeta,
                                           Estatus = result.Status,
                                           DescripcionAccion = (result.Action_Description != null) ? result.Action_Description : pmd.Mensaje,
                                           IdTransaccion = result.Transaction_Id,
                                           CodigoAutorizacion = result.Authorization_Code,
                                           NumeroRastreo = result.Trace_Nummber,
                                           MontoOperacion = result.Amount
                                       }
                                       )
                                       .OrderBy(pmd => pmd.IdPagoMasivoDetalle)
                                       .ToList();
                    if (respDetalle != null)
                    {
                        respuesta.Resultado.PagoMasivoDetalle = respDetalle;
                    }
                }
            }
            catch (Exception e)
            {
                //TODO: Enviar error a log de aplicaciones
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de obtener el detalle de la carga masiva.";
            }
            return respuesta;
        }

        public static ProcesarArchivoPagoMasivoRespose ProcesarArchivoPagoMasivo(GeneraPagoMavisoRequest pagos)
        {
            ProcesarArchivoPagoMasivoRespose respuesta = new ProcesarArchivoPagoMasivoRespose();
            PagoMasivoDetalle nuevoPagDetalle;
            List<string> validaciones = new List<string>();
            decimal monto = 0;
            int expiracion = 0;
            int registroMinimo = 0;
            int registroMaximo = 0;
            int valorEntero = 0;

            PagoMasivo pagoMasivo = new PagoMasivo()
            {
                IdUsuarioRegistro = pagos.IdUsuario,
                FechaRegistro = DateTime.Now,
                IdEstatusProceso = 1, //Cargando
                NombreArchivo = pagos.NombreArchivo,
                Error = false
            };
            List<PagoMasivoDetalle> pagoMasivoDetalle = new List<PagoMasivoDetalle>();

            //Procesamiento de la carga del archivo.
            #region ProcesoCargaArchivo
            try
            {
                var ms = new MemoryStream(pagos.ContenidoArchivo);
                var excel = new ExcelPackage(ms);
                registroMinimo = 0;
                registroMaximo = 0;
                valorEntero = 0;

                if (excel != null)
                {
                    var hoja1 = excel.Workbook.Worksheets.First();
                    if (hoja1 != null)
                    {
                        registroMinimo = hoja1.Dimension.Start.Row;
                        registroMaximo = hoja1.Dimension.End.Row;
                        //Para saltar los titulos
                        registroMinimo++;
                        for (int i = registroMinimo; i <= registroMaximo; i++)
                        {
                            monto = 0;
                            expiracion = 0;
                            valorEntero = 0;
                            validaciones = new List<string>();

                            nuevoPagDetalle = new PagoMasivoDetalle
                            {
                                Error = false
                            };
                            try
                            {
                                int.TryParse(hoja1.Cells[i, 1].Text.Trim(), out valorEntero);
                                nuevoPagDetalle.IdCuenta = valorEntero;
                                decimal.TryParse(hoja1.Cells[i, 2].Text.Trim(), out monto);
                                nuevoPagDetalle.Monto = monto;
                                nuevoPagDetalle.NoTarjeta = hoja1.Cells[i, 3].Text.Trim();
                                int.TryParse(hoja1.Cells[i, 4].Text.Trim(), out expiracion);
                                nuevoPagDetalle.MesExpiracion = expiracion;
                                int.TryParse(hoja1.Cells[i, 5].Text.Trim(), out expiracion);
                                nuevoPagDetalle.AnioExpiracion = expiracion;

                                //Validaciones de los campos
                                #region Validaciones de campos
                                if (nuevoPagDetalle.IdCuenta <= 0) validaciones.Add("La cuenta no es válida.");
                                if (nuevoPagDetalle.Monto <= 0) validaciones.Add("El valor del pago no es válido.");
                                if (string.IsNullOrEmpty(nuevoPagDetalle.NoTarjeta) || nuevoPagDetalle.NoTarjeta.Length != 16)
                                    validaciones.Add("El numero de tarjeta no es válido");
                                if (nuevoPagDetalle.MesExpiracion <= 0 || nuevoPagDetalle.MesExpiracion > 12)
                                    validaciones.Add("El mes de expiración no es válido.");                                
                                #endregion
                            }
                            catch (Exception e)
                            {
                                validaciones.Add("Ocurrió un error al cargar el registro." + e.Message);
                                nuevoPagDetalle.Error = true;
                            }
                            if (validaciones.Count > 0)
                            {
                                nuevoPagDetalle.Mensaje = string.Join(" ", validaciones);
                                nuevoPagDetalle.Error = true;
                            }
                            pagoMasivoDetalle.Add(nuevoPagDetalle);
                        }
                    }
                }
                else
                {
                    pagoMasivo.Error = true;
                    pagoMasivo.Mensaje = "No se encontró información de pagos a procesar.";
                }
            }
            catch (Exception e)
            {
                pagoMasivo.Error = true;
                pagoMasivo.Mensaje += "Ocurrió un error al procesar la carga del archivo de pagos - " + e.Message;
            }

            if (pagoMasivoDetalle.Any(p => p.Error.Value))
            {
                pagoMasivo.Error = true;
                pagoMasivo.Mensaje = "Se encontro uno o varios errores en la información del archivo." + pagoMasivo.Mensaje;
            }

            bool procesaPagos = true;

            //Guardar el contenido del archivo de carga
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    con.Cobranza_PagoMasivo.Add(pagoMasivo);
                    con.SaveChanges();
                    respuesta.IdPagoMasivo = pagoMasivo.IdPagoMasivo;

                    //Guardar detalle del pago masivo
                    foreach (var pago in pagoMasivoDetalle)
                    {
                        pago.IdPagoMasivo = pagoMasivo.IdPagoMasivo;
                        pago.FechaRegistro = DateTime.Now;
                        pago.IdPeticion = 0;
                        con.Cobranza_PagoMasivoDetalle.Add(pago);
                        con.SaveChanges();
                    }
                    pagoMasivo.TotalRegistros = pagoMasivoDetalle.Count();
                    pagoMasivo.MontoTotal = pagoMasivoDetalle.Sum(p => p.Monto);
                    pagoMasivo.UltimaActualizacion = DateTime.Now;
                    con.SaveChanges();
                }
            }
            catch (Exception e)
            {
                procesaPagos = false;
            }
            #endregion

            #region ProcesaPagosVisanet
            if (procesaPagos)
            {
                new Thread(() =>
                {
                    ProcesaPagosMasivos(pagoMasivoDetalle, pagoMasivo.IdPagoMasivo, pagos.IdUsuario);
                })
                { IsBackground = true }.Start();
            }
            #endregion

            return respuesta;
        }

        public static PagoMasivoEstatusEnviosResponse PagoMasivoEstatusEnvios(PagoMasivoEstatusEnviosRequest peticion)
        {
            PagoMasivoEstatusEnviosResponse respuesta = new PagoMasivoEstatusEnviosResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es valido.");
                if (peticion.IdPagosMasivos == null || peticion.IdPagosMasivos.Count() == 0) throw new ValidacionExcepcion("La clave del pago masivo no es válido.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta.PagosMasivosEstatus = con.Cobranza_PagoMasivo.Where(pm => peticion.IdPagosMasivos.Contains(pm.IdPagoMasivo))
                        .Select(pm => new PagoMasivoEstatusVM
                        {
                            IdPagoMasivo = pm.IdPagoMasivo,
                            TotalRegistros = pm.TotalRegistros ?? 0,
                            IdEstatusProceso = pm.IdEstatusProceso,
                            TotalProcesados = pm.TotalProcesado ?? 0,
                            TotalAprobados = pm.TotalAprobados ?? 0
                        }).ToList();
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region Metodos privados

        private static PagoResponse PagoVisanet(PagoRequest peticion)
        {
            HttpClient apiVisanet;
            PagoResponse respuestaPago = new PagoResponse();
            string urlMetodo = Utils.UrlWsPFVisanet + "/v1/pagos";
            string strRespuesta = string.Empty;

            apiVisanet = new HttpClient
            {
                BaseAddress = new Uri(Utils.UrlWsPFVisanet)
            };
            apiVisanet.DefaultRequestHeaders.Accept.Clear();
            apiVisanet.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            apiVisanet.Timeout = new TimeSpan(0, 1, 0);
#if DEBUG
            apiVisanet.Timeout = new TimeSpan(0, 5, 0);
#endif

            try
            {
                string contenido = JsonConvert.SerializeObject(peticion);
                var buffer = Encoding.UTF8.GetBytes(contenido);
                var byteContenido = new ByteArrayContent(buffer);
                byteContenido.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");

                HttpResponseMessage httpResponse = apiVisanet.PostAsync(urlMetodo, byteContenido).Result;
                strRespuesta = httpResponse.Content.ReadAsStringAsync().Result;
                respuestaPago = JsonConvert.DeserializeObject<PagoResponse>(strRespuesta);
            }
            catch (Exception e)
            {
                respuestaPago.Error = true;
                respuestaPago.MensajeOperacion = "Ocurrió un error al momento de intentar realizar el pago | " + e.InnerException;
            }

            return respuestaPago;
        }

        private static void RegistrarPago(int IdPeticionVisanet)
        {
            DataAccess.Cobranza.PagosDAL dal = new DataAccess.Cobranza.PagosDAL();

            dal.RegistrarPagoVisaNet(new Pago() { IdPeticionVisaNet = IdPeticionVisanet });
        }

        private static void ProcesaPagosMasivos(List<PagoMasivoDetalle> pagoMasivoDetalle, int idPagoMasivo, int idUsuarioRegistro)
        {
            PagoRequest pagoVisanet = null;
            int registroMinimo = 0;
            int totalAprobados = 0;

            if (pagoMasivoDetalle == null) return;

            // CONSULTAR PARAMETROS CONFIGURACION DE ENABLEAUTH, BLACKLIST
            // Y USUARIO SISTEMA ACTUALIZA TARJETA
            bool validaCampoStatusCodeBlacklist = true;
            bool validaCampoVisaAuthEnab = true;
            int idUsuarioSistemaActualiza = 0;

            List<TB_CATParametro> pBlacklist = Dbo.ParametrosBLL.Parametro("VBL");
            if (pBlacklist != null && pBlacklist.Count > 0)
                validaCampoStatusCodeBlacklist = pBlacklist[0].valor == "1";

            List<TB_CATParametro> pVisaAuth = Dbo.ParametrosBLL.Parametro("VCV");
            if (pVisaAuth != null && pVisaAuth.Count > 0)
                validaCampoVisaAuthEnab = pVisaAuth[0].valor == "1";

            List<TB_CATUsuario> usuarios = new List<TB_CATUsuario>();
            using (CreditOrigContext con = new CreditOrigContext())
            {
                usuarios = con.dbo_TB_CATUsuario.Where(u => u.Usuario == "sistema.cobranza").ToList();
            }

            if (usuarios != null && usuarios.Count > 0)
                idUsuarioSistemaActualiza = usuarios[0].IdUsuario;
            
            foreach (var pago in pagoMasivoDetalle)
            {
                registroMinimo++;
                try
                {
                    //Realizar la autorización del pago.
                    pagoVisanet = new PagoRequest()
                    {
                        IdPagoMasivoDetalle = pago.IdPagoMasivoDetalle,
                        IdUsuario = idUsuarioRegistro,
                        IdProducto = pago.IdCuenta.ToString(),
                        Monto = pago.Monto,
                        NumeroTarjeta = pago.NoTarjeta,
                        MesExpiracion = pago.MesExpiracion.Value,
                        AnioExpiracion = pago.AnioExpiracion.Value,
                        TipoMonena = PagoRequest.Moneda.PEN,
                        ValidaCampoStatusCodeBlacklist = validaCampoStatusCodeBlacklist,
                        ValidaCampoVisaAuthEnab = validaCampoVisaAuthEnab,
                        IdUsuarioSistemaActualiza = idUsuarioSistemaActualiza
                    };

                    // *** REALIZAR LA PETICION DE PAGO AL API
                    PagoResponse pagoRespVisanet = null;
                    if (pago.Error.Value == false)
                    {
                        pagoRespVisanet = PagoVisanet(pagoVisanet);
                    }

                    //Guardar la respuesta de la petición de pago.
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        var pagMasivo = con.Cobranza_PagoMasivo
                            .Where(pm => pm.IdPagoMasivo == idPagoMasivo)
                            .FirstOrDefault();

                        if (pagMasivo != null)
                        {
                            // Procesamiento de pago
                            pagMasivo.IdEstatusProceso = 2;
                            pagMasivo.UltimaActualizacion = DateTime.Now;
                            pagMasivo.TotalProcesado = registroMinimo;
                            con.SaveChanges();
                        }

                        var pagMasivoDetalle = con.Cobranza_PagoMasivoDetalle
                            .Where(pd => pd.IdPagoMasivoDetalle == pago.IdPagoMasivoDetalle)
                            .FirstOrDefault();

                        if (pagMasivoDetalle != null)
                        {
                            if (pagoRespVisanet == null || pagoRespVisanet.Resultado == null)
                            {
                                pagMasivoDetalle.Error = true;
                                pagMasivoDetalle.Mensaje = string.IsNullOrEmpty(pagMasivoDetalle.Mensaje) ? "Ocurrió un error al momento de solicitar la autorización del pago." :
                                    $"{pagMasivoDetalle.Mensaje}. Ocurrió un error al momento de solicitar la autorización del pago.";
                            }
                            else
                            {
                                // Registrar Pago de la Peticion Visanet
                                var pagDataMap = con.Cobranza_VisanetDataMap
                                .Where(pd => pd.IdPeticionVisanet == pagoRespVisanet.Resultado.IdPeticion)
                                .FirstOrDefault();

                                if (pagDataMap != null && pagDataMap.Status == "Authorized")
                                {
                                    RegistrarPago(pagDataMap.IdPeticionVisanet);
                                    totalAprobados++;
                                }

                                // Actualizar el detalle de la Peticion
                                pagMasivoDetalle.IdPeticion = pagoRespVisanet.Resultado.IdPeticion;
                                pagMasivoDetalle.Error = pagoRespVisanet.Error;

                                if (!string.IsNullOrEmpty(pagoRespVisanet.MensajeOperacion))
                                    pagoRespVisanet.MensajeOperacion = pagoRespVisanet.MensajeOperacion.Substring(0, (pagoRespVisanet.MensajeOperacion.Length > 170) ? 170 : pagoRespVisanet.MensajeOperacion.Length);

                                pagMasivoDetalle.Mensaje = string.IsNullOrEmpty(pagMasivoDetalle.Mensaje) ? pagoRespVisanet.MensajeOperacion : $"{pagMasivoDetalle.Mensaje}. {pagoRespVisanet.MensajeOperacion}";
                            }
                            con.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                        $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(idPagoMasivo)}", JsonConvert.SerializeObject(ex));
                }
            }

            try
            {
                //Procesamiento Terminado
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var pagMasivo = con.Cobranza_PagoMasivo
                        .Where(pm => pm.IdPagoMasivo == idPagoMasivo)
                        .FirstOrDefault();

                    if (pagMasivo != null)
                    {
                        pagMasivo.IdEstatusProceso = 3;
                        pagMasivo.UltimaActualizacion = DateTime.Now;
                        pagMasivo.FechaFin = DateTime.Now;
                        pagMasivo.TotalAprobados = totalAprobados;
                        con.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                        $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(idPagoMasivo)}", JsonConvert.SerializeObject(ex));
            }
        }
        #endregion
    }
}
