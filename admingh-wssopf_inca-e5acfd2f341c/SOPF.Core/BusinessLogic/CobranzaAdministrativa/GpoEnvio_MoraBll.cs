﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.DataAccess.CobranzaAdministrativa;


namespace SOPF.Core.BusinessLogic.CobranzaAdministrativa
{
    class GpoEnvio_MoraBll
    {
        private static GpoEnvio_MoraDal dal;
        

        static GpoEnvio_MoraBll()
        {
            dal = new GpoEnvio_MoraDal();
            
        }
        public static List<GpoEnvio_Mora> ObtenerGrupoEnvio_Mora(int Accion, int idGrupoenvio)
        {
            return dal.ObtenerGrupoEnvio_Mora(Accion, idGrupoenvio);
        }
    }
}
