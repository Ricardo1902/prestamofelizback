﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.DataAccess.CobranzaAdministrativa;
using SOPF.Core.BusinessLogic.Tesoreria;

namespace SOPF.Core.BusinessLogic.CobranzaAdministrativa
{
    public static class EnvioCobroBll
    {
        private static EnvioCobroDal dal;

        static EnvioCobroBll()
        {
            dal = new EnvioCobroDal();
        }
        public static string EnivoCobro()
        {
            string Mensaje = "";
            List<EnvioCobro> Envios = new List<EnvioCobro>();
            Envios = dal.SelEnvioCobro();

            int GpoEnvio = 0;
            int idBanco = 0;
            int totalArchivos = 0;
            string[] Particiones;
            int DiasVencido;
            int DiasIni;
            decimal Porc;
            int totalParticiones = 0;
            string Emisora = "";
            string Convenio = "";
            int AplicarGastos = 0;

            foreach (EnvioCobro envio in Envios)
            {
                GpoEnvio = envio.IdGrupo;
                idBanco = envio.idBanco;
                totalArchivos = envio.TotalArchivos;
                Particiones = envio.Particiones.Split(',');
                DiasVencido = envio.DiasVencido;
                DiasIni = envio.DiasIni;
                totalParticiones = envio.TotParticiones;

                switch (idBanco)
                {
                    case 2:

                        Mensaje = CatArchivosBll.GeneraArchivoDomiciliacionBancomer(GpoEnvio, DiasIni, DiasVencido, idBanco, envio.Particiones, totalArchivos, totalParticiones, Emisora, Convenio, AplicarGastos, 0);
                        break;
                }
            }

            return Mensaje;
        }
    }
}
