﻿using SOPF.Core.DataAccess.CobranzaAdministrativa;
using SOPF.Core.Model.Response.Cobranza;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace SOPF.Core.BusinessLogic.CobranzaAdministrativa
{
    public static class DescargarPagosFallidosVisanetBLL
    {
        private static VisanetDal dal;

        static DescargarPagosFallidosVisanetBLL()
        {
            dal = new VisanetDal();
        }

        /// <summary>
        /// Obtiene un reporte en excel de los pagos masivos fallidos para boton de pantalla
        /// </summary>
        /// <returns>object</returns>
        public static DescargarPagosFallidosVisanetResponse DescargarPagosFallidosVisanet(int IdPagoMasivo)
        {
            DescargarPagosFallidosVisanetResponse Resultado = new DescargarPagosFallidosVisanetResponse();
            DataSet ds = new DataSet();
            ds.Tables.Add(ConvertToDatatable(dal.DescargarPagosFallidosVisanet(IdPagoMasivo)));
            Resultado.Resultado.PagosFallidosVisanetVM = ds;
            if(ds.Tables.Count <= 0)
            {
                Resultado.Error = true;
                Resultado.MensajeOperacion = "El archivo no tiene pagos fallidos.";
            }
            return Resultado;
        }

        public static DataTable ConvertToDatatable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
    }
}