﻿using Newtonsoft.Json;
using OfficeOpenXml;
using SOPF.Core.BusinessLogic.Catalogo;
using SOPF.Core.DataAccess;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.Model.Cobranza;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Poco.dbo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;

namespace SOPF.Core.BusinessLogic.CobranzaAdministrativa
{
    public class KushkiBLL
    {
        #region Constructor

        public KushkiBLL()
        {
        }
        #endregion

        #region Metodos publicos
       
        public static string GenerarPagoMasivoKushki(GeneraPagoMavisokushkiRequest peticion)
        {

            PagarKushkiResponse respuesta = new PagarKushkiResponse();
            string PrivateMerchardId= ConfigurationManager.AppSettings.Get("PrivateMerchandId");
            string IdPago = "";
            try
            { 
                string url = "https://api.kushkipagos.com/subscriptions/v1/card/" + peticion.subscriptionId;
                var client = new HttpClient();

                PagarKushkiRequest pago = new PagarKushkiRequest()
                {
                    fullResponse = "v2",
                    amount =
                    {
                        subtotalIva=0,
                        subtotalIva0=peticion.Monto,
                        iva=0,
                        currency= peticion.Moneda
                    },
                    language = "es"
                };

                string contenido = JsonConvert.SerializeObject(pago);
                var respValida = GuargarPago(url, contenido, peticion);
                var byteContenido = new StringContent(contenido);
                byteContenido.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");
                byteContenido.Headers.Add("Private-Merchant-Id", PrivateMerchardId);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var httpResponse =  client.PostAsync(url, byteContenido).Result;
                var result = httpResponse.Content.ReadAsStringAsync().Result;
                var PostResult = JsonConvert.DeserializeObject<PagarKushkiResponse>(result);
                respuesta = PostResult;
                if (respuesta.ticketNumber != null)
                {
                    IdPago = respuesta.ticketNumber;
                    var respValida2 = ActualizarGuargarPago(true, respValida.Resultado.IdPeticion, peticion, IdPago, result);
                    GuardarPagoMasivoPagar(peticion, respValida2.MensajeOperacion, respValida.Resultado.IdPeticion);
                    if (respValida.Error)
                    {
                        IdPago = "";
                    }
                }
                else
                {
                    var respValida2 = ActualizarGuargarPago(false, respValida.Resultado.IdPeticion, peticion, "", result);
                    var resPagoMasivo = GuardarPagoMasivoPagar(peticion, respuesta.message, respValida.Resultado.IdPeticion);
                }
            }
            catch(Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return IdPago;
           
        }

        public static string GenerarSuscripcionMasivoKushki(GeneraPagoMavisokushkiRequest peticion)
        {
            SuscripcionKushkiResponse respuesta = new SuscripcionKushkiResponse();

            string PrivateMerchardId = ConfigurationManager.AppSettings.Get("PrivateMerchandId");
            string idSubscripcion = "";
            try
            {
                string url = "https://api.kushkipagos.com/subscriptions/v1/card";
                var client = new HttpClient();

                SuscripcionKushkiRequest suscripcion = new SuscripcionKushkiRequest()
                {
                    token = peticion.Token,
                    planName = "On-Demand-Risk-Testing",
                    periodicity = "custom",
                    fullResponse = "v2",
                    contactDetails =
                {
                    documentType = peticion.TipoDocumento,
                    documentNumber = peticion.Documento,
                    email = peticion.email,
                    firstName = peticion.NombreTarjetahabiente,
                    lastName = peticion.Apellido,
                    phoneNumber = peticion.Telefono
                },
                    amount =
                {
                    subtotalIva=0,
                    subtotalIva0=0,
                    iva=0,
                    ice=0,
                    currency= peticion.Moneda
                },
                    startDate = peticion.fecha,
                    language = "es"
                };
                string contenido = JsonConvert.SerializeObject(suscripcion);
                var respValida = GuardarSuscripcion(url, contenido, peticion);
                var byteContenido = new StringContent(contenido);
                byteContenido.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");
                byteContenido.Headers.Add("Private-Merchant-Id", PrivateMerchardId);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var httpResponse = client.PostAsync(url, byteContenido).Result;
                var result = httpResponse.Content.ReadAsStringAsync().Result;
                var PostResult =JsonConvert.DeserializeObject<SuscripcionKushkiResponse>(result);
                respuesta = PostResult;
                if (respuesta.subscriptionId != null)
                {
                    idSubscripcion = respuesta.subscriptionId;
                    var respValida2 = ActualizarGuardarSuscripcion(true,respValida.Resultado.IdPeticion, peticion, idSubscripcion, result);

                    var resPagoMasivo = GuardarPagoMasivoSuscripcion(peticion, respValida2.MensajeOperacion, respValida.Resultado.IdPeticion);
                    if (respValida.Error)
                    {

                        idSubscripcion = "";
                    }
                }
                else
                {
                    var respValida2 = ActualizarGuardarSuscripcion(false,respValida.Resultado.IdPeticion, peticion, "", result);
                    var resPagoMasivo = GuardarPagoMasivoSuscripcion(peticion, respuesta.message, respValida.Resultado.IdPeticion);
                }
            }
            catch(Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return idSubscripcion;
        }
      
        public static PagosMasivosKushkiResponse PagosMasivosKushki(PagosMasivosKushkiRequest peticion)
        {
            PagosMasivosKushkiResponse respuesta = new PagosMasivosKushkiResponse();
            TB_CATEstatus estatus = TipoEstatusBLL.ObtenerTipoEstatus("Para Carga Masiva PagosKushki");
            if (estatus == null) estatus = new TB_CATEstatus() { IdTipoEstatus = 23 };

            DateTime fIni = new DateTime(peticion.FechaDesde.Year, peticion.FechaDesde.Month, peticion.FechaDesde.Day, 0, 0, 0);
            DateTime fFin = new DateTime(peticion.FechaHasta.Year, peticion.FechaHasta.Month, peticion.FechaHasta.Day, 23, 59, 59);
            
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respCarga = (from pm in con.Cobranza_PagoMasivoKushki
                                     join es in con.dbo_TB_CATEstatus on pm.iIdEstatusProceso equals es.IdEstatus
                                     where es.IdTipoEstatus == estatus.IdTipoEstatus
                                        && (string.IsNullOrEmpty(peticion.NombreArchivo.Trim()) || pm.vNombreArchivo.Equals(peticion.NombreArchivo, StringComparison.CurrentCultureIgnoreCase))
                                        && pm.sdFechaRegistro >= fIni && pm.sdFechaRegistro <= fFin
                                     select new PagoMasivoKushkiVM
                                     {
                                         IdPagoMasivoKushki = pm.iIdPagosMasivosKushki,
                                         NombreArchivo = pm.vNombreArchivo,
                                         FechaRegistro = pm.sdFechaRegistro,
                                         TotalRegistros = pm.iTotalRegistros,
                                         TotalProcesado = pm.iTotalProcesado,
                                         TotalAprobados = pm.iTotalAprobados,
                                         TotalNoProcesado = pm.iTotalNoProcesado,
                                         MontoTotal = pm.MontoTotal,
                                         MontoCobrado=pm.MontoCobrado,
                                         PorcentajeCobrado=pm.dePorcentajeCobrado,
                                         Mensaje = pm.vMensaje,
                                         IdEstatusProceso = pm.iIdEstatusProceso,
                                         EstatusProceso = es.Descripcion,
                                         UltimaActualizacion = pm.sdUltimaActualizacion,
                                         FechaFin = pm.sdFechaFin,
                                         IdUsuarioRegistro = pm.iIdUsuarioRegistro
                                     })
                                     .ToList();
                    if (respCarga != null)
                    {
                        respuesta.Resultado.PagosMasivosKushki = respCarga;
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static PagosMasivosDetalleKushkiResponse PagosMasivosDetalleKushki(PagosMasivosDetalleKushkiRequest peticion)
        {
            PagosMasivosDetalleKushkiResponse respuesta = new PagosMasivosDetalleKushkiResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respDetalle = (from pmd in con.Cobranza_PagoMasivoDetalleKushki
                                       join pk  in con.Cobranza_PeticionKushki
                                       on new { IdPeticion = pmd.iIdPeticion.Value }
                                       equals new { IdPeticion = pk.iIdPeticionKushki } 
                                       into pdata
                                       from result in pdata.DefaultIfEmpty()
                                       where (peticion.IdPagoMasivo == 0 || pmd.iIdPagosMasivosKushki == peticion.IdPagoMasivo)
                                       select new PagoMasivoDetalleKushkiVM
                                       {
                                           IdPagoMasivoDetalleKushki = pmd.iIdPagosDetMasivosKushki,
                                           IdProducto = pmd.vIdProducto,
                                           Monto = pmd.Monto,
                                           NoTarjeta = pmd.vNoTarjeta,
                                           Estatus= (pmd.bError==false)? "Autorizado": "No Autorizado",
                                           DescripcionAccion = pmd.vMensaje,
                                           IdTransaccion =result.vIdPago,
                                           NumeroRastreo= result.vIdSuscripcion,
                                           NombreTarjetahabiente=pmd.vNomTarjHabiente,
                                           Telefono=pmd.vTelefono
                                       }
                                       )
                                       .OrderBy(pmd => pmd.IdPagoMasivoDetalleKushki)
                                       .ToList();
                    if (respDetalle != null)
                    {
                        respuesta.Resultado.PagoMasivoDetalleKushki = respDetalle;
                    }
                }
            }
            catch (Exception ex)
            {
                
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de obtener el detalle de la carga masiva.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
               $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static PagosMasivosKushkiResponse SuscripcionMasivosKushki(PagosMasivosKushkiRequest peticion)
        {
            PagosMasivosKushkiResponse respuesta = new PagosMasivosKushkiResponse();
            TB_CATEstatus estatus = TipoEstatusBLL.ObtenerTipoEstatus("Para Carga Masiva PagosKushki");
            if (estatus == null) estatus = new TB_CATEstatus() { IdTipoEstatus = 23 };

            DateTime fIni = new DateTime(peticion.FechaDesde.Year, peticion.FechaDesde.Month, peticion.FechaDesde.Day, 0, 0, 0);
            DateTime fFin = new DateTime(peticion.FechaHasta.Year, peticion.FechaHasta.Month, peticion.FechaHasta.Day, 23, 59, 59);

            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respCarga = (from pm in con.Cobranza_SuscripcionMasivoKushki
                                     join es in con.dbo_TB_CATEstatus on pm.iIdEstatusProceso equals es.IdEstatus
                                     where es.IdTipoEstatus == estatus.IdTipoEstatus
                                        && (string.IsNullOrEmpty(peticion.NombreArchivo.Trim()) || pm.vNombreArchivo.Equals(peticion.NombreArchivo, StringComparison.CurrentCultureIgnoreCase))
                                        && pm.sdFechaRegistro >= fIni && pm.sdFechaRegistro <= fFin
                                     select new PagoMasivoKushkiVM
                                     {
                                         IdPagoMasivoKushki = pm.iIdSuscripcionMasivaKushki,
                                         NombreArchivo = pm.vNombreArchivo,
                                         FechaRegistro = pm.sdFechaRegistro,
                                         TotalRegistros = pm.iTotalRegistros,
                                         TotalProcesado = pm.iTotalProcesado,
                                         TotalAprobados = pm.iTotalAprobados,
                                         TotalNoProcesado = pm.iTotalNoProcesado,
                                         Mensaje = pm.vMensaje,
                                         IdEstatusProceso = pm.iIdEstatusProceso,
                                         EstatusProceso = es.Descripcion,
                                         UltimaActualizacion = pm.sdUltimaActualizacion,
                                         FechaFin = pm.sdFechaFin,
                                         IdUsuarioRegistro = pm.iIdUsuarioRegistro
                                     })
                                     .ToList();
                    if (respCarga != null)
                    {
                        respuesta.Resultado.PagosMasivosKushki = respCarga;
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
               $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static PagosMasivosDetalleKushkiResponse SuscripcionMasivosDetalleKushki(PagosMasivosDetalleKushkiRequest peticion)
        {
            PagosMasivosDetalleKushkiResponse respuesta = new PagosMasivosDetalleKushkiResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respDetalle = (from pmd in con.Cobranza_SuscripcionMasivoDetalleKushki
                                       join pk in con.Cobranza_PeticionKushki
                                       on new { IdPeticion = pmd.iIdPeticion.Value }
                                       equals new { IdPeticion = pk.iIdPeticionKushki }
                                       into pdata
                                       from result in pdata.DefaultIfEmpty()
                                       where (peticion.IdPagoMasivo == 0 || pmd.iIdSuscripcionMasivaKushki == peticion.IdPagoMasivo)
                                       select new PagoMasivoDetalleKushkiVM
                                       {
                                           IdPagoMasivoDetalleKushki = pmd.iIdSuscripcionDetMasivaKushki,
                                           NoTarjeta = pmd.vNoTarjeta,
                                           Estatus = (pmd.bError == false) ? "Autorizado" : "No Autorizado",
                                           DescripcionAccion = pmd.vMensaje,
                                           NumeroRastreo = result.vIdSuscripcion,
                                           NombreTarjetahabiente = pmd.vNomTarjHabiente,
                                           Telefono = pmd.vTelefono
                                       }
                                       )
                                       .OrderBy(pmd => pmd.IdPagoMasivoDetalleKushki)
                                       .ToList();
                    if (respDetalle != null)
                    {
                        respuesta.Resultado.PagoMasivoDetalleKushki = respDetalle;
                    }
                }
            }
            catch (Exception ex)
            {

                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de obtener el detalle de la carga masiva.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
               $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static PagoKushkiResponse GenerarPagoKushki(GenerarPagoKushkiRequest pago)
        {

            PagoKushkiResponse respuesta = new PagoKushkiResponse();
            ProcesarArchivoPagoMasivoRespose pagoMasivo = new ProcesarArchivoPagoMasivoRespose();
            try
            {
                pagoMasivo = ProcesarArchivoPagoMasivo(pago);
                respuesta.Resultado.IdPeticion = pagoMasivo.IdPagoMasivo;

            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "EI03";
                respuesta.MensajeOperacion = "Ocurrió un error al momento de procesar los datos del pago.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
               $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pago)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static PagoKushkiResponse GenerarSuscripcionKushki(GenerarPagoKushkiRequest pago)
        {

            PagoKushkiResponse respuesta = new PagoKushkiResponse();
            ProcesarArchivoPagoMasivoRespose pagoMasivo = new ProcesarArchivoPagoMasivoRespose();
            try
            {
                pagoMasivo = ProcesarArchivoSuscripcionMasivo(pago);
                respuesta.Resultado.IdPeticion = pagoMasivo.IdPagoMasivo;

            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "EI03";
                respuesta.MensajeOperacion = "Ocurrió un error al momento de procesar los datos del pago.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
               $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pago)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static PagoKushkiResponse GenerarSuscripcionConErrKushki(GenerarPagoKushkiRequest pago)
        {

            PagoKushkiResponse respuesta = new PagoKushkiResponse();
            ProcesarArchivoPagoMasivoRespose pagoMasivo = new ProcesarArchivoPagoMasivoRespose();
            try
            {
                pagoMasivo = ProcesarArchivoSuscripcionConErrMasivo(pago);
                respuesta.Resultado.IdPeticion = pagoMasivo.IdPagoMasivo;

            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "EI03";
                respuesta.MensajeOperacion = "Ocurrió un error al momento de procesar los datos del pago.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
               $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pago)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static PagoKushkiResponse GenerarPagoConErrKushki(GenerarPagoKushkiRequest pago)
        {

            PagoKushkiResponse respuesta = new PagoKushkiResponse();
            ProcesarArchivoPagoMasivoRespose pagoMasivo = new ProcesarArchivoPagoMasivoRespose();
            try
            {
                pagoMasivo = ProcesarArchivoPagoConErrMasivo(pago);
                respuesta.Resultado.IdPeticion = pagoMasivo.IdPagoMasivo;

            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "EI03";
                respuesta.MensajeOperacion = "Ocurrió un error al momento de procesar los datos del pago.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
               $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pago)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static PagoKushkiResponse GuardarPagoMasivoSuscripcion(GeneraPagoMavisokushkiRequest peticion, string mensaje, int IdPeticion)
        {
            PagoKushkiResponse respuesta = new PagoKushkiResponse();
            SuscripcionMasivoDetalleKushki pagMasivoDetalle = new SuscripcionMasivoDetalleKushki();
            try
            {

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var pagMasivo = con.Cobranza_SuscripcionMasivoKushki
                             .Where(pm => pm.iIdSuscripcionMasivaKushki == peticion.IdPagoMasivo)
                             .FirstOrDefault();


                    var contP = (pagMasivo.iTotalProcesado + 1);
                    var contA = (pagMasivo.iTotalAprobados + 1);
                    // Procesamiento de pago
                        if (contP == pagMasivo.iTotalRegistros)
                        {
                            pagMasivo.iIdEstatusProceso = 3;

                        }
                        else
                        {
                            pagMasivo.iIdEstatusProceso = 2;
                        }
                        pagMasivo.sdUltimaActualizacion = DateTime.Now;
                        pagMasivo.iTotalProcesado = contP;
                    if (mensaje == null)
                    {
                        pagMasivo.iTotalAprobados = contA;
                    }
                    con.SaveChanges();
                      
                    

                }
                var Deta = GuardarPagoMasivoSuscripcionDetalle(peticion, mensaje, IdPeticion);
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error mientras se actualizaba la tabla pago masivo.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
               $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        public static PagoKushkiResponse TerminoProcesoPago(int IdPagoMasivo)
        {
            PagoKushkiResponse respuesta = new PagoKushkiResponse();
            try
            {

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var pagMasivo = con.Cobranza_PagoMasivoKushki
                             .Where(pm => pm.iIdPagosMasivosKushki == IdPagoMasivo)
                             .FirstOrDefault();
                   var pagMasivoCant = con.Cobranza_PagoMasivoDetalleKushki
                            .Where(pm => pm.iIdPagosMasivosKushki == IdPagoMasivo).Count();

                    var pagMasivoApr = con.Cobranza_PagoMasivoDetalleKushki
                            .Where(pm => pm.iIdPagosMasivosKushki == IdPagoMasivo && pm.bError==false).Count();

                    var pagMasivoMonAp = con.Cobranza_PagoMasivoDetalleKushki
                            .Where(pm => pm.iIdPagosMasivosKushki == IdPagoMasivo && pm.bError == false).ToList();


                    var pagMasivoMonApr = pagMasivoMonAp.Sum(p=>p.Monto);
                    var NoPro = pagMasivo.iTotalNoProcesado;

                    pagMasivo.iTotalAprobados = pagMasivoApr;
                    pagMasivo.iIdEstatusProceso = 3;
                    pagMasivo.iTotalProcesado = (pagMasivoCant - NoPro);
                    pagMasivo.MontoCobrado = Convert.ToDecimal(pagMasivoMonApr);
                    pagMasivo.dePorcentajeCobrado = Convert.ToDecimal((pagMasivoMonApr/pagMasivo.MontoTotal)*100);
                    pagMasivo.sdFechaFin = DateTime.Now;
                    pagMasivo.sdUltimaActualizacion = DateTime.Now;
                    con.SaveChanges();


                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error mientras se actualizaba la tabla pago masivo.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
               $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(IdPagoMasivo)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        public static PagoKushkiResponse TerminoProcesoSuscripcion(int IdPagoMasivo)
        {
            PagoKushkiResponse respuesta = new PagoKushkiResponse();
            try
            {

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var pagMasivo = con.Cobranza_SuscripcionMasivoKushki
                             .Where(pm => pm.iIdSuscripcionMasivaKushki == IdPagoMasivo)
                             .FirstOrDefault();
                    var pagMasivoCant = con.Cobranza_SuscripcionMasivoDetalleKushki
                             .Where(pm => pm.iIdSuscripcionMasivaKushki == IdPagoMasivo).Count();

                    var pagMasivoApr = con.Cobranza_SuscripcionMasivoDetalleKushki
                            .Where(pm => pm.iIdSuscripcionMasivaKushki == IdPagoMasivo && pm.bError == false).Count();
                    var NoPro = pagMasivo.iTotalNoProcesado;

                    pagMasivo.iTotalAprobados = pagMasivoApr;
                    pagMasivo.iIdEstatusProceso = 3;
                    pagMasivo.iTotalProcesado = (pagMasivoCant- NoPro);
                    pagMasivo.sdFechaFin = DateTime.Now;
                    pagMasivo.sdUltimaActualizacion = DateTime.Now;
                    con.SaveChanges();


                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error mientras se actualizaba la tabla pago masivo.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
              $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(IdPagoMasivo)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        public static string RecuperarIdSuscripcionKushki(string tarjeta)
        {
            string IdSuscripcion = "";
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var idpeti = con.Cobranza_KushkiTarjeta
                             .Where(pm => pm.vNumTarjeta == tarjeta && pm.iIdAccionKushki==1)
                             .FirstOrDefault();

                    var idSus = con.Cobranza_PeticionKushki
                            .Where(pm => pm.iIdPeticionKushki == idpeti.iIdPeticionKushki && pm.bExito==true)
                            .FirstOrDefault();

                    IdSuscripcion = idSus.vIdSuscripcion;

                }
            }
            catch 
            {
              
            }

            return IdSuscripcion;
        }
        #endregion

        #region Metodos privados
        private static PagoKushkiResponse GuardarPagoMasivoSuscripcionDetalle(GeneraPagoMavisokushkiRequest peticion, string mensaje, int IdPeticion)
        {
            PagoKushkiResponse respuesta = new PagoKushkiResponse();
            SuscripcionMasivoDetalleKushki pagMasivoDetalle = new SuscripcionMasivoDetalleKushki();
            try
            {

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    if (mensaje != null)
                    {
                        pagMasivoDetalle.vMensaje = mensaje;
                        pagMasivoDetalle.bError = true;

                    }
                    else
                    {
                        pagMasivoDetalle.vMensaje = "Suscrito Correctamente";
                        pagMasivoDetalle.bError = false;
                    }
                    pagMasivoDetalle.vNoTarjeta = peticion.NumeroTarjeta;
                    pagMasivoDetalle.vMesExpiracion = peticion.MesExpiracion;
                    pagMasivoDetalle.vAnioExpiracion = peticion.AnioExpiracion;
                    pagMasivoDetalle.vNomTarjHabiente = peticion.NombreTarjetahabiente;
                    pagMasivoDetalle.vCVV = peticion.cvv;
                    pagMasivoDetalle.vTipDocumento = peticion.TipoDocumento;
                    pagMasivoDetalle.vDocumento = peticion.Documento;
                    pagMasivoDetalle.vEmail = peticion.email;
                    pagMasivoDetalle.vApeTarjHabiente = peticion.Apellido;
                    pagMasivoDetalle.vTelefono = peticion.Telefono;
                    pagMasivoDetalle.iIdSuscripcionMasivaKushki = peticion.IdPagoMasivo;
                    pagMasivoDetalle.sdFechaRegistro = DateTime.Now;
                    pagMasivoDetalle.iIdPeticion = IdPeticion;
                    con.Cobranza_SuscripcionMasivoDetalleKushki.Add(pagMasivoDetalle);
                    con.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error mientras se actualizaba la tabla Detalle pago masivo.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        public static PagoKushkiResponse GuardarPagoMasivoPagar(GeneraPagoMavisokushkiRequest peticion, string mensaje, int IdPeticion)
        {
            PagoKushkiResponse respuesta = new PagoKushkiResponse();

            PagoMasivoDetalleKushki pagMasivoDetalle = new PagoMasivoDetalleKushki();
            try
            {

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var pagMasivo = con.Cobranza_PagoMasivoKushki
                             .Where(pm => pm.iIdPagosMasivosKushki == peticion.IdPagoMasivo)
                             .FirstOrDefault();

                    // Procesamiento de pago
                          var contP = (pagMasivo.iTotalProcesado + 1);
                        var contA = (pagMasivo.iTotalAprobados + 1);
                        var MontoTot = pagMasivo.MontoTotal;
                    var MontoCobra = pagMasivo.MontoCobrado + Convert.ToDecimal(peticion.Monto);
                    var Porcentaje = ((MontoCobra / MontoTot)*100);
                        if (contP == pagMasivo.iTotalRegistros)
                        {
                            pagMasivo.iIdEstatusProceso = 3;
                        }
                        else
                        {
                            pagMasivo.iIdEstatusProceso = 2;
                        }
                        pagMasivo.sdUltimaActualizacion = DateTime.Now;
                        pagMasivo.iTotalProcesado = contP;
                       
                        if (mensaje == null) {
                             RegistrarPago(IdPeticion);
                             pagMasivo.iTotalAprobados = contA;
                             pagMasivo.MontoCobrado = MontoCobra;
                             pagMasivo.dePorcentajeCobrado = Porcentaje;
                        }
                        con.SaveChanges();
                    
                }
                var Deta = GuardarPagoMasivoPagarDetalle(peticion, mensaje, IdPeticion);
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error mientras se actualizaba la tabla pago masivo.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        private static PagoKushkiResponse GuardarPagoMasivoPagarDetalle(GeneraPagoMavisokushkiRequest peticion, string mensaje, int IdPeticion)
        {
            PagoKushkiResponse respuesta = new PagoKushkiResponse();

            PagoMasivoDetalleKushki pagMasivoDetalle = new PagoMasivoDetalleKushki();
            try
            {

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    if (mensaje != null)
                    {
                        pagMasivoDetalle.vMensaje = mensaje;
                        pagMasivoDetalle.bError = true;

                    }
                    else
                    {
                        pagMasivoDetalle.vMensaje = "Pagado y Completado Correctamente";
                        pagMasivoDetalle.bError = false;
                    }
                    pagMasivoDetalle.vIdProducto = peticion.IdProducto;
                    pagMasivoDetalle.vNoTarjeta = peticion.NumeroTarjeta;
                    pagMasivoDetalle.Monto = Convert.ToDecimal(peticion.Monto);
                    pagMasivoDetalle.vNomTarjHabiente = peticion.NombreTarjetahabiente;
                    pagMasivoDetalle.vTelefono = peticion.Telefono;
                    pagMasivoDetalle.iIdPagosMasivosKushki = peticion.IdPagoMasivo;
                    pagMasivoDetalle.sdFechaRegistro = DateTime.Now;
                    pagMasivoDetalle.iIdPeticion = IdPeticion;
                    con.Cobranza_PagoMasivoDetalleKushki.Add(pagMasivoDetalle);
                    con.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error mientras se actualizaba la tabla Detalle pago masivo.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        private static PagoKushkiResponse GuardarSuscripcion(string url, string jSuscripcionEnvio, GeneraPagoMavisokushkiRequest peticion)
        {
            PagoKushkiResponse respuesta = new PagoKushkiResponse();

            PeticionKushki nuevaPeticion = null;
            KushkiTarjeta tarjeta = null;

            DateTime fechaTransaccion = DateTime.Now;
            int iIdPeticionKushki = 0;
            try
            {
                nuevaPeticion = new PeticionKushki
                {
                    iIdUsuario = peticion.IdUsuario,
                    sdFechaRegistro = DateTime.Now
                };
                // Se registra la petición.
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    con.Cobranza_PeticionKushki.Add(nuevaPeticion);
                    con.SaveChanges();

                    iIdPeticionKushki = nuevaPeticion.iIdPeticionKushki;
                    respuesta.Resultado.IdPeticion = iIdPeticionKushki;
                    respuesta.Resultado.FechaTransaccion = fechaTransaccion;

                    // Guardamos la cadena de petición.
                    nuevaPeticion.vUrl = url;
                    nuevaPeticion.vJsSuscripcionEnvio = jSuscripcionEnvio;
                    nuevaPeticion.bExito = false;
                    con.SaveChanges();

                    //Guardamos datos de la tarjeta
                    tarjeta = new KushkiTarjeta
                    {
                        iIdPeticionKushki = iIdPeticionKushki,
                        vNumTarjeta = peticion.NumeroTarjeta,
                        vMesExpiracion = peticion.MesExpiracion,
                        vAnioExpiracion = peticion.AnioExpiracion,
                        vCVV = peticion.cvv,
                        vNomTarjHabiente = peticion.NombreTarjetahabiente,
                        vTipDocumento = peticion.TipoDocumento,
                        vEmail = peticion.email,
                        vDocumento = peticion.Documento,
                        vApeTarjHabiente = peticion.Apellido,
                        vTelefono = peticion.Telefono,
                        iIdAccionKushki = 1

                    };
                    con.Cobranza_KushkiTarjeta.Add(tarjeta);
                    con.SaveChanges();
                    

                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "EI01";
                respuesta.MensajeOperacion = "Ocurrió un error mientras se procesaba la Suscripcion.";
                respuesta.Resultado.IdPeticion = 0;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        private static PagoKushkiResponse ActualizarGuardarSuscripcion(bool bExito, int IdPeticion, GeneraPagoMavisokushkiRequest peticion, string idSubscripcion, string jSuscripcionRecep)
        {
            PagoKushkiResponse respuesta = new PagoKushkiResponse();
            
            try
            {
               
                // Se actualiza la petición.
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var nuevaPeticion = con.Cobranza_PeticionKushki
                             .Where(pm => pm.iIdPeticionKushki == IdPeticion)
                             .FirstOrDefault();

                    // Guardamos la cadena response de petición.
                    nuevaPeticion.vIdSuscripcion = idSubscripcion;
                    nuevaPeticion.vJsSuscripcionRecep = jSuscripcionRecep;
                    nuevaPeticion.bExito = bExito;
                    con.SaveChanges();



                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "EI01";
                respuesta.MensajeOperacion = "Ocurrió un error mientras se procesaba la Suscripcion.";
                respuesta.Resultado.IdPeticion = 0;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }



        private static PagoKushkiResponse GuargarPago(string url, string jPagoEnvio, GeneraPagoMavisokushkiRequest peticion)
        {
            PagoKushkiResponse respuesta = new PagoKushkiResponse();

            PeticionKushki nuevaPeticion = null;
            KushkiOrden orden = null;

            DateTime fechaTransaccion = DateTime.Now;
            int iIdPeticionKushki = 0;
            try
            {
                nuevaPeticion = new PeticionKushki
                {
                    iIdUsuario = peticion.IdUsuario,
                    sdFechaRegistro = DateTime.Now
                };
                // Se registra la petición.
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    con.Cobranza_PeticionKushki.Add(nuevaPeticion);
                    con.SaveChanges();

                    iIdPeticionKushki = nuevaPeticion.iIdPeticionKushki;
                    respuesta.Resultado.IdPeticion = iIdPeticionKushki;
                    respuesta.Resultado.FechaTransaccion = fechaTransaccion;

                    // Guardamos la cadena de petición.
                    nuevaPeticion.vIdSuscripcion = peticion.subscriptionId;
                    nuevaPeticion.vUrl = url;
                    nuevaPeticion.vJsPagoEnvio = jPagoEnvio;
                    nuevaPeticion.bExito = false;
                    con.SaveChanges();


                    //Guardamos la Orden
                    orden = new KushkiOrden
                    {
                        iIdPeticionKushki = iIdPeticionKushki,
                        vNumCompra = iIdPeticionKushki.ToString(),
                        vIdProducto = peticion.IdProducto,
                        Monto = peticion.Monto,
                        iIdAccionKushki = 2
                    };
                    con.Cobranza_KushkiOrden.Add(orden);
                    con.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "EI02";
                respuesta.MensajeOperacion = "Ocurrió un error mientras se procesaba el Pago.";
                respuesta.Resultado.IdPeticion = 0;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        private static PagoKushkiResponse ActualizarGuargarPago(bool bExito, int IdPeticion, GeneraPagoMavisokushkiRequest peticion, string IdPago, string jPagoRecep)
        {
            PagoKushkiResponse respuesta = new PagoKushkiResponse();
            

            try
            {
                // Se actualiza la petición.
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var nuevaPeticion = con.Cobranza_PeticionKushki
                             .Where(pm => pm.iIdPeticionKushki == IdPeticion)
                             .FirstOrDefault();

                    // Guardamos la cadena de petición.
                    nuevaPeticion.vIdPago = IdPago;
                    nuevaPeticion.vJsPagoRecep = jPagoRecep;
                    nuevaPeticion.bExito = bExito;
                    con.SaveChanges();
                    

                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "EI02";
                respuesta.MensajeOperacion = "Ocurrió un error mientras se procesaba el Pago.";
                respuesta.Resultado.IdPeticion = 0;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }


        private static ProcesarArchivoPagoMasivoRespose ProcesarArchivoPagoMasivo(GenerarPagoKushkiRequest pagos)
        {
            ProcesarArchivoPagoMasivoRespose respuesta = new ProcesarArchivoPagoMasivoRespose();
            PagoMasivoDetalleKushki pago = new PagoMasivoDetalleKushki();

            PagoMasivoKushki pagoMasivo = new PagoMasivoKushki()
            {
                iIdUsuarioRegistro = pagos.IdUsuario,
                sdFechaRegistro = DateTime.Now,
                iIdEstatusProceso = 1, //Cargando
                vNombreArchivo = pagos.NombreDocumento,
                bError = false,
                iTotalAprobados = 0,
                iTotalProcesado = 0,
                iTotalNoProcesado = 0,
                dePorcentajeCobrado=0,
                MontoCobrado=0

            };
            List<PagoMasivoDetalleKushki> pagoMasivoDetalle = new List<PagoMasivoDetalleKushki>();

            //Procesamiento de la carga del archivo.
            #region ProcesoCargaArchivo
            try
            {
                if (pagos.pagos != null)
                {
                    for (int i = 0; i < pagos.NumeroDatos; i++)
                    {
                        pago = new PagoMasivoDetalleKushki();
                        pago.vIdProducto = pagos.pagos[i][0];
                        pago.Monto = Convert.ToDecimal(pagos.pagos[i][1]);
                        pago.vNoTarjeta = pagos.pagos[i][2];
                        pago.vNomTarjHabiente = pagos.pagos[i][3];
                        pago.vTelefono= pagos.pagos[i][4];
                        pagoMasivoDetalle.Add(pago);
                    }
                }
                else
                {
                    pagoMasivo.bError = true;
                    pagoMasivo.vMensaje = "No se encontró información de pagos a procesar.";
                }
            }
            catch (Exception ex)
            {
                pagoMasivo.bError = true;
                pagoMasivo.vMensaje += "Ocurrió un error al procesar la carga del archivo de pagos - " + ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pagos)}", JsonConvert.SerializeObject(ex));
            }
            

            //Guardar el contenido del archivo de carga
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    con.Cobranza_PagoMasivoKushki.Add(pagoMasivo);
                    con.SaveChanges();
                    respuesta.IdPagoMasivo = pagoMasivo.iIdPagosMasivosKushki;
                    
                    pagoMasivo.iTotalRegistros = pagoMasivoDetalle.Count();
                    pagoMasivo.MontoTotal = pagoMasivoDetalle.Sum(p => p.Monto);
                    pagoMasivo.sdUltimaActualizacion = DateTime.Now;
                    con.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
              $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pagos)}", JsonConvert.SerializeObject(ex));
            }
            #endregion



            return respuesta;
        }

        private static ProcesarArchivoPagoMasivoRespose ProcesarArchivoSuscripcionMasivo(GenerarPagoKushkiRequest pagos)
        {
            ProcesarArchivoPagoMasivoRespose respuesta = new ProcesarArchivoPagoMasivoRespose();
            SuscripcionMasivoDetalleKushki pago = new SuscripcionMasivoDetalleKushki();

            SuscripcionMasivoKushki pagoMasivo = new SuscripcionMasivoKushki()
            {
                iIdUsuarioRegistro = pagos.IdUsuario,
                sdFechaRegistro = DateTime.Now,
                iIdEstatusProceso = 1, //Cargando
                vNombreArchivo = pagos.NombreDocumento,
                bError = false,
                iTotalAprobados = 0,
                iTotalProcesado = 0,
                iTotalNoProcesado = 0

            };
            List<SuscripcionMasivoDetalleKushki> pagoMasivoDetalle = new List<SuscripcionMasivoDetalleKushki>();

            //Procesamiento de la carga del archivo.
            #region ProcesoCargaArchivo
            try
            {
                if (pagos.pagos != null)
                {
                    for (int i = 0; i < pagos.NumeroDatos; i++)
                    {
                        pago = new SuscripcionMasivoDetalleKushki();
                        pago.vNoTarjeta = pagos.pagos[i][0];
                        pago.vMesExpiracion = pagos.pagos[i][1];
                        pago.vAnioExpiracion = pagos.pagos[i][2];
                        pago.vNomTarjHabiente = pagos.pagos[i][3];
                        pago.vCVV = pagos.pagos[i][4];
                        pago.vTipDocumento = pagos.pagos[i][5];
                        pago.vDocumento = pagos.pagos[i][6];
                        pago.vEmail = pagos.pagos[i][7];
                        pago.vApeTarjHabiente = pagos.pagos[i][8];
                        pago.vTelefono = pagos.pagos[i][9];
                        pagoMasivoDetalle.Add(pago);
                    }
                }
                else
                {
                    pagoMasivo.bError = true;
                    pagoMasivo.vMensaje = "No se encontró información de pagos a procesar.";
                }
            }
            catch (Exception ex)
            {
                pagoMasivo.bError = true;
                pagoMasivo.vMensaje += "Ocurrió un error al procesar la carga del archivo de pagos - " + ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pagos)}", JsonConvert.SerializeObject(ex));
            }


            //Guardar el contenido del archivo de carga
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    con.Cobranza_SuscripcionMasivoKushki.Add(pagoMasivo);
                    con.SaveChanges();
                    respuesta.IdPagoMasivo = pagoMasivo.iIdSuscripcionMasivaKushki;

                    pagoMasivo.iTotalRegistros = pagoMasivoDetalle.Count();
                    pagoMasivo.sdUltimaActualizacion = DateTime.Now;
                    con.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pagos)}", JsonConvert.SerializeObject(ex));

            }
            #endregion



            return respuesta;
        }

        private static ProcesarArchivoPagoMasivoRespose ProcesarArchivoSuscripcionConErrMasivo(GenerarPagoKushkiRequest pagos)
        {
            ProcesarArchivoPagoMasivoRespose respuesta = new ProcesarArchivoPagoMasivoRespose();
            SuscripcionMasivoDetalleKushki pago = new SuscripcionMasivoDetalleKushki();

            SuscripcionMasivoKushki pagoMasivo = new SuscripcionMasivoKushki()
            {
                iIdUsuarioRegistro = pagos.IdUsuario,
                sdFechaRegistro = DateTime.Now,
                iIdEstatusProceso = 1, //Cargando
                vNombreArchivo = pagos.NombreDocumento,
                bError = false,
                iTotalAprobados = 0,
                iTotalProcesado = 0,
                iTotalRegistros=pagos.NumeroDatos

            };
            List<SuscripcionMasivoDetalleKushki> pagoMasivoDetalleErr = new List<SuscripcionMasivoDetalleKushki>();
            try
            {
                if (pagos.pagosErr != null)
                {
                    for (int i = 0; i < pagos.NumeroDatosCE; i++)
                    {
                        pago = new SuscripcionMasivoDetalleKushki();
                        pago.vNoTarjeta = pagos.pagosErr[i][0];
                        pago.vMesExpiracion = pagos.pagosErr[i][1];
                        pago.vAnioExpiracion = pagos.pagosErr[i][2];
                        pago.vNomTarjHabiente = pagos.pagosErr[i][3];
                        pago.vCVV = pagos.pagosErr[i][4];
                        pago.vTipDocumento = pagos.pagosErr[i][5];
                        pago.vDocumento = pagos.pagosErr[i][6];
                        pago.vEmail = pagos.pagosErr[i][7];
                        pago.vApeTarjHabiente = pagos.pagosErr[i][8];
                        pago.vTelefono = pagos.pagosErr[i][9];
                        pago.bError = true;
                        pago.vMensaje = "Existe error antes de la validación"; ;
                        pagoMasivoDetalleErr.Add(pago);
                    }
                }
                else
                {
                    pagoMasivo.bError = true;
                    pagoMasivo.vMensaje = "No se encontró información de pagos erroneos.";
                }

            }
            catch (Exception ex)
            {
                pagoMasivo.bError = true;
                pagoMasivo.vMensaje += "Ocurrió un error al procesar la carga del archivo de pagos - " + ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pagos)}", JsonConvert.SerializeObject(ex));
            }
            List<SuscripcionMasivoDetalleKushki> pagoMasivoDetalle = new List<SuscripcionMasivoDetalleKushki>();

            //Procesamiento de la carga del archivo.
            #region ProcesoCargaArchivo
            try
            {
                if (pagos.pagos != null)
                {
                    for (int i = 0; i < pagos.NumeroDatosSE; i++)
                    {
                        pago = new SuscripcionMasivoDetalleKushki();
                        pago.vNoTarjeta = pagos.pagos[i][0];
                        pago.vMesExpiracion = pagos.pagos[i][1];
                        pago.vAnioExpiracion = pagos.pagos[i][2];
                        pago.vNomTarjHabiente = pagos.pagos[i][3];
                        pago.vCVV = pagos.pagos[i][4];
                        pago.vTipDocumento = pagos.pagos[i][5];
                        pago.vDocumento = pagos.pagos[i][6];
                        pago.vEmail = pagos.pagos[i][7];
                        pago.vApeTarjHabiente = pagos.pagos[i][8];
                        pago.vTelefono = pagos.pagos[i][9];
                        pagoMasivoDetalle.Add(pago);
                    }
                }
                else
                {
                    pagoMasivo.bError = true;
                    pagoMasivo.vMensaje = "No se encontró información de pagos a procesar.";
                }
            }
            catch (Exception ex)
            {
                pagoMasivo.bError = true;
                pagoMasivo.vMensaje += "Ocurrió un error al procesar la carga del archivo de pagos - " + ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pagos)}", JsonConvert.SerializeObject(ex));
            }


            //Guardar el contenido del archivo de carga
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    con.Cobranza_SuscripcionMasivoKushki.Add(pagoMasivo);
                    con.SaveChanges();
                    respuesta.IdPagoMasivo = pagoMasivo.iIdSuscripcionMasivaKushki;

                    //Guardar detalle de la suscripcion masivo Err
                    foreach (var pagoErro in pagoMasivoDetalleErr)
                    {
                        pagoErro.iIdSuscripcionMasivaKushki = pagoMasivo.iIdSuscripcionMasivaKushki;
                        pagoErro.sdFechaRegistro = DateTime.Now;
                        pagoErro.iIdPeticion = 0;
                        con.Cobranza_SuscripcionMasivoDetalleKushki.Add(pagoErro);
                        con.SaveChanges();
                    }

                    pagoMasivo.iTotalNoProcesado = pagoMasivoDetalleErr.Count();
                    pagoMasivo.sdUltimaActualizacion = DateTime.Now;
                    con.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pagos)}", JsonConvert.SerializeObject(ex));
            }
            #endregion



            return respuesta;
        }

        private static ProcesarArchivoPagoMasivoRespose ProcesarArchivoPagoConErrMasivo(GenerarPagoKushkiRequest pagos)
        {
            ProcesarArchivoPagoMasivoRespose respuesta = new ProcesarArchivoPagoMasivoRespose();
            PagoMasivoDetalleKushki pago = new PagoMasivoDetalleKushki();

            PagoMasivoKushki pagoMasivo = new PagoMasivoKushki()
            {
                iIdUsuarioRegistro = pagos.IdUsuario,
                sdFechaRegistro = DateTime.Now,
                iIdEstatusProceso = 1, //Cargando
                vNombreArchivo = pagos.NombreDocumento,
                bError = false,
                iTotalAprobados = 0,
                iTotalProcesado = 0,
                iTotalRegistros = pagos.NumeroDatos,
                dePorcentajeCobrado = 0,
                MontoCobrado = 0


            };
            List<PagoMasivoDetalleKushki> pagoMasivoDetalleErr = new List<PagoMasivoDetalleKushki>();
            try
            {
                if (pagos.pagosErr != null)
                {
                    for (int i = 0; i < pagos.NumeroDatosCE; i++)
                    {
                        pago = new PagoMasivoDetalleKushki();
                        pago.vIdProducto = pagos.pagosErr[i][0];
                        pago.Monto = Convert.ToDecimal(pagos.pagosErr[i][1]);
                        pago.vNoTarjeta = pagos.pagosErr[i][2];
                        pago.vNomTarjHabiente = pagos.pagosErr[i][3];
                        pago.vTelefono = pagos.pagosErr[i][4];
                        pago.bError = true;
                        pago.vMensaje = "Existe error antes de la validación";
                        pagoMasivoDetalleErr.Add(pago);
                    }
                }
                else
                {
                    pagoMasivo.bError = true;
                    pagoMasivo.vMensaje = "No se encontró información de pagos erroneos.";
                }

            }
            catch (Exception ex)
            {
                pagoMasivo.bError = true;
                pagoMasivo.vMensaje += "Ocurrió un error al procesar la carga del archivo de pagos - " + ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
             $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pagos)}", JsonConvert.SerializeObject(ex));
            }

            List<PagoMasivoDetalleKushki> pagoMasivoDetalle = new List<PagoMasivoDetalleKushki>();

            //Procesamiento de la carga del archivo.
            #region ProcesoCargaArchivo
            try
            {
                if (pagos.pagos != null)
                {
                    for (int i = 0; i < pagos.NumeroDatosSE; i++)
                    {
                        pago = new PagoMasivoDetalleKushki();
                        pago.vIdProducto = pagos.pagos[i][0];
                        pago.Monto = Convert.ToDecimal(pagos.pagos[i][1]);
                        pago.vNoTarjeta = pagos.pagos[i][2];
                        pago.vNomTarjHabiente = pagos.pagos[i][3];
                        pago.vTelefono = pagos.pagos[i][4];
                        pagoMasivoDetalle.Add(pago);
                    }
                }
                else
                {
                    pagoMasivo.bError = true;
                    pagoMasivo.vMensaje = "No se encontró información de pagos a procesar.";
                }
            }
            catch (Exception ex)
            {
                pagoMasivo.bError = true;
                pagoMasivo.vMensaje += "Ocurrió un error al procesar la carga del archivo de pagos - " + ex.Message; 
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pagos)}", JsonConvert.SerializeObject(ex));
            }


            //Guardar el contenido del archivo de carga
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    con.Cobranza_PagoMasivoKushki.Add(pagoMasivo);
                    con.SaveChanges();
                    respuesta.IdPagoMasivo = pagoMasivo.iIdPagosMasivosKushki;
                    //Guardar detalle de la suscripcion masivo Err
                    foreach (var pagoErro in pagoMasivoDetalleErr)
                    {
                        pagoErro.iIdPagosMasivosKushki = pagoMasivo.iIdPagosMasivosKushki;
                        pagoErro.sdFechaRegistro = DateTime.Now;
                        pagoErro.iIdPeticion = 0;
                        con.Cobranza_PagoMasivoDetalleKushki.Add(pagoErro);
                        con.SaveChanges();
                    }

                    var sum1 = pagoMasivoDetalle.Sum(p => p.Monto);
                    var sum2 = pagoMasivoDetalleErr.Sum(p => p.Monto);
                    pagoMasivo.iTotalNoProcesado = pagoMasivoDetalleErr.Count();
                    pagoMasivo.MontoTotal = (sum1+sum2);
                    pagoMasivo.sdUltimaActualizacion = DateTime.Now;
                    con.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                 $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(pagos)}", JsonConvert.SerializeObject(ex));
            }
            #endregion

            return respuesta;
        }

        private static void RegistrarPago(int IdPeticionKushki)
        {
            DataAccess.Cobranza.PagosDAL dal = new DataAccess.Cobranza.PagosDAL();

            dal.RegistrarPagoKushki(new Pago() { IdPeticionKushki = IdPeticionKushki });
        }
        #endregion
    }
}
