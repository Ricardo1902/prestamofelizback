#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.DataAccess.CobranzaAdministrativa;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: GrupoEnvioBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase GrupoEnvioDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.CobranzaAdministrativa
{
    public static class GrupoEnvioBll
    {
        private static GrupoEnvioDal dal;
        private static GrupoEnvioDetalleDal daldet;

        static GrupoEnvioBll()
        {
            dal = new GrupoEnvioDal();
            daldet = new GrupoEnvioDetalleDal();
        }

        public static int InsertarGrupoEnvio(GrupoEnvio entidad, int Accion)
        {
            return dal.InsertarGrupoEnvio(entidad, Accion);			
        }

        public static List<GrupoEnvio> ObtenerGrupoEnvio(int Accion, int idGrupoenvio, string GrupoEnvio)
        {
            return dal.ObtenerGrupoEnvio(Accion, idGrupoenvio, GrupoEnvio);
        }
        
        public static void ActualizarGrupoEnvio()
        {
            dal.ActualizarGrupoEnvio();
        }

        public static void EliminarGrupoEnvio()
        {
            dal.EliminarGrupoEnvio();
        }
    }
}