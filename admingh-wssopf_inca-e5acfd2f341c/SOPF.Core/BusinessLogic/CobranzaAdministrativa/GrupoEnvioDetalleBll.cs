#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.DataAccess.CobranzaAdministrativa;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: GrupoEnvioDetalleBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase GrupoEnvioDetalleDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.CobranzaAdministrativa
{
    public static class GrupoEnvioDetalleBll
    {
        private static GrupoEnvioDetalleDal dal;

        static GrupoEnvioDetalleBll()
        {
            dal = new GrupoEnvioDetalleDal();
        }

        public static string InsertarGrupoEnvioDetalle(int accion, GrupoEnvioDetalle entidad)
        {
            DataResultCobAdm.Resultado respuesta = new DataResultCobAdm.Resultado();
            respuesta = dal.InsertarGrupoEnvioDetalle(accion,entidad);
            return respuesta.Descripcion;
        }
        

        public static GrupoEnvioDetalle ObtenerGrupoEnvioDetalle()
        {
            return dal.ObtenerGrupoEnvioDetalle();
        }
        
        public static void ActualizarGrupoEnvioDetalle()
        {
            dal.ActualizarGrupoEnvioDetalle();
        }

        public static void EliminarGrupoEnvioDetalle()
        {
            dal.EliminarGrupoEnvioDetalle();
        }
    }
}