﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Poco.Cobranza;
using System;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Cobranza
{
    public static class TB_OrigenExclusionBLL
    {
        private static CreditOrigContext con;
        static TB_OrigenExclusionBLL()
        {
            con = new CreditOrigContext();
        }

        #region Métodos públicos
        public static TB_OrigenExclusion OrigenExclusionCargaExclusiones()
        {
            return OrigenExclusion("Carga de exclusiones");
        }

        #endregion

        #region Métodos privados
        private static TB_OrigenExclusion OrigenExclusion(string origen)
        {
            TB_OrigenExclusion origenExclusion = null;
            try
            {
                origenExclusion = con.Cobranza_TB_OrigenExclusion.Where(oe => oe.Origen == origen).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(origen)}", JsonConvert.SerializeObject(ex));
            }
            return origenExclusion;
        }
        #endregion
    }
}
