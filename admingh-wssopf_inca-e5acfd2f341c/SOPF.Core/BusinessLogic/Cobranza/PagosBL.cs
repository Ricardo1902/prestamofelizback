﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.DataAccess.Cobranza;
using SOPF.Core.DataAccess.Creditos;
using SOPF.Core.Entities.Creditos;

namespace SOPF.Core.BusinessLogic.Cobranza
{   
    public static class PagosBL
    {
        private static PagosDAL dal;
        private static TBCreditosDal dalCreditos;

        static PagosBL()
        {
            dal = new PagosDAL();
            dalCreditos = new TBCreditosDal();
        }

        public static ResultadoDetallePagoMasivo ConsultarPagosMasivos(DetallePagoMasivoCondiciones condiciones)
        {
            return dal.ConsultarPagosMasivos(condiciones);
        }

        public static List<ProcesoPagosAplicacion> ConsultarProcesosPagosAplicacion(ProcesoPagosAplicacionCondiciones condiciones)
        {
            return dal.ConsultarProcesosPagosAplicacion(condiciones);
        }

        public static List<ProcesoPagosAplicacionDetalle> ConsultarProcesoPagosAplicacionDetalle(ProcesoPagosAplicacionDetalleCondiciones condiciones)
        {
            return dal.ConsultarProcesoPagosAplicacionDetalle(condiciones);
        }

        public static int ProcesoPagosAplicacionAlta(ProcesoPagosAplicacion proceso)
        {
            return dal.ProcesoPagosAplicacionAlta(proceso);
        }

        public static Resultado<bool> Proceso_AplicarPago(AplicarPagoParametros parametros)
        {
            return dal.Proceso_AplicarPago(parametros);
        }

        public static Resultado<bool> Proceso_DesaplicarPago(DesaplicarPagoParametros parametros)
        {
            return dal.Proceso_DesaplicarPago(parametros);
        }

        public static List<Pago> Filtrar(Pago Entity)
        {
            List<Pago> objReturn = new List<Pago>();

            objReturn = dal.Filtrar(Entity);

            return objReturn;
        }

        public static Resultado<bool> HardResetCobranzaCuenta(int IdSolicitud)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            objReturn = dal.HardResetCobranzaCuenta(IdSolicitud);

            return objReturn;
        }

        public static Resultado<bool> AplicarPago(Pago Entity, bool AplicarUnico)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            objReturn = dal.AplicarPago(Entity, AplicarUnico);

            return objReturn;
        }

        public static Resultado<bool> DesaplicarPago(Pago Entity)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            objReturn = dal.DesaplicarPago(Entity);

            return objReturn;
        }

        public static Resultado<bool> CancelarPago(Pago Entity)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            objReturn = dal.CancelarPago(Entity);

            return objReturn;
        }

        public static Resultado<bool> DevolverPago(Pago Entity)
        {
            Resultado<bool> objReturn = new Resultado<bool>();
            objReturn = dal.DevolverPago(Entity);
            return objReturn;
        }

        public static Resultado<bool> LiquidarPago(Pago Entity, int IdUsuario)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            TBCreditos.Liquidacion entidadLiquidar = new TBCreditos.Liquidacion()
            {
                IdPago = Entity.IdPago,
                IdUsuarioConfirma = IdUsuario
            };

            Resultado<TBCreditos.Liquidacion> res = dalCreditos.ConfirmarLiquidacionManual(entidadLiquidar);

            if(res.Codigo >= 1)
            {
                objReturn.ResultObject = res.Codigo == 0;
                objReturn.Codigo = res.Codigo;
                objReturn.Mensaje = res.Mensaje;
            }
            else
            {
                objReturn = dal.LiquidarPago(Entity);
            }

            return objReturn;
        }

        public static Resultado<bool> RegistrarPago(Pago Entity, int IdUsuario)
        {
            Resultado<bool> objReturn = new Resultado<bool>();
            objReturn = dal.RegistrarPago(Entity, IdUsuario);
            return objReturn;
        }

        public static Resultado<bool> ActualizarCanalPago(Pago Entity)
        {
            return dal.ActualizarCanalPago(Entity);
        }

        public static List<Pagos.ChartCartera> ChartCartera(int Anio, int MesInicio, int MesFin)
        {
            List<Pagos.ChartCartera> objReturn = new List<Pagos.ChartCartera>();

            objReturn = dal.ChartCartera(Anio, MesInicio, MesFin);

            return objReturn;
        }

        public static List<BalanceCuenta> Balance_Filtrar(BalanceCuenta entity)
        {
            List<BalanceCuenta> objReturn = new List<BalanceCuenta>();

            objReturn = dal.Balance_Filtrar(entity);

            return objReturn;
        }

        public static BalanceCuenta Balance_Obtener(BalanceCuenta entity)
        {
            BalanceCuenta objReturn = new BalanceCuenta();

            objReturn = dal.Balance_Obtener(entity);

            return objReturn;
        }

        public static List<BalanceCuenta.Recibos> Balance_Obtener_Recibos(BalanceCuenta entity)
        {
            List<BalanceCuenta.Recibos> objReturn = new List<BalanceCuenta.Recibos>();

            objReturn = dal.Balance_Obtener_Recibos(entity);

            return objReturn;
        }
    }
}
