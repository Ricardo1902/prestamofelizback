﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Cobranza;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Poco.Cobranza;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace SOPF.Core.BusinessLogic.Cobranza
{
    public static class PlantillaBL
    {
        private static PlantillaDAL dal;

        #region Métodos públicos

        static PlantillaBL()
        {
            dal = new PlantillaDAL();
        }

        public static Resultado<Plantilla> Guardar(Plantilla objEntity)
        {
            Resultado<Plantilla> objReturn = new Resultado<Plantilla>();

            // Verificar si es un Alta o Actualizacion
            if (objEntity.IdPlantilla == null)
                objReturn = Alta(objEntity);
            else
                objReturn = Actualizar(objEntity);

            // Guardar Empresas/Buckets
            if (objReturn.Codigo == 0)
            {
                Guardar_ConfiguracionEmpresas(objReturn.ResultObject);
                Guardar_SituacionLaboral(objEntity);
            }

            return objReturn;
        }

        public static Resultado<Plantilla> Obtener(int IdPlantilla)
        {
            Resultado<Plantilla> objReturn = new Resultado<Plantilla>();
            Resultado<List<Plantilla>> res = dal.Filtrar(new Plantilla() { IdPlantilla = IdPlantilla });

            // Obtener la Plantilla
            if (res.Codigo == 0)
            {
                if (res.ResultObject.Count() <= 0)
                {
                    objReturn.Codigo = res.Codigo;
                    objReturn.Mensaje = string.Format("No se encontro la Plantilla con ID[{0}]", IdPlantilla.ToString());
                }
                else
                    objReturn.ResultObject = res.ResultObject.First();
            }
            else
            {
                objReturn.Codigo = res.Codigo;
                objReturn.Mensaje = res.Mensaje;
            }

            // Obtener las Situaciones Laborales de la Plantilla
            if (objReturn.Codigo == 0)
            {
                Resultado<List<TBCATSituacionLaboral_PE>> resSL = Cargar_SituacionLaboral(objReturn.ResultObject.IdPlantilla.GetValueOrDefault());

                if (resSL.Codigo > 0)
                {
                    objReturn.Codigo = resSL.Codigo;
                    objReturn.Mensaje = resSL.Mensaje;
                }
                else
                    objReturn.ResultObject.SituacionLaboral = resSL.ResultObject;
            }

            // Obtener los Empresas/Buckets
            if (objReturn.Codigo == 0)
            {
                Resultado<List<Plantilla.EmpresaConfiguracion>> resCC = Cargar_Empresas(objReturn.ResultObject.IdPlantilla.GetValueOrDefault());

                if (resCC.Codigo > 0)
                {
                    objReturn.Codigo = resCC.Codigo;
                    objReturn.Mensaje = resCC.Mensaje;
                }
                else
                    objReturn.ResultObject.Empresas = resCC.ResultObject;
            }

            return objReturn;
        }

        public static Resultado<List<Plantilla>> Buscar(Plantilla objEntity)
        {
            return dal.Filtrar(objEntity);
        }

        public static GenerarPlantillaEnvioCobroResponse GenerarPlantillaEnvioCobro(GenerarPlantillaEnvioCobroRequest peticion)
        {
            GenerarPlantillaEnvioCobroResponse respuesta = new GenerarPlantillaEnvioCobroResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta!");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (peticion.IdPlantilla <= 0) throw new ValidacionExcepcion("La clave de la plantilla no es válida.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var resp = con.Cobranza_SP_PlantillaEnvioCobroPRO(peticion.IdUsuario, peticion.IdPlantilla);
                    if (resp != null)
                    {
                        respuesta.Error = !resp.OperacionCorrecta;
                        respuesta.MensajeOperacion = resp.MensajeOperacion;
                        if (resp.OperacionCorrecta) respuesta.Resultado.IdPlantillaEnvioCobro = resp.IdPlantillaEnvioCobro;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static DescargaArchivoDomiciliacionResponse DescargaArchivoDomiciliacion(GenerarArchivoDomiciliacionRequest peticion)
        {
            DescargaArchivoDomiciliacionResponse respuesta = new DescargaArchivoDomiciliacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida.");
                if (peticion.IdPlantillaEnvioCobro <= 0) throw new ValidacionExcepcion("La clave del archivo de domiciliacion no es válido.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var plantilla = (from p in con.Cobranza_TB_Plantilla
                                     join tl in con.Cobranza_TB_CATTipoLayoutCobro on p.IdTipoLayout equals tl.IdTipoLayoutCobro
                                     join la in con.Cobranza_TB_LayoutArchivo on tl.IdLayoutArchivo equals la.IdLayoutArchivo
                                     join pe in con.Cobranza_TB_PlantillaEnvioCobro on p.IdPlantilla equals pe.IdPlantilla
                                     where pe.IdPlantillaEnvioCobro == peticion.IdPlantillaEnvioCobro
                                        && p.Activo == true
                                     //&& pe.ArchivoGenerado == false
                                     select new
                                     {
                                         p.IdPlantilla,
                                         p.IdTipoLayout,
                                         tl.IdTipoLayoutCobro,
                                         tl.IdBanco,
                                         tl.IdLayoutArchivo,
                                         pe.IdPlantillaEnvioCobro,
                                         pe.TotalRegistros,
                                         pe.MontoTotal,
                                         pe.FechaRegistro
                                     })
                                     .FirstOrDefault();
                    if (plantilla == null) throw new ValidacionExcepcion("No se encontró una configuración activa para la plantilla de domiciliacion.");

                    List<TB_PlantillaEnvioCobroDetalle> plantillaEnvioDetalle = con.Cobranza_TB_PlantillaEnvioCobroDetalle
                        .Where(ped => ped.IdPlantillaEnvioCobro == plantilla.IdPlantillaEnvioCobro
                            //&& ped.EnArchivo == false
                            )
                        .ToList();
                    if (plantillaEnvioDetalle == null) throw new ValidacionExcepcion("No existen registros para generar el archivo de domiciliación.");

                    respuesta = LayoutArchivoBLL.GenerarArchivoDomiciliacion<object, TB_PlantillaEnvioCobroDetalle, object, object>(plantilla.IdLayoutArchivo ?? 0, plantilla, plantillaEnvioDetalle, null, null);
                    new Thread(() => ActualizarPlantillaEnvioCobroConteoGeneracion(peticion.IdPlantillaEnvioCobro, (plantilla.IdLayoutArchivo ?? 0), peticion.IdUsuario))
                    {
                        IsBackground = true
                    }.Start();
#if DEBUG
                    new Thread(() => GuardarArchivo(respuesta))
                    {
                        IsBackground = true
                    }.Start();
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar el archivo de domiciliacion.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static Resultado<List<int>> ObtenerDiasEjecucion(int IdPlantilla)
        {
            return dal.ObtenerDiasEjecucion(IdPlantilla);
        }

        #endregion

        #region "Metodos Privados"

        private static Resultado<Plantilla> Alta(Plantilla objEntity)
        {
            return dal.Alta(objEntity);
        }

        private static Resultado<Plantilla> Actualizar(Plantilla objEntity)
        {
            return dal.Actualizar(objEntity);
        }

        private static void Guardar_ConfiguracionEmpresas(Plantilla objEntity)
        {
            // Eliminar los Buckets actuales de la Plantilla
            dal.EliminarEmpresaBuckets(objEntity.IdPlantilla.GetValueOrDefault());

            // Guardar los Buckets actualizados de la Plantilla
            foreach (Plantilla.EmpresaConfiguracion c in objEntity.Empresas)
                foreach (Plantilla.EmpresaConfiguracion.BucketConfiguracion b in c.Buckets)
                    dal.GuardarEmpresaBuckets(objEntity.IdPlantilla.GetValueOrDefault(), c.Empresa.IdEmpresa, b);

        }

        private static void Guardar_SituacionLaboral(Plantilla objEntity)
        {
            if (dal.EliminarSituacionLaboral(objEntity.IdPlantilla.GetValueOrDefault()).Codigo == 0)
            {
                foreach (TBCATSituacionLaboral_PE sl in objEntity.SituacionLaboral)
                    dal.GuardarSituacionLaboral(objEntity.IdPlantilla.GetValueOrDefault(), sl.IdSituacion);
            }
        }
        
        private static Resultado<List<TBCATSituacionLaboral_PE>> Cargar_SituacionLaboral(int IdPlantilla)
        {
            return dal.ObtenerSituacionLaboral(IdPlantilla);
        }

        private static Resultado<List<Plantilla.EmpresaConfiguracion>> Cargar_Empresas(int IdPlantilla)
        {
            Resultado<List<Plantilla.EmpresaConfiguracion>> res = dal.ObtenerEmpresas(IdPlantilla);

            // Validar error al obtener Empresas
            if (res.Codigo == 0)
            {
                // Recorrer los Empresas
                foreach (Plantilla.EmpresaConfiguracion cc in res.ResultObject)
                {
                    // Obtener los buckets de la Empresa Actual
                    Resultado<List<Plantilla.EmpresaConfiguracion.BucketConfiguracion>> resBC = dal.ObtenerBucketEmpresa(IdPlantilla, cc.Empresa.IdEmpresa);

                    if (resBC.Codigo == 0)
                    {
                        // Asignar los Buckets a la Empresa Actual
                        cc.Buckets = resBC.ResultObject;

                        // Recorrer los Buckets para cargar las Partidas
                        foreach (Plantilla.EmpresaConfiguracion.BucketConfiguracion bc in cc.Buckets)
                        {
                            // Obtener las partidas del Bucket Actual
                            Resultado<List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion>> resPC = dal.ObtenerPartidasBucket(IdPlantilla, cc.Empresa.IdEmpresa, bc.Bucket.IdBucket);

                            bc.Partidas = resPC.ResultObject;
                        }
                    }
                    else
                    {
                        res.Codigo = resBC.Codigo;
                        res.Mensaje = resBC.Mensaje;
                        break;
                    }
                }
            }

            return res;
        }

        private static void ActualizarPlantillaEnvioCobroConteoGeneracion(int idPlantillaEnvioCobro, int idLayoutArchivo, int idUsuario)
        {
            try
            {
                if (idPlantillaEnvioCobro > 0)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        TB_PlantillaEnvioCobro archivoEnvioCobro = con.Cobranza_TB_PlantillaEnvioCobro
                            .Where(p => p.IdPlantillaEnvioCobro == idPlantillaEnvioCobro)
                            .FirstOrDefault();
                        if (archivoEnvioCobro != null)
                        {
                            if (archivoEnvioCobro.ArchivoGenerado == false)
                            {
                                archivoEnvioCobro.ArchivoGenerado = true;
                            }
                            archivoEnvioCobro.VecesGenerado = (archivoEnvioCobro.VecesGenerado ?? 0) + 1;

                            TB_PlantillaEnvioCobroDescarga descarga = new TB_PlantillaEnvioCobroDescarga
                            {
                                IdPlantillaEnvioCobro = idPlantillaEnvioCobro,
                                IdLayoutArchivo = idLayoutArchivo,
                                IdUsuarioDescarga = idUsuario,
                                FechaDescarga = DateTime.Now
                            };
                            con.Cobranza_TB_PlantillaEnvioCobroDescarga.Add(descarga);
                        }
                        con.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"Error: {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                       $"{Environment.NewLine}Datos: idPlantillaEnvioCobro:{"idPlantillaEnvioCobro"}", JsonConvert.SerializeObject(ex));
            }
        }

        private static void GuardarArchivo(DescargaArchivoDomiciliacionResponse resultado)
        {
            try
            {
                if (resultado.Resultado != null && resultado.Resultado.ContenidoArchivo != null
                    && resultado.Resultado.ContenidoArchivo.Length > 0)
                {
                    string rutaArchivo = Path.Combine(Utils.AppSettings.DirectorioLog, "WsSOPF_Inca", $"{resultado.Resultado.NombreArchivo}.{resultado.Resultado.Extension.Replace(".", "")}");
                    if (!Directory.Exists(Path.GetDirectoryName(rutaArchivo)))
                        Directory.CreateDirectory(Path.GetDirectoryName(rutaArchivo));
                    File.WriteAllBytes(rutaArchivo, resultado.Resultado.ContenidoArchivo);
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"Error: {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
        }
        
        #endregion
    }
}

