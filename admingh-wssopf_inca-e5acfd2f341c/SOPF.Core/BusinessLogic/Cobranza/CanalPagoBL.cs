﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.DataAccess.Cobranza;

namespace SOPF.Core.BusinessLogic.Cobranza
{   
    public static class CanalPagoBL
    {
        private static CanalPagoDAL dal;

        static CanalPagoBL()
        {
            dal = new CanalPagoDAL();
        }

        public static List<CanalPago> ListarTodos()
        {
            return dal.ListarTodos();
        }

        public static List<CanalPago> ListarActivos()
        {
            return dal.ListarActivos();
        }

        public static List<CanalPago> ListarInactivos()
        {
            return dal.ListarInactivos();
        }               
    }
}
