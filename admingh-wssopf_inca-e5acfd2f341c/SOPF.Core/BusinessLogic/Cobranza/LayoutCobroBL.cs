﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.DataAccess.Cobranza;

namespace SOPF.Core.BusinessLogic.Cobranza
{   
    public static class LayoutCobroBL
    {
        private static LayoutCobroDAL dal;

        static LayoutCobroBL()
        {
            dal = new  LayoutCobroDAL();
        }


        #region "TipoLayoutCobro"

        public static List<TipoLayoutCobro> TipoLayoutCobro_ListarTodos()
        {
            return dal.TipoLayoutCobro_ListarTodos();
        }

        public static List<TipoLayoutCobro> TipoLayoutCobro_ListarActivos()
        {
            return dal.TipoLayoutCobro_ListarActivos();
        }

        public static List<TipoLayoutCobro> TipoLayoutCobro_ListarInactivos()
        {
            return dal.TipoLayoutCobro_ListarInactivos();
        }

        #endregion


        #region "LayoutGenerado"

        public static List<LayoutCobro.LayoutGenerado> LayoutGenerado_Filtrar(LayoutCobro.LayoutGenerado eFiltrar)
        {           
            return dal.LayoutGenerado_Filtrar(eFiltrar);
        }

        public static LayoutCobro.LayoutGenerado LayoutGenerado_ObtenerDetalle(int Id)
        {
            return dal.LayoutGenerado_ObtenerDetalle(Id);
        }

        #endregion

        public static List<LayoutCobro.LayoutSumatoria> LayoutCobro_Suma(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago)
        {
            return dal.LayoutCobro_Suma(fchCreditoDesde, fchCreditoHasta, ddlCanalPago);
        }

        public static List<LayoutCobro.LayoutNetCash> Obtener_LayoutCobroCobro_NetCash(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago)
        {
            return dal.ObtenerDatos_LayoutCobro_NetCash(fchCreditoDesde, fchCreditoHasta, ddlCanalPago);
        }

        public static List<LayoutCobro.LayoutVisaNet> Obtener_LayoutCobro_VisaNet(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago)
        {
            return dal.ObtenerDatos_LayoutCobro_VisaNet(fchCreditoDesde, fchCreditoHasta, ddlCanalPago);
        }

        public static List<LayoutCobro.LayoutInterbank> Obtener_LayoutCobro_Interbank(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago)
        {
            return dal.ObtenerDatos_LayoutCobro_Interbank(fchCreditoDesde, fchCreditoHasta, ddlCanalPago);
        }
    }
}
