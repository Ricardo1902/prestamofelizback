﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Poco.Cobranza;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Cobranza
{
    public static class TB_TipoIngresoBLL
    {
        private static CreditOrigContext con;
        static TB_TipoIngresoBLL()
        {
            con = new CreditOrigContext();
        }

        #region Métodos publicos
        public static TB_TipoIngreso TipoIngresoManual()
        {
            return ObtenerTipoIngreso("Ingreso Manual");
        }

        public static TB_TipoIngreso ObtenerTipoIngreso(string tipo)
        {
            TB_TipoIngreso tipoIngreso = null;
            try
            {
                TB_TipoIngreso resp = ObtenerTipoIngreso(ti => ti.Tipo == tipo).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(tipo)}", JsonConvert.SerializeObject(ex));
            }
            return tipoIngreso;
        }
        #endregion

        #region Métodos Privados
        private static IQueryable<TB_TipoIngreso> ObtenerTipoIngreso(Expression<Func<TB_TipoIngreso, bool>> predicado)
        {
            var consulta = (from con in con.Cobranza_TB_TipoIngreso
                            select con).Where(predicado);
            return consulta;
        }
        #endregion
    }
}
