﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Poco.Cobranza;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace SOPF.Core.BusinessLogic.Cobranza
{
    public static class PeticionDomiciliacionBLL
    {
        public static PeticionDomiciliacionResponse PeticionDomiciliacion(PeticionDomiciliacionRequest peticion)
        {
            PeticionDomiciliacionResponse respuesta = new PeticionDomiciliacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (peticion.IdLayuotArchivo <= 0) throw new ValidacionExcepcion("La clave del layout a construir no es válido");
                if (string.IsNullOrEmpty(peticion.OrigenDomiciliacion)) throw new ValidacionExcepcion("Proporcione el origen de la domiciliacion.");
                if (peticion.SolicitudesDomiciliar == null || peticion.SolicitudesDomiciliar.Count == 0)
                    throw new ValidacionExcepcion("Proporcione las cuentas a domiciliar");

                bool esProductivo = AppSettings.EsProductivo;

                XElement solicitudes = new XElement("Domiciliaciones", peticion.SolicitudesDomiciliar.Select(c =>
                        new XElement("Solicitud",
                            new XElement("IdSolicitud", c.IdSolicitud),
                            new XElement("Monto", esProductivo ? c.Monto : 0.01m))));

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Cobranza_SP_PeticionDomiciliacionALT(new PeticionDomiciliacionAltRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        IdLayuotArchivo = peticion.IdLayuotArchivo,
                        OrigenDomiciliacion = peticion.OrigenDomiciliacion,
                        Solicitudes = solicitudes
                    });
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static DescargaPeticionDomiciliacionResponse DescargaPeticionDomiciliacion(DescargaPeticionDomiciliacionRequest peticion)
        {
            DescargaPeticionDomiciliacionResponse respuesta = new DescargaPeticionDomiciliacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdPeticionDomiciliacion >= 0)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        TB_PeticionDomiciliacion peticionDom = con.Cobranza_TB_PeticionDomiciliacion.Where(pd => pd.IdPeticionDomiciliacion == peticion.IdPeticionDomiciliacion).FirstOrDefault();
                        if (peticionDom != null)
                        {
                            peticionDom.VecesDescargado++;
                            con.SaveChanges();
                        }
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
    }
}
