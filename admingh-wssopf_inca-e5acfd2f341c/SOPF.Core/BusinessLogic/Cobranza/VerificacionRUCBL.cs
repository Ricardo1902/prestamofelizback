﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.DataAccess.Cobranza;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Configuration;
using SOPF.Core.DataAccess;
using SOPF.Core.Poco.Catalogo;

namespace SOPF.Core.BusinessLogic.Cobranza
{   
    public class VerificacionRUCBL
    {
        #region Constructor

        public VerificacionRUCBL()
        {
        }
        #endregion

        public static VerificacionRUCResponse VerificarRUC(VerificacionRUCRequest verificar)
        {

             VerificacionRUCResponse respuesta = new VerificacionRUCResponse();
            string token = ConfigurationManager.AppSettings.Get("tokenApiPeru");
            string url = "https://apiperu.dev/api/ruc/"+verificar.RUC+"?api_token="+ token;
            var client = new HttpClient();
            
            var httpResponse = client.GetAsync(url).Result;
            var result = httpResponse.Content.ReadAsStringAsync().Result;
            var GetResult = JsonConvert.DeserializeObject<VerificacionRUCResponse>(result);
            respuesta = GetResult;
            if (respuesta.message != null)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = respuesta.message;
            }
            return respuesta;

        }
        public static VerificacionRUCResponse GuardarRUC(VerificacionRUCRequest verificar)
        {

            VerificacionRUCResponse respuesta = new VerificacionRUCResponse();

            TablaEmpresa tabempresa = null;
            
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    tabempresa = new TablaEmpresa()
                    {

                        RUC = verificar.RUC,
                        IdUsuarioRegistro = verificar.IdUsuario,
                        FechaRegistro = DateTime.Now,
                        Empresa = verificar.NombreEmp,
                        Activo = verificar.Estado == "ACTIVO" ? true : false,
                        iTipEmpresa = verificar.TipoEmp == "PRIVADA" ? 1 : 0
                    };
                    con.Catalogo_TB_CATEmpresa.Add(tabempresa);
                    con.SaveChanges();



                }
            }
            catch (Exception e)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error mientras se guardaba la Empresa.";
            }

            return respuesta;

        }
    }
}
