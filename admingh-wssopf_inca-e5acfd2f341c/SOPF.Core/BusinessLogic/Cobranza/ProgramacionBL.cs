﻿using Newtonsoft.Json;
using SOPF.Core.BusinessLogic.Catalogo;
using SOPF.Core.BusinessLogic.CobranzaAdministrativa;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Cobranza;
using SOPF.Core.DataAccess.CobranzaAdministrativa;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.Model;
using SOPF.Core.Model.Cobranza;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Poco.Catalogo;
using SOPF.Core.Poco.Cobranza;
using SOPF.Core.Poco.dbo;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Cobranza
{
    public static class ProgramacionBL
    {
        private static ProgramacionDAL dal;

        static ProgramacionBL()
        {
            dal = new ProgramacionDAL();
        }

        public static Resultado<GrupoLayout.ProgramacionGrupoLayout> Obtener(int IdProgramacion)
        {
            return dal.Obtener(IdProgramacion);
        }

        public static Resultado<GrupoLayout.ProgramacionGrupoLayout> Guardar(GrupoLayout.ProgramacionGrupoLayout objEntity)
        {
            Resultado<GrupoLayout.ProgramacionGrupoLayout> objReturn = new Resultado<GrupoLayout.ProgramacionGrupoLayout>();
            if (objEntity.IdProgramacion <= 0)
                objReturn = Alta(objEntity);
            else
                objReturn = Actualizar(objEntity);
            return objReturn;
        }

        private static Resultado<GrupoLayout.ProgramacionGrupoLayout> Alta(GrupoLayout.ProgramacionGrupoLayout objEntity)
        {
            return dal.Alta(objEntity);
        }

        private static Resultado<GrupoLayout.ProgramacionGrupoLayout> Actualizar(GrupoLayout.ProgramacionGrupoLayout objEntity)
        {
            return dal.Actualizar(objEntity);
        }

        public static TB_CATParametro ObtenerMaximoCobrosProgramaciones(string Clave)
        {
            TB_CATParametro parametro = null;
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    parametro = con.dbo_TB_CATParametro.Where(p => p.Parametro.Equals(Clave, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(Clave)}", JsonConvert.SerializeObject(ex));
            }
            return parametro;
        }

        public static Respuesta ValidaVigencias(int idUsuario)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                if (idUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es valido.");
                DateTime fechaActual = DateTime.Now.Date;
                DateTime horaActual = DateTime.Now;
                List<TB_ProgramacionEstatus> procesosEjecucion = ProgramacionEstatusBLL.ProgramacionEstatus(new ProgramacionEstatusRequest { });
                int idProcesoEjecucion = 2;
                int idProcesoExpirado = 4;
                if (procesosEjecucion != null && procesosEjecucion.Count > 0)
                {
                    idProcesoEjecucion = procesosEjecucion.Where(p => p.ClaveProgramacion == "PE").Select(p => p.IdProgramacionEstatus).DefaultIfEmpty(2).FirstOrDefault();
                    idProcesoExpirado = procesosEjecucion.Where(p => p.ClaveProgramacion == "PEX").Select(p => p.IdProgramacionEstatus).DefaultIfEmpty(4).FirstOrDefault();
                }

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<TB_GrupoLayoutProgramacionDetalle> programaciones = con.Cobranza_TB_GrupoLayoutProgramacionDetalle
                        .Where(p => p.Activo
                            && p.IdProgramacionEstatus != idProcesoEjecucion
                            && p.FechaTermino <= fechaActual).ToList();
                    if (programaciones != null && programaciones.Count > 0)
                    {
                        foreach (TB_GrupoLayoutProgramacionDetalle p in programaciones.Where(p => CombinarFechaHora(p.FechaTermino, p.HoraTermino) <= horaActual))
                        {
                            p.Activo = false;
                            p.IdProgramacionEstatus = idProcesoExpirado;
                        }
                        con.SaveChanges();
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(idUsuario)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ProgramacionesProximasResponse ProgramacionesProximas(ProgramacionesProximasRequest peticion)
        {
            ProgramacionesProximasResponse respuesta = new ProgramacionesProximasResponse();
            try
            {
                DateTime diaActual = DateTime.Now.Date;
                DateTime tiempoProximo = DateTime.Now;
                #region validaciones
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es valido.");
                if (peticion.HorasProximas <= 0 && peticion.MinutosProximos <= 0)
                {
                    peticion.MinutosProximos = 1;
                }
                if (peticion.MinutosProximos > 0)
                    tiempoProximo = tiempoProximo.AddMinutes(peticion.MinutosProximos);
                if (peticion.HorasProximas > 0)
                    tiempoProximo = tiempoProximo.AddHours(peticion.HorasProximas);
                #endregion

                List<TB_ProgramacionEstatus> procesosEjecucion = ProgramacionEstatusBLL.ProgramacionEstatus(new ProgramacionEstatusRequest { ClaveProgramacion = "P" });
                int idProcesoProgramado = 1;
                if (procesosEjecucion != null && procesosEjecucion.Count > 0) idProcesoProgramado = procesosEjecucion[0].IdProgramacionEstatus;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<ProgramacionDomiciliacionVM> programaciones = (from p in con.Cobranza_TB_GrupoLayoutProgramacion
                                                                        join pd in con.Cobranza_TB_GrupoLayoutProgramacionDetalle on p.IdProgramacion equals pd.IdProgramacion
                                                                        join tp in con.Catalogo_TB_CATTipoProgramacion on p.IdTipoProgramacion equals tp.IdTipoProgramacion
                                                                        join i in con.Catalogo_TB_CATProgIntervalos on pd.IdIntervalo equals i.IdIntervalo
                                                                        join pdi in con.Cobranza_TB_GrupoLayoutProgramacionDias on p.IdProgramacion equals pdi.IdProgramacion into pdil
                                                                        from lpdi in pdil.DefaultIfEmpty()
                                                                        where p.Activo
                                                                          && pd.Activo
                                                                          && pd.IdProgramacionEstatus == idProcesoProgramado
                                                                          && diaActual >= DbFunctions.TruncateTime(pd.FechaInicio)
                                                                          && diaActual <= DbFunctions.TruncateTime(pd.FechaTermino)
                                                                        select new ProgramacionDomiciliacionVM
                                                                        {
                                                                            IdProgramacionDetalle = pd.IdProgramacionDetalle,
                                                                            IdProgramacion = p.IdProgramacion,
                                                                            FechaInicio = pd.FechaInicio,
                                                                            FechaTermino = pd.FechaTermino,
                                                                            HoraInicio = pd.HoraInicio,
                                                                            HoraTermino = pd.HoraTermino,
                                                                            IdIntervalo = pd.IdIntervalo,
                                                                            ValorIntervalo = pd.ValorIntervalo,
                                                                            Intento = pd.Intentos,
                                                                            Dia = lpdi == null ? 0 : lpdi.Dia
                                                                        }).ToList();

                    if (programaciones != null && programaciones.Count > 0)
                    {
                        DateTime tiempoActual = DateTime.Now;
                        respuesta.Programaciones = new List<ProgramacionDomiciliacionVM>();
                        foreach (ProgramacionDomiciliacionVM p in programaciones)
                        {
                            p.FechaProcesar = diaActual;
                            if (p.Dia > 0 && p.Dia < 32)
                            {
                                p.FechaProcesar = new DateTime(diaActual.Year, diaActual.Month, p.Dia);
                            }
                            else if (p.Dia >= 32)
                            {
                                p.FechaProcesar = new DateTime(diaActual.Year, diaActual.Month, 1).AddMonths(1).AddDays(-1);
                            }

                            p.FechaProcesar = CombinarFechaHora(p.FechaProcesar, p.HoraInicio);
                            if (p.FechaProcesar >= tiempoActual && p.FechaProcesar <= tiempoProximo)
                            {
                                respuesta.Programaciones.Add(p);
                            }
                        }
                        respuesta.TotalRegistros = respuesta.Programaciones.Count;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ProgramacionEstatusResponse ProgramacionEstatus(ProgramacionEstatusRequest peticion)
        {
            ProgramacionEstatusResponse respuesta = new ProgramacionEstatusResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                List<TB_ProgramacionEstatus> programacionResp = ProgramacionEstatusBLL.ProgramacionEstatus(peticion);
                if (programacionResp != null && programacionResp.Count > 0)
                {
                    respuesta.ProgramacionEstatus = programacionResp.Select(p => p.Convertir<ProgramacionEstatusVM>()).ToList();
                    respuesta.TotalRegistros = programacionResp.Count;
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ActualizarEstatusProgramacionResponse ActualizarEstatusProgramacion(ActualizarEstatusProgramacionRequest peticion)
        {
            ActualizarEstatusProgramacionResponse respuesta = new ActualizarEstatusProgramacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.Programaciones == null || peticion.Programaciones.Count == 0) throw new ValidacionExcepcion("Proporcione al menos una programación para actualizar.");
                if (peticion.Programaciones.Any(p => p.IdEstatusProgramacion <= 0)) throw new ValidacionExcepcion("Existen programaciones que tienen un estatus válido.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int[] idsProgDet = peticion.Programaciones.Select(p => p.IdProgramacionDetalle).ToArray();

                    List<TB_GrupoLayoutProgramacionDetalle> programacionesDet = con.Cobranza_TB_GrupoLayoutProgramacionDetalle
                        .Where(pd => pd.Activo
                            && idsProgDet.Contains(pd.IdProgramacionDetalle)).ToList();

                    if (programacionesDet != null && programacionesDet.Count > 0)
                    {
                        respuesta.Programaciones = new List<ActualizarEstatusProgramacionResult>();
                        foreach (TB_GrupoLayoutProgramacionDetalle prog in programacionesDet)
                        {
                            ActualizarEstatusProgramacionResult actualizarResult = new ActualizarEstatusProgramacionResult
                            {
                                IdProgramacionDetalle = prog.IdProgramacionDetalle,
                                IdProgramacionEstatus = prog.IdProgramacionEstatus
                            };
                            try
                            {
                                ProgramacionDomiciliacionVM progEsta = peticion.Programaciones.Where(p => p.IdProgramacionDetalle == prog.IdProgramacionDetalle).FirstOrDefault();
                                prog.IdProgramacionEstatus = progEsta.IdEstatusProgramacion;
                                con.SaveChanges();

                                actualizarResult.IdProgramacionEstatus = prog.IdProgramacionEstatus;
                                actualizarResult.Actualizado = true;
                            }
                            catch (Exception ex)
                            {
                                respuesta.Error = true;
                                actualizarResult.Actualizado = false;
                                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
                            }
                            respuesta.Programaciones.Add(actualizarResult);
                        }
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static RegistrarProgramacionDetalleProResponse RegistrarProgramacionDetallePro(RegistrarProgramacionDetalleProRequest peticion)
        {
            RegistrarProgramacionDetalleProResponse respuesta = new RegistrarProgramacionDetalleProResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La peticion no es correcta.");
                if (peticion.IdUsuarioRegistro <= 0) throw new ValidacionExcepcion("La clave del usuario no es valida");
                if (peticion.IdProgramacionDetalle <= 0) throw new ValidacionExcepcion("La clave de la programación no es valida");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_ProgramacionDetalleProceso programacionDetalle = con.Cobranza_TB_ProgramacionDetalleProceso
                        .Where(p => p.IdProgramacionDetalle == peticion.IdProgramacionDetalle
                            && p.Activo)
                        .FirstOrDefault();
                    if (programacionDetalle != null && programacionDetalle.IdProgramacionDetallePro > 0)
                        throw new ValidacionExcepcion("Ya existe un proceso ejecutandose para esta programación.");

                    TB_ProgramacionDetalleProceso nuevo = new TB_ProgramacionDetalleProceso
                    {
                        IdProgramacionDetalle = peticion.IdProgramacionDetalle,
                        IdUsuarioRegistro = peticion.IdUsuarioRegistro,
                        FechaRegistro = DateTime.Now,
                        IdIntervalo = peticion.IdIntervalo,
                        ValorIntervalo = peticion.ValorIntervalo,
                        FechaEjecucionMax = peticion.FechaEjecucionMax,
                        Activo = true,
                        IdProgramacionEstatus = peticion.IdProgramacionEstatus
                    };
                    con.Cobranza_TB_ProgramacionDetalleProceso.Add(nuevo);
                    con.SaveChanges();
                    respuesta.IdProgramacionDetallePro = nuevo.IdProgramacionDetallePro;
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static Respuesta ActualizaProgramacionDetallePro(ActualizaProgramacionDetalleProRequest peticion)
        {
            Respuesta respuesta = new Respuesta();
            int idProcesoEstatusProgramado = 1;
            int idEstatusFinalizadoError = 3;
            int idProcesoEstatusFinalizado = 5;
            int[] estatusFin;
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La peticion no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es valida");
                if (peticion.IdProgramacionDetallePro <= 0) throw new ValidacionExcepcion("La clave de la programación no es valida");

                List<TB_ProgramacionEstatus> procesosEjecucion = ProgramacionEstatusBLL.ProgramacionEstatus(new ProgramacionEstatusRequest());

                if (procesosEjecucion != null && procesosEjecucion.Count > 0)
                {
                    idProcesoEstatusFinalizado = procesosEjecucion.Where(es => es.ClaveProgramacion == "F")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(5).FirstOrDefault();
                    idProcesoEstatusProgramado = procesosEjecucion.Where(es => es.ClaveProgramacion == "P")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(1).FirstOrDefault();
                    idEstatusFinalizadoError = procesosEjecucion.Where(es => es.ClaveProgramacion == "FE")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(1).FirstOrDefault();
                }
                estatusFin = new int[] { idEstatusFinalizadoError, idProcesoEstatusFinalizado };

                using (CreditOrigContext con = new CreditOrigContext())
                {

                    TB_ProgramacionDetalleProceso programDetalleProc = con.Cobranza_TB_ProgramacionDetalleProceso
                        .Where(p => p.IdProgramacionDetallePro == peticion.IdProgramacionDetallePro
                            && p.Activo)
                        .FirstOrDefault();

                    if (programDetalleProc != null)
                    {
                        programDetalleProc.FechaUltimoProceso = DateTime.Now;

                        if (peticion.IdProgramacionEstatus > 0)
                        {
                            programDetalleProc.IdProgramacionEstatus = peticion.IdProgramacionEstatus;
                            programDetalleProc.Activo = !estatusFin.Contains(peticion.IdProgramacionEstatus);
                            if (!string.IsNullOrEmpty(peticion.Mensaje))
                            {
                                programDetalleProc.Mensaje = peticion.Mensaje;
                            }

                            if (estatusFin.Contains(peticion.IdProgramacionEstatus))
                            {
                                TB_GrupoLayoutProgramacionDetalle programacionDetalle = con.Cobranza_TB_GrupoLayoutProgramacionDetalle
                                    .Where(pd => pd.IdProgramacionDetalle == programDetalleProc.IdProgramacionDetalle).FirstOrDefault();

                                if (programacionDetalle != null)
                                {
                                    programacionDetalle.IdProgramacionEstatus = idProcesoEstatusProgramado;
                                }
                            }
                        }
                        con.SaveChanges();
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ProgramacionGeneraMandoCobroResponse ProgramacionGeneraMandoCobro(ProgramacionGeneraMandoCobroRequest peticion)
        {
            ProgramacionGeneraMandoCobroResponse respuesta = new ProgramacionGeneraMandoCobroResponse();
            int idEstatusProcesoEjecucion = 2;
            int idEstatusGenArchivo = 6;
            int idEstatusFinalizadoError = 3;
            int idEstatusEnvioArchivoCobro = 7;
            int idEstatusValidaRespuesta = 8;
            try
            {
                #region ValidacionPrincipal
                if (peticion == null) throw new ValidacionExcepcion("La peticion no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es valida");
                if (peticion.IdProgramacionDetallePro <= 0) throw new ValidacionExcepcion("La clave de la programación no es valida");

                List<TB_ProgramacionEstatus> procesosEjecucion = ProgramacionEstatusBLL.ProgramacionEstatus(new ProgramacionEstatusRequest());

                if (procesosEjecucion != null && procesosEjecucion.Count > 0)
                {
                    idEstatusProcesoEjecucion = procesosEjecucion.Where(es => es.ClaveProgramacion == "PE")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(1).FirstOrDefault();
                    idEstatusGenArchivo = procesosEjecucion.Where(es => es.ClaveProgramacion == "GA")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(6).FirstOrDefault();
                    idEstatusFinalizadoError = procesosEjecucion.Where(es => es.ClaveProgramacion == "FE")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(3).FirstOrDefault();
                    idEstatusEnvioArchivoCobro = procesosEjecucion.Where(es => es.ClaveProgramacion == "AEC")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(7).FirstOrDefault();
                    idEstatusValidaRespuesta = procesosEjecucion.Where(es => es.ClaveProgramacion == "VR")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(7).FirstOrDefault();

                }

                List<Model.Dbo.EstatusVM> estatusLayouts = TB_CATEstatusBLL.EstatusLayoutsGenerados();
                int idEstatusLayoutTerminado = 3;
                if (estatusLayouts != null && estatusLayouts.Count > 0)
                {
                    idEstatusLayoutTerminado = estatusLayouts.Where(es => es.Estatus == "T").Select(es => es.IdEstatus)
                        .DefaultIfEmpty(3)
                        .FirstOrDefault();
                }
                #endregion

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    #region Obtener Proceso
                    TB_ProgramacionDetalleProceso programDetalleProc = con.Cobranza_TB_ProgramacionDetalleProceso
                        .Where(pdp => pdp.IdProgramacionDetallePro == peticion.IdProgramacionDetallePro
                            && pdp.Activo
                            && pdp.IdProgramacionEstatus == idEstatusProcesoEjecucion)
                        .FirstOrDefault();

                    if (programDetalleProc == null || programDetalleProc.IdProgramacionDetallePro <= 0) throw new ValidacionExcepcion("No fue posible encontrar el proceso asociado a la programación.");
                    var programacion = (from pd in con.Cobranza_TB_GrupoLayoutProgramacionDetalle
                                        join p in con.Cobranza_TB_GrupoLayoutProgramacion on pd.IdProgramacion equals p.IdProgramacion
                                        where pd.IdProgramacionDetalle == programDetalleProc.IdProgramacionDetalle
                                        select new
                                        {
                                            pd.IdProgramacionDetalle,
                                            p.IdProgramacion,
                                            p.IdGrupoLayout
                                        }).FirstOrDefault();

                    if (programacion == null || programacion.IdProgramacion <= 0) throw new ValidacionExcepcion("No fue posible encontrar la programacion del layout");
                    #endregion

                    #region GenerarLayouts
                    SP_ProcesarGrupoPROResponse generaLayoutResp = con.Cobranza_SP_ProcesarGrupoPRO(new SP_ProcesarGrupoPRORequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        IdGrupoLayout = programacion.IdGrupoLayout,
                        EsAutomatico = true
                    });

                    TB_LayoutGenerado layoutGenerado = con.Cobranza_TB_LayoutGenerado.Where(lg => lg.IdLayoutGenerado == generaLayoutResp.IdLayoutGenerado)
                        .FirstOrDefault();
                    if (layoutGenerado == null || layoutGenerado.IdLayoutGenerado <= 0) throw new ValidacionExcepcion("No fue posible encontrar el layout generado.");
                    if (layoutGenerado.IdEstatus != idEstatusLayoutTerminado) throw new ValidacionExcepcion("No fue posible genear el layout. Favor de verificar el proceso GenerarLayout");

                    con.Cobranza_TB_ProgramacionDetProLayoutGenerado.Add(new TB_ProgramacionDetProLayoutGenerado()
                    {
                        IdProgramacionDetallePro = programDetalleProc.IdProgramacionDetallePro,
                        IdLayoutGenerado = generaLayoutResp.IdLayoutGenerado,
                        IdUsuarioRegistro = peticion.IdUsuario,
                        FechaRegistro = DateTime.Now
                    });

                    con.SaveChanges();
                    respuesta.IdLayoutGenerado = layoutGenerado.IdLayoutGenerado;
                    #endregion

                    #region Generar Archivos
                    List<int> plantillasEnvioCobros = con.Cobranza_TB_LayoutGeneradoPlantilla
                        .Where(pg => pg.IdLayoutGenerado == layoutGenerado.IdLayoutGenerado
                            && (pg.IdPlantillaEnvioCobro ?? 0) > 0)
                        .Select(pg => pg.IdPlantillaEnvioCobro ?? 0).ToList();

                    if (plantillasEnvioCobros == null && plantillasEnvioCobros.Count == 0)
                        throw new ValidacionExcepcion("No se encontraron Archivos para generar");

                    programDetalleProc.IdProgramacionEstatus = idEstatusGenArchivo;
                    respuesta.IdProgramacionEstatus = idEstatusGenArchivo;
                    con.SaveChanges();

                    List<GenerarArchivoDomiciliacionResultado> archivosPlantillaEnvioCob = new List<GenerarArchivoDomiciliacionResultado>();
                    foreach (int idPlantillaEnvioCobro in plantillasEnvioCobros)
                    {
                        DescargaArchivoDomiciliacionResponse descargaPlantillaResp = PlantillaBL.DescargaArchivoDomiciliacion(new GenerarArchivoDomiciliacionRequest
                        {
                            IdPlantillaEnvioCobro = idPlantillaEnvioCobro,
                            IdUsuario = peticion.IdUsuario
                        });

                        if (descargaPlantillaResp.Error || descargaPlantillaResp.Resultado == null || descargaPlantillaResp.Resultado.ContenidoArchivo == null
                            || descargaPlantillaResp.Resultado.ContenidoArchivo.Length == 0)
                        {
                            if (string.IsNullOrEmpty(descargaPlantillaResp.MensajeOperacion))
                                descargaPlantillaResp.MensajeOperacion = "No fue posible generar el archivo de mando a cobro.";
                            throw new ValidacionExcepcion(descargaPlantillaResp.MensajeOperacion);
                        }
                        archivosPlantillaEnvioCob.Add(descargaPlantillaResp.Resultado);
                    }
                    if (archivosPlantillaEnvioCob.Count == 0) throw new ValidacionExcepcion("No se generaron archivos para realizar el envio a cobro.");
                    #endregion

                    #region Envio a VISANET
                    respuesta.IdPagosMasivos = new List<int>();
                    programDetalleProc.IdProgramacionEstatus = idEstatusEnvioArchivoCobro;
                    respuesta.IdProgramacionEstatus = idEstatusEnvioArchivoCobro;
                    con.SaveChanges();

                    foreach (GenerarArchivoDomiciliacionResultado genArchivoDomRes in archivosPlantillaEnvioCob)
                    {
                        ProcesarArchivoPagoMasivoRespose pagoMasivoResp = VisanetBLL.ProcesarArchivoPagoMasivo(new GeneraPagoMavisoRequest
                        {
                            ContenidoArchivo = genArchivoDomRes.ContenidoArchivo,
                            IdUsuario = peticion.IdUsuario,
                            NombreArchivo = genArchivoDomRes.NombreArchivo
                        });
                        respuesta.IdPagosMasivos.Add(pagoMasivoResp.IdPagoMasivo);
                    }

                    programDetalleProc.IdProgramacionEstatus = idEstatusValidaRespuesta;
                    respuesta.IdProgramacionEstatus = idEstatusValidaRespuesta;
                    con.SaveChanges();
                    #endregion
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            if (respuesta.Error)
            {
                ActualizaProgramacionDetallePro(new ActualizaProgramacionDetalleProRequest
                {
                    IdProgramacionDetallePro = peticion.IdProgramacionDetallePro,
                    IdProgramacionEstatus = idEstatusFinalizadoError,
                    IdUsuario = peticion.IdUsuario,
                    Mensaje = respuesta.MensajeOperacion
                });
                respuesta.IdProgramacionEstatus = idEstatusFinalizadoError;
            }

            return respuesta;
        }

        public static ProgramacionGeneraMandoCobroResponse ProgramacionReintentarMandoCobro(ProgramacionReintentarMandoCobroRequest peticion)
        {
            ProgramacionGeneraMandoCobroResponse respuesta = new ProgramacionGeneraMandoCobroResponse();
            int idEstatusProcesoEjecucion = 2;
            int idEstatusGenArchivo = 6;
            int idEstatusFinalizadoError = 3;
            int idEstatusEnvioArchivoCobro = 7;
            int idEstatusValidaRespuesta = 8;
            try
            {
                #region ValidacionPrincipal
                if (peticion == null) throw new ValidacionExcepcion("La peticion no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es valida");
                if (peticion.IdPagosMasivos == null || peticion.IdPagosMasivos.Count == 0) throw new ValidacionExcepcion("La clave de pagos masivo no son validas");

                List<TB_ProgramacionEstatus> procesosEjecucion = ProgramacionEstatusBLL.ProgramacionEstatus(new ProgramacionEstatusRequest());

                if (procesosEjecucion != null && procesosEjecucion.Count > 0)
                {
                    idEstatusProcesoEjecucion = procesosEjecucion.Where(es => es.ClaveProgramacion == "PE")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(1).FirstOrDefault();
                    idEstatusGenArchivo = procesosEjecucion.Where(es => es.ClaveProgramacion == "GA")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(6).FirstOrDefault();
                    idEstatusFinalizadoError = procesosEjecucion.Where(es => es.ClaveProgramacion == "FE")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(3).FirstOrDefault();
                    idEstatusEnvioArchivoCobro = procesosEjecucion.Where(es => es.ClaveProgramacion == "AEC")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(7).FirstOrDefault();
                    idEstatusValidaRespuesta = procesosEjecucion.Where(es => es.ClaveProgramacion == "VR")
                        .Select(es => es.IdProgramacionEstatus)
                        .DefaultIfEmpty(7).FirstOrDefault();

                }
                #endregion

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    #region Obtener Proceso
                    TB_ProgramacionDetalleProceso programDetalleProc = con.Cobranza_TB_ProgramacionDetalleProceso
                        .Where(pdp => pdp.IdProgramacionDetallePro == peticion.IdProgramacionDetallePro
                            && pdp.Activo
                            && pdp.IdProgramacionEstatus == idEstatusValidaRespuesta)
                        .FirstOrDefault();

                    if (programDetalleProc == null || programDetalleProc.IdProgramacionDetallePro <= 0) throw new ValidacionExcepcion("No fue posible encontrar el proceso asociado a la programación.");
                    #endregion

                    #region Obtener Pagos fallidos
                    List<LayoutVisanetVM> pagosFallidos = null;
                    VisanetDal vdal = new VisanetDal();
                    foreach (int idPagoMasivo in peticion.IdPagosMasivos)
                    {
                        List<PagoFallidoVisanetVM> pagosFallidosResp = vdal.DescargarPagosFallidosVisanet(idPagoMasivo);
                        if (pagosFallidosResp != null && pagosFallidosResp.Count > 0)
                        {
                            pagosFallidos.AddRange(pagosFallidosResp.Select(pf => new LayoutVisanetVM
                            {
                                IdSolicitud = int.TryParse(pf.Solicitud, out int idSolicitud) ? idSolicitud : 0,
                                MontoCobro = pf.Monto,
                                NumeroTarjeta = pf.NoTarjeta,
                                MesExpiracion = pf.MesExpiracion,
                                AnioExpiracion = pf.AnioExpiracion
                            }));
                        }
                    }
                    #endregion

                    #region Generar Archivos
                    List<GenerarArchivoDomiciliacionResultado> archivosPlantillaEnvioCob = new List<GenerarArchivoDomiciliacionResultado>();
                    if (pagosFallidos != null && pagosFallidos.Count > 0)
                    {
                        programDetalleProc.IdProgramacionEstatus = idEstatusGenArchivo;
                        respuesta.IdProgramacionEstatus = idEstatusGenArchivo;
                        con.SaveChanges();

                        DescargaArchivoDomiciliacionResponse archivoDomVisaResp = DomiciliacionBLL.GeneraArchivoDomVisanet(new GeneraArchivoDomVisanetRequest
                        {
                            IdUsuario = peticion.IdUsuario,
                            MandoCobro = pagosFallidos
                        });

                        if (archivoDomVisaResp.Error || archivoDomVisaResp.Resultado == null || archivoDomVisaResp.Resultado.ContenidoArchivo == null
                            || archivoDomVisaResp.Resultado.ContenidoArchivo.Length == 0)
                        {
                            if (string.IsNullOrEmpty(archivoDomVisaResp.MensajeOperacion))
                                archivoDomVisaResp.MensajeOperacion = "No fue posible generar el archivo de mando a cobro.";
                            throw new ValidacionExcepcion(archivoDomVisaResp.MensajeOperacion);
                        }
                        archivosPlantillaEnvioCob.Add(archivoDomVisaResp.Resultado);
                    }
                    if (archivosPlantillaEnvioCob.Count == 0) throw new ValidacionExcepcion("No se generaron archivos para realizar el envio a cobro.");
                    #endregion

                    #region Envio a VISANET
                    respuesta.IdPagosMasivos = new List<int>();
                    programDetalleProc.IdProgramacionEstatus = idEstatusEnvioArchivoCobro;
                    respuesta.IdProgramacionEstatus = idEstatusEnvioArchivoCobro;
                    con.SaveChanges();

                    foreach (GenerarArchivoDomiciliacionResultado genArchivoDomRes in archivosPlantillaEnvioCob)
                    {
                        ProcesarArchivoPagoMasivoRespose pagoMasivoResp = VisanetBLL.ProcesarArchivoPagoMasivo(new GeneraPagoMavisoRequest
                        {
                            ContenidoArchivo = genArchivoDomRes.ContenidoArchivo,
                            IdUsuario = peticion.IdUsuario,
                            NombreArchivo = genArchivoDomRes.NombreArchivo
                        });
                        respuesta.IdPagosMasivos.Add(pagoMasivoResp.IdPagoMasivo);
                    }

                    programDetalleProc.IdProgramacionEstatus = idEstatusValidaRespuesta;
                    respuesta.IdProgramacionEstatus = idEstatusValidaRespuesta;
                    con.SaveChanges();
                    #endregion
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            if (respuesta.Error)
            {
                ActualizaProgramacionDetallePro(new ActualizaProgramacionDetalleProRequest
                {
                    IdProgramacionDetallePro = peticion.IdProgramacionDetallePro,
                    IdProgramacionEstatus = idEstatusFinalizadoError,
                    IdUsuario = peticion.IdUsuario,
                    Mensaje = respuesta.MensajeOperacion
                });
                respuesta.IdProgramacionEstatus = idEstatusFinalizadoError;
            }
            respuesta.IdPagosMasivos = peticion.IdPagosMasivos;
            return respuesta;
        }

        #region "Tipo Programacion"
        public static TipoProgramacionesResponse TipoProgramaciones(TipoProgramacionesRequest peticion)
        {
            TipoProgramacionesResponse respuesta = new TipoProgramacionesResponse();
            try
            {
                List<TB_CATTipoProgramacion> tipoProgramacion = null;
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válida.");
                tipoProgramacion = TipoProgramacionBLL.TipoProgramaciones(peticion);

                if (tipoProgramacion != null)
                {
                    respuesta.TipoProgramacion = tipoProgramacion.Select(t => t.Convertir<TipoProgramacionVM>()).ToList();
                    respuesta.TotalRegistros = respuesta.TipoProgramacion.Count;
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static List<TipoProgramacion> TipoProgramacion_ObtenerActivos()
        {
            return dal.TipoProgramacion_ObtenerActivos();
        }

        #endregion

        #region "Intervalo"

        public static List<ProgramacionIntervalo> Intervalos_ObtenerActivos()
        {
            return dal.Intervalos_ObtenerActivos();
        }

        #endregion

        #region "Metodos Privados"
        private static DateTime HoraFecha(DateTime? fechaHora)
        {
            return default(DateTime).Add((fechaHora ?? default).TimeOfDay);
        }

        private static DateTime CombinarFechaHora(DateTime? fecha, DateTime? hora)
        {
            DateTime fechaFinal = (fecha ?? default).Date;
            fechaFinal = fechaFinal.Add((hora ?? default).TimeOfDay);
            return fechaFinal;
        }
        #endregion
    }
}
