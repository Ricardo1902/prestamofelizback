﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.DataAccess.Cobranza;

namespace SOPF.Core.BusinessLogic.Cobranza
{
    public static class VisanetBL
    {
        private static VisanetDAL dal;

        static VisanetBL()
        {
            dal = new VisanetDAL();
        }

        public static List<VisanetValidarTarjeta> ObtenerPendientesValidar()
        {
            return dal.ObtenerPendientesValidar();
        }
    }
}
