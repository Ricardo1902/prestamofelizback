﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Cobranza;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Model.Result.Cobranza;
using SOPF.Core.Poco.Cobranza;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static SOPF.Core.LayoutArchivo;

namespace SOPF.Core.BusinessLogic.Cobranza
{
    public static class DomiciliacionBLL
    {
        private static CreditOrigContext con;
        static DomiciliacionBLL()
        {
            con = new CreditOrigContext();
        }

        #region Métodos publicos
        public static ExcluirCuentasResponse ExcluirCuentas(ExcluirCuentasRequest peticion)
        {
            ExcluirCuentasResponse respuesta = new ExcluirCuentasResponse();
            List<CuentaExcluyeDomiciliacionResult> validaciones = new List<CuentaExcluyeDomiciliacionResult>();
            TB_TipoIngreso tipoIngresoManual = TB_TipoIngresoBLL.TipoIngresoManual();
            if (tipoIngresoManual == null) tipoIngresoManual = new TB_TipoIngreso { IdTipoIngreso = 1 };
            TB_OrigenExclusion origenExclusion = TB_OrigenExclusionBLL.OrigenExclusionCargaExclusiones();
            if (origenExclusion == null) origenExclusion = new TB_OrigenExclusion { IdOrigenExclusion = 1 };

            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta!");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido");
                if (peticion.CuentasExcluir == null || peticion.CuentasExcluir.Count == 0) throw new ValidacionExcepcion("Proporcione la(s) cuenta(s) a excluir.");

                int[] solicitudes = peticion.CuentasExcluir.Select(c => c.IdSolicitud).ToArray();
                var solicitudesValidas = con.dbo_TB_Solicitudes
                    .Where(s => solicitudes.Contains((int)s.IdSolicitud))
                    .Select(s => (int)s.IdSolicitud).ToArray();

                int[] solicitudesInvalidas = solicitudes.Where(s => !solicitudesValidas.Contains(s)).ToArray();
                List<CuentaExcluyeDomiciliacionVM> cuentasValidas = peticion.CuentasExcluir.Where(s => solicitudesValidas.Contains(s.IdSolicitud)).ToList();
                if (solicitudesInvalidas != null && solicitudesInvalidas.Length > 0)
                {
                    validaciones.AddRange(solicitudesInvalidas.Select(c => new CuentaExcluyeDomiciliacionResult
                    {
                        IdSolicitud = c,
                        Mensaje = "La solicitud no es váida."
                    }));
                    respuesta.MensajeOperacion = $"Existen {solicitudesInvalidas.Length} cuentas que no fueron excluidos.";
                }
                int[] solicitudesExistentes = con.Cobranza_TB_CuentaExcluyeDomiciliacion
                    .Where(ce => solicitudesValidas.Contains(ce.IdSolicitud)
                        && ce.Activo)
                    .Select(ce => ce.IdSolicitud).ToArray();
                if (solicitudesExistentes != null && solicitudesExistentes.Length > 0)
                {
                    validaciones.AddRange(solicitudesExistentes.Select(s => new CuentaExcluyeDomiciliacionResult
                    {
                        IdSolicitud = s,
                        Mensaje = "La solicitud ya se encuetra excluida."
                    }));
                    respuesta.MensajeOperacion += $" Existen {solicitudesExistentes.Length} cuentas registradas y activas.";
                    cuentasValidas = cuentasValidas.Where(cv => !solicitudesExistentes.Contains(cv.IdSolicitud)).ToList();
                }

                con.Cobranza_TB_CuentaExcluyeDomiciliacion.AddRange(
                    cuentasValidas.Select(cv => new TB_CuentaExcluyeDomiciliacion
                    {
                        IdTipoIngreso = tipoIngresoManual.IdTipoIngreso,
                        IdOrigenExclusion = origenExclusion.IdOrigenExclusion,
                        IdSolicitud = cv.IdSolicitud,
                        IdMotivoExclusionDomiciliacion = cv.IdMotivoExclusionDomiciliacion,
                        Comentario = cv.Comentario,
                        FechaRegistro = DateTime.Now,
                        IdUsuarioRegistro = peticion.IdUsuario,
                        Activo = true
                    }));
                con.SaveChanges();

                if (validaciones != null && validaciones.Count > 0) respuesta.Resultado.ErrorCuentas = validaciones;
                if (solicitudesInvalidas != null && solicitudesInvalidas.Length > 0) respuesta.Error = true;
                respuesta.MensajeOperacion += $" Se excluyeron {cuentasValidas.Count} cuentas con éxito!";
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar el archivo de domiciliacion.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static CuentasExcluidasResponse CuentasExcluidas(CuentasExcluidasRequest peticion)
        {
            CuentasExcluidasResponse respuesta = new CuentasExcluidasResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta!");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válida");
                IQueryable<TB_CuentaExcluyeDomiciliacion> cuentasFiltro = con.Cobranza_TB_CuentaExcluyeDomiciliacion;
                if (peticion.IdSolicitud != null && peticion.IdSolicitud > 0)
                    cuentasFiltro = cuentasFiltro.Where(c => c.IdSolicitud == peticion.IdSolicitud);

                List<CuentasExcluidasVM> excluidos = (from c in cuentasFiltro
                                                      join m in con.Cobranza_TB_MotivoExclusionDomiciliacion on c.IdMotivoExclusionDomiciliacion equals m.IdMotivoExclusionDomiciliacion into left1
                                                      from lm in left1.DefaultIfEmpty()
                                                      select new CuentasExcluidasVM
                                                      {
                                                          IdCuentaExcluyeDomiciliacion = c.IdCuentaExcluyeDomiciliacion,
                                                          IdSolicitud = c.IdSolicitud,
                                                          IdMotivoExclusionDomiciliacion = c.IdMotivoExclusionDomiciliacion,
                                                          Comentario = c.Comentario,
                                                          FechaRegistro = c.FechaRegistro,
                                                          MotivoExclusionDomiciliacion = lm.MotivoExclusion
                                                      }).ToList();

                if (excluidos != null && excluidos.Count > 0)
                {
                    respuesta.TotalRegistros = excluidos.Count;
                    if (peticion.Pagina > 0 && peticion.RegistrosPagina > 0)
                    {
                        excluidos = excluidos
                            .Skip(((peticion.Pagina ?? 0) - 1) * (peticion.RegistrosPagina ?? 0))
                            .Take(peticion.RegistrosPagina ?? 0)
                            .ToList();
                    }
                    respuesta.Resultado.CuentasExcluidas = excluidos;
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar el archivo de domiciliacion.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static MotivosExclusionResponse MotivosExclusion(MotivosExclusionRequest peticion)
        {
            MotivosExclusionResponse respuesta = new MotivosExclusionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta!");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válida");

                var respMotivos = con.Cobranza_TB_MotivoExclusionDomiciliacion.Where(m => m.Activo == peticion.Activo)
                    .Select(m => new MotivoExclusionDomiciliacionVM
                    {
                        IdMotivoExclusionDomiciliacion = m.IdMotivoExclusionDomiciliacion,
                        MotivoExclusion = m.MotivoExclusion,
                        Activo = m.Activo
                    })
                    .ToList();
                if (respMotivos != null && respMotivos.Count > 0)
                {
                    respuesta.TotalRegistros = respMotivos.Count;
                    respuesta.Resultado.MotivosExclusion = respMotivos;
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar el archivo de domiciliacion.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static BancosDomiciliacionCuentaResponse BancosDomiciliacionCuenta()
        {
            BancosDomiciliacionCuentaResponse respuesta = new BancosDomiciliacionCuentaResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta.Bancos = (from b in con.dbo_TB_CATBanco
                                        join bd in con.Cobranza_TB_BancoDomiciliacion on b.IdBanco equals bd.IdBanco
                                        join bm in con.Cobranza_TB_MedioDomiciliacion on bd.IdMedioDomiciliacion equals bm.IdMedioDomiciliacion
                                        where bm.Medio == "CuentaBancaria"
                                        select new BancoVM { IdBanco = b.IdBanco, Banco = b.Banco }
                                        ).ToList();
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static DescargaArchivoDomiciliacionResponse GeneraArchivoDomVisanet(GeneraArchivoDomVisanetRequest peticion)
        {
            DescargaArchivoDomiciliacionResponse respuesta = new DescargaArchivoDomiciliacionResponse();
            try
            {
                #region Validaciones
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La calve del usario no es válido");
                #endregion

                LayoutArchivoVM layout = null;
                if (peticion.MandoCobro != null && peticion.MandoCobro.Count > 0)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        layout = con.Cobranza_TB_LayoutArchivo
                        .Where(la => (la.Activo ?? false) == true
                            && la.IdTipoLayout == (int)TipoLayot.Domiciliacion
                            && la.IdTipoArchivo == (int)TipoArchivo.Salida
                            && la.NombreLayout == "Domiciliacion Visanet")
                        .Select(la => new LayoutArchivoVM
                        {
                            IdLayoutArchivo = la.IdLayoutArchivo,
                            NombreArchivo = la.NombreArchivo,
                            DelimitadorColumna = la.DelimitadorColumna,
                            Extension = la.Extension,
                            TipoContenido = la.TipoContenido,
                            CaracteresRemplazo = la.CaracteresRemplazo
                        })
                        .FirstOrDefault();
                    }
                }

                if (layout != null)
                {
                    respuesta = LayoutArchivoBLL.GenerarArchivoDomiciliacion<object, LayoutVisanetVM, object, object>(layout.IdLayoutArchivo, peticion.MandoCobro[0], peticion.MandoCobro, null, null);
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                      $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        #endregion

        #region Métodos privados
        #endregion

    }
}
