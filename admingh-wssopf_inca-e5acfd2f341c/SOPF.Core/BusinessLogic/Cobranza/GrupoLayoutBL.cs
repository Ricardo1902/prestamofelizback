﻿using System.Collections.Generic;
using System.Linq;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.DataAccess.Cobranza;

namespace SOPF.Core.BusinessLogic.Cobranza
{
    public static class GrupoLayoutBL
    {
        private static GrupoLayoutDAL dal;

        static GrupoLayoutBL()
        {
            dal = new GrupoLayoutDAL();
        }

        public static Resultado<GrupoLayout> Guardar(GrupoLayout objEntity)
        {
            Resultado<GrupoLayout> objReturn = new Resultado<GrupoLayout>();

            // Verificar si es un Alta o Actualizacion
            if (objEntity.IdGrupoLayout == null)
                objReturn = Alta(objEntity);
            else
                objReturn = Actualizar(objEntity);

            // Guardar las Plantillas
            if (objReturn.Codigo == 0)
            {
                Guardar_Plantillas(objEntity);
            }

            return objReturn;
        }

        public static Resultado<GrupoLayout> Obtener(int IdGrupoLayout)
        {
            Resultado<GrupoLayout> objReturn = new Resultado<GrupoLayout>();
            Resultado<List<GrupoLayout>> res = dal.Filtrar(new GrupoLayout() { IdGrupoLayout = IdGrupoLayout });

            // Obtener el Grupo
            if (res.Codigo == 0)
            {
                if (res.ResultObject.Count() <= 0)
                {
                    objReturn.Codigo = res.Codigo;
                    objReturn.Mensaje = string.Format("No se encontro el Grupo con ID[{0}]", IdGrupoLayout.ToString());
                }
                else
                    objReturn.ResultObject = res.ResultObject.First();
            }
            else
            {
                objReturn.Codigo = res.Codigo;
                objReturn.Mensaje = res.Mensaje;
            }

            // Obtener las Plantillas del Grupo
            if (objReturn.Codigo == 0)
            {
                Resultado<List<GrupoLayout.PlantillaGrupoLayout>> resP = Cargar_Plantillas(objReturn.ResultObject.IdGrupoLayout.GetValueOrDefault());

                if (resP.Codigo > 0)
                {
                    objReturn.Codigo = resP.Codigo;
                    objReturn.Mensaje = resP.Mensaje;
                }
                else
                    objReturn.ResultObject.Plantillas = resP.ResultObject;
            }

            // Obtener los Dias de Ejecucion del Grupo
            if (objReturn.Codigo == 0)
            {
                Resultado<List<GrupoLayout.ConfiguracionEjecucion>> resDE = Cargar_DiasEjecucion(objReturn.ResultObject.IdGrupoLayout.GetValueOrDefault());

                if (resDE.Codigo > 0)
                {
                    objReturn.Codigo = resDE.Codigo;
                    objReturn.Mensaje = resDE.Mensaje;
                }
                else
                    objReturn.ResultObject.DiasEjecucion = resDE.ResultObject;
            }

            //Obtener las programaciones y dias de ejecucion del grupo
            if (objReturn.Codigo == 0)
            {
                Resultado<List<GrupoLayout.ProgramacionGrupoLayout>> resPr = Cargar_Programaciones(objReturn.ResultObject.IdGrupoLayout.GetValueOrDefault());

                if (resPr.Codigo > 0)
                {
                    objReturn.Codigo = resPr.Codigo;
                    objReturn.Mensaje = resPr.Mensaje;
                }
                else
                    objReturn.ResultObject.Programaciones = resPr.ResultObject;
            }

            return objReturn;
        }        

        public static Resultado<List<GrupoLayout>> Buscar(GrupoLayout objEntity)
        {
            return dal.Filtrar(objEntity);
        }

        private static Resultado<GrupoLayout> Alta(GrupoLayout objEntity)
        {
            return dal.Alta(objEntity);
        }

        private static Resultado<GrupoLayout> Actualizar(GrupoLayout objEntity)
        {
            return dal.Actualizar(objEntity);
        }

        public static Resultado<bool> Procesar(GrupoLayout.PeticionProcesar objEntity)
        {
            return dal.Procesar(objEntity);
        }

        #region "Metodos Privados"       

        private static void Guardar_Plantillas(GrupoLayout objEntity)
        {
            if (dal.EliminarPlantillas(objEntity.IdGrupoLayout.GetValueOrDefault()).Codigo == 0)
            {
                foreach (GrupoLayout.PlantillaGrupoLayout p in objEntity.Plantillas)
                    dal.GuardarPlantilla(objEntity.IdGrupoLayout.GetValueOrDefault(), p.Plantilla.IdPlantilla.GetValueOrDefault(), p.DiasVencimiento);
            }
        }

        private static Resultado<List<GrupoLayout.PlantillaGrupoLayout>> Cargar_Plantillas(int IdGrupoLayout)
        {
            return dal.ObtenerPlantillas(IdGrupoLayout);
        }

        private static Resultado<List<GrupoLayout.ConfiguracionEjecucion>> Cargar_DiasEjecucion(int IdGrupoLayout)
        {          
            return dal.ObtenerDiasEjecucion(IdGrupoLayout);
        }

        private static Resultado<List<GrupoLayout.ProgramacionGrupoLayout>> Cargar_Programaciones(int IdGrupoLayout)
        {
            return dal.ObtenerProgramaciones(IdGrupoLayout);
        }

        private static Resultado<List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion>> Cargar_ProgramacionDiasEjecucion(int IdProgramacion)
        {
            return dal.ObtenerProgramacionDiasEjecucion(IdProgramacion);
        }

        #endregion
    }
}
