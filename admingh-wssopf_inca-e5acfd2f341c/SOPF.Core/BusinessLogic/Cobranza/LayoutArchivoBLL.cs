﻿using log4net.Plugin;
using Newtonsoft.Json;
using OfficeOpenXml;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using static SOPF.Core.LayoutArchivo;

namespace SOPF.Core.BusinessLogic.Cobranza
{
    public static class LayoutArchivoBLL
    {
        #region Métodos publicos
        public static DescargaArchivoDomiciliacionResponse GenerarArchivoDomiciliacion<T, T2, T3, T4>(int idLayoutArchivo, T encabezado, List<T2> detalle, List<T3> subDetalle, T4 resumen)
        {
            DescargaArchivoDomiciliacionResponse respuesta = new DescargaArchivoDomiciliacionResponse();
            LayoutArchivoVM layout;
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    layout = con.Cobranza_TB_LayoutArchivo
                        .Where(la => (la.Activo ?? false) == true
                            && la.IdTipoLayout == (int)TipoLayot.Domiciliacion
                            && la.IdTipoArchivo == (int)TipoArchivo.Salida
                            && la.IdLayoutArchivo == idLayoutArchivo)
                        .Select(la => new LayoutArchivoVM
                        {
                            IdLayoutArchivo = la.IdLayoutArchivo,
                            NombreArchivo = la.NombreArchivo,
                            DelimitadorColumna = la.DelimitadorColumna,
                            Extension = la.Extension,
                            TipoContenido = la.TipoContenido,
                            CaracteresRemplazo = la.CaracteresRemplazo
                        })
                        .FirstOrDefault();
                    if (layout != null)
                    {
                        layout.Campos = con.Cobranza_TB_LayoutArchivoCampo
                            .Where(lac => lac.Activo == true
                                && lac.IdLayoutArchivo == layout.IdLayoutArchivo)
                            .Select(lac => new LayoutArchivoCampoVM
                            {
                                IdLayoutArchivoCampo = lac.IdLayoutArchivoCampo,
                                IdLayoutArchivoSeccion = lac.IdLayoutArchivoSeccion,
                                DescripcionCampo = lac.DescripcionCampo,
                                IdTipoCampo = lac.IdTipoCampo,
                                EntidadValor = lac.EntidadValor,
                                Longitud = lac.Longitud,
                                Enteros = lac.Enteros,
                                Decimales = lac.Decimales,
                                IncluyePunto = lac.IncluyePunto,
                                IdLayoutArchivoAlineacion = lac.IdLayoutArchivoAlineacion,
                                Relleno = lac.Relleno,
                                TextoFijo = lac.TextoFijo,
                                FormatoFecha = lac.FormatoFecha,
                                Orden = lac.Orden
                            })
                            .OrderBy(o => new { o.IdLayoutArchivoSeccion, o.Orden })
                            .ToList();
                    }
                }
                if (layout == null && layout.Campos == null) throw new ValidacionExcepcion("No existe archivo de configuración para la domiciliacion.");
                if (!layout.Campos.Exists(c => c.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Encabezado)
                        && !layout.Campos.Exists(c => c.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Detalle))
                    throw new ValidacionExcepcion("No existe un archivo de configuración de domiciliacion.");
                if (detalle == null || detalle.Count <= 0) throw new ValidacionExcepcion("No existen registros para generar un archivo de domiciliacion.");
                var t = detalle.GetType();
                switch (layout.TipoContenido)
                {
                    case "text/plain":
                    case "text/csv":
                        respuesta = GenerarArchivoTexto<T, T2, T3, T4>(layout, encabezado, detalle, subDetalle, resumen);
                        break;
                    case "application/vnd.ms-excel":
                        respuesta = GenerarArchivoExcel<T, T2, T3, T4>(layout, encabezado, detalle, subDetalle, resumen);
                        break;
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar el archivo de domiciliacion.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error: {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: idLayoutArchivo: {idLayoutArchivo}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        #endregion

        #region Métodos privados
        private static DescargaArchivoDomiciliacionResponse GenerarArchivoTexto<T, T2, T3, T4>(LayoutArchivoVM layout, T encabezado, List<T2> detalle, List<T3> subDetalle, T4 resumen)
        {
            DescargaArchivoDomiciliacionResponse respuesta = new DescargaArchivoDomiciliacionResponse();
            byte[] archivo = null;
            StringBuilder lineas = new StringBuilder();
            string linea = string.Empty;
            string campo = string.Empty;
            int posicion, maxPosicion;

            if (layout == null) return respuesta;
            try
            {
                #region Encabezado
                if (encabezado != null && layout.Campos.Exists(c => c.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Encabezado))
                {
                    posicion = 0;
                    maxPosicion = layout.Campos.Where(c => c.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Encabezado).Count();
                    foreach (LayoutArchivoCampoVM detCampo in layout.Campos.Where(c => c.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Encabezado).ToList())
                    {
                        campo = Utils.FomatearCampo(detCampo, encabezado);
                        if (!string.IsNullOrEmpty(layout.DelimitadorColumna) && layout.TipoContenido == "text/csv")
                        {
                            campo = $"{campo}{((posicion < maxPosicion - 1) ? layout.DelimitadorColumna : string.Empty)}";
                        }
                        linea += campo;
                        posicion++;
                    }
                    if (!string.IsNullOrEmpty(linea)) lineas.AppendLine(linea);
                }
                #endregion

                #region detalle
                if (detalle != null && layout.Campos.Exists(c => c.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Detalle))
                {
                    foreach (var detalleContenido in detalle)
                    {
                        linea = string.Empty;
                        posicion = 0;
                        maxPosicion = layout.Campos.Where(lc => lc.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Detalle).Count();
                        foreach (LayoutArchivoCampoVM detCampo in layout.Campos.Where(lc => lc.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Detalle).ToList())
                        {
                            campo = Utils.FomatearCampo(detCampo, detalleContenido);
                            if (!string.IsNullOrEmpty(layout.DelimitadorColumna) && layout.TipoContenido == "text/csv")
                            {
                                campo = $"{campo}{((posicion < maxPosicion - 1) ? layout.DelimitadorColumna : string.Empty)}";
                            }
                            linea += campo;
                            posicion++;
                        }
                        if (!string.IsNullOrEmpty(linea)) lineas.AppendLine(linea);

                        if (subDetalle != null)
                        {
                            foreach (var subDetalleContenido in subDetalle)
                            {
                                linea = string.Empty;
                                posicion = 0;
                                maxPosicion = layout.Campos.Where(lc => lc.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Subdetalle).Count();
                                foreach (LayoutArchivoCampoVM detCampo in layout.Campos.Where(lc => lc.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Subdetalle).ToList())
                                {
                                    campo = Utils.FomatearCampo(detCampo, subDetalleContenido);
                                    if (!string.IsNullOrEmpty(layout.DelimitadorColumna) && layout.TipoContenido == "text/csv")
                                    {
                                        campo = $"{campo}{((posicion < maxPosicion - 1) ? layout.DelimitadorColumna : string.Empty)}";
                                    }
                                    linea += campo;
                                    posicion++;
                                }
                                if (!string.IsNullOrEmpty(linea)) lineas.AppendLine(linea);
                            }
                        }
                        else
                        {
                            linea = string.Empty;
                            posicion = 0;
                            maxPosicion = layout.Campos.Where(lc => lc.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Subdetalle).Count();
                            foreach (LayoutArchivoCampoVM detCampo in layout.Campos.Where(lc => lc.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Subdetalle).ToList())
                            {
                                campo = Utils.FomatearCampo(detCampo, null);
                                if (!string.IsNullOrEmpty(layout.DelimitadorColumna) && layout.TipoContenido == "text/csv")
                                {
                                    campo = $"{campo}{((posicion < maxPosicion - 1) ? layout.DelimitadorColumna : string.Empty)}";
                                }
                                linea += campo;
                                posicion++;
                            }
                            if (!string.IsNullOrEmpty(linea)) lineas.AppendLine(linea);
                        }
                    }
                }
                #endregion

                #region resumen
                linea = string.Empty;
                posicion = 0;
                maxPosicion = layout.Campos.Where(lc => lc.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Resumen).Count();
                foreach (LayoutArchivoCampoVM detCampo in layout.Campos.Where(lc => lc.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Resumen).ToList())
                {
                    campo = Utils.FomatearCampo(detCampo, resumen);
                    if (!string.IsNullOrEmpty(layout.DelimitadorColumna) && layout.TipoContenido == "text/csv")
                    {
                        if (campo.IndexOf(',') > 0)
                        {
                            campo = $"{campo}";
                        }
                        campo = $"{campo}{((posicion < maxPosicion - 1) ? layout.DelimitadorColumna : string.Empty)}";
                    }
                    linea += campo;
                    posicion++;
                }
                if (!string.IsNullOrEmpty(linea)) lineas.AppendLine(linea);
                #endregion

                if (lineas.Length > 0)
                {
                    DateTime fechaActual = DateTime.Now;
                    if (!string.IsNullOrEmpty(layout.CaracteresRemplazo))
                    {
                        string[] caracteresRemplazar = layout.CaracteresRemplazo.Split('|');
                        if (caracteresRemplazar != null && caracteresRemplazar.Length > 0)
                        {
                            foreach (string cadena in caracteresRemplazar)
                            {
                                string[] caracteres = cadena.Split('>');
                                if (caracteres != null && caracteres.Length == 2 && caracteres[0] != caracteres[1])
                                {
                                    lineas = lineas.Replace(caracteres[0], caracteres[1]);
                                }
                            }
                        }
                    }

                    archivo = Encoding.UTF8.GetBytes(lineas.ToString());
                    if (archivo.Length >= 0)
                    {
                        respuesta.Resultado.ContenidoArchivo = archivo;
                        respuesta.Resultado.NombreArchivo = $"{layout.NombreArchivo}_{fechaActual:ddMMyy_HHmmss}";
                        respuesta.Resultado.Extension = layout.Extension;
                        respuesta.Resultado.TipoContenido = layout.TipoContenido;
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar el archivo de domicliación.";
                Utils.EscribirLog($"Error: {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        private static DescargaArchivoDomiciliacionResponse GenerarArchivoExcel<T, T2, T3, T4>(LayoutArchivoVM layout, T encabezado, List<T2> detalle, List<T3> subDetalle, T4 resumen)
        {
            DescargaArchivoDomiciliacionResponse respuesta = new DescargaArchivoDomiciliacionResponse();
            int linea;
            string campo = string.Empty;
            int columna;
            try
            {
                if (layout == null) return respuesta;
                linea = 1;
                using (ExcelPackage excel = new ExcelPackage())
                {
                    ExcelWorksheet hoja = excel.Workbook.Worksheets.Add(layout.NombreArchivo);
                    #region Encabezado
                    if (encabezado != null && layout.Campos.Exists(c => c.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Encabezado))
                    {
                        columna = 1;
                        foreach (LayoutArchivoCampoVM detCampo in layout.Campos.Where(c => c.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Encabezado).ToList())
                        {
                            campo = Utils.FomatearCampo(detCampo, encabezado);
                            hoja.Cells[linea, columna].Value = campo;
                            columna++;
                        }
                        linea++;
                    }
                    #endregion

                    #region detalle
                    if (detalle != null && layout.Campos.Exists(c => c.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Detalle))
                    {
                        foreach (var detalleContenido in detalle)
                        {
                            columna = 1;
                            foreach (LayoutArchivoCampoVM detCampo in layout.Campos.Where(lc => lc.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Detalle).ToList())
                            {
                                campo = Utils.FomatearCampo(detCampo, detalleContenido);
                                hoja.Cells[linea, columna].Value = campo;
                                columna++;
                            }

                            if (subDetalle != null)
                            {
                                foreach (var subDetalleContenido in subDetalle)
                                {
                                    columna = 1;
                                    foreach (LayoutArchivoCampoVM detCampo in layout.Campos.Where(lc => lc.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Subdetalle).ToList())
                                    {
                                        campo = Utils.FomatearCampo(detCampo, subDetalleContenido);
                                        hoja.Cells[linea, columna].Value = campo;
                                        columna++;
                                    }
                                    linea++;
                                }
                            }
                            else
                            {
                                columna = 1;
                                foreach (LayoutArchivoCampoVM detCampo in layout.Campos.Where(lc => lc.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Subdetalle).ToList())
                                {
                                    campo = Utils.FomatearCampo(detCampo, null);
                                    hoja.Cells[linea, columna].Value = campo;
                                    columna++;
                                }
                                linea++;
                            }
                        }
                    }
                    #endregion

                    #region resumen
                    columna = 1;
                    foreach (LayoutArchivoCampoVM detCampo in layout.Campos.Where(lc => lc.IdLayoutArchivoSeccion == (int)ArchivoSeccion.Resumen).ToList())
                    {
                        campo = Utils.FomatearCampo(detCampo, resumen);
                        hoja.Cells[linea, columna].Value = campo;
                        columna++;
                    }
                    linea++;
                    #endregion

                    byte[] archivo = excel.GetAsByteArray();
                    if (archivo.Length > 0)
                    {
                        DateTime fechaActual = DateTime.Now;
                        respuesta.Resultado.ContenidoArchivo = archivo;
                        respuesta.Resultado.NombreArchivo = $"{layout.NombreArchivo}_{fechaActual:ddMMyy_HHmmss}";
                        respuesta.Resultado.Extension = layout.Extension;
                        respuesta.Resultado.TipoContenido = layout.TipoContenido;
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar el archivo de domicliación.";
                Utils.EscribirLog($"Error: {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        #endregion
    }
}
