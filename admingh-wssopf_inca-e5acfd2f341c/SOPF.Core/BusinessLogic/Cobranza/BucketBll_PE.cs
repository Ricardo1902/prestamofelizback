﻿using System.Collections.Generic;
using SOPF.Core.DataAccess.Cobranza;
using SOPF.Core.Entities;

namespace SOPF.Core.BusinessLogic.Cobranza
{
    public class BucketBll_PE
    {
        private static BucketDal_PE dal;

        static BucketBll_PE()
        {
            dal = new BucketDal_PE();
        }

        public static List<Combo> ObtenerCATCombo()
        {
            return dal.ObtenerCATCombo();
        }
    }
}
