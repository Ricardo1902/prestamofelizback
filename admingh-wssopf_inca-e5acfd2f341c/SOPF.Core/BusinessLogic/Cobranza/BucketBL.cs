﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;
using SOPF.Core.DataAccess.Cobranza;

namespace SOPF.Core.BusinessLogic.Cobranza
{   
    public static class BucketBL
    {
        private static BucketDAL dal;

        static BucketBL()
        {
            dal = new BucketDAL();
        }

        public static List<Bucket> Listar()
        {
            return dal.Listar();
        }

        public static List<Bucket> ListarLayout()
        {
            return dal.ListarLayout();
        }
    }
}
