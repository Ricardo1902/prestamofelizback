﻿using SOPF.Core.DataAccess.Hubble;
using SOPF.Core.Entities.Hubble;
using SOPF.Core.Poco.Hubble;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.BusinessLogic.Hubble
{
    public static class TemplateBll
    {
        private static TemplateDAL dal;

        static TemplateBll()
        {
            dal = new TemplateDAL();
        }

        public static List<Template> GetAllTemplates()
        {
            List<Template> templates = new List<Template>();
            List<TB_EmailTemplates> tbEmailTemplates = new List<TB_EmailTemplates>();
            List<TB_EmailTemplateParams> tbEmailTemplateParams = new List<TB_EmailTemplateParams>();
            List<TB_TemplateParamInputTypes> tbTemplateParamInputTypes = new List<TB_TemplateParamInputTypes>();

            try
            {
                tbEmailTemplates = dal.GetTableTemplates();
                tbEmailTemplateParams = dal.GetTableTemplateParams();
                tbTemplateParamInputTypes = dal.GetTableTemplateParamInputTypes();

                tbEmailTemplates.ForEach(et => {
                    List<TemplateParams> templateParams = new List<TemplateParams>();
                    List<TB_EmailTemplateParams> etp = tbEmailTemplateParams.Where(x => x.EmailTemplate_Id == et.IdEmailTemplate).ToList();

                    etp.ForEach(x => {
                        TB_TemplateParamInputTypes inputType = tbTemplateParamInputTypes.Where(it => it.IdEmailTemplateParamType == x.EmailTemplateParamType_Id).FirstOrDefault();

                        templateParams.Add(new TemplateParams {
                            Id = x.IdEmailTemplateParam,
                            Parameter = x.Parametro,
                            Description = x.Descripcion,
                            FriendlyName = x.FriendlyName,
                            Placeholder = x.Placeholder,
                            Help = x.Help,
                            InputType = inputType.Tipo,
                            HtmlFragment = inputType.HtmlFragment
                        });
                    });

                    templates.Add(new Template {
                        Id = et.IdEmailTemplate,
                        Name = et.Template,
                        Description = et.Descripcion,
                        Tipo = et.Tipo,
                        Parameters = templateParams
                    });
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return templates;
        }

        public static List<TB_TemplateParamInputTypes> GetAllTemplateParamInputTypes()
        {
            List<TB_TemplateParamInputTypes> tbTemplateParamInputTypes = new List<TB_TemplateParamInputTypes>();

            try
            {
                tbTemplateParamInputTypes = dal.GetTableTemplateParamInputTypes();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tbTemplateParamInputTypes;
        }

        public static List<TB_SmartTypes> GetAllSmartTypes()
        {
            List<TB_SmartTypes> tbSmartTypes = new List<TB_SmartTypes>();

            try
            {
                tbSmartTypes = dal.GetTableSmartTypes();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tbSmartTypes;
        }
    }
}
