﻿using SOPF.Core.DataAccess.Hubble;
using SOPF.Core.Entities.Hubble;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.BusinessLogic.Hubble
{
    public static class SmartEmailScheduleBll
    {
        private static SmartEmailScheduleDAL dal;

        static SmartEmailScheduleBll()
        {
            dal = new SmartEmailScheduleDAL();
        }

        public static bool CreateSmartEmailSchedule(
            string nombre,
            string descripcion,
            string asunto,
            DateTime horarioGeneracion,
            DateTime horarioEnvio,
            DateTime fechaUltimaGeneracion,
            bool archivosEnZip,
            int emailTemplateId,
            int emailAccountId,
            int smartType,
            int usuarioId,
            List<ReqTemplateParam> emailParams,
            List<string> attachments
        )
        {
            return dal.CreateSmartEmailSchedule(nombre, descripcion, asunto, horarioGeneracion, horarioEnvio, fechaUltimaGeneracion, archivosEnZip, emailTemplateId, emailAccountId, smartType, usuarioId, emailParams, attachments);
        }
    }
}
