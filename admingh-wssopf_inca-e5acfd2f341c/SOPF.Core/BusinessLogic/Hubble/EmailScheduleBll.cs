﻿using SOPF.Core.DataAccess.Hubble;
using SOPF.Core.Entities.Hubble;
using System;
using System.Collections.Generic;

namespace SOPF.Core.BusinessLogic.Hubble
{
    public static class EmailScheduleBll
    {
        private static EmailScheduleDAL dal;

        static EmailScheduleBll()
        {
            dal = new EmailScheduleDAL();
        }

        public static bool CreateEmailSchedule(
            string nombre,
            string descripcion,
            string asunto,
            DateTime fechaProgramacion,
            bool emailUnico,
            bool archivosEnZip,
            int emailTemplateId,
            int emailAccountId,
            int usuarioId,
            List<string> To,
            List<string> CC,
            List<string> BCC,
            List<ReqTemplateParam> emailParams,
            List<string> attachments
        )
        {
            return dal.CreateEmailSchedule(nombre, descripcion, asunto, fechaProgramacion, emailUnico, archivosEnZip, emailTemplateId, emailAccountId, usuarioId, To, CC, BCC, emailParams, attachments);
        }

        public static int UnsubscribeEmail(string email, string emailTemplate)
        {
            return dal.UnsubscribeEmail(email, emailTemplate);
        }
    }
}
