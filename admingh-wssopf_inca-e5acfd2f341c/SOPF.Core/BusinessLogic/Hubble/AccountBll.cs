﻿using SOPF.Core.DataAccess.Hubble;
using SOPF.Core.Poco.Hubble;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.BusinessLogic.Hubble
{
    public static class AccountBll
    {
        private static AccountDAL dal;

        static AccountBll()
        {
            dal = new AccountDAL();
        }

        public static List<TB_EmailAccounts> GetAllAccounts()
        {
            return dal.GetTableAccounts();
        }
    }
}
