﻿using iText.Kernel.Colors;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public partial class AmpliacionCreditoSimple
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private Image logoPF;
        private float tab = 10;
        private float fSizeNormal = 10;
        private AmpliacionCreditoVM datosAmp;

        public AmpliacionCreditoSimple(int version)
        {
            this.version = version <= 0 ? 1 : version;
            Firmas = new List<PosicionFirmaVM>();
            logoPF = AppSettings.LogPF;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
        }

        public byte[] Generar(AmpliacionCreditoVM datosAmpliacion)
        {
            byte[] ampliacion = null;
            this.datosAmp = datosAmpliacion;

            switch (version)
            {
                case 1:
                    ampliacion = AmpliacionV1();
                    break;
                case 2:
                    ampliacion = AmpliacionV2();
                    break;
            }
            return ampliacion;
        }

        private byte[] AmpliacionV1()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            firma.SetWidth(UnitValue.CreatePercentValue(100));
            try
            {
                if (datosAmp == null)
                    datosAmp = new AmpliacionCreditoVM();

                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.A4);
                        documento.SetMargins(50, 30, 30, 30);

                        #region Titulo
                        Table tContenido = new Table(UnitValue.CreatePercentArray(2)).UseAllAvailableWidth();
                        logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                        Cell celda = new Cell()
                            .Add(logoPF)
                            .SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);

                        celda = NuevaCelda("", arial, 8);
                        celda.SetBorder(Border.NO_BORDER);
                        AgregarCelda(tContenido, celda, false);

                        documento.Add(tContenido);

                        Paragraph parrafo = new Paragraph(Environment.NewLine + "AMPLIACIÓN DE PRESTAMOS SIMPLE")
                            .SetFont(arialNegrita)
                            .SetFontSize(10)
                            .SetTextAlignment(TextAlignment.CENTER);
                        documento.Add(parrafo);
                        #endregion

                        #region Nota
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10)
                            .SetFontColor(ColorConstants.RED);

                        Text texto = new Text("Nota Importante" + Environment.NewLine)
                            .SetFont(arialNegrita);
                        parrafo.Add(texto);

                        texto = new Text("Estimado Cliente: para continuar con la ampliación, deberá imprimir el presente documento, firmarlo, tomar una fotografía, y subirlo a través de nuestra aplicación.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .6f, 10 })).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("1.", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Derecha), false);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10)
                            .SetPaddingLeft(tab);
                        texto = new Text($"Datos del Cliente:{Environment.NewLine}")
                            .SetFont(arialNegrita);
                        parrafo.Add(texto);
                        texto = new Text($"Nombres y Apellidos: {datosAmp.Nombres} {datosAmp.ApellidoPaterno} {datosAmp.ApellidoMaterno}{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"DNI: {datosAmp.NumeroDocumento}");
                        parrafo.Add(texto);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);

                        AgregarCelda(tContenido, NuevaCelda("2.", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Derecha), false);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10)
                            .SetPaddingLeft(tab);
                        texto = new Text($"Datos de la ampliación del préstamo:{Environment.NewLine}")
                            .SetFont(arialNegrita);
                        parrafo.Add(texto);
                        texto = new Text($"Monto del Crédito Nuevo: {datosAmp.MontoCredito.ToString("C", cultPeru)}{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Monto de Liquidación del Préstamo anterior: {datosAmp.MontoLiquidacion.ToString("C", cultPeru)}{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Monto de Desembolso: {datosAmp.MontoDesembolso.ToString("C", cultPeru)}{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Total de Intereses: {datosAmp.TotalInteres.ToString("C", cultPeru)}{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Tasa de Interés (TEA): {datosAmp.TasaInteres:00.00} %{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Tasa de costo efectiva anual (TCEA): {datosAmp.TasaCostoEfectivoAnual:00.00} %{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Monto de la cuota: {datosAmp.Erogacion.ToString("C", cultPeru)}{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Plazo del crédito: {datosAmp.TotalRecibos}");
                        parrafo.Add(texto);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);

                        documento.Add(tContenido);
                        #endregion

                        #region Acuerdo
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10);
                        parrafo.Add("Préstamo Feliz y el Cliente acuerdan la Modificación del Préstamo en los términos detallados en el numeral 2. ");
                        parrafo.Add("El Cliente suscribe el presente documento en señal de conformidad y declarar estar de acuerdo con la recepción de la Hoja Resumen ");
                        parrafo.Add($"y Cronograma de Pagos del Préstamo a través del siguiente medio electrónico: {datosAmp.CorreoElectronico}");
                        parrafo.Add(Environment.NewLine + Environment.NewLine);
                        parrafo.Add("Todos los términos y condiciones que rigen el préstamo y que no hayan sido expresamente modificados por el presente documento, ");
                        parrafo.Add("mantendrán su plena validez, vigencia y eficacia.");
                        parrafo.Add(Environment.NewLine + Environment.NewLine);
                        parrafo.Add("Lima,");
                        documento.Add(parrafo);
                        #endregion

                        #region Firmas
                        tContenido = new Table(UnitValue.CreatePercentArray(3)).UseAllAvailableWidth();
                        tContenido.SetMarginTop(50);

                        tContenido.AddCell(new Cell().Add(new Paragraph("Aceptación a través de medios electrónicos")
                            .SetFont(arialNegrita).SetFontSize(8))
                            .SetVerticalAlignment(VerticalAlignment.BOTTOM)
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        Table tSubTabla = new Table(UnitValue.CreatePercentArray(3)).UseAllAvailableWidth();
                        tSubTabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tSubTabla.AddCell(new Cell().Add(firma).SetBorder(Border.NO_BORDER));
                        tSubTabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(tSubTabla));
                        AgregarCelda(tContenido, NuevaCelda("El Cliente", arial, fSizeNormal, true, false, false, false, Alineacion.Centro), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                        AgregarCelda(tContenido, NuevaCelda("Préstamo Feliz", arial, fSizeNormal, true, false, false, false, Alineacion.Centro), false);
                        documento.Add(tContenido);
                        #endregion

                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }
    }
}
