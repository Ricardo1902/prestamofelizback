﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Model.Gestiones;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class UltimaNotificacionPrejudicial
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private UltimaNotificacionPrejudicialVM datosNotificacion;
        private float fSizeNormal = 10;
        private List<string> datosDocumento = new List<string>();
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public UltimaNotificacionPrejudicial(int version)
        {
            this.version = version <= 0 ? 1 : version;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
        }

        public byte[] Generar(UltimaNotificacionPrejudicialVM notificacionDatos)
        {
            byte[] notificacionByte = null;
            datosNotificacion = notificacionDatos;
            switch (version)
            {
                case 1:
                    notificacionByte = UltimaNotificacionPrejudicialV1();
                    break;
            }
            return notificacionByte;
        }

        private byte[] UltimaNotificacionPrejudicialV1()
        {
            byte[] archivo = null;

            try
            {
                if (datosNotificacion == null) datosNotificacion = new UltimaNotificacionPrejudicialVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LETTER);
                        Encabezado encabezado = new Encabezado(version);
                        documento.SetMargins(75, 85, 75, 85);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        string dia = string.Empty;
                        string mes = string.Empty;
                        string anio = string.Empty;
                        string fecha = string.Empty;
                        dia = datosNotificacion.FechaRegistro.Day.ToString("00");
                        mes = Utils.MesFecha(datosNotificacion.FechaRegistro);
                        anio = datosNotificacion.FechaRegistro.Year.ToString();
                        fecha = dia + " de " + mes + " del " + anio;

                        #region Titulo
                        Paragraph parrafo = new Paragraph("Lima, " + fecha).SetFont(arial).SetFontSize(fSizeNormal);
                        documento.Add(parrafo);
                        parrafo = new Paragraph("ÚLTIMA NOTIFICACIÓN PREJUDICIAL").SetFont(arialNegrita).SetFontSize(16).SetTextAlignment(TextAlignment.CENTER).SetFontColor(ColorConstants.RED);
                        documento.Add(parrafo);
                        #endregion

                        #region Datos
                        parrafo = new Paragraph().SetFont(arialNegrita).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        Text texto = new Text("Señor (a) (ita): ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosNotificacion.NombreCliente).SetFont(arial);
                        texto = new Text(Environment.NewLine + (datosNotificacion.IdDestino != 2 ? "Dirección: " : "Dirección laboral: ")).SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosNotificacion.Direccion).SetFont(arial);
                        texto = new Text(Environment.NewLine + (datosNotificacion.IdDestino != 2 ? "Referencia Domiciliaria: " : "Dependencia y Ubicación Centro Laboral: ")).SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosNotificacion.ReferenciaDomicilio ?? "").SetFont(arial);
                        texto = new Text(Environment.NewLine + "Deuda Impaga a la fecha de la emisión de la carta (no incluye intereses moratorios): ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosNotificacion.TotalDeuda.ToString("C", cultPeru)).SetFont(arial);
                        texto = new Text(Environment.NewLine + "Numero de cuotas vencidas: ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosNotificacion.CuotasVencidas.ToString()).SetFont(arial);
                        texto = new Text(Environment.NewLine + "Código de cliente: ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosNotificacion.IdSolicitud.ToString()).SetFont(arial);
                        documento.Add(parrafo);
                        #endregion

                        #region Texto
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Nos dirigimos a Ud. vía esta última notificación para informarle que sus cuotas continuan " + 
                                        "aún pendiente de pago a pesar de nuestros reiterados avisos.Dado que la extensión de su " + 
                                        "crédito ya ha excedido toda regla de crédito otorgado a un cliente, ha puesto en serio riesgo " + 
                                        "su prestigio crediticio.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Como nuestra buena disposición no ha sido correspondida por parte suya, no cumpliendo " +
                                        "sus obligaciones de pago pendientes desde hace meses, nos vemos obligados a iniciar las " +
                                        "acciones pertinentes, por lo que la presente deberá ser considerada con notificación " +
                                        "prejudicial.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Le pedimos que efectúe el pago en un plazo de 48 horas a fin de evitar la continuación del " +
                                        "proceso.Esperamos contar con su cooperación para evitar tener que recurrir a medidas " + 
                                        "más drásticas en la protección de nuestros intereses.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("De no efectuarse el pago, nuestros abogados iniciarán el Proceso Judicial de conformidad " +
                        "con lo normado en los artículos 1219 del Código Civil y 727 del Código Procesal Civil; " +
                        "ejecutarán el PAGARÉ conforme la cláusula Quinta del Contrato y de la N° 27282 - Ley de " +
                         "Títulos Valores, y a su vez, solicitarán que el Juez encargado dicte una MEDIDA " +
                         "CAUTELAR, según lo también previsto en los artículos 611, 637, 641 y 642 del Código " +
                        "Procesal Civil, la misma que podrá recaer en los bienes muebles de su propiedad los que " +
                        "pueden ser objeto de remate(Art. 728 del Código Procesal Civil).");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Puede consultar mayor información a los teléfonos 914-684621 - 986-900939.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Sin otro particular, nos despedimos.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.CENTER);
                        texto = new Text("Atentamente,");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arialNegrita).SetFontSize(14).SetTextAlignment(TextAlignment.CENTER);
                        texto = new Text("PRÉSTAMO FELIZ EN 15 MINUTOS SAC");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion
                        
                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private int version;
            private Image logoPF;
            private PdfFont arial;
            private PdfFont arialNegrita;
            public int PaginaActual { get; set; }

            public Encabezado(int version)
            {
                this.version = version <= 0 ? 1 : version;
                logoPF = AppSettings.LogPF;
                arial = AppSettings.Arial;
                arialNegrita = AppSettings.ArialNegrita;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(82, paginaTamanio.GetTop() - 80, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }
        }

        protected class CellRadius : CellRenderer
        {
            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}
