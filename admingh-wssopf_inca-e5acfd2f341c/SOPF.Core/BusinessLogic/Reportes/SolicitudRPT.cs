﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class SolicitudRPT
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private SolicitudRptVM datosSolicitud;
        private float fSizeNormal = 8;
        private float fSizeTabla = 7;
        public List<PosicionFirmaVM> Firmas { get; set; }
        private List<string> datosDocumento = new List<string>();
        private iText.Kernel.Colors.Color colorAzulPeru = new DeviceRgb(16, 38, 79);
        private iText.Kernel.Colors.Color colorNormal = new DeviceRgb(System.Drawing.Color.Black);
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public SolicitudRPT(int version)
        {
            this.version = version <= 0 ? 1 : version;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
            Firmas = new List<PosicionFirmaVM>();
        }

        public byte[] Generar(SolicitudRptVM solicitudRPT)
        {
            byte[] estadoCuentaByte = null;
            datosSolicitud = solicitudRPT;
            switch (version)
            {
                case 1:
                    estadoCuentaByte = SolicitudRPTV1();
                    break;
            }
            return estadoCuentaByte;
        }

        private byte[] SolicitudRPTV1()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            firma.SetWidth(UnitValue.CreatePercentValue(30));
            //float punto = 0.35277f;
            float fSizeNormalS = 6;

            try
            {
                if (datosSolicitud == null) datosSolicitud = new SolicitudRptVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.A4);
                        Encabezado encabezado = new Encabezado(version, datosSolicitud.IdSolicitud);
                        documento.SetMargins(52, 35, 20, 35);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        // =====


                        //logoPF = AppSettings.LogPF;
                        //Image logoPF;                        
                        //logoPF = AppSettings.SolLogFec;
                        //logoPF.SetWidth(UnitValue.CreatePercentValue(15)); //62  
                        //logoPF.SetHeight(UnitValue.CreatePercentValue(15)); //62                                       
                        //logoPF.SetFixedPosition(58, 767);
                        //celda.Add(logoPF);

                        //logoPF.ScalePercent(200f);
                        //logoPF.SetTextAlignment(TextAlignment.LEFT);
                        //string imageFile = "C:/itextExamples/javafxLogo.jpg";
                        //ImageData data = ImageDataFactory.Create(imageFile);                    
                        //Image logoPF = new AppSettings.LogPF;
                        //celda.Add(logoPF.setWidthPercent(50));
                        //celda.Add(logoPF.SetAutoScale(true));
                        // =====

                        // Solicitud =============================================================================
                        Table tFila0 = new Table(UnitValue.CreatePercentArray(4)).UseAllAvailableWidth();
                        //tFila0 = new Table(UnitValue.CreatePercentArray(new float[] { .5f, .5f, .5f, .5f, .5f })).UseAllAvailableWidth();
                        tFila0 = new Table(UnitValue.CreatePercentArray(new float[] { 2f, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 })).UseAllAvailableWidth();
                        AgregarCelda(tFila0, NuevaCelda("SOLICITUD", arialNegrita, fSizeNormalS, true, true, true, true, 0, 12, Alineacion.Centro, PdfCelda.Color.WHITE, PdfCelda.Colorb.GRAY), false);
                        AgregarCelda(tFila0, NuevaCelda("Fecha:", arialNegrita, fSizeNormalS, true, false, true, false, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila0, NuevaCelda("Solicitud:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila0, NuevaCelda("Promotor de Ventas:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila0, NuevaCelda("Canal de Ventas:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila0, NuevaCelda(datosSolicitud.FechaSolicitud.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila0, NuevaCelda(datosSolicitud.IdSolicitud.ToString().Trim(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila0, NuevaCelda(datosSolicitud.Promotor.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila0, NuevaCelda(datosSolicitud.CanalVenta.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila0, NuevaCelda("Score Experian:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila0, NuevaCelda("Origen:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila0, NuevaCelda("Producto:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila0, NuevaCelda("SubProducto:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila0, NuevaCelda(datosSolicitud.ScoreExperian.ToString(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila0, NuevaCelda(datosSolicitud.Origen.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila0, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila0, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        tFila0.SetMarginTop(0);
                        documento.Add(tFila0);

                        //Image ICabS;
                        //ICabS = AppSettings.SolCab;
                        //ICabS.SetWidth(UnitValue.CreatePercentValue(3)); //62  
                        //ICabS.SetHeight(UnitValue.CreatePercentValue(3)); //62                                       
                        //ICabS.SetFixedPosition(317, 770);
                        //documento.Add(ICabS);

                        Image IFecS;
                        IFecS = AppSettings.SolLogFec;
                        IFecS.SetWidth(UnitValue.CreatePercentValue(2)); //62  
                        IFecS.SetHeight(UnitValue.CreatePercentValue(2)); //62                                       
                        IFecS.SetFixedPosition(58, 766);
                        documento.Add(IFecS);

                        Image ISolS;
                        ISolS = AppSettings.SolDoc;
                        ISolS.SetWidth(UnitValue.CreatePercentValue(2)); //62  
                        ISolS.SetHeight(UnitValue.CreatePercentValue(2)); //62                                       
                        ISolS.SetFixedPosition(197, 766);
                        documento.Add(ISolS);

                        // Datos del Cliente ===============================================================
                        Table tFila1 = new Table(UnitValue.CreatePercentArray(12)).UseAllAvailableWidth();
                        tFila1 = new Table(UnitValue.CreatePercentArray(new float[] { 2f, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 })).UseAllAvailableWidth();
                        //tFila1 = new Table(UnitValue.CreatePercentArray(new float[] { 2f, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 })).UseAllAvailableWidth();
                        AgregarCelda(tFila1, NuevaCelda("DATOS DEL CLIENTE", arialNegrita, fSizeNormalS, true, true, true, true, 0, 12, Alineacion.Centro, PdfCelda.Color.WHITE, PdfCelda.Colorb.GRAY), false);
                        AgregarCelda(tFila1, NuevaCelda("Nombres:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Apellido Paterno:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Apellido Materno:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Sexo:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Fecha:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.ClienteNombre.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.ClienteApePat.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.ClienteApeMat.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.Sexo.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.FechaNacimiento.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila1, NuevaCelda("Edad:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Estado Civil:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Numero de Dependientes:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Lugar de Nacimiento:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Nacionalidad:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.Edad.ToString().Trim(), arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.EstadoCivil.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.NumDepen.ToString(), arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.LugNac.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.Nacion.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila1, NuevaCelda("Tipo de Documento:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Número de Documento:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Celular 1:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Celular 2:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Correo Electrónico:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.TipoDocumento.Trim(), arial, 5, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.NumeroDocumento.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.Celular.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.Email, arial, fSizeNormalS, false, true, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Dirección de Cliente:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 12, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.Direccion, arial, fSizeNormalS, false, true, true, true, 0, 12, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Referencia de Domicilio:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 12, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.RefDom, arial, fSizeNormalS, false, true, true, true, 0, 12, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Departamento:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Provincia:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Distrito:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Tipo Residencia:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("Tiempo de Residencia en dicho Inmueble:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.Departamento.Trim(), arial, 5, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.Provincia.Trim(), arial, 5, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.Distrito.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda(datosSolicitud.TipRes.Trim(), arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila1, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        tFila1.SetMarginTop(0);
                        documento.Add(tFila1);

                        //Image ICabC;
                        //ICabC = AppSettings.CliCab;
                        //ICabC.SetWidth(UnitValue.CreatePercentValue(3)); //62  
                        //ICabC.SetHeight(UnitValue.CreatePercentValue(3)); //62                                       
                        //ICabC.SetFixedPosition(332, 714);
                        //documento.Add(ICabC);

                        Image IFecC;
                        IFecC = AppSettings.SolLogFec;
                        IFecC.SetWidth(UnitValue.CreatePercentValue(27)); //62  
                        IFecC.SetHeight(UnitValue.CreatePercentValue(27)); //62                                       
                        IFecC.SetFixedPosition(495, 700);
                        documento.Add(IFecC);

                        Image ICel1C;
                        ICel1C = AppSettings.CliCel;
                        ICel1C.SetWidth(UnitValue.CreatePercentValue(2)); //62  
                        ICel1C.SetHeight(UnitValue.CreatePercentValue(2)); //62                                       
                        ICel1C.SetFixedPosition(240, 648);
                        documento.Add(ICel1C);

                        Image ICel2C;
                        ICel2C = AppSettings.CliCel;
                        ICel2C.SetWidth(UnitValue.CreatePercentValue(3)); //62  
                        ICel2C.SetHeight(UnitValue.CreatePercentValue(3)); //62                                       
                        ICel2C.SetFixedPosition(328, 648);
                        documento.Add(ICel2C);


                        Image ICorC;
                        ICorC = AppSettings.CliCor;
                        ICorC.SetWidth(UnitValue.CreatePercentValue(8)); //62  
                        ICorC.SetHeight(UnitValue.CreatePercentValue(8)); //62                                       
                        ICorC.SetFixedPosition(444, 648);
                        documento.Add(ICorC);


                        Image IDirC;
                        IDirC = AppSettings.CliDir;
                        IDirC.SetWidth(UnitValue.CreatePercentValue(2)); //62  
                        IDirC.SetHeight(UnitValue.CreatePercentValue(2)); //62                                       
                        IDirC.SetFixedPosition(99, 619);
                        documento.Add(IDirC);


                        Image ITDocC;
                        ITDocC = AppSettings.CliTDoc;
                        ITDocC.SetWidth(UnitValue.CreatePercentValue(2)); //62  
                        ITDocC.SetHeight(UnitValue.CreatePercentValue(2)); //62                                       
                        ITDocC.SetFixedPosition(97, 647);
                        documento.Add(ITDocC);

                        Image INDeC;
                        INDeC = AppSettings.CliNde;
                        INDeC.SetWidth(UnitValue.CreatePercentValue(3)); //62  
                        INDeC.SetHeight(UnitValue.CreatePercentValue(3)); //62                                       
                        INDeC.SetFixedPosition(288, 674);
                        documento.Add(INDeC);

                        // Datos Laborales ======================================================================
                        Table tFila2 = new Table(UnitValue.CreatePercentArray(12)).UseAllAvailableWidth();
                        tFila2 = new Table(UnitValue.CreatePercentArray(new float[] { 2f, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 })).UseAllAvailableWidth();
                        AgregarCelda(tFila2, NuevaCelda("DATOS LABORALES", arialNegrita, fSizeNormalS, true, true, true, true, 0, 12, Alineacion.Centro, PdfCelda.Color.WHITE, PdfCelda.Colorb.GRAY), false);

                        AgregarCelda(tFila2, NuevaCelda("RUC:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Nombre de Empleador/Empresa:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 6, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Situación Laboral:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.RUC, arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.Empresa, arial, fSizeNormalS, false, true, true, true, 0, 6, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.SituacionLaboral.ToString(), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Rubro Empresa:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Dependencia:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Ubicación:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 5, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("", arial, 5, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.Dependencia, arial, fSizeNormalS, false, true, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.UbicacionOfic, arial, fSizeNormalS, false, true, true, true, 0, 5, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila2, NuevaCelda("Referencia Ubicalidad:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 6, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Telefono:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Anexo:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Puesto:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 6, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.TelefonoLaboral, arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.AnexoLaboral, arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.Puesto, arial, 5, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila2, NuevaCelda("Antigüedad Laboral:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Remuneración Bruta:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Bonificación no Permanente:", arialNegrita, 5, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Ingreso de CAFAE:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Descuentos de Ley:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.AntiLaboral.ToString(), arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.IngresoBruto.ToString("C",cultPeru), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.DescuentosLey.ToString("C",cultPeru), arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila2, NuevaCelda("Otros Descuentos:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Ingreso Neto Mensual:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Ingresos Adicionales:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("Procedencia de Ingresos Adicionales:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda("¿Es Cliente PEP?", arialNegrita, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.OtrosDsctos.ToString("C", cultPeru), arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.IngresoNeto.ToString("C",cultPeru), arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.Otros_Ingr.ToString("C", cultPeru), arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.ProOIng, arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila2, NuevaCelda(datosSolicitud.PEP, arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        tFila2.SetMarginTop(0);
                        documento.Add(tFila2);

                        //Image ICabL;
                        //ICabL = AppSettings.LabCab;
                        //ICabL.SetWidth(UnitValue.CreatePercentValue(3)); //62  
                        //ICabL.SetHeight(UnitValue.CreatePercentValue(3)); //62                                       
                        //ICabL.SetFixedPosition(331, 541);
                        //documento.Add(ICabL);

                        Image IEmpL;
                        IEmpL = AppSettings.LabEmp;
                        IEmpL.SetWidth(UnitValue.CreatePercentValue(3)); //62  
                        IEmpL.SetHeight(UnitValue.CreatePercentValue(3)); //62                                       
                        IEmpL.SetFixedPosition(265, 528);
                        documento.Add(IEmpL);

                        Image IDirL;
                        IDirL = AppSettings.CliDir;
                        IDirL.SetWidth(UnitValue.CreatePercentValue(5)); //62  
                        IDirL.SetHeight(UnitValue.CreatePercentValue(5)); //62                                       
                        IDirL.SetFixedPosition(375, 500);
                        documento.Add(IDirL);

                        // Información de Conyuge ======================================================
                        Table tFila3 = new Table(UnitValue.CreatePercentArray(12)).UseAllAvailableWidth();
                        tFila3 = new Table(UnitValue.CreatePercentArray(new float[] { 2f, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 })).UseAllAvailableWidth();
                        AgregarCelda(tFila3, NuevaCelda("INFORMACIÓN DE CONYUGUE", arialNegrita, fSizeNormalS, true, true, true, true, 0, 12, Alineacion.Centro, PdfCelda.Color.WHITE, PdfCelda.Colorb.GRAY), false);

                        AgregarCelda(tFila3, NuevaCelda("Tipo de Documento Identidad:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila3, NuevaCelda("Número de Documento Identidad:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);                        
                        AgregarCelda(tFila3, NuevaCelda("Teléfono/Celular:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila3, NuevaCelda(datosSolicitud.ConyTDoc, arial, fSizeNormalS, false, true, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila3, NuevaCelda(datosSolicitud.ConyNDoc, arial, fSizeNormalS, false, true, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);                        
                        AgregarCelda(tFila3, NuevaCelda(datosSolicitud.cony_tele, arial, fSizeNormalS, false, true, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila3, NuevaCelda("Nombres y Apellidos:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 12, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila3, NuevaCelda(datosSolicitud.conyuge, arial, fSizeNormalS, false, true, true, true, 0, 12, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        //AgregarCelda(tFila3, NuevaCelda("Tipo de Ingresos:", arial, fSizeNormalS, true, false, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        //AgregarCelda(tFila3, NuevaCelda("Empresa donde Trabaja:", arial, fSizeNormalS, true, false, true, true, 0, 5, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        //AgregarCelda(tFila3, NuevaCelda("Rubro de Empresa:", arial, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        //AgregarCelda(tFila3, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        //AgregarCelda(tFila3, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 5, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        //AgregarCelda(tFila3, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        //AgregarCelda(tFila3, NuevaCelda("N° de RUC/RUS :", arial, fSizeNormalS, true, false, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        //AgregarCelda(tFila3, NuevaCelda("Teléfono de Trabajo:", arial, fSizeNormalS, true, false, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        //AgregarCelda(tFila3, NuevaCelda("Ingreso Neto Mensual:", arial, fSizeNormalS, true, false, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        //AgregarCelda(tFila3, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        //AgregarCelda(tFila3, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        //AgregarCelda(tFila3, NuevaCelda("", arial, fSizeNormalS, false, true, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        tFila3.SetMarginTop(0);
                        documento.Add(tFila3);

                        //Image ICabCo;
                        //ICabCo = AppSettings.ConCab;
                        //ICabCo.SetWidth(UnitValue.CreatePercentValue(3)); //62  
                        //ICabCo.SetHeight(UnitValue.CreatePercentValue(3)); //62                                       
                        //ICabCo.SetFixedPosition(346, 386);
                        //documento.Add(ICabCo);

                        Image ICelCo;
                        ICelCo = AppSettings.CliCel;
                        ICelCo.SetWidth(UnitValue.CreatePercentValue(7)); //62  
                        ICelCo.SetHeight(UnitValue.CreatePercentValue(7)); //62                                       
                        ICelCo.SetFixedPosition(437, 380);
                        documento.Add(ICelCo);


                        Image ITDocCo;
                        ITDocCo = AppSettings.CliTDoc;
                        ITDocCo.SetWidth(UnitValue.CreatePercentValue(2)); //62  
                        ITDocCo.SetHeight(UnitValue.CreatePercentValue(2)); //62                                       
                        ITDocCo.SetFixedPosition(125, 381);
                        documento.Add(ITDocCo);

                        // Referencia Laborales ======================================================================
                        Table tFila4 = new Table(UnitValue.CreatePercentArray(12)).UseAllAvailableWidth();
                        tFila4 = new Table(UnitValue.CreatePercentArray(new float[] { 2f, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 })).UseAllAvailableWidth();
                        AgregarCelda(tFila4, NuevaCelda("REFERENCIA LABORALES Y/O PERSONALES", arialNegrita, fSizeNormalS, true, true, true, true, 0, 12, Alineacion.Centro, PdfCelda.Color.WHITE, PdfCelda.Colorb.GRAY), false);

                        AgregarCelda(tFila4, NuevaCelda("Referencia 1:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 6, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila4, NuevaCelda("Parentesco:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila4, NuevaCelda("Celular:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila4, NuevaCelda(datosSolicitud.Ref1Nom, arial, fSizeNormalS, false, true, true, true, 0, 6, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila4, NuevaCelda(datosSolicitud.Ref1Pare, arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila4, NuevaCelda(datosSolicitud.Ref1Tel, arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila4, NuevaCelda("Referencia 2:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 6, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila4, NuevaCelda("Parentesco:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila4, NuevaCelda("Celular:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila4, NuevaCelda(datosSolicitud.Ref2Nom, arial, fSizeNormalS, false, true, true, true, 0, 6, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila4, NuevaCelda(datosSolicitud.Ref2Pare, arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila4, NuevaCelda(datosSolicitud.Ref2Tel, arial, fSizeNormalS, false, true, true, true, 0, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        tFila4.SetMarginTop(0);
                        documento.Add(tFila4);

                        //Image ICabRe;
                        //ICabRe = AppSettings.RefCab;
                        //ICabRe.SetWidth(UnitValue.CreatePercentValue(4)); //62  
                        //ICabRe.SetHeight(UnitValue.CreatePercentValue(4)); //62                                       
                        //ICabRe.SetFixedPosition(367, 318);
                        //documento.Add(ICabRe);

                        Image ICelRe1;
                        ICelRe1 = AppSettings.CliCel;
                        ICelRe1.SetWidth(UnitValue.CreatePercentValue(8)); //62  
                        ICelRe1.SetHeight(UnitValue.CreatePercentValue(8)); //62                                       
                        ICelRe1.SetFixedPosition(454, 315);
                        documento.Add(ICelRe1);

                        Image ICelRe2;
                        ICelRe2 = AppSettings.CliCel;
                        ICelRe2.SetWidth(UnitValue.CreatePercentValue(8)); //62  
                        ICelRe2.SetHeight(UnitValue.CreatePercentValue(8)); //62                                       
                        ICelRe2.SetFixedPosition(454, 288);
                        documento.Add(ICelRe2);

                        // Datos del Credito ======================================================================
                        Table tFila5 = new Table(UnitValue.CreatePercentArray(12)).UseAllAvailableWidth();
                        tFila5 = new Table(UnitValue.CreatePercentArray(new float[] { 2f, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 })).UseAllAvailableWidth();
                        AgregarCelda(tFila5, NuevaCelda("DATOS DEL CREDITO", arialNegrita, fSizeNormalS, true, true, true, true, 0, 12, Alineacion.Centro, PdfCelda.Color.WHITE, PdfCelda.Colorb.GRAY), false);

                        AgregarCelda(tFila5, NuevaCelda("Importe del Crédito (S/)", arialNegrita, fSizeNormalS, true, false, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK, PdfCelda.Colorb.WHITE), false);

                        AgregarCelda(tFila5, NuevaCelda("Plazo:", arialNegrita, fSizeNormalS, true, false, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila5, NuevaCelda("Monto de Cuota (S/):", arialNegrita, fSizeNormalS, true, false, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila5, NuevaCelda(datosSolicitud.ImpCred.ToString("C",cultPeru), arial, fSizeNormalS, false, true, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila5, NuevaCelda(datosSolicitud.PlaCred.ToString(), arial, fSizeNormalS, false, true, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila5, NuevaCelda(datosSolicitud.MtoCuota.ToString("C",cultPeru), arial, fSizeNormalS, false, true, true, true, 0, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);


                        tFila5.SetMarginTop(0);
                        documento.Add(tFila5);

                        //Image ICabCre;
                        //ICabCre = AppSettings.CreCab;
                        //ICabCre.SetWidth(UnitValue.CreatePercentValue(4)); //62  
                        //ICabCre.SetHeight(UnitValue.CreatePercentValue(4)); //62                                       
                        //ICabCre.SetFixedPosition(332, 251);
                        //documento.Add(ICabCre);

                        // Texto Legal ======================================================================
                        Table tFila6 = new Table(UnitValue.CreatePercentArray(12)).UseAllAvailableWidth();
                        tFila6 = new Table(UnitValue.CreatePercentArray(new float[] { 2f, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 })).UseAllAvailableWidth();
                        //AgregarCelda(tFila6, NuevaCelda("TEXTO LEGAL", arialNegrita, fSizeNormalS, true, true, true, true, 0, 12, Alineacion.Centro, PdfCelda.Color.WHITE, PdfCelda.Colorb.GRAY), false);
                      
                        AgregarCelda(tFila6, NuevaCelda("Mediante la firma de la presente solicitud:", arial, fSizeNormalS, false, false, true, true, 0, 12, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila6, NuevaCelda("Autorizo el tratamiento de mis datos personales para fines comerciales, conforme a lo establecido en nuestra página web https://prestamofeliz.pe", arial, fSizeNormalS, false, false, true, true, 0, 12, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila6, NuevaCelda("Declaro aceptar que, en caso de incurrir en mora, Préstamo Feliz podrá aplicar las máximas tasas legales permitidas a los intereses moratorios y compensatorios correspondientes. ", arial, fSizeNormalS, false, false, true, true, 0, 12, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila6, NuevaCelda("Declaro que los datos contenidos en la presente Solicitud son verdaderos; sin perjuicio de ello, declaro conocer que Préstamo Feliz se reserva el derecho a no contratar en caso toda o parte de la información proporcionada fuera falsa, o, tratándose de documentos, éstos hubieran sido adulterados o alterados", arial, fSizeNormalS, false, false, true, true, 0, 12, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila6, NuevaCelda("Me comprometo a comunicar en forma inmediata a Préstamo Feliz cualquier cambio en los datos consignados en la presente Solicitud. Finalmente, declaro que los recursos con los cuales he de pagar los servicios o productos recibidos, así como las obligaciones contraídas han sido obtenidos o generados a través de una fuente de origen lícito. El destino de los servicios o productos adquiridos será dedicado tan solo a fines permitidos por la ley.", arial, fSizeNormalS, false, false, true, true, 0, 12, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        AgregarCelda(tFila6, NuevaCelda("Otorgo mi previo y expreso consentimiento a que las comunicaciones de cobranza por falta de pago se notifiquen en cualquiera de las direcciones aquí indicadas, incluyendo mi centro de labores.", arial, fSizeNormalS, false, true, true, true, 0, 12, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);

                        tFila6.SetMarginTop(0);
                        documento.Add(tFila6);

                        //Image ICabLeg;
                        //ICabLeg = AppSettings.LegCab;
                        //ICabLeg.SetWidth(UnitValue.CreatePercentValue(4)); //62  
                        //ICabLeg.SetHeight(UnitValue.CreatePercentValue(4)); //62                                       
                        //ICabLeg.SetFixedPosition(323, 211);
                        //documento.Add(ICabLeg);


                        // Firma ======================================================================
                        Table tFila7 = new Table(UnitValue.CreatePercentArray(12)).UseAllAvailableWidth();
                        tFila7 = new Table(UnitValue.CreatePercentArray(new float[] { 2f, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 })).UseAllAvailableWidth();
                        AgregarCelda(tFila7, NuevaCelda("___________________________", arial, fSizeNormalS, false, false, false, false, 0, 12, Alineacion.Centro, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tFila7, NuevaCelda("Firma del Cliente", arial, fSizeNormalS, false, false, false, false, 0, 12, Alineacion.Centro, PdfCelda.Color.BLACK), false);
                        tFila7.SetMarginTop(10);
                        documento.Add(tFila7);
                        //========================================================================================


                        #region Info
                        ////Table tContenido = new Table(UnitValue.CreatePercentArray(4)).UseAllAvailableWidth();
                        ////tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .25f, .25f, .25f, .25f })).UseAllAvailableWidth();
                        ////AgregarCelda(tContenido, NuevaCelda("Solicitud", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda("Cliente", arial, fSizeNormal, false, false, false, false, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda("Fecha Crédito", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.IdSolicitud.ToString(), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.Cliente, arial, fSizeNormal, true, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.FechaCredito, arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda("Monto Uso Canal", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda("Monto GAT", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda("Cuota", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda("Capital", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.MontoUsoCanal.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.MontoGAT.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.Cuota.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.Capital.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda("Producto", arial, fSizeNormal, false, false, false, false, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda("Saldo Vencido", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda("Costo Total Crédito", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.Producto, arial, fSizeNormal, true, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.SaldoVencido.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.CostoTotalCredito.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda("Tipo Credito", arial, fSizeNormal, true, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda("Monto Comisión Uso de Canal", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                        ////AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                        ////AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.TipoCredito, arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.MontoComisionUsoCanal.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        ////tContenido.SetMarginTop(20);
                        ////documento.Add(tContenido);
                        //datosDocumento.Add("EstadoCuenta");
                        #endregion

                        documento.Add(new Paragraph().Add(Environment.NewLine));                        

                        documento.Close();
                    }

                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }

            return archivo;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private Image logoPF;
            private decimal version;
            private string Folio;
            private float fSizeNormal = 9;
            private iText.Kernel.Colors.Color colorNormal = new DeviceRgb(16, 38, 79);
            public int PaginaActual { get; set; }

            private PdfFont arial;
            private PdfFont arialNegrita;

            public Encabezado(decimal version, int Folio)
            {
                this.version = version <= 0 ? 1 : version;
                arial = AppSettings.Arial;
                arialNegrita = AppSettings.ArialNegrita;
                this.Folio = Folio <= 0 ? string.Empty : Folio.ToString();
                logoPF = AppSettings.LogPF;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF.SetWidth(UnitValue.CreatePercentValue(55)); //62
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(3));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 80, paginaTamanio.GetWidth() - 60);
                //==========================================================================================
                Paragraph p = new Paragraph("")
                        .SetFont(arialNegrita)
                        .SetFontSize(6)
                        .SetTextAlignment(TextAlignment.CENTER);
                Text t1 = new Text("SOLICITUD DE CRÉDITO SIMPLE"); //Text(Folio);                
                Text t2 = new Text(Environment.NewLine + "PRÉSTAMO FELIZ EN 15 MINUTOS S.A.C.");                
                p.Add(t1).Add(t2);
                tEncabezado.AddCell(new Cell()
                                   .Add(p)
                                   .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                                   .SetBorder(Border.NO_BORDER));
                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 50, paginaTamanio.GetWidth() - 60);
                //==========================================================================================
                Paragraph p1 = new Paragraph("")
                        //.SetFont(arial)
                        //.SetFontSize(6)
                        .SetTextAlignment(TextAlignment.LEFT);                
                Text t3 = new Text(" "); //Text(Folio);
                //t1.SetFontColor(ColorConstants.RED);
                //Text t4 = new Text(Environment.NewLine +  "N° DE SOLICITUD : "+ Folio);
                //t4.SetFontColor(colorNormal);
                p1.Add(t3);
                tEncabezado.AddCell(new Cell()
                                   .Add(p1)
                                   .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                                   .SetBorder(Border.NO_BORDER));
                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 50, paginaTamanio.GetWidth() - 60);
                //==========================================================================================
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }

            public void FijarPaginado(Document document, string lugar, string folio)
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                int totalPaginas = pdf.GetNumberOfPages();
                Paragraph paginas;
                switch (version)
                {
                    case 1:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"{lugar} N° {folio}").SetFont(arialNegrita).SetFontSize(7).SetFontColor(ColorConstants.RED);
                            document.ShowTextAligned(paginas, paginaTamanio.GetWidth() - 90, paginaTamanio.GetTop() - 45, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                }
            }
        }

        protected class CellRadius : CellRenderer
        {
            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}
