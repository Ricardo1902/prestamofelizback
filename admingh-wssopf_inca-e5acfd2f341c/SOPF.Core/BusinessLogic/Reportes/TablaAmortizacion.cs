﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class TablaAmortizacion
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private CronogramaPagos datosCronograma;
        private float fSizeNormal = 7;
        private const float punto = 0.35277f;
        private List<string> datosDocumento = new List<string>();
        public List<PosicionFirmaVM> Firmas { get; set; }
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public string Titulo { get; set; }
        public bool EsExtrajudicial;
        public int IdSolicitudNuevo { get; set; }

        public TablaAmortizacion(int version)
        {
            this.version = version <= 0 ? 1 : version;
            Firmas = new List<PosicionFirmaVM>();
        }

        public byte[] Generar(CronogramaPagos cronogramaPagos)
        {
            byte[] tablaByte = null;
            datosCronograma = cronogramaPagos;

            switch (version)
            {
                case 1:
                    tablaByte = AmortizacionV1();
                    break;
            }
            return tablaByte;
        }

        private byte[] AmortizacionV1()
        {
            byte[] archivo = null;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;

            try
            {
                if (datosCronograma == null) datosCronograma = new CronogramaPagos();
                int totalRecibos = datosCronograma.Recibos == null ? 0 : datosCronograma.Recibos.Where(r => r.NoRecibo != "-").ToList().Count;
                decimal MontoComision = datosCronograma.Recibos.FirstOrDefault(r => r.NoRecibo == "-")?.OtrosCargos ?? 0;

                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL);
                        Encabezado encabezado = new Encabezado(version, EsExtrajudicial, IdSolicitudNuevo);
                        documento.SetMargins(50, 30, 30, 30);

                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        #region Cronograma
                        Table tCronograma = new Table(UnitValue.CreatePercentArray(new float[] { 1 })).UseAllAvailableWidth();
                        tCronograma.SetMarginTop(10);
                        tCronograma.SetMarginBottom(10);
                        AgregarCelda(tCronograma, NuevaCelda(string.IsNullOrEmpty(Titulo) ? "Cronograma de Pagos" : Titulo, arial, 14, Alineacion.Centro), false);

                        documento.Add(tCronograma);
                        #endregion

                        #region DatosCliente
                        Table tDatosCte = new Table(UnitValue.CreatePercentArray(new float[] { 4, 1, 4, 1, 4, 1, 4 })).UseAllAvailableWidth();
                        tDatosCte.SetMarginBottom(10);
                        Cell celda;

                        AgregarCelda(tDatosCte, NuevaCelda("Cliente", arialNegrita, fSizeNormal, false, false, false, false, 0, 5, Alineacion.Izquierda), false);
                        AgregarCelda(tDatosCte, NuevaCelda("", arial, 6, false, false, false, false), false);
                        AgregarCelda(tDatosCte, NuevaCelda("DNI", arialNegrita, fSizeNormal, false, false, false, false), false);

                        celda = NuevaCelda($"{datosCronograma.NombreCliente} {datosCronograma.ApPaternoCliente} {datosCronograma.ApMaternoCliente}", arial, fSizeNormal, 0, 5);
                        celda.SetNextRenderer(new CellRadius(celda));
                        celda.SetBorder(Border.NO_BORDER);
                        celda.SetPadding(5);
                        AgregarCelda(tDatosCte, celda, false);
                        datosDocumento.Add($"{datosCronograma.NombreCliente} {datosCronograma.ApPaternoCliente} {datosCronograma.ApMaternoCliente}");

                        AgregarCelda(tDatosCte, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);

                        celda = NuevaCelda(datosCronograma.DNICliente, arial, 7);
                        celda.SetNextRenderer(new CellRadius(celda));
                        celda.SetBorder(Border.NO_BORDER);
                        celda.SetPadding(5);
                        AgregarCelda(tDatosCte, celda, false);
                        datosDocumento.Add(datosCronograma.DNICliente);

                        AgregarCelda(tDatosCte, NuevaCelda("Dirección", arialNegrita, fSizeNormal, false, false, false, false, 0, 7, Alineacion.Izquierda), false);

                        celda = NuevaCelda(datosCronograma.DireccionCliente, arial, fSizeNormal, 0, 7);
                        celda.SetNextRenderer(new CellRadius(celda));
                        celda.SetBorder(Border.NO_BORDER);
                        celda.SetPadding(5);
                        AgregarCelda(tDatosCte, celda, false);
                        datosDocumento.Add(datosCronograma.DireccionCliente);
                        if (!EsExtrajudicial)
                        {
                            AgregarCelda(tDatosCte, NuevaCelda("Monto del crédito", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                            AgregarCelda(tDatosCte, NuevaCelda("", arialNegrita, fSizeNormal, false, false, false, false), false);
                            AgregarCelda(tDatosCte, NuevaCelda("Tasa Efectiva Anual", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                            AgregarCelda(tDatosCte, NuevaCelda("", arialNegrita, fSizeNormal, false, false, false, false), false);
                            AgregarCelda(tDatosCte, NuevaCelda("Tasa Costo Efectiva Anual", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                            AgregarCelda(tDatosCte, NuevaCelda("", arialNegrita, fSizeNormal, false, false, false, false), false);
                            AgregarCelda(tDatosCte, NuevaCelda("Total Interés", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);

                            celda = NuevaCelda(datosCronograma.MontoDesembolsar.ToString("C", cultPeru), arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda);
                            celda.SetPadding(5);
                            celda.SetNextRenderer(new CellRadius(celda));
                            AgregarCelda(tDatosCte, celda, false);
                            AgregarCelda(tDatosCte, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                            celda = NuevaCelda($"{datosCronograma.TasaAnual:00.00} %", arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda);
                            celda.SetPadding(5);
                            celda.SetNextRenderer(new CellRadius(celda));
                            AgregarCelda(tDatosCte, celda, false);
                            AgregarCelda(tDatosCte, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                            celda = NuevaCelda($"{datosCronograma.TasaCostoAnual:00.00} %", arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda);
                            celda.SetPadding(5);
                            celda.SetNextRenderer(new CellRadius(celda));
                            AgregarCelda(tDatosCte, celda, false);
                            AgregarCelda(tDatosCte, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                            celda = NuevaCelda(datosCronograma.TotalInteres.ToString("C", cultPeru), arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda);
                            celda.SetPadding(5);
                            celda.SetNextRenderer(new CellRadius(celda));
                            AgregarCelda(tDatosCte, celda, false);
                            datosDocumento.Add(datosCronograma.MontoDesembolsar.ToString("C", cultPeru));
                            datosDocumento.Add($"{datosCronograma.TasaAnual:00.00} %");
                            datosDocumento.Add($"{datosCronograma.TasaCostoAnual:00.00} %");
                            datosDocumento.Add(datosCronograma.TotalInteres.ToString("C", cultPeru));

                            AgregarCelda(tDatosCte, NuevaCelda("Número de cuotas", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                            AgregarCelda(tDatosCte, NuevaCelda("", arialNegrita, fSizeNormal, false, false, false, false), false);
                            AgregarCelda(tDatosCte, NuevaCelda("Fecha 1er. Vcto.", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                            AgregarCelda(tDatosCte, NuevaCelda("", arialNegrita, fSizeNormal, false, false, false, false), false);
                            AgregarCelda(tDatosCte, NuevaCelda("Fecha Ult. Vcto.", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                            AgregarCelda(tDatosCte, NuevaCelda("", arialNegrita, fSizeNormal, false, false, false, false), false);
                            AgregarCelda(tDatosCte, NuevaCelda("Fecha Desembolso", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);

                            celda = NuevaCelda(totalRecibos.ToString(), arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda);
                            celda.SetPadding(5);
                            celda.SetNextRenderer(new CellRadius(celda));
                            AgregarCelda(tDatosCte, celda, false);
                            AgregarCelda(tDatosCte, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                            celda = NuevaCelda(datosCronograma.PrimerFechaPago.ToString("dd/MM/yyyy"), arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda);
                            celda.SetPadding(5);
                            celda.SetNextRenderer(new CellRadius(celda));
                            AgregarCelda(tDatosCte, celda, false);
                            AgregarCelda(tDatosCte, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                            celda = NuevaCelda(datosCronograma.UltimaFechaPago.ToString("dd/MM/yyyy"), arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda);
                            celda.SetPadding(5);
                            celda.SetNextRenderer(new CellRadius(celda));
                            AgregarCelda(tDatosCte, celda, false);
                            AgregarCelda(tDatosCte, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                            celda = NuevaCelda(datosCronograma.FechaDesembolso.ToString("dd/MM/yyyy"), arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda);
                            celda.SetPadding(5);
                            celda.SetNextRenderer(new CellRadius(celda));
                            AgregarCelda(tDatosCte, celda, false);

                            datosDocumento.Add(datosCronograma.PrimerFechaPago.ToString("dd/MM/yyyy"));
                            datosDocumento.Add(datosCronograma.UltimaFechaPago.ToString("dd/MM/yyyy"));                            
                        }

                        AgregarCelda(tDatosCte, NuevaCelda("Monto Comisión uso de canal", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        AgregarCelda(tDatosCte, NuevaCelda("", arialNegrita, fSizeNormal, false, false, false, false, 0, 6), false);

                        celda = NuevaCelda(MontoComision.ToString("C", cultPeru), arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda);
                        celda.SetPadding(5);
                        celda.SetNextRenderer(new CellRadius(celda));
                        AgregarCelda(tDatosCte, celda, false);
                        AgregarCelda(tDatosCte, NuevaCelda("", arialNegrita, fSizeNormal, false, false, false, false, 0, 6), false);


                        celda = NuevaCelda("", arialNegrita, fSizeNormal, false, true, false, false, 0, 7, Alineacion.Izquierda);
                        celda.SetHeight(10);
                        AgregarCelda(tDatosCte, celda, false);

                        documento.Add(tDatosCte);
                        #endregion

                        #region Recibos
                        Dictionary<string, string> tituloEntidad = new Dictionary<string, string> {
                            {"N° Cuota", "NoRecibo" },
                            {"Fecha", "FechaRecibo" },
                            {"Capital Inicial", "CapitalInicial" },
                            {"Capital Amortizado", "Capital" },
                            {"Interes", "Interes" },
                            {"IGV", "IGV" },
                            {"GAT", "GAT" },
                            {"Comisión", "OtrosCargos" },
                            {"IGV Comisión", "IGVOtrosCargos" },
                            {"Saldo Capital", "SaldoCapital" },
                            {"Monto/Cuota", "Cuota" }
                        };

                        Table tRecibos = new Table(UnitValue.CreatePercentArray(new float[] { 1.2f, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2 })).UseAllAvailableWidth();
                        tRecibos.SetMarginBottom(10);
                        foreach (var titulo in tituloEntidad.Select(t => t.Key))
                        {
                            AgregarCelda(tRecibos, NuevaCelda(titulo, arialNegrita, fSizeNormal, Alineacion.Izquierda), true);
                        }
                        if (datosCronograma.Recibos != null && datosCronograma.Recibos.Count > 0)
                        {
                            foreach (var recibo in datosCronograma.Recibos)
                            {
                                foreach (var campo in tituloEntidad.Select(t => t.Value))
                                {
                                    string valorCadena = string.Empty;
                                    decimal valorDecimal = 0;
                                    DateTime valorFecha = new DateTime();

                                    try
                                    {
                                        PropertyInfo propiedad = recibo.GetType().GetProperty(campo);
                                        if (propiedad != null)
                                        {

                                            if (propiedad.PropertyType == typeof(decimal))
                                            {
                                                decimal.TryParse(propiedad.GetValue(recibo, null).ToString(), out valorDecimal);
                                                valorCadena = valorDecimal.ToString("C", cultPeru);
                                            }
                                            else if (propiedad.PropertyType == typeof(DateTime))
                                            {
                                                DateTime.TryParse(propiedad.GetValue(recibo, null).ToString(), out valorFecha);
                                                valorCadena = valorFecha.ToString("dd/MM/yyyy");
                                            }
                                            else
                                            {
                                                valorCadena = propiedad.GetValue(recibo, null).ToString();
                                            }
                                        }
                                        AgregarCelda(tRecibos, NuevaCelda(valorCadena, arial, fSizeNormal, Alineacion.Izquierda), false);
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                            }
                        }
                        documento.Add(tRecibos);
                        #endregion

                        #region FinTabla
                        Paragraph parrafo = new Paragraph("Autorizo a que se realice el débito de automático de mis cuentas afiliadas u otras cuentas que mantenga en el sistema financiero " +
                            "y / o tarjeta de débito asociadas en la fecha pactada en el cronograma de pago, fecha de abono de haberes y/ o días posteriores. El cliente " +
                            "reconoce el derecho de Préstamo Feliz a gestionar en cualquier momento el cargo automático de cualquier acreencia vencida o por vencer, a través de la modalidad " +
                            "de cargo automático incluyendo abonos por concepto de CAFAE, gratificaciones, bonos, entre otros, sin reserva ni limitación alguna.")

                               .SetFont(arialNegrita).SetFontSize(8);
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Condiciones:").SetFont(arial).SetFontSize(8);
                        documento.Add(parrafo);
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(7);
                        parrafo.Add("- El desembolso del crédito y pago de cuotas generarán el cobro de ITF en la cuenta de origen del cliente.");
                        parrafo.Add(Environment.NewLine);
                        parrafo.Add("- Este cronograma se elabora bajo el supuesto del cumplimiento de pago de las cuotas en las fechas indicadas.");
                        documento.Add(parrafo);

                        Table tCondiciones = new Table(UnitValue.CreatePercentArray(new float[] { 1, 1, 1 })).UseAllAvailableWidth();
                        tCondiciones.SetMarginTop(50);
                        tCondiciones.SetMarginBottom(10);
                        AgregarCelda(tCondiciones, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                        AgregarCelda(tCondiciones, NuevaCelda("", arialNegrita, fSizeNormal, false, true, false, false, Alineacion.Centro), false);
                        AgregarCelda(tCondiciones, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                        celda = NuevaCelda("Firma del Cliente", arial, fSizeNormal, false, false, false, false, 0, 3, Alineacion.Centro);
                        celda.SetMarginTop(10);
                        AgregarCelda(tCondiciones, celda, false);
                        documento.Add(tCondiciones);

                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();
                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 240 * punto,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 100)) * punto
                            //PosX = 240, //Test
                            //PosY = (Utils.AltoAreaRestante(documento) + 100) //Test
                        });
                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0); //Test
                        #endregion

                        if (!EsExtrajudicial)
                            encabezado.FijarTotal(documento);
                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private Image logoPF;
            private int version;
            private bool esExtrajudicial;
            private int idSolicitudNuevo;
            private PdfFont arial;
            private PdfFont arialNegrita;
            private float fSizeNormal = 10;
            public int PaginaActual { get; set; }

            public Encabezado(int version, bool esExtrajudicial, int idSolicitudNuevo)
            {
                this.version = version <= 0 ? 1 : version;
                this.esExtrajudicial = esExtrajudicial;
                this.idSolicitudNuevo = idSolicitudNuevo;
                arial = AppSettings.Arial;
                arialNegrita = AppSettings.ArialNegrita;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF = AppSettings.LogPF;
                logoPF.SetWidth(UnitValue.CreatePercentValue(62));

                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 50, paginaTamanio.GetWidth() - 60);
                if (esExtrajudicial)
                {
                    Paragraph p = new Paragraph("")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.RIGHT);
                    Text t1 = new Text(idSolicitudNuevo.ToString());
                    t1.SetFontColor(ColorConstants.RED);
                    p.Add(t1);
                    tEncabezado.AddCell(new Cell()
                        .Add(p)
                        .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                        .SetBorder(Border.NO_BORDER));
                }
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }

            public void FijarTotal(Document document)
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                int totalPaginas = pdf.GetNumberOfPages();
                Paragraph paginas;
                switch (version)
                {
                    case 1:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"Página {i} de {totalPaginas}").SetFont(arial).SetFontSize(7);
                            document.ShowTextAligned(paginas, paginaTamanio.GetWidth() - 90, paginaTamanio.GetTop() - 31, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                }
            }
        }

        protected class CellRadius : CellRenderer
        {

            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}
