﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class AcuerdoPagare
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private AcuerdoPagareVM datosAcuerdo;
        private float fSizeNormal = 8;
        private iText.Kernel.Colors.Color negro = ColorConstants.BLACK;
        private iText.Kernel.Colors.Color azulPeru = new DeviceRgb(16, 38, 79);
        private const float punto = 0.35277f;
        private List<string> datosDocumento = new List<string>();
        public List<PosicionFirmaVM> Firmas { get; set; }

        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public AcuerdoPagare(int version)
        {
            this.version = version <= 0 ? 1 : version;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
            Firmas = new List<PosicionFirmaVM>();
        }

        public byte[] Generar(AcuerdoPagareVM acuerdoDatos)
        {
            byte[] acuerdoByte = null;
            datosAcuerdo = acuerdoDatos;
            switch (version)
            {
                case 1:
                    acuerdoByte = AcuerdoV1();
                    break;
            }
            return acuerdoByte;
        }

        private byte[] AcuerdoV1()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            firma.SetWidth(UnitValue.CreatePercentValue(50));

            try
            {
                if (datosAcuerdo == null) datosAcuerdo = new AcuerdoPagareVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL);
                        Encabezado encabezado = new Encabezado(version, datosAcuerdo.Folio);
                        documento.SetMargins(25, 35, 25, 35);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        #region Acuerdo
                        Table tContenido = new Table(UnitValue.CreatePercentArray(new float[] { 6.2f, 3.8f })).UseAllAvailableWidth();
                        tContenido.SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetMarginTop(40);
                        tContenido.AddCell(new Cell()
                            .Add(new Paragraph("Conste por el presente documento, el ACUERDO DE LLENADO DE PAGARE que suscriben:"))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new SolidBorder(0.5f)));
                        tContenido.AddCell(new Cell(0, 2)
                            .Add(new Paragraph(datosAcuerdo.Cliente))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new SolidBorder(0.5f)));
                        documento.Add(tContenido);
                        datosDocumento.Add(datosAcuerdo.Cliente);

                        Paragraph parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(negro).SetTextAlignment(TextAlignment.JUSTIFIED);

                        parrafo.Add("(en adelante denominado EL CLIENTE) " +
                            "y Préstamo Feliz S.A.C (en adelante, “Préstamo Feliz”). " + Environment.NewLine +
                            "EL CLIENTE ha solicitado a Préstamo Feliz uno ó más créditos, y en representación o como instrumento de cobro de dichas obligaciones, ha suscrito un " +
                            "Pagaré incompleto a favor de Préstamo Feliz; por lo que mediante la firma del presente documento, EL CLIENTE y Préstamo Feliz acuerdan de manera expresa " +
                            "e irrevocable, que Préstamo Feliz queda autorizado para proceder con el llenado del mencionado Pagaré incompleto en los siguientes términos y condiciones:");
                        documento.Add(parrafo);
                        #endregion

                        #region Primero
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetMarginTop(10);
                        Text texto = new Text(@"PRIMERO").SetFont(arialNegrita).SetUnderline(0.1f, -2f);
                        parrafo.Add(texto);
                        parrafo.Add(": AUTORIZACION DE LLENADO DE PAGARE" + Environment.NewLine +
                            "Préstamo Feliz podrá proceder al llenado del Pagaré y en consecuencia a la ejecución del mismo, y de ser el caso automáticamente vencerán todos los " +
                            "plazos, en cualquiera de los siguientes casos: " + Environment.NewLine +
                            "a Si EL CLIENTE incumpliese con el pago debido y oportuno de las obligaciones contraídas con Préstamo Feliz y representadas en el Pagaré." + Environment.NewLine +
                            "c Si EL CLIENTE es declarado en concurso, suspende sus pagos o acuerda disolverse o liquidarse. " + Environment.NewLine +
                            "d Si Préstamo Feliz detectase falsedad en la información suministrada por EL CLIENTE. " + Environment.NewLine +
                            "e Si EL CLIENTE de un modo general dejara de cumplir cualquiera de las obligaciones contraídas con Préstamo Feliz");
                        documento.Add(parrafo);
                        #endregion

                        #region Segundo
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetMarginTop(10);
                        texto = new Text(@"SEGUNDO:").SetFont(arialNegrita).SetUnderline(0.1f, -2f);
                        parrafo.Add(texto);
                        parrafo.Add(" INSTRUCCIONES DEL LLENADO DEL PAGARE INCOMPLETO");
                        parrafo.Add(new Paragraph("Préstamo Feliz queda autorizado a llenar el Pagaré de acuerdo a lo siguiente:"));
                        documento.Add(parrafo);

                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .01f, 1 })).UseAllAvailableWidth()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        tContenido.AddCell(new Cell()
                            .Add(new Paragraph("a"))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell()
                            .Add(new Paragraph("Fecha de emisión: Será la fecha de efectuado el desembolso o en la que se hubiera efectuado el último pago de intereses del crédito representado en el " +
                            "Pagaré, o aquella en la cual Préstamo Feliz proceda a hacer uso de las facultades otorgadas por el presente documento, a criterio de Préstamo Feliz."))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell()
                            .Add(new Paragraph("b"))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell()
                            .Add(new Paragraph("Fecha de vencimiento: Será la fecha de vencimiento del crédito representado en el Pagaré o la fecha en que Préstamo Feliz dé por vencidos todos los " +
                            "plazos de acuerdo a lo señalado en la Cláusula Primera, o podrá ser a la vista, a criterio de Préstamo Feliz."))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell()
                            .Add(new Paragraph("c"))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell()
                            .Add(new Paragraph("Importe: La cantidad que Préstamo Feliz deberá consignar en el documento será equivalente al saldo de capital en la fecha de emisión del Pagaré. " +
                            "Préstamo Feliz queda autorizado, de ser necesario, para colocar un sello en el reverso del Pagaré indicando el importe total adeudado en la fecha de " +
                            "llenado del Pagaré."))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell()
                            .Add(new Paragraph("d"))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell()
                            .Add(new Paragraph("Tasas de interés: Las tasas de interés compensatorio y moratorio fijadas por Préstamo Feliz."))
                            .SetBorder(Border.NO_BORDER));

                        documento.Add(tContenido);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add("EL CLIENTE acepta y da por válidas todas las renovaciones y prórrogas totales o parciales que se anoten en el Pagaré, aun cuando no estén suscritas por EL CLIENTE. " + Environment.NewLine +
                            "El presente documento constituye el acuerdo entre EL CLIENTE y Préstamo Feliz, al amparo del artículo 10º de la Ley de Títulos Valores (Ley 27287). Si EL " +
                            "CLIENTE hubiera suscrito un contrato con Préstamo Feliz, acordando condiciones distintas a las fijadas en este Acuerdo de Llenado de Pagaré, prevalecerán " +
                            "las condiciones del contrato.");
                        documento.Add(parrafo);
                        #endregion

                        #region Tercero
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetMarginTop(10);
                        texto = new Text(@"TERCERO").SetFont(arialNegrita).SetUnderline(0.1f, -2f);
                        parrafo.Add(texto);
                        parrafo.Add(": COPIA DEL PAGARE " + Environment.NewLine +
                            "EL CLIENTE deja expresa constancia que ha recibido de Préstamo Feliz copia del Pagaré incompleto, en el momento en que Préstamo Feliz recibió el " +
                            "mencionado Pagaré incompleto.");
                        documento.Add(parrafo);
                        #endregion

                        #region Cuarto
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetMarginTop(10);
                        texto = new Text(@"CUARTO").SetFont(arialNegrita).SetUnderline(0.1f, -2f);
                        parrafo.Add(texto);
                        parrafo.Add(": RENUNCIA A TRANSFERENCIA DEL PAGARE " + Environment.NewLine +
                            "EL CLIENTE renuncia expresamente a la inclusión de una cláusula que impida o limite la libre negociación del Pagaré.");
                        documento.Add(parrafo);
                        #endregion

                        #region Quinto
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetMarginTop(10);
                        texto = new Text(@"QUINTO").SetFont(arialNegrita).SetUnderline(0.1f, -2f);
                        parrafo.Add(texto);
                        parrafo.Add(": FIADORES SOLIDARIOS" + Environment.NewLine +
                            "Los Fiadores solidarios de EL CLIENTE aceptan todas y cada una de las condiciones y términos señalados en el presente documento. Asimismo, dejan " +
                            "constancia que la información proporcionada por EL CLIENTE y LOS FIADORES en el presente documento es completa y verdadera." + Environment.NewLine);
                        documento.Add(parrafo);

                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();

                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 32,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 25)) * punto
                        });

                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        #endregion

                        #region Cliente
                        parrafo = new Paragraph("EL CLIENTE").SetFont(arialNegrita).SetFontSize(fSizeNormal).SetMarginTop(10);
                        documento.Add(parrafo);

                        tContenido = new Table(UnitValue.CreatePercentArray(20)).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("Firma(s)", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 19, Alineacion.Izquierda), false);

                        AgregarCelda(tContenido, NuevaCelda("(para persona jurídica: representante(s) legal(es) del cliente / para persona natural: cliente y cónyuge)", arial,
                            fSizeNormal, false, false, false, false, 0, 20, Alineacion.Izquierda), false);

                        AgregarCelda(tContenido, NuevaCelda("Razón Social / Nombre Cliente", arial, fSizeNormal, false, false, false, false, 0, 4, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda(datosAcuerdo.Cliente, arial, fSizeNormal, false, true, false, false, 0, 16, Alineacion.Izquierda), false);

                        AgregarCelda(tContenido, NuevaCelda("DNI", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda(datosAcuerdo.DNI, arial, fSizeNormal, false, true, false, false, 0, 6, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("Estado Civil", arial, fSizeNormal, false, false, false, false, 0, 2, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda(datosAcuerdo.EstadoCivil, arial, fSizeNormal, false, true, false, false, 0, 11, Alineacion.Izquierda), false);
                        datosDocumento.Add(datosAcuerdo.DNI);
                        datosDocumento.Add(datosAcuerdo.EstadoCivil);

                        AgregarCelda(tContenido, NuevaCelda("Domicilio", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda(datosAcuerdo.Domicilio, arial, fSizeNormal, false, true, false, false, 0, 19, Alineacion.Izquierda), false);
                        datosDocumento.Add(datosAcuerdo.Domicilio);

                        AgregarCelda(tContenido, NuevaCelda("Nombre representante legal / cónyuge", arial, fSizeNormal, false, false, false, false, 0, 5, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 8, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("DNI", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 6, Alineacion.Izquierda), false);

                        AgregarCelda(tContenido, NuevaCelda("Nombre representante legal", arial, fSizeNormal, false, false, false, false, 0, 4, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 9, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("DNI", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 6, Alineacion.Izquierda), false);

                        documento.Add(tContenido);
                        #endregion

                        #region Fiador
                        parrafo = new Paragraph("FIADOR").SetFont(arialNegrita).SetFontSize(fSizeNormal).SetMarginTop(10);
                        documento.Add(parrafo);

                        tContenido = new Table(UnitValue.CreatePercentArray(20)).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("Firma(s)", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 19, Alineacion.Izquierda), false);

                        AgregarCelda(tContenido, NuevaCelda("(para persona jurídica: representante(s) legal(es) del cliente / para persona natural: cliente y cónyuge)", arial,
                            fSizeNormal, false, false, false, false, 0, 20, Alineacion.Izquierda), false);

                        AgregarCelda(tContenido, NuevaCelda("Razón Social / Nombre Fiador", arial, fSizeNormal, false, false, false, false, 0, 4, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 16, Alineacion.Izquierda), false);

                        AgregarCelda(tContenido, NuevaCelda("DNI", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 6, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("Estado Civil", arial, fSizeNormal, false, false, false, false, 0, 2, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 11, Alineacion.Izquierda), false);

                        AgregarCelda(tContenido, NuevaCelda("Domicilio", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 19, Alineacion.Izquierda), false);

                        AgregarCelda(tContenido, NuevaCelda("Nombre representante legal / cónyuge", arial, fSizeNormal, false, false, false, false, 0, 5, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 8, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("DNI", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 6, Alineacion.Izquierda), false);

                        AgregarCelda(tContenido, NuevaCelda("Nombre representante legal", arial, fSizeNormal, false, false, false, false, 0, 4, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 9, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("DNI", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false, 0, 6, Alineacion.Izquierda), false);

                        documento.Add(tContenido);
                        #endregion

                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private Image logoPF;
            private decimal version;
            private string Folio;
            private float fSizeNormal = 10;
            private iText.Kernel.Colors.Color colorNormal = new DeviceRgb(16, 38, 79);
            public int PaginaActual { get; set; }

            private PdfFont arial;
            private PdfFont arialNegrita;

            public Encabezado(decimal version, int Folio)
            {
                this.version = version <= 0 ? 1 : version;
                arial = AppSettings.Arial;
                arialNegrita = AppSettings.ArialNegrita;
                this.Folio = Folio <= 0 ? string.Empty : Folio.ToString();
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF = AppSettings.LogPF;
                logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 80, paginaTamanio.GetWidth() - 60);
                Paragraph p = new Paragraph("")
                        .SetFont(arial)
                        .SetFontSize(fSizeNormal)
                        .SetTextAlignment(TextAlignment.RIGHT);
                Text t1 = new Text(Folio);
                t1.SetFontColor(ColorConstants.RED);
                Text t2 = new Text(Environment.NewLine + "Acuerdo de Llenado de Pagaré");
                t2.SetFontColor(colorNormal);
                p.Add(t1).Add(t2);
                tEncabezado.AddCell(new Cell()
                    .Add(p)
                    .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                    .SetBorder(Border.NO_BORDER));
                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 50, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }

            public void FijarPaginado(Document document, string lugar, string folio)
            {
                //TODO: ajustar metodo con mas de 3 paginas para folio puesto que aroja el error: "Cannot draw elements on already flushed pages."
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                int totalPaginas = pdf.GetNumberOfPages();
                Paragraph paginas;
                switch (version)
                {
                    case 1:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"{lugar} N° {folio}").SetFont(arialNegrita).SetFontSize(7).SetFontColor(ColorConstants.RED);
                            document.ShowTextAligned(paginas, paginaTamanio.GetWidth() - 90, paginaTamanio.GetTop() - 45, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                }
            }
        }

        protected class CellRadius : CellRenderer
        {
            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}
