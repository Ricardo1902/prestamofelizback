﻿using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using Newtonsoft.Json;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class DeclaracionJurada
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialN;
        private DeclaracionJuradaVM datosDeclaracion;
        private float fSizeNormal = 7;
        private float fSizeTitulo = 11;
        private float fixedLeading = 14;
        private const float punto = 0.35277f;
        private List<string> datosDocumento = new List<string>();
        public List<PosicionFirmaVM> Firmas { get; set; }
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public DeclaracionJurada(int version)
        {
            this.version = version <= 0 ? 1 : version;
            Firmas = new List<PosicionFirmaVM>();
            CultureInfo culture = new CultureInfo("es-PE");
            Thread.CurrentThread.CurrentCulture = culture;
            arial = AppSettings.Arial;
            arialN = AppSettings.ArialNegrita;
        }

        public byte[] Generar(DeclaracionJuradaVM declaracionI)
        {
            byte[] archivo = null;
            datosDeclaracion = declaracionI;

            switch (version)
            {
                case 1:
                    archivo = DeclaracionIrrevocabilidadV1();
                    break;
            }
            return archivo;
        }

        public byte[] DeclaracionIrrevocabilidadV1()
        {
            byte[] archivo = null;
            fSizeTitulo = 11;
            fSizeNormal = 11;
            try
            {
                if (datosDeclaracion == null) datosDeclaracion = new DeclaracionJuradaVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL, false);
                        documento.SetMargins(80, 82, 25, 80);
                        Encabezado encabezado = new Encabezado(version);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        #region Titulo
                        Paragraph parrafo = new Paragraph("DECLARACIÓN JURADA DE IRREVOCABILIDAD E IRRENUNCIABILIDAD")
                            .SetFont(arialN)
                            .SetFontSize(fSizeTitulo)
                            .SetTextAlignment(TextAlignment.CENTER)
                            .SetFixedLeading(fixedLeading)
                            .SetMarginTop(20);
                        documento.Add(parrafo);
                        #endregion

                        #region P1
                        parrafo = new Paragraph("Señores ")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading)
                            .SetMarginTop(20);

                        parrafo.Add(new Text($"{Environment.NewLine}PRÉSTAMO FELIZ EN 15 MINUTOS S.A.C.{Environment.NewLine}{Environment.NewLine}").SetFont(arialN));
                        parrafo.Add("Yo ");
                        parrafo.Add(new Text($" {datosDeclaracion.NombreCliente} ").SetUnderline().SetFont(arialN));
                        parrafo.Add(", identificado con DNI ");
                        parrafo.Add(new Text($" {datosDeclaracion.DNI} ").SetUnderline().SetFont(arialN));
                        parrafo.Add(new Text(", DECLARO BAJO JURAMENTO CONOCER y ACEPTAR:").SetFont(arialN));
                        documento.Add(parrafo);

                        datosDocumento.Add(datosDeclaracion.NombreCliente);
                        datosDocumento.Add(datosDeclaracion.DNI);
                        #endregion

                        #region Puntos
                        Table tabla = new Table(UnitValue.CreatePercentArray(new float[] { 0.8f, 9.2f })).UseAllAvailableWidth()
                            .SetFont(arial).SetFontSize(fSizeNormal).SetMarginTop(5);

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("•").SetFont(arial).SetFontSize(fSizeNormal)
                                .SetTextAlignment(TextAlignment.CENTER)));

                        parrafo = new Paragraph("Haber solicitado a su entidad un ")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading);
                        parrafo.Add(new Text("PRÉSTAMO ").SetFont(arialN));
                        parrafo.Add("con el objeto de cubrir mis necesidades de financiamiento.");
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER)
                            .Add(parrafo));

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("•").SetFont(arial).SetFontSize(fSizeNormal)
                                .SetTextAlignment(TextAlignment.CENTER)));

                        parrafo = new Paragraph("Haber suscrito de manera ")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading);
                        parrafo.Add(new Text("VOLUNTARIA ").SetFont(arialN));
                        parrafo.Add("una Carta de Autorización de ");
                        parrafo.Add(new Text("DESCUENTO POR PLANILLA").SetFont(arialN));
                        parrafo.Add("/Carta de Autorización de ");
                        parrafo.Add(new Text("DÉBITO AUTOMÁTICO").SetFont(arialN));
                        parrafo.Add(", la misma que tienen carácter expreso, voluntario, ");
                        parrafo.Add(new Text("IRREVOCABLE e IRRENUNCIABLE ").SetFont(arialN));
                        parrafo.Add("hasta la cancelación de la última cuota correspondiente al préstamo.");
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER)
                            .Add(parrafo));

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("•").SetFont(arial).SetFontSize(fSizeNormal)
                                .SetTextAlignment(TextAlignment.CENTER)));

                        parrafo = new Paragraph("Garantizo que mantendré ")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading);
                        parrafo.Add(new Text("ACTIVA MI CUENTA ").SetFont(arialN));
                        parrafo.Add("de débito automático hasta la cancelación de la ");
                        parrafo.Add(new Text("ÚLTIMA CUOTA ").SetFont(arialN));
                        parrafo.Add("del préstamo. En el supuesto negado que tenga que cambiar el número de mi cuenta, daré aviso a su empresa dentro de las ");
                        parrafo.Add(new Text("48 HORAS ").SetFont(arialN));
                        parrafo.Add("de producido el cambio.");
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER)
                            .Add(parrafo));

                        documento.Add(tabla);
                        #endregion

                        #region Afirmacion y fecha
                        parrafo = new Paragraph("Me afirmo y ratifico en lo expresado, en señal de lo cual firmo la presente declaración jurada.")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading)
                            .SetMarginTop(20);
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Lima, ")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading)
                            .SetMarginTop(20);
                        parrafo.Add(new Text($" {DateTime.Now:dd} ").SetUnderline());
                        parrafo.Add(" de ");
                        parrafo.Add(new Text($" {Utils.MesFecha(DateTime.Now)} ").SetUnderline());
                        parrafo.Add(" de ");
                        parrafo.Add(new Text($" {DateTime.Now:yyyy} ").SetUnderline());
                        documento.Add(parrafo);
                        #endregion

                        #region Firmas
                        parrafo = new Paragraph("Atentamente,")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading)
                            .SetMarginTop(40);
                        documento.Add(parrafo);

                        tabla = new Table(UnitValue.CreatePercentArray(new float[] { 5.4f, 0.6f, 1.8f, 2.2f })).UseAllAvailableWidth()
                            .SetFont(arial).SetFontSize(fSizeNormal).SetMarginTop(10);
                        tabla.AddCell(new Cell().Add(new Paragraph(""))
                            .SetHeight(80));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("")));
                        tabla.AddCell(new Cell()
                                .Add(new Paragraph("Huella Digital")
                                    .SetFont(arial)
                                    .SetFontSize(9)
                                    .SetTextAlignment(TextAlignment.CENTER)));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("")));

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("FIRMA DEL CLIENTE")
                                .SetFont(arial)
                                .SetFontSize(fSizeNormal)
                                .SetTextAlignment(TextAlignment.CENTER)));
                        tabla.AddCell(new Cell(0, 3).SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("")));

                        #region PosicionFirma
                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();

                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 145 * punto,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento))) * punto
                            //PosX = 145,//test
                            //PosY = (Utils.AltoAreaRestante(documento))//Para test
                        });

                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        #endregion

                        documento.Add(tabla);

                        tabla = new Table(UnitValue.CreatePercentArray(new float[] { 2.5f, 7.5f })).UseAllAvailableWidth()
                            .SetFont(arial).SetFontSize(fSizeNormal).SetMarginTop(10);
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("Nombre del Cliente:")
                                .SetFont(arial)
                                .SetFontSize(fSizeNormal)));
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph($"{datosDeclaracion.NombreCliente}")
                                .SetFont(arialN)
                                .SetFontSize(fSizeNormal)));
                        documento.Add(tabla);

                        #endregion
                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(datosDeclaracion)}", JsonConvert.SerializeObject(ex));
            }
            return archivo;
        }

        protected partial class Encabezado : IEventHandler
        {
            public int PaginaActual { get; set; }
            private PdfDocumentEvent docEvento;
            private Image logoPF;
            private int version;
            private PdfFont arial;
            private PdfFont arialN;
            private float tNomal = 11;

            public Encabezado(int version)
            {
                this.version = version <= 0 ? 1 : version;
                arial = AppSettings.Arial;
                arialN = AppSettings.ArialNegrita;
                logoPF = AppSettings.LogPF;

                switch (version)
                {
                    case 1:
                        tNomal = 11;
                        logoPF.SetWidth(UnitValue.CreatePercentValue(66));
                        break;
                }
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(80, paginaTamanio.GetTop() - 70, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }
        }
    }
}
