﻿using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using Newtonsoft.Json;
using SOPF.Core.Model.Reportes;
using SOPF.Core.Model.Solicitudes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public partial class Contrato
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private PdfFont arialN;
        private PdfFont arialNN;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private ContratoVM datosContrato;
        private float fSizeNormal = 10;
        private float fSizeTitulo = 11;
        private List<string> datosDocumento = new List<string>();
        public List<PosicionFirmaVM> Firmas { get; set; }
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public Contrato(int version)
        {
            this.version = version <= 0 ? 1 : version;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
            arialN = AppSettings.ArialNarrow;
            arialNN = AppSettings.ArialNarrowNegrita;
            Firmas = new List<PosicionFirmaVM>();
            switch (version)
            {
                case 2:
                    fSizeNormal = 11;
                    fSizeTitulo = 12;
                    break;
            }
        }

        public byte[] Generar(ContratoVM contratoDatos)
        {
            byte[] contratoByte = null;
            datosContrato = contratoDatos;
            switch (version)
            {
                case 1:
                    contratoByte = ContratoV1();
                    break;
                case 2:
                    contratoByte = ContratoV2();
                    break;
            }
            return contratoByte;
        }

        private byte[] ContratoV1()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            float punto = 0.35277f;
            firma.SetWidth(UnitValue.CreatePercentValue(50));

            try
            {
                if (datosContrato == null) datosContrato = new ContratoVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL);
                        Encabezado encabezado = new Encabezado(version);
                        documento.SetMargins(75, 80, 65, 90);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        #region Titulo
                        Paragraph parrafo = new Paragraph(Environment.NewLine + "CONTRATO DE PRÉSTAMO SIMPLE")
                            .SetFont(arialNegrita)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.CENTER);
                        documento.Add(parrafo);
                        #endregion

                        #region Intro
                        parrafo = new Paragraph()
                                            .SetFont(arial)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        Text texto = new Text("El presente documento contiene los términos y condiciones que rigen el Contrato de Préstamo Personal (en adelante, el “Contrato”) " +
                            @"que celebran de una parte Préstamo Feliz en 15 Minutos, S.A.C.con RUC N° 20601194181 (en adelante, “Préstamo Feliz”), y de otra parte el Cliente, " +
                            "quien participa conjuntamente con su cónyuge, de ser el caso; y cuyos datos de identificación se detallan en la Solicitud de Préstamo " +
                            @"(“en adelante, la Solicitud”) y que forma parte integral de este Contrato.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Primera
                        parrafo = new Paragraph(Environment.NewLine + "PRIMERA.- DECLARACIONES:")
                                            .SetFont(arialNegrita)
                                            .SetFontSize(fSizeNormal);
                        documento.Add(parrafo);
                        parrafo = new Paragraph()
                                            .SetFont(arial)
                                            .SetFontSize(fSizeNormal);
                        texto = new Text("Préstamo Feliz en mérito al presente Contrato declara:" + Environment.NewLine + " ");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        Table tContenido = new Table(UnitValue.CreatePercentArray(2)).UseAllAvailableWidth();
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .6f, 10 })).UseAllAvailableWidth();

                        #region 1.1
                        AgregarCelda(tContenido, NuevaCelda("1.1", arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Ser una Sociedad Anónima Cerrada, debidamente constituida y funcionando conforme a las leyes de la República del Perú, " +
                            "cuyo objeto social es la colocación de préstamos al público en general con recursos propios.");
                        parrafo.Add(texto);
                        Cell celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        #endregion

                        #region 1.2
                        AgregarCelda(tContenido, NuevaCelda("1.2", arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Su compromiso de otorgar préstamos de forma expeditiva, esto es, en un promedio de 15 minutos, luego de la correspondiente evaluación técnica y crediticia " +
                            "según sus criterios y/o políticas sobre evaluación de deudores de Préstamo Feliz. La suscripción de la Solicitud por el Cliente no garantiza necesariamente el otorgamiento " +
                            "del Préstamo por parte de Préstamo Feliz, quién iniciará el procedimiento de evaluación técnica y crediticia del solicitante, luego que éste le alcance toda la información " +
                            "y documentación requerida y suficiente a criterio de Préstamo Feliz. En caso el solicitante no califique como deudor, Préstamo Feliz le comunicará dicha situación mediante " +
                            "cualquiera de los medios de comunicación señalados en el presente contrato.");
                        parrafo.Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        #endregion

                        #region 1.3
                        AgregarCelda(tContenido, NuevaCelda("1.3", arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal);
                        texto = new Text("Que le asiste el derecho al Cliente de retractarse de su Solicitud con anterioridad al desembolso del Préstamo Personal, sin que ello " +
                            "conlleve la aplicación de ningún cargo o penalidad alguna.");
                        parrafo.Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        #endregion

                        documento.Add(tContenido);
                        parrafo = new Paragraph()
                                            .SetFont(arial)
                                            .SetFontSize(fSizeNormal);
                        texto = new Text("Por su parte, el Cliente declara lo siguiente:");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        #region 1.4
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .6f, 10 })).UseAllAvailableWidth()
                            .SetMarginTop(5);
                        AgregarCelda(tContenido, NuevaCelda("1.4", arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Que los datos declarados en virtud del presente Contrato, incluida la Solicitud, tienen el carácter de declaración jurada. " +
                            "El Cliente se obliga a comunicar en forma inmediata a Préstamo Feliz cualquier cambio en los datos consignados, asumiendo las posibles " +
                            "consecuencias de su falta de actualización.");
                        parrafo.Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        #endregion

                        #region 1.5
                        AgregarCelda(tContenido, NuevaCelda("1.5", arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Que ha solicitado a Préstamo Feliz un préstamo (en adelante, el “Préstamo”) por el monto y en las condiciones y términos regulados en " +
                            "el presente Contrato, incluida su Hoja Resumen.");
                        parrafo.Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        #endregion

                        #region 1.6
                        AgregarCelda(tContenido, NuevaCelda("1.6", arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Que cuenta con la solvencia económica y calidad moral suficiente para hacer frente a las obligaciones contenidas en el presente Contrato " +
                            "y de no encontrarse sujeto a procesos y/o procedimientos de cobro de acreencias con terceros agentes a la fecha de la Solicitud, que pudiera afectar en " +
                            "forma adversa e importante su condición financiera u operaciones.");
                        parrafo.Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        #endregion

                        #region 1.7
                        AgregarCelda(tContenido, NuevaCelda("1.7", arial, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Que Préstamo Feliz podrá proceder a la suspensión del desembolso del Préstamo si por cualquier motivo variaran de modo adverso las condiciones del mercado " +
                            "financiero, las condiciones políticas, económicas o legales, la situación financiera del Cliente o, en general, las circunstancias bajo las cuales Préstamo Feliz aprobó " +
                            "el Préstamo; sin que ello conlleve a aplicación de ningún cargo por ningún concepto ni la aplicación de penalidad alguna en contra de Préstamo Feliz. En tal caso, " +
                            "resultará aplicable lo dispuesto en la cláusula décima tercera del Contrato.");
                        parrafo.Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        #endregion

                        #region Segunda
                        documento.Add(tContenido);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal);
                        texto = new Text(Environment.NewLine + "SEGUNDA.- DEFINICIONES:")
                            .SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add("Para efectos del presente Contrato, son aplicables los siguientes términos:" + Environment.NewLine);
                        documento.Add(parrafo);
                        #endregion

                        #region 2.1 - 2.6
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("2.1. Carta de Autorización de Descuento por Planilla: Se refiere al documento mediante el cual el Cliente solicita y autoriza de manera " +
                            "irrevocable a su empleador, el descuento de cualquier ingreso remunerativo o no remunerativo para el pago de (i) la cuota correspondiente al saldo deudor; " +
                            "(ii) la(s) cuota(s) y/o interés(es) pendiente(s) de pago del saldo deudor en caso no haya(n) podido ser retenida(s) o descontada(s) a favor de Préstamo Feliz " +
                            "en la oportunidad correspondiente El Cliente reconoce desde ya, que el presente contrato constituye una autorización para el descuento mensual, adicional a " +
                            "la Autorización de Descuento suscrita conjuntamente al presente documento." + Environment.NewLine +
                            "2.2. Carta de Autorización de Débito Automático: Se refiere al documento mediante el cual el Cliente autoriza el descuento automático mensual en la cuenta de " +
                            "depósitos y/o tarjeta vinculada a esta cuenta, que el Cliente mantenga en la institución del sistema financiero que señale para tal efecto, de (i) la cuota " +
                            "correspondiente al saldo deudor; (ii) la(s) cuota(s) y/o interés(es) pendiente(s) de pago del saldo deudor en caso no haya(n) podido ser retenida(s) o " +
                            "descontada(s) a favor de Préstamo Feliz en la oportunidad correspondiente. El Cliente reconoce desde ya, que el presente contrato constituye una autorización para " +
                            "el débito automático mensual, adicional a la Autorización de Débito Automático suscrita conjuntamente al presente documento. " + Environment.NewLine +
                            "2.3. Contrato: los términos y condiciones que rigen el Préstamo personal contenidos en el presente documento, incluyendo la Solicitud, Hoja Resumen, Cronograma " +
                            "de pagos y cualquier modificación debidamente pactada por las partes que pudiera surgir de los mismos. 2.4.Cronograma de Pagos: forma parte integrante de la Hoja " +
                            "de Resumen y contiene el número de cuotas, y su periodicidad y fecha de pago; desagregándose los conceptos que integran la cuota. La fecha de corte para el " +
                            "cómputo de los intereses compensatorios y moratorios coincidirá con la fecha de pago de las cuotas. " + Environment.NewLine +
                            "2.5.Solicitud: oferta contractual que realiza el Cliente hacia Préstamo Feliz manifestando su voluntad de contratar una vez conocidos y aceptados los términos y " +
                            "condiciones del Contrato, formando parte integrante de este último. " + Environment.NewLine +
                            "2.6.Hoja Resumen: anexo del Contrato que informa de modo sintetizado las condiciones del Contrato de Préstamo Personal, tasa de costo efectivo anual(TCEA), la tasa " +
                            "de interés compensatoria, la tasa de interés moratoria o penalidad aplicable en caso de incumplimiento de pago, las comisiones y gastos de cargo de EL CLIENTE, el " +
                            "cronograma de pagos; entre otra información relevante conforme a la legislación vigente.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Tercera
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + "TERCERA.- OBJETO DEL CONTRATO: ").SetFont(arialNegrita);
                        Text texto2 = new Text("De ser aprobada la Solicitud del Cliente, Préstamo Feliz otorgará en favor de aquél un préstamo personal en los términos, monto, plazo y finalidad " +
                            "indicados en la Hoja Resumen, y en la oportunidad prevista en la cláusula siguiente. Por su parte, el Cliente se obliga a restituir a Préstamo Feliz el importe del " +
                            "préstamo personal en los términos y plazo indicados en este Contrato, incluida su Hoja de Resumen.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region Cuarta
                        parrafo = new Paragraph()
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"CUARTA.- DISPOSICIÓN DEL PRÉSTAMO: ").SetFont(arialNegrita);
                        texto2 = new Text("El Cliente podrá disponer del importe del préstamo en el plazo máximo de 72 horas luego suscrito este Contrato, a través de orden de pago o transferencia " +
                            "electrónica en el número de cuenta y entidad financiera que el Cliente indique en la Hoja Resumen. Salvo el supuesto de transferencia electrónica, en el caso que el " +
                            "Cliente no disponga del préstamo otorgado en un plazo de diez días hábiles posteriores a la puesta a disposición de los fondos por parte de Préstamo Feliz, el Cliente " +
                            "o Préstamo Feliz podrán solicitar la cancelación del mismo sin responsabilidad y costo alguno.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                                            .SetFont(arial)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + "En caso de que por error Préstamo Feliz deposite una cantidad superior al monto del Préstamo pactado, el Cliente tiene la " +
                            "obligación de devolver la cantidad excedente a Préstamo Feliz a más tardar al día hábil siguiente en el cual tales fondos son puestos a su disposición considerando " +
                            "las modalidades antes referidas, caso contrario dicho excedente será considerado como parte del Préstamo siendo en tal caso el Cliente informado con posterioridad de " +
                            "las eventuales variaciones que su decisión genere en el cronograma de pagos. Ello, sin perjuicio de la facultad de Préstamo Feliz de resolver el Contrato de pleno " +
                            "derecho en caso el Cliente incumpla con devolver la cantidad excedente en el plazo antes referido, siendo aplicable el procedimiento establecido en la cláusula " +
                            "décima tercera de este Contrato.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Quinta
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"QUINTA.- EMISIÓN DE TITULO VALOR: ").SetFont(arialNegrita);
                        texto2 = new Text("Préstamo Feliz podrá requerir al cliente la emisión y suscripción de un pagaré emitido a la orden de Préstamo Feliz SAC de manera incompleta de " +
                            "conformidad a lo establecido en el artículo 10 de la Ley de Títulos Valores, Ley N° 27287, en tal caso Préstamo Feliz le proporcionará al momento de la suscripción " +
                            "del referido título valor una copia del mismo. En caso de incumplimiento de las obligaciones que asume el Cliente con Préstamo Feliz, este último queda facultado a " +
                            "completar el pagaré antes indicado con el monto que resulte de la obligación exigible según la liquidación que realice Préstamo Feliz en mérito a lo pactado en el " +
                            "presente Contrato, la misma que comprenderá intereses compensatorios, intereses moratorios, impuestos, comisiones y gastos según lo establecido en la Hoja de Resumen, " +
                            "incluidos aquellos costos en los que haya incurrido Préstamo Feliz derivados del incumplimiento de las obligaciones por parte del Cliente, siendo la fecha de vencimiento, " +
                            "aquella en la que se practique dicha liquidación y llene el pagaré. El llenado del pagare se hará conforme a la hoja de instrucción del llenado del pagaré que será " +
                            "entregada al Cliente.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + "El Cliente renuncia expresamente a su derecho a incluir en el mencionado pagaré una cláusula que limite su transferencia, reconociendo " +
                            "de Préstamo Feliz a negociar libremente el referido título valor. El cliente declara tener conocimiento de los mecanismos de protección que la ley permite para la emisión " +
                            "o aceptación de títulos valores incompletos. Asimismo, de conformidad con lo dispuesto por el artículo 1279° del Código Civil, la emisión del pagaré a que se refiere la " +
                            "presente cláusula, no constituirá novación o sustitución de la obligación primitiva o causal. La prórroga del pagaré o cualquier otro cambio accesorio de la obligación, " +
                            "de ser el caso, tampoco constituirá novación de la misma. Ambas partes convienen que la entrega o emisión del Pagaré, o de cualquier otro título valor que constituya orden " +
                            "o promesa de pago, en ningún caso extinguirá la obligación primitiva ni aun cuando éstos hubiesen sido perjudicados por cualquier causa.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Sexta
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"SEXTA.- INTERESES, COMISIONES Y GASTOS: ").SetFont(arialNegrita);
                        texto2 = new Text("Queda pactado que Préstamo Feliz cobrará al Cliente la tasa de interés efectivo compensatorio y, en caso de incumplimiento los intereses moratorios, " +
                            "en ambos casos respetándose los límites establecidos en la normativa vigente; así como las comisiones, impuestos y gastos, en los porcentajes, montos y periodicidad " +
                            "indicados en la Hoja Resumen; la que a su vez informará la TCEA respectiva en el cronograma de pagos.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + "Queda expresamente pactado que la demora en el pago de cualquiera de las obligaciones del Cliente determinará que las sumas adeudadas " +
                            "generen intereses moratorios o penalidades, en forma diaria adicional al interés compensatorio pactado. Por tanto, la falta de pago oportuno por parte del Cliente de " +
                            "cualquiera de sus obligaciones, determina que incurra en mora automática a partir del día siguiente de la fecha en que debió haberse realizado el pago según lo indicado " +
                            "en el cronograma de pagos, sin necesidad de requerimiento o intimación alguna por parte de Préstamo Feliz, encentrándose este último facultado a dar por vencido el Préstamo " +
                            "y a proceder con las acciones de cobranza respectivas conforme a lo indicado en el penúltimo párrafo de la cláusula siguiente, y sin perjuicio de aplicar el procedimiento " +
                            "previsto en la cláusula décima tercera del Contrato.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Séptima
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"SEPTIMA.- GESTION DE COBRANZA: ").SetFont(arialNegrita);
                        texto2 = new Text("El Cliente presta su consentimiento sobre la posible gestión de cobranza que deba ser realizada por Préstamo Feliz directamente o a través de terceros en sus " +
                            "domicilios como en su centro de trabajo, a través de vía telefónica, medios electrónicos, mensajería instantánea o escrita de conformidad con la ley aplicable.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region Octava
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"OCTAVA.- MODALIDAD Y LUGAR DE PAGO: ").SetFont(arialNegrita);
                        texto2 = new Text("El préstamo otorgado podrá ser pagado mediante depósito en efectivo, descuento por planilla, cheque o a través de débito automático en la cuenta de Préstamo Feliz " +
                            "en cualquiera de los bancos autorizados por Préstamo Feliz para recibir los pagos. El número de cuenta de Préstamo Feliz, y la modalidad de pago elegida por el Cliente " +
                            "constarán en la Hoja de Resumen. En caso no sea posible realizar el descuento de las cuotas a través de la modalidad de pago elegida por el Cliente, éste es responsable de " +
                            "pagar las cuotas a favor de Préstamo Feliz a través de cualquier otra modalidad.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("De optar el Cliente por la modalidad de débito automático a través de una entidad depositaria afiliada a Préstamo Feliz para recibir tales pagos, deberá suscribir " +
                            "la solicitud de cargo automático y/o documento correspondiente en favor de Préstamo Feliz de modo tal que la modalidad de pago a través de débito automático surta efectos durante " +
                            "toda la vigencia del presente Contrato garantizando el derecho de Préstamo Feliz a recibir el pago automáticamente al vencimiento de las cuotas del Préstamo. El cliente reconoce " +
                            "el derecho de Préstamo Feliz a gestionar en cualquier momento el cargo automático de cualquier acreencia vencida o por vencer, a través de la modalidad de cargo automático de sus " +
                            "cuentas afiliadas u otras cuentas que mantenga en cualquier empresa del sistema financiero, incluyendo abonos por concepto de gratificaciones, bonos, entre otros, sin reserva ni " +
                            "limitación alguna. El Cliente está obligado a informar inmediatamente cualquier cambio de institución financiera, número de cuenta, tarjeta y/o cualquier otra modificación que " +
                            "pueda impactar en el descuento automático de las cuotas del préstamo. Sin perjuicio de ello reconoce que Préstamo Feliz tiene puede acceder a esta información, autorizándolo " +
                            "desde ya a obtener la información del Cliente.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Las cuotas del préstamo incluyen: capital, interés compensatorio, interés moratorio, comisiones, gastos, seguros y tributos, según lo informado en la " +
                            "Hoja Resumen. El pago de las cuotas se realizará en la periodicidad y fechas establecidas en el Cronograma de Pagos, el mismo que considerará como fecha de inicio para " +
                            "el cómputo del plazo del Préstamo la fecha del desembolso de éste.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Novena
                        parrafo = new Paragraph()
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(@"NOVENA.- SEGURO DE DESGRAVAMEN: ").SetFont(arialNegrita);
                        texto2 = new Text("Préstamo Feliz podrá requerir la contratación de un Seguro de Desgravamen para otorgar el préstamo. A tal propósito, el Cliente podrá contratar el " +
                            "Seguro de Desgravamen que comercializa Préstamo Feliz, cuyos términos mínimos legales son informados en la Hoja de Resumen. En tal caso, Préstamo Feliz le proporcionará " +
                            "las condiciones generales y particulares que se establecen en la póliza de seguro, la misma que le será proporcionada adicionalmente a la Hoja de Resumen para su " +
                            "aceptación expresa al momento de la suscripción del presente Contrato. Con la contratación del referido seguro, el Cliente puede optar por su financiamiento, en tal caso " +
                            "el monto de la prima de seguro será incorporado al valor de la cuota mensual de pago, de acuerdo a lo informado en el cronograma de pagos.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Se hace la salvedad que en caso que el Cliente decida contratar con otra compañía de seguros un Seguro de Desgravamen de iguales características y requisitos a " +
                            "los exigidos por Préstamo Feliz, no estará obligado a tomar el seguro ofrecido por ésta, pudiendo sustituirlo por el que hubiera contratado. Las condiciones mínimas que " +
                            "serán requeridas en la póliza de seguro respectiva por parte de Préstamo Feliz se informan en la página web www.prestamofeliz.pe De proceder en Cliente en este sentido, " +
                            "la póliza de seguro deberá ser endosada a favor de Préstamo Feliz hasta por el monto adeudado del Préstamo. En virtud del endoso, Préstamo Feliz podrá pactar con el Cliente " +
                            "que el pago de la prima del seguro se adicione al pago de las cuotas periódicas previamente pactadas por el Préstamo. El endoso y forma de pago por acuerdo de las partes, " +
                            "podrá ser tramitado por el Cliente ante la empresa del sistema de seguros que emite la póliza de Seguro de Desgravamen y deberá ser entregado a Préstamo Feliz.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + "En caso EL CLIENTE no contrate el referido seguro bajo alguna de las modalidades antes indicadas, Préstamo Feliz se encontrará " +
                            "facultado a contratar de forma directa dicho seguro. En tal caso, los gastos por concepto de seguro que Préstamo Feliz pueda contratar directamente serán trasladados " +
                            "al Cliente. En este caso particular, Préstamo Feliz entregará la póliza o en un plazo que no excederá de 10 días contados a partir de su recepción.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Décima
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"DÉCIMA.- CESIÓN: ").SetFont(arialNegrita);
                        texto2 = new Text("Préstamo Feliz podrá ceder sus derechos o su posición contractual en este Contrato a cualquier tercero, prestando el Cliente, en este acto, " +
                            "su consentimiento anticipado a la referida cesión, la misma que le será oportunamente informada. Por su parte, el Cliente no podrá transmitir sus derechos y " +
                            "obligaciones al amparo del presente Contrato sin la autorización previa y por escrito de Préstamo Feliz.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region DécimaPrimera
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"DÉCIMA PRIMERA.- ORDEN DE IMPUTACIÓN DE PAGOS: ").SetFont(arialNegrita);
                        texto2 = new Text("Cualquier pago que efectúe el Cliente se aplicará en el siguiente orden: deuda vencida, impuestos, gastos, comisiones, intereses " +
                            "moratorios, intereses compensatorios del periodo y capital.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region DécimaSegunda
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + "DÉCIMA SEGUNDA.- PAGOS ANTICIPADOS O ADELANTO DE CUOTAS: ").SetFont(arialNegrita);
                        texto2 = new Text("El Cliente podrá realizar pagos por encima de la cuota exigible del periodo, ya sea a través de pagos anticipados o adelanto de cuotas, " +
                            "en forma total o parcial, sin cobro alguno, condiciones, limitaciones, gastos, comisiones o penalidades de ningún tipo. El Pago Anticipado conlleva a " +
                            "la aplicación del monto capital al crédito, con la consiguiente reducción de los intereses, comisiones y gastos al día del pago. Los pagos mayores a dos " +
                            "cuotas (incluyendo la exigible en el periodo) se consideran pagos anticipados. Por su parte, el Adelanto de Cuotas supone la aplicación del monto " +
                            "pagado a las cuotas inmediatamente posteriores a la exigible en el periodo, sin que se produzca una reducción de los intereses, comisiones y gastos " +
                            "al día del pago. Los pagos menores o iguales a dos cuotas (que incluyen aquella exigible en el período) se consideran adelanto de cuotas.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region DécimaTercera
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"DÉCIMA TERCERA.- ACELERACIÓN DE PLAZOS Y RESOLUCIÓN DEL CONTRATO: ").SetFont(arialNegrita);
                        texto2 = new Text("En mérito a lo dispuesto en el artículo 1430° del Código Civil, ambas partes acuerdan que el Contrato podrá ser resuelto de pleno " +
                            "derecho por Préstamo Feliz desde la fecha en la que ésta le comunique al Cliente, previamente y sin necesidad de declaración judicial, que quiere " +
                            "valerse de alguna de las siguientes causales: si el Cliente: (i) no paga en la forma, plazo y oportunidad convenidas sus obligaciones de crédito " +
                            "en mérito al presente Contrato y/o en el cronograma de pagos respectivo;(ii)no cumple cualquier otra obligación frente a Préstamo Feliz que, a " +
                            "criterio de éste último, le genere un perjuicio y/o dificulte la ejecución del Contrato; (iii) cuando, a criterio de Préstamo Feliz, el Cliente: a) " +
                            "no satisfaga los requerimientos de información efectuados por Préstamo Feliz, como parte de sus políticas y acciones vinculadas al conocimiento del " +
                            "Cliente y/o a la prevención del lavado de activos y financiamiento del terrorismo, incluyendo, de manera enunciativa y no limitativa, los casos en " +
                            "los que dicha información no es entregada dentro de los plazos fijados por Préstamo Feliz y/o es entregada de manera incompleta y/o habiendo sido " +
                            "entregada oportunamente, a criterio de Préstamo Feliz, no justifica las operaciones del Cliente; b) si se solicita la declaración del inicio de " +
                            "concurso del Cliente o se presentase cualquier otra situación que afecte su capacidad de pago; c) si Préstamo Feliz comprueba que cualquier " +
                            "información, documentación o dato proporcionado por el Cliente para sustentar el Crédito Personal que haya solicitado ante Préstamo Feliz " +
                            "fueran falsas, o, tratándose de documentos, éstos hubieran sido adulterados o alterados, d) si habiendo suscrito el Compromiso de no revocar la " +
                            "instrucción a su depositario, el Cliente incumple dicho compromiso, e) si mantener vigente el Crédito implica el incumplimiento de alguna " +
                            "disposición legal, en especial aquellas referidas a políticas crediticias o de lavado de activos y financiamiento del terrorismo; o f) por cese " +
                            "o fallecimiento del Cliente.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("En cualquiera de los supuestos previamente indicados, Préstamo Feliz podrá resolver este Contrato señalando en la comunicación correspondiente, " +
                            "el motivo de la misma. Dicho aviso se cursará con una anticipación no menor a cinco (5) días hábiles a la fecha de resolución efectiva.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("En cualquier caso de resolución o terminación del Contrato, el Cliente, bajo su responsabilidad, se obliga a cancelar dentro de las " +
                            "veinticuatro (24) horas siguientes o dentro del plazo adicional que le otorgue Préstamo Feliz expresamente y por escrito, el íntegro del saldo deudor " +
                            "pendiente de pago según la liquidación que realice Préstamo Feliz.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("El Cliente podrá a su vez poner término a este Contrato cuando así lo decida, dando aviso por escrito a Préstamo Feliz, de acuerdo a las " +
                            "indicaciones en éste establecidas, sin perjuicio de su obligación de pagar previamente el saldo deudor total del Préstamo que liquide Préstamo Feliz. " +
                            "Préstamo Feliz podrá mantener vigente el Préstamo hasta la cancelación de dicho saldo deudor.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region DécimaCuarta
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + "DÉCIMA CUARTA.-MODIFICACIONES AL CONTRATO: ").SetFont(arialNegrita);
                        texto2 = new Text("Ambas partes acuerdan la facultad de Préstamo Feliz de modificar las condiciones contractuales cuando a su criterio se presenten " +
                            "alguno de los siguientes supuestos: (i) cambios en las condiciones de la economía nacional o internacional; (ii) cambios en el funcionamiento o " +
                            "tendencias de los mercados o la competencia; (iii) cambios en las políticas de gobierno o de Estado que afecten las condiciones del mercado; " +
                            "(iv) impacto de alguna disposición legal sobre costos, características, definición o condiciones de los productos y servicios de crédito en general; " +
                            "(v) inflación o deflación; devaluación o revaluación de la moneda; (vi) campañas promocionales; (vii) evaluación crediticia del Cliente o de " +
                            "su empleador, de ser el caso; (viii) encarecimiento de los servicios prestados por terceros cuyos costos son trasladados al Cliente o de los costos " +
                            "de prestación de los productos y servicios de crédito ofrecidos por Préstamo Feliz; (ix) crisis financiera; o (x) hechos ajenos a la voluntad " +
                            "de las partes; conmoción social; desastres naturales; terrorismo; guerra; caso fortuito o fuerza mayor ;(xi) el cliente no satisfaga los requerimientos " +
                            "de información efectuados por Préstamo Feliz, como parte de sus políticas y acciones vinculadas al conocimiento del Cliente y/o a la prevención del " +
                            "lavado de activos y financiamiento del terrorismo, incluyendo, de manera enunciativa y no limitativa, los casos en los que dicha información no es " +
                            "entregada dentro de los plazos fijados por Préstamo Feliz y/o es entregada de manera incompleta y/o habiendo sido entregada oportunamente, a criterio de " +
                            "Préstamo Feliz, no justifica las operaciones del Cliente. " + Environment.NewLine +
                            "Cualquier comunicación y modificación al Contrato , será efectuadas indistintamente, a través de: comunicaciones al domicilio señalado en la Solicitud, " +
                            "correo electrónico, llamadas telefónicas, envío de mensajes de texto o, a través de aplicaciones de mensajería instantánea, llamadas telefónicas y demás " +
                            "medios de comunicación directos que Préstamo Feliz pudiese implementar. Tales modificaciones serán informadas con una anticipación no menor a cuarenta y " +
                            "cinco(45) días calendario a la fecha de entrada en vigencia de las mismas.De no estar de acuerdo el Cliente con las modificaciones introducidas podrá optar " +
                            "por resolver el contrato dentro de dicho plazo, sin cargo o penalidad alguna cursando comunicación escrita Préstamo Feliz.La resolución del Contrato por el " +
                            "Cliente conllevará a su vez al pago de todo saldo deudor u obligación que mantuviera pendiente frente a Préstamo Feliz. " + Environment.NewLine +
                            "En el caso de modificaciones más beneficiosas al Cliente, Préstamo Feliz podrá utilizar medios masivos de información, tales como: publicaciones en oficinas, " +
                            "página web y / o en otro; sin necesidad de comunicación previa al Cliente.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region DécimaQuinta
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"DÉCIMA QUINTA.- AMPLIACIONES DEL CRÉDITO").SetFont(arialNegrita);
                        texto2 = new Text(Environment.NewLine + "El Cliente, podrá solicitar a Préstamo Feliz ampliaciones del crédito en caso demuestre un incremento de su capacidad de " +
                            "endeudamiento, siempre sujeto a evaluación aprobación de Préstamo Feliz conforme sus políticas de crédito. Las partes acuerdan que las ampliaciones se " +
                            "regirán por los mismos términos, condiciones, pagaré, garantías y/o fianzas que forman parte integrante del presente contrato. En caso Préstamo feliz apruebe " +
                            "la ampliación, procederá al desembolso y emisión de la Hoja Resumen y Cronograma de Pagos actualizados.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region DécimaSexta
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"DÉCIMO SEXTA.- DATOS PERSONALES: ").SetFont(arialNegrita);
                        texto2 = new Text("Se informa que los datos personales proporcionados a Préstamo Feliz quedan incorporados al banco de datos de Clientes de Préstamo Feliz, " +
                            "quien utilizará dicha información para efectos de la gestión de los servicios de préstamo solicitados y/o contratados (incluyendo evaluaciones financieras, " +
                            "procesamiento de datos, formalizaciones contractuales, cobro de deudas y remisión de correspondencia, entre otros), la misma que podrá ser realizada a través " +
                            "de terceros. Asimismo, el titular de los datos personales autoriza a Préstamo Feliz a utilizar sus datos personales, incluyendo datos sensibles, que hubieran " +
                            "sido proporcionados directamente a Préstamo Feliz, aquellos que pudieran encontrarse en fuentes accesibles para el público o los que hayan sido obtenidos de " +
                            "terceros; para tratamientos que supongan desarrollo de acciones comerciales, incluyendo evaluaciones crediticias, la remisión (vía medio físico, electrónico o " +
                            "telefónico) de publicidad, información u ofertas (personalizadas o generales) de productos y/o servicios de Préstamo Feliz.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Se informa al titular de los datos personales, que puede revocar la autorización para el tratamiento de sus datos personales en cualquier " +
                            "momento, de conformidad con lo previsto en las normas que prevén la protección de datos personales. Para ejercer este derecho, o cualquier otro previsto " +
                            "en dichas normas, el titular de datos personales podrá presentar su solicitud por escrito a Préstamo Feliz en la siguiente dirección: Calle Manuel " +
                            "Segura N° 166 - Lince Esta autorización se mantendrá vigente luego de la terminación o resolución del Contrato. Sin perjuicio de ello, el Cliente declara " +
                            "conocer su derecho a revocar en cualquier momento su consentimiento y autorización, así como realizar las coordinaciones pertinentes, en caso haya decidido " +
                            "no recibir algún otro tipo de información.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region DécimaSéptima
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"DÉCIMA SÉTIMA.- OBLIGACIONES ADICIONALES: ").SetFont(arialNegrita);
                        texto2 = new Text("El Cliente a su vez se obliga a: (i) notificar a Préstamo Feliz respecto de cualquier cambio en su domicilio en un plazo que no exceda de " +
                            "10 días hábiles posteriores a que dicho cambio haya ocurrido; (ii) notificar a Préstamo Feliz respecto de cualquier disminución en sus ingresos o cualquier " +
                            "situación que pueda afectar su capacidad de pago, en un plazo que no exceda de 10 días hábiles posteriores a que dicha situación haya ocurrido.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region DécimaOctava
                        parrafo = new Paragraph()
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"DÉCIMA OCTAVA.- TRIBUTOS: ").SetFont(arialNegrita);
                        texto2 = new Text("Todos los impuestos, contribuciones y derechos que deban cubrirse con motivo de la celebración del presente Contrato, serán cubiertos " +
                            "por la parte que resulte obligada a ello, de conformidad con la legislación aplicable. En este sentido, aquellos tributos que correspondan ser " +
                            "cubiertos por el Cliente serán informados en la Hoja de Resumen, la misma que hará referencia al tipo de tributo al que se sujeta, el porcentaje " +
                            "y monto aplicable en situación de cumplimiento. En tal caso, la emisión y el otorgamiento del comprobante de pago respectivo estarán a cargo de " +
                            "Préstamo Feliz lo que procederá en la oportunidad que se indica en la referida Hoja de Resumen.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region DécimaNovena
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"DÉCIMA NOVENA.- LEGISLACION Y COMPETENCIA: ").SetFont(arialNegrita);
                        texto2 = new Text("este Contrato se rige en forma supletoriamente por las disposiciones del Código Civil Peruano.Para el caso de cualquier discrepancia " +
                            "derivada de la ejecución y / o interpretación de este Contrato, las partes se someten a la competencia de los jueces y tribunales de la ciudad de " +
                            "Lima, señalando como domicilio a los indicados en la Solicitud, a donde se harán llegar todas las citaciones y notificaciones judiciales o " +
                            "extrajudiciales a que hubiere lugar.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region DécimaSexta
                        parrafo = new Paragraph().SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + @"VIGÉSIMA.- VIGENCIA:- ").SetFont(arialNegrita);
                        texto2 = new Text("Sin perjuicio de lo establecido en la cláusula Décima tercera, el presente Contrato entrará en vigor a partir de su fecha de firma y " +
                            "se dará por terminado en la fecha en que el Cliente haya liquidado en su totalidad sus obligaciones crediticias derivadas del presente Contrato.")
                            .SetFont(arial);
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Leídos en su integridad los términos y condiciones del presente Contrato por las partes que en éste intervienen, se procede a la " +
                            "suscripción del Contrato, incluido sus anexos, y a la entrega al Cliente de un ejemplar del mismo.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region LugarFechaCliente
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { 0.2f, .5f, 2 })).UseAllAvailableWidth()
                            .SetMarginTop(50)
                            .SetPaddings(0, 0, 0, 0)
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetVerticalAlignment(VerticalAlignment.BOTTOM);

                        tContenido.AddCell(new Cell().Add(new Paragraph("Lugar:").SetFont(arialNegrita))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().Add(new Paragraph(datosContrato.Lugar))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new DashedBorder(.5f)));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));

                        tContenido.AddCell(new Cell().Add(new Paragraph("Fecha:").SetFont(arialNegrita))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().Add(new Paragraph(datosContrato.Fecha.ToString("dd/MM/yyyy")))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new DashedBorder(.5f)));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        documento.Add(tContenido);

                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .7f, 2.3f })).UseAllAvailableWidth()
                            .SetPaddings(0, 0, 0, 0)
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetVerticalAlignment(VerticalAlignment.BOTTOM);

                        tContenido.AddCell(new Cell().Add(new Paragraph("Nombre del Cliente:").SetFont(arialNegrita))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().Add(new Paragraph(datosContrato.NombreCliente))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new DashedBorder(.5f)));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));

                        documento.Add(tContenido);
                        datosDocumento.Add(datosContrato.NombreCliente);


                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();

                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 45,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 39)) * punto
                        });
                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        #endregion

                        #region Firmas
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { 1, 0.3f, 1 })).UseAllAvailableWidth()
                            .SetMarginTop(80)
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal);

                        tContenido.AddCell(new Cell().Add(new Paragraph("Firma del Titular").SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new SolidBorder(0.5f)));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().Add(new Paragraph("Firma de Cónyuge").SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new SolidBorder(0.5f)));

                        Table subTabla = new Table(UnitValue.CreatePercentArray(new float[] { .7f, 1, .5f })).UseAllAvailableWidth();
                        subTabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        subTabla.AddCell(new Cell().Add(firma).SetBorder(Border.NO_BORDER));
                        subTabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell()
                            .Add(subTabla)
                            .SetBorder(Border.NO_BORDER));

                        tContenido.AddCell(new Cell(0, 2).SetBorder(Border.NO_BORDER));

                        AgregarCelda(tContenido, NuevaCelda("Firma del Préstamo Feliz", arial, fSizeNormal, true, false, false, false, Alineacion.Centro), false);
                        tContenido.AddCell(new Cell(0, 2).SetBorder(Border.NO_BORDER));

                        documento.Add(tContenido);
                        #endregion

                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(datosContrato)}", JsonConvert.SerializeObject(ex));
            }
            return archivo;
        }

        protected partial class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private int version;
            private Image logoPF;
            private PdfFont arial;
            private PdfFont arialNegrita;
            private PdfFont arialN;
            private PdfFont arialNN;
            private float fSizeNormal = 11;

            public int PaginaActual { get; set; }

            public Encabezado(int version)
            {
                this.version = version <= 0 ? 1 : version;
                logoPF = AppSettings.LogPF;
                arial = AppSettings.Arial;
                arialNegrita = AppSettings.ArialNegrita;
                arialN = AppSettings.ArialNarrow;
                arialNN = AppSettings.ArialNarrowNegrita;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                    case 2:
                        EncabezadoV2();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(50, paginaTamanio.GetTop() - 80, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }

            public void FijarPaginado(Document document)
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                int totalPaginas = pdf.GetNumberOfPages();
                Paragraph paginas;
                switch (version)
                {
                    case 2:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"Página {i} de {totalPaginas}").SetFont(arialN).SetFontSize(fSizeNormal);
                            document.ShowTextAligned(paginas, paginaTamanio.GetWidth() - 80, paginaTamanio.GetTop() - 35, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                }
            }

        }

        protected class CellRadius : CellRenderer
        {
            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}
