﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Model.Reportes;
using SOPF.Core.Model.Solicitudes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class TransaccionExtrajudicial
    {
        private int version;
        private PdfFont calibri;
        private PdfFont calibriNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private TransaccionExtrajudicialVM datosTransaccionExtrajudicial;
        private float fSizeNormal = 11f;
        private List<string> datosDocumento = new List<string>();
        public List<PosicionFirmaVM> Firmas { get; set; }
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public TransaccionExtrajudicial(int version)
        {
            this.version = version <= 0 ? 1 : version;
            calibri = AppSettings.Calibri;
            calibriNegrita = AppSettings.CalibriNegrita;
            Firmas = new List<PosicionFirmaVM>();
        }

        public byte[] Generar(TransaccionExtrajudicialVM TransaccionExtrajudicialDatos)
        {
            byte[] TransaccionExtrajudicialByte = null;
            datosTransaccionExtrajudicial = TransaccionExtrajudicialDatos;
            switch (version)
            {
                case 1:
                    TransaccionExtrajudicialByte = TransaccionExtrajudicialV1();
                    break;
            }
            return TransaccionExtrajudicialByte;
        }

        private byte[] TransaccionExtrajudicialV1()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            float punto = 0.35277f;
            firma.SetWidth(UnitValue.CreatePercentValue(50));

            try
            {
                if (datosTransaccionExtrajudicial == null) datosTransaccionExtrajudicial = new TransaccionExtrajudicialVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LETTER);
                        Encabezado encabezado = new Encabezado(version);
                        documento.SetMargins(45, 80, 45, 80);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        Text textoEC = new Text("EL CLIENTE").SetFont(calibriNegrita);
                        Text textoPF = new Text("PRÉSTAMO FELIZ").SetFont(calibriNegrita);

                        #region Titulo
                        Paragraph parrafo = new Paragraph(Environment.NewLine + "TRANSACCIÓN EXTRAJUDICIAL")
                            .SetFont(calibriNegrita)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.CENTER);
                        documento.Add(parrafo);
                        #endregion

                        #region Conste
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        Text texto = new Text("Conste por el presente documento, el acuerdo que celebran: ");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region De una parte
                        Table tContenido = new Table(1).SetBorder(Border.NO_BORDER);
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                                            .SetMarginLeft(30);
                        texto = new Text("De una parte, ");
                        Text texto2 = new Text("PRÉSTAMO FELIZ S.A.C.")
                            .SetFont(calibriNegrita);
                        parrafo.Add(texto).Add(texto2);
                        texto = new Text(", con RUC N° 20601194181 domiciliado en Calle Manuel Segura N° 166 - Lince" +
                            ", provincia y departamento de Lima, debidamente representado por Pamela Giuliana " +
                            "Navarrete López con Documento Nacional de Identidad N° 42260909 a quién en adelante se " +
                            "le denominará ");
                        parrafo.Add(texto).Add(textoPF);
                        texto = new Text(".");
                        parrafo.Add(texto);
                        Cell celda = new Cell().SetBorder(Border.NO_BORDER).Add(parrafo);
                        tContenido.AddCell(celda);
                        documento.Add(tContenido);
                        #endregion

                        #region Y, por otra
                        tContenido = new Table(1);
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                                            .SetMarginLeft(30);
                        texto = new Text("Y, por otra parte, la señor (a) " + datosTransaccionExtrajudicial.NombreCliente +
                            ", con Documento Nacional de Identidad N° " + datosTransaccionExtrajudicial.DNI +
                            ", domiciliada en calle " + datosTransaccionExtrajudicial.Calle + ", distrito de " +
                            datosTransaccionExtrajudicial.Distrito + ", provincia y departamento de Lima, a quien " +
                            "en adelante se le denominará ");
                        parrafo.Add(texto).Add(textoEC);
                        texto = new Text(". Asimismo, ");
                        parrafo.Add(texto).Add(textoEC);
                        texto = new Text(" declara ser letrada y conocedor del idioma castellano; según " +
                            "los términos contenidos en las cláusulas siguientes:");
                        parrafo.Add(texto);
                        celda = new Cell().SetBorder(Border.NO_BORDER).Add(parrafo);
                        tContenido.AddCell(celda);
                        documento.Add(tContenido);

                        datosDocumento.Add(datosTransaccionExtrajudicial.NombreCliente);
                        datosDocumento.Add(datosTransaccionExtrajudicial.DNI);
                        datosDocumento.Add(datosTransaccionExtrajudicial.Calle);
                        datosDocumento.Add(datosTransaccionExtrajudicial.Distrito);
                        #endregion

                        #region PRIMERA
                        parrafo = new Paragraph()
                                            .SetFont(calibriNegrita)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("PRIMERA").SetUnderline();
                        texto2 = new Text(": ANTECEDENTES E IDENTIFICACIÓN DE LOS ASUNTOS LITIGIOSOS");
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region 1.1
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .6f, 10 })).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("1.1", calibri, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Con fecha " + datosTransaccionExtrajudicial.FechaCredito.ToString("dd/MM/yyyy") +
                            ", ");
                        parrafo.Add(texto).Add(textoEC);
                        texto = new Text(" solicitó a ");
                        parrafo.Add(texto).Add(textoPF);
                        texto = new Text(" un préstamo por " + datosTransaccionExtrajudicial.ImporteCredito.ToString("C", cultPeru) +
                            " a " + datosTransaccionExtrajudicial.PlazoActual + " cuotas. ");
                        parrafo.Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);

                        datosDocumento.Add(datosTransaccionExtrajudicial.FechaCredito.ToString("dd/MM/yyyy"));
                        datosDocumento.Add(datosTransaccionExtrajudicial.ImporteCredito.ToString("C", cultPeru));
                        datosDocumento.Add(datosTransaccionExtrajudicial.PlazoActual.ToString());
                        #endregion

                        #region 1.2
                        AgregarCelda(tContenido, NuevaCelda("1.2", calibri, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Por motivos económicos ");
                        parrafo.Add(texto).Add(textoEC);
                        texto = new Text(" dejó de pagar las cuotas señaladas en el numeral anterior, manteniendo " +
                            "cuotas desde el mes de " + Utils.MesFecha(datosTransaccionExtrajudicial.FechaPrimerRecibo) +
                            ", por el importe de " + datosTransaccionExtrajudicial.CuotaActual.ToString("C", cultPeru) +
                            ", más intereses moratorios.");
                        parrafo.Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        documento.Add(tContenido);

                        datosDocumento.Add(datosTransaccionExtrajudicial.FechaPrimerRecibo.ToString());
                        datosDocumento.Add(datosTransaccionExtrajudicial.CuotaActual.ToString("C", cultPeru));
                        #endregion

                        #region SEGUNDA
                        parrafo = new Paragraph()
                                            .SetFont(calibriNegrita)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("SEGUNDA").SetUnderline();
                        texto2 = new Text(": ANTECEDENTES E IDENTIFICACIÓN DE LOS ASUNTOS LITIGIOSOS");
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region A fin
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("A fin de poner término al presente asunto litigioso y a cualquier " +
                            "discusión o litigio generado o que pueda generarse derivado de los hechos expuestos, " +
                            "las partes han decidido libremente llegar al presente acuerdo, haciéndose recíprocas " +
                            "concesiones y, evitando cualquier pleito que pudiera derivarse de los mismos hechos.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region TERCERA
                        parrafo = new Paragraph()
                                            .SetFont(calibriNegrita)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("TERCERA").SetUnderline();
                        texto2 = new Text(": ACUERDOS Y CONCESIONES RECÍPROCAS");
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region Las partes
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Las partes intervinientes, haciéndose recíprocas concesiones, han convenido lo siguiente:");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region 3.1
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .6f, 10 })).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("3.1", calibri, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("A fin de poder honrar sus obligaciones, ");
                        parrafo.Add(texto).Add(textoEC);
                        texto = new Text(" ha presentado una propuesta de pago del total de la deuda, la misma que " +
                            "se encuentra detallada en el Anexo 1 – Cronograma de Pagos.");
                        parrafo.Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        #endregion

                        #region 3.2
                        AgregarCelda(tContenido, NuevaCelda("3.2", calibri, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(" se compromete a dejar el saldo de la cuota en su cuenta de haberes y/o " +
                            "cuenta afiliada en el sistema financiero, en caso contrario el cliente no cuente con " +
                            "una cuenta de haberes, esté se compromete a realizar el pago a través de ventanillas " +
                            "(ventanillas y/o agentes del BBVA y/o Interbank o transferencias a nuestras " +
                            "cuentas corrientes) hasta que obtenga una cuenta de pago de haberes, momento en el que " +
                            "se afiliará al débito automático de modo tal, que las cuotas correspondientes al " +
                            "préstamo sean debitadas automáticamente desde dicha cuenta a favor de ");
                        parrafo.Add(textoEC).Add(texto).Add(textoPF).Add(new Text(". ")).Add(textoEC)
                            .Add(new Text(" reconoce el derecho de ")).Add(textoPF);
                        texto = new Text(" a gestionar en cualquier momento el cargo automático de cualquier " +
                            "acreencia vencida o por vencer, a través de la modalidad de cargo automático de " +
                            "sus cuentas afiliadas u otras cuentas que mantenga en cualquier empresa del sistema" +
                            "financiero, incluyendo abonos por concepto de gratificaciones, bonos, entre otros, " +
                            "sin reserva ni limitación alguna. ");
                        parrafo.Add(texto).Add(textoEC);
                        texto = new Text(" está obligado a informar inmediatamente cualquier cambio de institución " +
                            "financiera, número de cuenta, tarjeta y/o cualquier otra modificación que pueda " +
                            "impactar en el descuento automático de las cuotas del préstamo. Sin perjuicio de ello" +
                            ", reconoce que ");
                        parrafo.Add(texto).Add(textoPF);
                        texto = new Text(" tiene puede acceder a esta información, autorizándolo desde ya a " +
                            "obtener la información de ");
                        parrafo.Add(texto).Add(textoEC).Add(new Text(". Asimismo se compromete a: "));
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        #endregion

                        #region 3.3
                        AgregarCelda(tContenido, NuevaCelda("3.3", calibri, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(" exonerará de los intereses moratorios en las cuotas pendientes de " +
                            "pago y acepta la propuesta detallada en Cronograma de Pagos.");
                        parrafo.Add(textoPF).Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        #endregion

                        #region 3.4
                        AgregarCelda(tContenido, NuevaCelda("3.4", calibri, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(" también acepta la propuesta de PRÉSTAMO FELIZ y se compromete a pagar " +
                            "puntualmente las cuotas del nuevo préstamo según el nuevo Cronograma de Pagos aceptado " +
                            "por las partes.");
                        parrafo.Add(textoEC).Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        #endregion

                        #region 3.5
                        AgregarCelda(tContenido, NuevaCelda("3.5", calibri, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Las partes reconocen que la presente transacción extrajudicial es " +
                            "un documento complementario al Contrato de préstamo señalado en la Sección Primera " +
                            "del presente documento, por lo que las cláusulas del referido documento, el pagaré, " +
                            "la autorización de descuento y demás documentos suscritos por ");
                        parrafo.Add(texto).Add(textoEC).Add(new Text(" mantienen su plena vigencia."));
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        documento.Add(tContenido);
                        #endregion

                        #region EC Y PF
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(" declaran su conformidad con el acuerdo transaccional arribado " +
                            "y declaran, además, que no tienen ninguna discrepancia adicional originada por los " +
                            "hechos mencionados en la cláusula Primera y segunda del presente documento. De igual " +
                            "modo, declaran que el presente acuerdo ha sido llevado a cabo con pleno conocimiento " +
                            "de los derechos y obligaciones que a cada uno le concierne, sin que haya mediado " +
                            "ninguna causal de nulidad o anulabilidad que lo invalide.");
                        parrafo.Add(textoPF).Add(new Text(" y ")).Add(textoEC).Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region CUARTA
                        parrafo = new Paragraph()
                                            .SetFont(calibriNegrita)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("CUARTA").SetUnderline();
                        texto2 = new Text(": RENUNCIA DE ACCIONES");
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region Conforme
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Conforme lo exige el artículo 1303° del Código Civil, las partes renuncian " +
                            "a las siguientes acciones sobre el asunto litigioso: ");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region 4.1
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .6f, 10 })).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("4.1", calibri, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(" renuncia a cualquier acción legal dirigida a cobrar la deuda vencida " +
                            "detallada en la cláusula primera.");
                        parrafo.Add(textoPF).Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        #endregion

                        #region 4.2
                        AgregarCelda(tContenido, NuevaCelda("4.2", calibri, fSizeNormal, false, false, false, false, Alineacion.Izquierda), false);
                        parrafo = new Paragraph()
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(" renuncia a cualquier otra acción o pretensión judicial de carácter " +
                            "administrativa, civil y/o penal.");
                        parrafo.Add(textoEC).Add(texto);
                        celda = new Cell()
                            .SetBorder(Border.NO_BORDER);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);
                        documento.Add(tContenido);
                        #endregion

                        #region QUINTA
                        parrafo = new Paragraph()
                                            .SetFont(calibriNegrita)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("QUINTA").SetUnderline();
                        texto2 = new Text(": VALOR DE COSA JUZGADA");
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region Estando
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Estando a lo señalado en el tercer párrafo del artículo 1302° del Código " +
                            "Civil, la presente Transacción tiene el valor de cosa juzgada respecto de cualquier " +
                            "acción de restitución o indemnización para ");
                        texto2 = new Text(", o cualquier otra acción similar o vinculada, que derive de los hechos " +
                            "expuestos.");
                        parrafo.Add(texto).Add(textoEC).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region SEXTA
                        parrafo = new Paragraph()
                                            .SetFont(calibriNegrita)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("SEXTA").SetUnderline();
                        texto2 = new Text(": JURISDICCIÓN");
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region De suscitarse
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("De suscitarse cualquier conflicto relacionado con el cumplimiento de la " +
                            "presente transacción extrajudicial, las partes se someten a la competencia de los " +
                            "Jueces del Distrito Judicial de la Corte Superior de Justicia de Lima, fijando como " +
                            "sus domicilios los indicados en la introducción del presente documento.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region De suscitarse
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + "En señal de conformidad, las partes suscriben este " +
                            "documento, en la Ciudad de Lima el ");
                        texto2 = new Text(DateTime.Today.Day.ToString("00") + " de " + Utils.MesFecha(DateTime.Today) + " del " + DateTime.Today.Year.ToString());
                        parrafo.Add(texto).Add(texto2);
                        documento.Add(parrafo);
                        #endregion

                        #region Firmas
                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();

                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 390 * punto,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 45)) * punto
                            //PosX = 390, //Test
                            //PosY = ((Utils.AltoAreaRestante(documento) + 45)) //Test
                        });
                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0); //Test

                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { 1, 0.3f, 1 })).UseAllAvailableWidth()
                            .SetMarginTop(40)
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal);

                        tContenido.AddCell(new Cell().Add(new Paragraph().Add(new Text("DEPARTAMENTO LEGAL").SetFont(calibriNegrita)).SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().Add(new Paragraph().Add(textoEC).SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER));

                        documento.Add(tContenido);
                        #endregion

                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private int version;
            private Image logoPF;
            private PdfFont calibri;
            private PdfFont calibriNegrita;
            public int PaginaActual { get; set; }

            public Encabezado(int version)
            {
                this.version = version <= 0 ? 1 : version;
                logoPF = AppSettings.LogPF;
                calibri = AppSettings.Calibri;
                calibriNegrita = AppSettings.CalibriNegrita;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    //.Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(50, paginaTamanio.GetTop() - 50, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }

            public void FijarPaginado(Document document, string lugar, string folio)
            {
                //TODO: ajustar metodo con mas de 3 paginas para folio puesto que aroja el error: "Cannot draw elements on already flushed pages."
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                int totalPaginas = pdf.GetNumberOfPages();
                Paragraph paginas;
                switch (version)
                {
                    case 1:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"{lugar} N° {folio}").SetFont(calibriNegrita).SetFontSize(7).SetFontColor(ColorConstants.RED);
                            document.ShowTextAligned(paginas, paginaTamanio.GetWidth() - 90, paginaTamanio.GetTop() - 45, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                }
            }
        }

        protected class CellRadius : CellRenderer
        {
            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}