﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Model.Reportes;
using SOPF.Core.Model.Solicitudes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class Reestructura
    {
        private int version;
        private PdfFont calibri;
        private PdfFont calibriNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private ReestructuraVM datosReestructura;
        private float fSizeNormal = 10.5f;
        private List<string> datosDocumento = new List<string>();
        public List<PosicionFirmaVM> Firmas { get; set; }
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public Reestructura(int version)
        {
            this.version = version <= 0 ? 1 : version;
            calibri = AppSettings.Calibri;
            calibriNegrita = AppSettings.CalibriNegrita;
            Firmas = new List<PosicionFirmaVM>();
        }

        public byte[] Generar(ReestructuraVM ReestructuraDatos)
        {
            byte[] ReestructuraByte = null;
            datosReestructura = ReestructuraDatos;
            switch (version)
            {
                case 1:
                    ReestructuraByte = ReestructuraV1();
                    break;
            }
            return ReestructuraByte;
        }

        private byte[] ReestructuraV1()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            float punto = 0.35277f;
            firma.SetWidth(UnitValue.CreatePercentValue(50));

            try
            {
                if (datosReestructura == null) datosReestructura = new ReestructuraVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LETTER);
                        Encabezado encabezado = new Encabezado(version);
                        documento.SetMargins(45, 80, 45, 80);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        #region Titulo
                        Paragraph parrafo = new Paragraph(Environment.NewLine + "REESTRUCTURACIÓN DE PRÉSTAMO SIMPLE")
                            .SetFont(calibriNegrita)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.CENTER);
                        documento.Add(parrafo);
                        #endregion

                        #region Datos
                        Table tContenido = new Table(UnitValue.CreatePercentArray(new float[] { 1f })).UseAllAvailableWidth()
                            .SetPaddings(0, 0, 0, 0)
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal)
                            .SetVerticalAlignment(VerticalAlignment.BOTTOM);

                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().Add(new Paragraph("I. Datos del Cliente: " + datosReestructura.NombreCliente))
                            .SetBorder(Border.NO_BORDER));

                        datosDocumento.Add(datosReestructura.NombreCliente);

                        tContenido.AddCell(new Cell().Add(new Paragraph("II. Datos del Préstamo: " + datosReestructura.TipoCredito))
                            .SetBorder(Border.NO_BORDER));

                        tContenido.AddCell(new Cell().Add(new Paragraph("III. N° de ID cliente: " + datosReestructura.IdSolicitud))
                            .SetBorder(Border.NO_BORDER));

                        parrafo = new Paragraph();
                        string importe = datosReestructura.ImporteCredito.ToString("C", cultPeru);
                        Text texto = new Text("IV. Restructuración del Préstamo: Importe de ");
                        Text texto2 = new Text(importe + " por "
                            + datosReestructura.PlazoActual.ToString() + " meses. (importe a reestructurar)")
                            .SetFont(calibriNegrita);
                        parrafo.Add(texto).Add(texto2);
                        datosDocumento.Add(importe);

                        tContenido.AddCell(new Cell().Add(parrafo)
                            .SetBorder(Border.NO_BORDER));

                        documento.Add(tContenido);
                        #endregion

                        #region El Cliente
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + "El Cliente identificado el numeral I. del presente documento, mantiene un Préstamo con Préstamo Feliz, cuyas características se encuentran " +
                            " detalladas en el numeral II.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Mediante el
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Mediante el presente documento, Préstamo Feliz y el Cliente acuerdan la Reestructuración del Préstamo en los términos " +
                            "detallados en el numeral IV.El Cliente suscribe el presente documento en señal de conformidad y declarar recibir adjunto a este documento el nuevo " +
                            "cronograma de pago de su préstamo.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region El cliente
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("El cliente reconoce el derecho de Préstamo Feliz a gestionar en cualquier momento el cargo automático de cualquier " +
                            "acreencia vencida, a través de la modalidad de cargo automático de sus cuentas o tarjetas afiliadas u otras cuentas que mantenga en cualquier empresa " +
                            "del sistema financiero, incluyendo abonos por concepto de gratificaciones, bonos, entre otros, sin reserva ni limitación alguna; así como el descuento " +
                            "en la Planilla de Pagos.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Asimismo
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Asimismo, de manera excepcional y por la situación que está atravesando EL CLIENTE, Prestamo Feliz realizará la exoneración de " +
                            datosReestructura.Condonado.ToString("C", cultPeru) + " de intereses corridos a la fecha de " + datosReestructura.Vencidas.ToString() +
                            " cuotas vencidas, así como el extorno del 100% de intereses moratorios y penalidades por pago tardío.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Comprometiéndose
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Comprometiéndose el cliente a dejar desde el mes presente el importe de " + datosReestructura.NuevaCuota.ToString("C", cultPeru) +
                            " donde se le estará debitando los días de pago de sus remuneraciones por el periodo de " + datosReestructura.NuevoPlazo.ToString() + " meses. Estos débitos se " +
                            "realizarán a la cuenta de haberes que mantenga el cliente en el sistema financiero. Asimismo, el cliente está obligado a informar inmediatamente cualquier cambio " +
                            "de institución financiera, número de cuenta, tarjeta y/o cualquier otra modificación que pueda impactar en el descuento automático de las cuotas del préstamo. " +
                            "Sin perjuicio de ello reconoce que Prestamo Feliz puede acceder a esta información, autorizándolo desde ya a obtener la información del cliente.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Todos
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Todos los términos y condiciones que rigen el préstamo y que no hayan sido expresamente modificados por el presente documento, mantendrán " +
                            "su plena validez, vigencia y eficacia.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Lima
                        parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Lima, " + DateTime.Today.Day.ToString("00") + " de " + Utils.MesFecha(DateTime.Today) + " del " + DateTime.Today.Year.ToString());
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Firmas
                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();

                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            //PosX = 120,//test                            
                            //PosY = (Utils.AltoAreaRestante(documento) + 48) //Para test
                            PosX = 85 * punto,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 48)) * punto
                        });
                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0); //Test

                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { 1, 0.3f, 1 })).UseAllAvailableWidth()
                            .SetMarginTop(40)
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal);

                        tContenido.AddCell(new Cell().Add(new Paragraph("El Cliente").SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new SolidBorder(0.5f)));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().Add(new Paragraph("Departamento Legal").SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new SolidBorder(0.5f)));

                        documento.Add(tContenido);
                        #endregion

                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private int version;
            private Image logoPF;
            private PdfFont calibri;
            private PdfFont calibriNegrita;
            public int PaginaActual { get; set; }

            public Encabezado(int version)
            {
                this.version = version <= 0 ? 1 : version;
                logoPF = AppSettings.LogPF;
                calibri = AppSettings.Calibri;
                calibriNegrita = AppSettings.CalibriNegrita;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(50, paginaTamanio.GetTop() - 50, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }

            public void FijarPaginado(Document document, string lugar, string folio)
            {
                //TODO: ajustar metodo con mas de 3 paginas para folio puesto que aroja el error: "Cannot draw elements on already flushed pages."
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                int totalPaginas = pdf.GetNumberOfPages();
                Paragraph paginas;
                switch (version)
                {
                    case 1:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"{lugar} N° {folio}").SetFont(calibriNegrita).SetFontSize(7).SetFontColor(ColorConstants.RED);
                            document.ShowTextAligned(paginas, paginaTamanio.GetWidth() - 90, paginaTamanio.GetTop() - 45, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                }
            }
        }

        protected class CellRadius : CellRenderer
        {
            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}