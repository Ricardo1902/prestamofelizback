﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Model.Reportes;
using SOPF.Core.Model.Solicitudes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class Solicitud
    {
        private int version;
        private PdfFont calibri;
        private PdfFont calibriNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private SolicitudFormatoVM datosSolicitud;
        private float fSizeNormal = 5f;
        private iText.Kernel.Colors.Color negro = ColorConstants.BLACK;
        private iText.Kernel.Colors.Color azulPeru = new DeviceRgb(16, 38, 79);
        private List<string> datosDocumento = new List<string>();
        public List<PosicionFirmaVM> Firmas { get; set; }
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public Solicitud(int version)
        {
            this.version = version <= 0 ? 1 : version;
            calibri = AppSettings.Calibri;
            calibriNegrita = AppSettings.CalibriNegrita;
            Firmas = new List<PosicionFirmaVM>();
        }

        public byte[] Generar(SolicitudFormatoVM SolicitudDatos)
        {
            byte[] SolicitudByte = null;
            datosSolicitud = SolicitudDatos;
            switch (version)
            {
                case 1:
                    SolicitudByte = SolicitudV1();
                    break;
            }
            return SolicitudByte;
        }

        private byte[] SolicitudV1()
        {
            byte[] archivo = null;
            Image logo = AppSettings.LogPF;
            logo.SetWidth(UnitValue.CreatePercentValue(100));
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            float punto = 0.35277f;
            firma.SetWidth(UnitValue.CreatePercentValue(50));

            try
            {
                if (datosSolicitud == null) datosSolicitud = new SolicitudFormatoVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL);
                        Encabezado encabezado = new Encabezado(version);
                        documento.SetMargins(65, 20, 65, 20);
                        //pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);
                        //Image logoPF = AppSettings.LogPF;
                        //logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                        //Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                        //Cell celda = new Cell()
                        //    .Add(logoPF)
                        //    .SetBorder(Border.NO_BORDER);
                        //tEncabezado.AddCell(celda);
                        //tEncabezado.SetFixedPosition(50, documento.GetTopMargin() - 50, documento.GetRightMargin() - 60);
                        //documento.Add(tEncabezado);

                        Cell celdaV = new Cell(1, 1).SetBorder(Border.NO_BORDER);
                        string salto = Environment.NewLine;

                        #region Encabezado
                        Table tEncabezado = new Table(UnitValue.CreatePercentArray(8)).UseAllAvailableWidth();
                        Cell celda = new Cell(1, 2).SetBorder(Border.NO_BORDER).Add(new Paragraph().Add(logo));
                        tEncabezado.AddCell(celda);
                        celda = NuevaCelda("SOLICITUD DE CREDITO SIMPLE" + salto + "PRÉSTAMO FELIZ EN 15 MINUTOS S.A.C.", calibriNegrita, fSizeNormal, false, false, false, false, 1, 4, Alineacion.Centro, PdfCelda.Color.BLACK).SetVerticalAlignment(VerticalAlignment.MIDDLE);
                        tEncabezado.AddCell(celda);
                        celda = new Cell(1, 2).SetBorder(Border.NO_BORDER).SetVerticalAlignment(VerticalAlignment.MIDDLE).SetTextAlignment(TextAlignment.LEFT).Add(new Paragraph().SetFont(calibriNegrita).SetFontSize(fSizeNormal).Add(new Text("CRÉDITO FELIZ" + salto + "N°. DE SOLICITUD: ")).Add(new Text(datosSolicitud.IdSolicitud.ToString()).SetFontColor(ColorConstants.RED)));
                        tEncabezado.AddCell(celda);
                        celda = NuevaCelda("Vendedor: " + datosSolicitud.Vendedor, calibri, fSizeNormal, true, true, true, true, 1, 5, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tEncabezado.AddCell(celda);
                        tEncabezado.AddCell(celdaV);
                        celda = NuevaCelda("Fecha: " + DateTime.Today.Day.ToString("00") + "/" + DateTime.Today.Month.ToString("00") + "/" + DateTime.Today.Year.ToString("00"), calibri, fSizeNormal, true, true, true, true, 1, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tEncabezado.AddCell(celda);
                        documento.Add(tEncabezado);
                        #endregion

                        #region Cuerpo
                        Table tabla = new Table(UnitValue.CreatePercentArray(10)).UseAllAvailableWidth().SetMarginTop(5f);
                        celda = new Cell(1, 10).SetBackgroundColor(azulPeru).SetTextAlignment(TextAlignment.CENTER).Add(new Paragraph().Add(new Text("I DATOS DEL SOLICITANTE").SetFont(calibriNegrita).SetFontSize(fSizeNormal).SetFontColor(ColorConstants.WHITE)));
                        tabla.AddCell(celda);
                        celda = NuevaCelda("Nombre (s)" + salto + datosSolicitud.NombresCliente, calibri, fSizeNormal, true, true, true, true, 1, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda("Apellido Paterno                Apellido Materno" + salto + datosSolicitud.ApellidoPaternoCliente + " " + datosSolicitud.ApellidoMaternoCliente, calibri, fSizeNormal, true, true, true, true, 1, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda("Sexo" + salto + (datosSolicitud.Sexo == "NA" ? datosSolicitud.Sexo : (datosSolicitud.Sexo == "Masculino" ? "M" : "F")), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda("Fecha de Nacimiento (DD/MM/AA)" + salto + datosSolicitud.FechaNacimiento.ToString("dd/MM/yy"), calibri, fSizeNormal, true, true, true, true, 1, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        
                        celda = NuevaCelda("Edad" + salto + GetAge(datosSolicitud.FechaNacimiento).ToString(), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda("Lugar de Nacimiento" + salto + datosSolicitud.LugarNacimiento, calibri, fSizeNormal, true, true, true, true, 1, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda("País de Nacimiento" + salto + datosSolicitud.PaisNacimiento, calibri, fSizeNormal, true, true, true, true, 1, 4, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda("Ocupación" + salto + datosSolicitud.Ocupacion, calibri, fSizeNormal, true, true, true, true, 1, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);

                        string Sl = string.Empty;
                        switch (datosSolicitud.SituacionLaboral)
                        {
                            case "Dependiente": Sl = "D"; break;
                            case "Independiente": Sl = "I"; break;
                            case "CAS": Sl = "C"; break;
                            case "Jubilado": Sl = "J"; break;
                            case "Pensionista": Sl = "P"; break;
                            case "Indefinido - Nombrado(728)": Sl = "IN"; break;
                            case "Plazo fijo - Contratado (276)": Sl = "PF"; break;
                            case "Civil Activo": Sl = "CA"; break;
                            case "Militar en Actividad": Sl = "M"; break;
                            default: break;
                        }   
                        celda = NuevaCelda(string.Concat("Situación Laboral:    Dependiente(", Sl == "D" ? "X" : " ", ")"
                            , "     Independiente(", Sl == "I" ? "X" : " ", ")", "      CAS(", Sl == "C" ? "X" : " ", ")"
                            , "     Jubilado(", Sl == "J" ? "X" : " ", ")", "       Pensionista(", Sl == "P" ? "X" : " ", ")"
                            , "     Indefinido - Nombrado(728)(", Sl == "IN" ? "X" : " ", ")", salto
                            , "                 Plazo fijo - Contratado (276)(", Sl == "PF" ? "X" : " ", ")"
                            , "     Civil Activo(", Sl == "CA" ? "X" : " ", ")", "      Militar en Actividad(", Sl == "M" ? "X" : " ", ")"), calibri, fSizeNormal, true, true, true, true, 1, 5, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        string Rp = string.Empty;
                        switch (datosSolicitud.RegimenPension)
                        {
                            case "JUBILACIÓN": Rp = "J"; break;
                            case "D. LEY N° 20530": Rp = "D2"; break;
                            case "D. LEY N° 19990": Rp = "D1"; break;
                            case "VIUDEZ": Rp = "V"; break;
                            case "INVALIDEZ PERMANENTE": Rp = "I"; break;
                            case "ORFANDAD": Rp = "O"; break;
                            case "OTROS": Rp = "OT"; break;
                            case "N/A": Rp = "N"; break;
                            case "CAJA DE PENSIONES": Rp = "C"; break;
                            case "AFP": Rp = "A"; break;
                            case "MONTEPIO": Rp = "M"; break;
                            default: break;
                        }   
                        celda = NuevaCelda(string.Concat("Tipo de Pensión:     Jubilación(", Rp == "J" ? "X" : " ", ")"
                            , "     D. Ley N° 20530(", Rp == "D2" ? "X" : " ", ")", "     D. Ley N° 19990(", Rp == "D1" ? "X" : " ", ")"
                            , "     Viudez(", Rp == "V" ? "X" : " ", ")", "     Invalidez Permanente(", Rp == "I" ? "X" : " ", ")", salto
                            , "                 Orfandad(", Rp == "O" ? "X" : " ", ")", "     Otros(", Rp == "OT" ? "X" : " ", ")"
                            , "     N/A(", Rp == "N" ? "X" : " ", ")", "     Caja de Pensiones(", Rp == "C" ? "X" : " ", ")"
                            , "     AFP(", Rp == "A" ? "X" : " ", ")", "     Montepio(", Rp == "M" ? "X" : " ", ")"), calibri, fSizeNormal, true, true, true, true, 1, 5, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);

                        string Td = string.Empty;
                        switch (datosSolicitud.TipoDocumento)
                        {
                            case "Documento Nacional de Identidad": Td = "D"; break;
                            case "D.N.I. Electrónico": Td = "DN"; break;
                            case "Carnet de Extranjería": Td = "C"; break;
                            case "Otros": Td = "O"; break;
                            default: break;
                        }
                        celda = NuevaCelda(string.Concat("Tipo de Documento:", "        Documento Nacional de Identidad(", Td == "D" ? "X" : " ", ")"
                            , salto, "      D.N.I. Electrónico(", Td == "DN" ? "X" : " ", ")", "    Carnet de Extranjería(", Td == "C" ? "X" : " ", ")"
                            , "     Otros(", Td == "O" ? "X" : " ", ")"), calibri, fSizeNormal, true, true, true, true, 1, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda("Ocupación: " + salto + datosSolicitud.Ocupacion, calibri, fSizeNormal, true, true, true, true, 1, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Número de documento: ", salto, datosSolicitud.NumeroDocumento == "00000000" ? "Sin Información" : datosSolicitud.NumeroDocumento), calibri, fSizeNormal, true, true, true, true, 1, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda("Correo Electrónico Personal (en caso cuente con él)" + salto + datosSolicitud.Correo, calibri, fSizeNormal, true, true, true, true, 1, 3, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);

                        string Tdi = string.Empty;
                        switch (datosSolicitud.TipoDireccion)
                        {
                            case "ALAMEDA": Tdi = "A"; break;
                            case "AVENIDA": Tdi = "AV"; break;
                            case "BLOCK": Tdi = "B"; break;
                            case "JIRÓN": Tdi = "J"; break;
                            case "MALECÓN": Tdi = "M"; break;
                            case "PLAZA": Tdi = "P"; break;
                            case "OVALO": Tdi = "O"; break;
                            case "CALLE": Tdi = "C"; break;
                            case "PARQUE": Tdi = "PA"; break;
                            case "PASAJE": Tdi = "PAS"; break;
                            case "CARRETERA": Tdi = "CA"; break;
                            case "N/A": Tdi = "N"; break;
                            default: break;
                        }
                        celda = NuevaCelda(string.Concat(salto, "Dirección"), calibri, fSizeNormal, true, true, true, true, 3, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Alameda         (", Tdi == "A" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Avenida          (", Tdi == "AV" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Lote            (", Tdi == "B" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Jirón         (", Tdi == "J" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Nombre de la vía: ", salto, datosSolicitud.NombreVia), calibri, fSizeNormal, true, true, true, true, 3, 5, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Malecón         (", Tdi == "M" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Plaza               (", Tdi == "P" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Ovalo         (", Tdi == "O" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Calle         (", Tdi == "C" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Parque             (", Tdi == "PA" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Pasaje              (", Tdi == "PAS" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Carretera: ", salto, Tdi == "CA" ? datosSolicitud.NombreVia : ""), calibri, fSizeNormal, true, true, true, true, 1, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);

                        celda = NuevaCelda(string.Concat("Nro.:", salto, "", datosSolicitud.NumeroExterior), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Piso:", salto, ""), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Interior:", salto, datosSolicitud.NumeroInterior), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        string Tz = string.Empty;
                        switch (datosSolicitud.TipoZona)
                        {
                            case "ASOCIACIÓN": Tz = "A"; break;
                            case "AA.HH.": Tz = "AA"; break;
                            case "CONJ. HAB.": Tz = "C"; break;
                            case "COOPERATIVA": Tz = "CO"; break;
                            case "CASERIO": Tz = "CA"; break;
                            case "FUNDO": Tz = "F"; break;
                            case "GRUPO": Tz = "G"; break;
                            case "RESIDENCIAL": Tz = "R"; break;
                            case "PP.JJ.": Tz = "P"; break;
                            case "UNID. VECINAL": Tz = "U"; break;
                            case "URBANIZACIÓN": Tz = "UR"; break;
                            case "ZONA INDUSTRIAL": Tz = "Z"; break;
                            case "N/A.": Tz = "N"; break;
                            default: break;
                        }
                        celda = NuevaCelda(string.Concat(salto, "Tipo de Zona"), calibri, fSizeNormal, true, true, true, true, 3, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Asociación          (", Tz == "A" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Caserío         (", Tz == "CA" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("PP.JJ.                        (", Tz == "P" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Conj. Hab.          (", Tz == "C" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Nombre de la zona:", salto, datosSolicitud.NombreZona), calibri, fSizeNormal, true, true, true, true, 3, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Nro. Dpto:", salto, ""), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Manzana:", salto, datosSolicitud.Manzana), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Sector:", salto, ""), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Cooperativa       (", Tz == "CO" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Grupo           (", Tz == "G" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Unid. Vecinal        (", Tz == "U" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Urbanización     (", Tz == "UR" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Oficina:", salto, ""), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Lote:", salto, datosSolicitud.Lote), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Km.:", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Residencial        (", Tz == "R" ? "X" : " " , ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Fundo           (", Tz == "F" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("Zona Industrial     (", Tz == "Z" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda(string.Concat("AA.HH                 (", Tz == "AA" ? "X" : " ", ")", salto), calibri, fSizeNormal, true, true, true, true, 1, 1, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);

                        celda = NuevaCelda("Departamento:" + salto + datosSolicitud.Departamento, calibri, fSizeNormal, true, true, true, true, 1, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda("Provincia:" + salto + datosSolicitud.Provincia, calibri, fSizeNormal, true, true, true, true, 1, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);
                        celda = NuevaCelda("Distrito:" + salto + datosSolicitud.Distrito, calibri, fSizeNormal, true, true, true, true, 1, 6, Alineacion.Izquierda, PdfCelda.Color.BLACK);
                        tabla.AddCell(celda);

                        documento.Add(tabla);
                        #endregion

                        #region Lima
                        Paragraph parrafo = new Paragraph()
                                            .SetFont(calibri)
                                            .SetFontSize(fSizeNormal)
                                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        Text texto = new Text("Lima, " + DateTime.Today.Day.ToString("00") + " de " + Utils.MesFecha(DateTime.Today) + " del " + DateTime.Today.Year.ToString());
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Firmas
                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();

                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 85,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 300)) * punto
                        });
                        documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);

                        Table tContenido = new Table(UnitValue.CreatePercentArray(new float[] { 1, 0.3f, 1 })).UseAllAvailableWidth()
                            .SetMarginTop(40)
                            .SetFont(calibri)
                            .SetFontSize(fSizeNormal);

                        tContenido.AddCell(new Cell().Add(new Paragraph("El Cliente").SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new SolidBorder(0.5f)));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().Add(new Paragraph("Departamento Legal").SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new SolidBorder(0.5f)));

                        documento.Add(tContenido);
                        #endregion

                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        private int GetAge(DateTime dateOfBirth)
        {
            var today = DateTime.Today;

            var a = (today.Year * 100 + today.Month) * 100 + today.Day;
            var b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;

            return (a - b) / 10000;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private int version;
            private Image logoPF;
            private PdfFont calibri;
            private PdfFont calibriNegrita;
            public int PaginaActual { get; set; }

            public Encabezado(int version)
            {
                this.version = version <= 0 ? 1 : version;
                logoPF = AppSettings.LogPF;
                calibri = AppSettings.Calibri;
                calibriNegrita = AppSettings.CalibriNegrita;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                //PdfDocument pdf = docEvento.GetDocument();
                //PdfPage pagina = docEvento.GetPage();
                //Rectangle paginaTamanio = pagina.GetPageSize();
                //logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                //Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                //Cell celda = new Cell()
                //    .Add(logoPF)
                //    .SetBorder(Border.NO_BORDER);
                //tEncabezado.AddCell(celda);
                //tEncabezado.SetFixedPosition(50, paginaTamanio.GetTop() - 50, paginaTamanio.GetWidth() - 60);
                //Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                //canvas.Add(tEncabezado);
                //canvas.Close();
                PaginaActual++;
            }

            public void FijarPaginado(Document document, string lugar, string folio)
            {
                //TODO: ajustar metodo con mas de 3 paginas para folio puesto que aroja el error: "Cannot draw elements on already flushed pages."
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                int totalPaginas = pdf.GetNumberOfPages();
                Paragraph paginas;
                switch (version)
                {
                    case 1:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"{lugar} N° {folio}").SetFont(calibriNegrita).SetFontSize(7).SetFontColor(ColorConstants.RED);
                            document.ShowTextAligned(paginas, paginaTamanio.GetWidth() - 90, paginaTamanio.GetTop() - 45, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                }
            }
        }

        protected class CellRadius : CellRenderer
        {
            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}