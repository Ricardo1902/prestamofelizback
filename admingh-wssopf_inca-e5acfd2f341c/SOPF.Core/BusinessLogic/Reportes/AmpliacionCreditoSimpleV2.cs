﻿using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.IO;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public partial class AmpliacionCreditoSimple
    {
        private float fixedLeading = 14;
        private const float punto = 0.35277f;
        private List<string> datosDocumento = new List<string>();
        public List<PosicionFirmaVM> Firmas { get; set; }
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        private byte[] AmpliacionV2()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            firma.SetWidth(UnitValue.CreatePercentValue(100));
            try
            {
                if (datosAmp == null)
                    datosAmp = new AmpliacionCreditoVM();

                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.A4);
                        documento.SetMargins(50, 30, 30, 30);
                        Encabezado encabezado = new Encabezado(version);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        #region Titulo
                        Table tContenido = new Table(UnitValue.CreatePercentArray(2)).UseAllAvailableWidth();
                        logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                        Cell celda = new Cell()
                            .Add(logoPF)
                            .SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);

                        celda = NuevaCelda("", arial, 8);
                        celda.SetBorder(Border.NO_BORDER);
                        AgregarCelda(tContenido, celda, false);

                        documento.Add(tContenido);

                        Paragraph parrafo = new Paragraph(Environment.NewLine + "AMPLIACIÓN DE PRÉSTAMO SIMPLE")
                            .SetFont(arialNegrita)
                            .SetFontSize(10)
                            .SetTextAlignment(TextAlignment.CENTER);
                        documento.Add(parrafo);
                        #endregion

                        #region Nota
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .6f, 10 })).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("1.", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Derecha), false);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10)
                            .SetPaddingLeft(tab);
                        Text texto = new Text($"Datos del Cliente:{Environment.NewLine}")
                            .SetFont(arialNegrita);
                        parrafo.Add(texto);
                        texto = new Text($"Nombres y Apellidos: {datosAmp.Nombres} {datosAmp.ApellidoPaterno} {datosAmp.ApellidoMaterno}{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"DNI: {datosAmp.NumeroDocumento}");
                        parrafo.Add(texto);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);

                        AgregarCelda(tContenido, NuevaCelda("2.", arialNegrita, fSizeNormal, false, false, false, false, Alineacion.Derecha), false);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10)
                            .SetPaddingLeft(tab);
                        texto = new Text($"Datos de la ampliación del préstamo:{Environment.NewLine}")
                            .SetFont(arialNegrita);
                        parrafo.Add(texto);
                        texto = new Text($"Monto anterior: {datosAmp.MontoAnterior.ToString("C", cultPeru)}{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Desembolso adicional: {datosAmp.MontoDesembolso.ToString("C", cultPeru)}{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Monto de préstamo con ampliación: {datosAmp.MontoCredito.ToString("C", cultPeru)}{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Tasa de Interés: {datosAmp.TasaInteres:00.00} %{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Tasa de costo efectiva anual: {datosAmp.TasaCostoEfectivoAnual:00.00} %{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Monto de la cuota: {datosAmp.Erogacion.ToString("C", cultPeru)}{Environment.NewLine}");
                        parrafo.Add(texto);
                        texto = new Text($"Cantidad de cuotas: {datosAmp.TotalRecibos}");
                        parrafo.Add(texto);
                        celda = new Cell().Add(parrafo).SetBorder(Border.NO_BORDER);
                        tContenido.AddCell(celda);

                        datosDocumento.Add($"{datosAmp.Nombres} {datosAmp.ApellidoPaterno} {datosAmp.ApellidoMaterno}");
                        datosDocumento.Add($"{datosAmp.NumeroDocumento}");

                        documento.Add(tContenido);
                        #endregion

                        #region Primera
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10);
                        texto = new Text($"Primera: Modificación del Préstamo. -{Environment.NewLine}")
                            .SetFont(arialNegrita);
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add("Préstamo Feliz y el Cliente acuerdan la Modificación del Préstamo en los términos detallados en el numeral 2. ");
                        parrafo.Add("El Cliente suscribe el presente documento en señal de conformidad y declarar estar de acuerdo con la recepción de la Hoja Resumen ");
                        parrafo.Add($"y Cronograma de Pagos del Préstamo a través del siguiente medio electrónico: {datosAmp.CorreoElectronico}");
                        parrafo.Add(Environment.NewLine);
                        documento.Add(parrafo);
                        #endregion

                        #region Segunda
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10);
                        texto = new Text($"Segunda: Modalidad de Pago:{Environment.NewLine}")
                            .SetFont(arialNegrita);
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add("El préstamo otorgado será pagado mediante Débito Automático a través de cualquiera de las cuentas que el Cliente tenga afiliadas " +
                            "al sistema financiero, pudiendo ser utilizada como mecanismo de cobro, las tarjetas de débito o crédito asociadas a dichas cuentas. El " +
                            "número de cuenta de Préstamo Feliz y la modalidad de pago se consignan en la Hoja de Resumen. En caso no sea posible realizar el cobro " +
                            "de las cuotas a través del Débito Automático, el Cliente es responsable de pagar las cuotas a favor de Préstamo Feliz a través de cualquier " +
                            "otra modalidad como descuento por planilla, depósito en cuenta o cheque, lo que tendrá como consecuencia la aplicación de la tasa máxima permitida.");
                        parrafo.Add(Environment.NewLine);
                        parrafo.Add("Las cuotas del préstamo incluyen: capital, interés compensatorio, interés moratorio, comisiones, gastos, seguros y tributos, según lo informado " +
                            "en la Hoja Resumen. El pago de las cuotas se realizará en la periodicidad y fechas establecidas en el Cronograma de Pagos, el mismo que considerará " +
                            "como fecha de inicio para el cómputo del plazo del préstamo, la fecha del desembolso de éste.");
                        parrafo.Add(Environment.NewLine);
                        parrafo.Add("Débito Automático: El Cliente deberá firmar por única vez y en blanco la Autorización de Débito Automático, Solicitud de Cargo en Cuenta y/o " +
                            "documento correspondiente en favor de Préstamo Feliz de modo tal que, la modalidad de pago a través de débito automático surta efectos para todas sus " +
                            "cuentas afiliadas al sistema financiero, durante toda la vigencia del presente Contrato, garantizando el derecho de Préstamo Feliz a gestionar el cobro " +
                            "automáticamente al vencimiento de las cuotas del préstamo a través de cualquiera de ellas. El Cliente reconoce el derecho de Préstamo Feliz a gestionar " +
                            "en cualquier momento el cargo automático de cualquier acreencia vencida o por vencer.");
                        parrafo.Add(Environment.NewLine);
                        parrafo.Add("La Autorización de Débito Automático y solicitud de Cargo en Cuenta, se extiende a todas las cuentas y/o tarjetas afiliadas al sistema financiero " +
                            "que el Cliente pudiera tener a su nombre. En virtud de ello autoriza a Préstamo Feliz para que, ante algún eventual impedimento de cobro en la cuenta " +
                            "principal, proceda con el llenado de formatos requeridos para la gestión de cobro por débito automático de sus cuotas de pago del préstamo vencidas o " +
                            "por vencer, a través de cualquiera de sus cuentas y/o tarjetas afiliadas al sistema financiero, reconociendo por este acto que dicha autorización tiene " +
                            "validez durante toda la vigencia del contrato de préstamo hasta el pago total del mismo.");
                        parrafo.Add(Environment.NewLine);
                        parrafo.Add("Poder Irrevocable: El Cliente otorga poder expreso e irrevocable a Préstamo Feliz para que, en nombre y representación del Cliente pueda solicitar, " +
                            "obtener y gestionar a entidades financieras públicas o privadas, ante cualquier organismo nacional o internacional, así como ante su empleador, cualquier " +
                            "información acerca del monto de su sueldo, cuentas bancarias, tarjetas de crédito o tarjetas de débito. El Cliente declara conocer y entender que el presente " +
                            "poder otorgar el acceso por parte de Préstamo Feliz, a información protegida por la normativa de datos personales y / o ley de secreto bancario, por lo que el " +
                            "Cliente autoriza expresamente el levantamiento de su secreto bancario o reserva bancaria, para que el destinatario de este documento pueda brindar esta " +
                            "información a los funcionarios de Préstamo Feliz, así como el llenado de los formatos requeridos para la gestión del pago de sus cuotas bajo cualquiera de " +
                            "las modalidades descritas en la presente cláusula, por tanto el Cliente reconoce que se desiste desde ya de efectuar reclamo alguno ante Préstamo Feliz, las " +
                            "entidades anteriormente mencionadas o cualquier organismo de administración de justicia.");
                        parrafo.Add(Environment.NewLine);
                        documento.Add(parrafo);
                        #endregion

                        #region Tercera
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10);
                        texto = new Text($"Tercera: Sistemas Digitales {Environment.NewLine}")
                            .SetFont(arialNegrita);
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add("El Cliente podrá acceder y utilizar los sistemas y aplicaciones digitales que Préstamo Feliz ponga a su disposición.El Cliente manifiesta expresamente " +
                            "conocer que, los siguientes mecanismos de autenticación: ingreso de claves o contraseñas, clic o cliquear en dispositivos, aceptación por voz, datos biométricos " +
                            "(huella dactilar, identificación facial, etc.), códigos, autogeneración de códigos, entre otros mecanismos que Préstamo Feliz ponga a su disposición, sustituyen " +
                            "su firma autógrafa para la contratación o ampliación de las operaciones y servicios.Las partes reconocen que, el uso de los medios de autenticación producen los " +
                            "mismos efectos que las leyes otorgan a la firma autógrafa, entre éstos la identificación de las partes y el acuerdo libre y expreso de voluntades.");
                        parrafo.Add(Environment.NewLine + Environment.NewLine);
                        documento.Add(parrafo);
                        #endregion

                        string dia = string.Empty;
                        string mes = string.Empty;
                        string anio = string.Empty;
                        string fecha = string.Empty;
                        DateTime hoy = DateTime.Now;
                        dia = hoy.Day.ToString("00");
                        mes = Utils.MesFecha(hoy);
                        anio = hoy.Year.ToString();
                        fecha = dia + " de " + mes + " del " + anio;

                        #region Todos
                        parrafo = new Paragraph()
                            .SetFont(arial)
                            .SetFontSize(10)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add("Todos los términos y condiciones que rigen el préstamo y que no hayan sido expresamente modificados por el presente documento, ");
                        parrafo.Add("mantendrán su plena validez, vigencia y eficacia.");
                        parrafo.Add(Environment.NewLine + Environment.NewLine);
                        parrafo.Add($"Lima, {DateTime.Now.Day:00} de {Utils.MesFecha(DateTime.Now)} de {DateTime.Now.Year}");
                        documento.Add(parrafo);
                        #endregion

                        #region Firmas
                        tContenido = new Table(UnitValue.CreatePercentArray(3)).UseAllAvailableWidth();
                        tContenido.SetMarginTop(50);

                        tContenido.AddCell(new Cell().Add(new Paragraph("")
                            .SetFont(arialNegrita).SetFontSize(8))
                            .SetVerticalAlignment(VerticalAlignment.BOTTOM)
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        Table tSubTabla = new Table(UnitValue.CreatePercentArray(3)).UseAllAvailableWidth();
                        tSubTabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tSubTabla.AddCell(new Cell().Add(firma).SetBorder(Border.NO_BORDER));
                        tSubTabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(tSubTabla));
                        AgregarCelda(tContenido, NuevaCelda("El Cliente", arial, fSizeNormal, true, false, false, false, Alineacion.Centro), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                        AgregarCelda(tContenido, NuevaCelda("Préstamo Feliz", arial, fSizeNormal, true, false, false, false, Alineacion.Centro), false);
                        documento.Add(tContenido);

                        #region PosicionFirma
                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();

                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 75 * punto,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 95)) * punto
                            //PosX = 75,//test
                            //PosY = (Utils.AltoAreaRestante(documento) + 95)//Para test
                        });

                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0); //Para test
                        #endregion

                        #endregion

                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        protected partial class Encabezado : IEventHandler
        {
            public int PaginaActual { get; set; }
            private PdfDocumentEvent docEvento;
            private int version;
            private PdfFont arial;
            private PdfFont arialN;
            private float tNomal = 11;

            public Encabezado(int version)
            {
                this.version = version <= 0 ? 1 : version;
                arial = AppSettings.Arial;
                arialN = AppSettings.ArialNegrita;

                switch (version)
                {
                    case 1:
                        tNomal = 11;
                        break;
                }
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 2:
                        EncabezadoV2();
                        break;
                }
            }

            private void EncabezadoV2()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();

                PaginaActual++;
            }
        }
    }
}
