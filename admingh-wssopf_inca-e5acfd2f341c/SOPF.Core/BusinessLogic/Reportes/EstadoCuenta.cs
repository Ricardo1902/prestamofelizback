﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class EstadoCuenta
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private EstadoCuentaVM datosEstadoCuenta;
        private float fSizeNormal = 8;
        private float fSizeTabla = 7;
        public List<PosicionFirmaVM> Firmas { get; set; }
        private List<string> datosDocumento = new List<string>();
        private iText.Kernel.Colors.Color colorAzulPeru = new DeviceRgb(16, 38, 79);
        private iText.Kernel.Colors.Color colorNormal = new DeviceRgb(System.Drawing.Color.Black);
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public EstadoCuenta(int version)
        {
            this.version = version <= 0 ? 1 : version;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
            Firmas = new List<PosicionFirmaVM>();
        }

        public byte[] Generar(EstadoCuentaVM estadoCuentaDatos)
        {
            byte[] estadoCuentaByte = null;
            datosEstadoCuenta = estadoCuentaDatos;
            switch (version)
            {
                case 1:
                    estadoCuentaByte = EstadoCuentaV1();
                    break;
            }
            return estadoCuentaByte;
        }

        private byte[] EstadoCuentaV1()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            firma.SetWidth(UnitValue.CreatePercentValue(50));
            float punto = 0.35277f;

            try
            {
                if (datosEstadoCuenta == null) datosEstadoCuenta = new EstadoCuentaVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LETTER);
                        Encabezado encabezado = new Encabezado(version, datosEstadoCuenta.IdSolicitud);
                        documento.SetMargins(65, 35, 25, 35);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        Paragraph parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal);
                        parrafo.Add(Environment.NewLine + "ESTATUS DEL CREDITO: " + datosEstadoCuenta.EstatusCredito);
                        documento.Add(parrafo);

                        #region Info
                        Table tContenido = new Table(UnitValue.CreatePercentArray(4)).UseAllAvailableWidth();
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .25f, .25f, .25f, .25f })).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("Solicitud", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda("Cliente", arial, fSizeNormal, false, false, false, false, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda("Fecha Crédito", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.IdSolicitud.ToString(), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.Cliente, arial, fSizeNormal, true, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.FechaCredito, arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda("Monto Uso Canal", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda("Monto GAT", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda("Cuota", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda("Capital", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.MontoUsoCanal.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.MontoGAT.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.Cuota.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.Capital.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda("Producto", arial, fSizeNormal, false, false, false, false, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda("Saldo Vencido", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda("Costo Total Crédito", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.Producto, arial, fSizeNormal, true, true, true, true, 0, 2, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.SaldoVencido.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.CostoTotalCredito.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda("Tipo Credito", arial, fSizeNormal, true, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda("Monto Comisión Uso de Canal", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                        AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.TipoCredito, arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        AgregarCelda(tContenido, NuevaCelda(datosEstadoCuenta.MontoComisionUsoCanal.ToString("C", cultPeru), arial, fSizeNormal, true, true, true, true, 0, 0, Alineacion.Izquierda, PdfCelda.Color.BLACK), false);
                        tContenido.SetMarginTop(20);
                        documento.Add(tContenido);
                        //datosDocumento.Add("EstadoCuenta");
                        #endregion

                        documento.Add(new Paragraph().Add(Environment.NewLine));

                        #region Recibos
                        Dictionary<string, string> tituloEntidad = new Dictionary<string, string> {
                            {"#", "Recibo" },
                            {"Fecha", "FechaRecibo" },
                            {"Cap. Ini.", "CapitalInicial" },
                            {"S. Capital", "SaldoCapital" },
                            {"S. Interes", "SaldoInteres" },
                            {"S. IGV", "SaldoIGV" },
                            {"S. Seguro", "SaldoSeguro" },
                            {"S. GAT", "SaldoGAT" },
                            {"S. Comisión", "SaldoOtrosCargos" },
                            {"S. IGV Comisión", "SaldoIGVOtrosCargos" },
                            {"Status", "EstatusClave" },
                            {"Saldo", "SaldoRecibo" },
                            {"Cap. Ins.", "CapitalInsoluto" }
                        };

                        Table tRecibos = new Table(UnitValue.CreatePercentArray(new float[] { 1f, 2, 2, 3, 2, 2, 2, 2, 2, 3, 2, 2, 2 })).UseAllAvailableWidth();
                        tRecibos.SetMarginBottom(10);
                        foreach (var titulo in tituloEntidad.Select(t => t.Key))
                        {
                            AgregarCelda(tRecibos, NuevaCelda(titulo, arialNegrita, fSizeTabla, Alineacion.Izquierda), true);
                        }

                        if (datosEstadoCuenta.Recibos != null && datosEstadoCuenta.Recibos.Count > 0)
                        {
                            int inRec = 0;
                            foreach (var recibo in datosEstadoCuenta.Recibos)
                            {
                                foreach (var campo in tituloEntidad.Select(t => t.Value))
                                {
                                    string valorCadena = string.Empty;
                                    decimal valorDecimal = 0;
                                    DateTime valorFecha = new DateTime();

                                    try
                                    {
                                        PropertyInfo propiedad = recibo.GetType().GetProperty(campo);
                                        if (propiedad != null)
                                        {
                                            if (propiedad.PropertyType == typeof(decimal))
                                            {
                                                decimal.TryParse(propiedad.GetValue(recibo, null).ToString(), out valorDecimal);
                                                valorCadena = valorDecimal.ToString("C", cultPeru);
                                            }
                                            else if (propiedad.PropertyType == typeof(Utils.DateTimeR))
                                            {
                                                valorCadena = datosEstadoCuenta.Recibos[inRec].FechaRecibo.Value.Value.ToString("dd/MM/yyyy");
                                            }
                                            else if (propiedad.PropertyType == typeof(DateTime))
                                            {
                                                DateTime.TryParse(propiedad.GetValue(recibo, null).ToString(), out valorFecha);
                                                valorCadena = valorFecha.ToString("dd/MM/yyyy");
                                            }
                                            else
                                            {
                                                valorCadena = propiedad.GetValue(recibo, null).ToString();
                                            }
                                        }

                                        AgregarCelda(tRecibos, NuevaCelda(valorCadena, arial, fSizeTabla, Alineacion.Izquierda), false);
                                    }
                                    catch (Exception e)
                                    {
                                    }
                                }
                                inRec++;
                            }
                        }

                        documento.Add(tRecibos);
                        #endregion

                        documento.Close();
                    }

                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }

            return archivo;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private Image logoPF;
            private decimal version;
            private string Folio;
            private float fSizeNormal = 10;
            private iText.Kernel.Colors.Color colorNormal = new DeviceRgb(16, 38, 79);
            public int PaginaActual { get; set; }

            private PdfFont arial;
            private PdfFont arialNegrita;

            public Encabezado(decimal version, int Folio)
            {
                this.version = version <= 0 ? 1 : version;
                arial = AppSettings.Arial;
                arialNegrita = AppSettings.ArialNegrita;
                this.Folio = Folio <= 0 ? string.Empty : Folio.ToString();
                logoPF = AppSettings.LogPF;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 80, paginaTamanio.GetWidth() - 60);
                Paragraph p = new Paragraph("")
                        .SetFont(arial)
                        .SetFontSize(fSizeNormal)
                        .SetTextAlignment(TextAlignment.RIGHT);
                Text t1 = new Text(Folio);
                t1.SetFontColor(ColorConstants.RED);
                Text t2 = new Text(Environment.NewLine + "Estado de Cuenta");
                t2.SetFontColor(colorNormal);
                p.Add(t1).Add(t2);
                tEncabezado.AddCell(new Cell()
                    .Add(p)
                    .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                    .SetBorder(Border.NO_BORDER));
                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 50, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }

            public void FijarPaginado(Document document, string lugar, string folio)
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                int totalPaginas = pdf.GetNumberOfPages();
                Paragraph paginas;
                switch (version)
                {
                    case 1:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"{lugar} N° {folio}").SetFont(arialNegrita).SetFontSize(7).SetFontColor(ColorConstants.RED);
                            document.ShowTextAligned(paginas, paginaTamanio.GetWidth() - 90, paginaTamanio.GetTop() - 45, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                }
            }
        }

        protected class CellRadius : CellRenderer
        {
            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}
