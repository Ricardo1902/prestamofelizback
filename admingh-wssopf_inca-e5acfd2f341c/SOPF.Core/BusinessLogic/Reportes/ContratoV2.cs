﻿using iText.Kernel.Events;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using Newtonsoft.Json;
using SOPF.Core.Model.Reportes;
using SOPF.Core.Model.Solicitudes;
using System;
using System.IO;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public partial class Contrato
    {
        private float fixedLeading = 14;

        private byte[] ContratoV2()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            float punto = 0.35277f;
            firma.SetWidth(UnitValue.CreatePercentValue(62));

            try
            {
                if (datosContrato == null) datosContrato = new ContratoVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL, false);
                        Encabezado encabezado = new Encabezado(version);
                        documento.SetMargins(80, 82, 25, 80);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        #region Título
                        Paragraph parrafo = new Paragraph("CONTRATO DE PRÉSTAMO SIMPLE")
                            .SetFont(arialNN)
                            .SetFontSize(fSizeTitulo)
                            .SetTextAlignment(TextAlignment.CENTER)
                            .SetFixedLeading(fixedLeading);
                        documento.Add(parrafo);
                        #endregion

                        #region Intro
                        parrafo = new Paragraph("El presente documento contiene los términos y condiciones que rigen el Contrato de Préstamo Personal en adelante “El Contrato”; que celebran de una " +
                            "parte Préstamo Feliz en 15 Minutos S.A.C. con RUC N° 20601194181 en adelante “Préstamo Feliz”; y de otra parte el Cliente, quien participa " +
                            "conjuntamente con su cónyuge, de ser el caso, cuyos datos de identificación se detallan en la Solicitud de Préstamo en adelante “la Solicitud”, " +
                            "y que constituye parte integral de este Contrato.")
                            .SetFont(arialN)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading)
                            .SetMarginTop(20);
                        documento.Add(parrafo);
                        #endregion

                        #region Primera
                        parrafo = new Paragraph("PRIMERA.- DECLARACIONES:")
                            .SetFont(arialNN).SetFontSize(fSizeNormal).SetFixedLeading(fixedLeading).SetMarginTop(20);
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Préstamo Feliz en mérito al presente Contrato declara:")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetFixedLeading(fixedLeading).SetMarginTop(20);
                        documento.Add(parrafo);

                        #region 1.1                        
                        parrafo = new Paragraph("1.1.    Ser una Sociedad Anónima Cerrada, debidamente constituida y funcionando conforme a las leyes de la República del Perú, " +
                            "cuyo objeto social es la colocación de préstamos al público en general con recursos propios.")
                            .SetFont(arialN)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading);

                        documento.Add(parrafo);
                        #endregion

                        #region 1.2
                        parrafo = new Paragraph("1.2.    Su compromiso de otorgar préstamos de forma expeditiva, esto es, en un promedio de 15 minutos, luego de la correspondiente " +
                            "evaluación técnica y crediticia según sus criterios y/o políticas sobre evaluación de deudores de Préstamo Feliz. La suscripción de la Solicitud " +
                            "por el Cliente no garantiza necesariamente el otorgamiento del Préstamo por parte de Préstamo Feliz, quién iniciará el procedimiento de evaluación " +
                            "técnica y crediticia del solicitante, luego que éste le alcance toda la información y documentación requerida y suficiente, a criterio de " +
                            "Préstamo Feliz. En caso el solicitante no califique como deudor, Préstamo Feliz le comunicará dicha situación mediante cualquiera de los medios de " +
                            "comunicación señalados en el presente contrato.")
                            .SetFont(arialN)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading);

                        documento.Add(parrafo);
                        #endregion

                        #region 1.3
                        parrafo = new Paragraph("1.3.    Que le asiste el derecho al Cliente de retractarse de su Solicitud con anterioridad al desembolso del Préstamo Personal, " +
                            "sin que ello conlleve la aplicación de ningún cargo o penalidad alguna." + Environment.NewLine + Environment.NewLine
                            + "Por su parte, el Cliente declara lo siguiente:" + Environment.NewLine + Environment.NewLine + " ")
                            .SetFont(arialN)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);

                        documento.Add(parrafo);
                        #endregion

                        #region 1.4
                        parrafo = new Paragraph("1.4.    Que los datos declarados en virtud del presente Contrato, incluida la Solicitud, tienen el carácter de declaración jurada. " +
                            "El Cliente se obliga a comunicar en forma inmediata a Préstamo Feliz cualquier cambio en los datos consignados, asumiendo las posibles consecuencias " +
                            "de su falta de actualización.")
                            .SetFont(arialN)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading);

                        documento.Add(parrafo);
                        #endregion

                        #region 1.5
                        parrafo = new Paragraph("1.5.    Que ha solicitado a Préstamo Feliz un préstamo (en adelante, el “Préstamo”) por el monto y en las condiciones y términos " +
                            "regulados en el presente Contrato, incluida su Hoja Resumen.")
                            .SetFont(arialN)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading);

                        documento.Add(parrafo);
                        #endregion

                        #region 1.6
                        parrafo = new Paragraph("1.6.    Que cuenta con la solvencia económica y calidad moral suficiente para hacer frente a las obligaciones contenidas en el " +
                            "presente Contrato y de no encontrarse sujeto a procesos y/o procedimientos de cobro de acreencias con terceros agentes a la fecha de la " +
                            "Solicitud, que pudiera afectar en forma adversa e importante su condición financiera u operaciones.")
                            .SetFont(arialN)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading);
                        documento.Add(parrafo);
                        #endregion

                        #region 1.7                        
                        parrafo = new Paragraph("1.7.    Que Préstamo Feliz podrá proceder a la suspensión del desembolso del Préstamo si por cualquier motivo variaran de modo adverso " +
                            "las condiciones del mercado financiero, las condiciones políticas, económicas o legales, la situación financiera del Cliente o, en general, las " +
                            "circunstancias bajo las cuales Préstamo Feliz aprobó el Préstamo; sin que ello conlleve a aplicación de ningún cargo por ningún concepto ni la " +
                            "aplicación de penalidad alguna en contra de Préstamo Feliz. En tal caso, resultará aplicable lo dispuesto en la cláusula décima tercera del Contrato.")
                            .SetFont(arialN)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading);

                        documento.Add(parrafo);
                        #endregion

                        #endregion

                        #region Segunda
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        Text texto = new Text("SEGUNDA.- DEFINICIONES: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("Para efectos del presente Contrato, son aplicables los siguientes términos:");
                        documento.Add(parrafo);

                        #region 2.1 - 2.6
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading);
                        texto = new Text("2.1.    Carta de Autorización de Débito Automático: Se refiere al documento mediante el cual el Cliente autoriza los descuentos mensuales que se " +
                            "debitarán automáticamente a través de sus cuentas y/o tarjetas asociadas a ellas, que el Cliente mantenga en el sistema financiero para los descuentos de: " +
                            "(i) la cuota correspondiente al saldo deudor; (ii) la(s) cuota(s) y/o interés(es) pendiente(s) de pago del saldo deudor en caso no haya(n) podido ser retenida(s) " +
                            "o descontada(s) a favor de Préstamo Feliz en la oportunidad correspondiente. El Cliente reconoce desde ya, que el presente contrato constituye una autorización " +
                            "para el débito automático mensual, adicional a la Carta de Autorización de Débito Automático suscrita conjuntamente al presente documento." +
                            Environment.NewLine + "2.2.    Carta de Autorización de Descuento por Planilla: Se refiere al documento mediante el cual el Cliente solicita y autoriza de manera irrevocable " +
                            "a su empleador, el descuento de cualquier ingreso remunerativo o no remunerativo para el pago de (i) la cuota correspondiente al saldo deudor; (ii) " +
                            "la(s) cuota(s) y/o interés(es) pendiente(s) de pago del saldo deudor en caso no haya(n) podido ser retenida(s) o descontada(s) a favor de Préstamo Feliz " +
                            "en la oportunidad correspondiente El Cliente reconoce desde ya, que el presente contrato constituye una autorización para el descuento mensual, adicional " +
                            "a la Autorización de Descuento suscrita conjuntamente al presente documento. Esta autorización se extiende a los beneficios sociales, remuneraciones " +
                            "impagas y/o cualquier otra bonificación o beneficio al que tuviera derecho el Cliente, incluidos casos de suspensión o extinción de su relación laboral " +
                            "por cese, despido, renuncia, fallecimiento, jubilación o invalidez permanente o traslado." +
                            Environment.NewLine + "2.3.    Carta Poder: Documento mediante el cual el Cliente otorga poder irrevocable a Préstamo Feliz para gestionar información en su representación " +
                            "y efectuar la cobranza del Préstamo." +
                            Environment.NewLine + "2.4.    Contrato: Los términos y condiciones que rigen el Préstamo personal contenidos en el presente documento, incluyendo la Solicitud, " +
                            "Hoja Resumen, Cronograma de pagos y cualquier modificación debidamente pactada por las partes que pudiera surgir de los mismos." +
                            Environment.NewLine + "2.5.    Cronograma de Pagos: Documento que forma parte de la Hoja de Resumen y contiene el número de cuotas, su periodicidad y fecha de pago; " +
                            "detallando los conceptos que integran la cuota. La fecha de corte para el cómputo de los intereses compensatorios y moratorios coincidirá " +
                            "con la fecha de pago de las cuotas." +
                            Environment.NewLine + "2.6.    Solicitud: Oferta contractual que realiza el Cliente hacia Préstamo Feliz manifestando su voluntad de contratar una vez conocidos y aceptados " +
                            "los términos y condiciones del Contrato, formando parte integrante de este último." +
                            Environment.NewLine + "2.7.     Hoja Resumen: Anexo del Contrato que informa de modo sintetizado las condiciones del Contrato de Préstamo Personal, tasa de costo efectivo anual " +
                            "(TCEA), la tasa de interés compensatoria, la tasa de interés moratoria o penalidad aplicable en caso de incumplimiento de pago, las comisiones y gastos de cargo de el Cliente, " +
                            "el cronograma de pagos; entre otra información relevante conforme a la legislación vigente.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion
                        #endregion

                        #region Tercera
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("TERCERA.- OBJETO DEL CONTRATO: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("De ser aprobada la Solicitud del Cliente, Préstamo Feliz otorgará en favor de aquél un préstamo personal en los términos, monto, plazo y finalidad " +
                            "indicados en la Hoja Resumen, y en la oportunidad prevista en la cláusula siguiente. Por su parte, el Cliente se obliga a restituir a Préstamo Feliz el importe " +
                            "del préstamo personal en los términos y plazos indicados en este Contrato y su Hoja de Resumen.");
                        documento.Add(parrafo);
                        #endregion

                        #region Cuarta
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("CUARTA.- DISPOSICIÓN DEL PRÉSTAMO: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("El Cliente podrá disponer del importe del Préstamo en el plazo máximo de 72 horas luego de suscrito este Contrato, a través de orden de pago o " +
                            "transferencia electrónica en el número de cuenta y entidad financiera que el Cliente indique en la Hoja Resumen. Salvo el supuesto de transferencia electrónica, " +
                            "en el caso que el Cliente no disponga del préstamo otorgado en un plazo de diez días hábiles posteriores a la puesta a disposición de los fondos por parte de " +
                            "Préstamo Feliz, el Cliente o Préstamo Feliz podrán solicitar la cancelación del mismo sin responsabilidad y costo alguno.");
                        documento.Add(parrafo);

                        parrafo = new Paragraph("En caso de que por error, Préstamo Feliz deposite una cantidad superior al monto del Préstamo pactado, el Cliente tiene la obligación de devolver " +
                            "la cantidad excedente a Préstamo Feliz a más tardar al día hábil siguiente en el cual tales fondos son puestos a su disposición considerando las modalidades antes " +
                            "referidas, caso contrario dicho excedente será considerado como parte del Préstamo siendo en tal caso el Cliente informado con posterioridad de las eventuales variaciones " +
                            "que su decisión genere en el cronograma de pagos. Ello, sin perjuicio de la facultad de Préstamo Feliz de resolver el Contrato de pleno derecho en caso el Cliente incumpla " +
                            "con devolver la cantidad excedente en el plazo antes referido, siendo aplicable el procedimiento establecido en la cláusula décima tercera de este Contrato.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Si el desembolso del préstamo es mayor o igual a S/. 10,000 (diez mil soles y 00/100), Préstamo Feliz previa evaluación, podrá requerir la presentación " +
                            "de un aval o fiador, quien participará conjuntamente en la suscripción del presente contrato de préstamo. El aval o fiador declara conocer este compromiso, declarando conocer " +
                            "todas las consecuencias legales que pudieran ocurrir como consecuencia del incumplimiento de pago por parte del titular del préstamo.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        documento.Add(parrafo);

                        parrafo = new Paragraph("El aval o fiador se compromete a entregar a Préstamo Feliz, la documentación necesaria que garantice la ejecución del cobro ante un eventual incumplimiento " +
                            "por parte del titular. Esta garantía procederá previa evaluación de Préstamo Feliz, pudiendo efectuarse a través de cualquiera de las cuentas afiliadas al sistema financiero y/o " +
                            "bienes de propiedad del fiador o aval. El fiador o aval también presentará a Préstamo Feliz, la información correspondiente a todas sus cuentas afiliadas al sistema financiero, " +
                            "y firmará en blanco y por única vez la autorización de Débito Automático para que Préstamo Feliz pueda realizar los cobros respectivos a cualquiera de las cuentas afiliadas al " +
                            "sistema financiero del fiador o aval, en caso de incumplimiento del titular del Préstamo. En ese sentido se compromete a firmar los documentos que requiere Préstamo Feliz para su " +
                            "constitución como garante de la deuda, incluyendo la respectiva autorización de débito automático o la solicitud de descuento por planilla.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);
                        #endregion

                        #region Quinta
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("QUINTA.- EMISIÓN DE TITULO VALOR: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("Préstamo Feliz podrá requerir al cliente la emisión y suscripción de un pagaré emitido a la orden de Préstamo Feliz SAC de manera incompleta de " +
                            "conformidad a lo establecido en el artículo 10 de la Ley N° 27287- Ley de Títulos Valores, en tal caso Préstamo Feliz le proporcionará al momento de la " +
                            "suscripción del referido título valor una copia del mismo. En caso de incumplimiento de las obligaciones que asume el Cliente con Préstamo Feliz, este " +
                            "último queda facultado a completar el pagaré antes indicado, con el monto que resulte de la obligación exigible según la liquidación que realice Préstamo " +
                            "Feliz en mérito a lo pactado en el presente Contrato, la misma que comprenderá intereses compensatorios, intereses moratorios, impuestos, comisiones y " +
                            "gastos según lo establecido en la Hoja de Resumen, incluidos aquellos costos en los que haya incurrido Préstamo Feliz derivados del incumplimiento de las " +
                            "obligaciones por parte del Cliente, siendo la fecha de vencimiento, aquella en la que se practique dicha liquidación y llene el pagaré. El llenado del pagare " +
                            "se hará conforme a la hoja de instrucción del llenado del pagaré que será entregada al Cliente.");
                        documento.Add(parrafo);

                        parrafo = new Paragraph("El Cliente renuncia expresamente a su derecho de incluir en el mencionado pagaré, una cláusula que limite su transferencia, reconociendo " +
                            "que Préstamo Feliz podrá negociar libremente el referido título valor. El Cliente declara tener conocimiento de los mecanismos de protección que la ley permite " +
                            "para la emisión o aceptación de títulos valores incompletos. Asimismo, de conformidad con lo dispuesto por el artículo 1279° del Código Civil, la emisión del " +
                            "pagaré a que se refiere la presente cláusula no constituirá novación o sustitución de la obligación primitiva o causal. La prórroga del pagaré o cualquier otro " +
                            "cambio accesorio de la obligación, de ser el caso, tampoco constituirá novación de la misma. Ambas partes convienen que la entrega o emisión del Pagaré, o de " +
                            "cualquier otro título valor que constituya orden o promesa de pago, en ningún caso extinguirá la obligación primitiva ni aun cuando éstos hubiesen sido " +
                            "perjudicados por cualquier causa.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);
                        #endregion

                        #region Sexta
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("SEXTA.- INTERESES, COMISIONES, GASTOS Y PENALIDADES: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("Queda pactado que Préstamo Feliz cobrará al Cliente la tasa de interés efectivo compensatorio y, en caso de incumplimiento los intereses moratorios " +
                            "y penalidades por pago tardío, respetándose los límites establecidos en la normativa vigente; así como las comisiones, impuestos y gastos, en los porcentajes, " +
                            "montos y periodicidad indicados en la Hoja Resumen; la que a su vez informará la respectiva TCEA(Tasa de Costo Efectiva Anual) en el cronograma de pagos.");
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Queda expresamente pactado que, luego de 24 horas de no poder efectuarse el Débito Automático, se considera pago tardío. La demora en el " +
                            "pago de cualquiera de las obligaciones del Cliente determinará que las sumas adeudadas generen intereses moratorios y penalidades por pago tardío establecidas " +
                            "en la Hoja Resumen, en forma diaria adicional al interés compensatorio pactado. Por tanto, si el Cliente incumple el pago oportuno de cualquiera de sus " +
                            "obligaciones, según lo indicado en el cronograma de pagos, incurre en mora automática a partir del día siguiente de la fecha en que debió haberse realizado " +
                            "el pago según lo indicado en el cronograma de pagos, sin necesidad de requerimiento o intimación alguna por parte de Préstamo Feliz, encontrándose este último " +
                            "facultado a dar por vencido el Préstamo y a proceder con las acciones de cobranza respectivas conforme a lo indicado en el penúltimo párrafo de la cláusula " +
                            "siguiente, y sin perjuicio de aplicar el procedimiento previsto en la cláusula décima tercera del Contrato.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Queda pactado que las cuotas pagadas posterior a la fecha de vencimiento, estarán afectas al cobro de una penalidad por pago tardío, la cual " +
                            "se encuentra establecida en la Hoja Resumen. La oportunidad de cobro de dicha penalidad se realiza al momento del pago de la cuota.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);
                        #endregion

                        #region Séptima
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("SEPTIMA.- GESTIÓN DE COBRANZA: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("El Cliente presta su consentimiento sobre la posible gestión de cobranza que deba ser realizada por Préstamo Feliz directamente o a través de terceros en " +
                            "sus domicilios, en su centro de trabajo, a través de vía telefónica, medios electrónicos, mensajería instantánea o escrita de conformidad con la ley aplicable.");
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Se hace presente que las copias adicionales de documentos que a futuro requiera el Cliente, quedan sujetas al cobro de las respectivas " +
                            "comisiones por gastos administrativos precisados en la Hoja Resumen.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);
                        #endregion

                        #region Octava
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("OCTAVA.- MODALIDAD Y MEDIO DE PAGO: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("El préstamo otorgado será pagado mediante Débito Automático a través de cualquiera de las cuentas que el Cliente tenga afiliadas al sistema financiero, " +
                            "pudiendo ser utilizada como mecanismo de cobro, las tarjetas de débito o crédito asociadas a dichas cuentas. El número de cuenta de Préstamo Feliz y la modalidad " +
                            "de pago constarán en la Hoja de Resumen. En caso no sea posible realizar el cobro de las cuotas a través del Débito Automático, el Cliente es responsable de pagar " +
                            "las cuotas a favor de Préstamo Feliz a través de cualquier otra modalidad como descuento por planilla, depósito en cuenta o cheque, lo que tendrá como consecuencia " +
                            "la aplicación de la tasa máxima permitida.");
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Las cuotas del préstamo incluyen: capital, interés compensatorio, interés moratorio, comisiones, gastos, seguros y tributos, según lo informado " +
                            "en la Hoja Resumen. El pago de las cuotas se realizará en la periodicidad y fechas establecidas en el Cronograma de Pagos, el mismo que considerará como fecha de " +
                            "inicio para el cómputo del plazo del préstamo, la fecha del desembolso de éste.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("Débito Automático: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("El Cliente deberá firmar por única vez y en blanco la Autorización de Débito Automático, Solicitud de Cargo en Cuenta y/o documento correspondiente en favor " +
                            "de Préstamo Feliz de modo tal que, la modalidad de pago a través de débito automático surta efectos para todas sus cuentas afiliadas al sistema financiero, durante " +
                            "toda la vigencia del presente Contrato, garantizando el derecho de Préstamo Feliz a gestionar el cobro automáticamente al vencimiento de las cuotas del préstamo a " +
                            "través de cualquiera de ellas. El Cliente reconoce el derecho de Préstamo Feliz a gestionar en cualquier momento el cargo automático de cualquier acreencia vencida o " +
                            "por vencer.");
                        documento.Add(parrafo);

                        parrafo = new Paragraph("La Autorización de Débito Automático y solicitud de Cargo en Cuenta, se extiende a todas las cuentas y/o tarjetas afiliadas al sistema financiero " +
                            "que el Cliente pudiera tener a su nombre. En virtud de ello autoriza a Préstamo Feliz para que, ante algún eventual impedimento de cobro en la cuenta principal, proceda " +
                            "con el llenado de formatos requeridos para la gestión de cobro por débito automático de sus cuotas de pago del préstamo vencidas o por vencer, a través de cualquiera de " +
                            "sus cuentas y/o tarjetas afiliadas al sistema financiero, reconociendo por este acto que dicha autorización tiene validez durante toda la vigencia del contrato de préstamo " +
                            "hasta el pago total del mismo.")
                           .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                           .SetMarginTop(10);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("Poder Irrevocable: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("El Cliente otorga poder expreso e irrevocable a Préstamo Feliz para que, en nombre y representación del Cliente pueda solicitar, obtener y gestionar a entidades " +
                            "financieras públicas o privadas, ante cualquier organismo nacional o internacional, así como ante su empleador, cualquier información acerca del monto de su sueldo, cuentas " +
                            "bancarias, tarjetas de crédito o tarjetas de débito. El Cliente declara conocer y entender que el presente poder otorgar el acceso por parte de Préstamo Feliz, a información " +
                            "protegida por la normativa de datos personales y / o ley de secreto bancario, por lo que el Cliente autoriza expresamente el levantamiento de su secreto bancario o reserva " +
                            "bancaria, para que el destinatario de este documento pueda brindar esta información a los funcionarios de Préstamo Feliz, así como el llenado de los formatos requeridos para " +
                            "la gestión del pago de sus cuotas bajo cualquiera de las modalidades descritas en la presente cláusula, por tanto el Cliente reconoce que se desiste desde ya de efectuar " +
                            "reclamo alguno ante Préstamo Feliz, las entidades anteriormente mencionadas o cualquier organismo de administración de justicia");
                        documento.Add(parrafo);
                        #endregion

                        #region Novena
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("NOVENA.- SEGURO DE DESGRAVAMEN: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("Préstamo Feliz previa evaluación podrá requerir la contratación de un Seguro de Desgravamen para otorgar el préstamo. En ese sentido, el Cliente " +
                            "podrá contratar el Seguro de Desgravamen que comercializa Préstamo Feliz, cuyos términos mínimos legales serán informados en la Hoja de Resumen. En tal " +
                            "caso, Préstamo Feliz le proporcionará las condiciones generales y particulares que se establecen en la póliza de seguro, la misma que le será proporcionada " +
                            "adicionalmente a la Hoja de Resumen para su aceptación expresa al momento de la suscripción del presente Contrato. Con la contratación del referido seguro, " +
                            "el Cliente puede optar por su financiamiento, en tal caso el monto de la prima de seguro será incorporado al valor de la cuota mensual de pago, de acuerdo " +
                            "con lo informado en el cronograma de pagos.");
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Se hace la salvedad que, en caso el Cliente decida contratar con otra compañía de seguros un Seguro de Desgravamen de iguales " +
                            "características y requisitos a los exigidos por Préstamo Feliz, no estará obligado a tomar el seguro ofrecido por ésta, pudiendo sustituirlo por " +
                            "el que hubiera contratado. Las condiciones mínimas que serán requeridas en la póliza de seguro respectiva por parte de Préstamo Feliz se informan " +
                            "en la página web www.prestamofeliz.pe. De proceder el Cliente en este sentido, la póliza de seguro deberá ser endosada a favor de Préstamo Feliz " +
                            "hasta por el monto adeudado del Préstamo. En virtud del endoso, Préstamo Feliz podrá pactar con el Cliente que el pago de la prima del seguro se " +
                            "adicione al pago de las cuotas periódicas previamente pactadas por el Préstamo. El endoso y forma de pago por acuerdo de las partes, podrá ser " +
                            "tramitado por el Cliente ante la empresa del sistema de seguros que emite la póliza de Seguro de Desgravamen y deberá ser entregado a Préstamo Feliz.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);

                        parrafo = new Paragraph("En caso el Cliente no contrate el referido seguro bajo alguna de las modalidades antes indicadas, Préstamo Feliz se encontrará facultado " +
                            "a contratar de forma directa dicho seguro. En tal caso, los gastos por concepto de seguro que Préstamo Feliz pueda contratar directamente serán trasladados " +
                            "al Cliente. En este caso particular, Préstamo Feliz entregará la póliza en un plazo que no excederá de 10 días contados a partir de su recepción.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);
                        #endregion

                        #region Décima
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("DÉCIMA.- CESIÓN: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("Préstamo Feliz podrá ceder sus derechos o su posición contractual en este Contrato a cualquier tercero, prestando el Cliente, en este acto, su " +
                            "consentimiento anticipado a la referida cesión, la misma que le será oportunamente informada. Por su parte, el Cliente no podrá transmitir sus derechos y " +
                            "obligaciones al amparo del presente Contrato sin la autorización previa y por escrito de Préstamo Feliz.");
                        documento.Add(parrafo);
                        #endregion

                        #region Décima primera
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("DÉCIMA PRIMERA.- ORDEN DE IMPUTACIÓN DE PAGOS: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("Cualquier pago que efectúe el Cliente se aplicará en el siguiente orden: deuda vencida, impuestos, gastos, comisiones, intereses moratorios, " +
                            "intereses compensatorios del periodo y capital.");
                        documento.Add(parrafo);
                        #endregion

                        #region Décima segunda
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("DÉCIMA SEGUNDA.- PAGOS ANTICIPADOS O ADELANTO DE CUOTAS: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("El Cliente podrá realizar pagos por encima de la cuota exigible del periodo, ya sea a través de pagos anticipados o adelanto de cuotas, en " +
                            "forma total o parcial, sin cobro alguno, condiciones, limitaciones, gastos, comisiones o penalidades de ningún tipo. El Pago Anticipado conlleva a " +
                            "la aplicación del monto capital al Préstamo, con la consiguiente reducción de los intereses, comisiones y gastos al día del pago. Los pagos mayores " +
                            "a dos cuotas (incluyendo la exigible en el periodo) se consideran pagos anticipados. Por su parte, el Adelanto de Cuotas supone la aplicación del monto " +
                            "pagado a las cuotas inmediatamente posteriores a la exigible en el periodo, sin que se produzca una reducción de los intereses, comisiones y gastos al " +
                            "día del pago. Los pagos menores o iguales a dos cuotas (que incluyen aquella exigible en el período) se consideran adelanto de cuotas.");
                        documento.Add(parrafo);
                        #endregion

                        #region Décima tercera
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("DÉCIMA TERCERA.- ACELERACIÓN DE PLAZOS Y RESOLUCIÓN DEL CONTRATO: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("En mérito a lo dispuesto en el artículo 1430° del Código Civil, ambas partes acuerdan que el Contrato podrá ser resuelto de " +
                            "pleno derecho por Préstamo Feliz desde la fecha en la que ésta le comunique al Cliente, previamente y sin necesidad de declaración " +
                            "judicial, que quiere valerse de alguna de las siguientes causales: si el Cliente: (i) no paga en la forma, plazo y oportunidad " +
                            "convenidas sus obligaciones de crédito en mérito al presente Contrato y/o en el cronograma de pagos respectivo; (ii) no cumple " +
                            "cualquier otra obligación frente a Préstamo Feliz que, a criterio de éste último, le genere un perjuicio y/o dificulte la ejecución " +
                            "del Contrato; (iii) cuando, a criterio de Préstamo Feliz, el Cliente: a) no satisfaga los requerimientos de información efectuados " +
                            "por Préstamo Feliz, como parte de sus políticas y acciones vinculadas al conocimiento del Cliente y/o a la prevención del lavado de " +
                            "activos y financiamiento del terrorismo, incluyendo, de manera enunciativa y no limitativa, los casos en los que dicha información no " +
                            "es entregada dentro de los plazos fijados por Préstamo Feliz y/o es entregada de manera incompleta y/o habiendo sido entregada " +
                            "oportunamente, a criterio de Préstamo Feliz, no justifica las operaciones del Cliente; b) si se solicita la declaración del inicio de " +
                            "concurso del Cliente o se presentase cualquier otra situación que afecte su capacidad de pago; c) si Préstamo Feliz comprueba que " +
                            "cualquier información, documentación o dato proporcionado por el Cliente para sustentar el Crédito Personal que haya solicitado ante " +
                            "Préstamo Feliz fueran falsas, o, tratándose de documentos, éstos hubieran sido adulterados o alterados, d) si habiendo suscrito el " +
                            "Compromiso de no revocar la instrucción a su depositario, el Cliente incumple dicho compromiso, e) si mantener vigente el Crédito implica " +
                            "el incumplimiento de alguna disposición legal, en especial aquellas referidas a políticas crediticias o de lavado de activos y financiamiento " +
                            "del terrorismo; o f) por cese o fallecimiento del Cliente.");
                        documento.Add(parrafo);

                        parrafo = new Paragraph("En cualquiera de los supuestos previamente indicados, Préstamo Feliz podrá resolver este Contrato señalando en la " +
                            "comunicación correspondiente, el motivo de la misma. Dicho aviso se cursará con una anticipación no menor a cinco (5) días hábiles a la " +
                            "fecha de resolución efectiva.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);

                        parrafo = new Paragraph("En cualquier caso de resolución o terminación del Contrato, el Cliente, bajo su responsabilidad, se obliga a cancelar " +
                            "dentro de las veinticuatro (24) horas siguientes o dentro del plazo adicional que le otorgue Préstamo Feliz expresamente y por escrito, el " +
                            "íntegro del saldo deudor pendiente de pago según la liquidación que realice Préstamo Feliz.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);

                        parrafo = new Paragraph("El Cliente podrá a su vez poner término a este Contrato cuando así lo decida, dando aviso por escrito a Préstamo Feliz, " +
                            "de acuerdo a las indicaciones en éste establecidas, sin perjuicio de su obligación de pagar previamente el saldo deudor total del préstamo " +
                            "que liquide Préstamo Feliz. Préstamo Feliz podrá mantener vigente el préstamo hasta la cancelación de dicho saldo deudor.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);
                        #endregion

                        #region Décima cuarta
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("DÉCIMA CUARTA.- MODIFICACIONES AL CONTRATO: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("Ambas partes acuerdan la facultad de Préstamo Feliz de modificar las condiciones contractuales cuando a su criterio se presenten alguno de " +
                            "los siguientes supuestos: (i) cambios en las condiciones de la economía nacional o internacional; (ii) cambios en el funcionamiento o tendencias de " +
                            "los mercados o la competencia; (iii) cambios en las políticas de gobierno o de Estado que afecten las condiciones del mercado; (iv) impacto de alguna " +
                            "disposición legal sobre costos, características, definición o condiciones de los productos y servicios de crédito en general; (v) inflación o deflación; " +
                            "devaluación o revaluación de la moneda; (vi) campañas promocionales; (vii) evaluación crediticia del Cliente o de su empleador, de ser el caso; (viii) " +
                            "encarecimiento de los servicios prestados por terceros cuyos costos son trasladados al Cliente o de los costos de prestación de los productos y servicios " +
                            "de crédito ofrecidos por Préstamo Feliz; (ix) crisis financiera; o (x) hechos ajenos a la voluntad de las partes; conmoción social; desastres naturales; " +
                            "terrorismo; guerra; caso fortuito o fuerza mayor; (xi) en caso el Cliente no satisfaga los requerimientos de información efectuados por Préstamo Feliz, " +
                            "como parte de sus políticas y acciones vinculadas al conocimiento del Cliente y/o a la prevención del lavado de activos y financiamiento del terrorismo, " +
                            "incluyendo, de manera enunciativa y no limitativa, los casos en los que dicha información no es entregada dentro de los plazos fijados por Préstamo Feliz " +
                            "y/o es entregada de manera incompleta y/o habiendo sido entregada oportunamente, a criterio de Préstamo Feliz, no justifica las operaciones del Cliente.");
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Cualquier comunicación y modificación al Contrato, será efectuada indistintamente, a través de: comunicaciones al domicilio señalado en " +
                            "la Solicitud, correo electrónico, llamadas telefónicas, envío de mensajes de texto, ó a través de aplicaciones de mensajería instantánea, y demás medios de " +
                            "comunicación directos que Préstamo Feliz pudiese implementar. Tales modificaciones serán informadas con una anticipación no menor a cuarenta y cinco (45) días " +
                            "calendario a la fecha de entrada en vigencia de las mismas. De no estar de acuerdo el Cliente con las modificaciones introducidas, podrá optar por resolver el " +
                            "contrato dentro de dicho plazo, sin cargo o penalidad alguna cursando la respectiva comunicación escrita a Préstamo Feliz. La resolución del Contrato por el " +
                            "Cliente conllevará a su vez al pago de todo saldo deudor u obligación que mantuviera pendiente frente a Préstamo Feliz. En el caso de modificaciones más " +
                            "beneficiosas al Cliente, Préstamo Feliz podrá utilizar medios masivos de información, tales como: publicaciones en oficinas, página web y/o en otro; sin " +
                            "necesidad de comunicación previa al Cliente.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);
                        #endregion

                        #region Décima quinta
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("DÉCIMA QUINTA.- AMPLIACIÓN DEL CRÉDITO").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add(Environment.NewLine + "El Cliente, podrá solicitar a Préstamo Feliz la ampliación del crédito, la cual estará sujeta a evaluación y aprobación " +
                            "previa de Préstamo Feliz conforme sus políticas de crédito. Las partes acuerdan que las ampliaciones se regirán por los mismos términos, condiciones, " +
                            "pagaré, garantías y/o fianzas que forman parte integrante del presente contrato. En caso Préstamo feliz apruebe la ampliación, se compromete a firmar " +
                            "la documentación correspondiente exigida por Préstamo Feliz para efectos del desembolso, y emisión de la Hoja Resumen y Cronograma de Pagos actualizados.");
                        documento.Add(parrafo);
                        #endregion

                        #region Decimo sexta
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("DÉCIMO SEXTA.- DATOS PERSONALES: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("Se informa que los datos personales proporcionados a Préstamo Feliz quedan incorporados al banco de datos de Clientes de Préstamo Feliz, " +
                            "quien utilizará dicha información para efectos de la gestión de los servicios de préstamo solicitados y/o contratados (incluyendo evaluaciones " +
                            "financieras, procesamiento de datos, formalizaciones contractuales, cobro de deudas y remisión de correspondencia, entre otros), la misma que podrá " +
                            "ser realizada a través de terceros. Asimismo, el titular de los datos personales autoriza a Préstamo Feliz a utilizar sus datos personales, incluyendo " +
                            "datos sensibles, que hubieran sido proporcionados directamente a Préstamo Feliz, aquellos que pudieran encontrarse en fuentes accesibles para el público " +
                            "o los que hayan sido obtenidos de terceros; para tratamientos que supongan desarrollo de acciones comerciales, incluyendo evaluaciones crediticias, " +
                            "la remisión (vía medio físico, electrónico o telefónico) de publicidad, información u ofertas (personalizadas o generales) de productos y/o servicios " +
                            "de Préstamo Feliz");
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Se informa al titular de los datos personales, que puede revocar la autorización para el tratamiento de sus datos personales en " +
                            "cualquier momento, de conformidad con lo previsto en las normas que prevén la protección de datos personales (Ley 29733). Para ejercer este derecho, " +
                            "o cualquier otro previsto en dichas normas, el titular de datos personales podrá presentar su solicitud por escrito a Préstamo Feliz en la siguiente " +
                            "dirección: Calle Manuel Segura N° 166 – Lince. Esta autorización se mantendrá vigente luego de la terminación o resolución del Contrato. Sin perjuicio " +
                            "de ello, el Cliente declara conocer su derecho a revocar en cualquier momento su consentimiento y autorización, así como realizar las coordinaciones " +
                            "pertinentes, en caso haya decidido no recibir algún otro tipo de información.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);
                        #endregion

                        #region Décima séptima
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("DÉCIMA SÉPTIMA.- OBLIGACIONES ADICIONALES: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("El Cliente a su vez se obliga a: (i) notificar a Préstamo Feliz respecto de cualquier cambio en su domicilio en un plazo que no exceda de 10 días " +
                            "hábiles posteriores a que dicho cambio haya ocurrido; (ii) notificar a Préstamo Feliz respecto de cualquier disminución en sus ingresos o cualquier " +
                            "situación que pueda afectar su capacidad de pago, en un plazo que no exceda de 10 días hábiles posteriores a que dicha situación haya ocurrido, (iii) " +
                            "en general, cualquier cambio de trabajo, cuenta de haberes o cualquier situación que pueda influir en la cobranza normal del préstamo.");
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Tanto el titular como el fiador o aval se obligan a informar cualquier cambio y/o actualización de datos que pudiera afectar " +
                            "el cobro de las cuotas oportunamente, incluyendo los relacionados a su lugar de trabajo, y a las cuentas bancarias, o tarjetas de débito y/o crédito, " +
                            "desde las cuales se efectuará los descuentos de las cuotas. En caso de incumplimiento de este compromiso, y que Préstamo Feliz tenga que gestionar " +
                            "la información de conformidad con lo indicado en la cláusula Octava, se aplicará la penalidad establecida en la Hoja Resumen parte de este contrato.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);
                        #endregion

                        #region Décima octava
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("DÉCIMA OCTAVA.- TRIBUTOS: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("Todos los impuestos, contribuciones y derechos que deban cubrirse con motivo de la celebración del presente Contrato, serán cubiertos por la " +
                            "parte que resulte obligada a ello, de conformidad con la legislación aplicable. En este sentido, aquellos tributos que correspondan ser cubiertos por " +
                            "el Cliente serán informados en la Hoja de Resumen, la misma que hará referencia al tipo de tributo al que se sujeta, el porcentaje y monto aplicable en " +
                            "situación de cumplimiento. En tal caso, la emisión y el otorgamiento del comprobante de pago respectivo estarán a cargo de Préstamo Feliz lo que " +
                            "procederá en la oportunidad que se indica en la referida Hoja de Resumen.");
                        documento.Add(parrafo);
                        #endregion

                        #region Décima novena
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("DÉCIMA NOVENA.- LEGISLACIÓN Y COMPETENCIA: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("Este Contrato se rige en forma supletoria por las disposiciones del Código Civil Peruano. Para el caso de cualquier discrepancia derivada de la " +
                            "ejecución y/o interpretación de este Contrato, las partes se someten a la competencia de los jueces y tribunales de la ciudad de Lima, señalando como " +
                            "domicilio a los indicados en la Solicitud, adonde se harán llegar todas las citaciones y notificaciones judiciales o extrajudiciales a que hubiere lugar.");
                        documento.Add(parrafo);
                        #endregion

                        #region Vigécima
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("VIGÉSIMA.- VIGENCIA: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("Sin perjuicio de lo establecido en la cláusula Décima tercera, el presente Contrato entrará en vigor a partir de su fecha de firma, y se " +
                            "dará por terminado en la fecha en que el Cliente haya liquidado en su totalidad, las obligaciones crediticias derivadas del presente Contrato.");
                        documento.Add(parrafo);
                        #endregion

                        #region Vigécima primera
                        parrafo = new Paragraph().SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading).SetMarginTop(10);
                        texto = new Text("VIGÉSIMA PRIMERA.- SISTEMAS DIGITALES: ").SetFont(arialNN);
                        parrafo.Add(texto);
                        parrafo.Add("El Cliente podrá acceder y utilizar los sistemas y aplicaciones digitales que Préstamo Feliz ponga a su disposición. El Cliente manifiesta " +
                            "expresamente conocer que, los siguientes mecanismos de autenticación: ingreso de claves o contraseñas, clic o cliquear en dispositivos, aceptación " +
                            "por voz, datos biométricos (huella dactilar, identificación facial, etc.), códigos, autogeneración de códigos, entre otros mecanismos que Préstamo " +
                            "Feliz ponga a su disposición, sustituyen su firma autógrafa para la contratación o ampliación de las operaciones y servicios. Las partes reconocen " +
                            "que, el uso de los medios de autenticación, produce los mismos efectos que las leyes otorgan a la firma autógrafa, entre éstos la identificación de " +
                            "las partes y el acuerdo libre y expreso de voluntades.");
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Leídos en su integridad los términos y condiciones del presente Contrato, por las partes que en éste intervienen, se procede a la " +
                            "suscripción del Contrato, incluido sus anexos, y a la entrega al Cliente de un ejemplar del mismo.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);
                        documento.Add(parrafo);
                        #endregion

                        #region Lugar y fecha
                        Table tContenido = new Table(UnitValue.CreatePercentArray(new float[] { 0.2f, .7f, 0.1f, 0.2f, 1f, 2 })).UseAllAvailableWidth()
                            .SetMarginTop(10)
                            .SetPaddings(0, 0, 0, 0)
                            .SetFont(arialN)
                            .SetFontSize(fSizeNormal)
                            .SetVerticalAlignment(VerticalAlignment.BOTTOM);

                        tContenido.AddCell(new Cell().Add(new Paragraph("Lugar:").SetFont(arialNN))
                           .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().Add(new Paragraph(datosContrato.Lugar))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new DashedBorder(.5f)));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));

                        tContenido.AddCell(new Cell().Add(new Paragraph("Fecha:").SetFont(arialNN))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().Add(new Paragraph(datosContrato.Fecha.ToString("dd/MM/yyyy")))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new DottedBorder(.5f)));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        documento.Add(tContenido);
                        #endregion

                        #region Firmas
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { 1, 0.3f, 1 })).UseAllAvailableWidth()
                            .SetMarginTop(50)
                            .SetFont(arialN)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.CENTER);

                        tContenido.AddCell(new Cell().Add(new Paragraph(datosContrato.NombreCliente).SetFont(arialN).SetFontSize(9).SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));

                        datosDocumento.Add(datosContrato.NombreCliente);

                        tContenido.AddCell(new Cell().Add(new Paragraph("Firma del Titular").SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new SolidBorder(0.5f)));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().Add(new Paragraph("Firma de Cónyuge").SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new SolidBorder(0.5f)));

                        Table subTabla = new Table(UnitValue.CreatePercentArray(new float[] { .7f, 1, .5f })).UseAllAvailableWidth();
                        subTabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        subTabla.AddCell(new Cell().Add(firma).SetBorder(Border.NO_BORDER));
                        subTabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell()
                            .Add(subTabla)
                            .SetBorder(Border.NO_BORDER));

                        tContenido.AddCell(new Cell(0, 2).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell().Add(new Paragraph("Firma del Préstamo Feliz"))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new SolidBorder(0.5f)));
                        tContenido.AddCell(new Cell(0, 2).SetBorder(Border.NO_BORDER));

                        documento.Add(tContenido);

                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();

                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 130 * punto,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 180)) * punto
                            //PosX = 130,//Para test
                            //PosY = (Utils.AltoAreaRestante(documento) + 180)//Para test
                        });
                        //Para test
                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        #endregion

                        encabezado.FijarPaginado(documento);
                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(datosContrato)}", JsonConvert.SerializeObject(ex));
            }
            return archivo;
        }

        protected partial class Encabezado
        {
            private void EncabezadoV2()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF.SetWidth(UnitValue.CreatePercentValue(70));
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(80, paginaTamanio.GetTop() - 70, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }
        }
    }
}
