﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class Pagare
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private PagareVM datosPagare;
        private float fSizeNormal = 8;
        public List<PosicionFirmaVM> Firmas { get; set; }
        private List<string> datosDocumento = new List<string>();
        private iText.Kernel.Colors.Color colorNormal = new DeviceRgb(16, 38, 79);
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public Pagare(int version)
        {
            this.version = version <= 0 ? 1 : version;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
            Firmas = new List<PosicionFirmaVM>();
        }

        public byte[] Generar(PagareVM pagareDatos)
        {
            byte[] pagareByte = null;
            datosPagare = pagareDatos;
            switch (version)
            {
                case 1:
                    pagareByte = PagareV1();
                    break;
            }
            return pagareByte;
        }

        private byte[] PagareV1()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            firma.SetWidth(UnitValue.CreatePercentValue(50));
            float punto = 0.35277f;

            try
            {
                if (datosPagare == null) datosPagare = new PagareVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL);
                        Encabezado encabezado = new Encabezado(version, datosPagare.Folio);
                        documento.SetMargins(25, 35, 25, 35);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        #region ImporteVencimiento
                        Table tContenido = new Table(UnitValue.CreatePercentArray(6)).UseAllAvailableWidth();
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .6f, 10, .6f, 10, .6f, 10 })).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("IMPORTE", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Derecha, PdfCelda.Color.PERUBLUE), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, false, false, false), false);
                        AgregarCelda(tContenido, NuevaCelda("VENCIMIENTO", arial, fSizeNormal, false, false, false, false, 0, 0, Alineacion.Derecha, PdfCelda.Color.PERUBLUE), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, true, false, false), false);
                        tContenido.SetMarginTop(40);
                        documento.Add(tContenido);
                        datosDocumento.Add("Pagare");
                        #endregion

                        #region Pagaré
                        Paragraph parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add(Environment.NewLine + Environment.NewLine + "Pagaré/Pagaremos solidariamente a Préstamo Feliz S.A.C (en adelante, “Préstamo Feliz”), a su orden o a quién esté " +
                            "endosado el presente título, en sus oficinas de esta ciudad o donde se presente este título para su cobro, la suma de _____________________________________________ , " +
                            "importe que expresamente declaro/declaramos adeudar a Préstamo Feliz y por el cual me/nos obligo/obligamos a abonar intereses compensatorios a una tasa " +
                            "del _____________ % efectivo ________________________, y durante el período de mora intereses compensatorios e intereses moratorios a las tasas de " +
                            "_______________________ % efectivo ________________________ y ___________________ % efectivo ___________________, respectivamente, así como penalidades, " +
                            "seguros, gastos notariales, de cobranza judicial y extrajudicial, y en general los gastos y comisiones que pudiéramos adeudar derivados del crédito representado " +
                            "en este Pagaré, y que se pudieran generar desde la fecha de emisión del presente Pagaré hasta la cancelación total de la presente obligación, sin que sea necesario " +
                            "requerimiento alguno de pago para constituirme/constituirnos en mora, pues es entendido que ésta se producirá de modo automático por el solo hecho del vencimiento " +
                            "de este Pagaré. Expresamente acepto/aceptamos toda variación de las condiciones antes indicadas, dentro de los límites legales autorizados, las mismas que se aplicarán " +
                            "luego de la comunicación efectuada por Préstamo Feliz con una anticipación no menor a cuarenta y cinco (45) días calendario a la fecha de entrada en vigencia de las mismas. " +
                            "Asimismo, si a su vencimiento este Pagaré no fuese pagado, autorizo/autorizamos irrevocablemente a Préstamo Feliz para que cobre el importe total adeudado por concepto " +
                            "de este Pagaré, incluidos intereses y demás conceptos derivados del mismo a través de cualquiera de las acciones de cobro permitidas por la legislación vigente. " + Environment.NewLine +
                            "Acepto/Aceptamos y doy/damos por válidas desde ahora todas las renovaciones y prórrogas totales o parciales que se anoten en este documento, aún cuando no estén " +
                            "suscritas por mi/nosotros.Este título no está sujeto a protesto por falta de pago, conforme a lo dispuesto en el artículo 81.1 de la Ley de Títulos Valores, " +
                            "Ley 27287 y sus normas complementarias y/o modificatorias; salvo, lo dispuesto en el artículo 81.2 de la referida ley. Queda expresamente establecido que mi/nuestro " +
                            "domicilio es el indicado en el presente título. Préstamo Feliz podrá entablar acción judicial donde lo tuviera por conveniente, conforme a ley. Declaro/Declaramos estar " +
                            "plenamente facultado/facultados para suscribir y emitir el presente Pagaré, asumiendo en caso contrario la responsabilidad civil y/o penal a que hubiera lugar. " +
                            "Dejo(Dejamos) constancia en la calidad de emitente(s) en el presente documento, que la información proporcionada por mi(nosotros) es completa y verdadera.");
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.RIGHT);
                        Text texto = new Text(Environment.NewLine + Environment.NewLine + Environment.NewLine +
                                         @"___________ , _____________ de _______________");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Cliente
                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();
                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 32,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 45)) * punto
                        });
                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);

                        tContenido = new Table(UnitValue.CreatePercentArray(6)).UseAllAvailableWidth();
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .6f, 10, .6f, 10, .6f, 10 })).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("EL CLIENTE", arialNegrita, fSizeNormal, false, false, false, false, 0, 6, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("Firma(s)", arial, fSizeNormal, false, true, false, false, 0, 6, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("(para persona jurídica: representante(s) legal(es) del cliente / para persona natural: cliente y cónyuge)", arial, fSizeNormal, false, false, false, false, 0, 6, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("Razón Social / Nombre Cliente", arial, fSizeNormal, false, true, false, false, 0, 6, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("DNI", arial, fSizeNormal, false, true, false, false, 0, 2, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("Estado Civil", arial, fSizeNormal, false, true, false, false, 0, 4, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("Domicilio", arial, fSizeNormal, false, true, false, false, 0, 6, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("Nombre representante legal / cónyuge", arial, fSizeNormal, false, true, false, false, 0, 4, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("DNI", arial, fSizeNormal, false, true, false, false, 0, 2, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("Nombre representante legal", arial, fSizeNormal, false, true, false, false, 0, 4, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("DNI", arial, fSizeNormal, false, true, false, false, 0, 2, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        documento.Add(tContenido);
                        #endregion

                        #region Fianza
                        parrafo = new Paragraph().SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text(Environment.NewLine + Environment.NewLine + @"FIANZA").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(Environment.NewLine + Environment.NewLine +
                            "Me/Nos constituyo/constituimos en fiador/fiadores solidario/solidarios del/de los emitente/emitentes de este Pagaré, por las obligaciones contraídas en " +
                            "este documento obligándome/obligándonos al pago de la cantidad adeudada, intereses compensatorios y moratorios, así como comisiones, penalidades, " +
                            "seguros, gastos notariales, de cobranza judicial y extrajudicial, que se pudieran devengar desde la fecha de emisión hasta la cancelación total de la presente " +
                            "obligación. Esta fianza es solidaria, incondicional, irrevocable y por plazo indefinido y estará vigente mientras no se encuentren totalmente pagadas las " +
                            "obligaciones principales garantizadas. Acepto/Aceptamos desde ahora, las prórrogas y/o renovaciones que se concedan a mi/nuestro fiado aún cuando no " +
                            "estén suscritas por él y sin que sea necesario nos sean comunicadas. Acepto/Aceptamos desde ahora que este título no está sujeto a protesto por falta de " +
                            "pago, conforme a lo dispuesto en el artículo 81.1 de la Ley de Títulos Valores, Ley 27287 y sus normas complementarias y/o modificatorias; salvo lo " +
                            "dispuesto en el articulo 81.2 de la referida Ley. Queda expresamente establecido que mi/nuestro domicilio es el indicado en el presente título. Préstamo " +
                            "Feliz podrá entablar acción judicial donde lo tuviere por conveniente conforme a ley. Declaro/Declaramos estar plenamente facultado/facultados para " +
                            "afianzar el presente Pagaré, asumiendo en caso contrario la responsabilidad civil y/o penal que hubiere lugar. Dejo (Dejamos) constancia en la calidad de " +
                            "emitente(s) en el presente documento, que la información proporcionada por mi (nosotros) es completa y verdadera."
                            + Environment.NewLine + Environment.NewLine);
                        documento.Add(parrafo);
                        #endregion


                        #region Fiador
                        tContenido = new Table(UnitValue.CreatePercentArray(6)).UseAllAvailableWidth();
                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { .6f, 10, .6f, 10, .6f, 10 })).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("FIADOR", arialNegrita, fSizeNormal, false, false, false, false, 0, 6, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("Firma(s)", arial, fSizeNormal, false, true, false, false, 0, 6, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("para persona jurídica: representante(s) legal(es) del fiador / para persona natural: fiador y cónyuge)", arial, fSizeNormal, false, false, false, false, 0, 6, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("Razón Social / Nombre Fiador", arial, fSizeNormal, false, true, false, false, 0, 6, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("DNI", arial, fSizeNormal, false, true, false, false, 0, 2, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("Estado Civil", arial, fSizeNormal, false, true, false, false, 0, 4, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("Domicilio", arial, fSizeNormal, false, true, false, false, 0, 6, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("Nombre representante legal / cónyuge", arial, fSizeNormal, false, true, false, false, 0, 4, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("DNI", arial, fSizeNormal, false, true, false, false, 0, 2, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("Nombre representante legal", arial, fSizeNormal, false, true, false, false, 0, 4, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tContenido, NuevaCelda("DNI", arial, fSizeNormal, false, true, false, false, 0, 2, Alineacion.Izquierda, PdfCelda.Color.PERUBLUE).SetBorder(Border.NO_BORDER), false);
                        documento.Add(tContenido);
                        #endregion

                        firma.SetFixedPosition(65, 230);
                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private Image logoPF;
            private decimal version;
            private string Folio;
            private float fSizeNormal = 10;
            private iText.Kernel.Colors.Color colorNormal = new DeviceRgb(16, 38, 79);
            public int PaginaActual { get; set; }

            private PdfFont arial;
            private PdfFont arialNegrita;

            public Encabezado(decimal version, int Folio)
            {
                this.version = version <= 0 ? 1 : version;
                arial = AppSettings.Arial;
                arialNegrita = AppSettings.ArialNegrita;
                this.Folio = Folio <= 0 ? string.Empty : Folio.ToString();
                logoPF = AppSettings.LogPF;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 80, paginaTamanio.GetWidth() - 60);
                Paragraph p = new Paragraph("")
                        .SetFont(arial)
                        .SetFontSize(fSizeNormal)
                        .SetTextAlignment(TextAlignment.RIGHT);
                Text t1 = new Text(Folio);
                t1.SetFontColor(ColorConstants.RED);
                Text t2 = new Text(Environment.NewLine + "Pagaré");
                t2.SetFontColor(colorNormal);
                p.Add(t1).Add(t2);
                tEncabezado.AddCell(new Cell()
                    .Add(p)
                    .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                    .SetBorder(Border.NO_BORDER));
                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 50, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }

            public void FijarPaginado(Document document, string lugar, string folio)
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                int totalPaginas = pdf.GetNumberOfPages();
                Paragraph paginas;
                switch (version)
                {
                    case 1:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"{lugar} N° {folio}").SetFont(arialNegrita).SetFontSize(7).SetFontColor(ColorConstants.RED);
                            document.ShowTextAligned(paginas, paginaTamanio.GetWidth() - 90, paginaTamanio.GetTop() - 45, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                }
            }
        }

        protected class CellRadius : CellRenderer
        {
            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}
