﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public partial class ResumenCredito
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private PdfFont arialN;
        private PdfFont arialNN;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private Image logoPF;
        private float tab = 10;
        private float fSizeNormal = 9;
        private float fSizeTitulo = 10;
        private iText.Kernel.Colors.Color colorNormal = new DeviceRgb(16, 38, 79);        
        private CronogramaPagos datosCredito;
        private List<string> datosDocumento = new List<string>();
        public List<PosicionFirmaVM> Firmas { get; set; }
        public bool esCampaniaPF;
        public bool esAmpliacion;
        public decimal MontoDesembolso;
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }        

        public ResumenCredito(int version)
        {
            this.version = version <= 0 ? 1 : version;
            logoPF = AppSettings.LogPF;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
            arialN = AppSettings.ArialNarrow;
            arialNN = AppSettings.ArialNarrowNegrita;
            Firmas = new List<PosicionFirmaVM>();
            switch (version)
            {
                case 2:
                    fSizeNormal = 11;
                    fSizeTitulo = 12;
                    break;
            }
        }

        public byte[] Generar(CronogramaPagos datosCredito)
        {
            byte[] archivo = null;
            this.datosCredito = datosCredito;            

            switch (version)
            {
                case 1:
                    archivo = ResumenCreditoV1();
                    break;
                case 2:
                    archivo = ResumenCreditoV2();
                    break;
            }
            return archivo;
        }

        private byte[] ResumenCreditoV1()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            firma.SetWidth(UnitValue.CreatePercentValue(50));
            float punto = 0.35277f;
            string valorTexto = string.Empty;

            try
            {
                if (datosCredito == null) datosCredito = new CronogramaPagos();

                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Encabezado encabezado = new Encabezado(version, datosCredito.IdSolicitud);
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL);
                        documento.SetMargins(50, 30, 30, 30);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        #region Titulo
                        Paragraph parrafo = new Paragraph($"HOJA RESUMEN{Environment.NewLine}")
                            .SetFontColor(colorNormal)
                            .SetFont(arialNegrita)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.CENTER);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFontColor(colorNormal)
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal);
                        Text texto = new Text("Este documento es parte integrante del Contrato de Préstamo Simple (en adelante, el Contrato), cuyos términos y" +
                            " condiciones son aceptadas por el cliente y Préstamo Feliz en 15 minutos (en adelante, Préstamo Feliz), y so las siguientes:");
                        parrafo.Add(texto);

                        documento.Add(parrafo);
                        #endregion

                        #region CondicionesTab
                        valorTexto = datosCredito.MontoDesembolsar.ToString("C", cultPeru);
                        Table tabla = new Table(UnitValue.CreatePercentArray(new float[] { 1.5f, 1, 2 })).UseAllAvailableWidth()
                            .SetFontColor(colorNormal);
                        AgregarCelda(tabla, NuevaCelda("1. Moneda y Capital desembolsado", arialNegrita, fSizeNormal).SetFontColor(colorNormal), false);
                        AgregarCelda(tabla, NuevaCelda(valorTexto, arial, fSizeNormal, 0, 2)
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE), false);
                        datosDocumento.Add(valorTexto);

                        valorTexto = datosCredito.Cuota.ToString("C", cultPeru);
                        AgregarCelda(tabla, NuevaCelda("2. Valor cuota", arialNegrita, fSizeNormal).SetFontColor(colorNormal), false);
                        AgregarCelda(tabla, NuevaCelda(valorTexto, arial, fSizeNormal, 0, 2)
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE), false);
                        datosDocumento.Add(valorTexto);

                        valorTexto = $"{datosCredito.TasaAnual:00.00} %";
                        AgregarCelda(tabla, NuevaCelda("3. Tasa de Interés Efectiva Anual compensatoria (TEA 360 días)", arialNegrita, fSizeNormal).SetFontColor(colorNormal), false);
                        AgregarCelda(tabla, NuevaCelda(valorTexto, arial, fSizeNormal, 0, 2)
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE), false);
                        datosDocumento.Add(valorTexto);

                        valorTexto = $"{datosCredito.TasaInteresMoratoria:00.00} %";
                        AgregarCelda(tabla, NuevaCelda("4. Tasa de Interés Moratoria (TEA 360 días) o Penalidad por incumplimiento", arialNegrita, fSizeNormal).SetFontColor(colorNormal), false);
                        AgregarCelda(tabla, NuevaCelda(valorTexto, arial, fSizeNormal, 0, 2)
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE), false);
                        datosDocumento.Add(valorTexto);

                        valorTexto = $"{datosCredito.TasaCostoAnual:00.00} %";
                        AgregarCelda(tabla, NuevaCelda("5. Tasa de Costo Efectiva Anual (TCEA)", arialNegrita, fSizeNormal).SetFontColor(colorNormal), false);
                        AgregarCelda(tabla, NuevaCelda(valorTexto, arial, fSizeNormal, 0, 2)
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE), false);
                        datosDocumento.Add(valorTexto);

                        valorTexto = datosCredito.TotalInteres.ToString("C", cultPeru);
                        AgregarCelda(tabla, NuevaCelda("6. Monto Total de los Intereses compensatorios", arialNegrita, fSizeNormal).SetFontColor(colorNormal), false);
                        AgregarCelda(tabla, NuevaCelda(valorTexto, arial, fSizeNormal, 0, 2)
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE), false);
                        datosDocumento.Add(valorTexto);

                        AgregarCelda(tabla, NuevaCelda("7. Cuenta Bancaria para el Desembolso", arialNegrita, fSizeNormal).SetFontColor(colorNormal), false);
                        AgregarCelda(tabla, NuevaCelda(datosCredito.NombreBancoDesembolso, arial, fSizeNormal, 0, 2), false);
                        datosDocumento.Add(datosCredito.NombreBancoDesembolso);

                        AgregarCelda(tabla, NuevaCelda("8. Cuenta Bancaria para la realización de pagos", arialNegrita, fSizeNormal).SetFontColor(colorNormal), false);
                        AgregarCelda(tabla, NuevaCelda(datosCredito.CuentaBancariaDesembolso, arial, fSizeNormal, 0, 2)
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE), false);
                        datosDocumento.Add(datosCredito.CuentaBancariaDesembolso);

                        AgregarCelda(tabla, NuevaCelda("9. Comisiones aplicables", arialNegrita, fSizeNormal, 7, 0).SetFontColor(colorNormal).SetVerticalAlignment(VerticalAlignment.MIDDLE), false);
                        AgregarCelda(tabla, NuevaCelda("Uso de canales", arialNegrita, fSizeNormal).SetFontColor(colorNormal).SetVerticalAlignment(VerticalAlignment.MIDDLE), false);
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Operaciones ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        texto = new Text($"(pagos, consultas, entre otros) en agencia u oficina de Préstamo Feliz {Environment.NewLine}Costo: ");
                        parrafo.Add(texto);
                        texto = new Text("S/.10.00 ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        AgregarCelda(tabla, new Cell().Add(parrafo), false);

                        AgregarCelda(tabla, NuevaCelda("Modificación de condiciones contractuales", arialNegrita, fSizeNormal, 3, 0).SetFontColor(colorNormal).SetVerticalAlignment(VerticalAlignment.MIDDLE), false);
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Cambio de fecha de pago ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        texto = new Text($"a solicitud del cliente.{Environment.NewLine}Costo: ");
                        parrafo.Add(texto);
                        texto = new Text("SIN COSTO").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        AgregarCelda(tabla, new Cell().Add(parrafo), false);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Modificaciones al contrato ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        texto = new Text($"solicitadas por el cliente distintas de la fecha de pago.{Environment.NewLine}Costo: ");
                        parrafo.Add(texto);
                        texto = new Text("SIN COSTO").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        AgregarCelda(tabla, new Cell().Add(parrafo), false);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Reprogramación: ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        texto = new Text($"Modificación de la programación de las cuotas a solicitud del cliente.{Environment.NewLine}Costo: ");
                        parrafo.Add(texto);
                        texto = new Text("SIN COSTO").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        AgregarCelda(tabla, new Cell().Add(parrafo), false);

                        AgregarCelda(tabla, NuevaCelda("Servicios Asociados al Préstamo Simple", arialNegrita, fSizeNormal, 2, 0).SetFontColor(colorNormal).SetVerticalAlignment(VerticalAlignment.MIDDLE), false);
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Gestión del proceso de Descuento por Planilla: ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        texto = new Text($"Procedimiento interno de verificación y aceptación de pago vía descuento por planilla conforme a las instrucciones del Cliente.{Environment.NewLine}Costo: ");
                        parrafo.Add(texto);
                        texto = new Text("S/.15.00").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        AgregarCelda(tabla, new Cell().Add(parrafo), false);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Gestión del proceso de débito automático: ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        texto = new Text($"Procedimiento interno de verificación y aceptación de pago a través de empresas del sistema financiero conforme a las instrucciones del Cliente.{Environment.NewLine}Costo: ");
                        parrafo.Add(texto);
                        texto = new Text("S/.10.00").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        AgregarCelda(tabla, new Cell().Add(parrafo), false);

                        AgregarCelda(tabla, NuevaCelda("Evaluación de Póliza de Seguro Endosa", arialNegrita, fSizeNormal).SetFontColor(colorNormal).SetVerticalAlignment(VerticalAlignment.MIDDLE), false);
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text($"Evaluación interna a cargo de Préstamo Feliz").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        texto = new Text($"{Environment.NewLine}Costo: ");
                        parrafo.Add(texto);
                        texto = new Text(" ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        AgregarCelda(tabla, new Cell().Add(parrafo), false);

                        documento.Add(tabla);

                        #endregion

                        #region Condiciones2
                        tabla = new Table(UnitValue.CreatePercentArray(new float[] { .5f, 10 })).UseAllAvailableWidth();
                        AgregarCelda(tabla, NuevaCelda("10.", arial, fSizeNormal).SetFontColor(colorNormal).SetBorder(Border.NO_BORDER), false);
                        parrafo = new Paragraph($"El cliente tiene derecho a efectuar el ").SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add(new Text("pago adelantado ").SetFont(arialNegrita));
                        parrafo.Add($"de sus cuotas, de conformidad con los términos establecidos en la cláusula 11 del Contrato.{Environment.NewLine}");
                        tabla.AddCell(new Cell().Add(parrafo).SetBorder(Border.NO_BORDER));

                        AgregarCelda(tabla, NuevaCelda("11.", arial, fSizeNormal).SetFontColor(colorNormal).SetBorder(Border.NO_BORDER), false);
                        parrafo = new Paragraph("El cliente tiene derecho a efectuar ").SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("pagos anticipados ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add("de las cuotas o saldos, ya sea en forma total o parcial, con la siguiente subsiguiente reducción de los intereses al día del pago, deduciendo así mismo " +
                            "las comisiones y gastos derivados de las cláusulas del Contrato, sin que le sea aplicable ninguna comisión, gasto, penalidad o cobros de naturaleza o efecto " +
                            $"similar, de conformidad con los términos indicados en la cláusula 11 del Contrato.{Environment.NewLine}");
                        tabla.AddCell(new Cell().Add(parrafo).SetBorder(Border.NO_BORDER));

                        AgregarCelda(tabla, NuevaCelda("12.", arial, fSizeNormal).SetFontColor(colorNormal).SetBorder(Border.NO_BORDER), false);
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text($"Modalidad de pago elegida por el Cliente:{Environment.NewLine}").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add($"(    ) Descuento en la planilla de pagos del empleador del Cliente.{Environment.NewLine}" +
                            $"( x ) Débito automático en empresas del sistema financiero autorizadas.{Environment.NewLine}" +
                            $"(    ) Efectivo, cheque u otra modalidad a través de la agencias u oficinas de terceros autorizados.{Environment.NewLine}");
                        tabla.AddCell(new Cell().Add(parrafo).SetBorder(Border.NO_BORDER));

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        Table subTabla = new Table(UnitValue.CreatePercentArray(2)).UseAllAvailableWidth().SetFontColor(colorNormal);
                        AgregarCelda(subTabla, NuevaCelda("Cuenta bancaria de titularidad de Préstamo Feliz:", arial, fSizeNormal), false);
                        AgregarCelda(subTabla, NuevaCelda(datosCredito.NoTarjetaVisa, arial, fSizeNormal), false);
                        tabla.AddCell(new Cell().Add(subTabla).SetBorder(Border.NO_BORDER));
                        datosDocumento.Add(datosCredito.NoTarjetaVisa);

                        AgregarCelda(tabla, NuevaCelda("13.", arial, fSizeNormal).SetFontColor(colorNormal).SetBorder(Border.NO_BORDER), false);
                        AgregarCelda(tabla, NuevaCelda("Seguro de Desgravamen:", arialNegrita, fSizeNormal).SetFontColor(colorNormal).SetBorder(Border.NO_BORDER), false);

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        subTabla = new Table(UnitValue.CreatePercentArray(2)).UseAllAvailableWidth().SetFontColor(colorNormal);
                        AgregarCelda(subTabla, NuevaCelda("Monto de la prima de Seguro", arial, fSizeNormal), false);
                        AgregarCelda(subTabla, NuevaCelda("Costo:\t\tSoles", arial, fSizeNormal), false);
                        AgregarCelda(subTabla, NuevaCelda("Nombre de la compañía aseguradora", arial, fSizeNormal), false);
                        AgregarCelda(subTabla, NuevaCelda("PROTECTA S.A. Compañía de Seguros.", arial, fSizeNormal), false);
                        AgregarCelda(subTabla, NuevaCelda("Número de Póliza Grupal", arial, fSizeNormal), false);
                        AgregarCelda(subTabla, NuevaCelda("Póliza de Seguro N° 0000000450", arial, fSizeNormal), false);
                        AgregarCelda(subTabla, NuevaCelda("Número de Certificado de Seguro", arial, fSizeNormal), false);
                        AgregarCelda(subTabla, NuevaCelda("NO APLICA", arial, fSizeNormal), false);
                        tabla.AddCell(new Cell().Add(subTabla).SetBorder(Border.NO_BORDER));

                        AgregarCelda(tabla, NuevaCelda("14.", arial, fSizeNormal).SetFontColor(colorNormal).SetBorder(Border.NO_BORDER), false);
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text($"Tributos:{Environment.NewLine}").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(new Text("Corresponde al cliente asumir el costo del Impuesto General a las Ventas (IGV). El IGV es del 18 % y se aplica respecto " +
                            "de los intereses compensatorios y aquellas comisiones asociadas al servicio de financiamiento. Se encuentran excluidas de su cálculo el capital, " +
                            $"los intereses moratorios o penalidad incumplimiento.{Environment.NewLine}{Environment.NewLine}" +
                            $"La emisión y otorgamiento del comprobante de pago respectivo es de cargo de Préstamo Feliz y procederá en sus oficinas y/o agencias, estando " +
                            $"disponibles a partir del vencimiento de las fechas de pago de las cuotas previstas en el cronograma de pago."));
                        tabla.AddCell(new Cell().Add(parrafo).SetBorder(Border.NO_BORDER));

                        documento.Add(tabla);
                        #endregion

                        #region Acuerdo
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add($"{Environment.NewLine}El Cliente declara que está de acuerdo, ha sido informado y ha recibido una explicación detallada de los " +
                            $"términos y condiciones del Préstamo Simple proporcionada por Préstamo Feliz, de conformidad con las disposiciones del Contrato y " +
                            $"de esta Hoja de Resumen, la cual el Cliente declara que entiende y acepta.{Environment.NewLine}");
                        documento.Add(parrafo);

                        tabla = new Table(UnitValue.CreatePercentArray(new float[] { 1, 0.3f, 1 })).UseAllAvailableWidth()
                            .SetMarginTop(50)
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetFontColor(colorNormal);

                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();
                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 35f,
                            PosY = altoPagina - (Utils.AltoAreaRestante(documento) + 25)
                        });

                        tabla.AddCell(new Cell().Add(new Paragraph("")
                            .SetTextAlignment(TextAlignment.CENTER)
                            .SetFont(arialNegrita))
                                .SetBorder(Border.NO_BORDER));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));

                        tabla.AddCell(new Cell().Add(new Paragraph("Firma del Cliente").SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new DashedBorder(colorNormal, .7f)));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));

                        tabla.AddCell(new Cell(0, 2).SetBorder(Border.NO_BORDER));

                        subTabla = new Table(UnitValue.CreatePercentArray(new float[] { .7f, 1, .5f })).UseAllAvailableWidth();
                        subTabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        subTabla.AddCell(new Cell().Add(firma).SetBorder(Border.NO_BORDER));
                        subTabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tabla.AddCell(new Cell()
                            .Add(subTabla)
                            .SetBorder(Border.NO_BORDER));

                        tabla.AddCell(new Cell().Add(new Paragraph("Firma del Cónyuge").SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new DashedBorder(colorNormal, .7f)));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));

                        tabla.AddCell(new Cell().Add(new Paragraph("Préstamo Feliz en 15 minutos S.A.C.").SetTextAlignment(TextAlignment.CENTER))
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new DashedBorder(colorNormal, .7f)));

                        tabla.AddCell(new Cell(0, 3).Add(new Paragraph($"{Environment.NewLine}RUC N° 20601194181").SetTextAlignment(TextAlignment.RIGHT))
                            .SetBorder(Border.NO_BORDER));

                        documento.Add(tabla);

                        //Firmas.Add(new PosicionFirmaVM
                        //{
                        //    Firmante = Firmante.Cliente,
                        //    Pagina = encabezado.PaginaActual,
                        //    PosX = 35f,
                        //    PosY = altoPagina - (Firmas[0].PosY + 50),
                        //    Alto = 30,
                        //    Ancho = 40
                        //});

                        #endregion
                        Firmas.ForEach((f) =>
                        {
                            f.PosY *= punto;
                        });

                        encabezado.FijarPaginado(documento);
                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        protected partial class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private Image logoPF;
            private decimal version;
            private PdfFont arial;
            private PdfFont arialN;
            private PdfFont arialNN;
            private string idSolicitud;
            private float tNomal = 9;
            private iText.Kernel.Colors.Color colorNormal = new DeviceRgb(16, 38, 79);
            public int PaginaActual { get; set; }

            public Encabezado(decimal version, int idSolicitud)
            {
                this.version = version <= 0 ? 1 : version;
                this.idSolicitud = idSolicitud <= 0 ? string.Empty : idSolicitud.ToString();
                arial = AppSettings.Arial;
                arialN = AppSettings.ArialNarrow;
                arialNN = AppSettings.ArialNarrowNegrita;
                if (version == 2) tNomal = 11;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                    case 2:
                        EncabezadoV2();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF = AppSettings.LogPF;
                logoPF.SetWidth(UnitValue.CreatePercentValue(62));

                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.AddCell(new Cell()
                    .Add(new Paragraph(idSolicitud)
                        .SetFont(arial)
                        .SetFontSize(tNomal)
                        .SetFontColor(ColorConstants.RED)
                        .SetTextAlignment(TextAlignment.RIGHT))
                    .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                    .SetBorder(Border.NO_BORDER));

                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 50, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }

            public void FijarPaginado(Document document)
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                int totalPaginas = pdf.GetNumberOfPages();
                Paragraph paginas;
                switch (version)
                {
                    case 1:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"Página {i} de {totalPaginas}").SetFont(arial).SetFontSize(tNomal).SetFontColor(colorNormal);
                            document.ShowTextAligned(paginas, paginaTamanio.GetWidth() - 90, paginaTamanio.GetTop() - 45, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                            document.ShowTextAligned(paginas, 30, paginaTamanio.GetBottom() + 30, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                    case 2:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"Página {i} de {totalPaginas}").SetFont(arialN).SetFontSize(tNomal).SetFontColor(colorNormal);                            
                            document.ShowTextAligned(paginas, 60, paginaTamanio.GetBottom() + 50, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                }
            }
        }
    }
}
