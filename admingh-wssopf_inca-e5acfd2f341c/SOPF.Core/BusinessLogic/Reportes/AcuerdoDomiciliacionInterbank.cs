﻿using iText.Kernel.Colors;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class AcuerdoDomiciliacionInterbank
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private float tNomal = 10;
        private AcuerdoDomiciliacionClienteVM datosCredito;
        private List<string> datosDocumento = new List<string>();
        public List<PosicionFirmaVM> Firmas { get; set; }
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public AcuerdoDomiciliacionInterbank(int version)
        {
            this.version = version <= 0 ? 1 : version;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
            Firmas = new List<PosicionFirmaVM>();
        }

        public byte[] Generar(AcuerdoDomiciliacionClienteVM datosCredito)
        {
            byte[] ampliacion = null;
            this.datosCredito = datosCredito;

            switch (version)
            {
                case 1:
                    ampliacion = AcuerdoDomiciliacionInterbankV1();
                    break;
            }
            return ampliacion;
        }

        private byte[] AcuerdoDomiciliacionInterbankV1()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            firma.SetWidth(UnitValue.CreatePercentValue(30));
            float punto = 0.35277f;
            string valorTexto = string.Empty;
            try
            {
                if (datosCredito == null) datosCredito = new AcuerdoDomiciliacionClienteVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL);
                        documento.SetMargins(50, 80, 50, 80);

                        #region Titulo
                        Paragraph parrafo = new Paragraph($"{datosCredito.IdSolicitud}")
                            .SetFontColor(ColorConstants.RED)
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.RIGHT);
                        documento.Add(parrafo);

                        parrafo = new Paragraph($"SOLICITUD DE CARGO AUTOMATICO DE RECIBOS EN CUENTA")
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.CENTER)
                            .SetMarginTop(40);
                        documento.Add(parrafo);

                        parrafo = new Paragraph($"Fecha: ")
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.RIGHT)
                            .SetMarginTop(30);
                        parrafo.Add(new Text($" {DateTime.Now.Day:00} / {DateTime.Now.Month:00} / {DateTime.Now.Year} ")
                            .SetUnderline());
                        documento.Add(parrafo);

                        datosDocumento.Add(datosCredito.IdSolicitud.ToString("C", cultPeru));
                        datosDocumento.Add($" {DateTime.Now.Day:00} / {DateTime.Now.Month:00} / {DateTime.Now.Year} ");
                        #endregion

                        #region Contenido
                        parrafo = new Paragraph($"Estimados señores: " + Environment.NewLine)
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetMarginTop(30);

                        parrafo.Add($"{Environment.NewLine}Mucho agradeceré(mos), se sirvan afiliar mi (nuestra)cuenta de ahorros/crédito al sistema de Cargo en Cuenta Automático, " +
                            "para cuyos fines autorizo(amos) al Banco Interbank a debitar de mi (nuestra) cuenta los montos correspondientes al pago de los " +
                            $"recibos afiliados a este servicio.");
                        documento.Add(parrafo);
                        #endregion

                        #region Nombre Razon Social
                        parrafo = new Paragraph("NOMBRE O RAZON SOCIAL DEL TITULAR DE LA CUENTA")
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetMarginTop(10f);
                        documento.Add(parrafo);

                        Table tabla = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth()
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph($"{datosCredito.NombreCliente} {datosCredito.ApPaternoCliente} {datosCredito.ApMaternoCliente}")));
                        documento.Add(tabla);

                        datosDocumento.Add($"{datosCredito.NombreCliente} {datosCredito.ApPaternoCliente} {datosCredito.ApMaternoCliente}");
                        #endregion

                        #region Direcion
                        parrafo = new Paragraph("DIRECCIÓN ( Calle / Número / Distrito / Provincia)")
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetMarginTop(10f);
                        documento.Add(parrafo);

                        tabla = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth()
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED);
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph($"{datosCredito.DireccionCliente}")));
                        documento.Add(tabla);

                        datosDocumento.Add($"{datosCredito.DireccionCliente}");
                        #endregion

                        #region Documentos                        
                        tabla = new Table(UnitValue.CreatePercentArray(new float[] { 1, 0.7f })).UseAllAvailableWidth()
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetMarginTop(10f);
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("DOCUMENTO DE IDENTIDAD")));
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("TELEFONOS")));

                        tabla.AddCell(new Cell()
                            .Add(new Paragraph($"{datosCredito.DNICliente}")));
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph($"{datosCredito.CelularCliente}")));

                        documento.Add(tabla);

                        datosDocumento.Add($"{datosCredito.DNICliente}");
                        datosDocumento.Add($"{datosCredito.CelularCliente}");

                        tabla = new Table(UnitValue.CreatePercentArray(new float[] { 1.7f, .1f, 2.1f, .1f, 1f, .1f, 1f })).UseAllAvailableWidth()
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetMarginTop(15f);
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph("DÉBITO / CRÉDITO")));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph("N° DE CUENTA/TARJETA")));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph("N° DE CUOTAS")));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph("IMPORTE")));

                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new SolidBorder(0.5f))
                            .Add(new Paragraph("")));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new SolidBorder(0.5f))
                            .Add(new Paragraph($"{datosCredito.CuentaDesembolso}")));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new SolidBorder(0.5f))
                            .Add(new Paragraph($"{datosCredito.CantidadRecibos}")));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new SolidBorder(0.5f))
                            .Add(new Paragraph(datosCredito.ImporteMaximoCuota.ToString("C", cultPeru))));

                        documento.Add(tabla);

                        datosDocumento.Add($"{datosCredito.CuentaDesembolso}");
                        datosDocumento.Add($"{datosCredito.CantidadRecibos}");
                        datosDocumento.Add(datosCredito.ImporteMaximoCuota.ToString("C", cultPeru));
                        #endregion

                        #region Entendimiento
                        parrafo = new Paragraph("Queda entendido que el abono a Préstamo Feliz estará condicionado a la disponibilidad de fondos en la cuenta indicada, " +
                            "por lo que declaro dejar un saldo total en mi cuenta desde el momento en el que viene abonada mi pensión y/o remuneración hasta que Préstamo " +
                            "Feliz pueda hacer efectivo el cobro mensual de la cuota del préstamo personal correspondiente al periodo de cobro.")
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetMarginTop(8f);
                        documento.Add(parrafo);

                        tabla = new Table(UnitValue.CreatePercentArray(new float[] { 1, 0.5f, 1 })).UseAllAvailableWidth()
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.CENTER)
                            .SetMarginTop(25f);
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("Firma del Cliente")));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetHeight(15f));
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("Préstamo Feliz en 15 minutos SAC")));

                        tabla.AddCell(new Cell(0, 3).SetBorder(Border.NO_BORDER));

                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new SolidBorder(0.5f)));
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderBottom(new SolidBorder(0.5f))
                            .Add(new Paragraph().Add(firma)));
                        documento.Add(tabla);

                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();
                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = 1,
                            PosX = 46.5f,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 100)) * punto
                        });

                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, 1, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        #endregion

                        #region Leyenda
                        tabla = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth()
                            .SetFont(arial)
                            .SetFontSize(tNomal)
                            .SetTextAlignment(TextAlignment.CENTER)
                            .SetMarginTop(130f);
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("CLIENTE")));
                        documento.Add(tabla);
                        #endregion                        

                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }

            }
            catch (Exception ex)
            {
            }
            return archivo;
        }
    }
}
