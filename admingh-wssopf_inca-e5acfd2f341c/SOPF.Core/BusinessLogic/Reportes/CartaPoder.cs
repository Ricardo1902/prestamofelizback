﻿using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using Newtonsoft.Json;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class CartaPoder
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialN;
        private CartaPoderVM datosCartaPoder;
        private float fSizeNormal = 7;
        private float fSizeTitulo = 11;
        private float fixedLeading = 14;
        private const float punto = 0.35277f;
        private List<string> datosDocumento = new List<string>();
        public List<PosicionFirmaVM> Firmas { get; set; }
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public CartaPoder(int version)
        {
            this.version = version <= 0 ? 1 : version;
            Firmas = new List<PosicionFirmaVM>();
            CultureInfo culture = new CultureInfo("es-PE");
            Thread.CurrentThread.CurrentCulture = culture;
            arial = AppSettings.Arial;
            arialN = AppSettings.ArialNegrita;
        }

        public byte[] Generar(CartaPoderVM cartaPoder)
        {
            byte[] archivo = null;
            datosCartaPoder = cartaPoder;

            switch (version)
            {
                case 1:
                    archivo = CartaPoderV1();
                    break;
            }
            return archivo;
        }

        public byte[] CartaPoderV1()
        {
            byte[] archivo = null;
            fSizeTitulo = 11;
            fSizeNormal = 11;
            try
            {
                if (datosCartaPoder == null) datosCartaPoder = new CartaPoderVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL, false);
                        documento.SetMargins(80, 82, 25, 80);
                        Encabezado encabezado = new Encabezado(version);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        #region Titulo
                        Paragraph parrafo = new Paragraph("CARTA PODER")
                            .SetFont(arialN)
                            .SetFontSize(fSizeTitulo)
                            .SetTextAlignment(TextAlignment.CENTER)
                            .SetFixedLeading(fixedLeading);
                        documento.Add(parrafo);
                        #endregion

                        #region P1
                        parrafo = new Paragraph("Yo ")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading)
                            .SetMarginTop(20);

                        Text texto = new Text($" {datosCartaPoder.NombreCliente} ").SetUnderline().SetFont(arialN);
                        parrafo.Add(texto);
                        parrafo.Add(" identificado con número de DNI ");
                        parrafo.Add(new Text($" {datosCartaPoder.DNI} ").SetUnderline().SetFont(arialN));
                        parrafo.Add(" con domicilio en ");
                        parrafo.Add(new Text($" {datosCartaPoder.Direccion} ").SetUnderline().SetFont(arialN));
                        parrafo.Add(new Text($" AUTORIZO ").SetFont(arialN));
                        parrafo.Add("a Préstamo Feliz en 15 minutos S.A.C. con RUC N° 20601194181 para que efectúe los cargos en cuenta automático a mi cuenta N°");
                        parrafo.Add(new Text($" {datosCartaPoder.NoCuentaDom} ").SetUnderline().SetFont(arialN));
                        parrafo.Add(" correspondientes al pago de mis cuotas del contrato de préstamo de fecha ");
                        parrafo.Add(new Text($" {datosCartaPoder.FechaSolicitud:dd/MM/yyyy} ").SetUnderline().SetFont(arialN));
                        parrafo.Add(", con la finalidad de que puedan ser debitados automáticamente durante la vigencia del contrato hasta el cumplimiento total del pago.");
                        documento.Add(parrafo);

                        datosDocumento.Add(datosCartaPoder.NombreCliente);
                        datosDocumento.Add(datosCartaPoder.DNI);
                        datosDocumento.Add(datosCartaPoder.Direccion);
                        datosDocumento.Add(datosCartaPoder.NoCuentaDom);
                        datosDocumento.Add("{datosCartaPoder.FechaSolicitud:dd/MM/yyyy}");
                        #endregion

                        #region P2
                        parrafo = new Paragraph("Reconozco que, en virtud de dicho contrato de préstamo, me encuentro obligado a informar " +
                            "inmediatamente cualquier cambio de número de cuenta, tarjeta, institución financiera y/o cualquier otra modificación que pueda impedir el " +
                            "normal descuento automático de las cuotas del préstamo.")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);

                        documento.Add(parrafo);
                        #endregion

                        #region P3
                        parrafo = new Paragraph("En caso de presentarse algún impedimento en la cobranza de alguna cuota por débito automático ")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);

                        parrafo.Add(new Text("OTORGO PODER IRREVOCABLE ").SetFont(arialN));
                        parrafo.Add("a Préstamo Feliz en 15 minutos S.A.C. para que, en mi nombre pueda solicitar, obtener y gestionar a entidades financieras públicas " +
                            "o privadas, ante cualquier organismo nacional o internacional, así como ante mi empleador, cualquier información acerca del monto de mi sueldo, " +
                            "cuentas bancarias, tarjetas de crédito o tarjetas de débito. DECLARO conocer y entender que el presente poder otorga el acceso por parte de " +
                            "Préstamo Feliz, a información protegida por datos personales o secreto bancario, por lo tanto AUTORIZO EXPRESAMENTE el levantamiento de mi secreto " +
                            "bancario o reserva bancaria, para que el destinatario de este documento pueda brindar esta información a los funcionarios de la empresa " +
                            "Préstamo Feliz, así como el llenado de los formatos de autorización de débito automático de cualquiera de mis cuentas o tarjetas del sistema " +
                            "financiero, en caso deba actualizar la información, para que pueda gestionar el cobro de cualquier acreencia vencida o por vencer a través de la " +
                            "modalidad de cargo automático de mis cuentas y/o tarjetas afiliadas a cualquier entidad del sistema financiero, empleadores y cualquier otra entidad " +
                            "que corresponda para efectuar el cargo automático referido, hasta el cumplimiento del pago total de mi préstamo.");
                        documento.Add(parrafo);
                        #endregion

                        #region P4
                        parrafo = new Paragraph("En caso de suspensión o extinción de mi relación laboral (cese, despido, renuncia, fallecimiento, jubilación o " +
                            "invalidez permanente) o traslado de institución, ")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);

                        parrafo.Add(new Text("AUTORIZO ").SetFont(arialN));
                        parrafo.Add("para que el monto pendiente de pago sea descontado de mis beneficios sociales, remuneraciones impagas y/o cualquier otra bonificación o " +
                            "beneficio al que tuviera derecho, o en su defecto se traslade la presente autorización a mi nuevo empleador para que realice el descuento " +
                            "que corresponda.");
                        documento.Add(parrafo);
                        #endregion

                        #region Fecha
                        parrafo = new Paragraph("Lima,")
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading)
                            .SetMarginTop(10);

                        parrafo.Add(new Text($" {DateTime.Now:dd} de {Utils.MesFecha(DateTime.Now)} de {DateTime.Now:yyyy} ")
                            .SetUnderline().SetFont(arialN));

                        documento.Add(parrafo);
                        #endregion

                        #region Firmas
                        Table tabla = new Table(UnitValue.CreatePercentArray(new float[] { 1.3f, 6.95f, 0.25f, 1.5f })).UseAllAvailableWidth()
                            .SetFont(arial).SetFontSize(fSizeNormal).SetMarginTop(10);

                        #region Firma y huella
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetHeight(80).SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph("Firma:")));

                        tabla.AddCell(new Cell().Add(new Paragraph("")));

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("")));

                        tabla.AddCell(new Cell().Add(new Paragraph("")));

                        tabla.AddCell(new Cell(0, 3).SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("")));

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetPadding(0)
                            .Add(new Paragraph("Huella Digital")
                                .SetTextAlignment(TextAlignment.CENTER)));
                        #endregion

                        #region DNI
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("DNI:")));

                        tabla.AddCell(new Cell()
                                .Add(new Paragraph($"{datosCartaPoder.DNI}")
                                    .SetFont(arialN)));

                        tabla.AddCell(new Cell(0, 2).SetBorder(Border.NO_BORDER).Add(new Paragraph("")));
                        tabla.AddCell(new Cell(0, 4).SetBorder(Border.NO_BORDER).SetHeight(10).Add(new Paragraph("")));
                        #endregion

                        #region Domicilio
                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph("Domicilio:")));

                        tabla.AddCell(new Cell(0, 3)
                                .Add(new Paragraph($"{datosCartaPoder.Direccion}")
                                    .SetFont(arialN)));
                        #endregion

                        #region PosicionFirma
                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();

                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 145 * punto,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 5)) * punto
                            //PosX = 145,//test
                            //PosY = (Utils.AltoAreaRestante(documento) + 5)//Para test
                        });

                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        #endregion

                        documento.Add(tabla);
                        #endregion

                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(datosCartaPoder)}", JsonConvert.SerializeObject(ex));
            }
            return archivo;
        }

        protected partial class Encabezado : IEventHandler
        {
            public int PaginaActual { get; set; }
            private PdfDocumentEvent docEvento;
            private int version;
            private PdfFont arial;
            private PdfFont arialN;
            private float tNomal = 11;

            public Encabezado(int version)
            {
                this.version = version <= 0 ? 1 : version;
                arial = AppSettings.Arial;
                arialN = AppSettings.ArialNegrita;

                switch (version)
                {
                    case 1:
                        tNomal = 11;
                        break;
                }
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();

                PaginaActual++;
            }
        }
    }
}
