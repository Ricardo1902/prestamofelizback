﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using Newtonsoft.Json;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Model.Reportes;
using System;
using System.IO;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public partial class ResumenCredito
    {
        private float fixedLeading = 12;

        private byte[] ResumenCreditoV2()
        {
            byte[] archivo = null;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            firma.SetWidth(UnitValue.CreatePercentValue(62));
            float punto = 0.35277f;
            string valorTexto = string.Empty;

            try
            {
                if (datosCredito == null) datosCredito = new CronogramaPagos();

                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Encabezado encabezado = new Encabezado(version, datosCredito.IdSolicitud);
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL);
                        documento.SetMargins(75, 72, 65, 60);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        int numeracion = 0;
                        Table tabla = new Table(UnitValue.CreatePercentArray(new float[] { 0.9f, 0.5f, 1.2f })).UseAllAvailableWidth()
                            .SetFont(arialN)
                            .SetFontColor(colorNormal)
                            .SetFontSize(fSizeNormal)
                            .SetMarginTop(10)
                            .SetMarginBottom(10);

                        #region Titulo
                        Paragraph parrafo = new Paragraph($"HOJA RESUMEN{Environment.NewLine}")
                            .SetFont(arialNN)
                            .SetFontSize(fSizeNormal)
                            .SetFontColor(colorNormal)
                            .SetTextAlignment(TextAlignment.CENTER);
                        documento.Add(parrafo);

                        parrafo = new Paragraph("Este documento es parte integrante del Contrato de Préstamo Simple (en adelante, el Contrato), cuyos términos y condiciones " +
                            "son aceptadas por el cliente y Préstamo Feliz en 15 minutos (en adelante, Préstamo Feliz), y son las siguientes:")
                            .SetFont(arialN)
                            .SetFontSize(fSizeNormal)
                            .SetFontColor(colorNormal)
                            .SetTextAlignment(TextAlignment.JUSTIFIED)
                            .SetFixedLeading(fixedLeading);
                        documento.Add(parrafo);
                        #endregion

                        #region "Importe del credito"
                        numeracion++;
                        valorTexto = datosCredito.MontoDesembolsar.ToString("C", cultPeru);
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph()
                                .SetFixedLeading(fixedLeading)
                                .Add(new Text($"{numeracion.ToString()}. Importe del crédito").SetFont(arialNN))
                            ));
                        tabla.AddCell(new Cell(0, 2)
                            .Add(new Paragraph(valorTexto)
                                .SetFixedLeading(fixedLeading)
                            ));

                        datosDocumento.Add(valorTexto);
                        #endregion

                        #region "Importe desembolsado"
                        if (this.esAmpliacion)
                        {                            
                            numeracion++;
                            valorTexto = this.MontoDesembolso.ToString("C", cultPeru);
                            tabla.AddCell(new Cell()
                                .Add(new Paragraph()
                                    .SetFixedLeading(fixedLeading)
                                    .Add(new Text($"{numeracion.ToString()}. Importe desembolsado").SetFont(arialNN))
                                ));
                            tabla.AddCell(new Cell(0, 2)
                                .Add(new Paragraph(valorTexto)
                                    .SetFixedLeading(fixedLeading)
                                ));

                            datosDocumento.Add(valorTexto);                            
                        }
                        #endregion

                        #region "Valor cuota"
                        numeracion++;
                        valorTexto = datosCredito.Cuota.ToString("C", cultPeru);
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph()
                                .SetFixedLeading(fixedLeading)
                                .Add(new Text($"{numeracion.ToString()}.Valor cuota").SetFont(arialNN))
                            ));
                        tabla.AddCell(new Cell(0, 2)
                            .Add(new Paragraph(valorTexto)
                                .SetFixedLeading(fixedLeading)
                            ));

                        datosDocumento.Add(valorTexto);
                        #endregion

                        #region "TEA"
                        numeracion++;
                        valorTexto = $"{datosCredito.TasaAnual:00.00} %";
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph()
                                .SetFixedLeading(fixedLeading)
                                .Add(new Text($"{numeracion.ToString()}. Tasa de Interés Efectiva Anual compensatoria (TEA 360 días)").SetFont(arialNN))
                            ));
                        tabla.AddCell(new Cell(0, 2)
                            .Add(new Paragraph(valorTexto)
                                .SetFixedLeading(fixedLeading)
                            ));

                        datosDocumento.Add(valorTexto);
                        #endregion

                        #region "TEA moratoria"
                        numeracion++;
                        valorTexto = $"{datosCredito.TasaInteresMoratoria:00.00} %";
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph()
                                .SetFixedLeading(fixedLeading)
                                .Add(new Text($"{numeracion.ToString()}. Tasa de Interés Moratoria {Environment.NewLine} (TEA 360 días)").SetFont(arialNN))
                            ));
                        tabla.AddCell(new Cell(0, 2)
                            .Add(new Paragraph(valorTexto)
                                .SetFixedLeading(fixedLeading)
                            ));

                        datosDocumento.Add(valorTexto);
                        #endregion

                        #region "TCEA"
                        numeracion++;
                        valorTexto = $"{datosCredito.TasaCostoAnual:00.00} %";
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph()
                                .SetFixedLeading(fixedLeading)
                                .Add(new Text($"{numeracion.ToString()}. Tasa de Costo Efectiva Anual (TCEA)").SetFont(arialNN))
                            ));
                        tabla.AddCell(new Cell(0, 2)
                            .Add(new Paragraph(valorTexto)
                                .SetFixedLeading(fixedLeading)
                            ));

                        datosDocumento.Add(valorTexto);
                        #endregion

                        #region "Monto total intereses"
                        numeracion++;
                        valorTexto = datosCredito.TotalInteres.ToString("C", cultPeru);
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph()
                                .SetFixedLeading(fixedLeading)
                                .Add(new Text($"{numeracion.ToString()}. Monto Total de los Intereses compensatorios").SetFont(arialNN))
                            ));
                        tabla.AddCell(new Cell(0, 2)
                            .Add(new Paragraph(valorTexto)
                                .SetFixedLeading(fixedLeading)
                            ));

                        datosDocumento.Add(valorTexto);
                        #endregion

                        #region "Cuenta afiliada"
                        numeracion++;
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph()
                                .SetFixedLeading(fixedLeading)
                                .Add(new Text($"{numeracion.ToString()}. Cuenta afiliada al sistema financiero para desembolso y cobros de cuotas mensuales").SetFont(arialNN))
                            ));
                        tabla.AddCell(new Cell(0, 2)
                            .Add(new Paragraph(datosCredito.NombreBancoDesembolso)
                                .SetFixedLeading(fixedLeading)
                            ));

                        datosDocumento.Add(datosCredito.NombreBancoDesembolso);
                        #endregion

                        #region "Tarjeta asociada"
                        numeracion++;
                        tabla.AddCell(new Cell()
                            .Add(new Paragraph()
                                .SetFixedLeading(fixedLeading)
                                .Add(new Text($"{numeracion.ToString()}. Tarjeta asociada a la cuenta bancaria").SetFont(arialNN))
                            ));
                        tabla.AddCell(new Cell(0, 2)
                            .Add(new Paragraph(datosCredito.CuentaBancariaDesembolso)
                                .SetFixedLeading(fixedLeading)
                            ));

                        datosDocumento.Add(datosCredito.CuentaBancariaDesembolso);
                        #endregion

                        #region "Comisiones aplicables"
                        numeracion++;
                        tabla.AddCell(new Cell(7, 0)
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph($"{numeracion.ToString()}. Comisiones aplicables")
                                .SetFont(arialNN).SetFontSize(fSizeNormal).SetFontColor(colorNormal)
                            ));

                        #region UsoCanales
                        tabla.AddCell(new Cell()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph("Uso de canales")
                                .SetFont(arialNN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.CENTER)
                            ));

                        tabla.AddCell(new Cell()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph()
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetFixedLeading(fixedLeading)
                                .Add(new Text($"Visita de nuestros ejecutivos a instalaciones del cliente.{Environment.NewLine}" +
                                    $"Costo: ").SetFont(arialNN))
                                .Add(esCampaniaPF ? new Text($"CAMPAÑA PRÉSTAMO FELIZ{Environment.NewLine}").SetFontColor(new DeviceRgb(255, 0, 0)) : new Text($"S/ 480.00 soles { Environment.NewLine}"))
                                .Add(new Text($"Uso de canales digitales de Préstamo Feliz{Environment.NewLine}Costo: ").SetFont(arialNN))
                                .Add("S/ 350.00 soles")
                            ));
                        #endregion

                        #region ModificaciónCodiciones
                        tabla.AddCell(new Cell(2, 0)
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph("Modificación de Condiciones Contractuales")
                                .SetFont(arialNN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.CENTER).SetFixedLeading(fixedLeading)
                            ));

                        tabla.AddCell(new Cell()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph()
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetFixedLeading(fixedLeading)
                                .Add(new Text("Modificaciones al contrato").SetFont(arialNN))
                                .Add($" solicitadas por el cliente distintas de la fecha de pago.{Environment.NewLine}")
                                .Add(new Text("SIN COSTO").SetFont(arialNN))
                            ));

                        tabla.AddCell(new Cell()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph()
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetFixedLeading(fixedLeading)
                                .Add(new Text("Reprogramación:").SetFont(arialNN))
                                .Add($" Modificación de la programación de las cuotas a solicitud del cliente ")
                                .Add(new Text($"SIN COSTO {Environment.NewLine}Reestructuración: ").SetFont(arialNN))
                                .Add($"Modificación de la programación de las cuotas vencidas a solicitud del cliente.{Environment.NewLine}")
                                .Add(new Text("Costo: ").SetFont(arialNN))
                                .Add($"2.50% del saldo vencido.")
                            ));
                        #endregion

                        #region Penalidades
                        tabla.AddCell(new Cell()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph("Penalidades")
                                .SetFont(arialNN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.CENTER).SetFixedLeading(fixedLeading)
                            ));

                        tabla.AddCell(new Cell()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph()
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetFixedLeading(fixedLeading)
                                .Add($"Penalidad por pago tardío.{Environment.NewLine}")
                                .Add(new Text("COSTO: ").SetFont(arialNN))
                                .Add($"S/ 35.00 soles a partir del segundo día hábil del vencimiento de la cuota.")
                            ));
                        #endregion

                        #region OtrasOperaciones
                        tabla.AddCell(new Cell()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph("Otras Operaciones")
                                .SetFont(arialNN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.CENTER).SetFixedLeading(fixedLeading)
                            ));

                        tabla.AddCell(new Cell()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph()
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetFixedLeading(fixedLeading)
                                .Add($"Duplicado de documentos.{ Environment.NewLine }")
                                .Add(new Text("COSTO: ").SetFont(arialNN))
                                .Add($"S/ 10.00 soles{ Environment.NewLine}" +
                                    $"Emisión de segunda carta de no adeudo{ Environment.NewLine }")
                                .Add(new Text("COSTO: ").SetFont(arialNN))
                                .Add($"S/ 10.00 soles")
                            ));
                        #endregion

                        #region GastosAdministrativos
                        tabla.AddCell(new Cell()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph()
                                .SetFont(arialNN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetFixedLeading(fixedLeading)
                                .Add(new Text($"Gastos{ Environment.NewLine }Administrativos{ Environment.NewLine}GAT").SetFont(arialNN))
                                .Add($" - Servicios asociados al Préstamo Simple")
                            ));

                        tabla.AddCell(new Cell()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph()
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                                .Add(new Text($"Gestión del proceso de Descuento por Planilla:{Environment.NewLine}").SetFont(arialNN))
                                .Add($"procedimiento interno de verificación y aceptación de pago vía descuento por planilla conforme a las instrucciones del Cliente.{Environment.NewLine}")
                                .Add(new Text($"Costo: ").SetFont(arialNN))
                                .Add($"S/ 15.00 soles mensual{Environment.NewLine}")
                                .Add(new Text($"Gestión de Débito Automático: ").SetFont(arialNN))
                                .Add($"procedimiento interno de verificación y aceptación de pago a través de empresas del sistema financiero conforme a las instrucciones del Cliente.{Environment.NewLine}")
                                .Add(new Text($"Costo: ").SetFont(arialNN))
                                .Add($"S/ 12.50 soles mensual")
                            ));
                        #endregion

                        #region Evaluación
                        tabla.AddCell(new Cell()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                            .Add(new Paragraph($"Evaluación de Póliza de Seguro Endosada")
                                .SetFont(arialNN).SetFontSize(8).SetFontColor(colorNormal).SetFixedLeading(fixedLeading)
                            ));

                        tabla.AddCell(new Cell()
                            .Add(new Paragraph()
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                                .Add(new Text($"Evaluación interna a cargo de Préstamo Feliz{Environment.NewLine}").SetFont(arialNN))
                                .Add($"Costo: S/ 50.00 soles")
                            ));
                        #endregion
                        #endregion
                        
                        documento.Add(tabla);
                        
                        #region "Derecho cliente - pago adelantado"
                        tabla = new Table(UnitValue.CreatePercentArray(new float[] { .4f, 10 })).UseAllAvailableWidth();

                        numeracion++;
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph($"{numeracion.ToString()}.")
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetFixedLeading(fixedLeading)
                            ));

                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("El Cliente tiene derecho a efectuar el ")
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                                .Add(new Text("pago adelantado ").SetFont(arialNN))
                                .Add("de sus cuotas, de conformidad con los términos establecidos en la cláusula 12 del Contrato.")
                            ));
                        #endregion

                        #region "Derecho cliente - pago anticipado"
                        numeracion++;
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph($"{numeracion.ToString()}.")
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetFixedLeading(fixedLeading)
                            ));

                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph("El Cliente tiene derecho a efectuar ")
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                                .Add(new Text("pagos anticipados ").SetFont(arialNN))
                                .Add("de las cuotas o saldos, ya sea en forma total o parcial, con la subsiguiente reducción de los intereses al día de pago, deduciendo asimismo " +
                                "las comisiones y gastos derivados de las cláusulas del Contrato, sin que le sea aplicable ninguna comisión, gasto, penalidad o cobros de " +
                                "naturaleza o efecto similar, de conformidad con los términos indicados en la cláusula 12 del Contrato.")
                            ));
                        #endregion

                        #region "Modalidad de pago"
                        numeracion++;
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph($"{numeracion.ToString()}.")
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetFixedLeading(fixedLeading)
                            ));

                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph()
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                                .Add(new Text($"Modalidad de pago elegida por el Cliente:{Environment.NewLine}").SetFont(arialNN))
                                .Add($"(    ) Descuento en la planilla de pagos del empleador del Cliente.{Environment.NewLine}")
                                .Add($"(    ) Débito automático en empresas del sistema financiero autorizadas.{Environment.NewLine}")
                                //.Add($"(    ) Efectivo, cheque u otra modalidad a través de las agencias u oficinas de terceros autorizado.{Environment.NewLine}")
                            ));

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));

                        Table subTabla = new Table(UnitValue.CreatePercentArray(new float[] { 1, 1 })).UseAllAvailableWidth().SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal);

                        subTabla.AddCell(new Cell()
                            .Add(new Paragraph("Cuenta bancaria de actualizada para cobros")
                                .SetFixedLeading(fixedLeading)
                            ));

                        subTabla.AddCell(new Cell()
                            .Add(new Paragraph($"{datosCredito.CuentaBancariaDesembolso}")
                                .SetFixedLeading(fixedLeading)
                            ));

                        subTabla.AddCell(new Cell()
                            .Add(new Paragraph("Tarjeta asociada a cuenta bancaria actualizada para cobros")
                                .SetFixedLeading(fixedLeading)
                            ));

                        subTabla.AddCell(new Cell()
                            .Add(new Paragraph($"{datosCredito.NoTarjetaVisa}")
                                .SetFixedLeading(fixedLeading)
                            ));

                        tabla.AddCell(new Cell().Add(subTabla).SetBorder(Border.NO_BORDER));

                        datosDocumento.Add(datosCredito.NoTarjetaVisa);
                        #endregion

                        #region "Tributos" 
                        numeracion++;
                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph($"{numeracion.ToString()}.")
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetFixedLeading(fixedLeading)
                            ));

                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .Add(new Paragraph()
                                .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                                .Add(new Text($"Tributos:{Environment.NewLine}").SetFont(arialNN))
                                .Add("Corresponde al Cliente asumir el costo del Impuesto General a las Ventas (IGV). El IGV es del 18% y se aplica respecto de los " +
                                "intereses compensatorios y aquellas comisiones asociadas al servicio de financiamiento. Se encuentran excluidas de su cálculo el capital, " +
                                $"los intereses moratorios o penalidad incumplimiento.{Environment.NewLine}{Environment.NewLine}" +
                                $"La emisión y otorgamiento del comprobante de pago respectivo es de cargo de Préstamo Feliz y procederá en sus oficinas y/o agencias, " +
                                $"estando disponibles a partir del vencimiento de las fechas de pago de las cuotas previstas en el cronograma de pago.")
                            ));
                        #endregion
                        

                        documento.Add(tabla);

                        #region "Declaracion Cliente"
                        parrafo = new Paragraph("El Cliente declara que está de acuerdo, ha sido informado y ha recibido una explicación detallada de los términos y " +
                            "condiciones del Préstamo Simple proporcionada por Préstamo Feliz, de conformidad con las disposiciones del Contrato y de esta Hoja de Resumen, " +
                            "la cual el Cliente declara que entiende y acepta.")
                            .SetFont(arialN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .SetMarginTop(10)
                            .SetMarginBottom(10);
                        documento.Add(parrafo);

                        parrafo = new Paragraph()
                            .SetFont(arialNN).SetFontSize(fSizeNormal).SetFontColor(colorNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetFixedLeading(fixedLeading)
                            .Add($"Lima, {DateTime.Now.Day:00} de {Utils.MesFecha(DateTime.Now)} de {DateTime.Now.Year}");

                        documento.Add(parrafo);
                        #endregion

                        #region Firmas
                        tabla = new Table(UnitValue.CreatePercentArray(new float[] { 1, 1, 1 })).UseAllAvailableWidth()
                            .SetMarginTop(60)
                            .SetFont(arialNN)
                            .SetFontSize(fSizeNormal)
                            .SetFontColor(colorNormal);

                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();

                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 100 * punto,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 60)) * punto
                            //PosX = 100, //Para test
                            //PosY = (Utils.AltoAreaRestante(documento) + 60)//Para test
                        });
                        //Para test
                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);

                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new SolidBorder(colorNormal, 0.5f))
                            .Add(new Paragraph($"Firma del Cliente{Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine}").SetTextAlignment(TextAlignment.CENTER)));

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));

                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new SolidBorder(colorNormal, 0.5f))
                            .Add(new Paragraph($"Firma de Cónyuge{Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine}").SetTextAlignment(TextAlignment.CENTER)
                            ));

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));

                        tabla.AddCell(new Cell().SetBorder(Border.NO_BORDER));

                        tabla.AddCell(new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .SetBorderTop(new SolidBorder(colorNormal, 0.5f))
                            .Add(new Paragraph($"Préstamo Feliz en 15 minutos S.A.C.{Environment.NewLine}RUC Nº 20601194181").SetTextAlignment(TextAlignment.CENTER)
                            ));

                        documento.Add(tabla);
                        #endregion

                        encabezado.FijarPaginado(documento);
                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(datosCredito)}", JsonConvert.SerializeObject(ex));
            }
            return archivo;
        }

        protected partial class Encabezado
        {
            private void EncabezadoV2()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF = AppSettings.LogPF;
                logoPF.SetWidth(UnitValue.CreatePercentValue(62));

                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.AddCell(new Cell()
                    .SetBorder(Border.NO_BORDER));

                tEncabezado.SetFixedPosition(50, paginaTamanio.GetTop() - 70, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }
        }
    }
}
