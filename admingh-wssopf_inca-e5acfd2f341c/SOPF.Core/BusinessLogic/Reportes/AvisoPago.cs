﻿using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Model.Gestiones;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class AvisoPago
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private AvisoPagoVM datosAviso;
        private float fSizeNormal = 11;
        private List<string> datosDocumento = new List<string>();
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public AvisoPago(int version)
        {
            this.version = version <= 0 ? 1 : version;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
        }

        public byte[] Generar(AvisoPagoVM avisoDatos)
        {
            byte[] avisoByte = null;
            datosAviso = avisoDatos;
            switch (version)
            {
                case 1:
                    avisoByte = AvisoPagoV1();
                    break;
            }
            return avisoByte;
        }

        private byte[] AvisoPagoV1()
        {
            byte[] archivo = null;

            try
            {
                if (datosAviso == null) datosAviso = new AvisoPagoVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LETTER);
                        Encabezado encabezado = new Encabezado(version);
                        documento.SetMargins(75, 85, 75, 85);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        string dia = string.Empty;
                        string mes = string.Empty;
                        string anio = string.Empty;
                        string fecha = string.Empty;
                        dia = datosAviso.FechaRegistro.Day.ToString("00");
                        mes = Utils.MesFecha(datosAviso.FechaRegistro);
                        anio = datosAviso.FechaRegistro.Year.ToString();
                        fecha = dia + " de " + mes + " del " + anio;

                        #region Titulo
                        Paragraph parrafo = new Paragraph("Lima, " + fecha).SetFont(arial).SetFontSize(fSizeNormal);
                        documento.Add(parrafo);
                        parrafo = new Paragraph("AVISO DE PAGO").SetFont(arialNegrita).SetFontSize(22).SetTextAlignment(TextAlignment.CENTER);
                        documento.Add(parrafo);
                        #endregion

                        #region Datos
                        parrafo = new Paragraph().SetFont(arialNegrita).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        Text texto = new Text("Señor (a) (ita): ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosAviso.NombreCliente).SetFont(arial);
                        texto = new Text(Environment.NewLine + (datosAviso.IdDestino != 2 ? "Dirección: " : "Dirección laboral: ")).SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosAviso.Direccion).SetFont(arial);
                        texto = new Text(Environment.NewLine + (datosAviso.IdDestino != 2 ? "Referencia Domiciliaria: " : "Dependencia y Ubicación Centro Laboral: ")).SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosAviso.ReferenciaDomicilio ?? "").SetFont(arial);
                        texto = new Text(Environment.NewLine + "Deuda Impaga a la fecha de la emisión de la carta (no incluye intereses moratorios): ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosAviso.TotalDeuda.ToString("C", cultPeru)).SetFont(arial);
                        texto = new Text(Environment.NewLine + "Numero de cuotas vencidas: ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosAviso.CuotasVencidas.ToString()).SetFont(arial);
                        texto = new Text(Environment.NewLine + "Código de cliente: ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosAviso.IdSolicitud.ToString()).SetFont(arial);
                        documento.Add(parrafo);
                        #endregion

                        #region Texto
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("De nuestra consideración,");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Le informamos que a pesar de nuestros requerimientos de notificación personalizada, " +
                                        "telefónica y / o sus ofrecimientos de pago, hasta la fecha, no ha cumplido con regularizar sus " + 
                                        "pagos pendientes.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Le recordamos que si no regulariza el pago de su deuda, en un plazo de 48 horas, será " + 
                                        "reportado a las centrales de riesgos, y daremos inicio a las acciones judiciales vía ejecución " + 
                                        "del pagaré y embargo, de conformidad con lo establecido en el contrato.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Le reiteramos que no es nuestra intención reportarlo, ejecutar el titulo valor e iniciar la " + 
                                        "demanda correspondiente, por lo que esperamos que cumpla con regularizar su situación.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Puede consultar mayor información a los teléfonos 914-684621 - 986-900939.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Sin otro particular, nos despedimos.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.CENTER);
                        texto = new Text(Environment.NewLine + "Atentamente,");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arialNegrita).SetFontSize(14).SetTextAlignment(TextAlignment.CENTER);
                        texto = new Text("PRÉSTAMO FELIZ EN 15 MINUTOS SAC" + Environment.NewLine + Environment.NewLine);
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion
                        
                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private int version;
            private Image logoPF;
            private PdfFont arial;
            private PdfFont arialNegrita;
            public int PaginaActual { get; set; }

            public Encabezado(int version)
            {
                this.version = version <= 0 ? 1 : version;
                logoPF = AppSettings.LogPF;
                arial = AppSettings.Arial;
                arialNegrita = AppSettings.ArialNegrita;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(82, paginaTamanio.GetTop() - 80, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }
        }

        protected class CellRadius : CellRenderer
        {
            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}
