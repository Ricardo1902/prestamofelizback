﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Model.Gestiones;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class PromocionReduccionMora
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private PromocionReduccionMoraVM datosNotificacion;
        private float fSizeNormal = 10;
        private List<string> datosDocumento = new List<string>();
        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public PromocionReduccionMora(int version)
        {
            this.version = version <= 0 ? 1 : version;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
        }

        public byte[] Generar(PromocionReduccionMoraVM notificacionDatos)
        {
            byte[] notificacionByte = null;
            datosNotificacion = notificacionDatos;
            switch (version)
            {
                case 1:
                    notificacionByte = PromocionReduccionMoraV1();
                    break;
            }
            return notificacionByte;
        }

        private byte[] PromocionReduccionMoraV1()
        {
            byte[] archivo = null;

            try
            {
                if (datosNotificacion == null) datosNotificacion = new PromocionReduccionMoraVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LETTER);
                        Encabezado encabezado = new Encabezado(version);
                        documento.SetMargins(75, 85, 75, 85);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        string dia = string.Empty;
                        string mes = string.Empty;
                        string anio = string.Empty;
                        string fecha = string.Empty;
                        dia = datosNotificacion.FechaRegistro.Day.ToString("00");
                        mes = Utils.MesFecha(datosNotificacion.FechaRegistro);
                        anio = datosNotificacion.FechaRegistro.Year.ToString();
                        fecha = dia + " de " + mes + " del " + anio;

                        #region Titulo
                        Paragraph parrafo = new Paragraph("Lima, " + fecha).SetFont(arial).SetFontSize(fSizeNormal);
                        documento.Add(parrafo);
                        parrafo = new Paragraph("PROMOCION" + Environment.NewLine).SetFont(arialNegrita).SetFontSize(16).SetTextAlignment(TextAlignment.CENTER);
                        Text texto = new Text("REDUCCION DE 100% DE MORA").SetFont(arialNegrita).SetFontSize(12);
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Datos
                        parrafo = new Paragraph().SetFont(arialNegrita).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Señor (a) (ita): ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosNotificacion.NombreCliente).SetFont(arial);
                        texto = new Text(Environment.NewLine + (datosNotificacion.IdDestino != 2 ? "Dirección: " : "Dirección laboral: ")).SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosNotificacion.Direccion).SetFont(arial);
                        texto = new Text(Environment.NewLine + (datosNotificacion.IdDestino != 2 ? "Referencia Domiciliaria: " : "Dependencia y Ubicación Centro Laboral: ")).SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosNotificacion.ReferenciaDomicilio ?? "").SetFont(arial);
                        texto = new Text(Environment.NewLine + "Deuda Impaga a la fecha de la emisión de la carta (no incluye intereses moratorios): ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosNotificacion.TotalDeuda.ToString("C", cultPeru)).SetFont(arial);
                        texto = new Text(Environment.NewLine + "Numero de cuotas vencidas: ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosNotificacion.CuotasVencidas.ToString()).SetFont(arial);
                        texto = new Text(Environment.NewLine + "Código de cliente: ").SetFont(arialNegrita);
                        parrafo.Add(texto);
                        parrafo.Add(datosNotificacion.IdSolicitud.ToString()).SetFont(arial);
                        documento.Add(parrafo);
                        #endregion

                        #region Texto
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Estimado cliente por esta única vez te damos la facilidad de cancelar tu deuda morosa " +
                                        "condonando los siguientes intereses y gastos: ");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        List lista = new List();
                        ListItem item = new ListItem("Condonación de intereses moratorios al 100%");
                        lista.Add(item);
                        item = new ListItem("Condonación de gastos administrativos morosos al 100%");
                        lista.Add(item);
                        item = new ListItem("Condonación de gastos legales por deuda morosa al 100%");
                        lista.Add(item);
                        documento.Add(lista);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Deuda Total" + Environment.NewLine + datosNotificacion.TotalDeuda.ToString("C", cultPeru));
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        Table tContenido = new Table(UnitValue.CreatePercentArray(3)).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, false, false, false, Alineacion.Centro), false);
                        Cell celda = new Cell(2, 0);
                        parrafo = new Paragraph("PAGUE SÓLO: " + (datosNotificacion.DescuentoDeuda.HasValue ? datosNotificacion.DescuentoDeuda.Value.ToString("C", cultPeru) : "")).SetFont(arial).SetTextAlignment(TextAlignment.CENTER).SetFontColor(new DeviceRgb(255, 0, 0));
                        celda.Add(parrafo);
                        celda.SetBorderTop(new SolidBorder(0.5f));
                        celda.SetBorderBottom(new SolidBorder(0.5f));
                        celda.SetBorderLeft(new SolidBorder(0.5f));
                        celda.SetBorderRight(new SolidBorder(0.5f));
                        celda.SetTextAlignment(TextAlignment.CENTER);
                        celda.SetStrokeColor(new DeviceRgb(255, 0, 0));
                        AgregarCelda(tContenido, celda, false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, fSizeNormal, false, false, false, false, Alineacion.Centro), false);
                        documento.Add(tContenido);

                        parrafo = new Paragraph().SetFont(arialNegrita).SetFontSize(14).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Promoción vigente únicamente hasta el " + datosNotificacion.FechaVigencia.ToString("dd/MM/yyyy"));
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Regula tu situación crediticia aprovechando esta promoción hacia tu deuda MOROSA " +
                                        "Comunícate con nuestros asesores para que te brinden otras opciones de pagos, no " +
                                        "desaproveches esta oportunidad y evita el inicio del proceso judicial, ejecución de pagaré y " +
                                        "embargos.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Puede consultar mayor información a los teléfonos 914-684621 - 986-900939.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        texto = new Text("Sin otro particular, nos despedimos.");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.CENTER);
                        texto = new Text("Atentamente,");
                        parrafo.Add(texto);
                        documento.Add(parrafo);

                        parrafo = new Paragraph().SetFont(arialNegrita).SetFontSize(14).SetTextAlignment(TextAlignment.CENTER);
                        texto = new Text("PRÉSTAMO FELIZ EN 15 MINUTOS SAC");
                        parrafo.Add(texto);
                        documento.Add(parrafo);
                        #endregion

                        #region Tabla
                        tContenido = new Table(UnitValue.CreatePercentArray(3)).UseAllAvailableWidth();
                        AgregarCelda(tContenido, NuevaCelda("FIRMA CLIENTE", arialNegrita, 9, true, true, true, true, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, 9, true, true, true, true), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, 9, false, false, false, false), false);
                        AgregarCelda(tContenido, NuevaCelda("TELÉFONO", arialNegrita, 9, true, true, true, true, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, 9, true, true, true, true), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, 9, false, false, false, false), false);
                        AgregarCelda(tContenido, NuevaCelda("CORREO", arialNegrita, 9, true, true, true, true, 0, 0, Alineacion.Izquierda), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, 9, true, true, true, true), false);
                        AgregarCelda(tContenido, NuevaCelda("", arial, 9, false, false, false, false), false);
                        documento.Add(tContenido);
                        #endregion
                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private int version;
            private Image logoPF;
            private PdfFont arial;
            private PdfFont arialNegrita;
            public int PaginaActual { get; set; }

            public Encabezado(int version)
            {
                this.version = version <= 0 ? 1 : version;
                logoPF = AppSettings.LogPF;
                arial = AppSettings.Arial;
                arialNegrita = AppSettings.ArialNegrita;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                logoPF.SetWidth(UnitValue.CreatePercentValue(62));
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(2));
                Cell celda = new Cell()
                    .Add(logoPF)
                    .SetBorder(Border.NO_BORDER);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(82, paginaTamanio.GetTop() - 80, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }
        }

        protected class CellRadius : CellRenderer
        {
            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}
