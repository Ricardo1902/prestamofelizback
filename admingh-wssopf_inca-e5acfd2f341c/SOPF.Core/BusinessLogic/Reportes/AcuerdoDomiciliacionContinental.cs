﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class AcuerdoDomiciliacionContinental
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private AcuerdoDomiciliacionClienteVM datosAcuerdo;
        private float fSizeNormal = 8;
        private iText.Kernel.Colors.Color negro = ColorConstants.BLACK;
        private iText.Kernel.Colors.Color azulPeru = new DeviceRgb(16, 38, 79);
        public List<PosicionFirmaVM> Firmas { get; set; }
        private List<string> datosDocumento = new List<string>();

        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public AcuerdoDomiciliacionContinental(int version)
        {
            this.version = version <= 0 ? 1 : version;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
            Firmas = new List<PosicionFirmaVM>();
        }

        public byte[] Generar(AcuerdoDomiciliacionClienteVM acuerdoDatos)
        {
            byte[] acuerdoByte = null;
            datosAcuerdo = acuerdoDatos;
            switch (version)
            {
                case 1:
                    acuerdoByte = AcuerdoV1();
                    break;
            }
            return acuerdoByte;
        }

        private byte[] AcuerdoV1()
        {
            byte[] archivo = null;
            float punto = 0.35277f;
            Image firma = AppSettings.FirmaAmpliacionCredito(version);
            try
            {
                if (datosAcuerdo == null) datosAcuerdo = new AcuerdoDomiciliacionClienteVM();
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL);
                        documento.SetMargins(25, 25, 25, 25);
                        Encabezado encabezado = new Encabezado(version, datosAcuerdo);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        #region Estimados
                        Paragraph parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED).SetMarginTop(100);
                        parrafo.Add("Estimados señores:" + Environment.NewLine + Environment.NewLine +
                            "Mucho agradeceré(mos), se sirvan afiliar mi(nuestra) cuenta de ahorros/corriente al Sistema de Cargo en Cuenta Automático, para " +
                            "cuyos fines autorizo(amos) al BBVA Banco Continental a debitar de mi(nuestra) cuenta los montos correspondientes al pago de los " +
                            "recibos afiliados a este servicio." + Environment.NewLine + Environment.NewLine +
                            "Queda entendido que el abono a la empresa, estará condicionado a la disponibilidad de fondos en la cuenta indicada y libero(amos) al " +
                            "Banco de toda responsabilidad por la ejecución de la orden, sin reserva, ni limitación alguna, no estando Uds. obligados a confirmar los " +
                            "importes de cargo indicados por emisor, ni gestionar ninguna autorización adicional a la presente." + Environment.NewLine + Environment.NewLine +
                            "Autorizo(amos) al Banco a entregarle a la(s) empresa(s) que presta(n) el(los) servicio(s), materia de esta afiliación, la información " +
                            "básica necesaria para poder brindar este servicio, como Nombres, Apellidos, Dirección, Número de Documento, el número de cuenta " +
                            "de cargo y cualquier otro dato que permita contactarme(nos)." + Environment.NewLine);
                        documento.Add(parrafo);
                        #endregion

                        datosDocumento.Add(datosAcuerdo.BCOficina);
                        datosDocumento.Add(datosAcuerdo.BCCuenta);
                        datosDocumento.Add(datosAcuerdo.BCDC);

                        #region NombreRazonSocial
                        Table tContenido = new Table(UnitValue.CreatePercentArray(6)).UseAllAvailableWidth()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE);
                        Cell celda = NuevaCelda("NOMBRE O RAZON SOCIAL DEL TITULAR DE LA CUENTA", arialNegrita, fSizeNormal, false, false, false, false, 1, 6, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda($"{datosAcuerdo.NombreCliente} {datosAcuerdo.ApPaternoCliente} {datosAcuerdo.ApMaternoCliente}", arial, fSizeNormal, true, true, true, true, 1, 6, Alineacion.Izquierda);
                        datosDocumento.Add($"{datosAcuerdo.NombreCliente} {datosAcuerdo.ApPaternoCliente} {datosAcuerdo.ApMaternoCliente}");
                        tContenido.AddCell(celda);
                        celda = NuevaCelda("DIRECCION (Calle / Número / Distrito / Provincia)", arialNegrita, fSizeNormal, false, false, false, false, 1, 6, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda(datosAcuerdo.DireccionCliente, arial, fSizeNormal, true, true, true, true, 1, 6, Alineacion.Izquierda);
                        datosDocumento.Add(datosAcuerdo.DireccionCliente);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda("RUC / DOCUMENTO DE IDENTIDAD", arialNegrita, fSizeNormal, false, false, false, false, 1, 3, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda("TELEFONOS", arialNegrita, fSizeNormal, false, false, false, false, 1, 3, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda(datosAcuerdo.DNICliente, arial, fSizeNormal, true, true, true, true, 1, 3, Alineacion.Izquierda);
                        datosDocumento.Add(datosAcuerdo.DNICliente);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda(datosAcuerdo.CelularCliente, arial, fSizeNormal, true, true, false, true, 1, 3, Alineacion.Izquierda);
                        datosDocumento.Add(datosAcuerdo.CelularCliente);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda("(*) Opcional", arialNegrita, fSizeNormal, false, false, false, false, 1, 6, Alineacion.Derecha);
                        documento.Add(tContenido);

                        tContenido = new Table(UnitValue.CreatePercentArray(new float[] { 4, 0.2f, 4, 0.2f, 1, 0.2f, 1 }))
                            .UseAllAvailableWidth()
                            .SetMarginTop(10)
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE);
                        tContenido.AddCell(NuevaCelda("EMPRESA DE SERVICIO/ INSTITUCIONES /" + Environment.NewLine + "OTRAS EMPRESAS", arialNegrita, fSizeNormal, true, true, true, true, Alineacion.Centro));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("CODIGO DEL SERVICIO*", arialNegrita, fSizeNormal, true, true, true, true, Alineacion.Centro));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("Cantidad de" + Environment.NewLine + " Recibos(*)", arialNegrita, fSizeNormal, true, true, true, true, Alineacion.Centro));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("Importe" + Environment.NewLine + "Máximo (*)", arialNegrita, fSizeNormal, true, true, true, true, Alineacion.Centro));

                        tContenido.AddCell(NuevaCelda("Prestamo Feliz", arial, fSizeNormal, false, true, false, false, Alineacion.Izquierda));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda(datosAcuerdo.IdSolicitud.ToString(), arial, fSizeNormal, false, true, false, false, Alineacion.Izquierda));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda(datosAcuerdo.CantidadRecibos.ToString(), arial, fSizeNormal, false, true, false, false, Alineacion.Centro));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda(datosAcuerdo.ImporteMaximoCuota.ToString("C", cultPeru), arial, fSizeNormal, false, true, false, false, Alineacion.Centro));

                        datosDocumento.Add(datosAcuerdo.IdSolicitud.ToString());
                        datosDocumento.Add(datosAcuerdo.CantidadRecibos.ToString());
                        datosDocumento.Add(datosAcuerdo.ImporteMaximoCuota.ToString("C", cultPeru));

                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Izquierda));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetHeight(10));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Izquierda));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Centro));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Centro));

                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Izquierda));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetHeight(10));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Izquierda));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Centro));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Centro));

                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Izquierda));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetHeight(10));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Izquierda));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Centro));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Centro));

                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Izquierda));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetHeight(10));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Izquierda));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Centro));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Centro));

                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Izquierda));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetHeight(10));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Izquierda));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Centro));
                        tContenido.AddCell(new Cell().SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(NuevaCelda("", arial, fSizeNormal, false, true, false, false, Alineacion.Centro));

                        documento.Add(tContenido);

                        #endregion

                        #region CodigoDeServicio
                        tContenido = new Table(UnitValue.CreatePercentArray(6)).UseAllAvailableWidth()
                            .SetVerticalAlignment(VerticalAlignment.MIDDLE);
                        parrafo = new Paragraph().SetFont(arialNegrita).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT);
                        Text texto1 = new Text("*");
                        Text texto2 = new Text("* CODIGO DE SERVICIO:").SetUnderline(0.1f, -2f);
                        parrafo.Add(texto1).Add(texto2);
                        tContenido.AddCell(new Cell(1, 6).Add(parrafo).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("Telefónica Básica:").SetFont(arialNegrita).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("N° de Inscripción").SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("Luz del Sur:").SetFont(arialNegrita).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("N° de Suministro").SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("").SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("").SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("Telefónica Movistar:").SetFont(arialNegrita).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("N° de Inscripción").SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("Edelnor:").SetFont(arialNegrita).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("N° de Cliente").SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("").SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("").SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("Cable Mágico:").SetFont(arialNegrita).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("Código del Cliente").SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("Sedapal:").SetFont(arialNegrita).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("N° de Suministro").SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("").SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));
                        tContenido.AddCell(new Cell(1, 1).Add(new Paragraph("").SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT)).SetBorder(Border.NO_BORDER));

                        parrafo = new Paragraph().SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.LEFT);
                        texto1 = new Text("Otros:").SetFont(arialNegrita);
                        texto2 = new Text("Para el caso de recibos de Colegios, Clubes, Seguros, TIM y Nextel debe consultarse el Código de Servicio a su empresa ó " +
                            "institución. Si es más comodo para usted puede dejarlo en la misma administración de su empresa o institución que ellos se encargarán " +
                            "de tramitar su solicitud.").SetFont(arial);
                        parrafo.Add(texto1).Add(texto2);
                        tContenido.AddCell(new Cell(1, 6).Add(parrafo).SetBorder(Border.NO_BORDER));

                        Table sContenido = new Table(UnitValue.CreatePercentArray(new float[] { .3f, 10 })).UseAllAvailableWidth();
                        AgregarCelda(sContenido, NuevaCelda("1.", arialNegrita, fSizeNormal).SetBorder(Border.NO_BORDER), false);
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add("Continúe realizando los pagos de sus recibos como regularmente lo hace, hasta que en su recibo la empresa le indique que será " +
                            "cargado en cuenta.");
                        sContenido.AddCell(new Cell().Add(parrafo).SetBorder(Border.NO_BORDER));

                        AgregarCelda(sContenido, NuevaCelda("2.", arialNegrita, fSizeNormal).SetBorder(Border.NO_BORDER), false);
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add("Ud. puede establecer el número de recibos a pagar de un servicio y el importe máximo a pagar por recibo. El Banco ingresará la " +
                            "instrucción al sistema y queda eximido de toda responsabilidad por el no pago de cualquier recibo, producto de la aplicación de los " +
                            "parámetros dados por el cliente.");
                        sContenido.AddCell(new Cell().Add(parrafo).SetBorder(Border.NO_BORDER));

                        AgregarCelda(sContenido, NuevaCelda("3.", arialNegrita, fSizeNormal).SetBorder(Border.NO_BORDER), false);
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add("La cuenta de cargo elegida podrá ser en moneda diferente a la de facturación del servicio. El cliente autoriza al Banco a efectuar la " +
                            "compra/venta de moneda al tipo de cambio del día del Banco para el pago de aquellos servicios cuya moneda de facturación sea " +
                            "distinta a la de la cuenta de cargo.");
                        sContenido.AddCell(new Cell().Add(parrafo).SetBorder(Border.NO_BORDER));

                        AgregarCelda(sContenido, NuevaCelda("4.", arialNegrita, fSizeNormal).SetBorder(Border.NO_BORDER), false);
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add("Ud. puede ordenar la suspensión de un débito o resolver definitivamente el pacto de adhesión al débito automático. cuarentiocho " +
                            "horas antes de la fecha de vencimiento.");
                        sContenido.AddCell(new Cell().Add(parrafo).SetBorder(Border.NO_BORDER));

                        AgregarCelda(sContenido, NuevaCelda("5.", arialNegrita, fSizeNormal).SetBorder(Border.NO_BORDER), false);
                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add("Ud. recibirá la información de los débitos automáticos realizados por los recibos autorizados, en su estado de movimiento y saldos " +
                            "de la cuenta de cargo. Así mismo, podrá solicitar en cualquier Oficina, en horario de atención, el estado de movimientos y saldos de " +
                            "su cuenta a donde se hacen el cargo de los recibos autorizados.");
                        sContenido.AddCell(new Cell().Add(parrafo).SetBorder(Border.NO_BORDER));

                        tContenido.AddCell(new Cell(1, 6).Add(sContenido).SetBorder(Border.NO_BORDER));

                        parrafo = new Paragraph().SetFont(arial).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.JUSTIFIED);
                        parrafo.Add("Una vez consignados sus datos y su(s) firma(s), sírvase(sírvanse) entregar este formulario en cualquiera de nuestras oficinas a nivel " +
                            "nacional. Adjunte(n) una fotocopia del último recibo de cada servicio que desee(n) inscribir.");
                        tContenido.AddCell(new Cell(1, 6).Add(parrafo).SetBorder(Border.NO_BORDER));

                        celda = NuevaCelda(Environment.NewLine, arial, fSizeNormal, false, false, false, false, 1, 6, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda(Environment.NewLine, arial, fSizeNormal, false, false, false, false, 1, 6, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda(Environment.NewLine, arial, fSizeNormal, false, false, false, false, 1, 6, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda(Environment.NewLine, arial, fSizeNormal, false, false, false, false, 1, 6, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda(Environment.NewLine, arial, fSizeNormal, false, false, false, false, 1, 6, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda(Environment.NewLine, arial, fSizeNormal, false, false, false, false, 1, 6, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda(Environment.NewLine, arial, fSizeNormal, false, false, false, false, 1, 6, Alineacion.Izquierda);
                        tContenido.AddCell(celda);

                        celda = NuevaCelda("", arial, fSizeNormal, false, true, false, false, 1, 2, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda("", arial, fSizeNormal, false, false, false, false, 1, 2, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda("", arial, fSizeNormal, false, true, false, false, 1, 2, Alineacion.Izquierda);
                        tContenido.AddCell(celda);

                        parrafo = new Paragraph().SetFont(arialNegrita).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.CENTER);
                        parrafo.Add("Firma(s) del(los) titular(es) o"
                            + Environment.NewLine + "Representante(s) Legal(es)");
                        tContenido.AddCell(new Cell(1, 2).Add(parrafo).SetBorder(Border.NO_BORDER));
                        celda = NuevaCelda("", arial, fSizeNormal, false, false, false, false, 1, 2, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        parrafo = new Paragraph().SetFont(arialNegrita).SetFontSize(fSizeNormal).SetTextAlignment(TextAlignment.CENTER);
                        parrafo.Add("p. BBVA BANCO CONTINENTAL");
                        tContenido.AddCell(new Cell(1, 2).Add(parrafo).SetBorder(Border.NO_BORDER));

                        celda = NuevaCelda("E1051 - 28 Set. 2005", arial, 6, false, false, false, false, 1, 2, Alineacion.Izquierda);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda("CLIENTE", arialNegrita, fSizeNormal, false, false, false, false, 1, 2, Alineacion.Centro);
                        tContenido.AddCell(celda);
                        celda = NuevaCelda("", arial, fSizeNormal, false, false, false, false, 1, 2, Alineacion.Izquierda);
                        tContenido.AddCell(celda);

                        documento.Add(tContenido);
                        #endregion

                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();
                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = encabezado.PaginaActual,
                            PosX = 20,
                            PosY = (altoPagina - (Utils.AltoAreaRestante(documento) + 120)) * punto
                        });
                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private decimal version;
            private string FechaSolicitud;
            private string IdSolicitud;
            private AcuerdoDomiciliacionClienteVM datosAcuerdo;
            private float fSizeNormal = 10;
            private iText.Kernel.Colors.Color colorNormal = new DeviceRgb(16, 38, 79);
            public int PaginaActual { get; set; }

            private PdfFont arial;
            private PdfFont arialNegrita;

            public Encabezado(decimal version, AcuerdoDomiciliacionClienteVM obj)
            {
                this.version = version <= 0 ? 1 : version;
                arial = AppSettings.Arial;
                arialNegrita = AppSettings.ArialNegrita;
                this.FechaSolicitud = obj.FechaSolicitud == null ? string.Empty : obj.FechaSolicitud.ToString("dd/MM/yyyy");
                this.IdSolicitud = obj.IdSolicitud <= 0 ? string.Empty : obj.IdSolicitud.ToString();
                datosAcuerdo = obj;
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(6)).UseAllAvailableWidth();
                Cell celda = NuevaCelda("", arial, fSizeNormal, false, false, false, false, 1, 1, Alineacion.Centro, PdfCelda.Color.BLACK);
                tEncabezado.AddCell(celda);
                celda = NuevaCelda("SOLICITUD DE CARGO AUTOMATICO DE RECIBOS EN CUENTA", arialNegrita, 11, false, false, false, false, 1, 4, Alineacion.Centro, PdfCelda.Color.BLACK).SetVerticalAlignment(VerticalAlignment.TOP);
                tEncabezado.AddCell(celda);
                celda = NuevaCelda(IdSolicitud, arial, 8, false, false, false, false, 1, 1, Alineacion.Derecha, PdfCelda.Color.RED).SetVerticalAlignment(VerticalAlignment.BOTTOM);
                tEncabezado.AddCell(celda);
                celda = NuevaCelda("", arial, fSizeNormal, false, false, false, false, 1, 5, Alineacion.Centro, PdfCelda.Color.BLACK);
                tEncabezado.AddCell(celda);
                Paragraph p = new Paragraph().SetFontSize(8);
                Text t1 = new Text("Fecha: ").SetFontSize(8);
                Text t2 = new Text(FechaSolicitud).SetUnderline(0.1f, -2f).SetFontSize(8);
                p.Add(t1).Add(t2);
                celda = new Cell(1, 2).SetTextAlignment(TextAlignment.CENTER).SetVerticalAlignment(VerticalAlignment.BOTTOM).SetBorder(Border.NO_BORDER);
                celda.Add(p);
                tEncabezado.AddCell(celda);
                celda = NuevaCelda("", arial, fSizeNormal, false, false, false, false, 1, 4, Alineacion.Centro, PdfCelda.Color.BLACK);
                tEncabezado.AddCell(celda);
                celda = new Cell(1, 2).SetTextAlignment(TextAlignment.CENTER).SetVerticalAlignment(VerticalAlignment.MIDDLE).SetBorder(Border.NO_BORDER);
                Table sEncabezado = new Table(UnitValue.CreatePercentArray(7)).UseAllAvailableWidth().SetBorder(Border.NO_BORDER);
                Cell sCelda = NuevaCelda("NUMERO DE CUENTA DE CARGO (Titular de la Cuenta)", arialNegrita, 5, true, false, true, true, 1, 7, Alineacion.Centro, PdfCelda.Color.BLACK);
                sEncabezado.AddCell(sCelda);
                sCelda = NuevaCelda("ENTIDAD", arialNegrita, 7, true, false, true, true, 1, 1, Alineacion.Centro, PdfCelda.Color.BLACK).SetMarginTop(0.5f).SetMarginBottom(0.5f);
                sEncabezado.AddCell(sCelda);
                sCelda = NuevaCelda("OFICINA", arialNegrita, 7, true, false, true, true, 1, 1, Alineacion.Centro, PdfCelda.Color.BLACK).SetMarginTop(0.5f).SetMarginBottom(0.5f);
                sEncabezado.AddCell(sCelda);
                sCelda = NuevaCelda("CUENTA", arialNegrita, 7, true, false, true, true, 1, 4, Alineacion.Centro, PdfCelda.Color.BLACK).SetMarginTop(0.5f).SetMarginBottom(0.5f);
                sEncabezado.AddCell(sCelda);
                sCelda = NuevaCelda("D.C.", arialNegrita, 7, true, false, true, true, 1, 1, Alineacion.Centro, PdfCelda.Color.BLACK).SetMarginTop(0.5f).SetMarginBottom(0.5f);
                sEncabezado.AddCell(sCelda);
                sCelda = NuevaCelda("0011", arialNegrita, 9, true, true, true, true, 1, 1, Alineacion.Centro, PdfCelda.Color.BLACK);
                sEncabezado.AddCell(sCelda);
                sCelda = NuevaCelda(datosAcuerdo.BCOficina, arialNegrita, 7, true, true, true, true, 1, 1, Alineacion.Centro, PdfCelda.Color.BLACK);
                sEncabezado.AddCell(sCelda);
                sCelda = NuevaCelda(datosAcuerdo.BCCuenta, arialNegrita, 7, true, true, true, true, 1, 4, Alineacion.Centro, PdfCelda.Color.BLACK);
                sEncabezado.AddCell(sCelda);
                sCelda = NuevaCelda(datosAcuerdo.BCDC, arialNegrita, 7, true, true, true, true, 1, 1, Alineacion.Centro, PdfCelda.Color.BLACK);
                sEncabezado.AddCell(sCelda);
                celda.Add(sEncabezado);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 125, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
                PaginaActual++;
            }

            public void FijarPaginado(Document document, string lugar, string folio)
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                int totalPaginas = pdf.GetNumberOfPages();
                Paragraph paginas;
                switch (version)
                {
                    case 1:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"{lugar} N° {folio}").SetFont(arialNegrita).SetFontSize(7).SetFontColor(ColorConstants.RED);
                            document.ShowTextAligned(paginas, paginaTamanio.GetWidth() - 90, paginaTamanio.GetTop() - 45, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                }
            }
        }

        protected class CellRadius : CellRenderer
        {
            public CellRadius(Cell modelElement) : base(modelElement)
            {
            }

            public override IRenderer GetNextRenderer()
            {
                return new CellRadius((Cell)modelElement);
            }

            public override void Draw(DrawContext drawContext)
            {
                drawContext.GetCanvas()
                    .SetLineWidth(.5f);
                drawContext.GetCanvas()
                    .RoundRectangle(GetOccupiedAreaBBox().GetX() + 1.5f, GetOccupiedAreaBBox().GetY() + 1.5f, GetOccupiedAreaBBox().GetWidth() - 3, GetOccupiedAreaBBox().GetHeight() - 3, 3);
                drawContext.GetCanvas().Stroke();
                base.Draw(drawContext);
            }
        }
    }
}
