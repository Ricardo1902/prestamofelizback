﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Renderer;
using SOPF.Core.Model.Reportes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Policy;
using System.Web.Hosting;
using static SOPF.Core.PdfCelda;

namespace SOPF.Core.BusinessLogic.Reportes
{
    public class AcuerdoDomiciliacionVISA
    {
        private int version;
        private PdfFont arial;
        private PdfFont arialNegrita;
        private CultureInfo cultPeru = new CultureInfo("es-PE");
        private AcuerdoDomiciliacionVISAVM datosAcuerdo;
        private float fSizeNormal = 8;
        private iText.Kernel.Colors.Color negro = ColorConstants.BLACK;
        private iText.Kernel.Colors.Color rojo = ColorConstants.RED;
        public List<PosicionFirmaVM> Firmas { get; set; }
        private List<string> datosDocumento = new List<string>();
        private const int posYMax = 1008;

        public string DatosDocumento
        {
            get
            {
                try
                {
                    return string.Join("|", datosDocumento);
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public AcuerdoDomiciliacionVISA(int version)
        {
            this.version = version <= 0 ? 1 : version;
            arial = AppSettings.Arial;
            arialNegrita = AppSettings.ArialNegrita;
            Firmas = new List<PosicionFirmaVM>();
        }

        public byte[] Generar(AcuerdoDomiciliacionVISAVM acuerdoDatos)
        {
            byte[] acuerdoByte = null;
            datosAcuerdo = acuerdoDatos;
            switch (version)
            {
                case 1:
                    acuerdoByte = AcuerdoV1();
                    break;
            }
            return acuerdoByte;
        }

        private byte[] AcuerdoV1()
        {
            byte[] archivo = null;
            float punto = 0.35277f;
            string cadena = string.Format(@"\PlantillasPDF\{0}.pdf", "Acuerdo-Domiciliacion-VISA");
            string plantilla = HostingEnvironment.ApplicationPhysicalPath + cadena;
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms)))
                    {
                        Document documento = new Document(pdfArchivo, PageSize.LEGAL);
                        documento.SetMargins(25, 25, 25, 25);
                        Encabezado encabezado = new Encabezado(version, datosAcuerdo);
                        pdfArchivo.AddEventHandler(PdfDocumentEvent.START_PAGE, encabezado);

                        PdfReader originalReader = new PdfReader(plantilla);
                        PdfDocument originalDocument = new PdfDocument(originalReader);

                        originalDocument.CopyPagesTo(1, 1, pdfArchivo);
                        originalDocument.CopyPagesTo(1, 1, pdfArchivo);

                        for (int pag = 1; pag <= 2; pag++)
                        {
                            documento.ShowTextAligned(new Paragraph($"{DateTime.Now.Day:00}")
                                .SetFont(arial)
                                .SetFontSize(fSizeNormal), 211f, PosY(320), pag, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                            documento.ShowTextAligned(new Paragraph(Utils.MesFecha(DateTime.Now))
                                .SetFont(arial)
                                .SetFontSize(fSizeNormal), 248f, PosY(320), pag, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                            documento.ShowTextAligned(new Paragraph(DateTime.Now.ToString("yy"))
                                .SetFont(arial)
                                .SetFontSize(fSizeNormal), 343f, PosY(320), pag, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);

                            documento.ShowTextAligned(new Paragraph("Préstamo Feliz S.A.C.")
                                .SetFont(arial)
                                .SetFontSize(fSizeNormal), 218f, PosY(351), pag, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);

                            documento.ShowTextAligned(new Paragraph($"{datosAcuerdo.IdSolicitud}")
                                .SetFont(arial)
                                .SetFontSize(fSizeNormal), 275f, PosY(376), pag, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);

                            documento.ShowTextAligned(new Paragraph($"{datosAcuerdo.NombreCliente} {datosAcuerdo.ApPaternoCliente} {datosAcuerdo.ApMaternoCliente}")
                                .SetFont(arial)
                                .SetFontSize(fSizeNormal), 202f, PosY(414), pag, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                            documento.ShowTextAligned(new Paragraph(datosAcuerdo.DNICliente)
                                .SetFont(arial)
                                .SetFontSize(fSizeNormal), 220f, PosY(434), pag, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                            documento.ShowTextAligned(new Paragraph(datosAcuerdo.TelefonoCliente)
                                .SetFont(arial)
                                .SetFontSize(fSizeNormal), 220f, PosY(452), pag, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                            documento.ShowTextAligned(new Paragraph(datosAcuerdo.CelularCliente)
                                .SetFont(arial)
                                .SetFontSize(fSizeNormal), 320f, PosY(452), pag, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                            documento.ShowTextAligned(new Paragraph(datosAcuerdo.CorreoCliente)
                                .SetFont(arial)
                                .SetFontSize(fSizeNormal), 211f, PosY(471), pag, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }

                        datosDocumento.Add($"{datosAcuerdo.NombreCliente} {datosAcuerdo.ApPaternoCliente} {datosAcuerdo.ApMaternoCliente}");
                        datosDocumento.Add(datosAcuerdo.DNICliente);
                        datosDocumento.Add(datosAcuerdo.TelefonoCliente);
                        datosDocumento.Add(datosAcuerdo.CelularCliente);
                        datosDocumento.Add(datosAcuerdo.CorreoCliente);

                        string[][] valoresTarjeta = {
                                new string[] { "192", "509" },
                                new string[] { "205", "509" },
                                new string[] { "218", "509" },
                                new string[] { "231", "509" },

                                new string[] { "248", "509" },
                                new string[] { "261", "509" },
                                new string[] { "274", "509" },
                                new string[] { "287", "509" },

                                new string[] { "303", "509" },
                                new string[] { "316", "509" },
                                new string[] { "329", "509" },
                                new string[] { "342", "509" },

                                new string[] { "359", "509" },
                                new string[] { "372", "509" },
                                new string[] { "385", "509" },
                                new string[] { "397", "509" }
                        };

                        int f = 0;
                        datosAcuerdo.NumeroTarjetaVisa = datosAcuerdo.NumeroTarjetaVisa ?? "";
                        if (!string.IsNullOrEmpty(datosAcuerdo.NumeroTarjetaVisa))
                        {
                            foreach (string[] valor in valoresTarjeta)
                            {
                                documento.ShowTextAligned(new Paragraph(new Text(datosAcuerdo.NumeroTarjetaVisa
                                    .Substring(f, 1)))
                                    .SetFont(arial)
                                    .SetFontSize(14), float.Parse(valor[0]), PosY(Convert.ToInt32(valor[1])), encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                                f++;
                            }

                            documento.ShowTextAligned(new Paragraph(datosAcuerdo.MesExpiraTarjetaVisa.Substring(0, 1) + " " + datosAcuerdo.MesExpiraTarjetaVisa
                                .Substring(1, 1))
                                .SetFont(arial)
                                .SetFontSize(14), 295f, PosY(525), encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                            documento.ShowTextAligned(new Paragraph(datosAcuerdo.AnioExpiraTarjetaVisa.Substring(2, 1) + " " + datosAcuerdo.AnioExpiraTarjetaVisa
                                .Substring(3, 1))
                                .SetFont(arial)
                                .SetFontSize(14), 331f, PosY(525), encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }

                        documento.ShowTextAligned(new Paragraph(datosAcuerdo.DNICliente)
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal), 300f, PosY(697), encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);

                        documento.ShowTextAligned(new Paragraph(datosAcuerdo.DNICliente)
                            .SetFont(arial)
                            .SetFontSize(fSizeNormal), 300f, PosY(697), 2, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);

                        datosDocumento.Add(datosAcuerdo.NumeroTarjetaVisa);
                        datosDocumento.Add(datosAcuerdo.MesExpiraTarjetaVisa);
                        datosDocumento.Add(datosAcuerdo.AnioExpiraTarjetaVisa);

                        float altoPagina = documento.GetPdfDocument().GetDefaultPageSize().GetHeight();
                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = 1,
                            PosX = 66.9f,
                            PosY = 174.5f,
                            Alto = 20,
                            Ancho = 28
                        });

                        Firmas.Add(new PosicionFirmaVM
                        {
                            Firmante = Firmante.Cliente,
                            Pagina = 2,
                            PosX = 66.9f,
                            PosY = 174.5f,
                            Alto = 20,
                            Ancho = 28
                        });

                        //documento.ShowTextAligned(new Paragraph("x"), Firmas[0].PosX, Firmas[0].PosY, encabezado.PaginaActual, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);

                        documento.Close();
                    }
                    archivo = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
            }
            return archivo;
        }

        private static int PosY(int posY)
        {
            return posYMax - posY;
        }

        protected class Encabezado : IEventHandler
        {
            private PdfDocumentEvent docEvento;
            private decimal version;
            private string IdSolicitud;
            private float fSizeNormal = 8;
            private iText.Kernel.Colors.Color negro = ColorConstants.BLACK;
            public int PaginaActual { get; set; }

            private PdfFont arial;
            private PdfFont arialNegrita;

            public Encabezado(decimal version, AcuerdoDomiciliacionVISAVM obj)
            {
                this.version = version <= 0 ? 1 : version;
                arial = AppSettings.Arial;
                arialNegrita = AppSettings.ArialNegrita;
                this.IdSolicitud = string.IsNullOrEmpty(obj.IdSolicitud) ? string.Empty : obj.IdSolicitud.ToString();
            }

            public void HandleEvent(Event evento)
            {
                docEvento = (PdfDocumentEvent)evento;
                switch (version)
                {
                    case 1:
                        EncabezadoV1();
                        break;
                }
            }

            private void EncabezadoV1()
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                Table tEncabezado = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
                Cell celda = NuevaCelda(IdSolicitud, arial, 8, false, false, false, false, 1, 1, Alineacion.Derecha, PdfCelda.Color.RED).SetVerticalAlignment(VerticalAlignment.BOTTOM);
                tEncabezado.AddCell(celda);
                tEncabezado.SetFixedPosition(30, paginaTamanio.GetTop() - 125, paginaTamanio.GetWidth() - 60);
                Canvas canvas = new Canvas(new PdfCanvas(pagina), pdf, paginaTamanio);
                canvas.Add(tEncabezado);
                canvas.Close();
            }

            public void FijarPaginado(Document document, string lugar, string folio)
            {
                PdfDocument pdf = docEvento.GetDocument();
                PdfPage pagina = docEvento.GetPage();
                Rectangle paginaTamanio = pagina.GetPageSize();
                int totalPaginas = pdf.GetNumberOfPages();
                Paragraph paginas;
                switch (version)
                {
                    case 1:
                        for (int i = 1; i <= totalPaginas; i++)
                        {
                            paginas = new Paragraph($"{lugar} N° {folio}").SetFont(arialNegrita).SetFontSize(7).SetFontColor(ColorConstants.RED);
                            document.ShowTextAligned(paginas, paginaTamanio.GetWidth() - 90, paginaTamanio.GetTop() - 45, i, TextAlignment.LEFT, VerticalAlignment.MIDDLE, 0);
                        }
                        break;
                }
            }
        }
    }
}
