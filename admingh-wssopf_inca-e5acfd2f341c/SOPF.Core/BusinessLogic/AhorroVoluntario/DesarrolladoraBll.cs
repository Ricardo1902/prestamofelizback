#pragma warning disable 150527
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.AhorroVoluntario;
using SOPF.Core.DataAccess.AhorroVoluntario;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: DesarrolladoraBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase DesarrolladoraDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-05-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.AhorroVoluntario
{
    public static class DesarrolladoraBll
    {
        private static DesarrolladoraDal dal;

        static DesarrolladoraBll()
        {
            dal = new DesarrolladoraDal();
        }

        public static string InsertarDesarrolladora(Desarrolladora entidad, int Accion)
        {
            int Bandera = 0;
             Bandera = dal.InsertarDesarrolladora(entidad, Accion);
             string Mensaje = "";
             if (Bandera == 1)
             {
                 Mensaje =  "Desarrolladora insertada con éxito";
             }
             else
             {
                 Mensaje = "Error al insertar desarrolladora";
             }
             return Mensaje;

        }
        
        public static Desarrolladora ObtenerDesarrolladora()
        {
            return dal.ObtenerDesarrolladora();
        }
        
        public static void ActualizarDesarrolladora()
        {
            dal.ActualizarDesarrolladora();
        }

        public static void EliminarDesarrolladora()
        {
            dal.EliminarDesarrolladora();
        }
    }
}