#pragma warning disable 150807
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.AhorroVoluntario;
using SOPF.Core.DataAccess.AhorroVoluntario;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatTipoCreditoBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase CatTipoCreditoDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-07 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.AhorroVoluntario
{
    public static class CatTipoCreditoBll
    {
        private static CatTipoCreditoDal dal;

        static CatTipoCreditoBll()
        {
            dal = new CatTipoCreditoDal();
        }

        public static void InsertarCatTipoCredito(CatTipoCredito entidad)
        {
            dal.InsertarCatTipoCredito(entidad);			
        }
        
        public static List<CatTipoCredito> ObtenerCatTipoCredito(int Accion, int idTipo)
        {
            return dal.ObtenerCatTipoCredito(Accion, idTipo);
        }
        
        public static void ActualizarCatTipoCredito()
        {
            dal.ActualizarCatTipoCredito();
        }

        public static void EliminarCatTipoCredito()
        {
            dal.EliminarCatTipoCredito();
        }
    }
}