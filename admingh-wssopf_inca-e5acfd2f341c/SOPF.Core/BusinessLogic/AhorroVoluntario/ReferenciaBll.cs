#pragma warning disable 150920
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.AhorroVoluntario;
using SOPF.Core.DataAccess.AhorroVoluntario;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: ReferenciaBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase ReferenciaDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-09-20 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.AhorroVoluntario
{
    public static class ReferenciaBll
    {
        private static ReferenciaDal dal;

        static ReferenciaBll()
        {
            dal = new ReferenciaDal();
        }

        public static void InsertarReferencia(Referencia entidad)
        {
            
            int Bandera = 0;
            Bandera = dal.InsertarReferencia(entidad);
            
            
        }
        
        public static List<Referencia> ObtenerReferencia(int accion, int idcuenta)
        {
            return dal.ObtenerReferencia(accion, idcuenta);
        }
        
        public static void ActualizarReferencia()
        {
            dal.ActualizarReferencia();
        }

        public static void EliminarReferencia()
        {
            dal.EliminarReferencia();
        }
    }
}