#pragma warning disable 150408
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.AhorroVoluntario;
using SOPF.Core.DataAccess.AhorroVoluntario;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CreditoBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase CreditoDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.AhorroVoluntario
{
    public static class CreditoBll
    {
        private static CreditoDal dalCredito;
        private static ClienteDal dalCliente;

        static CreditoBll()
        {
            dalCredito = new CreditoDal();
            dalCliente = new ClienteDal();
        }

        public static DataResultAhorro InsertarCredito(Cliente cliente, Credito credito, DateTime fch_primerPago)
        {
            int idCliente = 0;
            int idcuenta = 0;
            DataResultAhorro Resultado = new DataResultAhorro();
            idCliente = dalCliente.InsertarCliente(cliente);

            if (idCliente != 0)
            {
                credito.IdCliente = idCliente;
                idcuenta = dalCredito.InsertarCredito(credito, fch_primerPago);

                if (idcuenta != 0)
                {
                    Resultado.Mensaje = "Credito Insertado con éxito";
                    Resultado.Obejto = idcuenta;
                }
                else
                {
                    Resultado.Mensaje = "Error al Insertar el crrédito";
                    Resultado.Obejto = 0;
                }
            }
            else
            {
                Resultado.Mensaje = "Error al Insertar el cliente";
                Resultado.Obejto = 0;
            }
            return Resultado;


        }
        
        public static Credito ObtenerCredito(int idcuenta)
        {
            return dalCredito.ObtenerCredito(idcuenta);
        }
        public static List<DataResultAhorro.SelCreditos> ObtenerCreditosPendientes(int Accion, int idEstatus, int Credito, int idDesarrolladora)
        {
            return dalCredito.ObtenerCreditoPendientes(Accion,idEstatus,Credito,idDesarrolladora);
        }
        
        public static void ActualizarCredito()
        {
            dalCredito.ActualizarCredito();
        }
        public static void ActualizarEstatusCredito(int idcuenta, int idestatus)
        {
            
                dalCredito.ActualizarEstatusCredito(idcuenta, idestatus);
            
        }

        public static void EliminarCredito()
        {
            dalCredito.EliminarCredito();
        }
    }
}