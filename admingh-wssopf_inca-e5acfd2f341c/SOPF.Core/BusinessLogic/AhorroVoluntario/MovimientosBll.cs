#pragma warning disable 150408
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.AhorroVoluntario;
using SOPF.Core.DataAccess.AhorroVoluntario;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: MovimientosBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase MovimientosDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.AhorroVoluntario
{
    public static class MovimientosBll
    {
        private static MovimientosDal dal;

        static MovimientosBll()
        {
            dal = new MovimientosDal();
        }

        public static void InsertarMovimientos(Movimientos entidad)
        {
            dal.InsertarMovimientos(entidad);			
        }
        
        public static Movimientos ObtenerMovimientos()
        {
            return dal.ObtenerMovimientos();
        }
        
        public static void ActualizarMovimientos()
        {
            dal.ActualizarMovimientos();
        }

        public static void EliminarMovimientos()
        {
            dal.EliminarMovimientos();
        }
    }
}