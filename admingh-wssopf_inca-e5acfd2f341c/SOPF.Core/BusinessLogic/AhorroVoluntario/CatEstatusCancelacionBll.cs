#pragma warning disable 150811
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.AhorroVoluntario;
using SOPF.Core.DataAccess.AhorroVoluntario;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatEstatusCancelacionBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase CatEstatusCancelacionDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-11 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.AhorroVoluntario
{
    public static class CatEstatusCancelacionBll
    {
        private static CatEstatusCancelacionDal dal;

        static CatEstatusCancelacionBll()
        {
            dal = new CatEstatusCancelacionDal();
        }

        public static void InsertarCatEstatusCancelacion(CatEstatusCancelacion entidad)
        {
            dal.InsertarCatEstatusCancelacion(entidad);			
        }
        
        public static List<CatEstatusCancelacion> ObtenerCatEstatusCancelacion(int Accion, int TipoEstatus)
        {
            return dal.ObtenerCatEstatusCancelacion(Accion, TipoEstatus);
        }
        
        public static void ActualizarCatEstatusCancelacion()
        {
            dal.ActualizarCatEstatusCancelacion();
        }

        public static void EliminarCatEstatusCancelacion()
        {
            dal.EliminarCatEstatusCancelacion();
        }
    }
}