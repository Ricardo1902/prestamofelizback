#pragma warning disable 150622
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.AhorroVoluntario;
using SOPF.Core.DataAccess.AhorroVoluntario;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: PromotorBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase PromotorDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-06-22 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.AhorroVoluntario
{
    public static class PromotorBll
    {
        private static PromotorDal dal;

        static PromotorBll()
        {
            dal = new PromotorDal();
        }

        public static void InsertarPromotor(Promotor entidad)
        {
            dal.InsertarPromotor(entidad);			
        }
        
        public static List<Promotor> ObtenerPromotor(int accion, int iddesarrolladora, int idpromotor, int idusuario)
        {
            return dal.ObtenerPromotor(accion,iddesarrolladora,idpromotor,idusuario);
        }
        
        public static void ActualizarPromotor()
        {
            dal.ActualizarPromotor();
        }

        public static void EliminarPromotor()
        {
            dal.EliminarPromotor();
        }
    }
}