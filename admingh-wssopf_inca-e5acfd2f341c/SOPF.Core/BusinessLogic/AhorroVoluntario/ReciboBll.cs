#pragma warning disable 150408
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.AhorroVoluntario;
using SOPF.Core.DataAccess.AhorroVoluntario;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: ReciboBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase ReciboDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.AhorroVoluntario
{
    public static class ReciboBll
    {
        private static ReciboDal dal;

        static ReciboBll()
        {
            dal = new ReciboDal();
        }

        public static void InsertarRecibo(Recibo entidad)
        {
            dal.InsertarRecibo(entidad);			
        }
        
        public static List<Recibo> ObtenerRecibos(int idcuenta)
        {
            return dal.ObtenerRecibos(idcuenta);
        }
        public static List<DataResultAhorro.SelRecibos> ObtenerRecibosAhorro(int idcuenta)
        {
            return dal.ObtenerRecibosAhorro(idcuenta);
        }
        
        public static void ActualizarRecibo()
        {
            dal.ActualizarRecibo();
        }

        public static void EliminarRecibo()
        {
            dal.EliminarRecibo();
        }
    }
}