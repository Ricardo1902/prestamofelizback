#pragma warning disable 150811
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.AhorroVoluntario;
using SOPF.Core.DataAccess.AhorroVoluntario;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatEstatusBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase CatEstatusDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-11 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.AhorroVoluntario
{
    public static class CatEstatusBll
    {
        private static CatEstatusDal dal;

        static CatEstatusBll()
        {
            dal = new CatEstatusDal();
        }

        public static void InsertarCatEstatus(CatEstatus entidad)
        {
            dal.InsertarCatEstatus(entidad);			
        }
        
        public static CatEstatus ObtenerCatEstatus()
        {
            return dal.ObtenerCatEstatus();
        }
        
        public static void ActualizarCatEstatus()
        {
            dal.ActualizarCatEstatus();
        }

        public static void EliminarCatEstatus()
        {
            dal.EliminarCatEstatus();
        }
    }
}