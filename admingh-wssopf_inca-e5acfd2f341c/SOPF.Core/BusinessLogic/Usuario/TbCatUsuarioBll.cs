#pragma warning disable 140811
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Usuario;
using SOPF.Core.DataAccess.Usuario;
using SOPF.Core.DataAccess;
using System.Reflection;
using Newtonsoft.Json;

#region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TbCatUsuarioBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TbCatUsuarioDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-11 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Usuario
{
    public static class TbCatUsuarioBll
    {
        private static TbCatUsuarioDal dal;

        static TbCatUsuarioBll()
        {
            dal = new TbCatUsuarioDal();
        }

        public static void InsertarTbCatUsuario(TBCATUsuario entidad)
        {
            dal.InsertarTbCatUsuario(entidad);			
        }
        
        public static List<TBCATUsuario> ObtenerTbCatUsuario(int Accion, int IdUsuario, string usuario)
        {
            return dal.ObtenerTbCatUsuario(Accion, IdUsuario, usuario);
        }

        public static List<TBCATUsuario> BuscarUsuario(int Accion, int IdUsuario, string usuario, int idsucursal)
        {
            return dal.BuscarUsuario(Accion, IdUsuario, usuario, idsucursal);
        }

        public static void ActualizarTbCatUsuario()
        {
            dal.ActualizarTbCatUsuario();
        }

        public static void EliminarTbCatUsuario()
        {
            dal.EliminarTbCatUsuario();
        }
        public static Resultado<bool> ValidaUsuario(string usuario, string contraseña, string IPAddress, string HostName, string MACAddress)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            respuesta = dal.validaUsuario(usuario, contraseña, IPAddress, HostName, MACAddress);
            return respuesta;
        }
        public static bool EsAdministrador(int accion, int idUsuario)
        {
            return dal.EsAdministrador(accion, idUsuario);
        }

        //ModificarSolicitud-LP
        public static List<Combo> ObtenerCATCombo()
        {
          return dal.ObtenerCATCombo();
        }

        public static bool EsTipoUsuario(string tipoUsuario, int idUsuario)
        {
            bool respuesta = false;
            try
            {
                if (tipoUsuario == null) throw new Exception("El tipo de usuario no es correcto.");
                if (idUsuario <= 0) throw new Exception("El ID de usuario no es correcto.");
                
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idTipoUsuario = 0;
                    idTipoUsuario = con.dbo_TB_CATTipoUsuario
                        .Where(c => c.TipoUsuario == tipoUsuario && c.IdEstatus == 1)
                        .Select(c => c.IdTipoUsuario)
                        .FirstOrDefault();
                    if (idTipoUsuario <= 0) throw new Exception("No se encontró ese tipo de usuario.");
                    var res = con.dbo_TB_CATUsuario
                        .Where(c => c.IdUsuario == idUsuario && c.IdTipoUsuario == idTipoUsuario)
                        .Select(c => c.IdUsuario).FirstOrDefault();
                    respuesta = res <= 0 ? false : true;
                }
            }
            catch (Exception ex)
            {
                respuesta = false;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(tipoUsuario)}, {JsonConvert.SerializeObject(idUsuario)}",
                    JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
    }
}