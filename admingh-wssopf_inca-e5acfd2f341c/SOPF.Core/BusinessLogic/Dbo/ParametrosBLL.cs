﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Poco.dbo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Dbo
{
    public static class ParametrosBLL
    {
        public static List<TB_CATParametro> Parametro(string claveParametro)
        {
            List<TB_CATParametro> parametro = null;
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    parametro = con.dbo_TB_CATParametro.Where(p => p.Parametro.Equals(claveParametro, StringComparison.CurrentCultureIgnoreCase)).ToList();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(claveParametro)}", JsonConvert.SerializeObject(ex));
            }
            return parametro;
        }

        public static DateTime? FechaCargaExpedienteV2()
        {
            DateTime? fechaCarga = null;
            List<TB_CATParametro> parametros = Parametro("CEV2");
            if (parametros != null && parametros.Count > 0)
            {
                DateTime fecha;
                if (DateTime.TryParse(parametros[0].valor, out fecha) && fecha != new DateTime())
                {
                    fechaCarga = fecha;
                }

            }
            return fechaCarga;
        }
    }
}
