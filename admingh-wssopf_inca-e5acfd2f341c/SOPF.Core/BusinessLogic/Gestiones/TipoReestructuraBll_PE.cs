﻿using System.Collections.Generic;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess.Gestiones;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public class TipoReestructuraBll_PE
    {
        private static TipoReestructuraDal_PE dal;

        static TipoReestructuraBll_PE()
        {
            dal = new TipoReestructuraDal_PE();
        }

        public static List<Combo> ObtenerCATCombo()
        {
            return dal.ObtenerCATCombo();
        }
    }
}
