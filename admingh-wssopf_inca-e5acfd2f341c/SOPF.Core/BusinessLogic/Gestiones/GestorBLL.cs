﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Response.Gestiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class GestorBLL
    {
        public static GestoresAsignadosResponse GestoresAsignados(GestoresAsignadosRequest peticion)
        {
            GestoresAsignadosResponse respuesta = new GestoresAsignadosResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<GestorVM> asignados = con.Gestiones_TB_CatGestores
                        .Where(g => g.Estatus == true)
                        .Where(g => con.Gestiones_TB_Asignacion.Any(a => a.IdGestor == g.IdGestor && a.EstatusAsignacion == true))
                        .Select(g => new GestorVM { IdGestor = g.IdGestor, Nombre = g.Nombre })
                        .ToList();

                    if (asignados != null && asignados.Count > 0)
                    {
                        respuesta.Gestores = asignados;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
    }
}
