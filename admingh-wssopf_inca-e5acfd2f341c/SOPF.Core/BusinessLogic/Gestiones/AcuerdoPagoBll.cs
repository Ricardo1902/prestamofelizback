using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Xml.Linq;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class AcuerdoPagoBll
    {

        #region GET
        public static ObtenerDatosClienteAcuerdoResponse ObtenerDatosClienteAcuerdo(ObtenerDatosClienteAcuerdoRequest peticion)
        {
            ObtenerDatosClienteAcuerdoResponse respuesta = new ObtenerDatosClienteAcuerdoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es válida");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Gestiones_SP_AcuerdoClienteCON(peticion);
                }
            }

            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        public static ObtenerAcuerdoPagoConDetalleResponse ObtenerAcuerdoPagoConDetalle(ObtenerAcuerdoPagoRequest peticion)
        {

            ObtenerAcuerdoPagoConDetalleResponse respuesta = new ObtenerAcuerdoPagoConDetalleResponse();
            try
            {
                var respuestaAcuerdo = ObtenerAcuerdoPago(peticion);
                if (respuestaAcuerdo != null && respuestaAcuerdo.Acuerdo != null)
                {
                    peticion.IdAcuerdoPago = respuestaAcuerdo.Acuerdo.IdAcuerdoPago;
                    var respuestaDetalle = ObtenerAcuerdoPagoDetalle(peticion.Convertir<ObtenerAcuerdoPagoDetalleRequest>());
                    if (respuestaDetalle != null && respuestaDetalle.Datos != null && respuestaDetalle.Datos.Count > 0)
                    {
                        respuesta.AcuerdoPago = respuestaAcuerdo.Acuerdo;
                        respuesta.Detalle = respuestaDetalle.Datos;
                    }
                }
            }

            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        public static ObtenerAcuerdoPagoConDetalleResponse ObtenerAcuerdoPagoConDetallePorSolicitud(ObtenerAcuerdoPagoRequest peticion)
        {

            ObtenerAcuerdoPagoConDetalleResponse respuesta = new ObtenerAcuerdoPagoConDetalleResponse();
            try
            {
                var respuestaAcuerdo = ObtenerAcuerdoPagoSolicitud(peticion);
                if (respuestaAcuerdo != null && respuestaAcuerdo.Acuerdo != null)
                {
                    peticion.IdAcuerdoPago = respuestaAcuerdo.Acuerdo.IdAcuerdoPago;
                    respuesta.AcuerdoPago = respuestaAcuerdo.Acuerdo;
                    var respuestaDetalle = ObtenerAcuerdoPagoDetalle(peticion.Convertir<ObtenerAcuerdoPagoDetalleRequest>());
                    if (respuestaDetalle != null && respuestaDetalle.Datos != null && respuestaDetalle.Datos.Count > 0)
                    {
                        respuesta.Detalle = respuestaDetalle.Datos;
                    }
                }
            }

            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        public static ObtenerAcuerdoPagoResponse ObtenerAcuerdoPago(ObtenerAcuerdoPagoRequest peticion)
        {
            ObtenerAcuerdoPagoResponse respuesta = new ObtenerAcuerdoPagoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdAcuerdoPago <= 0) throw new ValidacionExcepcion("La clave del plan de acuerdos no es válida");

                peticion.Accion = 1;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respuestaAcuerdo = con.Gestiones_SP_AcuerdoPagoCON(peticion);
                    if (respuestaAcuerdo != null && respuestaAcuerdo.Acuerdos != null && respuestaAcuerdo.Acuerdos.Count > 0)
                    {
                        respuesta.Acuerdo = respuestaAcuerdo.Acuerdos[0];
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        public static ObtenerAcuerdosPagoResponse ObtenerAcuerdosPago(ObtenerAcuerdoPagoRequest peticion)
        {
            ObtenerAcuerdosPagoResponse respuesta = new ObtenerAcuerdosPagoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");

                peticion.Accion = 2;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respuestaAcuerdo = con.Gestiones_SP_AcuerdoPagoCON(peticion);
                    if (respuestaAcuerdo != null && respuestaAcuerdo.Acuerdos != null && respuestaAcuerdo.Acuerdos.Count > 0)
                    {
                        return respuestaAcuerdo;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        public static ObtenerAcuerdoPagoResponse ObtenerAcuerdoPagoSolicitud(ObtenerAcuerdoPagoRequest peticion)
        {
            ObtenerAcuerdoPagoResponse respuesta = new ObtenerAcuerdoPagoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es válida");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respuestaAcuerdo = con.Gestiones_SP_AcuerdoPagoCON(peticion);
                    if (respuestaAcuerdo != null && respuestaAcuerdo.Acuerdos != null && respuestaAcuerdo.Acuerdos.Count > 0)
                    {
                        respuesta.Acuerdo = respuestaAcuerdo.Acuerdos[0];
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        public static ObtenerAcuerdoPagoDetalleResponse ObtenerAcuerdoPagoDetalle(ObtenerAcuerdoPagoDetalleRequest peticion)
        {
            ObtenerAcuerdoPagoDetalleResponse respuesta = new ObtenerAcuerdoPagoDetalleResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdAcuerdoPago <= 0) throw new ValidacionExcepcion("La clave del plan de acuerdos no es válida");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respuestaDetalle = con.Gestiones_SP_AcuerdoPagoDetalleCON(peticion);
                    if (respuestaDetalle != null && respuestaDetalle.Datos != null && respuestaDetalle.Datos.Count > 0)
                    {
                        respuesta.Datos = respuestaDetalle.Datos;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST
        public static AltaAcuerdoPagoResponse AltaAcuerdoPago(AltaAcuerdoPagoRequest peticion)
        {
            AltaAcuerdoPagoResponse respuesta = new AltaAcuerdoPagoResponse();
            using (CreditOrigContext con = new CreditOrigContext())
            {
                using (DbContextTransaction tran = con.Database.BeginTransaction())
                {
                    try
                    {
                        if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                        if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válida");
                        if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es válida");
                        if (peticion.Acuerdos == null || peticion.Acuerdos.Count <= 1) throw new ValidacionExcepcion("Es necesario ingresar al menos 2 acuerdos de pago para un plan de acuerdos");

                        // Se busca un tipo de gestion para acuerdo de pago e informacion de la cuenta y gestor
                        var tipoGestionAcuerdos = con.Gestiones_TipoGestion.SingleOrDefault(tg => tg.Tipo == "Acuerdo de Pago");
                        var cuenta = con.dbo_TB_Creditos.SingleOrDefault(c => c.IdSolicitud == peticion.IdSolicitud);
                        var gestor = con.Gestiones_TB_CatGestores.SingleOrDefault(g => g.Usuarioid == peticion.IdUsuario);

                        if (peticion.esNuevo)
                        {
                            // Se valida si la cuenta tiene una promesa/compromiso de pago. si existe un registro activo, no es posible generar el acuerdo de pagos
                            var respuestaPromesa = (from pp in con.Gestiones_TB_PromesaPago
                                                    where pp.idCuenta == cuenta.IdCuenta && pp.Estatus == 1 && pp.Cumplio == 26
                                                    select pp).SingleOrDefault();

                            if (respuestaPromesa != null && respuestaPromesa.idPromesa > 0) throw new ValidacionExcepcion("No es posible dar de alta un plan de acuerdos si la solicitud cuenta con un compromiso de pago vigente.");


                            AltaAcuerdoPagoProRequest peticionPRO = peticion.Convertir<AltaAcuerdoPagoProRequest>();
                            XElement acuerdos = new XElement("Acuerdos");
                            foreach (var a in peticion.Acuerdos)
                            {
                                var tempAcuerdo = new XElement("Acuerdo");
                                tempAcuerdo.Add(new XElement("IdAcuerdoPago", a.IdAcuerdoPago));
                                tempAcuerdo.Add(new XElement("Monto", a.Monto));
                                tempAcuerdo.Add(new XElement("GastoAdministrativo", a.GastoAdministrativo));
                                tempAcuerdo.Add(new XElement("MontoTotal", a.MontoTotal));
                                tempAcuerdo.Add(new XElement("FechaAcuerdo", a.FechaAcuerdo));
                                acuerdos.Add(tempAcuerdo);
                            }
                            peticionPRO.XmlAcuerdos = acuerdos;

                            respuesta = con.Gestiones_SP_AcuerdoPagoPRO(peticionPRO);

                            // Se crea una gestion con el proceso correspondiente
                            if (respuesta != null && !respuesta.Error)
                            {
                                if (tipoGestionAcuerdos != null && tipoGestionAcuerdos.IdTipoGestion > 0)
                                {
                                    // Se busca el resultado de gestion correspondiente para alta de acuerdo de pago
                                    var resGestionAltaAcuerdos = con.Gestiones_TB_CATResultadoGestion.SingleOrDefault(rg => rg.TipoGestion == tipoGestionAcuerdos.IdTipoGestion && rg.Descripcion == "Alta Plan Acuerdos");
                                    if (resGestionAltaAcuerdos != null && resGestionAltaAcuerdos.IdResultado > 0)
                                    {
                                        if (cuenta != null && cuenta.IdCuenta > 0 && gestor != null && gestor.IdGestor > 0)
                                        {
                                            var gestion = new Entities.Gestiones.Gestion
                                            {
                                                IdCuenta = cuenta.IdCuenta,
                                                IdGestor = gestor.IdGestor,
                                                IdResultadoGestion = resGestionAltaAcuerdos.IdResultado,
                                                IdTipoGestion = tipoGestionAcuerdos.IdTipoGestion
                                            };
                                            GestionBll.InsertarGestion(gestion);
                                        }
                                    }
                                }
                            }

                            tran.Commit();
                        }
                        else
                        {
                            var acuerdoPagoDB = (from ap in con.Gestiones_TB_AcuerdoPago
                                                 where ap.IdSolicitud == peticion.IdSolicitud && ap.Activo == true
                                                 select ap).FirstOrDefault();

                            if (acuerdoPagoDB == null || acuerdoPagoDB.IdAcuerdoPago <= 0) throw new ValidacionExcepcion("Error: No fue posible encontrar el registro de plan de acuerdo correspondiente para aplicar cambios");

                            var acuerdosDB = con.Gestiones_SP_AcuerdoPagoDetalleCON(new ObtenerAcuerdoPagoDetalleRequest { Accion = 0, IdAcuerdoPago = acuerdoPagoDB.IdAcuerdoPago });
                            if (acuerdosDB == null || acuerdosDB.Datos == null) throw new ValidacionExcepcion("Error: No fue posible obtener los acuerdos de pago para el plan correspondiente");

                            foreach (var acuerdoDB in acuerdosDB.Datos)
                            {
                                AcuerdoPagoDetalleVM tempAcuerdoPeticion = null;
                                tempAcuerdoPeticion = peticion.Acuerdos.Find(r => r.IdAcuerdoPagoDetalle == acuerdoDB.IdAcuerdoPagoDetalle);
                                if (tempAcuerdoPeticion == null)
                                {
                                    var respuestaEliminar = con.Gestiones_SP_AcuerdoPagoDetalleBAJ(new CancelarAcuerdoPagoDetalleRequest { Accion = 0, IdAcuerdoPagoDetalle = acuerdoDB.IdAcuerdoPagoDetalle });
                                }
                            }

                            foreach (var accuerdoPeticion in peticion.Acuerdos)
                            {
                                if (accuerdoPeticion.IdAcuerdoPagoDetalle == 0)
                                {
                                    var nuevoAcuerdo = accuerdoPeticion.Convertir<TB_AcuerdoPagoDetalle>();
                                    nuevoAcuerdo.IdAcuerdoPago = acuerdoPagoDB.IdAcuerdoPago;
                                    nuevoAcuerdo.Activo = true;
                                    nuevoAcuerdo.IdEstatus = 1;
                                    con.Gestiones_TB_AcuerdoPagoDetalle.Add(nuevoAcuerdo);
                                }
                            }

                            con.SaveChanges();


                            acuerdoPagoDB.TotalRegistros = (from apd in con.Gestiones_TB_AcuerdoPagoDetalle
                                                            where apd.IdAcuerdoPago == acuerdoPagoDB.IdAcuerdoPago
                                                            && apd.Activo == true
                                                            select apd).Count();
                            con.SaveChanges();

                            respuesta.AcuerdoPago = acuerdoPagoDB.Convertir<AcuerdoPagoVM>();
                            respuesta.Error = false;
                            respuesta.MensajeOperacion = "Los acuerdos se registraron con exito";

                            // Se crea una gestion con el proceso correspondiente
                            if (tipoGestionAcuerdos != null && tipoGestionAcuerdos.IdTipoGestion > 0)
                            {
                                // Se busca el resultado de gestion correspondiente para actualizacion de acuerdo de pago
                                var resGestionActAcuerdos = con.Gestiones_TB_CATResultadoGestion.SingleOrDefault(rg => rg.TipoGestion == tipoGestionAcuerdos.IdTipoGestion && rg.Descripcion == "Actualizacion Plan Acuerdos");
                                if (resGestionActAcuerdos != null && resGestionActAcuerdos.IdResultado > 0)
                                {
                                    if (cuenta != null && cuenta.IdCuenta > 0 && gestor != null && gestor.IdGestor > 0)
                                    {
                                        var gestion = new Entities.Gestiones.Gestion
                                        {
                                            IdCuenta = cuenta.IdCuenta,
                                            IdGestor = gestor.IdGestor,
                                            IdResultadoGestion = resGestionActAcuerdos.IdResultado,
                                            IdTipoGestion = tipoGestionAcuerdos.IdTipoGestion
                                        };
                                        GestionBll.InsertarGestion(gestion);
                                    }
                                }
                            }

                            tran.Commit();
                        }

                    }

                    catch (ValidacionExcepcion vex)
                    {
                        tran.Rollback();
                        respuesta.Error = true;
                        respuesta.MensajeOperacion = vex.Message;
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        respuesta.Error = true;
                        respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                        Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                           $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
                    }
                }
            }
            return respuesta;
        }
        public static CancelarAcuerdoPagoResponse CancelarAcuerdoPago(CancelarAcuerdoPagoRequest peticion)
        {
            if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
            if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es válida");
            if (peticion.IdAcuerdoPago <= 0) throw new ValidacionExcepcion("La clave del plan de acuerdos no es válida");

            CancelarAcuerdoPagoResponse respuesta = new CancelarAcuerdoPagoResponse();
            using (CreditOrigContext con = new CreditOrigContext())
            {
                using (DbContextTransaction tran = con.Database.BeginTransaction())
                {

                    try
                    {
                        var acuerdoPago = con.Gestiones_TB_AcuerdoPago.Find(peticion.IdAcuerdoPago);
                        if (acuerdoPago == null || acuerdoPago.IdAcuerdoPago <= 0) throw new ValidacionExcepcion("No fue posible identificar el plan de acuerdos correspondiente");
                        if (acuerdoPago.IdSolicitud != peticion.IdSolicitud) throw new ValidacionExcepcion("La peticion no es válida, el plan de acuerdos no pertenece a dicha solicitud");

                        respuesta = con.Gestiones_SP_AcuerdoPagoBAJ(peticion);


                        // Se crea una gestion con el proceso correspondiente
                        if (respuesta != null && !respuesta.Error)
                        {
                            var tipoGestionAcuerdos = con.Gestiones_TipoGestion.SingleOrDefault(tg => tg.Tipo == "Acuerdo de Pago");
                            if (tipoGestionAcuerdos != null && tipoGestionAcuerdos.IdTipoGestion > 0)
                            {
                                // Se busca el resultado de gestion correspondiente para alta de acuerdo de pago
                                var resGestionBajaAcuerdos = con.Gestiones_TB_CATResultadoGestion.SingleOrDefault(rg => rg.TipoGestion == tipoGestionAcuerdos.IdTipoGestion && rg.Descripcion == "Cancelacion Plan Acuerdos");
                                var cuenta = con.dbo_TB_Creditos.SingleOrDefault(c => c.IdSolicitud == peticion.IdSolicitud);
                                var gestor = con.Gestiones_TB_CatGestores.SingleOrDefault(g => g.Usuarioid == peticion.IdUsuario);
                                if (resGestionBajaAcuerdos != null && resGestionBajaAcuerdos.IdResultado > 0)
                                {
                                    if (cuenta != null && cuenta.IdCuenta > 0 && gestor != null && gestor.IdGestor > 0)
                                    {
                                        var gestion = new Entities.Gestiones.Gestion
                                        {
                                            IdCuenta = cuenta.IdCuenta,
                                            IdGestor = gestor.IdGestor,
                                            IdResultadoGestion = resGestionBajaAcuerdos.IdResultado,
                                            IdTipoGestion = tipoGestionAcuerdos.IdTipoGestion
                                        };
                                        GestionBll.InsertarGestion(gestion);
                                    }
                                }
                            }
                        }

                        tran.Commit();
                    }

                    catch (ValidacionExcepcion vex)
                    {
                        tran.Rollback();
                        respuesta.Error = true;
                        respuesta.MensajeOperacion = vex.Message;
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        respuesta.Error = true;
                        respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                        Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                           $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
                    }
                }
            }
            return respuesta;
        }
        #endregion
    }
}
