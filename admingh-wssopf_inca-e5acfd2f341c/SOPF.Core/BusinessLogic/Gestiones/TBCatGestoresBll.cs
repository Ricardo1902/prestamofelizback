#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Gestiones;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;

#region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TBCatGestoresBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCatGestoresDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class TBCatGestoresBll
    {
        private static TBCatGestoresDal dal;

        static TBCatGestoresBll()
        {
            dal = new TBCatGestoresDal();
        }

        public static void InsertarTBCatGestores(int idTipo, int idUsuarioGes, int Usuario)
        {
            dal.InsertarTBCatGestores(idTipo, idUsuarioGes, Usuario);
        }

        public static List<TBCatGestores> ObtenerTBCatGestores(int accion, int idGestor, int idTipo, int Usuario)
        {
            return dal.ObtenerTBCatGestores(accion, idGestor, idTipo, Usuario);
        }

        public static void ActualizarTBCatGestores()
        {
            dal.ActualizarTBCatGestores();
        }

        public static void EliminarTBCatGestores(int idUsuarioGes, int Usuario)
        {
            dal.EliminarTBCatGestores(idUsuarioGes, Usuario);
        }

        public static GestoresResponse Gestores(GestoresRequest peticion)
        {
            GestoresResponse respuesta = new GestoresResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<GestorVM> gestores = (from g in con.Gestiones_TB_CatGestores
                                               join tg in con.Gestiones_TB_CatTipoGestor on g.IdTipo equals tg.IdTipoGestor
                                               join u in con.dbo_TB_CATUsuario on g.Usuarioid equals u.IdUsuario
                                               where (peticion.Activo == null || u.IdEstatus == (peticion.Activo == true ? 1 : 0))
                                                    && (peticion.Activo == null || g.Estatus == peticion.Activo)
                                                   && (string.IsNullOrEmpty(peticion.Tipo) || peticion.Tipo == peticion.Tipo)
                                               select new GestorVM
                                               {
                                                   IdGestor = g.IdGestor,
                                                   Nombre = g.Nombre,
                                                   Usuario = u.Usuario,
                                                   TipoGestor = tg.Descripcion
                                               }).ToList();
                    if (gestores != null && gestores.Count > 0)
                    {
                        respuesta.Gestores = gestores;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
    }
}