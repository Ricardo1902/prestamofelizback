﻿using Newtonsoft.Json;
using OfficeOpenXml;
using SOPF.Core.BusinessLogic.Catalogo;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Dbo;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Response.Gestiones;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class AsignacionesBLL
    {
        #region GET
        public static CargaAsignacionResponse CargaAsignacion(CargaAsignacionRequest peticion)
        {
            CargaAsignacionResponse respuesta = new CargaAsignacionResponse();
            if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
            if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido");
            if (peticion.IdCargaAsignacion <= 0) throw new ValidacionExcepcion("La clave de la carga no es válido");
            respuesta.CargaAsignacion = CargaAsignacion(peticion.IdCargaAsignacion);
            return respuesta;
        }
        public static CargaAsignacionesResponse CargaAsignaciones(CargaAsignacionesRequest peticion)
        {
            CargaAsignacionesResponse respuesta = new CargaAsignacionesResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Gestiones_SP_CargaAsignacionCON(peticion.Convertir<CargaAsignacionConRequest>());
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static CargaAsignacionDetallesResponse CargaAsignacionDetalles(CargaAsignacionDetallesRequest peticion)
        {
            CargaAsignacionDetallesResponse respuesta = new CargaAsignacionDetallesResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido");
                if (peticion.IdCargaAsignacion <= 0) throw new ValidacionExcepcion("La clave de la carga no es váida");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Gestiones_SP_CargaAsignacionDetalleCON(peticion.Convertir<CargaAsignacionDetalleConRequest>());
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST
        public static CargarAsignacionResponse CargarAsignacion(CargarAsignacionRequest peticion)
        {
            CargarAsignacionResponse respuesta = new CargarAsignacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido");
                if (peticion.Contenido.Length <= 0 || peticion.Tamanio <= 0) throw new ValidacionExcepcion("El contenido del archivo no es válido");
                if (string.IsNullOrEmpty(peticion.NombreArchivo)) throw new ValidacionExcepcion("El nombre del archivo no es válido");

                byte[] contenido = Convert.FromBase64String(peticion.Contenido);
                if (contenido == null || contenido.Length <= 0) throw new ValidacionExcepcion("El contenido del archivo no es válido");

                #region Proceso del archivo
                MemoryStream ms = new MemoryStream(contenido);
                ExcelPackage excel = new ExcelPackage(ms);
                if (excel == null) throw new ValidacionExcepcion("No fue posible procesar el contenido del archivo.");
                int registroMinimo = 0;
                int registroMaximo = 0;
                int valorEntero = 0;

                ExcelWorksheet hoja1 = excel.Workbook.Worksheets.First();
                if (hoja1 == null) throw new ValidacionExcepcion("No fue posible procesar el contenido del archivo.");
                registroMinimo = hoja1.Dimension.Start.Row;
                registroMaximo = hoja1.Dimension.End.Row;

                //Se considera excel con titulos
                registroMinimo++;
                XElement cuentas = new XElement("Cuentas");
                cuentas.Add(
                    new XElement("NombreArchivo", peticion.NombreArchivo));
                if (!string.IsNullOrEmpty(peticion.Actividad))
                {
                    cuentas.Add(new XElement("Actividad", peticion.Actividad));
                }

                for (int i = registroMinimo; i <= registroMaximo; i++)
                {
                    int.TryParse(hoja1.Cells[i, 1].Text.Trim(), out valorEntero);
                    XElement cuenta = new XElement("Cuenta",
                        new XElement("IdSolicitud", valorEntero));

                    if (!string.IsNullOrEmpty(hoja1.Cells[i, 2].Text.Trim()))
                    {
                        cuenta.Add(new XElement("Gestor", hoja1.Cells[i, 2].Text.Trim()));
                    }

                    if (!string.IsNullOrEmpty(hoja1.Cells[i, 3].Text.Trim()))
                    {
                        cuenta.Add(new XElement("Actividad", hoja1.Cells[i, 3].Text.Trim()));
                    }
                    else if (!string.IsNullOrEmpty(peticion.Actividad))
                    {
                        cuenta.Add(new XElement("Actividad", peticion.Actividad));
                    }

                    cuentas.Add(cuenta);
                }

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Gestiones_SP_CargarAsignacionPRO(new CargarAsignacionProRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        XmlCarga = cuentas
                    });

                    int idTipoEstatus = 0;
                    TipoEstatusVM tipoEstatus = TipoEstatusBLL.TipoEstatus("Carga Asignaciones");
                    if (tipoEstatus != null) idTipoEstatus = tipoEstatus.IdTipoEstatus;

                    if (!respuesta.Error && respuesta.CargaAsignacion != null && respuesta.CargaAsignacion.IdCargaAsignacion > 0)
                    {
                        respuesta.CargaAsignacion = CargaAsignacion(respuesta.CargaAsignacion.IdCargaAsignacion);
                    }
                }
                #endregion

            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ActualizarCargaAsignacionResponse ActualizarCargaAsignacion(ActualizarCargaAsignacionRequest peticion)
        {
            int[] accionConAsignaciones = new int[] { 1, 2 };
            ActualizarCargaAsignacionResponse respuesta = new ActualizarCargaAsignacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido");
                if (peticion.Accion < 0) throw new ValidacionExcepcion("La acción a realizar no es correcta");
                if (peticion.IdCargaAsignacion < 0) throw new ValidacionExcepcion("La clave de la asignación válida");
                if (accionConAsignaciones.Contains(peticion.Accion) && (peticion.Asignaciones == null || peticion.Asignaciones.Count == 0))
                    throw new ValidacionExcepcion("Proporcione al menos un registro para realizar la acción");

                XElement asignaciones = new XElement("Cuentas");
                if (accionConAsignaciones.Contains(peticion.Accion) && peticion.Asignaciones != null)
                {
                    foreach (CargaAsignacionDetalleVM c in peticion.Asignaciones)
                    {
                        XElement cuenta = new XElement("Cuenta",
                         new XElement("IdCargaAsignacionDetalle", c.IdCargaAsignacionDetalle));

                        if (c.IdSolicitud > 0)
                        {
                            cuenta.Add(new XElement("IdSolicitud", c.IdSolicitud));
                        }

                        if (!string.IsNullOrEmpty(c.Gestor))
                        {
                            cuenta.Add(new XElement("Gestor", c.Gestor.Trim()));
                        }

                        if (!string.IsNullOrEmpty(c.Actividad))
                        {
                            cuenta.Add(new XElement("Actividad", c.Actividad.Trim()));
                        }

                        if (c.IdGestor > 0)
                        {
                            cuenta.Add(new XElement("IdGestor", c.IdGestor));
                        }

                        asignaciones.Add(cuenta);
                    }
                }

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Gestiones_SP_CargarAsignacionACT(new CargarAsignacionActRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        Accion = peticion.Accion,
                        IdCargaAsignacion = peticion.IdCargaAsignacion,
                        Error = peticion.Error,
                        Actividad = peticion.Actividad,
                        XmlCarga = asignaciones
                    });
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static AsignarCargaAsignacionResponse AsignarCargaAsignacion(AsignarCargaAsignacionRequest peticion)
        {
            AsignarCargaAsignacionResponse respuesta = new AsignarCargaAsignacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido");
                if (peticion.IdCargaAsignacion <= 0) throw new ValidacionExcepcion("La clave de la carga no es válido");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Gestiones_SP_CargaAsignacionAsignarPRO(new CargaAsignacionAsignarProRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        IdCargaAsignacion = peticion.IdCargaAsignacion
                    });
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region Metodos Privados
        private static CargaAsignacionVM CargaAsignacion(int idCargaAsignacion)
        {
            CargaAsignacionVM cargaAsignacion = null;
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idTipoEstatus = 0;
                    TipoEstatusVM tipoEstatus = TipoEstatusBLL.TipoEstatus("Carga Asignaciones");
                    if (tipoEstatus != null) idTipoEstatus = tipoEstatus.IdTipoEstatus;

                    var asignacion = (from ca in con.Gestiones_TB_CargaAsignacion
                                      join es in con.dbo_TB_CATEstatus on new { ca.IdEstatus, IdTipoEstatus = idTipoEstatus } equals new { es.IdEstatus, es.IdTipoEstatus }
                                      where ca.IdCargaAsignacion == idCargaAsignacion
                                      select new
                                      {
                                          ca.IdCargaAsignacion,
                                          ca.NombreArchivo,
                                          ca.TotalRegistros,
                                          ca.Actividad,
                                          ca.IdUsuarioRegistro,
                                          ca.FechaRegistro,
                                          ca.IdEstatus,
                                          Estatus = es.Descripcion
                                      }).FirstOrDefault();
                    cargaAsignacion = asignacion.Convertir<CargaAsignacionVM>();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(idCargaAsignacion)}", JsonConvert.SerializeObject(ex));
            }
            return cargaAsignacion;
        }
        #endregion

    }
}
