#pragma warning disable 150616
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.DataAccess.Gestiones;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBDevolverLlamadaBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBDevolverLlamadaDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-06-16 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class TBDevolverLlamadaBll
    {
        private static TBDevolverLlamadaDal dal;

        static TBDevolverLlamadaBll()
        {
            dal = new TBDevolverLlamadaDal();
        }

        public static void InsertarTBDevolverLlamada(int IdGestion, int IdCuenta, DateTime FchDevLlamada, int IdCliente, int IdGestor)
        {
            dal.InsertarTBDevolverLlamada(IdGestion, IdCuenta, FchDevLlamada, IdCliente, IdGestor);			
        }
        
       // public static TBDevolverLlamada ObtenerTBDevolverLlamada()
        //{
          //  return dal.ObtenerTBDevolverLlamada();
        //}

        public static List<TBDevolverLlamada> ObtenerTBDevolverLlamada(int idcuenta)
        {
            return dal.ObtenerTBDevolverLlamada(idcuenta);
        }
        
        public static void ActualizarTBDevolverLlamada()
        {
            dal.ActualizarTBDevolverLlamada();
        }

        public static void EliminarTBDevolverLlamada()
        {
            dal.EliminarTBDevolverLlamada();
        }
    }
}