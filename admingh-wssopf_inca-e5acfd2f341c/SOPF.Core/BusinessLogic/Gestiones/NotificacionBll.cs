﻿using SOPF.Core.DataAccess.Gestiones;
using SOPF.Core.Entities;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class NotificacionBll
    {
        static NotificacionDal dal;
        static NotificacionBll()
        {
            dal = new NotificacionDal(new DataAccess.CreditOrigContext());
        }

        public static List<NotificacionVM> ObtenerNotificacionesPorTipo(int IdTipoNotificacion)
        {
            return dal.ObtenerNotificacionesPorTipo(IdTipoNotificacion);
        }
        public static SolicitudNotificacionVM ObtenerNotificacionActivaSolicitud(int idSolicitud, int idNotificacion, int idDestinoNotificacion)
        {
            return dal.ObtenerNotificacionActivaSolicitud(idSolicitud, idNotificacion, idDestinoNotificacion);
        }
        public static Resultado<bool> ActualizarSolicitudNotificacion(ActualizarSolicitudNotificacionRequest peticion)
        {
            return dal.ActualizarSolicitudNotificacion(peticion);
        }
    }
}
