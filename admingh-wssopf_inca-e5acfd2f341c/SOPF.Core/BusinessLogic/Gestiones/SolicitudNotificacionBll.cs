﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Gestiones;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Model.Response.Sistema;
using SOPF.Core.Model.Sistema;
using SOPF.Core.Poco.dbo;
using SOPF.Core.Poco.Gestiones;
using SOPF.Core.Poco.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class SolicitudNotificacionBLL
    {
        private static SolicitudNotificacionDal dal;

        static SolicitudNotificacionBLL()
        {
            dal = new SolicitudNotificacionDal();
        }

        public static ObtenerEstadoNotificacionResponse ObtenerNotificacion(ObtenerEstadoNotificacionRequest peticion)
        {
            ObtenerEstadoNotificacionResponse respuesta = new ObtenerEstadoNotificacionResponse();
            switch (peticion.Accion)
            {
                case "GET_ESTADO_NOTIFICACION_POR_SOLICITUD":
                    try
                    {
                        using (CreditOrigContext con = new CreditOrigContext())
                        {
                            var respNotifsOpciones = (from cn in con.Gestiones_TB_CATNotificaciones
                                                      join ctn in con.Gestiones_TB_CATTipoNotificaciones on cn.TipoNotificacionId equals ctn.IdTipoNotificacion
                                                      where ctn.TipoNotificacion == "Cobranza"
                                                      && cn.Actual
                                                      orderby cn.Orden
                                                      select new Sel_NotificacionResult
                                                      {
                                                          IdNotificacion = cn.IdNotificacion,
                                                          Notificacion = cn.Notificacion,
                                                          NombreArchivo = cn.NombreArchivo,
                                                          Clave = cn.Clave,
                                                          Orden = cn.Orden
                                                      }).ToList();
                            var respNotif = (from sn in con.Gestiones_SolicitudNotificaciones
                                             join cn in con.Gestiones_TB_CATNotificaciones on sn.NotificacionId equals cn.IdNotificacion
                                             join ctn in con.Gestiones_TB_CATTipoNotificaciones on cn.TipoNotificacionId equals ctn.IdTipoNotificacion
                                             where sn.SolicitudId == peticion.IdSolicitud
                                                 && sn.Actual == true
                                                 && ctn.TipoNotificacion == "Cobranza"
                                             orderby cn.Orden descending, sn.FechaRegistro descending
                                             select new Sel_NotificacionResult
                                             {
                                                 IdNotificacion = cn.IdNotificacion,
                                                 Notificacion = cn.Notificacion,
                                                 NombreArchivo = cn.NombreArchivo,
                                                 Clave = cn.Clave,
                                                 Orden = cn.Orden,
                                                 Destino = sn.DestinoId,
                                                 FechaEscaneo = sn.FechaEscaneo,
                                                 Correo = sn.Correo,
                                                 EsCourier = sn.EsCourier
                                             })
                                           .ToList();
                            if (respNotif.Count > 0)
                            {
                                Sel_NotificacionResult not = respNotif.FirstOrDefault();
                                if (not.FechaEscaneo != null)
                                {
                                    if (respNotifsOpciones != null)
                                    {
                                        int notifMaxOrden = respNotifsOpciones.Max(r => r.Orden);
                                        if (not.Orden == notifMaxOrden)
                                            respuesta.Resultado.Notificacion = not;
                                        else
                                            respuesta.Resultado.Notificacion = respNotifsOpciones.Where(r => r.Orden == (not.Orden + 1)).FirstOrDefault();
                                    }
                                }
                                else
                                {
                                    respuesta.Resultado.Notificacion = not;
                                }
                            }
                            else
                            {
                                respuesta.Resultado.Notificacion = respNotifsOpciones.FirstOrDefault();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //TODO: Implementar log de errores.
                        respuesta.MensajeOperacion = ex.Message;
                        respuesta.Error = true;
                    }
                    break;
                case "GET_DATOS_NOTIFICACION_POR_SOLICITUD":
                    try
                    {
                        using (CreditOrigContext con = new CreditOrigContext())
                        {
                            var respNotif = (from sn in con.Gestiones_SolicitudNotificaciones
                                             join cn in con.Gestiones_TB_CATNotificaciones on sn.NotificacionId equals cn.IdNotificacion
                                             join snd in con.Gestiones_SolicitudNotificacionDetalle on sn.IdSolicitudNotificacion equals snd.SolicitudNotificacionId
                                             where sn.SolicitudId == peticion.IdSolicitud
                                                 && sn.Actual == true
                                                 && sn.NotificacionId == peticion.IdNotificacion
                                                 && sn.DestinoId == peticion.IdDestino
                                             select new Sel_NotificacionResult
                                             {
                                                 IdNotificacion = cn.IdNotificacion,
                                                 Notificacion = cn.Notificacion,
                                                 NombreArchivo = cn.NombreArchivo,
                                                 Clave = cn.Clave,
                                                 Orden = cn.Orden,
                                                 FechaHoraCitacion = snd.FechaHoraCitacion,
                                                 DescuentoDeuda = snd.DescuentoDeuda,
                                                 FechaVigencia = snd.FechaVigencia,
                                                 Destino = sn.DestinoId,
                                                 Correo = sn.Correo,
                                                 EsCourier = sn.EsCourier
                                             })
                                                .ToList();
                            if (respNotif != null && respNotif.Count > 0)
                            {
                                respuesta.Resultado.Notificacion = respNotif.FirstOrDefault();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //TODO: Implementar log de errores.
                        respuesta.Error = true;
                    }
                    break;
                default:
                    break;
            }
            return respuesta;
        }

        public static ObtenerNotificacionesResponse ObtenerNotificaciones()
        {
            ObtenerNotificacionesResponse respuesta = new ObtenerNotificacionesResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respNotifs = (from cn in con.Gestiones_TB_CATNotificaciones
                                      join ctn in con.Gestiones_TB_CATTipoNotificaciones on cn.TipoNotificacionId equals ctn.IdTipoNotificacion
                                      where ctn.TipoNotificacion == "Cobranza"
                                      orderby cn.Orden
                                      select new Sel_NotificacionResult
                                      {
                                          IdNotificacion = cn.IdNotificacion,
                                          Notificacion = cn.Notificacion,
                                          NombreArchivo = cn.NombreArchivo,
                                          Clave = cn.Clave,
                                          Orden = cn.Orden
                                      })
                                        .ToList();
                    if (respNotifs != null)
                    {
                        List<TB_CATNotificaciones> lista = new List<TB_CATNotificaciones>();
                        foreach (var not in respNotifs)
                        {
                            lista.Add(new TB_CATNotificaciones()
                            {
                                IdNotificacion = not.IdNotificacion,
                                Notificacion = not.Notificacion,
                                Clave = not.Clave,
                                Orden = not.Orden
                            });
                        }
                        respuesta.Resultado.Notificaciones = lista;
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: Implementar Log de error.
            }
            return respuesta;
        }

        public static ObtenerNombreArchivoPorClaveResponse ObtenerNombreArchivoPorClave(ObtenerNombreArchivoPorClaveRequest peticion)
        {
            ObtenerNombreArchivoPorClaveResponse respuesta = new ObtenerNombreArchivoPorClaveResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respNotif = (from cn in con.Gestiones_TB_CATNotificaciones
                                     where cn.Actual == true
                                         && cn.Clave == peticion.Clave
                                     select new Sel_NotificacionResult
                                     {
                                         IdNotificacion = cn.IdNotificacion,
                                         Notificacion = cn.Notificacion,
                                         NombreArchivo = cn.NombreArchivo,
                                         Clave = cn.Clave,
                                         Orden = cn.Orden
                                     })
                                        .ToList();
                    if (respNotif != null && respNotif.Count > 0)
                    {
                        respuesta.Resultado.Notificacion = respNotif.FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: Implementar log de errores.
                respuesta.Error = true;
            }
            return respuesta;
        }

        public static ObtenerDestinosResponse ObtenerDestinos()
        {
            ObtenerDestinosResponse respuesta = new ObtenerDestinosResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respDestinos = (from dn in con.Gestiones_TB_CATDestinoNotificacion
                                        where dn.Activo == true
                                        orderby dn.IdDestinoNotificacion
                                        select new Sel_DestinoResult
                                        {
                                            IdDestino = dn.IdDestinoNotificacion,
                                            DestinoNotificacion = dn.DestinoNotificacion
                                        })
                                        .ToList();
                    if (respDestinos != null)
                    {
                        List<TB_CATDestinoNotificacion> lista = new List<TB_CATDestinoNotificacion>();
                        foreach (var des in respDestinos)
                        {
                            lista.Add(new TB_CATDestinoNotificacion()
                            {
                                IdDestinoNotificacion = des.IdDestino,
                                DestinoNotificacion = des.DestinoNotificacion
                            });
                        }
                        respuesta.Resultado.Destinos = lista;
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: Implementar Log de error.
            }
            return respuesta;
        }

        public static DatosAvisoCobranzaResponse DatosAvisoCobranza(DatosAvisoCobranzaRequest peticion)
        {
            return dal.DatosAvisoCobranza(peticion);
        }

        public static DatosAvisoPagoResponse DatosAvisoPago(DatosAvisoPagoRequest peticion)
        {
            return dal.DatosAvisoPago(peticion);
        }

        public static DatosNotificacionPrejudicialResponse DatosNotificacionPrejudicial(DatosNotificacionPrejudicialRequest peticion)
        {
            return dal.DatosNotificacionPrejudicial(peticion);
        }

        public static DatosUltimaNotificacionPrejudicialResponse DatosUltimaNotificacionPrejudicial(DatosUltimaNotificacionPrejudicialRequest peticion)
        {
            return dal.DatosUltimaNotificacionPrejudicial(peticion);
        }

        public static DatosCitacionPrejudicialResponse DatosCitacionPrejudicial(DatosCitacionPrejudicialRequest peticion)
        {
            return dal.DatosCitacionPrejudicial(peticion);
        }

        public static DatosPromocionReduccionMoraResponse DatosPromocionReduccionMora(DatosPromocionReduccionMoraRequest peticion)
        {
            return dal.DatosPromocionReduccionMora(peticion);
        }

        public static ObtenerNotificacionesResponse ObtenerNotificacionPromociones()
        {
            ObtenerNotificacionesResponse respuesta = new ObtenerNotificacionesResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respNotifs = (from cn in con.Gestiones_TB_CATNotificaciones
                                      join ctn in con.Gestiones_TB_CATTipoNotificaciones on cn.TipoNotificacionId equals ctn.IdTipoNotificacion
                                      where ctn.TipoNotificacion == "Promociones de Cobranza" && cn.Actual
                                      orderby cn.Orden
                                      select new Sel_NotificacionResult
                                      {
                                          IdNotificacion = cn.IdNotificacion,
                                          Notificacion = cn.Notificacion,
                                          NombreArchivo = cn.NombreArchivo,
                                          Clave = cn.Clave,
                                          Orden = cn.Orden
                                      })
                                        .ToList();
                    if (respNotifs != null)
                    {
                        List<TB_CATNotificaciones> lista = new List<TB_CATNotificaciones>();
                        foreach (var not in respNotifs)
                        {
                            lista.Add(new TB_CATNotificaciones()
                            {
                                IdNotificacion = not.IdNotificacion,
                                Notificacion = not.Notificacion,
                                Clave = not.Clave,
                                Orden = not.Orden
                            });
                        }
                        respuesta.Resultado.Notificaciones = lista;
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: Implementar Log de error.
            }
            return respuesta;
        }

        public static ObtenerBitacoraNotificacionResponse ObtenerBitacoraNotificacion(ObtenerBitacoraNotificacionRequest peticion)
        {
            ObtenerBitacoraNotificacionResponse respuesta = new ObtenerBitacoraNotificacionResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var resp = (from sna in con.Gestiones_SolicitudNotificacionAcciones
                                join sn in con.Gestiones_SolicitudNotificaciones on sna.SolicitudNotificacionId equals sn.IdSolicitudNotificacion
                                join cn in con.Gestiones_TB_CATNotificaciones on sn.NotificacionId equals cn.IdNotificacion
                                join csnta in con.Gestiones_TB_CATSolNotTipoAcciones on sna.SolNotTipoAccionId equals csnta.IdSolNotTipoAccion
                                join tcdn in con.Gestiones_TB_CATDestinoNotificacion on sn.DestinoId equals tcdn.IdDestinoNotificacion
                                join u in con.dbo_TB_CATUsuario on sna.UsuarioId equals u.IdUsuario
                                where sn.SolicitudId == peticion.IdSolicitud
                                orderby sna.FechaRegistro
                                select new Sel_NotificacionBitacoraResult
                                {
                                    Notificacion = cn.Notificacion,
                                    Destino = tcdn.DestinoNotificacion,
                                    Accion = csnta.SolNotTipoAccion,
                                    Usuario = u.Usuario,
                                    Fecha = sna.FechaRegistro
                                }).ToList();

                    if (resp != null && resp.Count > 0)
                    {
                        respuesta.Resultado.Bitacora = resp;
                    }
                    else
                    {
                        respuesta.Error = true;
                        respuesta.MensajeOperacion = "No hay bitácora para mostrar.";
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: Implementar log de errores.
                respuesta.Error = true;
            }
            return respuesta;
        }

        public static ObtenerResultadoGestionNotificacionResponse ObtenerResultadoGestionNotificacion()
        {
            ObtenerResultadoGestionNotificacionResponse respuesta = new ObtenerResultadoGestionNotificacionResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respResultadoGestionNotificacion = (from tcp in con.dbo_TB_CATParametro
                                                            where tcp.Parametro == "RGEN"
                                                            && tcp.IdEstatus == 1
                                                            select new
                                                            {
                                                                tcp.valor
                                                            })
                                                            .FirstOrDefault();
                    if (respResultadoGestionNotificacion != null)
                    {
                        respuesta.Resultado.Parametro = new TB_CATParametro { valor = respResultadoGestionNotificacion.valor };
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: Implementar Log de error.
            }
            return respuesta;
        }

        public static NotificacionesSolNotPendientesResponse ObtenerNotificacionesSolNotPendientes(NotificacionesSolNotPendientesRequest peticion)
        {
            NotificacionesSolNotPendientesResponse respuesta = new NotificacionesSolNotPendientesResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (peticion.IdTipoNotificacion <= 0 && string.IsNullOrEmpty(peticion.TipoNotificacion)) throw new ValidacionExcepcion("El tipo de no notificación no es válido.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    if (peticion.IdTipoNotificacion == 0 && !string.IsNullOrEmpty(peticion.TipoNotificacion))
                    {
                        int idTipoNotificacion = con.Sistema_TB_TipoNotificacion
                                 .Where(tn => tn.Clave == peticion.TipoNotificacion)
                                 .Select(tn => tn.IdTipoNotificacion)
                                 .DefaultIfEmpty(0)
                                 .FirstOrDefault();
                        peticion.IdTipoNotificacion = idTipoNotificacion;
                    }

                    IQueryable<TB_Notificacion> notificionFiltro = con.Sistema_TB_Notificacion
                    .Where(n => n.IdTipoNotificacion == peticion.IdTipoNotificacion
                        && n.Enviado == false
                        && n.Intentos < n.MaxIntentos);

                    if (peticion.RegistrosPorPagina > 0)
                    {
                        notificionFiltro = notificionFiltro.Take(peticion.RegistrosPorPagina);
                    }

                    List<NotificacionSolNotVM> notificaciones = notificionFiltro.Select(n => new NotificacionSolNotVM
                    {
                        IdNotificacion = n.IdNotificacion,
                        IdSolicitud = n.IdSolicitud,
                        Titulo = n.Titulo,
                        CorreoElectronico = n.CorreoElectronico,
                        NumeroTelefono = n.NumeroTelefono,
                        NombreCliente = n.NombreCliente,
                        CapitalSolicitado = n.CapitalSolicitado,
                        Erogacion = n.Erogacion,
                        TotalRecibos = n.TotalRecibos,
                        Intentos = n.Intentos,
                        MaxIntentos = n.MaxIntentos,
                        Url = n.Url,
                        Clave = n.Clave,
                        FechaHoraCitacion = n.FechaHoraCitacion,
                        DescuentoDeuda = n.DescuentoDeuda,
                        FechaVigencia = n.FechaVigencia,
                        IdDestino = n.IdDestino.HasValue ? n.IdDestino.Value : 0,
                        IdUsuarioRegistro = n.IdUsuarioRegistro,
                        Courier = n.Courier ?? false,
                        Actualizar = n.Actualizar ?? false
                    }).ToList();

                    if (notificaciones != null)
                    {
                        respuesta.Resultado.Notificaciones = notificaciones;
                        respuesta.TotalRegistros = notificaciones.Count;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de obtener las notificaciones pendientes.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static AltaNotificacionResponse AltaNotificacionSolNot(AltaNotificacionSolicitudNotificacionRequest peticion)
        {
            AltaNotificacionResponse respuesta = new AltaNotificacionResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Sistema_SP_TB_NotificacionALT_SolicitudNotificacion(peticion);
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
    }
}
