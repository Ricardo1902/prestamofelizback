﻿using SOPF.Core.BusinessLogic.Sistema;
using SOPF.Core.DataAccess.Gestiones;
using SOPF.Core.Entities;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class ArchivoGestionBll
    {
        static ArchivoGestionDal dal;
        static ArchivoGestionBll()
        {
            dal = new ArchivoGestionDal(new DataAccess.CreditOrigContext());
        }

        public static ArchivoGestionVM ObtenerArchivoGestionPorId(int idArchivoGestion)
        {
            return dal.ObtenerArchivoGestionPorId(idArchivoGestion);
        }
        public static List<ArchivoGestionVM> ObtenerArchivoGestionPorSolicitud(int idSolicitud)
        {
            return dal.ObtenerArchivoGestionPorSolicitud(idSolicitud);
        }
        public static Resultado<bool> InsertarArchivoGestion(ArchivoGestion archivoGestion)
        {
            return dal.InsertarArchivoGestion(archivoGestion);
        }
        public static Resultado<bool> ActualizarArchivoGestion(ArchivoGestion archivoGestion)
        {
            return dal.ActualizarArchivoGestion(archivoGestion);
        }
        public static Resultado<bool> EliminarArchivoGestion(int idArchivoGestion, int idSolicitud)
        {
            return dal.EliminarArchivoGestion(idArchivoGestion, idSolicitud);
        }
        public static Resultado<bool> SubirArchivoGoogleDrive(int idSolicitud, int idUsuario, CargaArchivoGoogleDrive peticion)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            try
            {
                if (idSolicitud <= 0) throw new Exception("La calve de la solicitud es inválida");
                if (idUsuario <= 0) throw new Exception("La calve del usuario es inválida");
                if (peticion == null) throw new Exception("La petición es inválida");

                //Se intenta subir archivo a Google Drive
                respuesta = dal.SubirArchivoGoogleDrive(idSolicitud, idUsuario, peticion);
                if (respuesta == null) throw new Exception("Ocurrio un error no controlado al subir el archivo correspondiente.");
                if (respuesta.Codigo <= 0) throw new Exception(respuesta.Mensaje);

                //Validar que el documento destino existe y obtener el IdNotificacion y IdDestino
                var documentoDestino = DocumentoDestinoBll.ObtenerDocumentoDestinoPorId(new ObtenerDocumentoDestinoRequest { IdDocumentoDestino = peticion.IdDocumentoDestino });
                if (documentoDestino != null && documentoDestino.Resultado != null && documentoDestino.Resultado.DocumentoDestino != null)
                {
                    //Validar que la solicitud cuente con un registro de notificacion
                    var notificacionesSolicitud = NotificacionBll.ObtenerNotificacionActivaSolicitud(idSolicitud, documentoDestino.Resultado.DocumentoDestino.IdNotificacion, documentoDestino.Resultado.DocumentoDestino.IdDestinoNotificacion);
                    if (notificacionesSolicitud != null)
                    {

                        //Se ejecuta proceso de actualizacion de notificacion
                        var respActNotificacion = NotificacionBll.ActualizarSolicitudNotificacion(new ActualizarSolicitudNotificacionRequest
                        {
                            IdSolicitudNotificacion = notificacionesSolicitud.IdSolicitudNotificacion,
                            IdUsuarioEscaneo = idUsuario,
                            IdDestino = notificacionesSolicitud.DestinoId,
                            IdSolNotTipoAccion = 3 //Se envia siempre en este apartado valor 3 (Escaneo)
                        });
                        if (respActNotificacion == null) throw new Exception("Ha ocurrido un error al intentar actualizar la notificacion de la solicitud");
                        if (respActNotificacion.Codigo <= 0) throw new Exception(respActNotificacion.Mensaje);
                    }
                }


            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.Mensaje = ex.Message;
                respuesta.ResultObject = false;
            }
            return respuesta;
        }
        public static Resultado<ArchivoGoogleDrive> DescargarArchivoGoogleDrive(string idArchivo)
        {
            return dal.DescargarArchivoGoogleDrive(idArchivo);
        }
    }
}
