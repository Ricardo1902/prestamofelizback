#pragma warning disable 140822
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.DataAccess.Gestiones;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: AsignarCuentasTMPBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase AsignarCuentasTMPDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-22 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class AsignarCuentasTMPBll
    {
        private static AsignarCuentasTMPDal dal;

        static AsignarCuentasTMPBll()
        {
            dal = new AsignarCuentasTMPDal();
        }

        public static void InsertarAsignarCuentasTMP(AsignarCuentasTMP entidad)
        {
            dal.InsertarAsignarCuentasTMP(entidad);			
        }
        
        public static AsignarCuentasTMP ObtenerAsignarCuentasTMP()
        {
            return dal.ObtenerAsignarCuentasTMP();
        }
        
        public static void ActualizarAsignarCuentasTMP()
        {
            dal.ActualizarAsignarCuentasTMP();
        }

        public static void EliminarAsignarCuentasTMP()
        {
            dal.EliminarAsignarCuentasTMP();
        }
    }
}