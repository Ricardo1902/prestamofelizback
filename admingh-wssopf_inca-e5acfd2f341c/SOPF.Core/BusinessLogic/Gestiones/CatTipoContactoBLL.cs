﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class CatTipoContactoBLL
    {
        public static ObtenerTipoContactosResponse TipoContactosGestTelefonico(Peticion peticion)
        {
            ObtenerTipoContactosResponse respuesta = new ObtenerTipoContactosResponse();
            try
            {
                if (peticion == null || peticion.IdUsuario <= 0)
                    throw new ValidacionExcepcion("La petición no es correcta o la clave del usuario no es váido.");
                TipoGestion tipoGest = TipoGestionBLL.TipoGestion("Telefonico");
                if (tipoGest != null)
                {
                    respuesta = ObtenerTipoContactos(new TipoContactosRequest { IdTipoGestion = tipoGest.IdTipoGestion });
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ObtenerTipoContactosResponse ObtenerTipoContactos(TipoContactosRequest peticion)
        {
            ObtenerTipoContactosResponse respuesta = new ObtenerTipoContactosResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    IQueryable<CatTipoContaco> filtro = con.Gestiones_CatTipoContaco;
                    if (peticion != null && peticion.IdTipoGestion != null) filtro = filtro.Where(tc => tc.IdTipoGestion == peticion.IdTipoGestion);

                    List<CatTipoContaco> result = filtro.ToList();
                    if (result != null)
                    {
                        respuesta.Resultado.CatTipoContactos = result;
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
    }
}
