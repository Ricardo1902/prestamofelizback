﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Gestiones;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
#endregion
 
namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class AvisosBll
    {
        private static AvisosDal dal;

        static AvisosBll()
        {
            dal = new AvisosDal();
        }

        public static void InsertarAvisos(int IdCuenta, int Carteo, int sms, int wats, int mail, int circulo, int fisico)
        {
            dal.InsertarAvisos(IdCuenta, Carteo, sms, wats, mail, circulo, fisico);			
        }

        public static Avisos ObtenerAvisos(int idCuenta)
        {
            return dal.ObtenerAvisos(idCuenta);
        }

        //public static DetalleCuenta ObtenerDetalleCuenta(int idCuenta)
        //{
        //    return dal.ObtenerDetalleCuenta(idCuenta);
        //}

       
    }
}
