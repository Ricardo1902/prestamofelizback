#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.DataAccess.Gestiones;

# region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCatTipoGestorBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBCatTipoGestorDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class TBCatTipoGestorBll
    {
        private static TBCatTipoGestorDal dal;

        static TBCatTipoGestorBll()
        {
            dal = new TBCatTipoGestorDal();
        }

        public static void InsertarTBCatTipoGestor(TBCatTipoGestor entidad)
        {
            dal.InsertarTBCatTipoGestor(entidad);			
        }
        
        public static List<TBCatTipoGestor> ObtenerTBCatTipoGestor()
        {
            return dal.ObtenerTBCatTipoGestor();
        }
        
        public static void ActualizarTBCatTipoGestor()
        {
            dal.ActualizarTBCatTipoGestor();
        }

        public static void EliminarTBCatTipoGestor()
        {
            dal.EliminarTBCatTipoGestor();
        }
    }
}