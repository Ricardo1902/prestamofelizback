﻿using SOPF.Core.DataAccess;
using SOPF.Core.Model.Response.Gestiones;
using System;
using System.Linq;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class TipoGestorContactoBLL
    {
        public static ObtenerTipoGestorContactosResponse ObtenerTipoGestorContactos()
        {
            ObtenerTipoGestorContactosResponse respuesta = new ObtenerTipoGestorContactosResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var result = con.Gestiones_TipoGestorContacto.ToList();
                    if (result != null)
                    {
                        respuesta.Resultado.TipoGestorContactos = result;
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: Implementar log de errores.
                respuesta.Error = true;
            }
            return respuesta;
        }
    }
}
