﻿using Newtonsoft.Json;
using SOPF.Core.BusinessLogic.Solicitudes;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Gestiones;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Model.Response.Solicitudes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class PeticionReestructuraBll
    {
        private static PeticionReestructuraDal dal;

        static PeticionReestructuraBll()
        {
            dal = new PeticionReestructuraDal();
        }

        public static List<PeticionReestructura.BusquedaClienteSolicitudPRRes> Clientes_Filtrar(string Accion, PeticionReestructura.BusquedaClienteSolicitudPRReq entity)
        {
            return dal.Clientes_Filtrar(Accion, entity);
        }

        public static Resultado<bool> InsertarPeticionReestructura(string Accion, InsertarPeticionReestructuraRequest entity)
        {
            return dal.InsertarPeticionReestructura(Accion, entity);
        }

        public static Resultado<List<PeticionReestructura>> ObtenerPeticionReestructura(string Accion, PeticionReestructura entity)
        {
            return dal.ObtenerPeticionReestructura(Accion, entity);
        }

        public static Resultado<TBCreditos.CalculoLiquidacion> CalcularLiquidacionPeticionReestructura(TBCreditos.Liquidacion eOwner)
        {
            return dal.CalcularLiquidacionPeticionReestructura(eOwner);
        }

        public static Resultado<bool> AutorizarPeticionReestructura(AutorizarPeticionReestructuraRequest entity)
        {
            return dal.AutorizarPeticionReestructura(entity);
        }

        public static Resultado<bool> RechazarPeticionReestructura(RechazarPeticionReestructuraRequest entity)
        {
            return dal.RechazarPeticionReestructura(entity);
        }

        public static List<Combo> ObtenerCombo_PlazosReestructura(ObtenerComboPlazosReestructuraRequest entity)
        {
            return dal.ObtenerCombo_PlazosReestructura(entity);
        }

        public static ObtenerProductoReestructuraResponse ObtenerProductoReestructura(ObtenerProductoReestructuraRequest entity)
        {
            return dal.ObtenerProductoReestructura(entity);
        }

        public static PeticionReestDocDigitalesResponse PeticionReestDocDigitales(PeticionReestDocDigitalesRequest peticion)
        {
            PeticionReestDocDigitalesResponse respuesta = new PeticionReestDocDigitalesResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");

                if (peticion.EstatusPeticionReest != null && peticion.EstatusPeticionReest.Length > 0)
                {

                }
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var peticionReest = new PeticionReestructuraDocsDigCONRequest
                    {
                        IdSolicitud = peticion.IdSolicitud,
                        Pagina = peticion.Pagina,
                        RegistrosPagina = peticion.RegistrosPagina
                    };
                    if (peticion.EstatusPeticionReest != null && peticion.EstatusPeticionReest.Length > 0)
                    {
                        XElement estatusPeticion = new XElement("EstatusPeticion", peticion.EstatusPeticionReest.Select(est =>
                            new XElement("IdEstatus", est)));
                        peticionReest.IdEstatus = estatusPeticion;
                    }
                    respuesta = con.Solicitudes_SP_PeticionReestructuraDocsDigCON(peticionReest);
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrio un error al momento de obtener la peticiones";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Metodo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ValidarPeticionReestructuraResponse ValidarPeticionReestructura(ValidarPeticionReestructuraRequest peticion)
        {
            ValidarPeticionReestructuraResponse respuesta = new ValidarPeticionReestructuraResponse();

            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    if (peticion.Accion == "LiberarPetReest")
                    {
                        if (peticion.IdSolicitudNuevo <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es correcta.");

                        respuesta = con.Creditos_SP_PetResLiberarCreditoPRO(peticion);
                    }
                    else if (peticion.Accion == "CancelarPetReest")
                    {
                        if (peticion.IdPeticionReestructura <= 0) throw new ValidacionExcepcion("La clave de la petición no es correcta.");
                        if (string.IsNullOrEmpty(peticion.Comentario)) throw new ValidacionExcepcion("Ingrese un comentario para cancelar la petición.");

                        Resultado<bool> rechazarResp = dal.RechazarPeticionReestructura(new RechazarPeticionReestructuraRequest
                        {
                            Accion = "RECHAZAR_PETICION_REESTRUCTURA_POR_ID",
                            Id_PeticionReestructura = peticion.IdPeticionReestructura,
                            AutorizadorUsuario_Id = peticion.IdUsuario,
                            ComentarioAutorizador = peticion.Comentario
                        });
                        respuesta.Error = !rechazarResp.ResultObject;
                        respuesta.MensajeErrorException = rechazarResp.Mensaje;
                    }
                }
                if (string.IsNullOrEmpty(respuesta.MensajeOperacion)) respuesta.MensajeOperacion = respuesta.Error ? "Ocurrió un error al procesar la solicitud"
                                : "La solicitud ha sido procesada correctamente.";
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrio un error al momento de procesar la solicitud.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Metodo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }


        public static EstatusPeticionReestResponse EstatusPeticionReestEtapaVal()
        {
            EstatusPeticionReestResponse respuesta = new EstatusPeticionReestResponse();

            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta.EstatusPeticion = con.Solicitudes_TB_CATEstatusPeticionReestructuras
                        .Where(es => es.EtapaPeticionReestructura_Id == 3)
                        .Select(es => new Model.Solicitudes.EstatusPeticionReestructuraVM
                        {
                            IdEstatusPeticionReestructura = es.Id_EstatusPeticionReestructuras,
                            EstatusPeticionReestructura = es.EstatusPeticionReestructura
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrio un error al momento de obtener la peticiones";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Metodo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ReiniciarProcesoDigitalPetReestResponse ReiniciarProceso(ReiniciarProcesoDigitalPetReestRequest peticion)
        {
            ReiniciarProcesoDigitalPetReestResponse respuesta = new ReiniciarProcesoDigitalPetReestResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es correcta.");
                if (peticion.IdSolicitudPruebaVida <= 0 && peticion.IdSolicitudFirmaDigital <= 0
                    || peticion.IdSolicitudPruebaVida > 0 && peticion.IdSolicitudFirmaDigital > 0) throw new ValidacionExcepcion("La clave de la petición no es correcta.");

                ReiniciarProcesoRequest reiniciarProceso = new ReiniciarProcesoRequest
                {
                    IdSolicitud = peticion.IdSolicitud,
                    IdUsuario = peticion.IdUsuario,
                    Comentario = "Proceso Validacion PetReest",
                    InicioAutomatico = true,
                    ReinicioManual = true,
                    ReinicioInvidual = true
                };

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    if (peticion.IdSolicitudPruebaVida > 0)
                    {
                        int solicitudProc = con.Solicitudes_TB_SolicitudPruebaVida
                            .Where(sp => sp.IdSolicitudPruebaVida == peticion.IdSolicitudPruebaVida)
                            .Select(sp => sp.IdSolicitudProcesoDigital)
                            .DefaultIfEmpty(0)
                            .FirstOrDefault();

                        if (solicitudProc > 0)
                        {
                            reiniciarProceso.IdSolicitudProcesoDigital = solicitudProc;
                        }
                    }
                    else if (peticion.IdSolicitudFirmaDigital > 0)
                    {
                        int solicitudProc = con.Solicitudes_TB_SolicitudFirmaDigital
                                .Where(sp => sp.IdSolicitudFirmaDigital == peticion.IdSolicitudFirmaDigital)
                                .Select(sp => sp.IdSolicitudProcesoDigital)
                                .DefaultIfEmpty(0)
                                .FirstOrDefault();
                        reiniciarProceso.IdSolicitudProcesoDigital = solicitudProc;
                    }
                }

                if (reiniciarProceso.IdSolicitudProcesoDigital == 0) throw new ValidacionExcepcion("No fue posible encontrar el proceso a reiniciar.");

                ReiniciarProcesoResponse respReinicio = SolicitudProcesoDigitalBLL.ReiniciarProceso(reiniciarProceso);
                respuesta.Error = respReinicio.Error;
                respuesta.MensajeOperacion = respReinicio.MensajeOperacion;
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrio un error al momento de obtener la peticiones";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Metodo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static void EnviarCorreoCargaDocs(int idSolicitudFirmaDigital)
        {
            try
            {
                if (idSolicitudFirmaDigital > 0)
                {
                    Poco.Sistema.TB_TipoNotificacion tipoNotificacion = Sistema.TipoNotificacionBLL.TipoNotificacion(TipoNotificacion.CargaDocPetReest.GetDescription());
                    if (tipoNotificacion == null || tipoNotificacion.IdTipoNotificacion <= 0) throw new ValidacionExcepcion("No fue posible realizar el envio. Consulte a soporte técnico.");
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        var datoCte = (from sf in con.Solicitudes_TB_SolicitudFirmaDigital
                                       join pet in con.Solicitudes_TB_PeticionReestructuras on sf.IdSolicitud equals pet.IdSolicitudNuevo
                                       join s in con.dbo_TB_Solicitudes on sf.IdSolicitud equals s.IdSolicitud
                                       join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                       where sf.IdSolicitudFirmaDigital == idSolicitudFirmaDigital
                                       select new { s.IdSolicitud, pet.Solicitud_Id, c.Cliente, c.Nombres, c.apellidop, c.apellidom, c.Correo })
                                       .FirstOrDefault();

                        if (datoCte == null || string.IsNullOrEmpty(datoCte.Correo)) throw new ValidacionExcepcion("No se pudo obtener la cuenta de coreo del cliente.");
                        Model.Request.Sistema.NuevoRequest nuevaNotif = new Model.Request.Sistema.NuevoRequest
                        {
                            IdUsuarioRegistro = (int)UsuarioSistema.Administrador,
                            IdTipoNotificacion = tipoNotificacion.IdTipoNotificacion,
                            IdSolicitud = datoCte.Solicitud_Id,
                            Titulo = string.IsNullOrEmpty(tipoNotificacion.TituloCorreo) ? "Carga Docs. PeticionReest" : tipoNotificacion.TituloCorreo,
                            NombreCliente = $"{datoCte.Nombres} {datoCte.apellidop} {datoCte.apellidom}"
                        };

                        //Correos de usuarios asignados al menu
                        string urlMenu = MenuSistema.Gestiones_ValidacionPeticionReestrucura.GetDescription();
                        List<string> correos = (from m in con.Reportes_Cat_Menu
                                                join a in con.Reportes_Accesos on m.IdMenu equals a.Reporte_Id
                                                join u in con.dbo_TB_CATUsuario on a.UsuarioTipo_ID equals u.IdUsuario
                                                where m.URL == urlMenu
                                                    && a.TipoAcceso_Id == 2
                                                    && a.TipoAcc == 1
                                                    && u.IdUsuario != (int)UsuarioSistema.Administrador
                                                select u.Email)
                                                .ToList();
                        if (correos != null)
                        {
                            nuevaNotif.CorreoElectronico = string.Join(";", correos.Where(c => !string.IsNullOrEmpty(c)));
                        }

                        if (!string.IsNullOrEmpty(nuevaNotif.CorreoElectronico))
                        {
                            Model.Response.Sistema.NuevoResponse respNoti = Sistema.NotificacionBLL.Nuevo(nuevaNotif);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Metodo: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(idSolicitudFirmaDigital)}", JsonConvert.SerializeObject(ex));
            }
        }
    }
}
