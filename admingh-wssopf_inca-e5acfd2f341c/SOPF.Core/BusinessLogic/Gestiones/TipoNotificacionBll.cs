﻿using SOPF.Core.DataAccess.Gestiones;
using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class TipoNotificacionBll
    {
        static TipoNotificacionDal dal;
        static TipoNotificacionBll()
        {
            dal = new TipoNotificacionDal(new DataAccess.CreditOrigContext());
        }
        public static List<TipoNotificacionVM> ObtenerTiposNotificacion()
        {
            return dal.ObtenerTiposNotificacion();
        }
    }
}
