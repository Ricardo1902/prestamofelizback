#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Gestiones;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Response.Gestiones;
using System;
using System.Collections.Generic;
using System.Linq;

#region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TBAsignacionBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase TBAsignacionDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class TBAsignacionBll
    {
        private static TBAsignacionDal dal;

        static TBAsignacionBll()
        {
            dal = new TBAsignacionDal();
        }

        public static void InsertarTBAsignacion(TBAsignacion entidad, int usuarioIdAsigna)
        {
            dal.InsertarTBAsignacion(entidad, usuarioIdAsigna);
        }
        public static void InsertarAsignacionMasiva(List<AsignarCuentasTMP> Cuentas)
        {

            foreach (AsignarCuentasTMP Cuenta in Cuentas)
            {
                TBAsignacion asignacion = new TBAsignacion();
                asignacion.IdCuenta = Cuenta.IdCuenta;
                asignacion.IdGestor = Cuenta.IdGestor;

                try
                {
                    InsertarTBAsignacion(asignacion, Cuenta.IdUsuario);
                }
                catch
                {

                }

            }
        }

        public static void DesasignacionMasiva(List<AsignarCuentasTMP> Cuentas)
        {

            foreach (AsignarCuentasTMP Cuenta in Cuentas)
            {
                TBAsignacion asignacion = new TBAsignacion();
                asignacion.IdCuenta = Cuenta.IdCuenta;
                asignacion.IdGestor = Cuenta.IdGestor;

                try
                {
                    ActualizarTBAsignacion(asignacion);
                }
                catch
                {

                }

            }



        }
        public static List<DataResult.Usp_ObtenerAsignacion> ObtenerTBAsignacion(int idGestor, int accion, int idCuenta, string cliente, int idSucursal, string DNI, string convenio)
        {
            return dal.ObtenerTBAsignacion(idGestor, accion, idCuenta, cliente, idSucursal, DNI, convenio);
        }

        public static void ActualizarTBAsignacion(TBAsignacion entidad)
        {
            dal.ActualizarTBAsignacion(entidad);
        }

        public static void EliminarTBAsignacion()
        {
            dal.EliminarTBAsignacion();
        }

        public static ObtenerAsignacionAccionResponse ObtenerAsignacionAccion(ObtenerAsignacionAccionRequest peticion)
        {
            ObtenerAsignacionAccionResponse respuesta = new ObtenerAsignacionAccionResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var respAsig = (from a in con.Gestiones_TB_Asignacion
                                    join aa in con.Gestiones_AsignacionAccion on a.IdAsignacion equals aa.IdAsignacion
                                    join fd in con.Cobranza_FalloDomiciliacion on new { IdCuenta = (int)a.IdCuenta, RegistroActual = true }
                                        equals new { fd.IdCuenta, RegistroActual = fd.RegistroActual ?? false } into fd1
                                    from lfd in fd1.DefaultIfEmpty()
                                    where a.IdCuenta == peticion.IdCuenta
                                        && a.EstatusAsignacion == true
                                    select new ObtenerAsignacionAccionResult
                                    {
                                        IdAsignacion = a.IdAsignacion,
                                        IdAsignacionAccion = aa.IdAsignacionAccion,
                                        DescipcionActividad = aa.DescipcionActividad,
                                        DescripcionFallo = lfd == null ? string.Empty : lfd.DescripcionFallo
                                    })
                                    .ToList();
                    if (respAsig != null && respAsig.Count > 0)
                    {
                        respuesta.Resultado.AsignacionAccion = respAsig;
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: Implementar Log de error.
            }
            return respuesta;
        }
    }
}