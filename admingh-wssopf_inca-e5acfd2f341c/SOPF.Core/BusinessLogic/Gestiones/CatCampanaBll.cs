#pragma warning disable 150109
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.DataAccess.Gestiones;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatCampanaBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase CatCampanaDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-01-09 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class CatCampanaBll
    {
        private static CatCampanaDal dal;

        static CatCampanaBll()
        {
            dal = new CatCampanaDal();
        }

        public static void InsertarCatCampana(CatCampana entidad)
        {
            dal.InsertarCatCampana(entidad);			
        }
        
        public static List<CatCampana> ObtenerCatCampana(int accion, int idcampana)
        {
            return dal.ObtenerCatCampana(accion,idcampana);
        }
        
        public static void ActualizarCatCampana()
        {
            dal.ActualizarCatCampana();
        }

        public static void EliminarCatCampana()
        {
            dal.EliminarCatCampana();
        }
    }
}