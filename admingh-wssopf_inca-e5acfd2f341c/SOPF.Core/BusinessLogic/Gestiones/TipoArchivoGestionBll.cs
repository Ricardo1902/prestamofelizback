﻿using SOPF.Core.DataAccess.Gestiones;
using SOPF.Core.Entities;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class TipoArchivoGestionBll
    {
        static TipoArchivoGestionDal dal;
        static TipoArchivoGestionBll()
        {
            dal = new TipoArchivoGestionDal(new DataAccess.CreditOrigContext());
        }

        public static TipoArchivoGestionVM ObtenerTipoArchivoGestionPorId(int idTipoArchivoGestion)
        {
            return dal.ObtenerTipoArchivoGestionPorId(idTipoArchivoGestion);
        }
        public static TipoArchivoGestionVM ObtenerTipoArchivoGestionPorMime(string tipoMime)
        {
            return dal.ObtenerTipoArchivoGestionPorMime(tipoMime);
        }
        public static List<TipoArchivoGestionVM> ObtenerTiposArchivoGestion()
        {
            return dal.ObtenerTiposArchivoGestion();
        }
        public static Resultado<bool> InsertarTipoArchivoGestion(TipoArchivoGestion tipoArchivoGestion)
        {
            return dal.InsertarTipoArchivoGestion(tipoArchivoGestion);
        }
        public static Resultado<bool> ActualizarTipoArchivoGestion(TipoArchivoGestion tipoArchivoGestion)
        {
            return dal.ActualizarTipoArchivoGestion(tipoArchivoGestion);
        }

    }
}
