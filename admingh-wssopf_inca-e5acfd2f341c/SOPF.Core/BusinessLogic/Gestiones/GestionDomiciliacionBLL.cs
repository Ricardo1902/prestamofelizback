﻿using iText.Layout.Element;
using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class GestionDomiciliacionBLL
    {
        public static ActualizarGestionDomiciliacionResponse ActualizarGestionDomiciliacion(ActualizarGestionDomiciliacionRequest peticion)
        {
            ActualizarGestionDomiciliacionResponse respuesta = new ActualizarGestionDomiciliacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdGestionDomiciliacion == null || peticion.IdGestionDomiciliacion.Count == 0) throw new ValidacionExcepcion("Proporcione las cuentas a actualizar.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuairo no es váilda");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<TB_GestionDomiciliacion> gestDomiciliacion = con.Gestiones_TB_GestionDomiciliacion
                        .Where(gd => peticion.IdGestionDomiciliacion
                            .Contains(gd.IdGestionDomiciliacion)).ToList();
                    List<TB_GestionDomiciliacionAct> gestDomAct = new List<TB_GestionDomiciliacionAct>();

                    foreach (TB_GestionDomiciliacion gestDom in gestDomiciliacion)
                    {
                        if (peticion.EnviadoDom)
                        {
                            gestDom.EnviadoDom = peticion.EnviadoDom;
                            gestDom.FechaEnvioDom = DateTime.Now;
                            gestDom.IdUsuarioEnviaDom = peticion.IdUsuario;
                            gestDom.IdPeticionDomiciliacion = peticion.IdPeticionDomiciliacion;
                        }
                        gestDom.IdEstatus = peticion.IdEstatus;
                        
                        con.Gestiones_TB_GestionDomiciliacionAct.Add(new TB_GestionDomiciliacionAct
                        {
                            IdUsuario = peticion.IdUsuario,
                            IdEstatus = peticion.IdEstatus,
                            FechaRegistro = DateTime.Now,
                            IdGestionDomiciliacion = gestDom.IdGestionDomiciliacion
                        });
                    }
                    con.SaveChanges();
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
    }
}
