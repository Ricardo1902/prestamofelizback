﻿using SOPF.Core.DataAccess.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Response.Gestiones;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class DocumentoDestinoBll
    {

        static DocumentoDestinoDal dal;
        static DocumentoDestinoBll()
        {
            dal = new DocumentoDestinoDal(new DataAccess.CreditOrigContext());
        }

        public static ObtenerDocumentosDestinoResponse ObtenerDocumentosDestino()
        {
            return dal.ObtenerDocumentosDestino();
        }
        public static ObtenerDocumentoDestinoResponse ObtenerDocumentoDestinoPorId(ObtenerDocumentoDestinoRequest peticion)
        {
            return dal.ObtenerDocumentoDestinoPorId(peticion);
        }
    }
}
