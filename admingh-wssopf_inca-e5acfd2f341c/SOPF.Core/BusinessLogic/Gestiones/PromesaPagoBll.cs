#pragma warning disable 140827
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.DataAccess.Gestiones;

# region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: PromesaPagoBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase PromesaPagoDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class PromesaPagoBll
    {
        private static PromesaPagoDal dal;

        static PromesaPagoBll()
        {
            dal = new PromesaPagoDal();
        }

        public static void InsertarPromesaPago(PromesaPago entidad)
        {
            dal.InsertarPromesaPago(entidad);			
        }
        
        public static List<PromesaPago> ObtenerPromesaPago(int idCuenta)
        {
            return dal.ObtenerPromesaPago(idCuenta);
        }
        
        public static void ActualizarPromesaPago()
        {
            dal.ActualizarPromesaPago();
        }

        public static void EliminarPromesaPago()
        {
            dal.EliminarPromesaPago();
        }

        public static List<PromesaPago> ReportePromesaPago(int idcuenta, DateTime FecIni, DateTime FecFin, int Accion)
        {
            return dal.ReportePromesaPago(idcuenta, FecIni, FecFin, Accion);
        }
    }
}