﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Gestiones;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class TipoGestionBLL
    {
        private static TipoGestionDal dal;
        static TipoGestionBLL()
        {
            dal = new TipoGestionDal();
        }
        public static TipoGestion TipoGestion(string tipo)
        {
            TipoGestion tipoGestion = null;
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    tipoGestion = con.Gestiones_TipoGestion
                        .Where(tg => tg.Tipo.Equals(tipo, StringComparison.CurrentCultureIgnoreCase))
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(tipo)}", JsonConvert.SerializeObject(ex));
            }
            return tipoGestion;
        }

        public static List<TipoGestion> ObtenerTipoGestion(int accion)
        {
            return dal.ObtenerTipoGestion(accion);
        }
    }
}
