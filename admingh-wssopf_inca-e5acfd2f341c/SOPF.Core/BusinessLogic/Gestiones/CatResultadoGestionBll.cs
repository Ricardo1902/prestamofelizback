#pragma warning disable 140825
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.DataAccess.Gestiones;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatResultadoGestionBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase CatResultadoGestionDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-25 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class CatResultadoGestionBll
    {
        private static CatResultadoGestionDal dal;

        static CatResultadoGestionBll()
        {
            dal = new CatResultadoGestionDal();
        }

        public static void InsertarCatResultadoGestion(CatResultadoGestion entidad)
        {
            dal.InsertarCatResultadoGestion(entidad);			
        }
        
        public static List<CatResultadoGestion> ObtenerCatResultadoGestion()
        {
            return dal.ObtenerCatResultadoGestion(1,"");
        }

        public static List<CatResultadoGestion> ObtenerCatResultadoGestion(int accion, string resultado)
        {
            return dal.ObtenerCatResultadoGestion(accion,resultado);
        }
        
        public static void ActualizarCatResultadoGestion()
        {
            dal.ActualizarCatResultadoGestion();
        }

        public static void EliminarCatResultadoGestion()
        {
            dal.EliminarCatResultadoGestion();
        }
    }
}