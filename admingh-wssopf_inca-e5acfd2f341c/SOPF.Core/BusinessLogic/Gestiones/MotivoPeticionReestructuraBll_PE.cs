﻿using System.Collections.Generic;
using SOPF.Core.Entities;
using SOPF.Core.DataAccess.Gestiones;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public class MotivoPeticionReestructuraBll_PE
    {
        private static MotivoPeticionReestructuraDal_PE dal;

        static MotivoPeticionReestructuraBll_PE()
        {
            dal = new MotivoPeticionReestructuraDal_PE();
        }

        public static List<Combo> ObtenerCATCombo()
        {
            return dal.ObtenerCATCombo();
        }
    }
}
