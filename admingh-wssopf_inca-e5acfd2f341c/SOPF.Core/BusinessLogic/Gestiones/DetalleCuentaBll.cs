﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.DataAccess.Gestiones;

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class DetalleCuentaBll
    {
         private static DetalleCuentaDal dal;

         static DetalleCuentaBll()
        {
            dal = new DetalleCuentaDal();
        }
         public static DetalleCuenta ObtenerDetalleCuenta(int idCuenta)
         {
             DetalleCuenta detalle = new DetalleCuenta();
             
             return dal.ObtenerDetalleCuenta(idCuenta);
         }
    }
}
