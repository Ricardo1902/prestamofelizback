//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using Newtonsoft.Json;
using SOPF.Core.BusinessLogic.Catalogo;
using SOPF.Core.BusinessLogic.Cobranza;
using SOPF.Core.BusinessLogic.Creditos;
using SOPF.Core.BusinessLogic.Reportes;
using SOPF.Core.BusinessLogic.Sistema;
using SOPF.Core.BusinessLogic.Usuario;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Gestiones;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.Entities.Usuario;
using SOPF.Core.Gestiones;
using SOPF.Core.Model;
using SOPF.Core.Model.Cobranza;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Request.Reportes;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Model.Response.Sistema;
using SOPF.Core.Model.Sistema;
using SOPF.Core.Poco.Cobranza;
using SOPF.Core.Poco.dbo;
using SOPF.Core.Poco.Gestiones;
using SOPF.Core.Poco.Sistema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: GestionBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase GestionDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-25 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Gestiones
{
    public static class GestionBll
    {
        private static GestionDal dal;
        private static DetalleCuentaDal dalDet;
        const string origenDomiciliacion = "GestionesDom";

        static GestionBll()
        {
            dal = new GestionDal();
            dalDet = new DetalleCuentaDal();
        }

        public static int InsertarGestion(Gestion entidad)
        {
            int idGestion = dal.InsertarGestion(entidad);
            _ = Task.Factory.StartNew(() =>
            {
                entidad.IdGestion = idGestion;
                GestionDomiciliacion(entidad);
            });
            return idGestion;
        }

        public static List<SOPF.Core.Entities.Gestiones.DataResult.Usp_ObtenerGestion> ObtenerGestion(int idCuenta)
        {
            return dal.ObtenerGestion(0, idCuenta, 0, DateTime.Now, DateTime.Now, 0);
        }

        public static List<SOPF.Core.Entities.Gestiones.DataResult.Usp_ObtenerGestion> ObtenerGestion(int Accion, int idCuenta, int idGestion, DateTime FecIni, DateTime FecFin, int idGestor)
        {
            return dal.ObtenerGestion(Accion, idCuenta, idGestion, FecIni, FecFin, idGestor);
        }

        public static ObtenerGestionPordIdGestionResponse ObtenerGestionPordIdGestion(ObtenerGestionRequest peticion)
        {
            return dal.ObtenerGestionPordIdGestion(peticion);
        }

        public static string ActualizarGestion(Gestion Entidad)
        {
            string respuesta = "";
            string FchGestion = "";
            string FchActualizacion = "";
            int resultado = 0;
            List<SOPF.Core.Entities.Gestiones.DataResult.Usp_ObtenerGestion> Gestion = new List<DataResult.Usp_ObtenerGestion>();

            Gestion = ObtenerGestion(1, 0, Entidad.IdGestion, DateTime.Now, DateTime.Now, 0);
            FchGestion = Gestion[0].FchGestion.ToString("dd/MM/yyyy");
            FchActualizacion = DateTime.Now.ToString("dd/MM/yyyy");

            if (Entidad.IdGestor != Gestion[0].IdGestor)
            {
                respuesta = "0-No puede modificar gestiones de otro usuario";
            }
            else if (FchActualizacion != FchGestion)
            {
                respuesta = "0-Solo puede modificar gestiones del día";
            }
            else
            {
                resultado = dal.ActualizarGestion(Entidad);
                if (resultado == 1)
                {
                    respuesta = "1-Gestión Actualizada con éxito";
                }
                else
                {
                    respuesta = "0-Error al actualizar la gestión";
                }

            }


            return respuesta;
        }

        public static void EliminarGestion()
        {
            dal.EliminarGestion();
        }

        public static List<TBCreditos> ObtenerOtrasCuentas(int idCuenta)
        {
            return dal.ObtenerOtrasCuentas(idCuenta);
        }

        public static List<DataResult.RepGestiones> ReporteGestiones(int idcuenta, DateTime FecIni, DateTime FecFin, int idGestor)
        {
            return dal.ReporteGestiones(idcuenta, FecIni, FecFin, idGestor);
        }

        public static string InsertarGestionesMovil(List<GestionesMovil> gestiones)
        {
            string Mensaje = "Gestiones Insertadas con éxito";
            Gestion gestion = new Gestion();
            List<TBCATUsuario> usuario = new List<TBCATUsuario>();
            List<TBCatGestores> gestor = new List<TBCatGestores>();
            List<CatResultadoGestion> CatResultado = new List<CatResultadoGestion>();

            List<TBCreditos> cuenta = new List<TBCreditos>();

            foreach (GestionesMovil gestionMovil in gestiones)
            {
                cuenta = TBCreditosBll.ObtenerTBCreditos(1, gestionMovil.idsolicitud);
                usuario = TbCatUsuarioBll.ObtenerTbCatUsuario(3, 0, gestionMovil.usuario);
                gestor = TBCatGestoresBll.ObtenerTBCatGestores(3, 0, 0, usuario[0].IdUsuario);
                CatResultado = CatResultadoGestionBll.ObtenerCatResultadoGestion(5, gestionMovil.resultado);

                gestion.IdCuenta = cuenta[0].IdCuenta;
                gestion.FchGestion = gestionMovil.FechaGestion;
                gestion.IdGestor = gestor[0].IdGestor;
                gestion.Comentario = gestionMovil.comentarios;
                gestion.IdResultadoGestion = CatResultado[0].IdResultado;
                gestion.TipoGestion = "CAMPO";
                gestion.idCNT = 0;
                gestion.CNT = gestionMovil.Tipocontacto;
                gestion.idCampana = 0;
                gestion.latitud = gestionMovil.latitud;
                gestion.longitud = gestionMovil.longitud;

                try
                {
                    GestionBll.InsertarGestion(gestion);

                }
                catch
                {
                    Mensaje = Mensaje + "Error en " + Convert.ToInt32(gestion.IdCuenta);
                }




            }

            return Mensaje;
        }
        public static InsertarGestionConResultadoResponse InsertarGestionConResultado(Gestion gestion)
        {
            InsertarGestionConResultadoResponse respuesta = new InsertarGestionConResultadoResponse();
            try
            {
                if (gestion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (gestion.IdTipoGestion <= 0) throw new ValidacionExcepcion("La clave de descripción de gestion (tipo) no es válida");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var tipoGestion = con.Gestiones_TipoGestion.Find(gestion.IdTipoGestion);
                    if (tipoGestion == null || tipoGestion.IdTipoGestion <= 0) throw new ValidacionExcepcion("La clave de descripción de gestion (tipo) no es válida (>)");

                    if (gestion.IdResultadoGestion <= 0)
                    {

                        var resultadoRegistrado = con.Gestiones_TB_CATResultadoGestion.SingleOrDefault(res => res.TipoGestion == tipoGestion.IdTipoGestion && res.Descripcion == "Registrado");
                        if (resultadoRegistrado == null || resultadoRegistrado.IdResultado <= 0) respuesta.MensajeOperacion += "No se encontro resultado de tipo 'Registrado'";

                        gestion.IdResultadoGestion = resultadoRegistrado.IdResultado;

                    }
                    int respuestaInsert = InsertarGestion(gestion);
                    if (respuestaInsert <= 0)
                    {
                        respuesta.Error = true;
                        respuesta.MensajeOperacion += "No fue posible insertar la gestión";
                    }
                    else
                    {
                        respuesta.IdGestion = respuestaInsert;
                    }

                    //Validar si el tipo de gestion es una promesa, si lo es, eliminar cualquier notificacion que se haya registrado para una promesa anterior
                    bool tipoGestionPromesa = con.Gestiones_TB_TipoGestionPromesa.Any(tgp => tgp.IdTipoGestion == tipoGestion.IdTipoGestion);
                    if (tipoGestionPromesa)
                    {
                        var notificaciones = (from g in con.Gestiones_TB_Gestion
                                              join pp in con.Gestiones_TB_PromesaPago on g.IdGestion equals pp.idGestion
                                              join sn in con.Sistema_TB_Notificacion on g.IdGestion equals sn.IdGestion
                                              where g.idCuenta == gestion.IdCuenta
                                                   && g.IdTipoGestion == tipoGestion.IdTipoGestion
                                                   && pp.Estatus > 1
                                                   && sn.Enviado == false
                                                   && sn.Intentos < sn.MaxIntentos
                                              select sn).OrderByDescending(r => r.IdNotificacion).ToList();

                        if (notificaciones != null)
                        {
                            foreach (var notificacion in notificaciones)
                            {
                                notificacion.Intentos = notificacion.MaxIntentos;
                            }

                            con.SaveChanges();
                        }

                    }
                }
            }

            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(gestion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        #region GestionesNuevo
        public static ObtenerDatosClienteGestionResponse ObtenerDatosClienteGestion(ObtenerDatosClienteGestionRequest peticion)
        {
            return dal.ObtenerDatosClienteGestion(peticion);
        }

        public static ImpresionNotificacionResponse ImpresionAvisoCobranza(ImpresionAvisoCobranzaRequest peticion)
        {
            ImpresionNotificacionResponse respuesta = new ImpresionNotificacionResponse();
            try
            {
                if (peticion.Correo)
                {
                    _ = Task.Factory.StartNew(() =>
                    {
                        GenerarEnvioCorreo(new AltaNotificacionSolicitudNotificacionRequest()
                        {
                            IdUsuario = peticion.IdUsuario,
                            IdSolicitud = peticion.IdSolicitud,
                            Clave = peticion.Clave,
                            IdDestino = peticion.IdDestino,
                            Courier = peticion.Courier,
                            Actualizar = peticion.Actualizar
                        });
                    });
                }
                else
                {
                    respuesta = ProcesarAvisoCobranza(peticion);
                }
                TB_CATDestinoNotificacion dest = new TB_CATDestinoNotificacion();
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    dest = con.Gestiones_TB_CATDestinoNotificacion.Where(d => d.IdDestinoNotificacion == peticion.IdDestino && d.Activo).FirstOrDefault();
                    if (dest == null) throw new Exception("No fue posible obtener un destino de notificación válido.");
                }
                string comentario = string.Empty;
                comentario = $"Envío de Notificaciones: se creó una notificación con la solicitud {peticion.IdSolicitud.ToString()}" +
                    $", con clave de notificación {peticion.Clave}" +
                    $", fue enviada con destino a {dest.DestinoNotificacion}" +
                    $"; {(peticion.Correo ? "" : "no ")}fue programada para envío por correo" +
                    $", {(peticion.Courier ? "" : "no ")}fue programada para envío por Courier" +
                    $", y {(peticion.Actualizar ? "" : "no ")}fue programada para actualizar datos.";
                Respuesta respuestaGestion = InsertarGestionEnvioNotificacion(peticion.IdUsuario, peticion.IdSolicitud, comentario, peticion.Courier);
                respuesta.Error = respuestaGestion.Error;
                respuesta.MensajeOperacion = respuestaGestion.MensajeOperacion;
                respuesta.MensajeErrorException = respuestaGestion.MensajeErrorException;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionNotificacionResponse ProcesarAvisoCobranza(ImpresionAvisoCobranzaRequest peticion)
        {
            ImpresionNotificacionResponse respuesta = new ImpresionNotificacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El número de solicitud no es correcta.");
                AvisoCobranza avisoCobranza = new AvisoCobranza(peticion.Version);
                AvisoCobranzaVM datosAvisoCobranza = new AvisoCobranzaVM();

                DatosAvisoCobranzaRequest petiDatosAvisoCobranza = new DatosAvisoCobranzaRequest()
                {
                    IdUsuario = peticion.IdUsuario,
                    IdSolicitud = peticion.IdSolicitud,
                    Clave = peticion.Clave,
                    IdDestino = peticion.IdDestino,
                    Actualizar = peticion.Actualizar,
                    Correo = peticion.Correo,
                    Courier = peticion.Courier
                };

                DatosAvisoCobranzaResponse respDatosAvisoCobranza = new DatosAvisoCobranzaResponse();

                respDatosAvisoCobranza = SolicitudNotificacionBLL.DatosAvisoCobranza(petiDatosAvisoCobranza);

                if (respDatosAvisoCobranza.Error)
                    throw new Exception(respDatosAvisoCobranza.MensajeOperacion);

                datosAvisoCobranza.FechaRegistro = respDatosAvisoCobranza.Resultado.FechaRegistro;
                datosAvisoCobranza.NombreCliente = respDatosAvisoCobranza.Resultado.NombreCliente;
                datosAvisoCobranza.Direccion = respDatosAvisoCobranza.Resultado.Direccion;
                datosAvisoCobranza.ReferenciaDomicilio = respDatosAvisoCobranza.Resultado.ReferenciaDomicilio;
                datosAvisoCobranza.TotalDeuda = respDatosAvisoCobranza.Resultado.TotalDeuda;
                datosAvisoCobranza.CuotasVencidas = respDatosAvisoCobranza.Resultado.CuotasVencidas;
                datosAvisoCobranza.IdCliente = respDatosAvisoCobranza.Resultado.IdCliente;
                datosAvisoCobranza.IdDestino = peticion.IdDestino;
                datosAvisoCobranza.IdSolicitud = peticion.IdSolicitud;

                byte[] bArra = avisoCobranza.Generar(datosAvisoCobranza);
                if (bArra == null || bArra.Length == 0) throw new ValidacionExcepcion("Ocurrió un error durante la generación del aviso de cobranza.");

                respuesta.Resultado.ContenidoArchivo = bArra;
                respuesta.TotalRegistros = 1;
#if DEBUG
                Utils.GuardarArchivo($"AvisoCobranza_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar el aviso de cobranza.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionNotificacionResponse ImpresionAvisoPago(ImpresionAvisoPagoRequest peticion)
        {
            ImpresionNotificacionResponse respuesta = new ImpresionNotificacionResponse();
            try
            {
                if (peticion.Correo)
                {
                    _ = Task.Factory.StartNew(() =>
                    {
                        GenerarEnvioCorreo(new AltaNotificacionSolicitudNotificacionRequest()
                        {
                            IdUsuario = peticion.IdUsuario,
                            IdSolicitud = peticion.IdSolicitud,
                            Clave = peticion.Clave,
                            IdDestino = peticion.IdDestino,
                            Courier = peticion.Courier,
                            Actualizar = peticion.Actualizar
                        });
                    });
                }
                else
                {
                    respuesta = ProcesarAvisoPago(peticion);
                }
                TB_CATDestinoNotificacion dest = new TB_CATDestinoNotificacion();
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    dest = con.Gestiones_TB_CATDestinoNotificacion.Where(d => d.IdDestinoNotificacion == peticion.IdDestino && d.Activo).FirstOrDefault();
                    if (dest == null) throw new Exception("No fue posible obtener un destino de notificación válido.");
                }
                string comentario = string.Empty;
                comentario = $"Envío de Notificaciones: la solicitud {peticion.IdSolicitud.ToString()}" +
                    $", con clave de notificación {peticion.Clave}" +
                    $", fue enviada con destino a {dest.DestinoNotificacion}" +
                    $"; {(peticion.Correo ? "" : "no ")}fue programada para envío por correo" +
                    $", {(peticion.Courier ? "" : "no ")}fue programada para envío por Courier" +
                    $", y {(peticion.Actualizar ? "" : "no ")}fue programada para actualizar datos.";
                Respuesta respuestaGestion = InsertarGestionEnvioNotificacion(peticion.IdUsuario, peticion.IdSolicitud, comentario, peticion.Courier);
                respuesta.Error = respuestaGestion.Error;
                respuesta.MensajeOperacion = respuestaGestion.MensajeOperacion;
                respuesta.MensajeErrorException = respuestaGestion.MensajeErrorException;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionNotificacionResponse ProcesarAvisoPago(ImpresionAvisoPagoRequest peticion)
        {
            ImpresionNotificacionResponse respuesta = new ImpresionNotificacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El número de solicitud no es correcta.");
                AvisoPago avisoPago = new AvisoPago(peticion.Version);
                AvisoPagoVM datosAvisoPago = new AvisoPagoVM();

                DatosAvisoPagoRequest petiDatosAvisoPago = new DatosAvisoPagoRequest()
                {
                    IdUsuario = peticion.IdUsuario,
                    IdSolicitud = peticion.IdSolicitud,
                    Clave = peticion.Clave,
                    IdDestino = peticion.IdDestino,
                    Actualizar = peticion.Actualizar,
                    Correo = peticion.Correo,
                    Courier = peticion.Courier
                };

                DatosAvisoPagoResponse respDatosAvisoPago = new DatosAvisoPagoResponse();

                respDatosAvisoPago = SolicitudNotificacionBLL.DatosAvisoPago(petiDatosAvisoPago);

                if (respDatosAvisoPago.Error)
                    throw new Exception(respDatosAvisoPago.MensajeOperacion);

                datosAvisoPago.FechaRegistro = respDatosAvisoPago.Resultado.FechaRegistro;
                datosAvisoPago.NombreCliente = respDatosAvisoPago.Resultado.NombreCliente;
                datosAvisoPago.Direccion = respDatosAvisoPago.Resultado.Direccion;
                datosAvisoPago.ReferenciaDomicilio = respDatosAvisoPago.Resultado.ReferenciaDomicilio;
                datosAvisoPago.TotalDeuda = respDatosAvisoPago.Resultado.TotalDeuda;
                datosAvisoPago.CuotasVencidas = respDatosAvisoPago.Resultado.CuotasVencidas;
                datosAvisoPago.IdCliente = respDatosAvisoPago.Resultado.IdCliente;
                datosAvisoPago.IdDestino = peticion.IdDestino;
                datosAvisoPago.IdSolicitud = peticion.IdSolicitud;

                byte[] bArra = avisoPago.Generar(datosAvisoPago);
                if (bArra == null || bArra.Length == 0) throw new ValidacionExcepcion("Ocurrió un error durante la generación del aviso de pago.");

                respuesta.Resultado.ContenidoArchivo = bArra;
                respuesta.TotalRegistros = 1;
#if DEBUG
                Utils.GuardarArchivo($"AvisoPago_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar el aviso de pago.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionNotificacionResponse ImpresionNotificacionPrejudicial(ImpresionNotificacionPrejudicialRequest peticion)
        {
            ImpresionNotificacionResponse respuesta = new ImpresionNotificacionResponse();
            try
            {
                if (peticion.Correo)
                {
                    _ = Task.Factory.StartNew(() =>
                    {
                        GenerarEnvioCorreo(new AltaNotificacionSolicitudNotificacionRequest()
                        {
                            IdUsuario = peticion.IdUsuario,
                            IdSolicitud = peticion.IdSolicitud,
                            Clave = peticion.Clave,
                            IdDestino = peticion.IdDestino,
                            Courier = peticion.Courier,
                            Actualizar = peticion.Actualizar
                        });
                    });
                }
                else
                {
                    respuesta = ProcesarNotificacionPrejudicial(peticion);
                }
                TB_CATDestinoNotificacion dest = new TB_CATDestinoNotificacion();
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    dest = con.Gestiones_TB_CATDestinoNotificacion.Where(d => d.IdDestinoNotificacion == peticion.IdDestino && d.Activo).FirstOrDefault();
                    if (dest == null) throw new Exception("No fue posible obtener un destino de notificación válido.");
                }
                string comentario = string.Empty;
                comentario = $"Envío de Notificaciones: la solicitud {peticion.IdSolicitud.ToString()}" +
                    $", con clave de notificación {peticion.Clave}" +
                    $", fue enviada con destino a {dest.DestinoNotificacion}" +
                    $"; {(peticion.Correo ? "" : "no ")}fue programada para envío por correo" +
                    $", {(peticion.Courier ? "" : "no ")}fue programada para envío por Courier" +
                    $", y {(peticion.Actualizar ? "" : "no ")}fue programada para actualizar datos.";
                Respuesta respuestaGestion = InsertarGestionEnvioNotificacion(peticion.IdUsuario, peticion.IdSolicitud, comentario, peticion.Courier);
                respuesta.Error = respuestaGestion.Error;
                respuesta.MensajeOperacion = respuestaGestion.MensajeOperacion;
                respuesta.MensajeErrorException = respuestaGestion.MensajeErrorException;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionNotificacionResponse ProcesarNotificacionPrejudicial(ImpresionNotificacionPrejudicialRequest peticion)
        {
            ImpresionNotificacionResponse respuesta = new ImpresionNotificacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El número de solicitud no es correcta.");
                NotificacionPrejudicial notificacionPrejudicial = new NotificacionPrejudicial(peticion.Version);
                NotificacionPrejudicialVM datosNotificacionPrejudicial = new NotificacionPrejudicialVM();

                DatosNotificacionPrejudicialRequest petiDatosNotificacionPrejudicial = new DatosNotificacionPrejudicialRequest()
                {
                    IdUsuario = peticion.IdUsuario,
                    IdSolicitud = peticion.IdSolicitud,
                    Clave = peticion.Clave,
                    IdDestino = peticion.IdDestino,
                    Actualizar = peticion.Actualizar,
                    Correo = peticion.Correo,
                    Courier = peticion.Courier
                };

                DatosNotificacionPrejudicialResponse respDatosNotificacionPrejudicial = new DatosNotificacionPrejudicialResponse();

                respDatosNotificacionPrejudicial = SolicitudNotificacionBLL.DatosNotificacionPrejudicial(petiDatosNotificacionPrejudicial);

                if (respDatosNotificacionPrejudicial.Error)
                    throw new Exception(respDatosNotificacionPrejudicial.MensajeOperacion);

                datosNotificacionPrejudicial.FechaRegistro = respDatosNotificacionPrejudicial.Resultado.FechaRegistro;
                datosNotificacionPrejudicial.NombreCliente = respDatosNotificacionPrejudicial.Resultado.NombreCliente;
                datosNotificacionPrejudicial.Direccion = respDatosNotificacionPrejudicial.Resultado.Direccion;
                datosNotificacionPrejudicial.ReferenciaDomicilio = respDatosNotificacionPrejudicial.Resultado.ReferenciaDomicilio;
                datosNotificacionPrejudicial.TotalDeuda = respDatosNotificacionPrejudicial.Resultado.TotalDeuda;
                datosNotificacionPrejudicial.CuotasVencidas = respDatosNotificacionPrejudicial.Resultado.CuotasVencidas;
                datosNotificacionPrejudicial.IdCliente = respDatosNotificacionPrejudicial.Resultado.IdCliente;
                datosNotificacionPrejudicial.IdDestino = peticion.IdDestino;
                datosNotificacionPrejudicial.IdSolicitud = peticion.IdSolicitud;

                byte[] bArra = notificacionPrejudicial.Generar(datosNotificacionPrejudicial);
                if (bArra == null || bArra.Length == 0) throw new ValidacionExcepcion("Ocurrió un error durante la generación de la notificación.");

                respuesta.Resultado.ContenidoArchivo = bArra;
                respuesta.TotalRegistros = 1;
#if DEBUG
                Utils.GuardarArchivo($"NotificacionPrejudicial_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionNotificacionResponse ImpresionUltimaNotificacionPrejudicial(ImpresionUltimaNotificacionPrejudicialRequest peticion)
        {
            ImpresionNotificacionResponse respuesta = new ImpresionNotificacionResponse();
            try
            {
                if (peticion.Correo)
                {
                    _ = Task.Factory.StartNew(() =>
                    {
                        GenerarEnvioCorreo(new AltaNotificacionSolicitudNotificacionRequest()
                        {
                            IdUsuario = peticion.IdUsuario,
                            IdSolicitud = peticion.IdSolicitud,
                            Clave = peticion.Clave,
                            IdDestino = peticion.IdDestino,
                            Courier = peticion.Courier,
                            Actualizar = peticion.Actualizar
                        });
                    });
                }
                else
                {
                    respuesta = ProcesarUltimaNotificacionPrejudicial(peticion);
                }
                TB_CATDestinoNotificacion dest = new TB_CATDestinoNotificacion();
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    dest = con.Gestiones_TB_CATDestinoNotificacion.Where(d => d.IdDestinoNotificacion == peticion.IdDestino && d.Activo).FirstOrDefault();
                    if (dest == null) throw new Exception("No fue posible obtener un destino de notificación válido.");
                }
                string comentario = string.Empty;
                comentario = $"Envío de Notificaciones: la solicitud {peticion.IdSolicitud.ToString()}" +
                    $", con clave de notificación {peticion.Clave}" +
                    $", fue enviada con destino a {dest.DestinoNotificacion}" +
                    $"; {(peticion.Correo ? "" : "no ")}fue programada para envío por correo" +
                    $", {(peticion.Courier ? "" : "no ")}fue programada para envío por Courier" +
                    $", y {(peticion.Actualizar ? "" : "no ")}fue programada para actualizar datos.";
                Respuesta respuestaGestion = InsertarGestionEnvioNotificacion(peticion.IdUsuario, peticion.IdSolicitud, comentario, peticion.Courier);
                respuesta.Error = respuestaGestion.Error;
                respuesta.MensajeOperacion = respuestaGestion.MensajeOperacion;
                respuesta.MensajeErrorException = respuestaGestion.MensajeErrorException;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionNotificacionResponse ProcesarUltimaNotificacionPrejudicial(ImpresionUltimaNotificacionPrejudicialRequest peticion)
        {
            ImpresionNotificacionResponse respuesta = new ImpresionNotificacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El número de solicitud no es correcta.");
                UltimaNotificacionPrejudicial ultimaNotificacionPrejudicial = new UltimaNotificacionPrejudicial(peticion.Version);
                UltimaNotificacionPrejudicialVM datosUltimaNotificacionPrejudicial = new UltimaNotificacionPrejudicialVM();

                DatosUltimaNotificacionPrejudicialRequest petiDatosUltimaNotificacionPrejudicial = new DatosUltimaNotificacionPrejudicialRequest()
                {
                    IdUsuario = peticion.IdUsuario,
                    IdSolicitud = peticion.IdSolicitud,
                    Clave = peticion.Clave,
                    IdDestino = peticion.IdDestino,
                    Actualizar = peticion.Actualizar,
                    Correo = peticion.Correo,
                    Courier = peticion.Courier
                };

                DatosUltimaNotificacionPrejudicialResponse respDatosUltimaNotificacionPrejudicial = new DatosUltimaNotificacionPrejudicialResponse();

                respDatosUltimaNotificacionPrejudicial = SolicitudNotificacionBLL.DatosUltimaNotificacionPrejudicial(petiDatosUltimaNotificacionPrejudicial);

                if (respDatosUltimaNotificacionPrejudicial.Error)
                    throw new Exception(respDatosUltimaNotificacionPrejudicial.MensajeOperacion);

                datosUltimaNotificacionPrejudicial.FechaRegistro = respDatosUltimaNotificacionPrejudicial.Resultado.FechaRegistro;
                datosUltimaNotificacionPrejudicial.NombreCliente = respDatosUltimaNotificacionPrejudicial.Resultado.NombreCliente;
                datosUltimaNotificacionPrejudicial.Direccion = respDatosUltimaNotificacionPrejudicial.Resultado.Direccion;
                datosUltimaNotificacionPrejudicial.ReferenciaDomicilio = respDatosUltimaNotificacionPrejudicial.Resultado.ReferenciaDomicilio;
                datosUltimaNotificacionPrejudicial.TotalDeuda = respDatosUltimaNotificacionPrejudicial.Resultado.TotalDeuda;
                datosUltimaNotificacionPrejudicial.CuotasVencidas = respDatosUltimaNotificacionPrejudicial.Resultado.CuotasVencidas;
                datosUltimaNotificacionPrejudicial.IdCliente = respDatosUltimaNotificacionPrejudicial.Resultado.IdCliente;
                datosUltimaNotificacionPrejudicial.IdDestino = peticion.IdDestino;
                datosUltimaNotificacionPrejudicial.IdSolicitud = peticion.IdSolicitud;

                byte[] bArra = ultimaNotificacionPrejudicial.Generar(datosUltimaNotificacionPrejudicial);
                if (bArra == null || bArra.Length == 0) throw new ValidacionExcepcion("Ocurrió un error durante la generación de la notificación.");

                respuesta.Resultado.ContenidoArchivo = bArra;
                respuesta.TotalRegistros = 1;
#if DEBUG
                Utils.GuardarArchivo($"UltimaNotificacionPrejudicial_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionNotificacionResponse ImpresionCitacionPrejudicial(ImpresionCitacionPrejudicialRequest peticion)
        {
            ImpresionNotificacionResponse respuesta = new ImpresionNotificacionResponse();
            try
            {
                if (peticion.Correo)
                {
                    _ = Task.Factory.StartNew(() =>
                    {
                        GenerarEnvioCorreo(new AltaNotificacionSolicitudNotificacionRequest()
                        {
                            IdUsuario = peticion.IdUsuario,
                            IdSolicitud = peticion.IdSolicitud,
                            Clave = peticion.Clave,
                            IdDestino = peticion.IdDestino,
                            Courier = peticion.Courier,
                            Actualizar = peticion.Actualizar,
                            FechaHoraCitacion = peticion.FechaHoraCitacion
                        });
                    });
                }
                else
                {
                    respuesta = ProcesarCitacionPrejudicial(peticion);
                }
                TB_CATDestinoNotificacion dest = new TB_CATDestinoNotificacion();
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    dest = con.Gestiones_TB_CATDestinoNotificacion.Where(d => d.IdDestinoNotificacion == peticion.IdDestino && d.Activo).FirstOrDefault();
                    if (dest == null) throw new Exception("No fue posible obtener un destino de notificación válido.");
                }
                string comentario = string.Empty;
                comentario = $"Envío de Notificaciones: la solicitud {peticion.IdSolicitud.ToString()}" +
                    $", con clave de notificación {peticion.Clave}" +
                    $", fue enviada con destino a {dest.DestinoNotificacion}" +
                    $", con fecha y hora de cita de {peticion.FechaHoraCitacion.ToString("dd/MM/yyyy HH:mm")}" +
                    $"; {(peticion.Correo ? "" : "no ")}fue programada para envío por correo" +
                    $", {(peticion.Courier ? "" : "no ")}fue programada para envío por Courier" +
                    $", y {(peticion.Actualizar ? "" : "no ")}fue programada para actualizar datos.";
                Respuesta respuestaGestion = InsertarGestionEnvioNotificacion(peticion.IdUsuario, peticion.IdSolicitud, comentario, peticion.Courier);
                respuesta.Error = respuestaGestion.Error;
                respuesta.MensajeOperacion = respuestaGestion.MensajeOperacion;
                respuesta.MensajeErrorException = respuestaGestion.MensajeErrorException;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionNotificacionResponse ProcesarCitacionPrejudicial(ImpresionCitacionPrejudicialRequest peticion)
        {
            ImpresionNotificacionResponse respuesta = new ImpresionNotificacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El número de solicitud no es correcta.");

                CitacionPrejudicial citacionPrejudicial = new CitacionPrejudicial(peticion.Version);
                CitacionPrejudicialVM datosCitacionPrejudicial = new CitacionPrejudicialVM();

                DatosCitacionPrejudicialRequest petiDatosCitacionPrejudicial = new DatosCitacionPrejudicialRequest()
                {
                    IdUsuario = peticion.IdUsuario,
                    IdSolicitud = peticion.IdSolicitud,
                    Clave = peticion.Clave,
                    IdDestino = peticion.IdDestino,
                    Actualizar = peticion.Actualizar,
                    FechaHoraCitacion = peticion.FechaHoraCitacion,
                    Correo = peticion.Correo,
                    Courier = peticion.Courier
                };

                DatosCitacionPrejudicialResponse respDatosCitacionPrejudicial = new DatosCitacionPrejudicialResponse();

                respDatosCitacionPrejudicial = SolicitudNotificacionBLL.DatosCitacionPrejudicial(petiDatosCitacionPrejudicial);

                if (respDatosCitacionPrejudicial.Error)
                    throw new Exception(respDatosCitacionPrejudicial.MensajeOperacion);

                datosCitacionPrejudicial.FechaRegistro = respDatosCitacionPrejudicial.Resultado.FechaRegistro;
                datosCitacionPrejudicial.NombreCliente = respDatosCitacionPrejudicial.Resultado.NombreCliente;
                datosCitacionPrejudicial.Direccion = respDatosCitacionPrejudicial.Resultado.Direccion;
                datosCitacionPrejudicial.ReferenciaDomicilio = respDatosCitacionPrejudicial.Resultado.ReferenciaDomicilio;
                datosCitacionPrejudicial.TotalDeuda = respDatosCitacionPrejudicial.Resultado.TotalDeuda;
                datosCitacionPrejudicial.CuotasVencidas = respDatosCitacionPrejudicial.Resultado.CuotasVencidas;
                datosCitacionPrejudicial.IdCliente = respDatosCitacionPrejudicial.Resultado.IdCliente;
                datosCitacionPrejudicial.IdDestino = peticion.IdDestino;
                datosCitacionPrejudicial.FechaHoraCitacion = respDatosCitacionPrejudicial.Resultado.FechaHoraCitacion;
                datosCitacionPrejudicial.IdSolicitud = peticion.IdSolicitud;

                byte[] bArra = citacionPrejudicial.Generar(datosCitacionPrejudicial);
                if (bArra == null || bArra.Length == 0) throw new ValidacionExcepcion("Ocurrió un error durante la generación de la notificación.");

                respuesta.Resultado.ContenidoArchivo = bArra;
                respuesta.TotalRegistros = 1;
#if DEBUG
                Utils.GuardarArchivo($"CitacionPrejudicial_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionNotificacionResponse ImpresionPromocionReduccionMora(ImpresionPromocionReduccionMoraRequest peticion)
        {
            ImpresionNotificacionResponse respuesta = new ImpresionNotificacionResponse();
            try
            {
                if (peticion.Correo)
                {
                    _ = Task.Factory.StartNew(() =>
                    {
                        GenerarEnvioCorreo(new AltaNotificacionSolicitudNotificacionRequest()
                        {
                            IdUsuario = peticion.IdUsuario,
                            IdSolicitud = peticion.IdSolicitud,
                            Clave = peticion.Clave,
                            IdDestino = peticion.IdDestino,
                            Courier = peticion.Courier,
                            Actualizar = peticion.Actualizar,
                            DescuentoDeuda = peticion.DescuentoDeuda,
                            FechaVigencia = peticion.FechaVigencia
                        });
                    });
                }
                else
                {
                    respuesta = ProcesarPromocionReduccionMora(peticion);
                }
                TB_CATDestinoNotificacion dest = new TB_CATDestinoNotificacion();
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    dest = con.Gestiones_TB_CATDestinoNotificacion.Where(d => d.IdDestinoNotificacion == peticion.IdDestino && d.Activo).FirstOrDefault();
                    if (dest == null) throw new Exception("No fue posible obtener un destino de notificación válido.");
                }
                string comentario = string.Empty;
                comentario = $"Envío de Notificaciones: la solicitud {peticion.IdSolicitud.ToString()}" +
                    $", con clave de notificación {peticion.Clave}" +
                    $", fue enviada con destino a {dest.DestinoNotificacion}" +
                    $", con un descuento de deuda de S/ {peticion.DescuentoDeuda.Value.ToString()}" +
                    $", con fecha de vigencia de {peticion.FechaVigencia.ToString("dd/MM/yyyy")}" +
                    $"; {(peticion.Correo ? "" : "no ")}fue programada para envío por correo" +
                    $", {(peticion.Courier ? "" : "no ")}fue programada para envío por Courier" +
                    $", y {(peticion.Actualizar ? "" : "no ")}fue programada para actualizar datos.";
                Respuesta respuestaGestion = InsertarGestionEnvioNotificacion(peticion.IdUsuario, peticion.IdSolicitud, comentario, peticion.Courier);
                respuesta.Error = respuestaGestion.Error;
                respuesta.MensajeOperacion = respuestaGestion.MensajeOperacion;
                respuesta.MensajeErrorException = respuestaGestion.MensajeErrorException;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImpresionNotificacionResponse ProcesarPromocionReduccionMora(ImpresionPromocionReduccionMoraRequest peticion)
        {
            ImpresionNotificacionResponse respuesta = new ImpresionNotificacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta.");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El número de solicitud no es correcta.");

                PromocionReduccionMora promocionReduccionMora = new PromocionReduccionMora(peticion.Version);
                PromocionReduccionMoraVM datosPromocionReduccionMora = new PromocionReduccionMoraVM();

                DatosPromocionReduccionMoraRequest petiDatosPromocionReduccionMora = new DatosPromocionReduccionMoraRequest()
                {
                    IdUsuario = peticion.IdUsuario,
                    IdSolicitud = peticion.IdSolicitud,
                    Clave = peticion.Clave,
                    IdDestino = peticion.IdDestino,
                    Actualizar = peticion.Actualizar,
                    DescuentoDeuda = peticion.DescuentoDeuda,
                    FechaVigencia = peticion.FechaVigencia,
                    Correo = peticion.Correo,
                    Courier = peticion.Courier
                };

                DatosPromocionReduccionMoraResponse respDatosPromocionReduccionMora = new DatosPromocionReduccionMoraResponse();

                respDatosPromocionReduccionMora = SolicitudNotificacionBLL.DatosPromocionReduccionMora(petiDatosPromocionReduccionMora);

                if (respDatosPromocionReduccionMora.Error)
                    throw new Exception(respDatosPromocionReduccionMora.MensajeOperacion);

                datosPromocionReduccionMora.FechaRegistro = respDatosPromocionReduccionMora.Resultado.FechaRegistro;
                datosPromocionReduccionMora.NombreCliente = respDatosPromocionReduccionMora.Resultado.NombreCliente;
                datosPromocionReduccionMora.Direccion = respDatosPromocionReduccionMora.Resultado.Direccion;
                datosPromocionReduccionMora.ReferenciaDomicilio = respDatosPromocionReduccionMora.Resultado.ReferenciaDomicilio;
                datosPromocionReduccionMora.TotalDeuda = respDatosPromocionReduccionMora.Resultado.TotalDeuda;
                datosPromocionReduccionMora.CuotasVencidas = respDatosPromocionReduccionMora.Resultado.CuotasVencidas;
                datosPromocionReduccionMora.IdCliente = respDatosPromocionReduccionMora.Resultado.IdCliente;
                datosPromocionReduccionMora.IdDestino = peticion.IdDestino;
                datosPromocionReduccionMora.DescuentoDeuda = respDatosPromocionReduccionMora.Resultado.DescuentoDeuda;
                datosPromocionReduccionMora.FechaVigencia = respDatosPromocionReduccionMora.Resultado.FechaVigencia;
                datosPromocionReduccionMora.IdSolicitud = peticion.IdSolicitud;

                byte[] bArra = promocionReduccionMora.Generar(datosPromocionReduccionMora);
                if (bArra == null || bArra.Length == 0) throw new ValidacionExcepcion("Ocurrió un error durante la generación de la notificación.");

                respuesta.Resultado.ContenidoArchivo = bArra;
                respuesta.TotalRegistros = 1;
#if DEBUG
                Utils.GuardarArchivo($"PromocionReduccionMora_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static TelefonosTipoContactoResponse TelefonosTipoContacto(TelefonosTipoContactoRequest peticion)
        {
            TelefonosTipoContactoResponse respuesta = new TelefonosTipoContactoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido");
                if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("El número de solicitud no es válido");

                List<ContactoTelefonicoVM> contactos = new List<ContactoTelefonicoVM>();
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var cliente = (from s in con.dbo_TB_Solicitudes
                                   join c in con.dbo_TB_Clientes on s.IdCliente equals c.IdCliente
                                   where s.IdSolicitud == peticion.IdSolicitud
                                   select new
                                   {
                                       IdCliente = (int)c.IdCliente,
                                       c.Cliente,
                                       c.lada,
                                       c.telefono,
                                       c.Celular,
                                       c.lada_ofic,
                                       c.tele_ofic,
                                       c.exte_ofic
                                   })
                            .FirstOrDefault();

                    #region Obtener TelefonoContactos
                    long numTel = 0;
                    int lada = 0;
                    if (cliente != null)
                    {
                        #region Cliente
                        _ = long.TryParse(cliente.telefono, out numTel);
                        if (numTel > 0)
                        {
                            contactos.Add(new ContactoTelefonicoVM
                            {
                                TipoContacto = "Cliente",
                                NombreContacto = cliente.Cliente,
                                TipoTelefono = "Fijo",
                                Telefono = $"{cliente.lada}{cliente.telefono}"
                            });
                        }
                        numTel = 0;
                        _ = long.TryParse(cliente.Celular, out numTel);
                        if (numTel > 0)
                        {
                            contactos.Add(new ContactoTelefonicoVM
                            {
                                TipoContacto = "Cliente",
                                NombreContacto = cliente.Cliente,
                                TipoTelefono = "Móvil",
                                Telefono = cliente.Celular
                            });
                        }
                        #endregion

                        #region Referencias
                        var telefonos = (from r in con.Clientes_Referencia
                                         join tr in con.Catalogo_TB_CATTipoVade on new { IdTipoVade = r.IdParentesco, Tipo = 4 } equals new { tr.IdTipoVade, tr.Tipo }
                                         where r.IdCliente == cliente.IdCliente
                                          && r.Activo
                                         select new
                                         {
                                             r.Nombres,
                                             r.ApellidoPaterno,
                                             r.ApellidoMaterno,
                                             r.Lada,
                                             r.Telefono,
                                             r.LadaCelular,
                                             r.TelefonoCelular,
                                             Parentesco = tr.TipoVade
                                         }).ToList();
                        if (telefonos != null)
                        {
                            foreach (var tel in telefonos)
                            {
                                numTel = 0;
                                _ = long.TryParse(tel.Telefono, out numTel);
                                lada = 0;
                                _ = int.TryParse(tel.Lada, out lada);
                                if (numTel > 0)
                                {
                                    contactos.Add(new ContactoTelefonicoVM
                                    {
                                        TipoContacto = "Referencias",
                                        NombreContacto = $"{tel.Nombres} {tel.ApellidoPaterno} {tel.ApellidoMaterno}",
                                        TipoTelefono = "Fijo",
                                        Telefono = $"{(lada > 0 ? tel.Lada : "")}{tel.Telefono}",
                                        Parentesco = tel.Parentesco
                                    });
                                }
                                numTel = 0;
                                _ = long.TryParse(tel.TelefonoCelular, out numTel);
                                lada = 0;
                                _ = int.TryParse(tel.LadaCelular, out lada);
                                if (numTel > 0)
                                {
                                    contactos.Add(new ContactoTelefonicoVM
                                    {
                                        TipoContacto = "Referencias",
                                        NombreContacto = $"{tel.Nombres} {tel.ApellidoPaterno} {tel.ApellidoMaterno}",
                                        TipoTelefono = "Móvil",
                                        Telefono = $"{(lada > 0 ? tel.LadaCelular : "")}{tel.TelefonoCelular}",
                                        Parentesco = tel.Parentesco
                                    });
                                }
                            }
                        }
                        #endregion

                        #region Trabajo
                        numTel = 0;
                        _ = long.TryParse(cliente.tele_ofic, out numTel);
                        lada = 0;
                        _ = int.TryParse(cliente.lada_ofic, out lada);
                        _ = int.TryParse(cliente.exte_ofic, out int extension);
                        if (numTel > 0)
                        {
                            contactos.Add(new ContactoTelefonicoVM
                            {
                                NombreContacto = cliente.Cliente,
                                TipoTelefono = "Trabajo",
                                Telefono = $"{(lada > 0 ? cliente.lada_ofic : "")}{cliente.tele_ofic}",
                                Extension = $"{(extension > 0 ? cliente.exte_ofic : "")}"
                            });
                        }
                        #endregion
                    }
                    #endregion

                    if (peticion.IdTipoContacto > 0)
                    {
                        ObtenerTipoContactosResponse tipoContactos = CatTipoContactoBLL.ObtenerTipoContactos(null);
                        if (tipoContactos != null && tipoContactos.Resultado != null && tipoContactos.Resultado.CatTipoContactos != null)
                        {
                            var tipoContacto = tipoContactos.Resultado.CatTipoContactos.Where(tc => tc.id == peticion.IdTipoContacto)
                                .FirstOrDefault();

                            if (tipoContacto != null && contactos.Count > 0)
                            {
                                contactos = contactos.Where(tc => tc.TipoContacto == tipoContacto.contacto)
                                    .ToList();
                            }
                        }
                    }

                    if (contactos != null) respuesta.Contactos = contactos;
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ResultadosGestionResponse ResultadoGestionTelefonica(ResultadosGestionPeticion peticion)
        {
            ResultadosGestionResponse respuesta = new ResultadosGestionResponse();
            TipoGestion tipoGestion = TipoGestionBLL.TipoGestion("Telefonico");
            if (tipoGestion != null)
            {
                return ResultadosGestion(new ResultadosGestionPeticion
                {
                    IdUsuario = peticion.IdUsuario,
                    IdTipoGestion = tipoGestion.IdTipoGestion
                });
            }
            return respuesta;
        }

        public static ResultadosGestionResponse ResultadosGestion(ResultadosGestionPeticion peticion)
        {
            ResultadosGestionResponse respuesta = new ResultadosGestionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido");
                if (peticion.IdTipoGestion <= 0) throw new ValidacionExcepcion("El tipo de gestión no es válido");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta.ResultadosGestion = con.Gestiones_TB_CATResultadoGestion
                        .Where(r => r.TipoGestion == peticion.IdTipoGestion
                            && (peticion.Estatus == null || r.Estatus == peticion.Estatus))
                        .Select(r => new ResultadoGestionVM
                        {
                            IdResultado = r.IdResultado,
                            Descripcion = r.Descripcion
                        })
                        .ToList();
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ObtenerCarteraAsignadaResponse ObtenerCarteraAsignada(ObtenerCarteraAsignadaRequest peticion)
        {
            return dal.ObtenerCarteraAsignada(peticion);
        }

        public static ConfiguracionNotificacionResponse ConfiguracionNotificacion(ConfiguracionNotificacionRequest peticion)
        {
            ConfiguracionNotificacionResponse respuesta = new ConfiguracionNotificacionResponse();
            try
            {
                if (peticion == null || peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La petición o la clave del usuario no es correcta.");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var horasMinMax = con.dbo_TB_CATParametro
                        .Where(p => new string[] { "MinHoraAgenda", "MaxHoraAgenda" }
                            .Contains(p.Parametro))
                        .ToList();
                    if (horasMinMax != null)
                    {
                        var min = horasMinMax.Where(h => h.Parametro == "MinHoraAgenda").FirstOrDefault();
                        if (min != null)
                            respuesta.HoraMinAgenda = DateTime.Parse($"{DateTime.Now:yyyy-MM-dd} {min.valor}");
                        var max = horasMinMax.Where(h => h.Parametro == "MaxHoraAgenda").FirstOrDefault();
                        if (max != null)
                            respuesta.HoraMaxAgenda = DateTime.Parse($"{DateTime.Now:yyyy-MM-dd} {max.valor}");
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static DevolverLlamadaResponse DevolverLlamada(DevolverLlamadaRequest peticion)
        {
            DevolverLlamadaResponse respuesta = new DevolverLlamadaResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta");
                if (peticion.IdGestion <= 0) throw new ValidacionExcepcion("La clave de la gestión no es válida");
                if (peticion.IdGestoresNotificar == null || peticion.IdGestoresNotificar.Length == 0) peticion.IdGestoresNotificar = new int[] { peticion.IdGestor ?? 0 };
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    DevolverLlamadaAltRequest nuevoDevLlamada = peticion.Convertir<DevolverLlamadaAltRequest>();
                    con.Gestiones_SP_DevolverLlamadaALT(nuevoDevLlamada);
                }
                #region GenerarNotificacion
                _ = Task.Factory.StartNew(() =>
                {
                    ProgramarCorreoDevolverLlamada(peticion);
                });
                #endregion
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static CargarEnviosCourierResponse CargarEnviosCourier()
        {
            CargarEnviosCourierResponse respuesta = new CargarEnviosCourierResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var res = (from s in con.Gestiones_EnvioCourier
                               join u in con.dbo_TB_CATUsuario on s.UsuarioRegistroId equals u.IdUsuario
                               orderby s.FechaRegistro descending
                               select new CargarEnviosCourierVM
                               {
                                   IdEnvioCourier = s.IdEnvioCourier,
                                   UsuarioRegistro = u.Usuario,
                                   FechaRegistro = s.FechaRegistro
                               }).ToList();
                    if (res != null)
                    {
                        respuesta.MensajeOperacion = "Operación exitosa.";
                        respuesta.Resultado.Envios = res;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static VerEnvioCourierDetalleResponse VerEnvio(VerEnvioCourierDetalleRequest peticion)
        {
            VerEnvioCourierDetalleResponse respuesta = new VerEnvioCourierDetalleResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var res = (from e in con.Gestiones_EnvioCourier
                               join ed in con.Gestiones_EnvioCourierDetalle on e.IdEnvioCourier equals ed.IdEnvioCourier
                               join s in con.Gestiones_SolicitudNotificaciones on ed.IdSolicitudNotificacion equals s.IdSolicitudNotificacion
                               join u in con.dbo_TB_CATUsuario on s.UsuarioRegistroId equals u.IdUsuario
                               join n in con.Gestiones_TB_CATNotificaciones on s.NotificacionId equals n.IdNotificacion
                               join d in con.Gestiones_TB_CATDestinoNotificacion on s.DestinoId equals d.IdDestinoNotificacion
                               where e.IdEnvioCourier == peticion.IdEnvioCourier
                               orderby s.FechaRegistro descending
                               select new VerEnvioCourierSolicitudNotificacionVM
                               {
                                   IdEnvioCourier = e.IdEnvioCourier,
                                   IdSolicitudNotificacion = s.IdSolicitudNotificacion,
                                   IdSolicitud = (int)s.SolicitudId,
                                   Notificacion = n.Notificacion,
                                   Destino = d.DestinoNotificacion,
                                   UsuarioRegistroNotificacion = u.Usuario,
                                   FechaRegistroNotificacion = s.FechaRegistro,
                                   Error = ed.Error,
                                   Mensaje = ed.Mensaje
                               }).ToList();
                    if (res != null)
                    {
                        respuesta.MensajeOperacion = "Operación exitosa.";
                        respuesta.Resultado.Envios = res;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ObtenerSolicitudNotificacionesCourierResponse ObtenerSolicitudNotificacionesCourier()
        {
            ObtenerSolicitudNotificacionesCourierResponse respuesta = new ObtenerSolicitudNotificacionesCourierResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var res = (from s in con.Gestiones_SolicitudNotificaciones
                               join sd in con.Gestiones_SolicitudNotificacionDetalle on s.IdSolicitudNotificacion equals sd.SolicitudNotificacionId
                               join u in con.dbo_TB_CATUsuario on s.UsuarioRegistroId equals u.IdUsuario
                               join n in con.Gestiones_TB_CATNotificaciones on s.NotificacionId equals n.IdNotificacion
                               join d in con.Gestiones_TB_CATDestinoNotificacion on s.DestinoId equals d.IdDestinoNotificacion
                               join e in con.Gestiones_EnvioCourierDetalle on s.IdSolicitudNotificacion equals e.IdSolicitudNotificacion into se
                               from e in se.DefaultIfEmpty()
                               where s.EsCourier && (e == null || (e.Error && e.Actual)) && s.Actual
                               orderby s.FechaRegistro descending
                               select new ObtenerSolicitudNotificacionesCourierVM
                               {
                                   IdSolicitudNotificacion = s.IdSolicitudNotificacion,
                                   SolicitudId = (int)s.SolicitudId,
                                   NombreCliente = sd.Nombres,
                                   Notificacion = n.Notificacion,
                                   Destino = d.DestinoNotificacion,
                                   UsuarioRegistroNotificacion = u.Usuario,
                                   FechaRegistroNotificacion = s.FechaRegistro,
                                   Error = e.Error != null ? e.Error : false,
                                   Mensaje = e.Mensaje != null ? e.Mensaje : ""
                               }).ToList();
                    if (res != null)
                    {
                        respuesta.MensajeOperacion = "Operación exitosa.";
                        respuesta.Resultado.Notificaciones = res;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ImprimirNotificacionesCourierResponse ImprimirNotificacionesCourier(ImprimirNotificacionesCourierRequest peticion)
        {
            ImprimirNotificacionesCourierResponse respuesta = new ImprimirNotificacionesCourierResponse();
            try
            {
                List<byte[]> archivos = new List<byte[]>();
                List<int> archivosFallidos = new List<int>();
                byte[] bArra = null;
                foreach (int id in peticion.Notificaciones)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        var res = (from s in con.Gestiones_SolicitudNotificaciones
                                   join sd in con.Gestiones_SolicitudNotificacionDetalle on s.IdSolicitudNotificacion equals sd.SolicitudNotificacionId
                                   join n in con.Gestiones_TB_CATNotificaciones on s.NotificacionId equals n.IdNotificacion
                                   where s.IdSolicitudNotificacion == id && s.Actual
                                   select new
                                   {
                                       n.Clave,
                                       s.FechaRegistro,
                                       sd.Nombres,
                                       sd.ApellidoPaterno,
                                       sd.ApellidoMaterno,
                                       sd.Direccion,
                                       sd.ReferenciaDomicilio,
                                       sd.TotalDeuda,
                                       sd.CuotasVencidas,
                                       sd.IdCliente,
                                       s.DestinoId,
                                       sd.FechaHoraCitacion,
                                       sd.DescuentoDeuda,
                                       sd.FechaVigencia,
                                       s.SolicitudId
                                   }).FirstOrDefault();
                        if (res == null)
                            throw new ValidacionExcepcion("El ID no existe o tiene información inválida.");

                        switch (res.Clave)
                        {
                            case "AC":
                                AvisoCobranza avisoCobranza = new AvisoCobranza(1);
                                AvisoCobranzaVM datosAvisoCobranza = new AvisoCobranzaVM()
                                {
                                    FechaRegistro = res.FechaRegistro,
                                    NombreCliente = string.Format("{0} {1} {2}", res.Nombres, res.ApellidoPaterno, res.ApellidoMaterno),
                                    Direccion = res.Direccion,
                                    ReferenciaDomicilio = res.ReferenciaDomicilio,
                                    TotalDeuda = res.TotalDeuda,
                                    CuotasVencidas = res.CuotasVencidas,
                                    IdCliente = res.IdCliente,
                                    IdDestino = res.DestinoId,
                                    IdSolicitud = Convert.ToInt32(res.SolicitudId)
                                };
                                bArra = avisoCobranza.Generar(datosAvisoCobranza);
                                break;
                            case "AP":
                                AvisoPago avisoPago = new AvisoPago(1);
                                AvisoPagoVM datosAvisoPago = new AvisoPagoVM()
                                {
                                    FechaRegistro = res.FechaRegistro,
                                    NombreCliente = string.Format("{0} {1} {2}", res.Nombres, res.ApellidoPaterno, res.ApellidoMaterno),
                                    Direccion = res.Direccion,
                                    ReferenciaDomicilio = res.ReferenciaDomicilio,
                                    TotalDeuda = res.TotalDeuda,
                                    CuotasVencidas = res.CuotasVencidas,
                                    IdCliente = res.IdCliente,
                                    IdDestino = res.DestinoId,
                                    IdSolicitud = Convert.ToInt32(res.SolicitudId)
                                };
                                bArra = avisoPago.Generar(datosAvisoPago);
                                break;
                            case "NP":
                                NotificacionPrejudicial notificacionPrejudicial = new NotificacionPrejudicial(1);
                                NotificacionPrejudicialVM datosNotificacionPrejudicial = new NotificacionPrejudicialVM()
                                {
                                    FechaRegistro = res.FechaRegistro,
                                    NombreCliente = string.Format("{0} {1} {2}", res.Nombres, res.ApellidoPaterno, res.ApellidoMaterno),
                                    Direccion = res.Direccion,
                                    ReferenciaDomicilio = res.ReferenciaDomicilio,
                                    TotalDeuda = res.TotalDeuda,
                                    CuotasVencidas = res.CuotasVencidas,
                                    IdCliente = res.IdCliente,
                                    IdDestino = res.DestinoId,
                                    IdSolicitud = Convert.ToInt32(res.SolicitudId)
                                };
                                bArra = notificacionPrejudicial.Generar(datosNotificacionPrejudicial);
                                break;
                            case "UNP":
                                UltimaNotificacionPrejudicial ultimaNotificacionPrejudicial = new UltimaNotificacionPrejudicial(1);
                                UltimaNotificacionPrejudicialVM datosUltimaNotificacionPrejudicial = new UltimaNotificacionPrejudicialVM()
                                {
                                    FechaRegistro = res.FechaRegistro,
                                    NombreCliente = string.Format("{0} {1} {2}", res.Nombres, res.ApellidoPaterno, res.ApellidoMaterno),
                                    Direccion = res.Direccion,
                                    ReferenciaDomicilio = res.ReferenciaDomicilio,
                                    TotalDeuda = res.TotalDeuda,
                                    CuotasVencidas = res.CuotasVencidas,
                                    IdCliente = res.IdCliente,
                                    IdDestino = res.DestinoId,
                                    IdSolicitud = Convert.ToInt32(res.SolicitudId)
                                };
                                bArra = ultimaNotificacionPrejudicial.Generar(datosUltimaNotificacionPrejudicial);
                                break;
                            case "CP":
                                CitacionPrejudicial citacionPrejudicial = new CitacionPrejudicial(1);
                                CitacionPrejudicialVM datosCitacionPrejudicial = new CitacionPrejudicialVM()
                                {
                                    FechaRegistro = res.FechaRegistro,
                                    NombreCliente = string.Format("{0} {1} {2}", res.Nombres, res.ApellidoPaterno, res.ApellidoMaterno),
                                    Direccion = res.Direccion,
                                    ReferenciaDomicilio = res.ReferenciaDomicilio,
                                    TotalDeuda = res.TotalDeuda,
                                    CuotasVencidas = res.CuotasVencidas,
                                    IdCliente = res.IdCliente,
                                    IdDestino = res.DestinoId,
                                    FechaHoraCitacion = res.FechaHoraCitacion.Value,
                                    IdSolicitud = Convert.ToInt32(res.SolicitudId)
                                };
                                bArra = citacionPrejudicial.Generar(datosCitacionPrejudicial);
                                break;
                            case "PRM":
                                PromocionReduccionMora promocionReduccionMora = new PromocionReduccionMora(1);
                                PromocionReduccionMoraVM datosPromocionReduccionMora = new PromocionReduccionMoraVM()
                                {
                                    FechaRegistro = res.FechaRegistro,
                                    NombreCliente = string.Format("{0} {1} {2}", res.Nombres, res.ApellidoPaterno, res.ApellidoMaterno),
                                    Direccion = res.Direccion,
                                    ReferenciaDomicilio = res.ReferenciaDomicilio,
                                    TotalDeuda = res.TotalDeuda,
                                    CuotasVencidas = res.CuotasVencidas,
                                    IdCliente = res.IdCliente,
                                    IdDestino = res.DestinoId,
                                    DescuentoDeuda = res.DescuentoDeuda,
                                    FechaVigencia = res.FechaVigencia.Value,
                                    IdSolicitud = Convert.ToInt32(res.SolicitudId)
                                };
                                bArra = promocionReduccionMora.Generar(datosPromocionReduccionMora);
                                break;
                            default:
                                break;
                        }
                        if (bArra == null || bArra.Length == 0)
                        {
                            archivosFallidos.Add(id);
                        }
                        else
                        {
                            archivos.Add(bArra);
                        }
                    }
                }
                if (archivos.Count > 0)
                {
                    respuesta.Resultado.ContenidoArchivo = Utils.CombinarPdfs(archivos);
                    respuesta.TotalRegistros = 1;
#if DEBUG
                    Utils.GuardarArchivo($"EnvioCourier_{Path.GetRandomFileName()}.pdf", respuesta.Resultado.ContenidoArchivo);
#endif
                }
                if (respuesta.Resultado.ContenidoArchivo != null || archivosFallidos.Count > 0)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        EnvioCourier envio = new EnvioCourier()
                        {
                            FechaRegistro = DateTime.Now,
                            UsuarioRegistroId = peticion.IdUsuario
                        };
                        con.Gestiones_EnvioCourier.Add(envio);
                        con.SaveChanges();
                        foreach (int idNotificacion in peticion.Notificaciones)
                        {
                            EnvioCourierDetalle det = con.Gestiones_EnvioCourierDetalle
                                .Where(e => e.IdSolicitudNotificacion == idNotificacion && e.Actual && e.Error).FirstOrDefault();
                            if (det != null)
                                det.Actual = false;
                            bool fallo = archivosFallidos.Contains(idNotificacion);
                            EnvioCourierDetalle envioD = new EnvioCourierDetalle()
                            {
                                IdEnvioCourier = envio.IdEnvioCourier,
                                IdSolicitudNotificacion = idNotificacion,
                                Error = fallo,
                                Mensaje = fallo ? "Ocurrió un error al obtener la notificación." : "Notificación enviada a impresión.",
                                Actual = true
                            };
                            con.Gestiones_EnvioCourierDetalle.Add(envioD);
                        }
                        con.SaveChanges();
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Resultado.ContenidoArchivo = null;
                respuesta.TotalRegistros = 0;
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Resultado.ContenidoArchivo = null;
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ObtenerGestionesResponse ObtenerGestionesPorIdCuenta(ObtenerGestionRequest peticion)
        {
            ObtenerGestionesResponse respuesta = new ObtenerGestionesResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es válida");
                if (peticion.IdCuenta <= 0) throw new ValidacionExcepcion("La clave de la cuenta no es válida");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Gestiones_SP_GestionesCON(peticion.Convertir<ObtenerGestionRequest>());
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static TipoGestionDomiciliacionResponse TipoGestionDomiciliacion()
        {
            TipoGestionDomiciliacionResponse respuesta = new TipoGestionDomiciliacionResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta.TipoGestionDomiciliacion = (from tgd in con.Gestiones_TB_TipoGestionDomiciliacion
                                                          join tg in con.Gestiones_TipoGestion on tgd.IdTipoGestion equals tg.IdTipoGestion
                                                          where tg.Activo
                                                          orderby tg.IdTipoGestion
                                                          select new TipoGestionVM { IdTipoGestion = tg.IdTipoGestion, Tipo = tg.Tipo, Clave = tg.Clave })
                                                         .ToList();
                    if (respuesta.TipoGestionDomiciliacion != null)
                    {
                        foreach (TipoGestionVM tipoGestion in respuesta.TipoGestionDomiciliacion)
                        {
                            tipoGestion.LayoutDomiciliacion = (from tgd in con.Gestiones_TB_TipoGestionDomiciliacion
                                                               join tgda in con.Gestiones_TB_TipoGestionDomLayoutArchivo on tgd.IdTipoGestionDomiciliacion equals tgda.IdTipoGestionDomiciliacion
                                                               join la in con.Cobranza_TB_LayoutArchivo on tgda.IdLayoutArchivo equals la.IdLayoutArchivo
                                                               where tgd.IdTipoGestion == tipoGestion.IdTipoGestion
                                                               select new GestionLayoutArchivoVM
                                                               {
                                                                   IdLayoutArchivo = la.IdLayoutArchivo,
                                                                   NombreLayout = la.NombreLayout,
                                                                   IdBanco = la.IdBanco ?? 0
                                                               })
                                                             .ToList();
                        }
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static TipoGestionPromesaResponse TipoGestionPromesa()
        {
            TipoGestionPromesaResponse respuesta = new TipoGestionPromesaResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta.Tipos = (from tgp in con.Gestiones_TB_TipoGestionPromesa
                                       join tg in con.Gestiones_TipoGestion on tgp.IdTipoGestion equals tg.IdTipoGestion
                                       where tg.Activo
                                       orderby tg.IdTipoGestion
                                       select new TipoGestionVM { IdTipoGestion = tg.IdTipoGestion, Tipo = tg.Tipo, Clave = tg.Clave })
                                                         .ToList();
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static TipoGestionTipoNotificacionReponse TipoGestionTipoNotificacion()
        {
            TipoGestionTipoNotificacionReponse respuesta = new TipoGestionTipoNotificacionReponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta.TipoGestionTipoNotificacion = (from tgtn in con.Gestiones_TB_TipoGestionTipoNotificacion
                                                             select new TipoGestionTipoNotificacionVM
                                                             {
                                                                 IdTipoGestTipoNot = tgtn.IdTipoGestTipoNot,
                                                                 IdTipoGestion = tgtn.IdTipoGestion,
                                                                 IdTipoNotificacion = tgtn.IdTipoNotificacion,
                                                                 Activo = tgtn.Activo,
                                                                 FechaRegistro = tgtn.FechaRegistro
                                                             })
                                                         .ToList();
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static GestionDomiciacionesResponse GestionDomiciaciones(GestionDomiciacionesRequest peticion)
        {
            GestionDomiciacionesResponse respuesta = new GestionDomiciacionesResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (peticion.IdTipoGestion <= 0) throw new ValidacionExcepcion("El tipo de gestión no es válido.");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Gestiones_SP_GestionDomiciliacionCON(peticion.Convertir<GestionDomiciliacionConRequest>());
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static PeticionDomicilacionResponse PeticionDomicilacion(PeticionDomicilacionRequest peticion)
        {
            PeticionDomicilacionResponse respuesta = new PeticionDomicilacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (!peticion.Todo && (peticion.IdGestionDomiciliacion == null || peticion.IdGestionDomiciliacion.Length == 0))
                    throw new ValidacionExcepcion("El tipo de gestión no es válido.");
                if (peticion.IdTipoGestion <= 0) throw new ValidacionExcepcion("El tipo de domiciliación no es váildo");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    string tipoDom = con.Gestiones_TipoGestion.Where(tg => tg.IdTipoGestion == peticion.IdTipoGestion)
                        .Select(tg => tg.Clave)
                        .FirstOrDefault();

                    switch (tipoDom)
                    {
                        case "GenDomBanco":
                            if (peticion.IdLayoutArchivo <= 0) throw new ValidacionExcepcion("El layout de domiciliación no es válido.");
                            respuesta = ProcesoPeticionDomBancario(peticion);
                            break;
                        case "GenDomVisanet":
                            respuesta = ProcesoPeticionDomVisanet(peticion);
                            break;
                        case "GenPagoRef":
                            break;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ConexionTelefonicaResponse ConexionTelefonica(ConexionTelefonicaRequest peticion)
        {
            ConexionTelefonicaResponse respuesta = new ConexionTelefonicaResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");

                ConexionSipGestionResponse respConexion = ConexionSipBLL.ConexionSipGestion(new ConexionSipGestionRequest { IdUsuario = peticion.IdUsuario });
                if (respConexion != null && !respConexion.Error && respConexion.Conexion != null)
                {
                    respuesta.ConexionTelefonica = respConexion.Conexion.Convertir<ConexionTelefonicaVM>();

                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        List<TB_ConexionSipExtension> extensiones = con.Sistema_TB_ConexionSipExtension
                            .Where(se => se.IdConexionSip == respConexion.Conexion.IdConexionSip && se.Activo)
                            .ToList();

                        if (extensiones != null)
                        {
                            if (!extensiones.Any(e => e.Elegible))
                            {
                                foreach (var ex in extensiones)
                                {
                                    ex.Elegible = true;
                                }
                                con.SaveChanges();
                            }

                            TB_ConexionSipExtension disponible = extensiones
                                .Where(e => e.Elegible)
                                .OrderBy(o => o.IdConexionSipExtension)
                                .FirstOrDefault();

                            disponible.Elegible = false;
                            respuesta.ConexionTelefonica.Extension = disponible.Extension;

                            TB_ExtensionSesion sesion = new TB_ExtensionSesion
                            {
                                IdConexionSipExtension = disponible.IdConexionSipExtension,
                                IdUsuario = peticion.IdUsuario,
                                FechaRegistro = DateTime.Now
                            };

                            con.Gestiones_TB_ExtensionSesion.Add(sesion);
                            con.SaveChanges();
                            respuesta.IdExtensionSesion = sesion.IdExtensionSesion;
                        }
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static Respuesta FinalizarSesionTelefonica(ActualizarSesionTelefonicaRequest peticion)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (peticion.IdExtensionSesion <= 0) throw new ValidacionExcepcion("La clave de la sesión no es válida.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_ExtensionSesion sesionTel = con.Gestiones_TB_ExtensionSesion
                        .Where(es => es.IdExtensionSesion == peticion.IdExtensionSesion
                            && es.FechaFin == null)
                        .FirstOrDefault();
                    if (sesionTel != null)
                    {
                        sesionTel.FechaFin = DateTime.Now;
                        con.SaveChanges();
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static Respuesta ActualizarSesionTelefonica(ActualizarSesionTelefonicaRequest peticion)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (peticion.IdExtensionSesion <= 0) throw new ValidacionExcepcion("La clave de la sesión no es válida.");
                if (peticion.IdGestion <= 0) throw new ValidacionExcepcion("La clave de la gestión no es válida.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    TB_ExtensionSesion sesionTel = con.Gestiones_TB_ExtensionSesion
                        .Where(es => es.IdExtensionSesion == peticion.IdExtensionSesion
                            && es.FechaFin != null
                            && es.IdGestion == null)
                        .FirstOrDefault();
                    if (sesionTel != null)
                    {
                        sesionTel.IdGestion = peticion.IdGestion;
                        con.SaveChanges();
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static DevolucionLlamadaGestionResponse DevolucionLlamadaGestion(DevolucionLlamadaGestionRequest peticion)
        {
            DevolucionLlamadaGestionResponse respuesta = new DevolucionLlamadaGestionResponse();
            try
            {
                if (peticion != null && peticion.IdNotificacion > 0)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        respuesta.DevolucionLlamada = (from n in con.Sistema_TB_Notificacion
                                                       join d in con.Gestiones_TB_DevolverLlamada on n.IdGestion equals d.IdGestion
                                                       join s in con.dbo_TB_Creditos on d.IdCuenta equals s.IdCuenta
                                                       join g in con.Gestiones_TB_CatGestores on d.IdGestor equals g.IdGestor
                                                       join u in con.dbo_TB_CATUsuario on g.Usuarioid equals u.IdUsuario
                                                       where n.IdNotificacion == peticion.IdNotificacion
                                                       select new DevolucionLlamadaVM
                                                       {
                                                           Telefono = d.Telefono,
                                                           Tipo = d.Tipo,
                                                           AnexoReferencia = d.AnexoReferencia,
                                                           Comentario = d.Comentario,
                                                           IdSolicitud = (int)s.IdSolicitud,
                                                           NombreUsuario = u.NombreCompleto
                                                       })
                                                       .FirstOrDefault();
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static GestionNotificacionResponse GestionNotificacion(GestionNotificacionRequest peticion)
        {
            GestionNotificacionResponse respuesta = new GestionNotificacionResponse();
            string[] notificacionCorreo = new string[] { "CEGCP", "CEGPR", "CEGCC", "CEGCD" };

            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es váido.");
                if (peticion.IdTipoGestion <= 0) throw new ValidacionExcepcion("El tipo de gestión no es válido.");
                if (peticion.IdTipoNotificacion <= 0) throw new ValidacionExcepcion("El tipo de notificación no es válido.");
                if (peticion.FechaEnviar == new DateTime()) throw new ValidacionExcepcion("La fecha para el envio no es válido.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var tipoGesNot = (from tgn in con.Gestiones_TB_TipoGestionTipoNotificacion
                                      join tn in con.Sistema_TB_TipoNotificacion on tgn.IdTipoNotificacion equals tn.IdTipoNotificacion
                                      where tgn.IdTipoGestion == peticion.IdTipoGestion
                                      select new
                                      {
                                          tgn.IdTipoNotificacion,
                                          tn.Clave,
                                          tn.TituloCorreo,
                                          tn.CorreoCopia
                                      })
                                      .ToList();

                    if (tipoGesNot == null || tipoGesNot.Count == 0) throw new ValidacionExcepcion("No fue posible encontrar un configuración para la notificación.");

                    #region Notificaciones
                    #region Correos
                    if (tipoGesNot.Any(tn => notificacionCorreo.Contains(tn.Clave)))
                    {
                        #region Usuarios
                        List<string> correos = new List<string>
                        {
                            con.dbo_TB_CATUsuario.Where(u => u.IdUsuario == peticion.IdUsuario).Select(u => u.Email).DefaultIfEmpty("").FirstOrDefault()
                        };

                        var tipoNot = tipoGesNot.Where(tg => tg.IdTipoNotificacion == peticion.IdTipoNotificacion).FirstOrDefault();
                        if (tipoNot != null && !string.IsNullOrEmpty(tipoNot.CorreoCopia))
                        {
                            correos.Add(tipoNot.CorreoCopia);
                        }

                        if (correos != null && correos.Count > 0)
                        {
                            correos = correos.Where(c => !string.IsNullOrEmpty(c)).ToList();
                        }

                        if (correos != null && correos.Count > 0)
                        {
                            NuevoResponse resp = NotificacionBLL.Nuevo(new NuevoRequest
                            {
                                IdUsuarioRegistro = peticion.IdUsuario,
                                IdTipoNotificacion = peticion.IdTipoNotificacion,
                                Titulo = tipoNot.TituloCorreo ?? "Recordatorio",
                                CorreoElectronico = string.Join(";", correos),
                                FechaEnviar = peticion.FechaEnviar,
                                IdGestion = peticion.IdGestion
                            });

                            if (resp.Error)
                            {
                                respuesta.Error = true;
                                respuesta.MensajeOperacion = "No fue posible programar la notificación para el usurio actual.";
                            }
                        }
                        #endregion

                        #region Cliente
                        if (peticion.EnviarCliente == true)
                        {
                            var datosCte = (from g in con.Gestiones_TB_Gestion
                                            join c in con.dbo_TB_Creditos on g.idCuenta equals c.IdCuenta
                                            join s in con.dbo_TB_Solicitudes on c.IdSolicitud equals s.IdSolicitud
                                            join cl in con.dbo_TB_Clientes on s.IdCliente equals cl.IdCliente
                                            where g.IdGestion == peticion.IdGestion
                                            select new { cl.IdCliente, cl.Correo })
                                           .FirstOrDefault();
                            if (datosCte != null && !string.IsNullOrEmpty(datosCte.Correo))
                            {
                                NuevoResponse resp = NotificacionBLL.Nuevo(new NuevoRequest
                                {
                                    IdUsuarioRegistro = peticion.IdUsuario,
                                    IdTipoNotificacion = peticion.IdTipoNotificacion,
                                    Titulo = tipoNot.TituloCorreo ?? "Recordatorio",
                                    IdCliente = datosCte.IdCliente,
                                    CorreoElectronico = datosCte.Correo,
                                    FechaEnviar = peticion.FechaEnviar,
                                    IdGestion = peticion.IdGestion
                                });

                                if (resp.Error)
                                {
                                    respuesta.Error = true;
                                    respuesta.MensajeOperacion = "No fue posible programar la notificación para el usurio actual.";
                                }
                            }
                            else
                            {
                                respuesta.Error = true;
                                respuesta.MensajeOperacion += "No fue posible programar la notificación para el cliente.";
                            }
                        }
                        #endregion
                    }
                    #endregion
                    #endregion
                }

            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ValidarPeticionDomicilacionResponse ValidarPeticionDomicilacion(ValidarPeticionDomicilacionRequest peticion)
        {
            ValidarPeticionDomicilacionResponse respuesta = new ValidarPeticionDomicilacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");
                if (!peticion.Todo && (peticion.IdGestionDomiciliacion == null || peticion.IdGestionDomiciliacion.Length == 0))
                    throw new ValidacionExcepcion("El tipo de gestión no es válido.");
                if (peticion.IdTipoGestion <= 0) throw new ValidacionExcepcion("El tipo de domiciliación no es váildo");

                List<Model.Dbo.EstatusVM> estatusGestPetDom = TB_CATEstatusBLL.EstatusGestionPeticionDom();
                if (estatusGestPetDom == null) throw new ValidacionExcepcion("VCAT01. No fue posible realizar la actualización. Contacte a sopote técnivo.");
                int idEstatus = estatusGestPetDom.Where(es => es.Estatus == peticion.ClaveEstatus).Select(es => es.IdEstatus).DefaultIfEmpty(0).FirstOrDefault();
                if (idEstatus <= 0) throw new ValidacionExcepcion("VCAT02. No fue posible actualizar la peticón. Contacte a sopote técnivo.");
                if (string.IsNullOrEmpty(peticion.Origen)) throw new ValidacionExcepcion("VCAT03. No es posible determinar el origen de la petición.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idEstatusActual = 0;
                    switch (peticion.Origen)
                    {
                        case "ValidacionPeticion":
                            idEstatusActual = estatusGestPetDom.Where(es => es.Estatus == "R").Select(es => es.IdEstatus).DefaultIfEmpty(0).FirstOrDefault();
                            break;
                        case "AutorizaPeticion":
                            idEstatusActual = estatusGestPetDom.Where(es => es.Estatus == "A").Select(es => es.IdEstatus).DefaultIfEmpty(0).FirstOrDefault();
                            break;
                    }
                    #region Obtener solicitudes
                    List<PeticionDomiciliacionVM> cuentaDomiciliar = ObtenerGestionDomiciliacion(con, new PeticionDomicilacionRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        IdTipoGestion = peticion.IdTipoGestion,
                        Todo = peticion.Todo,
                        IdBanco = peticion.IdBanco,
                        IdGestionDomiciliacion = peticion.IdGestionDomiciliacion,
                        IdEstatus = idEstatusActual
                    });


                    if (cuentaDomiciliar == null || cuentaDomiciliar.Count == 0) throw new ValidacionExcepcion("VDAT01. No existen registros para realizar la actualización.");
                    #endregion

                    #region Actualizar el estatus
                    ActualizarGestionDomiciliacionResponse actualizarGestionDom = GestionDomiciliacionBLL.ActualizarGestionDomiciliacion(new ActualizarGestionDomiciliacionRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        IdEstatus = idEstatus,
                        IdGestionDomiciliacion = cuentaDomiciliar.Select(cv => cv.IdGestionDomiciliacion).ToList()
                    });

                    respuesta.Error = actualizarGestionDom.Error;
                    respuesta.MensajeErrorException = actualizarGestionDom.MensajeOperacion;
                    #endregion
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ObtenerPagosGestionesResponse ObtenerPagosGestiones(ObtenerPagosGestionesRequest peticion)
        {
            return dal.ObtenerPagosGestiones(peticion);
        }
        #endregion

        #region Métodos Privados
        private static void GenerarEnvioCorreo(AltaNotificacionSolicitudNotificacionRequest entidad)
        {
            try
            {
                int idTipoNotificacion = 0;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    idTipoNotificacion = con.Sistema_TB_TipoNotificacion.Where(tn => tn.Clave == AppSettings.ClaveSolicitudNotificacion)
                        .Select(tn => tn.IdTipoNotificacion)
                        .DefaultIfEmpty(0)
                        .FirstOrDefault();
                }
                if (idTipoNotificacion == 0) throw new ValidacionExcepcion("No existe el tipo de notificación para envios de correos de solicitud notificaciones en gestiones.");

                AltaNotificacionResponse notificacion = SolicitudNotificacionBLL.AltaNotificacionSolNot(new AltaNotificacionSolicitudNotificacionRequest
                {
                    IdUsuario = entidad.IdUsuario,
                    IdSolicitud = entidad.IdSolicitud,
                    IdTipoNotificacion = idTipoNotificacion,
                    Clave = entidad.Clave,
                    IdDestino = entidad.IdDestino,
                    Courier = entidad.Courier,
                    Actualizar = entidad.Actualizar,
                    FechaHoraCitacion = entidad.FechaHoraCitacion,
                    DescuentoDeuda = entidad.DescuentoDeuda,
                    FechaVigencia = entidad.FechaVigencia
                });
            }
            catch (ValidacionExcepcion vex)
            {
                Utils.EscribirLog($"{vex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                 $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { entidad.IdSolicitud, entidad.IdUsuario })}", JsonConvert.SerializeObject(vex));
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { entidad.IdSolicitud, entidad.IdUsuario })}", JsonConvert.SerializeObject(ex));
            }
        }

        private static void GestionDomiciliacion(Gestion peticion)
        {
            try
            {
                if (peticion == null || peticion.IdGestion <= 0) throw new ValidacionExcepcion("No es posible validar si la gestión es una domiciliación.");
                if (peticion.FechaDomiciliar != null && (peticion.MontoDomiciliar ?? 0) > 0 && peticion.IdCuenta > 0)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        if (con.Gestiones_TB_TipoGestionDomiciliacion.Any(g => g.IdTipoGestion == peticion.IdTipoGestion))
                        {
                            decimal idSolicitud = con.dbo_TB_Creditos.Where(cr => cr.IdCuenta == peticion.IdCuenta)
                                .Select(cr => cr.IdSolicitud)
                                .DefaultIfEmpty(0)
                                .FirstOrDefault();
                            con.Gestiones_TB_GestionDomiciliacion.Add(new TB_GestionDomiciliacion
                            {
                                IdSolicitud = (int)idSolicitud,
                                IdGestion = peticion.IdGestion,
                                IdTipoGestion = peticion.IdTipoGestion,
                                FechaDomiciliar = peticion.FechaDomiciliar ?? DateTime.Now.Date,
                                Monto = peticion.MontoDomiciliar ?? 0,
                                EnviadoDom = false,
                                IdEstatus = 1
                            });
                            con.SaveChanges();
                        }
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                Utils.EscribirLog($"{vex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                 $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(vex));
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
        }

        private static PeticionDomicilacionResponse ProcesoPeticionDomVisanet(PeticionDomicilacionRequest peticion)
        {
            PeticionDomicilacionResponse respuesta = new PeticionDomicilacionResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idLayoutArchivo = (from tgd in con.Gestiones_TB_TipoGestionDomiciliacion
                                           join tl in con.Gestiones_TB_TipoGestionDomLayoutArchivo on tgd.IdTipoGestionDomiciliacion equals tl.IdTipoGestionDomiciliacion
                                           where tgd.IdTipoGestion == peticion.IdTipoGestion
                                             && tgd.Activo
                                             && tl.Activo
                                           select tl.IdLayoutArchivo).DefaultIfEmpty(0).FirstOrDefault();
                    if (idLayoutArchivo <= 0) throw new ValidacionExcepcion("No fue posible encontrar un layout para el archivo de domiciliacion.");

                    #region Obtener solicitudes
                    peticion.IdEstatus = (int)EstatusGestionPeticionDom.Autorizado;
                    List<PeticionDomiciliacionVM> cuentaDomiciliar = ObtenerGestionDomiciliacion(con, peticion);

                    if (cuentaDomiciliar == null || cuentaDomiciliar.Count == 0) throw new ValidacionExcepcion("No existen registros para generar un archivo de domiciliación visanet.");
                    #endregion

                    #region Registrar peticion de domiliciliacion
                    PeticionDomiciliacionResponse peticionDomR = PeticionDomiciliacionBLL.PeticionDomiciliacion(new PeticionDomiciliacionRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        OrigenDomiciliacion = origenDomiciliacion,
                        IdLayuotArchivo = idLayoutArchivo,
                        SolicitudesDomiciliar = cuentaDomiciliar
                    });

                    if (peticionDomR.Error) throw new ValidacionExcepcion(peticionDomR.MensajeOperacion);
                    #endregion

                    #region Generar Layout
                    TB_PeticionDomiciliacion encabezados = con.Cobranza_TB_PeticionDomiciliacion
                        .Where(pd => pd.IdPeticionDomiciliacion == peticionDomR.IdPeticionDomiciliacion).FirstOrDefault();

                    List<TB_PeticionDomiciliacionDetalle> detalle = con.Cobranza_TB_PeticionDomiciliacionDetalle
                        .Where(pd => pd.IdPeticionDomiciliacion == peticionDomR.IdPeticionDomiciliacion).ToList();

                    DescargaArchivoDomiciliacionResponse generaAchivoResp = LayoutArchivoBLL.GenerarArchivoDomiciliacion<object, TB_PeticionDomiciliacionDetalle, object, object>(idLayoutArchivo, encabezados, detalle, null, null);
                    if (generaAchivoResp.Error || generaAchivoResp.Resultado == null
                        || generaAchivoResp.Resultado.ContenidoArchivo == null
                        || generaAchivoResp.Resultado.ContenidoArchivo.Length == 0) throw new ValidacionExcepcion(generaAchivoResp.MensajeOperacion);
                    #endregion

                    #region Actualizar GestionDom y PetcionDom
                    _ = PeticionDomiciliacionBLL.DescargaPeticionDomiciliacion(new DescargaPeticionDomiciliacionRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        IdPeticionDomiciliacion = peticionDomR.IdPeticionDomiciliacion
                    });

                    _ = GestionDomiciliacionBLL.ActualizarGestionDomiciliacion(new ActualizarGestionDomiciliacionRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        EnviadoDom = true,
                        IdPeticionDomiciliacion = peticionDomR.IdPeticionDomiciliacion,
                        IdEstatus = (int)EstatusGestionPeticionDom.ArchivoGenerado,
                        IdGestionDomiciliacion = cuentaDomiciliar.Select(cd => cd.IdGestionDomiciliacion).ToList()
                    });

                    respuesta.ArchivoDomiciliacion = new GenerarArchivoDomiciliacionResultado
                    {
                        NombreArchivo = encabezados.NombreArchivo,
                        ContenidoArchivo = generaAchivoResp.Resultado.ContenidoArchivo,
                        TipoContenido = generaAchivoResp.Resultado.TipoContenido,
                        Extension = generaAchivoResp.Resultado.Extension
                    };
                    #endregion
#if DEBUG
                    new Thread(() => GuardarArchivo(generaAchivoResp))
                    {
                        IsBackground = true
                    }.Start();
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }

        private static PeticionDomicilacionResponse ProcesoPeticionDomBancario(PeticionDomicilacionRequest peticion)
        {
            PeticionDomicilacionResponse respuesta = new PeticionDomicilacionResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    int idLayoutArchivo = 0;
                    TB_LayoutArchivo layoutArchivo = (from tgd in con.Gestiones_TB_TipoGestionDomiciliacion
                                                      join tl in con.Gestiones_TB_TipoGestionDomLayoutArchivo on tgd.IdTipoGestionDomiciliacion equals tl.IdTipoGestionDomiciliacion
                                                      join la in con.Cobranza_TB_LayoutArchivo on tl.IdLayoutArchivo equals la.IdLayoutArchivo
                                                      where tgd.IdTipoGestion == peticion.IdTipoGestion
                                                        && tgd.Activo
                                                        && tl.Activo
                                                        && tl.IdLayoutArchivo == peticion.IdLayoutArchivo
                                                      select la)
                                                      .FirstOrDefault();
                    if (layoutArchivo == null || layoutArchivo.IdLayoutArchivo <= 0) throw new ValidacionExcepcion("No fue posible encontrar un layout para el archivo de domiciliacion.");
                    idLayoutArchivo = layoutArchivo.IdLayoutArchivo;

                    #region Obtener solicitudes
                    peticion.IdEstatus = (int)EstatusGestionPeticionDom.Autorizado;
                    List<PeticionDomiciliacionVM> cuentaDomiciliar = ObtenerGestionDomiciliacion(con, peticion);

                    if (cuentaDomiciliar == null || cuentaDomiciliar.Count == 0) throw new ValidacionExcepcion("No existen registros para generar un archivo de domiciliación.");
                    #endregion

                    #region Registrar peticion de domiliciliacion
                    PeticionDomiciliacionResponse peticionDomR = PeticionDomiciliacionBLL.PeticionDomiciliacion(new PeticionDomiciliacionRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        OrigenDomiciliacion = origenDomiciliacion,
                        IdLayuotArchivo = idLayoutArchivo,
                        SolicitudesDomiciliar = cuentaDomiciliar
                    });

                    if (peticionDomR.Error) throw new ValidacionExcepcion(peticionDomR.MensajeOperacion);
                    #endregion

                    #region Generar Layout
                    TB_PeticionDomiciliacion encabezados = con.Cobranza_TB_PeticionDomiciliacion
                        .Where(pd => pd.IdPeticionDomiciliacion == peticionDomR.IdPeticionDomiciliacion).FirstOrDefault();

                    List<TB_PeticionDomiciliacionDetalle> detalle = con.Cobranza_TB_PeticionDomiciliacionDetalle
                        .Where(pd => pd.IdPeticionDomiciliacion == peticionDomR.IdPeticionDomiciliacion).ToList();

                    DescargaArchivoDomiciliacionResponse generaAchivoResp = LayoutArchivoBLL.GenerarArchivoDomiciliacion<object, TB_PeticionDomiciliacionDetalle, object, object>(idLayoutArchivo, encabezados, detalle, null, null);
                    if (generaAchivoResp.Error || generaAchivoResp.Resultado == null
                        || generaAchivoResp.Resultado.ContenidoArchivo == null
                        || generaAchivoResp.Resultado.ContenidoArchivo.Length == 0) throw new ValidacionExcepcion(generaAchivoResp.MensajeOperacion);
                    #endregion

                    #region Actualizar GestionDom y PetcionDom
                    _ = PeticionDomiciliacionBLL.DescargaPeticionDomiciliacion(new DescargaPeticionDomiciliacionRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        IdPeticionDomiciliacion = peticionDomR.IdPeticionDomiciliacion
                    });

                    _ = GestionDomiciliacionBLL.ActualizarGestionDomiciliacion(new ActualizarGestionDomiciliacionRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        EnviadoDom = true,
                        IdPeticionDomiciliacion = peticionDomR.IdPeticionDomiciliacion,
                        IdEstatus = (int)EstatusGestionPeticionDom.ArchivoGenerado,
                        IdGestionDomiciliacion = cuentaDomiciliar.Select(cd => cd.IdGestionDomiciliacion).ToList()
                    });

                    respuesta.ArchivoDomiciliacion = new GenerarArchivoDomiciliacionResultado
                    {
                        NombreArchivo = encabezados.NombreArchivo,
                        ContenidoArchivo = generaAchivoResp.Resultado.ContenidoArchivo,
                        TipoContenido = generaAchivoResp.Resultado.TipoContenido,
                        Extension = generaAchivoResp.Resultado.Extension
                    };
                    #endregion
#if DEBUG
                    new Thread(() => GuardarArchivo(generaAchivoResp))
                    {
                        IsBackground = true
                    }.Start();
#endif
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        private static void GuardarArchivo(DescargaArchivoDomiciliacionResponse resultado)
        {
            try
            {
                if (resultado.Resultado != null && resultado.Resultado.ContenidoArchivo != null
                    && resultado.Resultado.ContenidoArchivo.Length > 0)
                {
                    string rutaArchivo = Path.Combine(AppSettings.DirectorioLog, "WsSOPF_Inca", $"{resultado.Resultado.NombreArchivo}.{resultado.Resultado.Extension.Replace(".", "")}");
                    if (!Directory.Exists(Path.GetDirectoryName(rutaArchivo)))
                        Directory.CreateDirectory(Path.GetDirectoryName(rutaArchivo));
                    File.WriteAllBytes(rutaArchivo, resultado.Resultado.ContenidoArchivo);
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"Error: {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
        }

        private static List<PeticionDomiciliacionVM> ObtenerGestionDomiciliacion(CreditOrigContext con, PeticionDomicilacionRequest peticion)
        {
            List<PeticionDomiciliacionVM> cuentaDomiciliar = new List<PeticionDomiciliacionVM>();
            GestionDomiciliacionConRequest gestionDomReq = new GestionDomiciliacionConRequest
            {
                IdUsuario = peticion.IdUsuario,
                IdTipoGestion = peticion.IdTipoGestion,
                IdBanco = peticion.IdBanco,
                IdEstatus = peticion.IdEstatus
            };
            if (!peticion.Todo && peticion.IdGestionDomiciliacion != null && peticion.IdGestionDomiciliacion.Length > 0)
            {
                XElement xmlGestionDom = new XElement("GestionDomiciliacion", peticion.IdGestionDomiciliacion.Select(gd =>
                    new XElement("IdGestionDomiciliacion", gd)));
                gestionDomReq.GestionDomiciliacion = xmlGestionDom;
            }

            GestionDomiciacionesResponse gestionDomiciliacion = con.Gestiones_SP_GestionDomiciliacionCON(gestionDomReq);
            if (!gestionDomiciliacion.Error && gestionDomiciliacion.GestionDomiciliaciones != null)
            {
                cuentaDomiciliar = gestionDomiciliacion.GestionDomiciliaciones
                    .Select(gd => new PeticionDomiciliacionVM
                    {
                        IdGestionDomiciliacion = gd.IdGestionDomiciliacion,
                        IdSolicitud = gd.IdSolicitud,
                        Monto = gd.Monto
                    }).ToList();
            }
            return cuentaDomiciliar;
        }

        private static Respuesta InsertarGestionEnvioNotificacion(int idUsuario, int idSolicitud, string comentario, bool esCourier)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                List<TBCatGestores> resulGestor = TBCatGestoresBll.ObtenerTBCatGestores(4, 0, 0, idUsuario);
                if (resulGestor == null || resulGestor.Count == 0) throw new Exception("No fue posible obtener datos del gestor.");
                int idGestor = resulGestor[0].IdGestor;
                TB_Creditos credito = new TB_Creditos();
                TipoGestion tipoGestion = new TipoGestion();
                TB_CATResultadoGestion resultadoGestion = new TB_CATResultadoGestion();
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    credito = con.dbo_TB_Creditos.Where(c => c.IdSolicitud == idSolicitud && c.IdEstatus == 11).FirstOrDefault();
                    if (credito == null) throw new Exception("No se encontró la cuenta.");
                    if (esCourier)
                    {
                        tipoGestion = con.Gestiones_TipoGestion.Where(t => t.Clave == "EnvioNotCurrier" && t.Activo).FirstOrDefault();
                        if (tipoGestion == null) throw new Exception("No se encontró el tipo gestión para dar de alta.");
                    }
                    else
                    {
                        tipoGestion = con.Gestiones_TipoGestion.Where(t => t.Clave == "EnvioNotificacion" && t.Activo).FirstOrDefault();
                        if (tipoGestion == null) throw new Exception("No se encontró el tipo gestión para dar de alta.");
                    }
                    resultadoGestion = con.Gestiones_TB_CATResultadoGestion.Where(r => r.TipoGestion.HasValue && r.TipoGestion.Value == tipoGestion.IdTipoGestion && r.Descripcion == "Actualización enviada" && r.Estatus.HasValue && r.Estatus.Value).FirstOrDefault();
                    if (resultadoGestion == null) throw new Exception("No se encontró el resultado de gestión para dar de alta.");
                }
                Gestion gestionDB = new Gestion
                {
                    IdCuenta = credito.IdCuenta,
                    IdGestor = idGestor,
                    Comentario = comentario,
                    IdResultadoGestion = resultadoGestion.IdResultado,
                    IdTipoGestion = tipoGestion.IdTipoGestion,
                    idCNT = 0
                };
                int idGestion = GestionBll.InsertarGestion(gestionDB);
                if (idGestion > 0)
                {
                    respuesta.MensajeOperacion = idGestion.ToString();
                    return respuesta;
                }
                else
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "No se pudo insertar la gestión.";
                    respuesta.MensajeErrorException = "No se pudo insertar la gestión.";
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de dar de alta la gestión.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: " + idUsuario.ToString() + idSolicitud.ToString() + comentario, JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        private static void ProgramarCorreoDevolverLlamada(DevolverLlamadaRequest peticion)
        {
            try
            {
                List<string> correoElectronico = new List<string>();
                if (peticion != null && peticion.Fch_DevLlamada != null && peticion.IdGestoresNotificar != null)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        correoElectronico = (from g in con.Gestiones_TB_CatGestores
                                             join u in con.dbo_TB_CATUsuario on g.Usuarioid equals u.IdUsuario
                                             where peticion.IdGestoresNotificar.Contains(g.IdGestor)
                                             select u.Email).ToList();
                    }

                    if (correoElectronico != null && correoElectronico.Count > 0)
                    {
                        correoElectronico = correoElectronico.Where(c => !string.IsNullOrEmpty(c)).ToList();
                    }
                    if (correoElectronico != null && correoElectronico.Count > 0)
                    {
                        NotificacionBLL.DevolucionLlamada(new DevolucionLlamadaRequest
                        {
                            IdGestion = peticion.IdGestion,
                            CorreoElectronico = string.Join(";", correoElectronico),
                            FechaEnviar = peticion.Fch_DevLlamada ?? DateTime.Now.AddMinutes(5)
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: GestionBLL\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
        }
        #endregion
    }
}
