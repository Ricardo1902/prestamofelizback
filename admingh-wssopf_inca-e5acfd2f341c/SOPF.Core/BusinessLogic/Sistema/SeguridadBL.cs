﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.DataAccess.Sistema;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Sistema;
using SOPF.Core.Model;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response.Reportes;
using SOPF.Core.Model.Response.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Sistema
{
    public static class SeguridadBL
    {
        private static SeguridadDal dal;

        static SeguridadBL()
        {
            dal = new SeguridadDal();
        }

        #region "Usuarios"

        public static Resultado<List<UsuarioEntity>> FiltrarUsuario(UsuarioEntity entity)
        {
            return dal.FiltrarUsuario(entity);
        }

        public static Resultado<UsuarioEntity> ObtenerUsuario(UsuarioEntity entity)
        {
            Resultado<UsuarioEntity> objReturn = new Resultado<UsuarioEntity>();

            try
            {
                entity.IdEstatus = -1;
                Resultado<List<UsuarioEntity>> res = dal.FiltrarUsuario(entity);

                if (res.Codigo > 1)
                    throw new Exception(res.Mensaje);

                if (res.ResultObject.Count() <= 0)
                    throw new Exception("ID Usuario Proporcionado no se puede obtener");

                objReturn.ResultObject = res.ResultObject.First();
                objReturn.ResultObject.MenuAcceso = dal.ObtenerMenuUsuario(entity);
                objReturn.ResultObject.ReportesAcceso = dal.ObtenerReportesUsuario(entity);
                objReturn.ResultObject.AccionesAcceso = dal.ObtenerAccionesUsuario(entity);
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;

            }

            return objReturn;
        }

        public static Resultado<UsuarioEntity> GuardarUsuario(UsuarioEntity entity)
        {
            Resultado<UsuarioEntity> objReturn = new Resultado<UsuarioEntity>();

            objReturn = dal.GuardarUsuario(entity);

            return objReturn;
        }

        public static void InsertarAccesoMenuUsuario(int IdUsuario, int IdMenu)
        {
            dal.InsertarAccesoMenuUsuario(IdUsuario, IdMenu);
        }

        public static void InsertarAccesoReporteUsuario(int IdUsuario, int IdMenu)
        {
            dal.InsertarAccesoReporteUsuario(IdUsuario, IdMenu);
        }

        public static void QuitarAccesoMenuUsuario(int IdUsuario, int IdMenu)
        {
            dal.QuitarAccesoMenuUsuario(IdUsuario, IdMenu);
        }

        public static void QuitarAccesoReporteUsuario(int IdUsuario, int IdMenu)
        {
            dal.QuitarAccesoReporteUsuario(IdUsuario, IdMenu);
        }

        public static void InsertarAccesoAccionUsuario(int IdUsuario, int IdMenuAccion)
        {
            dal.InsertarAccesoAccionUsuario(IdUsuario, IdMenuAccion);
        }

        public static void QuitarAccesoAccionUsuario(int IdUsuario, int IdMenuAccion)
        {
            dal.QuitarAccesoAccionUsuario(IdUsuario, IdMenuAccion);
        }

        public static AutenticarResponse Autenticar(string usuario, string password)
        {
            AutenticarResponse respuesta = new AutenticarResponse();
            try
            {
                if (string.IsNullOrEmpty(usuario)) throw new ValidacionExcepcion("La cuenta de usuario o password no es válido.");
                if (string.IsNullOrEmpty(password)) throw new ValidacionExcepcion("La cuenta de usuario o password no es válido.");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    UsuariosResponse loginResp = con.Reportes_Usuarios(new Model.Request.Reportes.UsuariosRequest
                    {
                        Accion = 0,
                        Usuario = usuario.ToUpper(),
                        Pass = password.ToUpper()
                    });

                    if (loginResp == null || loginResp.UsuarioLogin == null || loginResp.UsuarioLogin.Exito <= 0)
                        throw new ValidacionExcepcion("El usuario o la contraseña son incorrectos.");
                    else if (loginResp.UsuarioLogin.Exito == 1)
                    {
                        respuesta.IdUsuario = loginResp.UsuarioLogin.Id_Usuario;
                        respuesta.IdSucursal = loginResp.UsuarioLogin.compania_Id;
                        respuesta.IdTipoUsuario = loginResp.UsuarioLogin.Perfil_Id;
                        respuesta.NombreUsuario = loginResp.UsuarioLogin.Nombre;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static Respuesta FinalizarSesion(int idUsuario)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                if (idUsuario <= 0) throw new ValidacionExcepcion("La cuenta de usuario no es válida.");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    UsuariosResponse loginResp = con.Reportes_Usuarios(new Model.Request.Reportes.UsuariosRequest
                    {
                        Accion = 5,
                        Id_Usuario = idUsuario
                    });
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error no controlado.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                   $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion


        #region "Perfiles(Tipo Usuario)"

        public static Resultado<List<TipoUsuarioEntity>> FiltrarTipoUsuario(TipoUsuarioEntity entity)
        {
            return dal.FiltrarTipoUsuario(entity);
        }

        public static List<TipoUsuarioEntity> TipoUsuario_Obtener_Activos()
        {
            Resultado<List<TipoUsuarioEntity>> res = dal.FiltrarTipoUsuario(new TipoUsuarioEntity() { IdEstatus = 1 });
            List<TipoUsuarioEntity> objReturn = new List<TipoUsuarioEntity>();

            if (res.Codigo > 0)
                throw new Exception(res.Mensaje);

            objReturn = res.ResultObject;

            return objReturn;
        }

        #endregion


        #region "Departamentos"

        public static Resultado<List<DepartamentoEntity>> FiltrarDepartamento(DepartamentoEntity entity)
        {
            return dal.FiltrarDepartamento(entity);
        }

        public static List<DepartamentoEntity> Departamentos_Obtener_Activos()
        {
            Resultado<List<DepartamentoEntity>> res = dal.FiltrarDepartamento(new DepartamentoEntity() { IdEstatus = 1 });
            List<DepartamentoEntity> objReturn = new List<DepartamentoEntity>();

            if (res.Codigo > 0)
                throw new Exception(res.Mensaje);

            objReturn = res.ResultObject;

            return objReturn;
        }

        #endregion

        #region "Sucursales"

        public static Resultado<List<SucursalEntity>> FiltrarSucursal(SucursalEntity entity)
        {
            return dal.FiltrarSucursal(entity);
        }

        public static List<SucursalEntity> Sucursales_Obtener_Activos()
        {
            Resultado<List<SucursalEntity>> res = dal.FiltrarSucursal(new SucursalEntity() { IdEstatus = 1 });
            List<SucursalEntity> objReturn = new List<SucursalEntity>();

            if (res.Codigo > 0)
                throw new Exception(res.Mensaje);

            objReturn = res.ResultObject;

            return objReturn;
        }

        #endregion

        #region "Menus"

        public static List<MenuEntity> FiltrarMenu(MenuEntity entity)
        {
            return dal.FiltrarMenu(entity);
        }

        public static List<MenuEntity> Menu_Obtener_Activos()
        {
            return dal.FiltrarMenu(new MenuEntity() { Activo = true });
        }

        #endregion

        #region "Reportes"

        public static List<AccionEntity> FiltrarAccion(AccionEntity entity)
        {
            return dal.FiltrarAccion(entity);
        }

        public static List<ReporteEntity> FiltrarReporte(ReporteEntity entity)
        {
            return dal.FiltrarReporte(entity);
        }

        public static List<ReporteEntity> Reporte_Obtener_Activos()
        {
            return dal.FiltrarReporte(new ReporteEntity() { Activo = true });
        }

        #endregion

        #region Accesos
        public static ValidaRecursoAccesoResponse ValidaRecursoAcceso(ValidaRecursoAccesoRequest peticion)
        {
            ValidaRecursoAccesoResponse respuesta = new ValidaRecursoAccesoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición es incorrecta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es váido.");
                if (string.IsNullOrEmpty(peticion.Recurso)) throw new ValidacionExcepcion("El recurso no es válido.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Sistema_SP_ValidaRecursoAccesoCON(peticion.Convertir<ValidaRecursoAccesoConRequest>());
                }
                if (respuesta.RecursoAcciones != null) respuesta.TotalRegistros = respuesta.RecursoAcciones.Count;
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;

        }
        #endregion
    }
}
