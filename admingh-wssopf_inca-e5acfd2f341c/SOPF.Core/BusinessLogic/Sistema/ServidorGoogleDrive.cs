﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v2;
using Google.Apis.Drive.v2.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Newtonsoft.Json;
using SOPF.Core.Entities;
using SOPF.Core.Extensiones.Sistema;
using SOPF.Core.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using static Google.Apis.Drive.v2.FilesResource;

namespace SOPF.Core.BusinessLogic.Sistema
{
    public class CargaArchivoGoogleDrive : IDisposable
    {
        bool _disposed;
        public string NombreArchivo { get; set; }
        public string Comentario { get; set; }
        public int IdTipoArchivo { get; set; }
        public int IdDocumentoDestino { get; set; }
        public string TipoArchivo { get; set; }
        public int IdServidorArchivos { get; set; }
        public string IdCarpeta { get; set; }
        public byte[] Contenido { get; set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
            }

            // release any unmanaged objects
            // set the object references to null
            Contenido = null;
            _disposed = true;
        }
        ~CargaArchivoGoogleDrive()
        {
            Dispose(false);
        }
    }

    public class ArchivoGoogleDrive
    {
        public string NombreArchivo { get; set; }
        public byte[] Contenido { get; set; }
        public string Extension { get; set; }
        public string Mime { get; set; }
        public string URL { get; set; }
    }
    public class ServidorGoogleDrive : IServidorArchivos<CargaArchivoGoogleDrive, ArchivoGoogleDrive>
    {
        bool _disposed;
        string[] Scopes = { DriveService.Scope.Drive };
        UserCredential credential;
        public ServidorGoogleDrive()
        {
            string strAuthPath = "";
            try
            {

                //strAuthPath = AppDomain.CurrentDomain.BaseDirectory;
                strAuthPath = System.Configuration.ConfigurationManager.AppSettings["Auth_GoogleDrive"];
                using (var stream = new FileStream(strAuthPath + "SistemasOA.json", FileMode.Open, FileAccess.Read))
                {
                    string credPath = strAuthPath + "user_json";

                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        Scopes,
                        "infraGH",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
            }
        }

        public Resultado<bool> SubirArchivo(CargaArchivoGoogleDrive peticion, ref string idArchivo, ref string urlArchivo)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            try
            {
                if (peticion == null) throw new Exception("La petición es inválida");
                if (String.IsNullOrEmpty(peticion.IdCarpeta)) throw new Exception("La carpeta para el archivo es inválida");
                if (String.IsNullOrWhiteSpace(peticion.TipoArchivo)) peticion.TipoArchivo = "text/plain";
                if (peticion.Contenido == null) throw new Exception("El contenido del archivo no es válido");
                if (peticion.Contenido.Length <= 0) throw new Exception("El contenido del archivo no es válido");

                using (peticion)
                {
                    string[] scopes = new string[] { DriveService.Scope.Drive };
                    var service = new DriveService(new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "appDrive",
                    });

                    Google.Apis.Drive.v2.Data.File body = new Google.Apis.Drive.v2.Data.File();
                    body.Title = peticion.NombreArchivo;
                    body.Description = peticion.Comentario;
                    body.MimeType = peticion.TipoArchivo;
                    body.Parents = new List<ParentReference> { new ParentReference() { Id = peticion.IdCarpeta } };
                    MemoryStream stream = new MemoryStream(peticion.Contenido);
                    InsertMediaUpload request = service.Files.Insert(body, stream, peticion.TipoArchivo);
                    var upload = request.Upload();
                    Google.Apis.Drive.v2.Data.File archivoRespuesta = request.ResponseBody;
                    if (archivoRespuesta != null && !String.IsNullOrEmpty(archivoRespuesta.Id))
                    {
                        idArchivo = archivoRespuesta.Id.ToString();
                        urlArchivo = archivoRespuesta.WebContentLink;

                        respuesta.Codigo = 1;
                        respuesta.Mensaje = "El archivo se subió con éxito";
                        respuesta.ResultObject = true;
                    }
                    else
                    {
                        string errorGoogle = (upload.Exception != null) ? upload.Exception.Message : "";

                        respuesta.Codigo = 0;
                        respuesta.Mensaje = "No fue posible subir el archivo: " + errorGoogle;
                        respuesta.ResultObject = false;
                    }
                    stream.Dispose(); //Limpiar el stream
                }
            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.Mensaje = "Ha ocurrido un error al subir el archivo.";
                respuesta.ResultObject = false;


                peticion.Contenido = null; //Limpiar contenido de archivo para log.
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public Respuesta CargarArchivo(CargaArchivoGoogleDrive peticion, ref string idArchivo, ref string urlArchivo)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                if (peticion == null) throw new Exception("La petición es inválida");
                if (string.IsNullOrEmpty(peticion.IdCarpeta)) throw new Exception("La carpeta para el archivo es inválida");
                if (string.IsNullOrWhiteSpace(peticion.TipoArchivo)) peticion.TipoArchivo = "text/plain";
                if (peticion.Contenido == null) throw new Exception("El contenido del archivo no es válido");
                if (peticion.Contenido.Length <= 0) throw new Exception("El contenido del archivo no es válido");

                using (peticion)
                {
                    string[] scopes = new string[] { DriveService.Scope.Drive };
                    var service = new DriveService(new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "appDrive",
                    });

                    Google.Apis.Drive.v2.Data.File body = new Google.Apis.Drive.v2.Data.File
                    {
                        Title = peticion.NombreArchivo,
                        Description = peticion.Comentario,
                        MimeType = peticion.TipoArchivo,
                        Parents = new List<ParentReference> { new ParentReference() { Id = peticion.IdCarpeta } }
                    };
                    MemoryStream stream = new MemoryStream(peticion.Contenido);
                    InsertMediaUpload request = service.Files.Insert(body, stream, peticion.TipoArchivo);
                    var upload = request.Upload();
                    Google.Apis.Drive.v2.Data.File archivoRespuesta = request.ResponseBody;
                    if (archivoRespuesta != null && !String.IsNullOrEmpty(archivoRespuesta.Id))
                    {
                        idArchivo = archivoRespuesta.Id.ToString();
                        urlArchivo = archivoRespuesta.WebContentLink;
                        respuesta.MensajeOperacion = "El archivo se subió con éxito";
                    }
                    else
                    {
                        string errorGoogle = (upload.Exception != null) ? upload.Exception.Message : "";
                        respuesta.Error = true;
                        respuesta.StatusCode = 500;
                        respuesta.MensajeOperacion = "No fue posible subir el archivo: " + errorGoogle;
                    }
                    stream.Dispose(); //Limpiar el stream
                }
            }
            catch (Exception ex)
            {
                respuesta.StatusCode = 500;
                respuesta.MensajeOperacion = "Ha ocurrido un error al subir el archivo.";
                respuesta.Error = true;

                peticion.Contenido = null; //Limpiar contenido de archivo para log.
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public Google.Apis.Drive.v2.Data.File BuscarCarperta(string idDirectorio, string nombreCarpeta)
        {
            Google.Apis.Drive.v2.Data.File folder = null;
            try
            {
                string[] scopes = new string[] { DriveService.Scope.Drive };
                DriveService service = new DriveService(new BaseClientService.Initializer() { HttpClientInitializer = credential, ApplicationName = "appDrive" });
                ListRequest listReq = service.Files.List();
                listReq.Q = $"mimeType='application/vnd.google-apps.folder' AND trashed = false AND title = '{nombreCarpeta}' " +
                    $"AND '{idDirectorio}' in parents";
                listReq.Spaces = "drive";

                FileList archivos = listReq.Execute();
                if (archivos.Items.Count > 0)
                {
                    folder = archivos.Items
                        .Select(a => a)
                        .OrderBy(o => o.CreatedDate)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(new { idDirectorio, nombreCarpeta })}", JsonConvert.SerializeObject(ex));
            }
            return folder;
        }

        public Respuesta CrearCarpeta(CargaArchivoGoogleDrive peticion, ref string idArchivo, ref string urlArchivo)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                if (peticion == null) throw new Exception("La petición es inválida");
                if (string.IsNullOrEmpty(peticion.IdCarpeta)) throw new Exception("La carpeta para el archivo es inválida");
                Google.Apis.Drive.v2.Data.File carpeta = BuscarCarperta(peticion.IdCarpeta, peticion.NombreArchivo);
                if (carpeta != null)
                {
                    idArchivo = carpeta.Id;
                    urlArchivo = carpeta.AlternateLink;
                }
                else
                {
                    using (peticion)
                    {
                        string[] scopes = new string[] { DriveService.Scope.Drive };
                        var service = new DriveService(new BaseClientService.Initializer()
                        {
                            HttpClientInitializer = credential,
                            ApplicationName = "appDrive",
                        });

                        Google.Apis.Drive.v2.Data.File body = new Google.Apis.Drive.v2.Data.File
                        {
                            Title = peticion.NombreArchivo,
                            Description = peticion.Comentario,
                            MimeType = "application/vnd.google-apps.folder",
                            Parents = new List<ParentReference> { new ParentReference() { Id = peticion.IdCarpeta } }
                        };
                        InsertRequest request = service.Files.Insert(body);
                        Google.Apis.Drive.v2.Data.File fileResp = request.Execute();
                        if (fileResp != null && !string.IsNullOrEmpty(fileResp.Id))
                        {
                            idArchivo = fileResp.Id;
                            urlArchivo = fileResp.AlternateLink;
                            respuesta.MensajeOperacion = "El directorio se creó con éxito";
                        }
                        else
                        {
                            respuesta.MensajeOperacion = "No fue posible crear el directorio";
                            respuesta.Error = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public ArchivoGoogleDrive DescargarArchivo(string idArchivo, ref string error)
        {
            ArchivoGoogleDrive archivo = null;
            error = "";
            try
            {
                if (idArchivo == null) throw new Exception("La petición es inválida. el Id del archivo es inválido");

                string[] scopes = new string[] { DriveService.Scope.Drive };
                var service = new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "appDrive",
                });
                Google.Apis.Drive.v2.Data.File archivoGoogle = service.Files.Get(idArchivo).Execute();
                var respuestaArchivo = DownloadFile(archivoGoogle, ref error);
                if (respuestaArchivo != null)
                {
                    var tempBytes = respuestaArchivo.ToByteArray();
                    respuestaArchivo.Dispose(); // Liberar la memoria

                    if (tempBytes != null && tempBytes.Length > 0)
                    {
                        archivo = new ArchivoGoogleDrive();
                        archivo.NombreArchivo = archivoGoogle.Title;
                        archivo.Contenido = tempBytes;
                        archivo.Extension = archivoGoogle.FileExtension;
                        archivo.Mime = archivoGoogle.MimeType;
                    }
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;

                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(idArchivo)}", JsonConvert.SerializeObject(ex));
            }
            return archivo;
        }
        public Resultado<bool> EliminarArchivo(CargaArchivoGoogleDrive archivo)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Download a file and return a string with its content.
        /// </summary>
        /// <param name="authenticator">
        /// Authenticator responsible for creating authorized web requests.
        /// </param>
        /// <param name="file">Drive File instance.</param>
        /// <returns>File's content if successful, null otherwise.</returns>
        public System.IO.Stream DownloadFile(Google.Apis.Drive.v2.Data.File file, ref string error)
        {
            if (!String.IsNullOrEmpty(file.DownloadUrl))
            {
                try
                {
                    var service = new DriveService(new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "appDrive",
                    });

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(file.DownloadUrl));
                    request.Headers.Add("Authorization: Bearer " + credential.Token.AccessToken);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        return response.GetResponseStream();
                    }
                    else
                    {
                        error = response.StatusDescription;
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    Utils.EscribirLog($"Error en {ex.Message + ", " + error}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                        $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
                    return null;
                }
            }
            else
            {
                // The file doesn't have any content stored on Drive.
                error = "No existe el archivo";
                return null;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
            }

            // release any unmanaged objects
            // set the object references to null
            Scopes = null;
            credential = null;
            _disposed = true;
        }
        ~ServidorGoogleDrive()
        {
            Dispose(false);
        }
    }
}
