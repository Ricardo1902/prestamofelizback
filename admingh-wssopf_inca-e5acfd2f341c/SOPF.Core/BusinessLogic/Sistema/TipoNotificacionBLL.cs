﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Poco.Sistema;
using System;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Sistema
{
    public static class TipoNotificacionBLL
    {
        public static TB_TipoNotificacion TipoNotificacion(string clave)
        {
            TB_TipoNotificacion tipo = null;
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    tipo = con.Sistema_TB_TipoNotificacion.Where(tn => tn.Clave == clave).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                       $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(clave)}", JsonConvert.SerializeObject(ex));
            }
            return tipo;
        }
    }
}
