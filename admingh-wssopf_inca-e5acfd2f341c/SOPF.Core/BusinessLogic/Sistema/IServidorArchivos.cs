﻿using SOPF.Core.Entities;
using System;

namespace SOPF.Core.BusinessLogic.Sistema
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">Entidad preparada para insertar en cada tipo de servicio</typeparam>
    public interface IServidorArchivos<T, K>: IDisposable where T: class where K : class
    {
        Resultado<bool> SubirArchivo(T archivo, ref string idArchivo, ref string urlArchivo);
        K DescargarArchivo(string idArchivo, ref string error);
        Resultado<bool> EliminarArchivo(T idArchivo);
    }
}
