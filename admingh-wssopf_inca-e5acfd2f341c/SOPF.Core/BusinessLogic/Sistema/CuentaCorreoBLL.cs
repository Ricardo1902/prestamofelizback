﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response.Sistema;
using SOPF.Core.Model.Sistema;
using SOPF.Core.Poco.Sistema;
using System;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Sistema
{
    public static class CuentaCorreoBLL
    {
        #region Métodos publicos
        #region GET
        public static CuentaCorreoResponse CuentaCorreo(CuentaCorreoRequest peticion)
        {
            CuentaCorreoResponse respuesta = new CuentaCorreoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    IQueryable<TB_CuentaCorreo> filtros = con.Sistema_TB_CuentaCorreo;
                    TB_TipoNotificacion tipoNotificacion;

                    if (peticion.IdCuentaCorreo > 0)
                        filtros = filtros.Where(c => c.IdCuentaCorreo == peticion.IdCuentaCorreo);
                    if (peticion.Activo != null)
                        filtros = filtros.Where(c => c.Activo == peticion.Activo);
                    if (!string.IsNullOrEmpty(peticion.Usuario))
                        filtros = filtros.Where(c => c.Usuario == peticion.Usuario);

                    if (!string.IsNullOrEmpty(peticion.ClaveTipoNotificacion))
                    {
                        tipoNotificacion = con.Sistema_TB_TipoNotificacion.Where(tn => tn.Clave == peticion.ClaveTipoNotificacion)
                            .FirstOrDefault();
                        if (tipoNotificacion != null) peticion.IdTipoNotificacion = tipoNotificacion.IdTipoNotificacion;
                    }
                    if (peticion.IdTipoNotificacion > 0)
                    {
                        var tipoNotCuenta = con.Sistema_TB_TipoNotificacionCuentaCorreo.Where(tnc => tnc.IdTipoNotificacion == peticion.IdTipoNotificacion)
                            .Where(tn => tn.Activo)
                            .OrderByDescending(o => o.FechaRegistro)
                            .FirstOrDefault();
                        if (tipoNotCuenta != null)
                        {
                            filtros = filtros.Where(c => c.IdCuentaCorreo == tipoNotCuenta.IdCuentaCorreo);
                        }
                    }

                    var cuentaCorreo = filtros
                        .Select(c => new CuentaCorreoVM
                        {
                            IdCuentaCorreo = c.IdCuentaCorreo,
                            Servidor = c.Servidor,
                            Puerto = c.Puerto,
                            Usuario = c.Usuario,
                            Password = c.Password,
                            UsaSSL = c.UsaSSL,
                            Activo = c.Activo
                        })
                        .ToList();
                    if (cuentaCorreo != null && cuentaCorreo.Count > 0)
                    {
                        respuesta.Resultado.CuentaCorreo = cuentaCorreo;
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de obtener las cuentas de correo.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST

        #endregion
        #endregion

        #region Métodos Privados

        #endregion
    }
}
