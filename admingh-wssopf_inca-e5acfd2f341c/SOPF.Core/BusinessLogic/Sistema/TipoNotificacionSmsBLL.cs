﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Poco.Sistema;
using System;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Sistema
{
    public static class TipoNotificacionSmsBLL
    {
        public static TB_TipoNotificacionSMS TipoNotificacionSMS(string clave)
        {
            TB_TipoNotificacionSMS tipo = null;
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    tipo = (from tn in con.Sistema_TB_TipoNotificacion
                            join tns in con.Sistema_TB_TipoNotificacionSMS on tn.IdTipoNotificacion equals tns.IdTipoNotificacion
                            where tns.Activo
                                && tn.Clave == clave
                            orderby tns.IdTipoNotificacionSMS descending
                            select tns)
                            .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                       $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(clave)}", JsonConvert.SerializeObject(ex));
            }
            return tipo;
        }
    }
}
