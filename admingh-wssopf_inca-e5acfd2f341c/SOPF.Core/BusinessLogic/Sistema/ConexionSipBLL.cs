﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response.Sistema;
using SOPF.Core.Model.Sistema;
using SOPF.Core.Poco.Sistema;
using System;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Sistema
{
    public class ConexionSipBLL
    {
        #region Metodos Publicos
        #region GET
        public static ConexionSipGestionResponse ConexionSipGestion(ConexionSipGestionRequest peticion)
        {
            ConexionSipGestionResponse respuesta = new ConexionSipGestionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es correcta");
                TB_ConexionSip conexion = ConexionSip("SIPG");
                if (conexion != null && conexion.Activo)
                {
                    respuesta.Conexion = new ConexionSipVM
                    {
                        IdConexionSip = conexion.IdConexionSip,
                        Servidor = conexion.Servidor,
                        Puerto = conexion.Puerto,
                        Password = conexion.Password
                    };

                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        respuesta.Conexion.ConexionSdk = con.Sistema_TB_ConexionSipSdk
                            .Where(c => c.IdConexionSip == conexion.IdConexionSip && c.Activo)
                            .Select(c => new ConexionSdkVM { Usuario = c.Usuario, Password = c.Password })
                            .FirstOrDefault();


                    }
                    respuesta.TotalRegistros++;
                }

            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de obtener la conexion";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST

        #endregion
        #endregion

        #region Metodos privados
        private static TB_ConexionSip ConexionSip(string claveConexion)
        {
            TB_ConexionSip conexionSip = null;
            try
            {
                if (string.IsNullOrEmpty(claveConexion)) return null;
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    conexionSip = con.Sistema_TB_ConexionSip.Where(c => c.ClaveConexion == claveConexion)
                        .FirstOrDefault();
                }
            }
            catch (Exception)
            {
            }
            return conexionSip;
        }
        #endregion
    }
}
