﻿using Newtonsoft.Json;
using SOPF.Core.DataAccess;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response.Sistema;
using SOPF.Core.Model.Sistema;
using SOPF.Core.Poco.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Sistema
{
    public static class NotificacionBLL
    {
        #region Metodos publicos
        #region GET
        public static TipoNotificacionResponse TipoNotificacion(TipoNotificacionRequest peticion)
        {
            TipoNotificacionResponse respuesta = new TipoNotificacionResponse();
            try
            {
                if (peticion == null || peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La petición o la clave es incorrecta.");
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta.TipoNotificacion = con.Sistema_TB_TipoNotificacion
                        .Where(tn => tn.Clave == peticion.Clave
                            && (peticion.Activo == null || tn.Activo == peticion.Activo.Value))
                        .Select(tn => new Model.Sistema.TipoNotificacionVM
                        {
                            IdTipoNotificacion = tn.IdTipoNotificacion,
                            Clave = tn.Clave,
                            EnvioProgramado = tn.EnvioProgramado,
                            HorasRecurrencia = tn.HorasRecurrencia,
                            MinutosRecurrencia = tn.MinutosRecurrencia
                        })
                        .FirstOrDefault();

                    if (respuesta.TipoNotificacion != null) respuesta.TotalRegistros = 1;
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static NotificacionesPendientesResponse Pendientes(NotificacionesPendientesRequest peticion)
        {
            NotificacionesPendientesResponse respuesta = new NotificacionesPendientesResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es váido.");
                if (peticion.IdTipoNotificacion <= 0 && string.IsNullOrEmpty(peticion.TipoNotificacion)) throw new ValidacionExcepcion("El tipo de no notificación no es válido");

                DateTime tiempoEnviosProx = DateTime.Now.AddMinutes(5);

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<TB_TipoNotificacion> notificacionProgramados = con.Sistema_TB_TipoNotificacion.Where(tn => tn.EnvioProgramado).ToList();

                    if (peticion.IdTipoNotificacion == 0 && !string.IsNullOrEmpty(peticion.TipoNotificacion))
                    {
                        int idTipoNotificacion = con.Sistema_TB_TipoNotificacion
                                 .Where(tn => tn.Clave == peticion.TipoNotificacion)
                                 .Select(tn => tn.IdTipoNotificacion)
                                 .DefaultIfEmpty(0)
                                 .FirstOrDefault();
                        peticion.IdTipoNotificacion = idTipoNotificacion;
                    }

                    IQueryable<TB_Notificacion> notificionFiltro = con.Sistema_TB_Notificacion
                    .Where(n => n.IdTipoNotificacion == peticion.IdTipoNotificacion
                        && n.Enviado == false
                        && n.Intentos < n.MaxIntentos);

                    if (notificacionProgramados != null && notificacionProgramados.Any(tn => tn.IdTipoNotificacion == peticion.IdTipoNotificacion))
                    {
                        notificionFiltro = notificionFiltro.Where(n => n.FechaEnviar <= tiempoEnviosProx
                            && !(n.Programado ?? false));
                    }

                    if (peticion.RegistrosPorPagina > 0)
                    {
                        notificionFiltro = notificionFiltro.Take(peticion.RegistrosPorPagina);
                    }

                    List<Model.Sistema.NotificacionVM> notificaciones = notificionFiltro.Select(n => new Model.Sistema.NotificacionVM
                    {
                        IdNotificacion = n.IdNotificacion,
                        IdCliente = (int)(n.IdCliente ?? 0),
                        IdSolicitud = n.IdSolicitud,
                        Titulo = n.Titulo,
                        CorreoElectronico = n.CorreoElectronico,
                        NumeroTelefono = n.NumeroTelefono,
                        NombreCliente = n.NombreCliente,
                        CapitalSolicitado = n.CapitalSolicitado,
                        Erogacion = n.Erogacion,
                        TotalRecibos = n.TotalRecibos,
                        Intentos = n.Intentos,
                        MaxIntentos = n.MaxIntentos,
                        Url = n.Url,
                        FechaEnviar = n.FechaEnviar,
                        IdGestion = n.IdGestion ?? 0,
                        CorreoCopia = n.CorreoCopia
                    }).ToList();

                    if (notificaciones != null)
                    {
                        respuesta.Resultado.Notificaciones = notificaciones;
                        respuesta.TotalRegistros = notificaciones.Count;

                        if (respuesta.TotalRegistros > 0)
                        {
                            NotificacionDatos(notificaciones, peticion, con);
                        }
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de obtener las notificaciones pendientes.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion

        #region POST
        public static AltaNotificacionResponse Alta(AltaNotificacionRequest peticion)
        {
            AltaNotificacionResponse respuesta = new AltaNotificacionResponse();
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    respuesta = con.Sistema_SP_TB_NotificacionALT(peticion);
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static AltaNotificacionResponse DevolucionLlamada(DevolucionLlamadaRequest peticion)
        {
            AltaNotificacionResponse respuesta = new AltaNotificacionResponse();
            try
            {
                TB_TipoNotificacion tipoNotificacion = TipoNotificacionBLL.TipoNotificacion("CEGDL");
                if (tipoNotificacion != null)
                {
                    using (CreditOrigContext con = new CreditOrigContext())
                    {
                        con.Sistema_TB_Notificacion.Add(new TB_Notificacion
                        {
                            IdUsuarioRegistro = peticion.IdUsuario,
                            IdTipoNotificacion = tipoNotificacion.IdTipoNotificacion,
                            Titulo = tipoNotificacion.TituloCorreo ?? "Realizar Llamada Telefónica",
                            CorreoElectronico = peticion.CorreoElectronico,
                            IdGestion = peticion.IdGestion ?? 0,
                            FechaEnviar = peticion.FechaEnviar,
                            Enviado = false,
                            MaxIntentos = 3,
                            FechaRegistro = DateTime.Now,
                            Programado = false
                        });
                        con.SaveChanges();
                    }
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static NuevoResponse Nuevo(NuevoRequest peticion)
        {
            NuevoResponse respuesta = new NuevoResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuarioRegistro <= 0) throw new ValidacionExcepcion("La clave del usuario no es váido.");

                TB_Notificacion nuevo = peticion.Convertir<TB_Notificacion>();
                if (nuevo == null) throw new ValidacionExcepcion("No es posible realizar el registro, verifique la información.");

                nuevo.FechaRegistro = DateTime.Now;
                nuevo.Enviado = false;
                nuevo.Intentos = 0;
                nuevo.MaxIntentos = 3;

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    con.Sistema_TB_Notificacion.Add(nuevo);
                    con.SaveChanges();
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de generar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(null)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static ActualizarNotificacionResponse Actualizar(ActualizarNotificacionRequest peticion)
        {
            ActualizarNotificacionResponse respuesta = new ActualizarNotificacionResponse();
            try
            {
                if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es váido.");
                if (peticion.IdNotificacion <= 0) throw new ValidacionExcepcion("La clave de notificación no es correcta.");

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    var notificacion = con.Sistema_TB_Notificacion
                    .Where(n => n.IdNotificacion == peticion.IdNotificacion && n.Enviado == false)
                    .FirstOrDefault();
                    if (notificacion == null) throw new ValidacionExcepcion("No se encontró la notificación.");

                    if (peticion.Enviado)
                    {
                        notificacion.Enviado = true;
                        notificacion.FechaEnvio = DateTime.Now;
                        if (notificacion.Programado != null && peticion.Programado != null)
                            notificacion.Programado = peticion.Programado;
                    }
                    else if (peticion.Enviado == false && peticion.Programado == true)
                    {
                        notificacion.Programado = peticion.Programado;
                    }
                    else
                    {
                        notificacion.Intentos++;
                        notificacion.Programado = false;
                        con.Sistema_TB_NotificacionError.Add(new TB_NotificacionError
                        {
                            IdNotificacion = notificacion.IdNotificacion,
                            FechaRegistro = DateTime.Now,
                            IdUsuarioRegistro = peticion.IdUsuario,
                            MensajeError = peticion.MensajeError
                        });
                    }
                    con.SaveChanges();
                }
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de actualizar la notificación.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static void NotificacionProgramadoReset()
        {
            try
            {
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    List<int> idTipoNotificacion = con.Sistema_TB_TipoNotificacion.Where(tn => tn.EnvioProgramado)
                        .Select(tn => tn.IdTipoNotificacion)
                        .ToList();

                    if (idTipoNotificacion != null)
                    {
                        List<TB_Notificacion> notificaciones = con.Sistema_TB_Notificacion
                            .Where(n => idTipoNotificacion.Contains(n.IdTipoNotificacion)
                                && n.Enviado == false
                                && n.Programado == true).ToList();
                        if (notificaciones != null)
                        {
                            foreach (TB_Notificacion notificacion in notificaciones)
                            {
                                notificacion.Programado = false;
                                notificacion.Intentos = 0;
                            }
                            con.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
            }
        }
        #endregion

        #endregion
        #region Metodos privados
        private static void NotificacionDatos(List<Model.Sistema.NotificacionVM> notificaciones, NotificacionesPendientesRequest peticion, CreditOrigContext con)
        {
            if (notificaciones != null && peticion != null && con != null)
            {
                List<int> idNotificaciones = notificaciones.Select(n => n.IdNotificacion).ToList();
                TipoNotificacion cveTipoNotificacion = EnumExtensions.TryValFromDescription<TipoNotificacion>(peticion.TipoNotificacion);
                switch (cveTipoNotificacion)
                {
                    case Core.TipoNotificacion.DevolucionLlamada:
                        List<DevolucionLlamadaVM> devolucionLlamada = (from n in con.Sistema_TB_Notificacion
                                                                       join d in con.Gestiones_TB_DevolverLlamada on n.IdGestion equals d.IdGestion
                                                                       join s in con.dbo_TB_Creditos on d.IdCuenta equals s.IdCuenta
                                                                       join g in con.Gestiones_TB_CatGestores on d.IdGestor equals g.IdGestor
                                                                       join u in con.dbo_TB_CATUsuario on g.Usuarioid equals u.IdUsuario
                                                                       where idNotificaciones.Contains(n.IdNotificacion)
                                                                       select new DevolucionLlamadaVM
                                                                       {
                                                                           IdNotificacion = n.IdNotificacion,
                                                                           Telefono = d.Telefono,
                                                                           Tipo = d.Tipo,
                                                                           AnexoReferencia = d.AnexoReferencia,
                                                                           Comentario = d.Comentario,
                                                                           IdSolicitud = (int)s.IdSolicitud,
                                                                           NombreUsuario = u.NombreCompleto
                                                                       }).ToList();
                        if (devolucionLlamada != null)
                        {
                            foreach (Model.Sistema.NotificacionVM notificacion in notificaciones)
                            {
                                foreach (DevolucionLlamadaVM devolucion in devolucionLlamada)
                                {
                                    if (notificacion.IdNotificacion == devolucion.IdNotificacion)
                                    {
                                        notificacion.DevolucionLlamada = devolucion;
                                    }
                                }
                            }
                        }
                        break;
                    case Core.TipoNotificacion.CompromisoPago:
                        List<GestionNotificacionVM> getNotifComp = (from n in con.Sistema_TB_Notificacion
                                                                    join g in con.Gestiones_TB_Gestion on n.IdGestion equals g.IdGestion
                                                                    join c in con.dbo_TB_Creditos on g.idCuenta equals c.IdCuenta
                                                                    join s in con.dbo_TB_Solicitudes on c.IdSolicitud equals s.IdSolicitud
                                                                    join cl in con.dbo_TB_Clientes on s.IdCliente equals cl.IdCliente
                                                                    join p in con.Gestiones_TB_PromesaPago on new { g.IdGestion, Estatus = 1 } equals new { IdGestion = p.idGestion ?? 0, Estatus = p.Estatus ?? 0 } into p1
                                                                    from lp in p1.DefaultIfEmpty()
                                                                    where idNotificaciones.Contains(n.IdNotificacion)
                                                                    select new GestionNotificacionVM
                                                                    {
                                                                        IdNotificacion = n.IdNotificacion,
                                                                        IdSolicitud = (int)s.IdSolicitud,
                                                                        IdCuenta = (int)(g.idCuenta ?? 0),
                                                                        NombreCliente = cl.Cliente,
                                                                        Dni = cl.NumeroDocumento,
                                                                        Monto = lp == null ? 0 : (lp.MontoPromesa ?? 0)
                                                                    }).ToList();
                        if (getNotifComp != null)
                        {
                            foreach (Model.Sistema.NotificacionVM notificacion in notificaciones)
                            {
                                foreach (GestionNotificacionVM gestNot in getNotifComp)
                                {
                                    if (notificacion.IdNotificacion == gestNot.IdNotificacion)
                                    {
                                        notificacion.GestionNotificacion = gestNot;
                                    }
                                }
                            }
                        }
                        break;
                    case Core.TipoNotificacion.PagoReferenciado:
                    case Core.TipoNotificacion.CitaCliente:
                    case Core.TipoNotificacion.CanceDeuda:
                        List<GestionNotificacionVM> gestionNotificacion = (from n in con.Sistema_TB_Notificacion
                                                                           join g in con.Gestiones_TB_Gestion on n.IdGestion equals g.IdGestion
                                                                           join c in con.dbo_TB_Creditos on g.idCuenta equals c.IdCuenta
                                                                           join s in con.dbo_TB_Solicitudes on c.IdSolicitud equals s.IdSolicitud
                                                                           join cl in con.dbo_TB_Clientes on s.IdCliente equals cl.IdCliente
                                                                           where idNotificaciones.Contains(n.IdNotificacion)
                                                                           select new GestionNotificacionVM
                                                                           {
                                                                               IdNotificacion = n.IdNotificacion,
                                                                               IdSolicitud = (int)s.IdSolicitud,
                                                                               IdCuenta = (int)(g.idCuenta ?? 0),
                                                                               NombreCliente = cl.Cliente,
                                                                               Dni = cl.NumeroDocumento
                                                                           }).ToList();
                        if (gestionNotificacion != null)
                        {
                            foreach (Model.Sistema.NotificacionVM notificacion in notificaciones)
                            {
                                foreach (GestionNotificacionVM gestNot in gestionNotificacion)
                                {
                                    if (notificacion.IdNotificacion == gestNot.IdNotificacion)
                                    {
                                        notificacion.GestionNotificacion = gestNot;
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        }
        #endregion
    }
}
