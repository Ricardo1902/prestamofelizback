﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SOPF.Core.Entities;
using SOPF.Core.Entities.Sistema;
using SOPF.Core.DataAccess.Sistema;

namespace SOPF.Core.BusinessLogic.Sistema
{
    public static class CuadreBL
    {
        private static TBCuadreDal dal;

        static CuadreBL()
        {
            dal = new TBCuadreDal();
        }

        public static Resultado<CuadreDatosSolicitudEntity> ObtenerDatosSolicitud(CuadreDatosSolicitudEntity entity)
        {
            Resultado<CuadreDatosSolicitudEntity> objReturn = new Resultado<CuadreDatosSolicitudEntity>();

            objReturn = dal.ObtenerDatosSolicitud(entity);

            return objReturn;
        }

        public static Resultado<bool> ActualizarDatosSolicitud(CuadreDatosSolicitudEntity entity)
        {
            return dal.ActualizarDatosSolicitud(entity);
        }

        public static Resultado<bool> HRCobranza(CuadreDatosSolicitudEntity entity)
        {
            return BusinessLogic.Cobranza.PagosBL.HardResetCobranzaCuenta(entity.IdSolicitud);
        }

        public static Resultado<bool> ReprocesarTablaAmortizacion(int IdSolicitud, string TipoTabla)
        {
            return dal.ReprocesarTablaAmortizacion(IdSolicitud, TipoTabla);
        }

        public static Resultado<bool> CambioFechaTablaAmortizacion(int IdSolicitud, DateTime FechaInicio)
        {
            return dal.CambioFechaTablaAmortizacion(IdSolicitud, FechaInicio);
        }

        public static Resultado<DatosAmpliacionEntity> ObtenerDatosAmpliacion(int IdSolicitud, int IdSolicitudAmplia, DateTime FechaAmpliacion)
        {
            return dal.CalcularDatosAmpliacion(IdSolicitud, IdSolicitudAmplia, FechaAmpliacion);
        }
    }
}
