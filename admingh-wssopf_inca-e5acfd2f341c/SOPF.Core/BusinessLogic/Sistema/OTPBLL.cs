﻿using Newtonsoft.Json;
using SOPF.Core.BusinessLogic.Clientes;
using SOPF.Core.BusinessLogic.Lleida;
using SOPF.Core.DataAccess;
using SOPF.Core.Model;
using SOPF.Core.Model.Clientes;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response.Cliente;
using SOPF.Core.Model.Response.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.BusinessLogic.Sistema
{
    public static class OTPBLL
    {
        public static EnviarOtpResponse Enviar(EnviarOtpRequest peticion)
        {
            EnviarOtpResponse respuesta = new EnviarOtpResponse();
            try
            {
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válido");
                if (peticion.IdSolicitud <= 0 && string.IsNullOrEmpty(peticion.DNI)
                    && string.IsNullOrEmpty(peticion.Correo) && string.IsNullOrEmpty(peticion.Celular)) throw new ValidacionExcepcion("Proporcione los datos del destinatario.");

                DatosClienteVM cliente = null;
                #region Validaciones
                if (peticion.IdSolicitud > 0 || !string.IsNullOrEmpty(peticion.DNI))
                {
                    BuscarClienteResponse datosCliente = TBClientesBll.BuscarCliente(new Model.Request.Cliente.BuscarClienteRequest
                    {
                        IdUsuario = peticion.IdUsuario,
                        IdSolicitud = peticion.IdSolicitud,
                        DNI = peticion.DNI
                    });

                    if (datosCliente != null && datosCliente.Cliente != null)
                    {
                        cliente = datosCliente.Cliente;
                    }
                }
                else if (!string.IsNullOrEmpty(peticion.Celular) || !string.IsNullOrEmpty(peticion.Correo))
                {
                    cliente = new DatosClienteVM
                    {
                        Email = peticion.Correo,
                        Celular = peticion.Celular
                    };
                }

                if (cliente == null) throw new ValidacionExcepcion("No se encontró la información necesaria para generar el envio.");
                if (peticion.MedioEnvio == MedioEnvioOTP.Ambos && (string.IsNullOrEmpty(cliente.Email) || !Utils.esCorreoValido(cliente.Email) || string.IsNullOrEmpty(cliente.Celular)))
                {
                    throw new ValidacionExcepcion("No se encontró la información necesaria para generar el envio, verifique que cuente con número de celular o correo electrónico.");
                }
                else if (peticion.MedioEnvio == MedioEnvioOTP.Sms && string.IsNullOrEmpty(cliente.Celular))
                {
                    throw new ValidacionExcepcion("Proporcione el número de celular. Número en formato internacional.");
                }
                else if (peticion.MedioEnvio == MedioEnvioOTP.Correo && (string.IsNullOrEmpty(cliente.Email) || !Utils.esCorreoValido(cliente.Email)))
                {
                    throw new ValidacionExcepcion("Proporcione un correo electrónico válido.");
                }

                if (!AppSettings.EsProductivo)
                {
                    if (!string.IsNullOrEmpty(cliente.Celular))
                        cliente.Celular = AppSettings.TelefonoCtePruebas;

                    if (!string.IsNullOrEmpty(cliente.Email))
                        cliente.Email = AppSettings.CorreoEnvioPruebas;
                }

                if (!string.IsNullOrEmpty(cliente.Celular) && !cliente.Celular.StartsWith("+"))
                {
                    cliente.Celular = AppSettings.PrefijoTelefonoInternacional + cliente.Celular;
                }
                #endregion

                #region Envio
                Poco.Sistema.TB_TipoNotificacionSMS tipoNot = TipoNotificacionSmsBLL.TipoNotificacionSMS(peticion.Tipo.GetDescription());
                if (tipoNot == null) throw new ValidacionExcepcion("No fue posible encontrar una configuración para realizar el envio.");

                Model.Lleida.DosFA.SmsVM sms = new Model.Lleida.DosFA.SmsVM
                {
                    Mensaje = tipoNot.Mensaje,
                    NumDestinatario = cliente.Celular
                };

                Model.Lleida.DosFA.CorreoVM correo = new Model.Lleida.DosFA.CorreoVM
                {
                    Remitente = tipoNot.Remitente ?? "Préstamo Feliz",
                    Asunto = tipoNot.AsuntoCorreo ?? "Clave de Registro",
                    Destinatario = cliente.Email,
                    Mensaje = tipoNot.Mensaje
                };

                Model.Request.WsPfLleida.DosFA.EnviarRequest envioOtpReq = new Model.Request.WsPfLleida.DosFA.EnviarRequest
                {
                    TiempoVigencia = tipoNot.TiempoVigencia ?? 300,
                    Formato = tipoNot.Formato,
                    Certificar = tipoNot.Certificar,
                    RazonSocialCertificar = tipoNot.RazonSocialCertificado,
                    NumFiscalCertificar = tipoNot.NumFiscalCertificado,
                    MaxIntentos = tipoNot.MaxIntentos,
                    Longitud = tipoNot.Longitud
                };

                switch (peticion.MedioEnvio)
                {
                    case MedioEnvioOTP.Ambos:
                        envioOtpReq.Sms = sms;
                        envioOtpReq.Correo = correo;
                        break;
                    case MedioEnvioOTP.Sms:
                        envioOtpReq.Sms = sms;
                        break;
                    case MedioEnvioOTP.Correo:
                        envioOtpReq.Correo = correo;
                        break;
                }

                using (CreditOrigContext con = new CreditOrigContext())
                {
                    Poco.Sistema.TB_PeticionOTP nuevo = new Poco.Sistema.TB_PeticionOTP()
                    {
                        IdUsuario = peticion.IdUsuario,
                        IdSolicitud = peticion.IdSolicitud,
                        DNI = peticion.DNI,
                        Celular = peticion.Celular,
                        Correo = peticion.Correo,
                        MedioEnvio = peticion.MedioEnvio.GetDescription(),
                        TipoNotificacion = peticion.Tipo.GetDescription(),
                        FechaRegistro = DateTime.Now,
                        Activo = true
                    };

                    List<Poco.Sistema.TB_PeticionOTP> peticiones = (from p in con.Sistema_TB_PeticionOTP
                                                                    where p.Activo
                                                                      && (peticion.IdSolicitud == 0 || peticion.IdSolicitud == (p.IdSolicitud ?? 0))
                                                                      && (string.IsNullOrEmpty(peticion.DNI) || peticion.DNI == p.DNI)
                                                                      && (string.IsNullOrEmpty(peticion.Celular) || peticion.Celular == p.Celular)
                                                                      && (string.IsNullOrEmpty(peticion.Correo) || peticion.Correo == p.Correo)
                                                                    select p).ToList();
                    if (peticiones != null && peticiones.Count > 0)
                    {
                        foreach (var p in peticiones)
                        {
                            p.Activo = false;
                        }
                    }
                    con.Sistema_TB_PeticionOTP.Add(nuevo);
                    con.SaveChanges();

                    Model.Response.WsPfLleida.DosFA.EnviarResponse enviarResp = DosFABLL.Enviar(envioOtpReq);
                    respuesta.Error = enviarResp.Error;
                    respuesta.MensajeOperacion = enviarResp.Mensaje;
                    nuevo.Mensaje = enviarResp.Mensaje;
                    nuevo.IdPeticion = enviarResp.IdPeticion;
                    con.SaveChanges();

                    if (!enviarResp.Error)
                    {
                        respuesta.IdPeticion = enviarResp.IdPeticion;
                    }
                }
                #endregion

            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
                Utils.EscribirLog($"{vex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(vex));
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }

        public static VerificarOtpResponse Verificar(VerificarOtpRequest peticion)
        {
            VerificarOtpResponse respuesta = new VerificarOtpResponse();
            try
            {
                #region Validaciones
                if (peticion.IdUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válida");
                if (peticion.IdPeticion <= 0) throw new ValidacionExcepcion("La clave de la petición no es válida");
                if (string.IsNullOrEmpty(peticion.Otp)) throw new ValidacionExcepcion("Proporcione la clave otp a validar.");
                #endregion

                #region ValidatOTP
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    Poco.Sistema.TB_PeticionOTP petOtp = con.Sistema_TB_PeticionOTP
                        .Where(p => p.IdPeticion == peticion.IdPeticion
                            && p.Activo)
                        .OrderByDescending(p => p.FechaRegistro)
                        .FirstOrDefault();
                    if (petOtp == null || petOtp.IdPeticionOTP <= 0) throw new ValidacionExcepcion("No fue posible encontrar la petición solicitada.");

                    Model.Response.WsPfLleida.DosFA.VerificarResponse verificarRep = DosFABLL.Verificar(new Model.Request.WsPfLleida.DosFA.VerificarRequest
                    {
                        IdPeticion = peticion.IdPeticion,
                        Otp = peticion.Otp
                    });

                    respuesta.IntentosRestantes = verificarRep.IntentosRestantes;
                    respuesta.Error = verificarRep.Error;
                    respuesta.MensajeOperacion = verificarRep.Mensaje;
                    if (!verificarRep.Error)
                    {
                        petOtp.Activo = false;
                        if (string.IsNullOrEmpty(verificarRep.Mensaje))
                            respuesta.MensajeOperacion = "La clave ha sido verificada correctamente.";
                    }

                    con.Sistema_TB_PeticionOtpValidar.Add(new Poco.Sistema.TB_PeticionOtpValidar
                    {
                        IdPeticionOtp = petOtp.IdPeticionOTP,
                        CodigoOtp = peticion.Otp,
                        IdUsuario = peticion.IdUsuario,
                        FechaRegistro = DateTime.Now,
                        Correcto = !verificarRep.Error,
                        Mensaje = verificarRep.Mensaje
                    });
                    con.SaveChanges();
                }
                #endregion
            }
            catch (ValidacionExcepcion vex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = vex.Message;
                Utils.EscribirLog($"{vex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(vex));
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error inesperado durante el proceso.";
                Utils.EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                  $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
    }
}
