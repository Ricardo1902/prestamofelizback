using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.Configuration;
using System.Threading;
using SOPF.Core.Entities.Pulsus;
using SOPF.Core.DataAccess.Pulsus;

using SOPF.Core.BusinessLogic.Tesoreria;
using SOPF.Core.Entities.Tesoreria;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.DataAccess.Tesoreria;
using SOPF.Core.DataAccess.Creditos;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: ArchivoDispersionBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase ArchivoDispersionDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-17 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Pulsus
{
    public static class ArchivoDispersionBll
    {
        private static ArchivoDispersionDal dal;
        private static CatArchivosDal dala;
        static ArchivoDispersionBll()
        {
            dal = new ArchivoDispersionDal();
            dala = new CatArchivosDal();
        }

        public static void InsertarArchivoDispersion(ArchivoDispersion entidad)
        {
            dal.InsertarArchivoDispersion(entidad);			
        }
        
        public static ArchivoDispersion ObtenerArchivoDispersion()
        {
            return dal.ObtenerArchivoDispersion();
        }
        
        public static void ActualizarArchivoDispersion()
        {
            dal.ActualizarArchivoDispersion();
        }

        public static void EliminarArchivoDispersion()
        {
            dal.EliminarArchivoDispersion();
        }
        public static List<CuentasDispersion> EmpleadosDispersion(int idbanco)
        {
            return dal.EmpleadosDispersion(idbanco);
        }
        public static string GeneraArchivoDispersion(int idBanco, string EMail)
        {
            string Mensaje = "";
            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutSDet = new List<CatArchivosDetalle>();
            List<CuentasDispersion> cuentasDispersar = new List<CuentasDispersion>();

            LayoutGral = dala.ObtenerCatArchivos(idBanco, "T", "S");
            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");
            //LayoutSDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "S");

            cuentasDispersar = EmpleadosDispersion(idBanco);
            List<string> lineas = new List<string>();

            string linea = "";
            string campo = "";
            string Relleno = "";
            int longitud = 0;
            int longitudCampo = 0;
            try
            {
                foreach (CuentasDispersion cuenta in cuentasDispersar)
                {
                    linea = String.Empty;
                    foreach (CatArchivosDetalle detalle in LayoutDet)
                    {

                        switch (detalle.IdCampo)
                        {
                            case 0:
                                campo = detalle.TextiFijo;
                                break;
                            case 1:
                                campo = cuenta.ClaveId;
                                break;
                            case 4:
                                campo = cuenta.RFC;
                                break;
                            case 10:
                                campo = cuenta.Operacion;
                                break;
                            case 11:
                                campo = cuenta.CuentaOrigen;
                                break;
                            case 12:
                                campo = cuenta.CuentaDestino;
                                break;
                            case 13:
                                campo = cuenta.MontoTransferencia.ToString();
                                break;
                            case 14:
                                campo = cuenta.Referencia;
                                break;
                            case 15:
                                campo = cuenta.Descripcion;
                                break;
                            case 16:
                                campo = cuenta.RFC;
                                break;
                            case 17:
                                campo = cuenta.iva.ToString();
                                break;
                            case 18:
                                campo = cuenta.fecha;
                                break;
                            case 19:
                                campo = cuenta.instruccion;
                                break;
                        }
                        Relleno = detalle.Relleno;


                        if (detalle.IdCampo == 12 && cuenta.Operacion == "02")
                        {
                            detalle.Longitud = 10;
                        }

                        longitud = detalle.Longitud;

                        if (campo != null)
                        {
                            longitudCampo = campo.Length;
                        }
                        else
                        {
                            longitudCampo = 0;
                        }

                        if (detalle.IdTipoCampo == "N")
                        {
                            campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                            if (detalle.Incluyepunto == false)
                            {
                                campo.Replace(".", "");

                            }
                            if (detalle.Alineacion == "I")
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                            else
                            {
                                for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                        if (detalle.IdTipoCampo == "T")
                        {


                            if (detalle.Alineacion == "I")
                            {
                                if (detalle.Longitud > 0)
                                {

                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = campo + Relleno;
                                    }
                                }
                            }
                            else
                            {
                                if (detalle.Longitud > 0)
                                {
                                    for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                    {
                                        campo = Relleno + campo;
                                    }
                                }
                            }
                        }
                        campo = String.Format("{0}\t", campo);
                        linea = linea + campo;




                    }
                    linea = linea.Substring(0, linea.Length - 1);
                    lineas.Add(linea);
                    linea = String.Empty;


                }
                //System.IO.File.WriteAllLines(@"C:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                System.IO.File.WriteAllLines(@"C:\AfiliacionBanorte_SOPF\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);
                Mensaje = "1-Archivo Generado con Exito";

                ArchivosDispersion archivo = new ArchivosDispersion();

                archivo.NombreArchivo = "Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt";
                
                archivo.Estatus = 0;

                

                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient("smtpout.secureserver.net");
                client.Port = 3535;
                client.EnableSsl = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("notificaciones@peaceandlovecompany.com", "notifica2013");
                mail.From = new MailAddress("notificaciones@peaceandlovecompany.com");
                mail.To.Add(new MailAddress(EMail.ToString()));
                mail.Subject = "Archivo de Dispersion.";
                mail.Body = "Archivo de Dispersion";

                //Attachment Data = new Attachment(@"C:\Users\Juan Carlos\Documents\DispersionBanorte" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
                Attachment Data = new Attachment(@"C:\AfiliacionBanorte_SOPF\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
                mail.Attachments.Add(Data);

                client.Send(mail);
                //System.IO.File.Delete(@"C:\Users\Juan Carlos\Documents\DispersionBanorte\Dispersion" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");

                return Mensaje;
            }
            catch (Exception ex)
            {
                Mensaje = ex.Message;
                return Mensaje;

            }
        }
    }
}