﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.Configuration;
using System.Threading;
using SOPF.Core.Entities.Pulsus;
using SOPF.Core.DataAccess.Pulsus;
using SOPF.Core.Entities.Tesoreria;
using SOPF.Core.DataAccess.Tesoreria;
using SOPF.Core.BusinessLogic.Tesoreria;

namespace SOPF.Core.BusinessLogic.Pulsus
{
    public class AfiliacionEmpleadosBll
    {
        private static CatArchivosDal dal;
        private static ArchivosDispersionDal dalA;
        private static AfiliacionEmpleadosDal dalE;

        static AfiliacionEmpleadosBll()
        {
            dal = new CatArchivosDal();
            dalA = new ArchivosDispersionDal();
            dalE = new AfiliacionEmpleadosDal();
        }

        public static string GeneraArchivoAfiliacion(int idBanco, string EMail)
        {
            string Mensaje = "";
            CatArchivos LayoutGral = new CatArchivos();
            List<CatArchivosDetalle> LayoutDet = new List<CatArchivosDetalle>();
            List<CatArchivosDetalle> LayoutSDet = new List<CatArchivosDetalle>();
            List<AfiliacionEmpleados> cuentasAfiliar = new List<AfiliacionEmpleados>();

            LayoutGral = dal.ObtenerCatArchivos(idBanco, "A", "S");
            LayoutDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "D");
            LayoutSDet = CatArchivosDetalleBll.ObtenerCatArchivosDetalle(LayoutGral.IdArchivo, "S");

            cuentasAfiliar = Pulsus.AfiliacionEmpleadosBll.ObtenerCuentas(1);
            List<string> lineas = new List<string>();

            string linea = "";
            string campo = "";
            string Relleno = "";
            int longitud = 0;
            int longitudCampo = 0;
            int idAfiliacion = 0;
            foreach (AfiliacionEmpleados cuenta in cuentasAfiliar)
            {
                linea = String.Empty;
                foreach (CatArchivosDetalle detalle in LayoutDet)
                {

                    switch (detalle.IdCampo)
                    {
                        case 0:
                            campo = detalle.TextiFijo;
                            break;
                        case 1:
                            campo = Convert.ToString(cuenta.ClaveId);
                            idAfiliacion = Convert.ToInt32(cuenta.IdAfiliacion);
                            break;
                        case 3:
                            campo = cuenta.Nombre;
                            break;
                        case 4:
                            campo = cuenta.RFC;
                            break;
                        case 5:
                            campo = cuenta.Telefono;
                            break;

                    }
                    Relleno = detalle.Relleno;
                    longitud = detalle.Longitud;

                    if (campo != null)
                    {
                        longitudCampo = campo.Length;
                    }
                    else
                    {
                        longitudCampo = 0;
                    }

                    if (detalle.IdTipoCampo == "N")
                    {
                        campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                        if (detalle.Incluyepunto == false)
                        {
                            campo.Replace(".", "");

                        }
                        if (detalle.Alineacion == "I")
                        {
                            for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                            {
                                campo = campo + Relleno;
                            }
                        }
                        else
                        {
                            for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                            {
                                campo = Relleno + campo;
                            }
                        }
                    }
                    if (detalle.IdTipoCampo == "T")
                    {


                        if (detalle.Alineacion == "I")
                        {
                            if (detalle.Longitud > 0)
                            {

                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                        }
                        else
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                    }
                    campo = String.Format("{0}\t", campo);
                    linea = linea + campo;




                }
                linea = linea.Substring(0, linea.Length - 1);
                lineas.Add(linea);
                linea = String.Empty;
                foreach (CatArchivosDetalle detalle in LayoutSDet)
                {

                    switch (detalle.IdCampo)
                    {
                        case 0:
                            campo = detalle.TextiFijo;
                            break;
                        case 1:
                            campo = Convert.ToString(cuenta.ClaveId);
                            break;
                        case 6:
                            campo = cuenta.TipoCuenta;
                            break;
                        case 7:
                            campo = cuenta.Moneda;
                            break;
                        case 8:
                            campo = cuenta.Banco;
                            break;
                        case 9:
                            campo = cuenta.Clabe;
                            break;

                    }
                    Relleno = detalle.Relleno;

                    if (detalle.IdCampo == 9 && cuenta.TipoCuenta == "001")
                    {
                        detalle.Longitud = 10;
                    }

                    longitud = detalle.Longitud;

                    if (campo != null)
                    {


                        longitudCampo = campo.Length;

                    }
                    else
                    {
                        longitudCampo = 0;
                    }

                    if (detalle.IdTipoCampo == "N")
                    {
                        campo = Convert.ToString(decimal.Round(Convert.ToDecimal(campo.ToString()), detalle.Decimales));
                        if (detalle.Incluyepunto == false)
                        {
                            campo.Replace(".", "");

                        }
                        if (detalle.Alineacion == "I")
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                        }
                        else
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Enteros - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                    }
                    if (detalle.IdTipoCampo == "T")
                    {


                        if (detalle.Alineacion == "I")
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = campo + Relleno;
                                }
                            }
                        }
                        else
                        {
                            if (detalle.Longitud > 0)
                            {
                                for (int i = 1; i <= detalle.Longitud - longitudCampo; i++)
                                {
                                    campo = Relleno + campo;
                                }
                            }
                        }
                    }
                    campo = String.Format("{0}\t", campo);
                    linea = linea + campo;




                }
                linea = linea.Substring(0, linea.Length - 1);
                lineas.Add(linea);
                dalE.ActualizaEmpleado(idAfiliacion);
            }
            System.IO.File.WriteAllLines(@"F:\AfiliacionBanorte_SOPF\" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt", lineas);




            MailMessage mail = new MailMessage();
            SmtpClient client = new SmtpClient("smtpout.secureserver.net");
            client.Port = 3535;
            client.EnableSsl = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("notificaciones@peaceandlovecompany.com", "notifica2013");
            mail.From = new MailAddress("notificaciones@peaceandlovecompany.com");
            mail.To.Add(new MailAddress(EMail.ToString()));
            mail.Subject = "Archivo de Afiliacion.";
            mail.Body = "Archivo de Afiliacion";

            Attachment Data = new Attachment(@"F:\AfiliacionBanorte_SOPF\" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + ".txt");
            mail.Attachments.Add(Data);

            client.Send(mail);

            return Mensaje;
        }
        public static List<AfiliacionEmpleados> ObtenerCuentas(int accion)
        {
            return dalE.CuentasAfiliar(accion);
        }
        public static void insertaEmpleados(List<AfiliacionEmpleados> Empleados)
        {
            

            foreach(AfiliacionEmpleados Empleado in Empleados)
            {
                dalE.insertatEmpleado(Empleado);
            }
        }
        

    }
   
}