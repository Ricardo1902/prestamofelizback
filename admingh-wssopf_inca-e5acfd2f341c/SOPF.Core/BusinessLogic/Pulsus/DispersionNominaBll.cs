#pragma warning disable 150817
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Pulsus;
using SOPF.Core.DataAccess.Pulsus;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: DispersionNominaBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase DispersionNominaDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-17 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Pulsus
{
    public static class DispersionNominaBll
    {
        private static DispersionNominaDal dal;

        static DispersionNominaBll()
        {
            dal = new DispersionNominaDal();
        }

        public static void InsertarDispersionNomina(DispersionNomina entidad)
        {
            dal.InsertarDispersionNomina(entidad);			
        }
        
        public static DispersionNomina ObtenerDispersionNomina()
        {
            return dal.ObtenerDispersionNomina();
        }
        
        public static void ActualizarDispersionNomina()
        {
            dal.ActualizarDispersionNomina();
        }

        public static void EliminarDispersionNomina()
        {
            dal.EliminarDispersionNomina();
        }
    }
}