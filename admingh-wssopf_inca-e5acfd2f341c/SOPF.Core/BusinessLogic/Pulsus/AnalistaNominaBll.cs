#pragma warning disable 150830
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Pulsus;
using SOPF.Core.DataAccess.Pulsus;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: AnalistaNominaBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase AnalistaNominaDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Pulsus
{
    public static class AnalistaNominaBll
    {
        private static AnalistaNominaDal dal;

        static AnalistaNominaBll()
        {
            dal = new AnalistaNominaDal();
        }

        public static void InsertarAnalistaNomina(AnalistaNomina entidad)
        {
            dal.InsertarAnalistaNomina(entidad);			
        }
        
        public static List<AnalistaNomina> ObtenerAnalistaNomina(int Accion, int idUsuario, int IdAnalista)
        {
            return dal.ObtenerAnalistaNomina(Accion, idUsuario, IdAnalista);
        }
        
        public static void ActualizarAnalistaNomina()
        {
            dal.ActualizarAnalistaNomina();
        }

        public static void EliminarAnalistaNomina()
        {
            dal.EliminarAnalistaNomina();
        }
    }
}