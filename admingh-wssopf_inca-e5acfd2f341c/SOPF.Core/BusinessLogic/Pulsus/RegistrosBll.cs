#pragma warning disable 150830
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities.Pulsus;
using SOPF.Core.DataAccess.Pulsus;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: RegistrosBll.cs
//
// Descripción:
// Clase  que provee el acceso a los metodos de la clase RegistrosDal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.BusinessLogic.Pulsus
{
    public static class RegistrosBll
    {
        private static RegistrosDal dal;

        static RegistrosBll()
        {
            dal = new RegistrosDal();
        }

        public static void InsertarRegistros(Registros entidad)
        {
            dal.InsertarRegistros(entidad);			
        }
        
        public static Registros ObtenerRegistros()
        {
            return dal.ObtenerRegistros();
        }
        
        public static void ActualizarRegistros()
        {
            dal.ActualizarRegistros();
        }

        public static void EliminarRegistros()
        {
            dal.EliminarRegistros();
        }
    }
}