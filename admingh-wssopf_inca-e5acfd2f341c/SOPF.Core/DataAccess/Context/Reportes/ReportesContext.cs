﻿using SOPF.Core.Poco.Reportes;
using System.Data.Entity;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        public DbSet<Cat_Menu> Reportes_Cat_Menu { get; set; }
        public DbSet<Accesos> Reportes_Accesos { get; set; }
    }
}
