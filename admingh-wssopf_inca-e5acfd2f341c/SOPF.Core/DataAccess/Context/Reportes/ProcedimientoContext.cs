﻿using Microsoft.VisualBasic.Logging;
using SOPF.Core.Model;
using SOPF.Core.Model.Reportes;
using SOPF.Core.Model.Request.Reportes;
using SOPF.Core.Model.Response.Reportes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        public UsuariosResponse Reportes_Usuarios(UsuariosRequest peticion)
        {
            UsuariosResponse resultado = new UsuariosResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@Id_Usuario", new SqlParameter("@Id_Usuario", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Id_Usuario??DBNull.Value } },
                { "@Perfil_Id", new SqlParameter("@Perfil_Id", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Perfil_Id??DBNull.Value } },
                { "@Compania_Id", new SqlParameter("@Compania_Id", SqlDbType.Int) { Value = peticion.Compania_Id } },
                { "@Usuario", new SqlParameter("@Usuario", SqlDbType.VarChar) { IsNullable = true, Value = (object)peticion.Usuario??DBNull.Value } },
                { "@Pass", new SqlParameter("@Pass", SqlDbType.VarChar) { IsNullable = true, Value = (object)peticion.Pass??DBNull.Value } },
                { "@Nombre", new SqlParameter("@Nombre", SqlDbType.VarChar) { IsNullable = true, Value = (object)peticion.Nombre??DBNull.Value } },
                { "@Accion", new SqlParameter("@Accion", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Accion??DBNull.Value } },
                { "@Correo", new SqlParameter("@Correo", SqlDbType.VarChar) { IsNullable = true, Value = (object)peticion.Correo??DBNull.Value } },
                { "@Token", new SqlParameter("@Token", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Token??DBNull.Value } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[Reportes].[Usuarios]", parametros.Select(p => p.Value).ToList());

            switch (peticion.Accion)
            {
                case 0:
                    List<UsuarioLoginVM> login = Database.SqlQuery<UsuarioLoginVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).ToList();
                    if (login != null && login.Count > 0)
                    {
                        resultado.UsuarioLogin = login[0];
                    }
                    break;
                case 5:
                    _ = Database.SqlQuery<UsuarioLoginVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).ToList();
                    break;
            }

            return resultado;
        }
    }
}
