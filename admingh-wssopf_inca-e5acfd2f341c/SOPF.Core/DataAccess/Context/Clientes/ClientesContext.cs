﻿using SOPF.Core.Poco.Clientes;
using System.Data.Entity;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        public DbSet<ClienteEmpleo> Clientes_ClienteEmpleo { get; set; }
        public DbSet<ClientesReferencia> Clientes_Referencia { get; set; }
        public DbSet<ClienteTelefono> Clientes_ClienteTelefono { get; set; }
        public DbSet<TB_ClienteDirecciones> Clientes_TB_ClienteDirecciones { get; set; }
        public DbSet<TB_Proceso_Buzon> Clientes_TB_Proceso { get; set; }
        public DbSet<TB_BuzonAcciones> Clientes_TB_BuzonAcciones { get; set; }
        public DbSet<TB_UsuarioFCMToken> Clientes_TB_UsuarioFCMToken { get; set; }
    }
}
