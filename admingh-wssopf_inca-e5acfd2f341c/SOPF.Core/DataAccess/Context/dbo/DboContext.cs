﻿using SOPF.Core.Poco.dbo;
using System.Data.Entity;

namespace SOPF.Core.DataAccess
{
    partial class CreditOrigContext
    {
        public DbSet<TB_CATBanco> dbo_TB_CATBanco { get; set; }
        public DbSet<TB_CATEstatus> dbo_TB_CATEstatus { get; set; }
        public DbSet<TB_CATParametro> dbo_TB_CATParametro { get; set; }
        public DbSet<TB_CATSucursal> dbo_TB_CATSucursal { get; set; }
        public DbSet<TB_CATUsuario> dbo_TB_CATUsuario { get; set; }
        public DbSet<TB_Clientes> dbo_TB_Clientes { get; set; }
        public DbSet<TB_Creditos> dbo_TB_Creditos { get; set; }
        public DbSet<TB_Solicitudes> dbo_TB_Solicitudes { get; set; }
        public DbSet<TB_Solicitudes_Reestructura> dbo_TB_Solicitudes_Reestructura { get; set; }
        public DbSet<TB_CATTipoUsuario> dbo_TB_CATTipoUsuario { get; set; }
        public DbSet<TB_CATTipoCredito_PE> dbo_TB_CATTipoCredito_PE { get; set; }
        public DbSet<TB_CATColonia> dbo_TB_CATColonia { get; set; }
        public DbSet<TB_Recibos> dbo_TB_Recibos { get; set; }
        public DbSet<TB_CATPromotor> dbo_TB_CATPromotor { get; set; }
        public DbSet<TB_CATEstado> dbo_TB_CATEstado { get; set; }
        public DbSet<SI_Pais> dbo_SI_Pais { get; set; }
        public DbSet<TB_CATSituacionLaboral_PE> dbo_TB_CATSituacionLaboral_PE { get; set; }
        public DbSet<TB_CATTipoDocumento_PE> dbo_TB_CATTipoDocumento_PE { get; set; }
        public DbSet<TB_CATDireccion_PE> dbo_TB_CATDireccion_PE { get; set; }
        public DbSet<TB_CATTipoZona_PE> dbo_TB_CATTipoZona_PE { get; set; }
        public DbSet<TB_CATCiudad> dbo_TB_CATCiudad { get; set; }
        public DbSet<TB_CATFormatos> dbo_TB_CATFormatos { get; set; }
    }
}
