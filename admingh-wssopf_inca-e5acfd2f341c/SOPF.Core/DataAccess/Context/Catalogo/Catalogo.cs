﻿
using SOPF.Core.Poco.Catalogo;
using SOPF.Core.Poco.dbo;
using System.Data.Entity;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        public DbSet<ServidorArchivos> Catalogo_ServidorArchivos { get; set; }
        public DbSet<TB_CATEmpresaConfiguracion> Catalogo_TB_CATEmpresaConfiguracion { get; set; }
        public DbSet<TB_CATOrigenClienteEdicion> Catalogo_TB_CATOrigenClienteEdicion { get; set; }
        public DbSet<TB_CATProgIntervalos> Catalogo_TB_CATProgIntervalos { get; set; }
        public DbSet<TB_CATTipoProgramacion> Catalogo_TB_CATTipoProgramacion { get; set; }
        public DbSet<TB_CATTipoVade> Catalogo_TB_CATTipoVade { get; set; }
        public DbSet<TB_ProgramacionEstatus> Catalogo_TB_ProgramacionEstatus { get; set; }
        public DbSet<TB_TipoCobro> Catalogo_TB_TipoCobro { get; set; }
        public DbSet<TB_TipoEstatus> Catalogo_TB_TipoEstatus { get; set; }
        public DbSet<TipoPension> Catalogo_TipoPension { get; set; }
        public DbSet<TB_CATFormatoPaginas> Catalogo_TB_CATFormatoPaginas { get; set; }
        public DbSet<TablaEmpresa> Catalogo_TB_CATEmpresa { get; set; }
    }
}
