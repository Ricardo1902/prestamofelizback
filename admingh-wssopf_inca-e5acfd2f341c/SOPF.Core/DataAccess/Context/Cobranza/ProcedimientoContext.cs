﻿using SOPF.Core.Model.Request.Cobranza;
using SOPF.Core.Model.Response.Cobranza;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Xml.Linq;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        #region GET

        #endregion

        #region POST
        public virtual Cobranza_SP_PlantillaEnvioCobroPROResponse Cobranza_SP_PlantillaEnvioCobroPRO(int idUsuario, int idPlantilla)
        {
            Cobranza_SP_PlantillaEnvioCobroPROResponse respuesta = new Cobranza_SP_PlantillaEnvioCobroPROResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>
            {
                { "IdUsuario", new SqlParameter("IdUsuario", idUsuario)},
                { "IdPlantilla", new SqlParameter("IdPlantilla", idPlantilla)},
                { "IdPlantillaEnvioCobro", new SqlParameter("IdPlantillaEnvioCobro", SqlDbType.Int){ IsNullable = true, Direction = ParameterDirection.InputOutput, Value = SqlInt32.Null } },
                { "OperacionCorrecta", new SqlParameter("OperacionCorrecta", SqlDbType.Bit){ IsNullable =true, Direction = ParameterDirection.InputOutput, Value = SqlBinary.Null } },
                { "MensajeOperacion", new SqlParameter("MensajeOperacion", SqlDbType.VarChar, 200){ IsNullable =true, Direction = ParameterDirection.InputOutput, Value = SqlString.Null } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("Cobranza.SP_PlantillaEnvioCobroPRO", parametros.Select(p => p.Value).ToList());
            Database.ExecuteSqlCommand(sqlCommand, parametros.Select(p => p.Value).ToArray());
            bool.TryParse(parametros["OperacionCorrecta"].Value.ToString(), out bool operacionCorrecta);
            int.TryParse(parametros["IdPlantillaEnvioCobro"].Value.ToString(), out int idPlantillaEnvioCobro);
            respuesta.OperacionCorrecta = operacionCorrecta;
            respuesta.IdPlantillaEnvioCobro = idPlantillaEnvioCobro;
            respuesta.MensajeOperacion = parametros["MensajeOperacion"].Value.ToString();
            return respuesta;
        }

        public PeticionDomiciliacionResponse Cobranza_SP_PeticionDomiciliacionALT(PeticionDomiciliacionAltRequest peticion)
        {
            PeticionDomiciliacionResponse resultado = new PeticionDomiciliacionResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario } },
                { "@OrigenDomiciliacion", new SqlParameter("@OrigenDomiciliacion", SqlDbType.VarChar) { Value = peticion.OrigenDomiciliacion } },
                { "@IdLayuotArchivo", new SqlParameter("@IdLayuotArchivo", SqlDbType.Int) { Value = peticion.IdLayuotArchivo} },
                { "@Solicitudes", new SqlParameter("@Solicitudes", SqlDbType.Xml) { Value = peticion.Solicitudes.ToString(SaveOptions.DisableFormatting) } },
                { "@OperacionCorrecta", new SqlParameter("@OperacionCorrecta", SqlDbType.Bit) { Direction = ParameterDirection.Output } },
                { "@MensajeOperacion", new SqlParameter("@MensajeOperacion", SqlDbType.VarChar) { Direction = ParameterDirection.Output, Size = 200 } },
                { "@IdPeticionDomiciliacion", new SqlParameter("@IdPeticionDomiciliacion", SqlDbType.Int) { Direction = ParameterDirection.Output } }
            };

            string sqlCommand = Utils.SqlQueryProcedureString("[Cobranza].[SP_PeticionDomiciliacionALT]", parametros.Select(p => p.Value).ToList());
            Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlCommand, parametros.Select(p => p.Value).ToArray());

            bool.TryParse(parametros["@OperacionCorrecta"].Value.ToString(), out bool operacionCorrecta);
            string mensajeOperacion = parametros["@MensajeOperacion"].Value.ToString();
            int.TryParse(parametros["@IdPeticionDomiciliacion"].Value.ToString(), out int idPeticionDomiciliacion);

            resultado.Error = !operacionCorrecta;
            resultado.MensajeOperacion = mensajeOperacion;
            resultado.IdPeticionDomiciliacion = idPeticionDomiciliacion;

            return resultado;
        }

        public SP_ProcesarGrupoPROResponse Cobranza_SP_ProcesarGrupoPRO(SP_ProcesarGrupoPRORequest peticion)
        {
            SP_ProcesarGrupoPROResponse resultado = new SP_ProcesarGrupoPROResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdGrupoLayout", new SqlParameter("@IdGrupoLayout", SqlDbType.Int) { Value = peticion.IdGrupoLayout } },
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario } },
                { "@EsAutomatico", new SqlParameter("@EsAutomatico", SqlDbType.Bit) { Value = peticion.EsAutomatico } },
                { "@IdLayoutGenerado", new SqlParameter("@IdLayoutGenerado", SqlDbType.Int) { IsNullable = true, Direction = ParameterDirection.Output, Value = peticion.IdLayoutGenerado } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[Cobranza].[SP_ProcesarGrupoPRO]", parametros.Select(p => p.Value).ToList());

            Database.ExecuteSqlCommand(sqlCommand, parametros.Select(p => p.Value).ToArray());

            int.TryParse(parametros["@IdLayoutGenerado"].Value.ToString(), out int idLayoutGenerado);
            resultado.IdLayoutGenerado = idLayoutGenerado;
            return resultado;
        }
        #endregion
    }
}
