﻿using SOPF.Core.Entities.Cobranza;
using SOPF.Core.Poco.Cobranza;
using System.Data.Entity;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        public DbSet<FalloDomiciliacion> Cobranza_FalloDomiciliacion { get; set; }
        public DbSet<PagoMasivo> Cobranza_PagoMasivo { get; set; }
        public DbSet<PagoMasivoDetalle> Cobranza_PagoMasivoDetalle { get; set; }
        public DbSet<TB_BancoDomiciliacion> Cobranza_TB_BancoDomiciliacion { get; set; }
        public DbSet<TB_CATTipoLayoutCobro> Cobranza_TB_CATTipoLayoutCobro { get; set; }
        public DbSet<TB_CuentaExcluyeDomiciliacion> Cobranza_TB_CuentaExcluyeDomiciliacion { get; set; }
        public DbSet<TB_GrupoLayoutProgramacion> Cobranza_TB_GrupoLayoutProgramacion { get; set; }
        public DbSet<TB_GrupoLayoutProgramacionDetalle> Cobranza_TB_GrupoLayoutProgramacionDetalle { get; set; }
        public DbSet<TB_GrupoLayoutProgramacionDias> Cobranza_TB_GrupoLayoutProgramacionDias { get; set; }
        public DbSet<TB_LayoutArchivo> Cobranza_TB_LayoutArchivo { get; set; }
        public DbSet<TB_LayoutArchivoCampo> Cobranza_TB_LayoutArchivoCampo { get; set; }
        public DbSet<TB_LayoutGenerado> Cobranza_TB_LayoutGenerado { get; set; }
        public DbSet<TB_LayoutGeneradoPlantilla> Cobranza_TB_LayoutGeneradoPlantilla { get; set; }
        public DbSet<TB_MedioDomiciliacion> Cobranza_TB_MedioDomiciliacion { get; set; }
        public DbSet<TB_MotivoExclusionDomiciliacion> Cobranza_TB_MotivoExclusionDomiciliacion { get; set; }
        public DbSet<TB_OrigenExclusion> Cobranza_TB_OrigenExclusion { get; set; }
        public DbSet<TB_PeticionDomiciliacion> Cobranza_TB_PeticionDomiciliacion { get; set; }
        public DbSet<TB_PeticionDomiciliacionDetalle> Cobranza_TB_PeticionDomiciliacionDetalle { get; set; }
        public DbSet<TB_Plantilla> Cobranza_TB_Plantilla { get; set; }
        public DbSet<TB_PlantillaBucket> Cobranza_TB_PlantillaBucket { get; set; }
        public DbSet<TB_PlantillaEnvioCobro> Cobranza_TB_PlantillaEnvioCobro { get; set; }
        public DbSet<TB_PlantillaEnvioCobroDescarga> Cobranza_TB_PlantillaEnvioCobroDescarga { get; set; }
        public DbSet<TB_PlantillaEnvioCobroDetalle> Cobranza_TB_PlantillaEnvioCobroDetalle { get; set; }
        public DbSet<TB_ProgramacionDetalleProceso> Cobranza_TB_ProgramacionDetalleProceso { get; set; }
        public DbSet<TB_ProgramacionDetProLayoutGenerado> Cobranza_TB_ProgramacionDetProLayoutGenerado { get; set; }
        public DbSet<TB_TipoIngreso> Cobranza_TB_TipoIngreso { get; set; }
        public DbSet<VisanetDataMap> Cobranza_VisanetDataMap { get; set; }
        public DbSet<Poco.Cobranza.Pago> Cobranza_Pago { get; set; }
        public DbSet<PagoMasivoKushki> Cobranza_PagoMasivoKushki { get; set; }
        public DbSet<PagoMasivoDetalleKushki> Cobranza_PagoMasivoDetalleKushki { get; set; }
        public DbSet<PeticionKushki> Cobranza_PeticionKushki { get; set; }
        public DbSet<KushkiOrden> Cobranza_KushkiOrden { get; set; }
        public DbSet<KushkiTarjeta> Cobranza_KushkiTarjeta { get; set; }
        public DbSet<SuscripcionMasivoKushki> Cobranza_SuscripcionMasivoKushki { get; set; }
        public DbSet<SuscripcionMasivoDetalleKushki> Cobranza_SuscripcionMasivoDetalleKushki { get; set; }
    }
}