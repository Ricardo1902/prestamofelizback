using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Response.Gestiones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Xml.Linq;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        #region GET
        public CargaAsignacionesResponse Gestiones_SP_CargaAsignacionCON(CargaAsignacionConRequest peticion)
        {
            CargaAsignacionesResponse resultado = new CargaAsignacionesResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario} },
                { "@IdEstatus", new SqlParameter("@IdEstatus", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.IdEstatus??DBNull.Value } },
                { "@Pagina", new SqlParameter("@Pagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Pagina??DBNull.Value } },
                { "@RegistrosPagina", new SqlParameter("@RegistrosPagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.RegistrosPagina??DBNull.Value } },
                { "@TotalRegistros", new SqlParameter("@TotalRegistros", SqlDbType.Int) { Direction = ParameterDirection.Output, IsNullable = true } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_CargaAsignacionCON]", parametros.Select(p => p.Value).ToList());
            resultado.CargaAsignaciones = Database.SqlQuery<CargaAsignacionVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).ToList();

            int.TryParse(parametros["@TotalRegistros"].Value.ToString(), out int totalRegistros);
            resultado.TotalRegistros = totalRegistros;

            return resultado;
        }

        public CargaAsignacionDetallesResponse Gestiones_SP_CargaAsignacionDetalleCON(CargaAsignacionDetalleConRequest peticion)
        {
            CargaAsignacionDetallesResponse resultado = new CargaAsignacionDetallesResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario } },
                { "@IdCargaAsignacion", new SqlParameter("@IdCargaAsignacion", SqlDbType.Int) { Value = peticion.IdCargaAsignacion } },
                { "@Error", new SqlParameter("@Error", SqlDbType.Bit) { IsNullable = true, Value = (object)peticion.Error??DBNull.Value } },
                { "@Pagina", new SqlParameter("@Pagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Pagina??DBNull.Value } },
                { "@RegistrosPagina", new SqlParameter("@RegistrosPagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.RegistrosPagina??DBNull.Value } },
                { "@TotalRegistros", new SqlParameter("@TotalRegistros", SqlDbType.Int) { Direction = ParameterDirection.Output, IsNullable = true }  }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_CargaAsignacionDetalleCON]", parametros.Select(p => p.Value).ToList());
            resultado.Asignaciones = Database.SqlQuery<CargaAsignacionDetalleVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).ToList();
            int.TryParse(parametros["@TotalRegistros"].Value.ToString(), out int totalRegistros);
            resultado.TotalRegistros = totalRegistros;
            return resultado;
        }

        public ObtenerGestionesResponse Gestiones_SP_GestionesCON(ObtenerGestionRequest peticion)
        {
            ObtenerGestionesResponse resultado = new ObtenerGestionesResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@Accion", new SqlParameter("@Accion", SqlDbType.Int) { Value = 5 } },
                { "@IdCuenta", new SqlParameter("@IdCuenta", SqlDbType.Int) { Value = peticion.IdCuenta } },
                { "@Pagina", new SqlParameter("@Pagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Pagina??DBNull.Value } },
                { "@RegistrosPagina", new SqlParameter("@RegistrosPagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.RegistrosPagina??DBNull.Value } },
                { "@TotalRegistros", new SqlParameter("@TotalRegistros", SqlDbType.Int) { Direction = ParameterDirection.Output, IsNullable = true }  }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_GestionesCON]", parametros.Select(p => p.Value).ToList());
            resultado.Gestiones = Database.SqlQuery<GestionVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).ToList();
            int.TryParse(parametros["@TotalRegistros"].Value.ToString(), out int totalRegistros);
            resultado.TotalRegistros = totalRegistros;
            return resultado;
        }

        public ObtenerDatosClienteAcuerdoResponse Gestiones_SP_AcuerdoClienteCON(ObtenerDatosClienteAcuerdoRequest peticion)
        {
            ObtenerDatosClienteAcuerdoResponse resultado = new ObtenerDatosClienteAcuerdoResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@Accion", new SqlParameter("@Accion", SqlDbType.Int) { Value = 0 } },
                { "@IdSolicitud", new SqlParameter("@IdSolicitud", SqlDbType.Int) { Value = peticion.IdSolicitud } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_AcuerdoClienteCON]", parametros.Select(p => p.Value).ToList());
            resultado.Datos = Database.SqlQuery<ClienteAcuerdoVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).FirstOrDefault();
            return resultado;
        }

        public ObtenerAcuerdosPagoResponse Gestiones_SP_AcuerdoPagoCON(ObtenerAcuerdoPagoRequest peticion)
        {
            ObtenerAcuerdosPagoResponse resultado = new ObtenerAcuerdosPagoResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@Accion", new SqlParameter("@Accion", SqlDbType.Int) { Value = peticion.Accion } },
                { "@IdSolicitud", new SqlParameter("@IdSolicitud", SqlDbType.Int) { Value = peticion.IdSolicitud } },
                { "@IdAcuerdoPago", new SqlParameter("@IdAcuerdoPago", SqlDbType.Int) { Value = peticion.IdAcuerdoPago } },
                { "@Pagina", new SqlParameter("@Pagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Pagina??DBNull.Value } },
                { "@RegistrosPagina", new SqlParameter("@RegistrosPagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.RegistrosPagina??DBNull.Value } },
                { "@TotalRegistros", new SqlParameter("@TotalRegistros", SqlDbType.Int) { Direction = ParameterDirection.Output, IsNullable = true } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_AcuerdoPagoCON]", parametros.Select(p => p.Value).ToList());
            resultado.Acuerdos = Database.SqlQuery<AcuerdoPagoVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).ToList();

            int.TryParse(parametros["@TotalRegistros"].Value.ToString(), out int totalRegistros);
            resultado.TotalRegistros = totalRegistros;
            return resultado;
        }

        public ObtenerAcuerdoPagoDetalleResponse Gestiones_SP_AcuerdoPagoDetalleCON(ObtenerAcuerdoPagoDetalleRequest peticion)
        {
            ObtenerAcuerdoPagoDetalleResponse resultado = new ObtenerAcuerdoPagoDetalleResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@Accion", new SqlParameter("@Accion", SqlDbType.Int) { Value = peticion.Accion } },
                { "@IdAcuerdoPago", new SqlParameter("@IdAcuerdoPago", SqlDbType.Int) { Value = peticion.IdAcuerdoPago } },
                { "@IdAcuerdoPagoDetalle", new SqlParameter("@IdAcuerdoPagoDetalle", SqlDbType.Int) { Value = peticion.IdAcuerdoPagoDetalle } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_AcuerdoPagoDetalleCON]", parametros.Select(p => p.Value).ToList());
            resultado.Datos = Database.SqlQuery<AcuerdoPagoDetalleVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).ToList();
            return resultado;
        }

        public GestionDomiciacionesResponse Gestiones_SP_GestionDomiciliacionCON(GestionDomiciliacionConRequest peticion)
        {
            GestionDomiciacionesResponse resultado = new GestionDomiciacionesResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario } },
                { "@IdTipoGestion", new SqlParameter("@IdTipoGestion", SqlDbType.Int) { Value = peticion.IdTipoGestion } },
                { "@IdEstatus", new SqlParameter("@IdEstatus", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.IdEstatus??DBNull.Value } },
                { "@FechaDomciliar", new SqlParameter("@FechaDomciliar", SqlDbType.DateTime) { IsNullable = true, Value = (object)peticion.FechaDomciliar??DBNull.Value } },
                { "@IdBanco", new SqlParameter("@IdBanco", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.IdBanco??DBNull.Value } },
                { "@Pagina", new SqlParameter("@Pagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Pagina??DBNull.Value} },
                { "@RegistrosPagina", new SqlParameter("@RegistrosPagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.RegistrosPagina??DBNull.Value } },
                { "@TotalRegistros", new SqlParameter("@TotalRegistros", SqlDbType.Int) { IsNullable = true, Direction = ParameterDirection.Output } }
            };
            if (peticion.GestionDomiciliacion != null)
            {
                parametros.Add("@GestionDomiciliacion", new SqlParameter("@GestionDomiciliacion", SqlDbType.Xml) { IsNullable = true, Value = peticion.GestionDomiciliacion.ToString(SaveOptions.DisableFormatting) });
            }
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_GestionDomiciliacionCON]", parametros.Select(p => p.Value).ToList());

            resultado.GestionDomiciliaciones = Database.SqlQuery<GestionDomiciliacionVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).ToList();

            int.TryParse(parametros["@TotalRegistros"].Value.ToString(), out int totalRegistros);
            resultado.TotalRegistros = totalRegistros;

            return resultado;
        }
        #endregion

        #region POST
        public CargarAsignacionResponse Gestiones_SP_CargarAsignacionPRO(CargarAsignacionProRequest peticion)
        {
            CargarAsignacionResponse resultado = new CargarAsignacionResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) {Value = peticion.IdUsuario } },
                { "@XmlCarga", new SqlParameter("@XmlCarga", SqlDbType.Xml) { Value = peticion.XmlCarga.ToString(SaveOptions.DisableFormatting) } },
                { "@OperacionCorrecta", new SqlParameter("@OperacionCorrecta", SqlDbType.Bit) { Direction = ParameterDirection.Output, IsNullable = true } },
                { "@MensajeOperacion", new SqlParameter("@MensajeOperacion", SqlDbType.VarChar) { Direction = ParameterDirection.Output, IsNullable = true, Size = 200 } },
                { "@IdCargaAsignacion", new SqlParameter("@IdCargaAsignacion", SqlDbType.Int) { Direction = ParameterDirection.Output, IsNullable = true, Value = (object)peticion.IdCargaAsignacion??DBNull.Value } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_CargarAsignacionPRO]", parametros.Select(p => p.Value).ToList());
            Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlCommand, parametros.Select(p => p.Value).ToArray());
            bool.TryParse(parametros["@OperacionCorrecta"].Value.ToString(), out bool operacionCorrecta);
            string mensajeOperacion = parametros["@MensajeOperacion"].Value.ToString();
            int.TryParse(parametros["@IdCargaAsignacion"].Value.ToString(), out int idCargaAsignacion);
            resultado.Error = !operacionCorrecta;
            resultado.MensajeOperacion = mensajeOperacion;
            if (idCargaAsignacion > 0)
                resultado.CargaAsignacion = new CargaAsignacionVM { IdCargaAsignacion = idCargaAsignacion };
            return resultado;
        }

        public ActualizarCargaAsignacionResponse Gestiones_SP_CargarAsignacionACT(CargarAsignacionActRequest peticion)
        {
            ActualizarCargaAsignacionResponse resultado = new ActualizarCargaAsignacionResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario } },
                { "@Accion", new SqlParameter("@Accion", SqlDbType.Int) { Value = peticion.Accion } },
                { "@IdCargaAsignacion", new SqlParameter("@IdCargaAsignacion", SqlDbType.Int) { Value = peticion.IdCargaAsignacion } },
                { "@Error", new SqlParameter("@Error", SqlDbType.Bit) { IsNullable = true, Value = (object)peticion.Error??DBNull.Value } },
                { "@Actividad", new SqlParameter("@Actividad", SqlDbType.VarChar) { IsNullable = true, Value = (object)peticion.Actividad??DBNull.Value } },
                { "@XmlCarga", new SqlParameter("@XmlCarga", SqlDbType.Xml) { Value = peticion.XmlCarga.ToString(SaveOptions.DisableFormatting) } },
                { "@OperacionCorrecta", new SqlParameter("@OperacionCorrecta", SqlDbType.Bit) { Direction = ParameterDirection.Output, IsNullable = true } },
                { "@MensajeOperacion", new SqlParameter("@MensajeOperacion", SqlDbType.VarChar) { Direction = ParameterDirection.Output, IsNullable = true, Size = 200 } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_CargaAsignacionACT]", parametros.Select(p => p.Value).ToList());
            Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlCommand, parametros.Select(p => p.Value).ToArray());

            bool.TryParse(parametros["@OperacionCorrecta"].Value.ToString(), out bool operacionCorrecta);
            string mensajeOperacion = parametros["@MensajeOperacion"].Value.ToString();
            resultado.Error = !operacionCorrecta;
            resultado.MensajeOperacion = mensajeOperacion;

            return resultado;
        }

        public AsignarCargaAsignacionResponse Gestiones_SP_CargaAsignacionAsignarPRO(CargaAsignacionAsignarProRequest peticion)
        {
            AsignarCargaAsignacionResponse resultado = new AsignarCargaAsignacionResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario } },
                { "@IdCargaAsignacion", new SqlParameter("@IdCargaAsignacion", SqlDbType.Int) { Value = peticion.IdCargaAsignacion } },
                { "@OperacionCorrecta", new SqlParameter("@OperacionCorrecta", SqlDbType.Bit) { Direction = ParameterDirection.Output, IsNullable = true } },
                { "@MensajeOperacion", new SqlParameter("@MensajeOperacion", SqlDbType.VarChar) { Direction = ParameterDirection.Output, IsNullable = true, Size = 200 } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_CargaAsignacionAsignarPRO]", parametros.Select(p => p.Value).ToList());
            Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlCommand, parametros.Select(p => p.Value).ToArray());

            bool.TryParse(parametros["@OperacionCorrecta"].Value.ToString(), out bool operacionCorrecta);
            string mensajeOperacion = parametros["@MensajeOperacion"].Value.ToString();

            resultado.Error = !operacionCorrecta;
            resultado.MensajeOperacion = mensajeOperacion;
            return resultado;
        }

        public DevolverLlamadaResponse Gestiones_SP_DevolverLlamadaALT(DevolverLlamadaAltRequest peticion)
        {
            DevolverLlamadaResponse resultado = new DevolverLlamadaResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdGestion", new SqlParameter("@IdGestion", SqlDbType.Int) { Value = peticion.IdGestion } },
                { "@IdCuenta", new SqlParameter("@IdCuenta", SqlDbType.NVarChar) { Value = peticion.IdCuenta } },
                { "@Fch_DevLlamada", new SqlParameter("@Fch_DevLlamada", SqlDbType.SmallDateTime) { Value = peticion.Fch_DevLlamada } },
                { "@IdCliente", new SqlParameter("@IdCliente", SqlDbType.NVarChar) { Value = peticion.IdCliente } },
                { "@IdGestor", new SqlParameter("@IdGestor", SqlDbType.Int) { Value = peticion.IdGestor } },
                { "@Telefono", new SqlParameter("@Telefono", SqlDbType.VarChar) { IsNullable = true, Value = peticion.Telefono } },
                { "@Tipo", new SqlParameter("@Tipo", SqlDbType.VarChar) { IsNullable  =true, Value = peticion.Tipo } },
                { "@AnexoReferencia", new SqlParameter("@AnexoReferencia", SqlDbType.VarChar) { IsNullable = true, Value = peticion.AnexoReferencia } },
                { "@Comentario", new SqlParameter("@Comentario", SqlDbType.VarChar) { IsNullable = true, Value = peticion.Comentario } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_DevolverLlamadaALT]", parametros.Select(p => p.Value).ToList());

            Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlCommand, parametros.Select(p => p.Value).ToArray());

            return resultado;
        }

        public AltaAcuerdoPagoResponse Gestiones_SP_AcuerdoPagoPRO(AltaAcuerdoPagoProRequest peticion)
        {
            AltaAcuerdoPagoResponse resultado = new AltaAcuerdoPagoResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdSolicitud", new SqlParameter("@IdSolicitud", SqlDbType.Int) {Value = peticion.IdSolicitud } },
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) {Value = peticion.IdUsuario } },
                { "@XmlAcuerdos", new SqlParameter("@XmlAcuerdos", SqlDbType.Xml) { Value = peticion.XmlAcuerdos.ToString(SaveOptions.DisableFormatting) } },
                { "@OperacionCorrecta", new SqlParameter("@OperacionCorrecta", SqlDbType.Bit) { Direction = ParameterDirection.Output, IsNullable = true } },
                { "@MensajeOperacion", new SqlParameter("@MensajeOperacion", SqlDbType.VarChar) { Direction = ParameterDirection.Output, IsNullable = true, Size = 200 } },
                { "@IdAcuerdoPago", new SqlParameter("@IdAcuerdoPago", SqlDbType.Int) { Direction = ParameterDirection.Output, IsNullable = true, Value = (object)peticion.IdAcuerdoPago??DBNull.Value } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_AcuerdoPagoPRO]", parametros.Select(p => p.Value).ToList());
            Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlCommand, parametros.Select(p => p.Value).ToArray());
            bool.TryParse(parametros["@OperacionCorrecta"].Value.ToString(), out bool operacionCorrecta);
            string mensajeOperacion = parametros["@MensajeOperacion"].Value.ToString();
            int.TryParse(parametros["@IdAcuerdoPago"].Value.ToString(), out int idAcuerdoPago);
            resultado.Error = !operacionCorrecta;
            resultado.MensajeOperacion = mensajeOperacion;
            if (idAcuerdoPago > 0)
                resultado.AcuerdoPago = new AcuerdoPagoVM { IdAcuerdoPago = idAcuerdoPago };
            return resultado;
        }

        public CancelarAcuerdoPagoResponse Gestiones_SP_AcuerdoPagoBAJ(CancelarAcuerdoPagoRequest peticion)
        {

            CancelarAcuerdoPagoResponse resultado = new CancelarAcuerdoPagoResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@Accion", new SqlParameter("@Accion", SqlDbType.Int) {Value = peticion.Accion } },
                { "@IdAcuerdoPago", new SqlParameter("@IdAcuerdoPago", SqlDbType.Int) {Value = peticion.IdAcuerdoPago } },
                { "@IdSolicitud", new SqlParameter("@IdSolicitud", SqlDbType.Int) {Value = peticion.IdSolicitud } },
                { "@OperacionCorrecta", new SqlParameter("@OperacionCorrecta", SqlDbType.Bit) { Direction = ParameterDirection.Output, IsNullable = true } },
                { "@MensajeOperacion", new SqlParameter("@MensajeOperacion", SqlDbType.VarChar) { Direction = ParameterDirection.Output, IsNullable = true, Size = 200 } },
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_AcuerdoPagoBAJ]", parametros.Select(p => p.Value).ToList());
            Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlCommand, parametros.Select(p => p.Value).ToArray());
            bool.TryParse(parametros["@OperacionCorrecta"].Value.ToString(), out bool operacionCorrecta);
            string mensajeOperacion = parametros["@MensajeOperacion"].Value.ToString();
            resultado.Error = !operacionCorrecta;
            resultado.MensajeOperacion = mensajeOperacion;

            return resultado;
        }

        public CancelarAcuerdoPagoDetalleResponse Gestiones_SP_AcuerdoPagoDetalleBAJ(CancelarAcuerdoPagoDetalleRequest peticion)
        {

            CancelarAcuerdoPagoDetalleResponse resultado = new CancelarAcuerdoPagoDetalleResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@Accion", new SqlParameter("@Accion", SqlDbType.Int) {Value = peticion.Accion } },
                { "@IdAcuerdoPagoDetalle", new SqlParameter("@IdAcuerdoPagoDetalle", SqlDbType.Int) {Value = peticion.IdAcuerdoPagoDetalle } },
                { "@OperacionCorrecta", new SqlParameter("@OperacionCorrecta", SqlDbType.Bit) { Direction = ParameterDirection.Output, IsNullable = true } },
                { "@MensajeOperacion", new SqlParameter("@MensajeOperacion", SqlDbType.VarChar) { Direction = ParameterDirection.Output, IsNullable = true, Size = 200 } },
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[gestiones].[SP_AcuerdoPagoDetalleBAJ]", parametros.Select(p => p.Value).ToList());
            Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlCommand, parametros.Select(p => p.Value).ToArray());
            bool.TryParse(parametros["@OperacionCorrecta"].Value.ToString(), out bool operacionCorrecta);
            string mensajeOperacion = parametros["@MensajeOperacion"].Value.ToString();
            resultado.Error = !operacionCorrecta;
            resultado.MensajeOperacion = mensajeOperacion;

            return resultado;
        }
        #endregion
    }
}
