﻿using SOPF.Core.Poco.Gestiones;
using System.Data.Entity;

namespace SOPF.Core.DataAccess
{
    partial class CreditOrigContext
    {
        public DbSet<ArchivoGestion> Gestiones_ArchivoGestion { get; set; }
        public DbSet<AsignacionAccion> Gestiones_AsignacionAccion { get; set; }
        public DbSet<CatTipoContaco> Gestiones_CatTipoContaco { get; set; }
        public DbSet<DocumentoDestino> Gestiones_DocumentoDestino { get; set; }
        public DbSet<EnvioCourier> Gestiones_EnvioCourier { get; set; }
        public DbSet<EnvioCourierDetalle> Gestiones_EnvioCourierDetalle { get; set; }
        public DbSet<SolicitudNotificacionAcciones> Gestiones_SolicitudNotificacionAcciones { get; set; }
        public DbSet<SolicitudNotificacionDetalle> Gestiones_SolicitudNotificacionDetalle { get; set; }
        public DbSet<SolicitudNotificaciones> Gestiones_SolicitudNotificaciones { get; set; }
        public DbSet<TB_AcuerdoPago> Gestiones_TB_AcuerdoPago { get; set; }
        public DbSet<TB_AcuerdoPagoDetalle> Gestiones_TB_AcuerdoPagoDetalle { get; set; }
        public DbSet<TB_Asignacion> Gestiones_TB_Asignacion { get; set; }
        public DbSet<TB_CargaAsignacion> Gestiones_TB_CargaAsignacion { get; set; }
        public DbSet<TB_CargaAsignacionDetalle> Gestiones_TB_CargaAsignacionDetalle { get; set; }
        public DbSet<TB_CATDestinoNotificacion> Gestiones_TB_CATDestinoNotificacion { get; set; }
        public DbSet<TB_CatGestores> Gestiones_TB_CatGestores { get; set; }
        public DbSet<TB_CATNotificaciones> Gestiones_TB_CATNotificaciones { get; set; }
        public DbSet<TB_CATResultadoGestion> Gestiones_TB_CATResultadoGestion { get; set; }
        public DbSet<TB_CATSolNotTipoAcciones> Gestiones_TB_CATSolNotTipoAcciones { get; set; }
        public DbSet<TB_CatTipoGestor> Gestiones_TB_CatTipoGestor { get; set; }
        public DbSet<TB_CATTipoNotificaciones> Gestiones_TB_CATTipoNotificaciones { get; set; }
        public DbSet<TB_DevolverLlamada> Gestiones_TB_DevolverLlamada { get; set; }
        public DbSet<TB_ExtensionSesion> Gestiones_TB_ExtensionSesion { get; set; }
        public DbSet<TB_Gestion> Gestiones_TB_Gestion { get; set; }
        public DbSet<TB_GestionDomiciliacion> Gestiones_TB_GestionDomiciliacion { get; set; }
        public DbSet<TB_GestionDomiciliacionAct> Gestiones_TB_GestionDomiciliacionAct { get; set; }
        public DbSet<TB_PromesaPago> Gestiones_TB_PromesaPago { get; set; }
        public DbSet<TB_TipoGestionDomiciliacion> Gestiones_TB_TipoGestionDomiciliacion { get; set; }
        public DbSet<TB_TipoGestionDomLayoutArchivo> Gestiones_TB_TipoGestionDomLayoutArchivo { get; set; }
        public DbSet<TB_TipoGestionPromesa> Gestiones_TB_TipoGestionPromesa { get; set; }
        public DbSet<TB_TipoGestionTipoNotificacion> Gestiones_TB_TipoGestionTipoNotificacion { get; set; }
        public DbSet<TipoArchivoGestion> Gestiones_TipoArchivoGestion { get; set; }
        public DbSet<TipoGestion> Gestiones_TipoGestion { get; set; }
        public DbSet<TipoGestorContacto> Gestiones_TipoGestorContacto { get; set; }
    }
}
