﻿using SOPF.Core.Poco.Creditos;
using System.Data.Entity;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        public DbSet<TB_CATOrigen> Creditos_TB_CATOrigen { get; set; }
    }
}
