﻿using SOPF.Core.Poco.MesaControl;
using System.Data.Entity;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        public DbSet<TB_DocumentosObservar> MesaControl_TB_DocumentosObservar { get; set; }
    }
}
