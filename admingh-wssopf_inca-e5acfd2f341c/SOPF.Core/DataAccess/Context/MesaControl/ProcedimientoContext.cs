﻿using SOPF.Core.Model;
using SOPF.Core.Model.Request.MesaControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        public Respuesta MesaControl_SP_DocumentosObservarMCACT(SP_DocumentosObservarMCACTRequest peticion)
        {
            Respuesta resultado = new Respuesta();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdSolicitud", new SqlParameter("@IdSolicitud", SqlDbType.Int) { Value = peticion.IdSolicitud } },
                { "@NumeroTransaccion", new SqlParameter("@NumeroTransaccion", SqlDbType.BigInt) { IsNullable = true, Value = (object)peticion.NumeroTransaccion??DBNull.Value } },
                { "@Transaccion", new SqlParameter("@Transaccion", SqlDbType.BigInt) { IsNullable = true, Value = (object)peticion.Transaccion??DBNull.Value } },
                { "@Sy_Paso", new SqlParameter("@Sy_Paso", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Sy_Paso??DBNull.Value } },
                { "@UsuarioId", new SqlParameter("@UsuarioId", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.UsuarioId??DBNull.Value } },
                { "@IP", new SqlParameter("@IP", SqlDbType.VarChar) { IsNullable = true, Value = (object)peticion.IP??DBNull.Value } },
                { "@ObjPadreId", new SqlParameter("@ObjPadreId", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.ObjPadreId??DBNull.Value } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[MesaControl].[SP_DocumentosObservarMCACT]", parametros.Select(p => p.Value).ToList());
            Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlCommand, parametros.Select(p => p.Value).ToArray());

            return resultado;
        }
    }
}
