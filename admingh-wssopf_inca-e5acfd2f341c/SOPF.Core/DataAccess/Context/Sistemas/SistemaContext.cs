﻿using SOPF.Core.Poco.Sistema;
using System.Data.Entity;

namespace SOPF.Core.DataAccess
{
    partial class CreditOrigContext
    {
        public DbSet<TB_ConexionSip> Sistema_TB_ConexionSip { get; set; }
        public DbSet<TB_ConexionSipExtension> Sistema_TB_ConexionSipExtension { get; set; }
        public DbSet<TB_ConexionSipSdk> Sistema_TB_ConexionSipSdk { get; set; }
        public DbSet<TB_CuentaCorreo> Sistema_TB_CuentaCorreo { get; set; }
        public DbSet<TB_Notificacion> Sistema_TB_Notificacion { get; set; }
        public DbSet<TB_NotificacionError> Sistema_TB_NotificacionError { get; set; }
        public DbSet<TB_PeticionOTP> Sistema_TB_PeticionOTP { get; set; }
        public DbSet<TB_PeticionOtpValidar> Sistema_TB_PeticionOtpValidar { get; set; }
        public DbSet<TB_TipoNotificacion> Sistema_TB_TipoNotificacion { get; set; }
        public DbSet<TB_TipoNotificacionCuentaCorreo> Sistema_TB_TipoNotificacionCuentaCorreo { get; set; }
        public DbSet<TB_TipoNotificacionSMS> Sistema_TB_TipoNotificacionSMS { get; set; }
    }
}
