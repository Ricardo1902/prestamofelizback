﻿using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Request.Sistema;
using SOPF.Core.Model.Response.Sistema;
using SOPF.Core.Model.Sistema;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        #region GET
        public ValidaRecursoAccesoResponse Sistema_SP_ValidaRecursoAccesoCON(ValidaRecursoAccesoConRequest peticion)
        {
            ValidaRecursoAccesoResponse resultado = new ValidaRecursoAccesoResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario } },
                { "@Recurso", new SqlParameter("@Recurso", SqlDbType.VarChar) { Value = peticion.Recurso } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[Sistema].[SP_ValidaRecursoAccesoCON]", parametros.Select(p => p.Value).ToList());
            resultado.RecursoAcciones = Database.SqlQuery<RecursoAccionVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).ToList();
            
            return resultado;
        }
        #endregion

        #region POST
        public AltaNotificacionResponse Sistema_SP_TB_NotificacionALT(AltaNotificacionRequest peticion)
        {
            AltaNotificacionResponse resultado = new AltaNotificacionResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario } },
                { "@IdSolicitud", new SqlParameter("@IdSolicitud", SqlDbType.Int) { Value = peticion.IdSolicitud } },
                { "@IdTipoNotificacion", new SqlParameter("@IdTipoNotificacion", SqlDbType.Int) { Value = peticion.IdTipoNotificacion } },
                { "@OperacionCorrecta", new SqlParameter("@OperacionCorrecta", SqlDbType.Bit) { Direction = ParameterDirection.Output } },
                { "@MensajeOperacion", new SqlParameter("@MensajeOperacion", SqlDbType.VarChar) { Direction = ParameterDirection.Output, Size = 200 } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[Sistema].[SP_TB_NotificacionALT]", parametros.Select(p => p.Value).ToList());

            _ = Database.ExecuteSqlCommand(sqlCommand, parametros.Select(p => p.Value).ToArray());

            bool.TryParse(parametros["@OperacionCorrecta"].Value.ToString(), out bool operacionCorrecta);
            string mensajeOperacion = parametros["@MensajeOperacion"].Value.ToString();

            resultado.Error = !operacionCorrecta;
            resultado.MensajeOperacion = mensajeOperacion;
            return resultado;
        }

        public AltaNotificacionResponse Sistema_SP_TB_NotificacionALT_SolicitudNotificacion(AltaNotificacionSolicitudNotificacionRequest peticion)
        {
            AltaNotificacionResponse resultado = new AltaNotificacionResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario } },
                { "@IdSolicitud", new SqlParameter("@IdSolicitud", SqlDbType.Int) { Value = peticion.IdSolicitud } },
                { "@IdTipoNotificacion", new SqlParameter("@IdTipoNotificacion", SqlDbType.Int) { Value = peticion.IdTipoNotificacion } },
                { "@Clave", new SqlParameter("@Clave", SqlDbType.VarChar) { Value = peticion.Clave } },
                { "@FechaHoraCitacion", new SqlParameter("@FechaHoraCitacion", SqlDbType.DateTime) { Value = (object)peticion.FechaHoraCitacion ?? DBNull.Value } },
                { "@DescuentoDeuda", new SqlParameter("@DescuentoDeuda", SqlDbType.Money) { Value = (object)peticion.DescuentoDeuda ?? DBNull.Value } },
                { "@FechaVigencia", new SqlParameter("@FechaVigencia", SqlDbType.DateTime) { Value = (object)peticion.FechaVigencia ?? DBNull.Value } },
                { "@IdDestino", new SqlParameter("@IdDestino", SqlDbType.Int) { Value = peticion.IdDestino } },
                { "@Courier", new SqlParameter("@Courier", SqlDbType.Bit) { Value = peticion.Courier } },
                { "@Actualizar", new SqlParameter("@Actualizar", SqlDbType.Bit) { Value = peticion.Actualizar } },
                { "@OperacionCorrecta", new SqlParameter("@OperacionCorrecta", SqlDbType.Bit) { Direction = ParameterDirection.Output } },
                { "@MensajeOperacion", new SqlParameter("@MensajeOperacion", SqlDbType.VarChar) { Direction = ParameterDirection.Output, Size = 200 } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[Sistema].[SP_TB_NotificacionALT]", parametros.Select(p => p.Value).ToList());

            _ = Database.ExecuteSqlCommand(sqlCommand, parametros.Select(p => p.Value).ToArray());

            bool.TryParse(parametros["@OperacionCorrecta"].Value.ToString(), out bool operacionCorrecta);
            string mensajeOperacion = parametros["@MensajeOperacion"].Value.ToString();

            resultado.Error = !operacionCorrecta;
            resultado.MensajeOperacion = mensajeOperacion;
            return resultado;
        }
        #endregion

    }
}
