﻿using SOPF.Core.Poco.Lleida;
using System.Data.Entity;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        public DbSet<Plantilla> Lleida_TB_Plantilla { get; set; }
    }
}
