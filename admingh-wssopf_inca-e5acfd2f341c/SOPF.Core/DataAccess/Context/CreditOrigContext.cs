﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Reflection;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext : DbContext
    {
        public static string ConexionBD
        {
            get
            {
                try
                {
                    return ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["CADENACONEXIONSQL"]].ConnectionString;
                }
                catch (Exception ex)
                {
                    Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                        $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject("")}", JsonConvert.SerializeObject(ex));
                    return string.Empty;
                }
            }
        }

        public CreditOrigContext()
            : base(ConexionBD)
        {
            Database.SetInitializer<CreditOrigContext>(null);
            Database.CommandTimeout = (2 * 60);
#if DEBUG
            Database.CommandTimeout = 0;
            Database.Log = sql => System.Diagnostics.Debug.WriteLine(sql);
#endif
        }

    }
}
