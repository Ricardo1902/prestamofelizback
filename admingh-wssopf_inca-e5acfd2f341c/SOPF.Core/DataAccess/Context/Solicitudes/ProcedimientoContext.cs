﻿using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Request.Solicitudes;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Model.Response.Solicitudes;
using SOPF.Core.Model.Solicitudes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Xml.Linq;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        public SolicitudFirmaDigitalResponse Solicitudes_SP_SolicitudFirmaDigitalCON(SolicitudFirmaDigitalRequest peticion)
        {
            SolicitudFirmaDigitalResponse resultado = new SolicitudFirmaDigitalResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario } },
                { "@IdSolicitud", new SqlParameter("@IdSolicitud", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.IdSolicitud??DBNull.Value } },
                { "@IdEstatus", new SqlParameter("@IdEstatus", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.IdEstatus??DBNull.Value  } },
                { "@FechaInicio", new SqlParameter("@FechaInicio", SqlDbType.DateTime) { IsNullable = true, Value = (object)peticion.FechaInicio??DBNull.Value } },
                { "@FechaFin", new SqlParameter("@FechaFin", SqlDbType.DateTime) { IsNullable = true, Value = (object)peticion.FechaFin??DBNull.Value } },
                { "@Pagina", new SqlParameter("@Pagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Pagina??DBNull.Value } },
                { "@RegistrosPagina", new SqlParameter("@RegistrosPagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.RegistrosPagina??DBNull.Value } },
                { "@TotalRegistros", new SqlParameter("@TotalRegistros", SqlDbType.Int) { Direction = ParameterDirection.Output, IsNullable = true } }
            };

            string sqlCommand = Utils.SqlQueryProcedureString("[Solicitudes].[SP_SolicitudFirmaDigitalCON]", parametros.Select(p => p.Value).ToList());
            resultado.Firmas = Database.SqlQuery<SolicitudFirmaDigitalVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).ToList();
            int.TryParse(parametros["@TotalRegistros"].Value.ToString(), out int totalRegistros);
            resultado.TotalRegistros = totalRegistros;

            return resultado;
        }

        public SolicitudPruebaVidaResponse Solicitudes_SP_SolicitudPruebaVidaCON(SolicitudPruebaVidaRequest peticion)
        {
            SolicitudPruebaVidaResponse resultado = new SolicitudPruebaVidaResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario } },
                { "@IdSolicitud", new SqlParameter("@IdSolicitud", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.IdSolicitud??DBNull.Value } },
                { "@SesionRealizada", new SqlParameter("@SesionRealizada", SqlDbType.Bit) { IsNullable = true, Value = (object)peticion.SesionRealizada??DBNull.Value } },
                { "@ValidacionCorrecta", new SqlParameter("@ValidacionCorrecta", SqlDbType.Bit) { IsNullable = true, Value = (object)peticion.ValidacionCorrecta??DBNull.Value } },
                { "@Aprobado", new SqlParameter("@Aprobado", SqlDbType.Bit) { IsNullable = true, Value = (object)peticion.Aprobado??DBNull.Value } },
                { "@FechaInicio", new SqlParameter("@FechaInicio", SqlDbType.DateTime) { IsNullable = true, Value = (object)peticion.FechaInicio??DBNull.Value } },
                { "@FechaFin", new SqlParameter("@FechaFin", SqlDbType.DateTime) { IsNullable = true, Value = (object)peticion.FechaFin??DBNull.Value } },
                { "@Pagina", new SqlParameter("@Pagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Pagina??DBNull.Value } },
                { "@RegistrosPagina", new SqlParameter("@RegistrosPagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.RegistrosPagina??DBNull.Value } },
                { "@TotalRegistros", new SqlParameter("@TotalRegistros", SqlDbType.Int) { Direction = ParameterDirection.Output, IsNullable = true } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[Solicitudes].[SP_SolicitudPruebaVidaCON]", parametros.Select(p => p.Value).ToList());
            resultado.PruebasVida = Database.SqlQuery<SolicitudPruebaVidaVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).ToList();
            int.TryParse(parametros["@TotalRegistros"].Value.ToString(), out int totalRegistros);
            resultado.TotalRegistros = totalRegistros;

            return resultado;
        }

        public PeticionReestDocDigitalesResponse Solicitudes_SP_PeticionReestructuraDocsDigCON(PeticionReestructuraDocsDigCONRequest peticion)
        {
            PeticionReestDocDigitalesResponse resultado = new PeticionReestDocDigitalesResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdSolicitud", new SqlParameter("@IdSolicitud", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.IdSolicitud??DBNull.Value} },
                { "@Pagina", new SqlParameter("@Pagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.Pagina??(DBNull.Value) } },
                { "@RegistrosPagina", new SqlParameter("@RegistrosPagina", SqlDbType.Int) { IsNullable = true, Value = (object)peticion.RegistrosPagina??(DBNull.Value)} },
                { "@TotalRegistros", new SqlParameter("@TotalRegistros", SqlDbType.Int) { IsNullable = true, Direction = ParameterDirection.Output } }
            };
            if (peticion.IdEstatus != null)
            {
                parametros.Add("@IdEstatus", new SqlParameter("@IdEstatus", SqlDbType.Xml) { IsNullable = true, Value = peticion.IdEstatus.ToString(SaveOptions.DisableFormatting) });
            }

            string sqlCommand = Utils.SqlQueryProcedureString("[Solicitudes].[SP_PeticionReestructuraDocsDigCON]", parametros.Select(p => p.Value).ToList());
            resultado.PeticionReestDocs = Database.SqlQuery<PeticionReestDocDigitalVM>(sqlCommand, parametros.Select(p => p.Value).ToArray()).ToList();
            int.TryParse(parametros["@TotalRegistros"].Value.ToString(), out int totalRegistros);
            resultado.TotalRegistros = totalRegistros;
            return resultado;
        }

        public ValidarPeticionReestructuraResponse Creditos_SP_PetResLiberarCreditoPRO(ValidarPeticionReestructuraRequest peticion)
        {
            ValidarPeticionReestructuraResponse resultado = new ValidarPeticionReestructuraResponse();
            Dictionary<string, SqlParameter> parametros = new Dictionary<string, SqlParameter>()
            {
                { "@IdSolicitudNuevo", new SqlParameter("@IdSolicitudNuevo", SqlDbType.Int) { Value = peticion.IdSolicitudNuevo } },
                { "@IdUsuario", new SqlParameter("@IdUsuario", SqlDbType.Int) { Value = peticion.IdUsuario } },
                { "@Comentario", new SqlParameter("@Comentario", SqlDbType.VarChar) { Value = peticion.Comentario } },
                { "@OperacionCorrecta", new SqlParameter("@OperacionCorrecta", SqlDbType.Bit) { Direction = ParameterDirection.Output } },
                { "@MensajeOperacion", new SqlParameter("@MensajeOperacion", SqlDbType.VarChar) { Direction = ParameterDirection.Output, Size = 200 } }
            };
            string sqlCommand = Utils.SqlQueryProcedureString("[Creditos].[SP_PetResLiberarCreditoPRO]", parametros.Select(p => p.Value).ToList());
            Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, sqlCommand, parametros.Select(p => p.Value).ToArray());
            bool.TryParse(parametros["@OperacionCorrecta"].Value.ToString(), out bool operacionCorrecta);
            string mensajeOperacion = parametros["@MensajeOperacion"].Value.ToString();
            resultado.Error = !operacionCorrecta;
            resultado.MensajeOperacion = mensajeOperacion;

            return resultado;
        }

    }
}
