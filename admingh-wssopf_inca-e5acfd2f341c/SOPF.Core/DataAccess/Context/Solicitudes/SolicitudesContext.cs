﻿using SOPF.Core.Poco.Solicitudes;
using System.Data.Entity;

namespace SOPF.Core.DataAccess
{
    public partial class CreditOrigContext
    {
        public DbSet<TB_CATDocumentos> Solicitudes_TB_CATDocumentos { get; set; }
        public DbSet<TB_CATEstatusPeticionReestructuras> Solicitudes_TB_CATEstatusPeticionReestructuras { get; set; }
        public DbSet<TB_CATMotivoReestructuras> Solicitudes_TB_CATMotivoReestructuras { get; set; }
        public DbSet<TB_CATTipoReestructuras> Solicitudes_TB_CATTipoReestructuras { get; set; }
        public DbSet<TB_DocumentoArchivo> Solicitudes_TB_DocumentoArchivo { get; set; }
        public DbSet<TB_DocumentoDigital> Solicitudes_TB_DocumentoDigital { get; set; }
        public DbSet<TB_DocumentoXSolicitud> Solicitudes_TB_DocumentoXSolicitud { get; set; }
        public DbSet<Tb_Expedientes> Solicitudes_Tb_Expedientes { get; set; }
        public DbSet<TB_FotoPeticionReestructuras> Solicitudes_TB_FotoPeticionReestructuras { get; set; }
        public DbSet<TB_FotoReciboPetRes> Solicitudes_TB_FotoReciboPetRes { get; set; }
        public DbSet<TB_PeticionReestructuras> Solicitudes_TB_PeticionReestructuras { get; set; }
        public DbSet<TB_Proceso> Solicitudes_TB_Proceso { get; set; }
        public DbSet<TB_ProcesoDigital> Solicitudes_TB_ProcesoDigital { get; set; }
        public DbSet<TB_ProcesoDigitalConfig> Solicitudes_TB_ProcesoDigitalConfig { get; set; }
        public DbSet<TB_ProcesoDigitalConfigDet> Solicitudes_TB_ProcesoDigitalConfigDet { get; set; }
        public DbSet<TB_SolicitudAmpliacionExpress> Solicitudes_SolicitudAmpliacionExpress { get; set; }
        public DbSet<TB_SolicitudDocDigital> Solicitudes_TB_SolicitudDocDigital { get; set; }
        public DbSet<TB_SolicitudFirmaDigital> Solicitudes_TB_SolicitudFirmaDigital { get; set; }
        public DbSet<TB_SolicitudFirmaDigitalArchivo> Solicitudes_TB_SolicitudFirmaDigitalArchivo { get; set; }
        public DbSet<TB_SolicitudProcesoDigital> Solicitudes_TB_SolicitudProcesoDigital { get; set; }
        public DbSet<TB_SolicitudPruebaVida> Solicitudes_TB_SolicitudPruebaVida { get; set; }
        public DbSet<TB_SolicitudPruebaVidaArchivo> Solicitudes_TB_SolicitudPruebaVidaArchivo { get; set; }
        public DbSet<TB_SolicitudPruebaVidaEstatus> Solicitudes_TB_SolicitudPruebaVidaEstatus { get; set; }
    }
}
