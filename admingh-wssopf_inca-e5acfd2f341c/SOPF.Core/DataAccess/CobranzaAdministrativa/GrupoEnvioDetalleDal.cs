#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.CobranzaAdministrativa;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: GrupoEnvioDetalleDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla GrupoEnvioDetalle
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.CobranzaAdministrativa
{
    public class GrupoEnvioDetalleDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public GrupoEnvioDetalleDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods
        public DataResultCobAdm.Resultado InsertarGrupoEnvioDetalle(int Accion, GrupoEnvioDetalle entidad)
        {
            DataResultCobAdm.Resultado respuesta = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("CobranzaAdministrativa.ABCGrupoEnvio_Detalle"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdGrupoEnvio", DbType.Int32, entidad.IdGrupoEnvio);
                    db.AddInParameter(com, "@idGrupoDetalle", DbType.Int32, entidad.IdGrupoDetalle);
                    db.AddInParameter(com, "@idTipoConvenio", DbType.Int32, entidad.IdConvenio);
                    db.AddInParameter(com, "@Frecuencia", DbType.String, entidad.Frecuencia);
                    //db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                respuesta = new DataResultCobAdm.Resultado();
                                if (!reader.IsDBNull(0)) respuesta.Bandera = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) respuesta.Descripcion = reader[1].ToString();                                
                            }
                        }

                        reader.Dispose();
                    }
                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return respuesta;
        }

        public GrupoEnvioDetalle ObtenerGrupoEnvioDetalle()
        {
            GrupoEnvioDetalle resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
                {
                    //Parametros
                    //db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new GrupoEnvioDetalle();
                            //Lectura de los datos del ResultSet
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public void ActualizarGrupoEnvioDetalle()
        {
        }

        public void EliminarGrupoEnvioDetalle()
        {
        }
        #endregion

        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}