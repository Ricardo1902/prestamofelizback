﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.CobranzaAdministrativa;


namespace SOPF.Core.DataAccess.CobranzaAdministrativa
{
    public class MoraConfgDal
    {
           #region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
        public MoraConfgDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion

        public List<MoraConfig> ObtenerMoraConfig(int Accion, int idGrupoMora)
        {
            List<MoraConfig> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("CobranzaAdministrativa.SelMoraConfig"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@idGpoMora", DbType.Int32, idGrupoMora);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<MoraConfig>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                MoraConfig grupo = new MoraConfig();
                                if (!reader.IsDBNull(0)) grupo.IdConfig = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) grupo.idGpoMora = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) grupo.NumAmorti = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) grupo.Porcentaje = Convert.ToDecimal(reader[3]);
                                if (!reader.IsDBNull(4)) grupo.monto = Convert.ToDecimal(reader[4]);
                                
                                resultado.Add(grupo);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
    }
}
