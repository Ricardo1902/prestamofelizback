﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.CobranzaAdministrativa;

namespace SOPF.Core.DataAccess.CobranzaAdministrativa
{
    public class GpoEnvio_MoraDal
    {
        #region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
        public GpoEnvio_MoraDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion

        public List<GpoEnvio_Mora> ObtenerGrupoEnvio_Mora(int Accion, int idGrupo)
        {
            List<GpoEnvio_Mora> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("CobranzaAdministrativa.SelGrupoEnvio_Mora"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdGrupo", DbType.Int32, idGrupo);
                    

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<GpoEnvio_Mora>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                GpoEnvio_Mora grupo = new GpoEnvio_Mora();
                                if (!reader.IsDBNull(0)) grupo.IdGrupoMora = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) grupo.IdGrupo = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) grupo.IdMora = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) grupo.IdEstatus = Convert.ToInt32(reader[3]);
                                if (!reader.IsDBNull(4)) grupo.NumAmortizaciones = Convert.ToInt32(reader[4]);
                                if (!reader.IsDBNull(6)) grupo.ConsecutivoReg = Convert.ToInt32(reader[6]);
                                resultado.Add(grupo);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
    }
}
