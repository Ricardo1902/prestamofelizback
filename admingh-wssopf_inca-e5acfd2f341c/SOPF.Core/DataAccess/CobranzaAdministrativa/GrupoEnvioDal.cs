#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.CobranzaAdministrativa;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: GrupoEnvioDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla GrupoEnvio
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.CobranzaAdministrativa
{
    public class GrupoEnvioDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public GrupoEnvioDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public int InsertarGrupoEnvio(GrupoEnvio entidad,int Accion)
        {
            int Respuesta = 0;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("CobranzaAdministrativa.ABC_GrupoEnvio"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@Descripcion", DbType.String, entidad.GpoEnvio);
				
					//Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0)) Respuesta = Convert.ToInt32(reader[0]);
                        }
                    }

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();

				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
            return Respuesta;
        }
        
        public List<GrupoEnvio> ObtenerGrupoEnvio(int Accion, int idGrupoenvio, string GrupoEnvio)
        {
			List<GrupoEnvio> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("CobranzaAdministrativa.SelGrupoEnvio"))
				{
					//Parametros
					db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdGrupoEnvio", DbType.Int32, idGrupoenvio);
                    db.AddInParameter(com, "@GrupoEnvio", DbType.String, GrupoEnvio);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<GrupoEnvio>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                GrupoEnvio grupo = new GrupoEnvio();
                                if (!reader.IsDBNull(0)) grupo.IdGrupoEnvio = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) grupo.GpoEnvio = reader[1].ToString();
                                resultado.Add(grupo);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarGrupoEnvio()
        {
        }

        public void EliminarGrupoEnvio()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}