﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.CobranzaAdministrativa;

namespace SOPF.Core.DataAccess.CobranzaAdministrativa
{
    public class EnvioCobroDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public EnvioCobroDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods
        public List<EnvioCobro> SelEnvioCobro()
        {
            List<EnvioCobro> Resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("cobranzaAdministrativa.EnvioCobro"))
                {
                    //Parametros
                    
                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        Resultado = new List<EnvioCobro>();
                        while (reader.Read())
                        {
                            EnvioCobro envio = new EnvioCobro();
                            if (!reader.IsDBNull(0)) envio.IdGrupo = Convert.ToInt32(reader[0]);
                            if (!reader.IsDBNull(1)) envio.idBanco = Convert.ToInt32(reader[1]);
                            if (!reader.IsDBNull(2)) envio.Fecha = Convert.ToDateTime(reader[2]);
                            if (!reader.IsDBNull(3)) envio.TotalArchivos = Convert.ToInt32(reader[3]);
                            if (!reader.IsDBNull(4)) envio.Particiones = reader[4].ToString();
                            if (!reader.IsDBNull(5)) envio.DiasVencido = Convert.ToInt32(reader[5]);
                            if (!reader.IsDBNull(6)) envio.DiasIni = Convert.ToInt32(reader[6]);
                            if (!reader.IsDBNull(7)) envio.TotParticiones = Convert.ToInt32(reader[6]);
                            Resultado.Add(envio);
                        }

                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Resultado;
        }

        public GrupoEnvio ObtenerGrupoEnvio()
        {
            GrupoEnvio resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
                {
                    //Parametros
                    //db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new GrupoEnvio();
                            //Lectura de los datos del ResultSet
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public void ActualizarGrupoEnvio()
        {
        }

        public void EliminarGrupoEnvio()
        {
        }
        #endregion

        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
