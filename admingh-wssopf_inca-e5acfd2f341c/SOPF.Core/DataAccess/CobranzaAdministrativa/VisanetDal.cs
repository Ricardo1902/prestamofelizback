﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Model.Cobranza;

namespace SOPF.Core.DataAccess.CobranzaAdministrativa
{
    public class VisanetDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public VisanetDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods
        public List<PagoFallidoVisanetVM> DescargarPagosFallidosVisanet(int IdPagoMasivo)
        {
            List<PagoFallidoVisanetVM> lista = new List<PagoFallidoVisanetVM>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_VisanetOrderCON"))
                {
                    db.AddInParameter(com, "@IdPagoMasivo", DbType.Int32, IdPagoMasivo);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                PagoFallidoVisanetVM pago = new PagoFallidoVisanetVM();
                                pago.Solicitud = Utils.GetString(reader, "Solicitud");
                                pago.Monto = Utils.GetDecimal(reader, "Monto");
                                pago.NoTarjeta = Utils.GetString(reader, "NoTarjeta");
                                pago.MesExpiracion = Utils.GetInt(reader, "MesExpiracion");
                                pago.AnioExpiracion = Utils.GetInt(reader, "AnioExpiracion");
                                lista.Add(pago);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lista;
        }
        #endregion

        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
