using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Sistema;
using SOPF.Core.Entities;

namespace SOPF.Core.DataAccess.Sistema
{
    public class SeguridadDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public SeguridadDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region "Usuarios"

        public Resultado<UsuarioEntity> GuardarUsuario(UsuarioEntity entity)
        {
            Resultado<UsuarioEntity> objReturn = new Resultado<UsuarioEntity>();

            try
            {
                string sp = (entity.IdUsuario == 0) ? "Sistema.SP_UsuarioALT" : "Sistema.SP_UsuarioACT";

                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand(sp))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, (entity.IdUsuario > 0) ? entity.IdUsuario : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdTipoUsuario", DbType.Int32, (entity.TipoUsuario.IdTipoUsuario > 0) ? entity.TipoUsuario.IdTipoUsuario : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdDepartamento", DbType.Int32, (entity.Departamento.IdDepartamento > 0) ? entity.Departamento.IdDepartamento : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, (entity.Sucursal.IdSucursal > 0) ? entity.Sucursal.IdSucursal : (object)DBNull.Value);

                    db.AddInParameter(com, "@Nombre", DbType.String, entity.Nombre);
                    db.AddInParameter(com, "@ApellidoPaterno", DbType.String, entity.ApellidoPaterno);
                    db.AddInParameter(com, "@ApellidoMaterno", DbType.String, entity.ApellidoMaterno);
                    db.AddInParameter(com, "@RFC", DbType.String, entity.RFC);
                    db.AddInParameter(com, "@Lada", DbType.String, entity.Lada);
                    db.AddInParameter(com, "@Telefono", DbType.String, entity.Telefono);
                    db.AddInParameter(com, "@Email", DbType.String, entity.Email);
                    db.AddInParameter(com, "@Usuario", DbType.String, entity.Usuario);
                    db.AddInParameter(com, "@Clave", DbType.String, (!string.IsNullOrEmpty(entity.Clave.Trim())) ? entity.Clave.Trim().ToUpper() : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, (entity.IdEstatus >= 0) ? entity.IdEstatus : (object)DBNull.Value);
                    db.AddInParameter(com, "@Bloquear", DbType.Int32, (entity.Bloquear) ? 1 : 0);
                    db.AddInParameter(com, "@EnSession", DbType.Int32, (entity.EnSession) ? 1 : 0);
                    db.AddInParameter(com, "@Inicializar", DbType.Boolean, entity.Inicializar);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new Resultado<UsuarioEntity>();
                            UsuarioEntity o;

                            while (reader.Read())
                            {
                                o = new UsuarioEntity();

                                // Lectura de los datos del ResultSet
                                o.IdUsuario = Utils.GetInt(reader, "IdUsuario");
                                o.Departamento.IdDepartamento = Utils.GetInt(reader, "IdDepartamento");
                                o.Departamento.Departamento = Utils.GetString(reader, "DepartamentoDesc");
                                o.Nombre = Utils.GetString(reader, "Nombre");
                                o.ApellidoPaterno = Utils.GetString(reader, "ApellidoPaterno");
                                o.ApellidoMaterno = Utils.GetString(reader, "ApellidoMaterno");
                                o.RFC = Utils.GetString(reader, "RFC");
                                o.Lada = Utils.GetString(reader, "Lada");
                                o.Telefono = Utils.GetString(reader, "Telefono");
                                o.Email = Utils.GetString(reader, "Email");
                                o.Usuario = Utils.GetString(reader, "Usuario");
                                o.TipoUsuario.IdTipoUsuario = Utils.GetInt(reader, "IdTipoUsuario");
                                o.TipoUsuario.TipoUsuario = Utils.GetString(reader, "TipoUsuarioDesc");
                                o.Sucursal.IdSucursal = Utils.GetInt(reader, "IdSucursal");
                                o.Sucursal.Sucursal = Utils.GetString(reader, "SucursalDesc");
                                o.IdEstatus = Utils.GetInt(reader, "IdEstatus");
                                o.Bloquear = Utils.GetBool(reader, "Bloquear");
                                o.EnSession = Utils.GetBool(reader, "EnSession");
                                o.Inicializar = Utils.GetBool(reader, "Inicializar");

                                objReturn.ResultObject = o;
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<List<UsuarioEntity>> FiltrarUsuario(UsuarioEntity entity)
        {
            Resultado<List<UsuarioEntity>> objReturn = new Resultado<List<UsuarioEntity>>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_UsuarioCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, (entity.IdUsuario > 0) ? entity.IdUsuario : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdTipoUsuario", DbType.Int32, (entity.TipoUsuario.IdTipoUsuario > 0) ? entity.TipoUsuario.IdTipoUsuario : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdDepartamento", DbType.Int32, (entity.Departamento.IdDepartamento > 0) ? entity.Departamento.IdDepartamento : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, (entity.Sucursal.IdSucursal > 0) ? entity.Sucursal.IdSucursal : (object)DBNull.Value);
                    db.AddInParameter(com, "@Nombre", DbType.String, entity.Nombre);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, (entity.IdEstatus >= 0) ? entity.IdEstatus : (object)DBNull.Value);
                    db.AddInParameter(com, "@Usuario", DbType.String, string.IsNullOrEmpty(entity.Usuario) ? (object)DBNull.Value : entity.Usuario);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new Resultado<List<UsuarioEntity>>();
                            UsuarioEntity o;

                            while (reader.Read())
                            {
                                o = new UsuarioEntity();

                                // Lectura de los datos del ResultSet
                                o.IdUsuario = Utils.GetInt(reader, "IdUsuario");
                                o.Departamento.IdDepartamento = Utils.GetInt(reader, "IdDepartamento");
                                o.Departamento.Departamento = Utils.GetString(reader, "DepartamentoDesc");
                                o.Nombre = Utils.GetString(reader, "Nombre");
                                o.ApellidoPaterno = Utils.GetString(reader, "ApellidoPaterno");
                                o.ApellidoMaterno = Utils.GetString(reader, "ApellidoMaterno");
                                o.RFC = Utils.GetString(reader, "RFC");
                                o.Lada = Utils.GetString(reader, "Lada");
                                o.Telefono = Utils.GetString(reader, "Telefono");
                                o.Email = Utils.GetString(reader, "Email");
                                o.Usuario = Utils.GetString(reader, "Usuario");
                                o.TipoUsuario.IdTipoUsuario = Utils.GetInt(reader, "IdTipoUsuario");
                                o.TipoUsuario.TipoUsuario = Utils.GetString(reader, "TipoUsuarioDesc");
                                o.Sucursal.IdSucursal = Utils.GetInt(reader, "IdSucursal");
                                o.Sucursal.Sucursal = Utils.GetString(reader, "SucursalDesc");
                                o.IdEstatus = Utils.GetInt(reader, "IdEstatus");
                                o.Bloquear = Utils.GetBool(reader, "Bloquear");
                                o.EnSession = Utils.GetBool(reader, "EnSession");
                                o.Inicializar = Utils.GetBool(reader, "Inicializar");

                                objReturn.ResultObject.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<MenuEntity> ObtenerMenuUsuario(UsuarioEntity entity)
        {
            List<MenuEntity> objReturn = new List<MenuEntity>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_MenuUsuarioCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, entity.IdUsuario);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new List<MenuEntity>();
                            MenuEntity o;

                            while (reader.Read())
                            {
                                o = new MenuEntity();

                                // Lectura de los datos del ResultSet
                                o.IdMenu = Utils.GetInt(reader, "IdMenu");
                                o.Menu = Utils.GetString(reader, "Menu");
                                o.Orden = Utils.GetInt(reader, "Orden");

                                objReturn.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<ReporteEntity> ObtenerReportesUsuario(UsuarioEntity entity)
        {
            List<ReporteEntity> objReturn = new List<ReporteEntity>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_ReportesUsuarioCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, entity.IdUsuario);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new List<ReporteEntity>();
                            ReporteEntity o;

                            while (reader.Read())
                            {
                                o = new ReporteEntity();

                                // Lectura de los datos del ResultSet
                                o.IdReporte = Utils.GetInt(reader, "IdReporte");
                                o.Nombre = Utils.GetString(reader, "Nombre");
                                o.Orden = Utils.GetInt(reader, "Orden");

                                objReturn.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<AccionEntity> ObtenerAccionesUsuario(UsuarioEntity entity)
        {
            List<AccionEntity> objReturn = new List<AccionEntity>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_AccionesUsuarioCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, entity.IdUsuario);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new List<AccionEntity>();
                            AccionEntity o;

                            while (reader.Read())
                            {
                                o = new AccionEntity
                                {
                                    IdMenuAccion = Utils.GetInt(reader, "IdMenuAccion"),
                                    Menu = Utils.GetString(reader, "Menu"),
                                    Accion = Utils.GetString(reader, "Accion"),
                                    Activo = Utils.GetBool(reader, "Activo")
                                };

                                objReturn.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<MenuEntity> FiltrarMenu(MenuEntity entity)
        {
            List<MenuEntity> objReturn = new List<MenuEntity>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_FLTMenuCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdMenu", DbType.Int32, (entity.IdMenu > 0) ? entity.IdMenu : (object)DBNull.Value);
                    db.AddInParameter(com, "@Menu", DbType.String, entity.Menu);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, (entity.Activo) ? 1 : 0);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new List<MenuEntity>();
                            MenuEntity o;

                            while (reader.Read())
                            {
                                o = new MenuEntity();

                                // Lectura de los datos del ResultSet
                                o.IdMenu = Utils.GetInt(reader, "IdMenu");
                                o.Menu = Utils.GetString(reader, "Menu");
                                o.Descripcion = Utils.GetString(reader, "Descripcion");
                                o.Carpeta = Utils.GetString(reader, "Carpeta");
                                o.Url = Utils.GetString(reader, "Url");
                                o.TipoRedirect = Utils.GetString(reader, "TipoRedirect");
                                o.IdPadre = Utils.GetInt(reader, "IdPadre");
                                o.Activo = (Utils.GetInt(reader, "IdEstatus") == 1) ? true : false;
                                o.Orden = Utils.GetInt(reader, "Orden");

                                objReturn.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<AccionEntity> FiltrarAccion(AccionEntity entity)
        {
            List<AccionEntity> objReturn = new List<AccionEntity>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_FLTAccionesCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdMenuAccion", DbType.Int32, (entity.IdMenuAccion > 0) ? entity.IdMenuAccion : (object)DBNull.Value);
                    db.AddInParameter(com, "@Accion", DbType.String, entity.Accion);
                    db.AddInParameter(com, "@Activo", DbType.Boolean, (entity.Activo) ? 1 : 0);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new List<AccionEntity>();
                            AccionEntity o;

                            while (reader.Read())
                            {
                                o = new AccionEntity
                                {
                                    IdMenuAccion = Utils.GetInt(reader, "IdMenuAccion"),
                                    Menu = Utils.GetString(reader, "Menu"),
                                    Accion = Utils.GetString(reader, "Accion"),
                                    Activo = Utils.GetBool(reader, "Activo")
                                };

                                objReturn.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<ReporteEntity> FiltrarReporte(ReporteEntity entity)
        {
            List<ReporteEntity> objReturn = new List<ReporteEntity>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_FLTReporteCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdReporte", DbType.Int32, (entity.IdReporte > 0) ? entity.IdReporte : (object)DBNull.Value);
                    db.AddInParameter(com, "@Reporte", DbType.String, entity.Nombre);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, (entity.Activo) ? 1 : 0);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new List<ReporteEntity>();
                            ReporteEntity o;

                            while (reader.Read())
                            {
                                o = new ReporteEntity();

                                // Lectura de los datos del ResultSet
                                o.IdReporte = Utils.GetInt(reader, "IdReporte");
                                o.Nombre = Utils.GetString(reader, "Nombre");
                                o.Descripcion = Utils.GetString(reader, "Descripcion");
                                o.Activo = (Utils.GetInt(reader, "IdEstatus") == 1) ? true : false;
                                o.Sp = Utils.GetString(reader, "Sp");
                                o.IdPagina = Utils.GetInt(reader, "IdPagina");
                                o.Orden = Utils.GetInt(reader, "Orden");

                                objReturn.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public void InsertarAccesoMenuUsuario(int IdUsuario, int IdMenu)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_MenuUsuarioALT"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@IdMenu", DbType.Int32, IdMenu);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertarAccesoReporteUsuario(int IdUsuario, int IdReporte)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_ReporteUsuarioALT"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@IdReporte", DbType.Int32, IdReporte);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertarAccesoAccionUsuario(int IdUsuario, int IdMenuAccion)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_AccionUsuarioALT"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@IdMenuAccion", DbType.Int32, IdMenuAccion);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void QuitarAccesoAccionUsuario(int IdUsuario, int IdMenuAccion)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_AccionUsuarioBAJ"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@IdMenuAccion", DbType.Int32, IdMenuAccion);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void QuitarAccesoMenuUsuario(int IdUsuario, int IdMenu)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_MenuUsuarioBAJ"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@IdMenu", DbType.Int32, IdMenu);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void QuitarAccesoReporteUsuario(int IdUsuario, int IdReporte)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_ReporteUsuarioBAJ"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@IdReporte", DbType.Int32, IdReporte);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Perfiles(Tipo Usuario)"

        public Resultado<List<TipoUsuarioEntity>> FiltrarTipoUsuario(TipoUsuarioEntity entity)
        {
            Resultado<List<TipoUsuarioEntity>> objReturn = new Resultado<List<TipoUsuarioEntity>>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_FLTPerfilCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdTipoUsuario", DbType.Int32, (entity.IdTipoUsuario > 0) ? entity.IdTipoUsuario : (object)DBNull.Value);
                    db.AddInParameter(com, "@TipoUsuario", DbType.String, entity.TipoUsuario);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, (entity.IdEstatus >= 0) ? entity.IdEstatus : (object)DBNull.Value);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new Resultado<List<TipoUsuarioEntity>>();
                            TipoUsuarioEntity o;

                            while (reader.Read())
                            {
                                o = new TipoUsuarioEntity();

                                // Lectura de los datos del ResultSet
                                o.IdTipoUsuario = Utils.GetInt(reader, "IdTipoUsuario");
                                o.TipoUsuario = Utils.GetString(reader, "TipoUsuario");
                                o.Descripcion = Utils.GetString(reader, "Descripcion");
                                o.IdEstatus = Utils.GetInt(reader, "IdEstatus");

                                objReturn.ResultObject.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        #endregion

        #region "Departamentos"

        public Resultado<List<DepartamentoEntity>> FiltrarDepartamento(DepartamentoEntity entity)
        {
            Resultado<List<DepartamentoEntity>> objReturn = new Resultado<List<DepartamentoEntity>>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_FLTDepartamentoCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdDepartamento", DbType.Int32, (entity.IdDepartamento > 0) ? entity.IdDepartamento : (object)DBNull.Value);
                    db.AddInParameter(com, "@Departamento", DbType.String, entity.Departamento);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, (entity.IdEstatus >= 0) ? entity.IdEstatus : (object)DBNull.Value);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new Resultado<List<DepartamentoEntity>>();
                            DepartamentoEntity o;

                            while (reader.Read())
                            {
                                o = new DepartamentoEntity();

                                // Lectura de los datos del ResultSet
                                o.IdDepartamento = Utils.GetInt(reader, "IdDepartamento");
                                o.Departamento = Utils.GetString(reader, "Departamento");
                                o.IdEstatus = Utils.GetInt(reader, "IdEstatus");

                                objReturn.ResultObject.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        #endregion

        #region "Sucursal"

        public Resultado<List<SucursalEntity>> FiltrarSucursal(SucursalEntity entity)
        {
            Resultado<List<SucursalEntity>> objReturn = new Resultado<List<SucursalEntity>>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_FLTSucursalCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, (entity.IdSucursal > 0) ? entity.IdSucursal : (object)DBNull.Value);
                    db.AddInParameter(com, "@Sucursal", DbType.String, entity.Sucursal);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, (entity.IdEstatus >= 0) ? entity.IdEstatus : (object)DBNull.Value);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new Resultado<List<SucursalEntity>>();
                            SucursalEntity o;

                            while (reader.Read())
                            {
                                o = new SucursalEntity();

                                // Lectura de los datos del ResultSet
                                o.IdSucursal = Utils.GetInt(reader, "IdSucursal");
                                o.Sucursal = Utils.GetString(reader, "Sucursal");
                                o.IdEstatus = Utils.GetInt(reader, "IdEstatus");

                                objReturn.ResultObject.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        #endregion
    }
}