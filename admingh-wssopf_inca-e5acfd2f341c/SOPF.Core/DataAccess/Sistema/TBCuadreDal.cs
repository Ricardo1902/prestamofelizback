using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Sistema;
using SOPF.Core.Entities;

namespace SOPF.Core.DataAccess.Sistema
{
    public class TBCuadreDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCuadreDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
        #endregion

        #region "CRUD Methods"

        public Resultado<CuadreDatosSolicitudEntity> ObtenerDatosSolicitud(CuadreDatosSolicitudEntity entity)
        {
            Resultado<CuadreDatosSolicitudEntity> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_Cuadre"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_DATOSSOLICITUD");
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, (entity.IdSolicitud > 0) ? entity.IdSolicitud : (object)DBNull.Value);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            if (reader.Read())
                            {
                                objReturn = new Resultado<CuadreDatosSolicitudEntity>();

                                // Lectura de los datos del ResultSet                                
                                objReturn.ResultObject.IdSolicitud = Utils.GetInt(reader, "IdSolicitud");
                                objReturn.ResultObject.IdCuenta = Utils.GetInt(reader, "IdCuenta");
                                objReturn.ResultObject.IdSolicitudAmplio = Utils.GetInt(reader, "IdSolicitudAmplio");
                                objReturn.ResultObject.IdPromotor = Utils.GetInt(reader, "IdPromotor");
                                objReturn.ResultObject.IdTipoCredito = Utils.GetInt(reader, "IdTipoCredito");
                                objReturn.ResultObject.FechaSolicitud = Utils.GetDateTime(reader, "FechaSolicitud");
                                objReturn.ResultObject.FechaCredito = Utils.GetDateTime(reader, "FechaCredito");
                                objReturn.ResultObject.ClienteNombre = Utils.GetString(reader, "ClienteNombre");
                                objReturn.ResultObject.ClienteApPaterno = Utils.GetString(reader, "ClienteApPaterno");
                                objReturn.ResultObject.ClienteApMaterno = Utils.GetString(reader, "ClienteApMaterno");
                                objReturn.ResultObject.IdTipoConvenio = Utils.GetInt(reader, "IdTipoConvenio");
                                objReturn.ResultObject.IdConvenio = Utils.GetInt(reader, "IdConvenio");                            
                                objReturn.ResultObject.IdProducto = Utils.GetInt(reader, "IdProducto");
                                objReturn.ResultObject.TasaInteres = Utils.GetDecimal(reader, "TasaInteres");
                                objReturn.ResultObject.MontoCuota = Utils.GetDecimal(reader, "Cuota");
                                objReturn.ResultObject.CostoTotalCredito = Utils.GetDecimal(reader, "CostoTotalCredito");
                                objReturn.ResultObject.MontoGAT = Utils.GetDecimal(reader, "MontoGAT");
                                objReturn.ResultObject.MontoUsoCanal = Utils.GetDecimal(reader, "MontoUsoCanal");
                                objReturn.ResultObject.TipoSeguroDesgravamen = Utils.GetInt(reader, "TipoSeguroDesgravamen");
                                objReturn.ResultObject.SeguroDesgravamen = Utils.GetDecimal(reader, "SeguroDesgravamen");
                                objReturn.ResultObject.Capital = Utils.GetDecimal(reader, "Capital");
                                objReturn.ResultObject.ImporteCredito = Utils.GetDecimal(reader, "ImporteCredito");
                                objReturn.ResultObject.SaldoCapital = Utils.GetDecimal(reader, "SaldoCapital");
                                objReturn.ResultObject.SaldoVencido = Utils.GetDecimal(reader, "SaldoVencido");
                                objReturn.ResultObject.IdOrigen = Utils.GetInt(reader, "Origen_Id");

                                // Datos de la Ampliacion
                                objReturn.ResultObject.DatosAmpliacion.IdSolicitudAmplia = Utils.GetInt(reader, "IdSolicitudAmplia");
                                objReturn.ResultObject.DatosAmpliacion.IdCuenta = Utils.GetInt(reader, "IdCuentaAmplia");
                                objReturn.ResultObject.DatosAmpliacion.FechaLiquidar = Utils.GetDateTime(reader, "FechaLiquidar");
                                objReturn.ResultObject.DatosAmpliacion.CapitalLiquidar = Utils.GetDecimal(reader, "CapitalLiquidar");
                                objReturn.ResultObject.DatosAmpliacion.InteresLiquidar = Utils.GetDecimal(reader, "InteresLiquidar");
                                objReturn.ResultObject.DatosAmpliacion.IGVLiquidar = Utils.GetDecimal(reader, "IGVLiquidar");
                                objReturn.ResultObject.DatosAmpliacion.GATLiquidar = Utils.GetDecimal(reader, "GATLiquidar");
                                objReturn.ResultObject.DatosAmpliacion.SeguroLiquidar = Utils.GetDecimal(reader, "SeguroLiquidar");
                                objReturn.ResultObject.DatosAmpliacion.TotalLiquidar = Utils.GetDecimal(reader, "TotalLiquidar");
                                objReturn.ResultObject.DatosAmpliacion.ComentariosSistema = Utils.GetString(reader, "ComentariosSistemas");


                                // Cargar datos del Estatus del Credito
                                objReturn.ResultObject.EstatusCredito.IdEstatus = Utils.GetInt(reader, "IdEstatusCredito");
                                objReturn.ResultObject.EstatusCredito.EstatusClave = Utils.GetString(reader, "ClaveEstatusCredito");
                                objReturn.ResultObject.EstatusCredito.EstatusDesc = Utils.GetString(reader, "DescEstatusCredito");

                                // Cargar datos del Estatus de la Solicitud
                                objReturn.ResultObject.EstatusSolicitud.IdEstatus = Utils.GetInt(reader, "IdEstatusSolicitud");
                                objReturn.ResultObject.EstatusSolicitud.EstatusClave = Utils.GetString(reader, "ClaveEstatusSolicitud");
                                objReturn.ResultObject.EstatusSolicitud.EstatusDesc = Utils.GetString(reader, "DescEstatusSolicitud");
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> ActualizarDatosSolicitud(CuadreDatosSolicitudEntity entity)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_Cuadre"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "UPD_DATOSSOLICITUD");
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, entity.IdSolicitud);
                    db.AddInParameter(com, "@IdEstatusCredito", DbType.Int32, entity.EstatusCredito.IdEstatus);
                    db.AddInParameter(com, "@IdEstatusSolicitud", DbType.Int32, entity.EstatusSolicitud.IdEstatus);
                    db.AddInParameter(com, "@FechaSolicitud", DbType.DateTime, entity.FechaSolicitud);
                    db.AddInParameter(com, "@FechaCredito", DbType.DateTime, entity.FechaCredito);
                    db.AddInParameter(com, "@IdTipoCredito", DbType.Int32, entity.IdTipoCredito);
                    db.AddInParameter(com, "@IdProducto", DbType.Int32, entity.IdProducto);
                    db.AddInParameter(com, "@TasaInteres", DbType.Decimal, entity.TasaInteres);
                    db.AddInParameter(com, "@MontoCuota", DbType.Decimal, entity.MontoCuota);
                    db.AddInParameter(com, "@MontoGAT", DbType.Decimal, entity.MontoGAT);
                    db.AddInParameter(com, "@MontoUsoCanal", DbType.Decimal, entity.MontoUsoCanal);
                    db.AddInParameter(com, "@SeguroDesgravamen", DbType.Decimal, entity.SeguroDesgravamen);
                    db.AddInParameter(com, "@ImporteCredito", DbType.Decimal, entity.ImporteCredito);
                    db.AddInParameter(com, "@IdPromotor", DbType.Int32, entity.IdPromotor);
                    db.AddInParameter(com, "@IdOrigen", DbType.Int32, entity.IdOrigen);

                    // Datos de la Ampliacion
                    if (entity.DatosAmpliacion != null)
                    {
                        db.AddInParameter(com, "@IdSolicitudAmplia", DbType.Int32, entity.DatosAmpliacion.IdSolicitudAmplia);
                        db.AddInParameter(com, "@FechaRees", DbType.DateTime, entity.DatosAmpliacion.FechaLiquidar);
                        db.AddInParameter(com, "@CapitalLiquidar", DbType.Decimal, entity.DatosAmpliacion.CapitalLiquidar);
                        db.AddInParameter(com, "@InteresLiquidar", DbType.Decimal, entity.DatosAmpliacion.InteresLiquidar);
                        db.AddInParameter(com, "@IGVLiquidar", DbType.Decimal, entity.DatosAmpliacion.IGVLiquidar);
                        db.AddInParameter(com, "@SeguroLiquidar", DbType.Decimal, entity.DatosAmpliacion.SeguroLiquidar);
                        db.AddInParameter(com, "@GATLiquidar", DbType.Decimal, entity.DatosAmpliacion.GATLiquidar);
                        db.AddInParameter(com, "@ComentariosSistemas", DbType.String, entity.DatosAmpliacion.ComentariosSistema);
                    }

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);
                   
                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }                
            }
            catch (Exception ex)
            {                
                objReturn = new Resultado<bool>();
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<bool> ReprocesarTablaAmortizacion(int IdSolicitud, string TipoTablaAmortizacion)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_Cuadre"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "UPD_TABLAAMORTIZACION");
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, IdSolicitud);
                    db.AddInParameter(com, "@TipoTablaAmortizacion", DbType.String, TipoTablaAmortizacion);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn = new Resultado<bool>();
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<bool> CambioFechaTablaAmortizacion(int IdSolicitud, DateTime FechaInicio)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_Cuadre"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "UPD_FECHASTABLAAMORTIZACION");
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, IdSolicitud);
                    db.AddInParameter(com, "@FechaInicio_TA", DbType.DateTime, FechaInicio);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn = new Resultado<bool>();
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<DatosAmpliacionEntity> CalcularDatosAmpliacion(int IdSolicitud, int IdSolicitudAmplia, DateTime FechaAmpliacion)
        {
            Resultado<DatosAmpliacionEntity> objReturn = new Resultado<DatosAmpliacionEntity>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Sistema.SP_Cuadre"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "CALC_DATOSAMPLIACION");
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, IdSolicitud);
                    db.AddInParameter(com, "@IdSolicitudAmplia", DbType.Int32, IdSolicitudAmplia);
                    db.AddInParameter(com, "@FechaRees", DbType.DateTime, FechaAmpliacion);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            if (reader.Read())
                            {
                                objReturn.ResultObject.IdSolicitud = Utils.GetInt(reader, "IdSolicitud");
                                objReturn.ResultObject.IdCuenta = Utils.GetInt(reader, "IdCuenta");
                                objReturn.ResultObject.CapitalLiquidar = Utils.GetDecimal(reader, "LiquidaCapital");
                                objReturn.ResultObject.InteresLiquidar = Utils.GetDecimal(reader, "LiquidaInteres");
                                objReturn.ResultObject.IGVLiquidar = Utils.GetDecimal(reader, "LiquidaIGV");
                                objReturn.ResultObject.SeguroLiquidar = Utils.GetDecimal(reader, "LiquidaSeguro");
                                objReturn.ResultObject.GATLiquidar = Utils.GetDecimal(reader, "LiquidaGAT");
                                objReturn.ResultObject.TotalLiquidar = Utils.GetDecimal(reader, "LiquidaTotal");
                                objReturn.ResultObject.ComentariosSistema = Utils.GetString(reader, "ComentariosSistema");
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        #endregion
    }
}