﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;

namespace SOPF.Core.DataAccess.Cobranza
{
    public class BucketDAL
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public BucketDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion
        
        public List<Bucket> Listar()
        {
            List<Bucket> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.BucketListCON"))
                {                                        
                    //Ejecucion de la Consulta                    
                    objReturn = ToList(db.ExecuteReader(com));                    

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<Bucket> ListarLayout()
        {
            List<Bucket> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.BucketLayoutListCON"))
                {
                    //Ejecucion de la Consulta                    
                    objReturn = ToList(db.ExecuteReader(com));

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        #region "Metodos Privados"

        private List<Bucket> ToList(IDataReader reader)
        {
            List<Bucket> objReturn = new List<Bucket>();

            if (reader != null)
            {
                Bucket objC = null;
                objReturn = new List<Bucket>();

                while (reader.Read())
                {
                    objC = new Bucket();

                    //Lectura de los datos del ResultSet
                    objC.IdBucket = Utils.GetInt(reader, "IdBucket");
                    objC.Descripcion = Utils.GetString(reader, "Descripcion");
                    objC.DiasMin = Utils.GetInt(reader, "DiasMin");
                    objC.DiasMax = Utils.GetInt(reader, "DiasMax");                    

                    objReturn.Add(objC);
                }
            }

            reader.Dispose();

            return objReturn;
        }

        #endregion
    }
}
