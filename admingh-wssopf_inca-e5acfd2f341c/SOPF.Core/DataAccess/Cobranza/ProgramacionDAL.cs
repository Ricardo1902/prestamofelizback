﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Xml;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;

namespace SOPF.Core.DataAccess.Cobranza
{
    public class ProgramacionDAL
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public ProgramacionDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public Resultado<GrupoLayout.ProgramacionGrupoLayout> Alta(GrupoLayout.ProgramacionGrupoLayout objEntity)
        {
            Resultado<GrupoLayout.ProgramacionGrupoLayout> objReturn = new Resultado<GrupoLayout.ProgramacionGrupoLayout>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ProgramacionALT"))
                {
                    db.AddInParameter(com, "@IdTipoProgramacion", DbType.Int32, objEntity.TipoProgramacion.IdTipoProgramacion);
                    db.AddInParameter(com, "@IdGrupoLayout", DbType.Int32, objEntity.IdGrupoLayout);
                    db.AddInParameter(com, "@IdUsuarioRegistro", DbType.Int32, objEntity.IdUsuarioRegistro);
                    db.AddInParameter(com, "@Activo", DbType.Boolean, objEntity.Activo);
                    db.AddInParameter(com, "@FechaInicio", DbType.DateTime, objEntity.FechaInicio);
                    db.AddInParameter(com, "@FechaTermino", DbType.DateTime, objEntity.FechaTermino);
                    db.AddInParameter(com, "@HoraInicio", DbType.DateTime, objEntity.HoraInicio);
                    db.AddInParameter(com, "@HoraTermino", DbType.DateTime, objEntity.HoraTermino);
                    db.AddInParameter(com, "@IdIntervalo", DbType.Int32, objEntity.Intervalo.IdIntervalo);
                    db.AddInParameter(com, "@ValorIntervalo", DbType.Decimal, objEntity.ValorIntervalo);
                    db.AddInParameter(com, "@Intentos", DbType.Int32, objEntity.Intentos);
                    db.AddInParameter(com, "@EsGrupoMoroso", DbType.Boolean, objEntity.EsGrupoMoroso);
                    db.AddInParameter(com, "@XmlDiasEjecucion", DbType.Xml, new XmlTextReader(objEntity.getProgramacionDiasEjecucionXML(), XmlNodeType.Document, null));
                    db.AddOutParameter(com, "@IdProgramacion", DbType.Int32, 0);
                    db.ExecuteNonQuery(com);
                    objEntity.IdProgramacion = int.Parse(db.GetParameterValue(com, "@IdProgramacion").ToString());
                    com.Dispose();
                    objReturn.ResultObject = objEntity;
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            return objReturn;
        }

        public Resultado<GrupoLayout.ProgramacionGrupoLayout> Actualizar(GrupoLayout.ProgramacionGrupoLayout objEntity)
        {
            Resultado<GrupoLayout.ProgramacionGrupoLayout> objReturn = new Resultado<GrupoLayout.ProgramacionGrupoLayout>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ProgramacionACT"))
                {
                    db.AddInParameter(com, "@IdProgramacion", DbType.Int32, objEntity.IdProgramacion);
                    db.AddInParameter(com, "@IdTipoProgramacion", DbType.Int32, objEntity.TipoProgramacion.IdTipoProgramacion);
                    db.AddInParameter(com, "@Activo", DbType.Boolean, objEntity.Activo);
                    db.AddInParameter(com, "@FechaInicio", DbType.DateTime, objEntity.FechaInicio);
                    db.AddInParameter(com, "@FechaTermino", DbType.DateTime, objEntity.FechaTermino);
                    db.AddInParameter(com, "@HoraInicio", DbType.DateTime, objEntity.HoraInicio);
                    db.AddInParameter(com, "@HoraTermino", DbType.DateTime, objEntity.HoraTermino);
                    db.AddInParameter(com, "@IdIntervalo", DbType.Int32, objEntity.Intervalo.IdIntervalo);
                    db.AddInParameter(com, "@ValorIntervalo", DbType.Decimal, objEntity.ValorIntervalo);
                    db.AddInParameter(com, "@Intentos", DbType.Int32, objEntity.Intentos);
                    db.AddInParameter(com, "@EsGrupoMoroso", DbType.Boolean, objEntity.EsGrupoMoroso);
                    db.AddInParameter(com, "@XmlDiasEjecucion", DbType.Xml, new XmlTextReader(objEntity.getProgramacionDiasEjecucionXML(), XmlNodeType.Document, null));
                    db.ExecuteNonQuery(com);
                    com.Dispose();
                    objReturn.ResultObject = objEntity;
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            return objReturn;
        }

        public Resultado<List<Programacion>> Filtrar(Programacion objEntity)
        {
            Resultado<List<Programacion>> objReturn = new Resultado<List<Programacion>>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ProgramacionCON"))
                {
                    db.AddInParameter(com, "@IdProgramacion", DbType.String, objEntity ?? (object)DBNull.Value);
                    objReturn.ResultObject = ToList(db.ExecuteReader(com));
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public Resultado<GrupoLayout.ProgramacionGrupoLayout> Obtener(int IdProgramacion)
        {
            Resultado<GrupoLayout.ProgramacionGrupoLayout> objReturn = new Resultado<GrupoLayout.ProgramacionGrupoLayout>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ProgramacionCON"))
                {
                    db.AddInParameter(com, "@IdProgramacion", DbType.Int32, IdProgramacion);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            GrupoLayout.ProgramacionGrupoLayout objC = new GrupoLayout.ProgramacionGrupoLayout();
                            while (reader.Read())
                            {
                                objC.IdProgramacion = Utils.GetInt(reader, "IdProgramacion");
                                objC.TipoProgramacion.IdTipoProgramacion = Utils.GetInt(reader, "IdTipoProgramacion");
                                objC.IdGrupoLayout = Utils.GetInt(reader, "IdGrupoLayout");
                                objC.IdUsuarioRegistro = Utils.GetInt(reader, "IdUsuarioRegistro");
                                objC.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro").GetValueOrDefault();
                                objC.Activo = Utils.GetBool(reader, "Activo");
                                objC.FechaInicio = Utils.GetDateTime(reader, "FechaInicio").GetValueOrDefault();
                                objC.FechaTermino = Utils.GetDateTime(reader, "FechaTermino").GetValueOrDefault();
                                objC.HoraInicio = Utils.GetDateTime(reader, "HoraInicio").GetValueOrDefault();
                                objC.HoraTermino = Utils.GetDateTime(reader, "HoraTermino").GetValueOrDefault();
                                objC.Intervalo.IdIntervalo = Utils.GetInt(reader, "IdIntervalo");
                                objC.ValorIntervalo = Utils.GetDecimal(reader, "ValorIntervalo");
                                objC.Intentos = Utils.GetInt(reader, "Intentos");
                                objC.EsGrupoMoroso = Utils.GetBool(reader, "EsGrupoMoroso");
                            }
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    objC.DiasEjecucion.Add(new GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion() { Dia = Utils.GetInt(reader, "Dia") });
                                }
                            }
                            if (objC.IdProgramacion <= 0)
                            {
                                objReturn.Codigo = 1;
                                objReturn.Mensaje = string.Format("No se encontro la programación con ID[{0}]", IdProgramacion.ToString());
                            }
                            else
                            {
                                objReturn.ResultObject = objC;
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
                throw ex;
            }
            return objReturn;
        }

        #region "Tipo Programacion"

        public List<TipoProgramacion> TipoProgramacion_ObtenerActivos()
        {
            List<TipoProgramacion> objReturn = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_TipoProgramacionCON"))
                {                
                    objReturn = TipoProgramacion_ToList(db.ExecuteReader(com));
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        #endregion

        #region "Intervalo"

        public List<ProgramacionIntervalo> Intervalos_ObtenerActivos()
        {
            List<ProgramacionIntervalo> objReturn = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ProgramacionIntervalosCON"))
                {
                    objReturn = Intervalo_ToList(db.ExecuteReader(com));
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        #endregion

        #region "Metodos Privados"

        private List<Programacion> ToList(IDataReader reader)
        {
            List<Programacion> objReturn = new List<Programacion>();

            if (reader != null)
            {
                Programacion objC = null;
                objReturn = new List<Programacion>();

                while (reader.Read())
                {
                    objC = new Programacion();
                    objC.IdProgramacion = Utils.GetInt(reader, "IdProgramacion");
                    objC.TipoProgramacion.IdTipoProgramacion = Utils.GetInt(reader, "IdTipoProgramacion");
                    objC.GrupoLayout.IdGrupoLayout = Utils.GetInt(reader, "IdGrupoLayout");
                    objC.IdUsuarioRegistro = Utils.GetInt(reader, "IdUsuarioRegistro");
                    objC.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro").Value;
                    objC.Activo = Utils.GetBool(reader, "Activo");
                    objReturn.Add(objC);
                }
            }

            reader.Dispose();

            return objReturn;
        }

        private List<TipoProgramacion> TipoProgramacion_ToList(IDataReader reader)
        {
            List<TipoProgramacion> objReturn = new List<TipoProgramacion>();
            if (reader != null)
            {
                TipoProgramacion objC = null;
                objReturn = new List<TipoProgramacion>();
                while (reader.Read())
                {
                    objC = new TipoProgramacion();
                    objC.IdTipoProgramacion = Utils.GetInt(reader, "IdTipoProgramacion");
                    objC.TipoProgramacionDescripcion = Utils.GetString(reader, "TipoProgramacion");
                    objC.Activo = Utils.GetBool(reader, "Activo");
                    objReturn.Add(objC);
                }
            }
            reader.Dispose();
            return objReturn;
        }

        private List<ProgramacionIntervalo> Intervalo_ToList(IDataReader reader)
        {
            List<ProgramacionIntervalo> objReturn = new List<ProgramacionIntervalo>();
            if (reader != null)
            {
                ProgramacionIntervalo objC = null;
                objReturn = new List<ProgramacionIntervalo>();
                while (reader.Read())
                {
                    objC = new ProgramacionIntervalo();
                    objC.IdIntervalo = Utils.GetInt(reader, "IdIntervalo");
                    objC.Intervalo = Utils.GetString(reader, "Intervalo");
                    objC.Activo = Utils.GetBool(reader, "Activo");
                    objReturn.Add(objC);
                }
            }
            reader.Dispose();
            return objReturn;
        }
        #endregion
    }
}
