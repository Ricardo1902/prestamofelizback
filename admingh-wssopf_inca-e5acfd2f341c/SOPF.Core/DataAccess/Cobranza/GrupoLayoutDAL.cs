﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Xml;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;

namespace SOPF.Core.DataAccess.Cobranza
{
    public class GrupoLayoutDAL
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public GrupoLayoutDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public Resultado<GrupoLayout> Alta(GrupoLayout objEntity)
        {
            Resultado<GrupoLayout> objReturn = new Resultado<GrupoLayout>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_GrupoLayoutALT"))
                {
                    // Parametros
                    db.AddInParameter(com, "@Nombre", DbType.String, objEntity.Nombre);
                    db.AddInParameter(com, "@IdUsuarioRegistro", DbType.Int32, objEntity.IdUsuarioRegistro);
                    db.AddInParameter(com, "@UsarCuentaEmergente", DbType.Boolean, (objEntity.UsarCuentaEmergente != null) ? objEntity.UsarCuentaEmergente : (object)DBNull.Value);
                    db.AddInParameter(com, "@Activo", DbType.Boolean, (objEntity.Activo != null) ? objEntity.Activo : (object)DBNull.Value);
                    db.AddInParameter(com, "@XmlDiasEjecucion", DbType.Xml, new XmlTextReader(objEntity.getDiasEjecucionXML(), XmlNodeType.Document, null));

                    // Parametros de Salida
                    db.AddOutParameter(com, "@IdGrupoLayout", DbType.Int32, 0);

                    // Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    // Recuperar Parametros de Salida
                    objEntity.IdGrupoLayout = int.Parse(db.GetParameterValue(com, "@IdGrupoLayout").ToString());

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.ResultObject = objEntity;
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<GrupoLayout> Actualizar(GrupoLayout objEntity)
        {
            Resultado<GrupoLayout> objReturn = new Resultado<GrupoLayout>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_GrupoLayoutACT"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdGrupoLayout", DbType.Int32, objEntity.IdGrupoLayout);
                    db.AddInParameter(com, "@Nombre", DbType.String, objEntity.Nombre);
                    db.AddInParameter(com, "@UsarCuentaEmergente", DbType.Boolean, (objEntity.UsarCuentaEmergente != null) ? objEntity.UsarCuentaEmergente : (object)DBNull.Value);
                    db.AddInParameter(com, "@Activo", DbType.Boolean, (objEntity.Activo != null) ? objEntity.Activo : (object)DBNull.Value);
                    db.AddInParameter(com, "@XmlDiasEjecucion", DbType.Xml, new XmlTextReader(objEntity.getDiasEjecucionXML(), XmlNodeType.Document, null));

                    // Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.ResultObject = objEntity;
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<List<GrupoLayout>> Filtrar(GrupoLayout objEntity)
        {
            Resultado<List<GrupoLayout>> objReturn = new Resultado<List<GrupoLayout>>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_GrupoLayoutCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdGrupoLayout", DbType.String, objEntity.IdGrupoLayout ?? (object)DBNull.Value);
                    db.AddInParameter(com, "@Nombre", DbType.String, objEntity.Nombre);
                    db.AddInParameter(com, "@Activo", DbType.Boolean, objEntity.Activo ?? (object)DBNull.Value);

                    //Ejecucion de la Consulta                    
                    objReturn.ResultObject = ToList(db.ExecuteReader(com));

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> Procesar(GrupoLayout.PeticionProcesar objEntity)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ProcesarGrupoPRO"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdGrupoLayout", DbType.Int32, objEntity.IdGrupoLayout);
                    db.AddInParameter(com, "@IdUsuario", DbType.String, objEntity.IdUsuarioProcesa);
                    db.AddInParameter(com, "@EsAutomatico", DbType.Boolean, objEntity.EsAutomatico);

                    db.AddOutParameter(com, "@IdLayoutGenerado", DbType.Int32, 0);

                    // Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.ResultObject = true;
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        #region "Plantillas"

        public Resultado<List<GrupoLayout.PlantillaGrupoLayout>> ObtenerPlantillas(int IdGrupoLayout)
        {
            Resultado<List<GrupoLayout.PlantillaGrupoLayout>> objReturn = new Resultado<List<GrupoLayout.PlantillaGrupoLayout>>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_GrupoLayoutPlantillaCON"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdGrupoLayout", DbType.Int32, IdGrupoLayout);

                    // Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        objReturn.ResultObject = new List<GrupoLayout.PlantillaGrupoLayout>();
                        GrupoLayout.PlantillaGrupoLayout p = new GrupoLayout.PlantillaGrupoLayout();
                        while (reader.Read())
                        {
                            p = new GrupoLayout.PlantillaGrupoLayout();

                            p.Plantilla.IdPlantilla = Utils.GetInt(reader, "IdPlantilla");
                            p.Plantilla.Nombre = Utils.GetString(reader, "Nombre");
                            p.DiasVencimiento = Utils.GetInt(reader, "DiasVencimientoExtra");
                            p.TipoLayoutStr = Utils.GetString(reader, "TipoLayout");

                            objReturn.ResultObject.Add(p);
                        }
                    }

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<bool> GuardarPlantilla(int IdGrupoLayout, int IdPlantilla, int DiasVencimiento)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_GrupoLayoutPlantillaALT"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdGrupoLayout", DbType.Int32, IdGrupoLayout);
                    db.AddInParameter(com, "@IdPlantilla", DbType.Int32, IdPlantilla);
                    db.AddInParameter(com, "@DiasVencimientoExtra", DbType.Int32, DiasVencimiento);

                    // Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<bool> EliminarPlantillas(int IdGrupoLayout)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_GrupoLayoutPlantillaELI"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdGrupoLayout", DbType.Int32, IdGrupoLayout);

                    // Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        #endregion

        #region "Dias Ejecucion"

        public Resultado<List<GrupoLayout.ConfiguracionEjecucion>> ObtenerDiasEjecucion(int IdGrupoLayout)
        {
            Resultado<List<GrupoLayout.ConfiguracionEjecucion>> objReturn = new Resultado<List<GrupoLayout.ConfiguracionEjecucion>>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_GrupoLayoutDEjecucionCON"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdGrupoLayout", DbType.Int32, IdGrupoLayout);

                    // Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        objReturn.ResultObject = new List<GrupoLayout.ConfiguracionEjecucion>();
                        while (reader.Read())
                        {
                            objReturn.ResultObject.Add(new GrupoLayout.ConfiguracionEjecucion() { Dia = Utils.GetString(reader, "Dia") });
                        }
                    }

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        #endregion

        #region "Programaciones"

        public Resultado<List<GrupoLayout.ProgramacionGrupoLayout>> ObtenerProgramaciones(int IdGrupoLayout)
        {
            Resultado<List<GrupoLayout.ProgramacionGrupoLayout>> objReturn = new Resultado<List<GrupoLayout.ProgramacionGrupoLayout>>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_GrupoLayoutProgramacionesCON"))
                {
                    db.AddInParameter(com, "@IdGrupoLayout", DbType.Int32, IdGrupoLayout);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        objReturn.ResultObject = new List<GrupoLayout.ProgramacionGrupoLayout>();
                        GrupoLayout.ProgramacionGrupoLayout p = new GrupoLayout.ProgramacionGrupoLayout();
                        Resultado<List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion>> d = new Resultado<List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion>>();
                        while (reader.Read())
                        {
                            p = new GrupoLayout.ProgramacionGrupoLayout();
                            d = new Resultado<List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion>>();

                            p.IdProgramacion = Utils.GetInt(reader, "IdProgramacion");
                            p.TipoProgramacion.IdTipoProgramacion = Utils.GetInt(reader, "IdTipoProgramacion");
                            p.TipoProgramacion.TipoProgramacionDescripcion = Utils.GetString(reader, "TipoProgramacion");
                            p.IdGrupoLayout = Utils.GetInt(reader, "IdGrupoLayout");
                            p.IdUsuarioRegistro = Utils.GetInt(reader, "IdUsuarioCreacion");
                            p.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro").Value;
                            p.Activo = Utils.GetBool(reader, "Activo");
                            p.FechaInicio = Utils.GetDateTime(reader, "FechaInicio").Value;
                            p.FechaTermino = Utils.GetDateTime(reader, "FechaTermino").GetValueOrDefault();
                            p.HoraInicio = Utils.GetDateTime(reader, "HoraInicio").GetValueOrDefault();
                            p.HoraTermino = Utils.GetDateTime(reader, "HoraTermino").GetValueOrDefault();
                            p.Intervalo.IdIntervalo = Utils.GetInt(reader, "IdIntervalo");
                            p.Intervalo.Intervalo = Utils.GetString(reader, "Intervalo");
                            p.ValorIntervalo = Utils.GetDecimal(reader, "ValorIntervalo");
                            p.Intentos = Utils.GetInt(reader, "Intentos");
                            p.EsGrupoMoroso = Utils.GetBool(reader, "EsGrupoMoroso");

                            d = ObtenerProgramacionDiasEjecucion(p.IdProgramacion);

                            if (d.Codigo > 0)
                            {
                                objReturn.Codigo = d.Codigo;
                                objReturn.Mensaje = d.Mensaje;
                            }
                            else
                            {
                                p.DiasEjecucion = d.ResultObject;
                            }
                            objReturn.ResultObject.Add(p);
                        }
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            return objReturn;
        }

        #endregion

        #region "Programaciones Dias Ejecucion"

        public Resultado<List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion>> ObtenerProgramacionDiasEjecucion(int IdProgramacion)
        {
            Resultado<List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion>> objReturn = new Resultado<List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion>>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_GrupoLayoutProgDEjecucionCON"))
                {
                    db.AddInParameter(com, "@IdProgramacion", DbType.Int32, IdProgramacion);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        objReturn.ResultObject = new List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion>();
                        while (reader.Read())
                        {
                            objReturn.ResultObject.Add(new GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion() { Dia = Utils.GetInt(reader, "Dia") });
                        }
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            return objReturn;
        }

        #endregion

        #region "Metodos Privados"

        private List<GrupoLayout> ToList(IDataReader reader)
        {
            List<GrupoLayout> objReturn = new List<GrupoLayout>();

            if (reader != null)
            {
                GrupoLayout objC = null;
                objReturn = new List<GrupoLayout>();

                while (reader.Read())
                {
                    objC = new GrupoLayout();

                    //Lectura de los datos del ResultSet
                    objC.IdGrupoLayout = Utils.GetInt(reader, "IdGrupoLayout");
                    objC.Nombre = Utils.GetString(reader, "Nombre");
                    objC.IdUsuarioRegistro = Utils.GetInt(reader, "IdUsuarioRegistro");
                    objC.FechaRegistro.Value = Utils.GetDateTime(reader, "FechaRegistro");
                    objC.Activo = Utils.GetBool(reader, "Activo");
                    objC.UsarCuentaEmergente = Utils.GetBool(reader, "UsarCuentaEmergente");

                    objReturn.Add(objC);
                }
            }

            reader.Dispose();

            return objReturn;
        }

        #endregion
    }
}
