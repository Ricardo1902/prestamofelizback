﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;

namespace SOPF.Core.DataAccess.Cobranza
{
    public class VisanetDAL
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public VisanetDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion
        
        public List<VisanetValidarTarjeta> ObtenerPendientesValidar()
        {
            List<VisanetValidarTarjeta> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_VisanetPendientesValidarCON"))
                {                                        
                    //Ejecucion de la Consulta                    
                    objReturn = ToList(db.ExecuteReader(com));                    

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        #region "Metodos Privados"

        private List<VisanetValidarTarjeta> ToList(IDataReader reader)
        {
            List<VisanetValidarTarjeta> objReturn = new List<VisanetValidarTarjeta>();

            if (reader != null)
            {
                VisanetValidarTarjeta objC = null;
                objReturn = new List<VisanetValidarTarjeta>();

                while (reader.Read())
                {
                    objC = new VisanetValidarTarjeta();

                    //Lectura de los datos del ResultSet
                    objC.IdSolicitud = Utils.GetInt(reader, "IdSolicitud");
                    objC.NombreCliente = Utils.GetString(reader, "NombreCliente");
                    objC.ApellidoPaterno = Utils.GetString(reader, "ApellidoPaterno");
                    objC.ApellidoMaterno = Utils.GetString(reader, "ApellidoMaterno");
                    objC.Correo = Utils.GetString(reader, "Correo");
                    objC.NumeroTarjeta = Utils.GetString(reader, "NumeroTarjetaVisa");
                    objC.MesVencimientoTarjeta = Utils.GetString(reader, "MesExpiraTarjetaVisa");
                    objC.AnioVencimientoTarjeta = Utils.GetString(reader, "AnioExpiraTarjetaVisa");
                    objC.FechaValidado.Value = Utils.GetDateTime(reader, "FechaValidado");
                    objC.Validado = Utils.GetBool(reader, "Validado");

                    objReturn.Add(objC);
                }
            }

            reader.Dispose();

            return objReturn;
        }

        #endregion
    }
}
