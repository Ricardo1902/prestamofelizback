﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Xml;
using System.Data.SqlClient;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.Entities.Cobranza;

namespace SOPF.Core.DataAccess.Cobranza
{
    public class PlantillaDAL
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public PlantillaDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public Resultado<Plantilla> Alta(Plantilla objEntity)
        {
            Resultado<Plantilla> objReturn = new Resultado<Plantilla>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PlantillaALT"))
                {
                    // Parametros
                    db.AddInParameter(com, "@Nombre", DbType.String, objEntity.Nombre);
                    db.AddInParameter(com, "@IdTipoLayout", DbType.Int32, objEntity.TipoLayout.IdTipoLayoutCobro);
                    db.AddInParameter(com, "@IdUsuarioRegistro", DbType.Int32, objEntity.IdUsuarioRegistro);
                    db.AddInParameter(com, "@UsarCuentaEmergente", DbType.Boolean, (objEntity.UsarCuentaEmergente != null) ? objEntity.UsarCuentaEmergente : (object)DBNull.Value);
                    db.AddInParameter(com, "@OtrosBancos", DbType.Boolean, (objEntity.OtrosBancos != null) ? objEntity.OtrosBancos : (object)DBNull.Value);
                    db.AddInParameter(com, "@Activo", DbType.Boolean, (objEntity.Activo != null) ? objEntity.Activo : (object)DBNull.Value);

                    // Parametros de Salida
                    db.AddOutParameter(com, "@IdPlantilla", DbType.Int32, 0);

                    // Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    // Recuperar Parametros de Salida
                    objEntity.IdPlantilla = int.Parse(db.GetParameterValue(com, "@IdPlantilla").ToString());                    

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.ResultObject = objEntity;
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<Plantilla> Actualizar(Plantilla objEntity)
        {
            Resultado<Plantilla> objReturn = new Resultado<Plantilla>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PlantillaACT"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdPlantilla", DbType.Int32, objEntity.IdPlantilla);
                    db.AddInParameter(com, "@Nombre", DbType.String, objEntity.Nombre);
                    db.AddInParameter(com, "@IdTipoLayout", DbType.Int32, objEntity.TipoLayout.IdTipoLayoutCobro);                    
                    db.AddInParameter(com, "@UsarCuentaEmergente", DbType.Boolean, (objEntity.UsarCuentaEmergente != null) ? objEntity.UsarCuentaEmergente : (object)DBNull.Value);
                    db.AddInParameter(com, "@OtrosBancos", DbType.Boolean, (objEntity.OtrosBancos != null) ? objEntity.OtrosBancos : (object)DBNull.Value);
                    db.AddInParameter(com, "@Activo", DbType.Boolean, (objEntity.Activo != null) ? objEntity.Activo : (object)DBNull.Value);                    

                    // Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);                    

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.ResultObject = objEntity;
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<List<Plantilla>> Filtrar(Plantilla objEntity)
        {
            Resultado<List<Plantilla>> objReturn = new Resultado<List<Plantilla>>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PlantillaCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdPlantilla", DbType.String, objEntity.IdPlantilla ?? (object)DBNull.Value);
                    db.AddInParameter(com, "@IdTipoLayout", DbType.String, objEntity.TipoLayout.IdTipoLayoutCobro ?? (object)DBNull.Value);
                    db.AddInParameter(com, "@Nombre", DbType.String, objEntity.Nombre);
                    db.AddInParameter(com, "@Activo", DbType.Boolean, objEntity.Activo ?? (object)DBNull.Value);

                    //Ejecucion de la Consulta                    
                    objReturn.ResultObject = ToList(db.ExecuteReader(com));                    

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<List<int>> ObtenerDiasEjecucion(int IdPlantilla)
        {
            Resultado<List<int>> objReturn = new Resultado<List<int>>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ObtenerPlantillaDiasXCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdPlantilla", DbType.String, IdPlantilla);

                    // Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        objReturn.ResultObject = new List<int>();
                        while (reader.Read())
                        {
                            objReturn.ResultObject.Add(Utils.GetInt(reader, "DiaPago"));
                        }
                    }                  

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        #region "Convenios - Buckets"

        public Resultado<List<Plantilla.EmpresaConfiguracion>> ObtenerEmpresas(int IdPlantilla)
        {
            Resultado<List<Plantilla.EmpresaConfiguracion>> objReturn = new Resultado<List<Plantilla.EmpresaConfiguracion>>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PlantillaEmpresaCON"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdPlantilla", DbType.Int32, IdPlantilla);

                    // Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        objReturn.ResultObject = new List<Plantilla.EmpresaConfiguracion>();
                        Plantilla.EmpresaConfiguracion cc = new Plantilla.EmpresaConfiguracion();
                        while (reader.Read())
                        {
                            cc = new Plantilla.EmpresaConfiguracion();

                            cc.Empresa.IdEmpresa = Utils.GetInt(reader, "IdEmpresa");
                            cc.Empresa.Empresa = Utils.GetString(reader, "Empresa");
                            cc.Empresa.RUC = Utils.GetString(reader, "RUC");

                            objReturn.ResultObject.Add(cc);
                        }
                    }

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<List<Plantilla.EmpresaConfiguracion.BucketConfiguracion>> ObtenerBucketEmpresa(int IdPlantilla, int IdEmpresa)
        {
            Resultado<List<Plantilla.EmpresaConfiguracion.BucketConfiguracion>> objReturn = new Resultado<List<Plantilla.EmpresaConfiguracion.BucketConfiguracion>>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PlantillaBucketCON"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdPlantilla", DbType.Int32, IdPlantilla);
                    db.AddInParameter(com, "@IdEmpresa", DbType.Int32, IdEmpresa);

                    // Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        objReturn.ResultObject = new List<Plantilla.EmpresaConfiguracion.BucketConfiguracion>();
                        Plantilla.EmpresaConfiguracion.BucketConfiguracion bc = new Plantilla.EmpresaConfiguracion.BucketConfiguracion();
                        while (reader.Read())
                        {
                            bc = new Plantilla.EmpresaConfiguracion.BucketConfiguracion();

                            bc.Bucket.IdBucket = Utils.GetInt(reader, "IdBucket");
                            bc.Bucket.Descripcion = Utils.GetString(reader, "Bucket");
                            bc.NoErogacionesMax = Utils.GetDecimal(reader, "NoErogacionesMax");
                            bc.MontoIndivisible = Utils.GetDecimal(reader, "MontoIndivisible");
                            bc.MontoIndivisible = (bc.MontoIndivisible != 0) ? bc.MontoIndivisible : null;

                            objReturn.ResultObject.Add(bc);
                        }
                    }

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<bool> GuardarEmpresaBuckets(int IdPlantilla, int IdEmpresa, Plantilla.EmpresaConfiguracion.BucketConfiguracion bucketEntity)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PlantillaBucketALT"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdPlantilla", DbType.Int32, IdPlantilla);
                    db.AddInParameter(com, "@IdEmpresa", DbType.Int32, IdEmpresa);
                    db.AddInParameter(com, "@IdBucket", DbType.Int32, bucketEntity.Bucket.IdBucket);
                    db.AddInParameter(com, "@MontoIndivisible", DbType.Decimal, (bucketEntity.MontoIndivisible != null) ? bucketEntity.MontoIndivisible : (object)DBNull.Value);
                    db.AddInParameter(com, "@NoErogacionesMax", DbType.Decimal, (bucketEntity.NoErogacionesMax != null) ? bucketEntity.NoErogacionesMax : (object)DBNull.Value);
                    db.AddInParameter(com, "@XmlPartidas", DbType.Xml, new XmlTextReader(bucketEntity.getPartidasXML(), XmlNodeType.Document, null));                    

                    // Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);                    

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();                    
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<bool> EliminarEmpresaBuckets(int IdPlantilla)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PlantillaBucketELI"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdPlantilla", DbType.Int32, IdPlantilla);

                    // Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion>> ObtenerPartidasBucket(int IdPlantilla, int IdEmpresa, int IdBucket)
        {
            Resultado<List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion>> objReturn = new Resultado<List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion>>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PlantillaPartidaCON"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdPlantilla", DbType.Int32, IdPlantilla);
                    db.AddInParameter(com, "@IdEmpresa", DbType.Int32, IdEmpresa);
                    db.AddInParameter(com, "@IdBucket", DbType.Int32, IdBucket);

                    // Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        objReturn.ResultObject = new List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion>();
                        Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion pc = new Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion();
                        while (reader.Read())
                        {
                            pc = new Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion();

                            pc.NoPartida = Utils.GetInt(reader, "NoPartida");
                            pc.PorcentajePartida = Utils.GetInt(reader, "PorcentajePartida");

                            objReturn.ResultObject.Add(pc);
                        }
                    }

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        #endregion

        #region "Situacion Laboral"

        public Resultado<List<TBCATSituacionLaboral_PE>> ObtenerSituacionLaboral(int IdPlantilla)
        {
            Resultado<List<TBCATSituacionLaboral_PE>> objReturn = new Resultado<List<TBCATSituacionLaboral_PE>>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PlantillaSituacionLaboralCON"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdPlantilla", DbType.Int32, IdPlantilla);

                    // Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        objReturn.ResultObject = new List<TBCATSituacionLaboral_PE>();
                        while (reader.Read())
                        {
                            objReturn.ResultObject.Add(new TBCATSituacionLaboral_PE() { IdSituacion = Utils.GetInt(reader, "IdSituacionLaboral") });
                        }
                    }

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<bool> EliminarSituacionLaboral(int IdPlantilla)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PlantillaSituacionLaboralELI"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdPlantilla", DbType.Int32, IdPlantilla);                    

                    // Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        public Resultado<bool> GuardarSituacionLaboral(int IdPlantilla, int IdSituacionLaboral)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PlantillaSituacionLaboralALT"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdPlantilla", DbType.Int32, IdPlantilla);
                    db.AddInParameter(com, "@IdSituacionLaboral", DbType.Int32, IdSituacionLaboral);                    

                    // Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    // Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }
                           
        #endregion

        #region "Metodos Privados"

        private List<Plantilla> ToList(IDataReader reader)
        {
            List<Plantilla> objReturn = new List<Plantilla>();

            if (reader != null)
            {
                Plantilla objC = null;
                objReturn = new List<Plantilla>();

                while (reader.Read())
                {
                    objC = new Plantilla();

                    //Lectura de los datos del ResultSet
                    objC.IdPlantilla = Utils.GetInt(reader, "IdPlantilla");
                    objC.Nombre = Utils.GetString(reader, "Nombre");
                    objC.IdUsuarioRegistro = Utils.GetInt(reader, "IdUsuarioRegistro");
                    objC.FechaRegistro.Value = Utils.GetDateTime(reader, "FechaRegistro");
                    objC.OtrosBancos = Utils.GetBool(reader, "OtrosBancos");
                    objC.UsarCuentaEmergente = Utils.GetBool(reader, "UsarCuentaEmergente");
                    objC.Activo = Utils.GetBool(reader, "Activo");

                    objC.TipoLayout.IdTipoLayoutCobro = Utils.GetInt(reader, "IdTipoLayoutCobro");
                    objC.TipoLayout.TipoLayout = Utils.GetString(reader, "TipoLayout");

                    objReturn.Add(objC);
                }
            }

            reader.Dispose();

            return objReturn;
        }

        #endregion
    }
}
