﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Entities;
using SOPF.Core.Entities.CobranzaAdministrativa;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace SOPF.Core.DataAccess.Cobranza
{
    public class DevolucionesDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public DevolucionesDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region Métodos Misceláneos
        private Resultado<bool> EliminarDevolucion(string accion, Devolucion devolucion, int IdUsuario = 0)
        {
            Resultado<bool> objReturn = new Resultado<bool>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_DevolucionesBAJ"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, accion);
                    db.AddInParameter(com, "@IdDevolucion", DbType.Int32, devolucion.idDevolucion);
                    if (IdUsuario > 0)
                        db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.ExecuteNonQuery(com);
                    com.Dispose();
                    objReturn.Codigo = 0;
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public Resultado<bool> EliminarDevolucion(Devolucion Entity, int IdUsuario)
        {
            return EliminarDevolucion("ELIMINAR_DEVOLUCION_POR_ID", Entity, IdUsuario);
        }
        #endregion

        #region Métodos Privados
        #endregion
    }
}
