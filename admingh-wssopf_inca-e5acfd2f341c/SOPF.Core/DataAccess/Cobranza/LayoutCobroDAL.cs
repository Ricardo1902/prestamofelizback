﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;

namespace SOPF.Core.DataAccess.Cobranza
{
    public class LayoutCobroDAL
    {
        #region Objetos Base de Datos

        Database db;
        
        #endregion

        #region Contructor

        public LayoutCobroDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        
        #endregion

        #region "TipoLayoutCobro"

        public List<TipoLayoutCobro> TipoLayoutCobro_ListarTodos()
        {
            return TipoLayoutCobro_FiltrarByActivo(null);
        }

        public List<TipoLayoutCobro> TipoLayoutCobro_ListarActivos()
        {
            return TipoLayoutCobro_FiltrarByActivo(true);
        }

        public List<TipoLayoutCobro> TipoLayoutCobro_ListarInactivos()
        {
            return TipoLayoutCobro_FiltrarByActivo(false);
        }

        #endregion        

        #region "Generar Layouts"

        public List<LayoutCobro.LayoutGenerado> LayoutGenerado_Filtrar(LayoutCobro.LayoutGenerado eFiltrar)
        {
            List<LayoutCobro.LayoutGenerado> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Stored Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_LayoutGeneradoCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdLayoutGenerado", DbType.Int32, (eFiltrar.IdLayoutGenerado) ?? (object)DBNull.Value);
                    db.AddInParameter(com, "@FechaRegistroIni", DbType.DateTime, eFiltrar.FechaRegistro.ValueIni);
                    db.AddInParameter(com, "@FechaRegistroFin", DbType.DateTime, eFiltrar.FechaRegistro.ValueEnd);                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, eFiltrar.IdUsuarioRegistro);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            LayoutCobro.LayoutGenerado e = null;
                            resultado = new List<LayoutCobro.LayoutGenerado>();
                            
                            while (reader.Read())
                            {
                                e = new LayoutCobro.LayoutGenerado();

                                //Lectura de los datos del ResultSet
                                e.IdLayoutGenerado = Utils.GetInt(reader, "IdLayoutGenerado");
                                e.GrupoLayout.IdGrupoLayout = Utils.GetInt(reader, "IdGrupoLayout");
                                e.GrupoLayout.Nombre = Utils.GetString(reader, "Nombre");
                                e.Estatus.IdEstatus = Utils.GetInt(reader, "IdEstatus");
                                e.Estatus.EstatusDesc = Utils.GetString(reader, "EstatusDescripcion");
                                e.FechaRegistro.Value = Utils.GetDateTime(reader, "FechaRegistro");
                                e.FechaRegistro.Value = Utils.GetDateTime(reader, "FechaTermino");
                                e.ResultadoMsg = Utils.GetString(reader, "ResultadoMsg");
                                e.IdUsuarioRegistro = Utils.GetInt(reader, "IdUsuarioRegistro");
                                e.EsAutomatico = Utils.GetBool(reader, "EsAutomatico");

                                resultado.Add(e);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public LayoutCobro.LayoutGenerado LayoutGenerado_ObtenerDetalle(int Id)
        {
            LayoutCobro.LayoutGenerado resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Stored Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_LayoutGeneradoDetalleCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdLayoutGenerado", DbType.Int32, Id);                   

                    //Ejecucion de la Consulta
                    using (DataSet ds = db.ExecuteDataSet(com))
                    {
                        DataTable dtMain = ds.Tables[0];
                        DataTable dtDetail = ds.Tables[1];

                        if (dtMain.Rows.Count > 0)
                        {                            
                            resultado = new LayoutCobro.LayoutGenerado();

                            foreach (DataRow dr in dtMain.Rows)
                            {
                                //Lectura de los datos del ResultSet
                                resultado.IdLayoutGenerado = Utils.GetInt(dr, "IdLayoutGenerado");
                                resultado.GrupoLayout.IdGrupoLayout = Utils.GetInt(dr, "IdGrupoLayout");
                                resultado.GrupoLayout.Nombre = Utils.GetString(dr, "Nombre");
                                resultado.Estatus.IdEstatus = Utils.GetInt(dr, "IdEstatus");
                                resultado.Estatus.EstatusDesc = Utils.GetString(dr, "EstatusDescripcion");
                                resultado.FechaRegistro.Value = Utils.GetDateTime(dr, "FechaRegistro");
                                resultado.FechaRegistro.Value = Utils.GetDateTime(dr, "FechaTermino");
                                resultado.ResultadoMsg = Utils.GetString(dr, "ResultadoMsg");
                                resultado.IdUsuarioRegistro = Utils.GetInt(dr, "IdUsuarioRegistro");
                                resultado.EsAutomatico = Utils.GetBool(dr, "EsAutomatico");
                            }
                        }

                        if (dtDetail.Rows.Count > 0)
                        {
                            LayoutCobro.LayoutGenerado.Plantilla p;

                            foreach (DataRow dr in dtDetail.Rows)
                            {
                                p = new LayoutCobro.LayoutGenerado.Plantilla();

                                //Lectura de los datos del ResultSet                                
                                p.IdPlantillaEnvioCobro = Utils.GetInt(dr, "IdPlantillaEnvioCobro");
                                p.TipoLayout.IdTipoLayoutCobro = Utils.GetInt(dr, "IdTipoLayoutCobro");
                                p.TipoLayout.TipoLayout = Utils.GetString(dr, "TipoLayout");                                
                                p.NombrePlantilla = Utils.GetString(dr, "NombrePlantilla");
                                p.OperacionCorrecta = Utils.GetBool(dr, "OperacionCorrecta");
                                p.MensajeOperacion = Utils.GetString(dr, "MensajeOperacion");                                
                                p.TotalRegistros = Utils.GetInt(dr, "TotalRegistros");
                                p.MontoTotal = Utils.GetDecimal(dr, "MontoTotal");
                                p.ArchivoGenerado = Utils.GetBool(dr, "ArchivoGenerado");
                                p.VecesGenerado = Utils.GetInt(dr, "VecesGenerado");
                                
                                resultado.Plantillas.Add(p);
                            }
                        }

                        ds.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<LayoutCobro.LayoutSumatoria> LayoutCobro_Suma(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago)
        {
            List<LayoutCobro.LayoutSumatoria> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ResLayoutDomiciliadoCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, fchCreditoDesde);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, fchCreditoHasta);
                    db.AddInParameter(com, "@Canal", DbType.Int32, ddlCanalPago);
                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            LayoutCobro.LayoutSumatoria suma = null;
                            resultado = new List<LayoutCobro.LayoutSumatoria>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                suma = new LayoutCobro.LayoutSumatoria();
                                //Lectura de los datos del ResultSet
                                suma.NumSolicitudes = Utils.GetInt(reader, "NumSolicitudes");
                                suma.NumeroRecibos = Utils.GetInt(reader, "NumeroRecibos");
                                suma.CantidadCobrar = Utils.GetDecimal(reader, "CantidadCobrar");

                                resultado.Add(suma);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<LayoutCobro.LayoutNetCash> ObtenerDatos_LayoutCobro_NetCash(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago)
        {
            List<LayoutCobro.LayoutNetCash> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ContLayoutDomiciliadoCON"))
                {
                    //Parametros
                    // db.AddInParameter(com, "@Accion", DbType.String, "LAYOUT");
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, fchCreditoDesde);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, fchCreditoHasta);
                    db.AddInParameter(com, "@Canal", DbType.Int32, ddlCanalPago);
                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            LayoutCobro.LayoutNetCash Datos = null;
                            resultado = new List<LayoutCobro.LayoutNetCash>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                Datos = new LayoutCobro.LayoutNetCash();
                                //Lectura de los datos del ResultSet
                                Datos.SOLICITUD = Utils.GetInt(reader, "IdSolicitud");
                                Datos.TIPODOC = Utils.GetString(reader, "TipoDocumento");
                                Datos.NUMDOC = Utils.GetString(reader, "NumeroDocumento");
                                Datos.NOMBRECLIENTE = Utils.GetString(reader, "NombreCliente");
                                Datos.IMPORTE = Utils.GetDecimal(reader, "Importe");
                                Datos.CUENTA = Utils.GetString(reader, "Cuenta");


                                resultado.Add(Datos);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<LayoutCobro.LayoutVisaNet> ObtenerDatos_LayoutCobro_VisaNet(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago)
        {
            List<LayoutCobro.LayoutVisaNet> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ContLayoutDomiciliadoCON"))
                {
                    //Parametros
                    //db.AddInParameter(com, "@Accion", DbType.String, "LAYOUT");
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, fchCreditoDesde);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, fchCreditoHasta);
                    db.AddInParameter(com, "@Canal", DbType.Int32, ddlCanalPago);
                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            LayoutCobro.LayoutVisaNet Datos = null;
                            resultado = new List<LayoutCobro.LayoutVisaNet>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                Datos = new LayoutCobro.LayoutVisaNet();
                                //Lectura de los datos del ResultSet
                                Datos.Solicitud = Utils.GetInt(reader, "IdSolicitud");
                                Datos.Monto = Utils.GetDecimal(reader, "Monto");
                                Datos.NoTarjeta = Utils.GetString(reader, "NumeroTarjetaVisa");
                                Datos.MesExpiracion = Utils.GetInt(reader, "MesExpiraTarjetaVisa");
                                Datos.AnioExpiracion = Utils.GetInt(reader, "AnioExpiraTarjetaVisa");

                                resultado.Add(Datos);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<LayoutCobro.LayoutInterbank> ObtenerDatos_LayoutCobro_Interbank(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta, int ddlCanalPago)
        {
            List<LayoutCobro.LayoutInterbank> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ContLayoutDomiciliadoCON"))
                {
                    //Parametros
                    //db.AddInParameter(com, "@Accion", DbType.String, "LAYOUT");
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, fchCreditoDesde);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, fchCreditoHasta);
                    db.AddInParameter(com, "@Canal", DbType.Int32, ddlCanalPago);
                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            LayoutCobro.LayoutInterbank Datos = null;
                            resultado = new List<LayoutCobro.LayoutInterbank>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                Datos = new LayoutCobro.LayoutInterbank();
                                //Lectura de los datos del ResultSet
                                Datos.RowInfo = Utils.GetString(reader, "RowInfo");

                                resultado.Add(Datos);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        #endregion

        #region "Metodos Privados"

        private List<TipoLayoutCobro> TipoLayoutCobro_FiltrarByActivo(Boolean? Activo)
        {
            List<TipoLayoutCobro> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_TipoLayoutCobroCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@Activo", DbType.Boolean, (Activo == null) ? (object)DBNull.Value : Activo);

                    //Ejecucion de la Consulta                    
                    objReturn = TipoLayoutCobro_ToList(db.ExecuteReader(com));

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        private List<TipoLayoutCobro> TipoLayoutCobro_ToList(IDataReader reader)
        {
            List<TipoLayoutCobro> objReturn = new List<TipoLayoutCobro>();

            if (reader != null)
            {
                TipoLayoutCobro objC = null;
                objReturn = new List<TipoLayoutCobro>();

                while (reader.Read())
                {
                    objC = new TipoLayoutCobro();

                    //Lectura de los datos del ResultSet
                    objC.IdTipoLayoutCobro = Utils.GetInt(reader, "IdTipoLayoutCobro");
                    objC.TipoLayout = Utils.GetString(reader, "TipoLayout");
                    objC.Activo = Utils.GetBool(reader, "Activo");

                    objReturn.Add(objC);
                }
            }

            reader.Dispose();

            return objReturn;
        }

        #endregion
    }
}
