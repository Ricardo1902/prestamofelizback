﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;

namespace SOPF.Core.DataAccess.Cobranza
{
    public class PagosDAL 
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public PagosDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public ResultadoDetallePagoMasivo ConsultarPagosMasivos(DetallePagoMasivoCondiciones condiciones)
        {
            List<DetallePagoMasivo> pagos = new List<DetallePagoMasivo>();
            ResultadoDetallePagoMasivo resultado = new ResultadoDetallePagoMasivo();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("cobranza.SP_FiltradoPagosCON"))
                {                    
                    if (condiciones.idSolicitud > 0)
                    {
                        db.AddInParameter(com, "@idSolicitud", DbType.Int32, condiciones.idSolicitud);
                    }
                    if (condiciones.idDomiciliacion > 0)
                    {
                        db.AddInParameter(com, "@idDomiciliacion", DbType.Int32, condiciones.idDomiciliacion);
                    }
                    if (condiciones.idPagoDirecto > 0)
                    {
                        db.AddInParameter(com, "@idPagoDirecto", DbType.Int32, condiciones.idPagoDirecto);
                    }
                    if (condiciones.idCanalPago > 0)
                    {
                        db.AddInParameter(com, "@idCanalPago", DbType.Int32, condiciones.idCanalPago);
                    }
                    if (condiciones.fechaPagoDesde != DateTime.MinValue)
                    {
                        db.AddInParameter(com, "@fechaPagoDesde", DbType.DateTime, condiciones.fechaPagoDesde);
                    }
                    if (condiciones.fechaPagoHasta != DateTime.MinValue)
                    {
                        db.AddInParameter(com, "@fechaPagoHasta", DbType.DateTime, condiciones.fechaPagoHasta);
                    }
                    if (condiciones.fechaRegistroDesde != DateTime.MinValue)
                    {
                        db.AddInParameter(com, "@fechaRegistroDesde", DbType.DateTime, condiciones.fechaRegistroDesde);
                    }
                    if (condiciones.fechaRegistroHasta != DateTime.MinValue)
                    {
                        db.AddInParameter(com, "@fechaRegistroHasta", DbType.DateTime, condiciones.fechaRegistroHasta);
                    }
                    if (condiciones.fechaAplicacionDesde != DateTime.MinValue)
                    {
                        db.AddInParameter(com, "@fechaAplicacionDesde", DbType.DateTime, condiciones.fechaAplicacionDesde);
                    }
                    if (condiciones.fechaAplicacionHasta != DateTime.MinValue)
                    {
                        db.AddInParameter(com, "@fechaAplicacionHasta", DbType.DateTime, condiciones.fechaAplicacionHasta);
                    }
                    if (condiciones.idTipoConvenio > 0)
                    {
                        db.AddInParameter(com, "@idTipoConvenio", DbType.Int32, condiciones.idTipoConvenio);
                    }
                    if (condiciones.idConvenio > 0)
                    {
                        db.AddInParameter(com, "@idConvenio", DbType.Int32, condiciones.idConvenio);
                    }
                    if (condiciones.idProducto > 0)
                    {
                        db.AddInParameter(com, "@idProducto", DbType.Int32, condiciones.idProducto);
                    }

                    DataTable table = new DataTable();
                    table.Columns.Add("value", typeof(int));
                    foreach (int id in condiciones.idEstatus)
                    {
                        table.Rows.Add(id);
                    }
                    SqlParameter temp = new SqlParameter("@idEstatus", SqlDbType.Structured);
                    temp.TypeName = "dbo.intList";
                    temp.Value = table;
                    com.Parameters.Add(temp);

                    db.AddOutParameter(com, "@total", DbType.Int32, 4);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {                                                        
                            while (reader.Read())
                            {
                                DetallePagoMasivo pago = new DetallePagoMasivo();
                                pago.idPago = Utils.GetInt(reader, "idPago");
                                pago.idDomiciliacion = Utils.GetInt(reader, "idDomiciliacion");
                                pago.idPagoDirecto = Utils.GetInt(reader, "idPagoDirecto");
                                pago.idSolicitud = Utils.GetInt(reader, "idSolicitud");
                                pago.montoCobrado = Utils.GetDecimal(reader, "montoCobrado");
                                pago.montoAplicado = Utils.GetDecimal(reader, "montoAplicado");
                                pago.montoPorAplicar = Utils.GetDecimal(reader, "montoPorAplicar");
                                pago.canalPagoClave = Utils.GetString(reader, "canalPagoClave");
                                pago.canalPagoDesc = Utils.GetString(reader, "canalPagoDesc");
                                pago.idEstatus = Utils.GetInt(reader, "idEstatus");
                                pago.estatusClave = Utils.GetString(reader, "estatusClave");
                                pago.estatusDesc = Utils.GetString(reader, "estatusDesc");
                                pago.fechaPago = Utils.GetString(reader, "fechaPago");
                                pago.fechaRegistro = Utils.GetString(reader, "fechaRegistro");
                                pago.fechaAplicacion = Utils.GetString(reader, "fechaAplicacion");
                                pagos.Add(pago);
                            }
                        }

                        reader.Dispose();
                    }

                    resultado.total = int.Parse(db.GetParameterValue(com, "@total").ToString());

                    com.Dispose();
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            resultado.pagos = pagos;

            return resultado;
        }

        public List<ProcesoPagosAplicacion> ConsultarProcesosPagosAplicacion(ProcesoPagosAplicacionCondiciones condiciones)
        {
            List<ProcesoPagosAplicacion> procesos = new List<ProcesoPagosAplicacion>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("cobranza.SP_ProcesoPagosAplicacionCON"))
                {
                    if (condiciones.idProceso > 0)
                    {
                        db.AddInParameter(com, "@idProceso", DbType.Int32, condiciones.idProceso);
                    }
                    if (condiciones.idUsuario > 0)
                    {
                        db.AddInParameter(com, "@idUsuario", DbType.Int32, condiciones.idUsuario);
                    }
                    if (condiciones.idEstatus > 0)
                    {
                        db.AddInParameter(com, "@idEstatus", DbType.Int32, condiciones.idEstatus);
                    }
                    if (condiciones.fechaCreacionDesde != null)
                    {
                        db.AddInParameter(com, "@fechaCreacionDesde", DbType.Int32, condiciones.fechaCreacionDesde);
                    }
                    if (condiciones.fechaCreacionHasta != null)
                    {
                        db.AddInParameter(com, "@fechaCreacionHasta", DbType.Int32, condiciones.fechaCreacionHasta);
                    }

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                ProcesoPagosAplicacion proceso = new ProcesoPagosAplicacion();
                                proceso.idProceso = Utils.GetInt(reader, "idProceso");
                                proceso.idUsuario = Utils.GetInt(reader, "idUsuario");
                                proceso.usuarioNombre = Utils.GetString(reader, "usuarioNombre");
                                proceso.idEstatus = Utils.GetInt(reader, "idEstatus");
                                proceso.estatus = Utils.GetString(reader, "estatus");
                                proceso.totalPagos = Utils.GetInt(reader, "totalPagos");
                                proceso.totalProcesados = Utils.GetInt(reader, "totalProcesados");
                                proceso.fechaCreacion = Utils.GetString(reader, "fechaCreacion");
                                proceso.aplicar = Utils.GetBool(reader, "aplicar");
                                proceso.tipo = Utils.GetString(reader, "tipo");
                                proceso.reversaIdProceso = Utils.GetInt(reader, "reversaIdProceso");
                                procesos.Add(proceso);
                            }
                        }

                        reader.Dispose();
                    }

                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return procesos;
        }

        public List<ProcesoPagosAplicacionDetalle> ConsultarProcesoPagosAplicacionDetalle(ProcesoPagosAplicacionDetalleCondiciones condiciones)
        {
            List<ProcesoPagosAplicacionDetalle> pagos = new List<ProcesoPagosAplicacionDetalle>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("cobranza.SP_ProcesoPagosAplicacionDetalleCON"))
                {
                    if (condiciones.idDetalle > 0)
                    {
                        db.AddInParameter(com, "@idDetalle", DbType.Int32, condiciones.idDetalle);
                    }
                    if (condiciones.idProceso > 0)
                    {
                        db.AddInParameter(com, "@idProceso", DbType.Int32, condiciones.idProceso);
                    }
                    if (condiciones.idPago > 0)
                    {
                        db.AddInParameter(com, "@idPago", DbType.Int32, condiciones.idPago);
                    }
                    if (condiciones.idEstatus > 0)
                    {
                        db.AddInParameter(com, "@idEstatus", DbType.Int32, condiciones.idEstatus);
                    }
                    if (condiciones.fechaAplicacionDesde != null)
                    {
                        db.AddInParameter(com, "@fechaAplicacionDesde", DbType.Int32, condiciones.fechaAplicacionDesde);
                    }
                    if (condiciones.fechaAplicacionHasta != null)
                    {
                        db.AddInParameter(com, "@fechaAplicacionHasta", DbType.Int32, condiciones.fechaAplicacionHasta);
                    }

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                ProcesoPagosAplicacionDetalle pago = new ProcesoPagosAplicacionDetalle();
                                pago.idDetalle = Utils.GetInt(reader, "idDetalle");
                                pago.idProceso = Utils.GetInt(reader, "idProceso");
                                pago.idPago = Utils.GetInt(reader, "idPago");
                                pago.idEstatus = Utils.GetInt(reader, "idEstatus");
                                pago.mensajeEstatus = Utils.GetString(reader, "mensajeEstatus");                                
                                pago.fechaAplicacion = Utils.GetString(reader, "fechaAplicacion");
                                pago.idSolicitud = Utils.GetInt(reader, "idSolicitud");
                                pago.montoAplicado = Utils.GetDecimal(reader, "montoAplicado");
                                pago.montoPorAplicar = Utils.GetDecimal(reader, "montoPorAplicar");
                                pagos.Add(pago);
                            }
                        }

                        reader.Dispose();
                    }

                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return pagos;
        }

        public int ProcesoPagosAplicacionAlta(ProcesoPagosAplicacion proceso)
        {
            int idProceso = 0;
            List<DetallePagoMasivo> pagos = new List<DetallePagoMasivo>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("cobranza.SP_ProcesoPagosAplicacionAL"))
                {
                    DataTable table = new DataTable();
                    table.Columns.Add("value", typeof(int));
                    foreach (int id in proceso.idPagos)
                    {
                        table.Rows.Add(id);
                    }

                    db.AddInParameter(com, "@idUsuario", DbType.String, proceso.idUsuario);
                    db.AddInParameter(com, "@aplicar", DbType.Boolean, proceso.aplicar);
                    if (proceso.reversaIdProceso > 0)
                    {
                        db.AddInParameter(com, "@reversaIdProceso", DbType.Int32, proceso.reversaIdProceso);
                    }                    

                    SqlParameter temp = new SqlParameter("@idPagos", SqlDbType.Structured);
                    temp.TypeName = "dbo.intList";
                    temp.Value = table;
                    com.Parameters.Add(temp);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                idProceso = Utils.GetInt(reader, "idProceso");
                            }
                        }

                        reader.Dispose();
                    }

                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return idProceso;
        }

        public Resultado<bool> Proceso_AplicarPago(AplicarPagoParametros parametros)
        {
            Resultado<bool> resultado = new Resultado<bool>();
            List<DetallePagoMasivo> pagos = new List<DetallePagoMasivo>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_AplicacionPagoPRO"))
                {
                    db.AddInParameter(com, "@idPago", DbType.Int32, parametros.idPago);
                    db.AddInParameter(com, "@aplicarUnico", DbType.Boolean, parametros.aplicarUnico);
                    db.AddInParameter(com, "@recalcular", DbType.Boolean, parametros.recalcular);

                    if (parametros.idProceso > 0)
                    {
                        db.AddInParameter(com, "@idProceso", DbType.Int32, parametros.idProceso);
                    }

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                DetallePagoMasivo pago = new DetallePagoMasivo();
                                pago.idPago = Utils.GetInt(reader, "idPago");                                
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }

                resultado.Codigo = 0;
            }
            catch (Exception ex)
            {
                resultado.Codigo = 1;
                resultado.Mensaje = ex.Message;
            }

            return resultado;
        }

        public Resultado<bool> Proceso_DesaplicarPago(DesaplicarPagoParametros parametros)
        {
            Resultado<bool> resultado = new Resultado<bool>();
            List<DetallePagoMasivo> pagos = new List<DetallePagoMasivo>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_DesaplicacionPagoPRO"))
                {
                    db.AddInParameter(com, "@idPago", DbType.Int32, parametros.idPago);                                        

                    if (parametros.idProceso > 0)
                    {
                        db.AddInParameter(com, "@idProceso", DbType.Int32, parametros.idProceso);
                    }

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                DetallePagoMasivo pago = new DetallePagoMasivo();
                                pago.idPago = Utils.GetInt(reader, "idPago");
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }

                resultado.Codigo = 0;
            }
            catch (Exception ex)
            {
                resultado.Codigo = 1;
                resultado.Mensaje = ex.Message;                
            }

            return resultado;
        }

        public List<Pago> Filtrar(Pago Entity)
        {
            List<Pago> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCPagos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "FLT_PAGO"); // Filtrar Pagos
                    db.AddInParameter(com, "@IdDomiciliacion", DbType.Int32, (Entity.IdDomiciliacion > 0) ? Entity.IdDomiciliacion : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, (Entity.IdSolicitud > 0) ? Entity.IdSolicitud : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, (Entity.Estatus.IdEstatus > 0) ? Entity.Estatus.IdEstatus : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdCanalPago", DbType.Int32, (Entity.CanalPago.IdCanalPago > 0) ? Entity.CanalPago.IdCanalPago : (object)DBNull.Value);
                    db.AddInParameter(com, "@FechaPagoDesde", DbType.DateTime, (Entity.FechaPago.ValueIni != null) ? Entity.FechaPago.ValueIni : (object)DBNull.Value);
                    db.AddInParameter(com, "@FechaPagoHasta", DbType.DateTime, (Entity.FechaPago.ValueEnd != null) ? Entity.FechaPago.ValueEnd : (object)DBNull.Value);
                    db.AddInParameter(com, "@FechaRegistroDesde", DbType.DateTime, (Entity.FechaRegistro.ValueIni != null) ? Entity.FechaRegistro.ValueIni : (object)DBNull.Value);
                    db.AddInParameter(com, "@FechaRegistroHasta", DbType.DateTime, (Entity.FechaRegistro.ValueEnd != null) ? Entity.FechaRegistro.ValueEnd : (object)DBNull.Value);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            Pago objC = null;
                            objReturn = new List<Pago>();
                            while (reader.Read())
                            {
                                objC = new Pago();

                                //Lectura de los datos del ResultSet
                                objC.IdPago = Utils.GetInt(reader, "IdPago");
                                objC.IdDomiciliacion = Utils.GetInt(reader, "IdDomiciliacion");
                                objC.IdPagoDirecto = Utils.GetInt(reader, "IdPagoDirecto");
                                objC.IdSolicitud = Utils.GetInt(reader, "IdSolicitud");
                                objC.MontoCobrado = Utils.GetDecimal(reader, "MontoCobrado");
                                objC.MontoAplicado = Utils.GetDecimal(reader, "MontoAplicado");
                                objC.MontoPorAplicar = Utils.GetDecimal(reader, "MontoPorAplicar");
                                objC.CanalPagoClave = Utils.GetString(reader, "CanalPagoClave");
                                objC.CanalPagoDesc = Utils.GetString(reader, "CanalPagoDesc");
                                objC.Estatus.IdEstatus = Utils.GetInt(reader, "IdEstatus");
                                objC.Estatus.EstatusClave = Utils.GetString(reader, "EstatusClave");
                                objC.Estatus.EstatusDesc = Utils.GetString(reader, "EstatusDesc");
                                objC.FechaPago.Value = Utils.GetDateTime(reader, "FechaPago");
                                objC.FechaRegistro.Value = Utils.GetDateTime(reader, "FechaRegistro");
                                objC.FechaAplicacion.Value = Utils.GetDateTime(reader, "FechaAplicacion");

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> HardResetCobranzaCuenta(int IdSolicitud)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCPagos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "HRESET_COBRANZA_CUENTA"); // HAR RESET Cobranza
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, IdSolicitud);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.Codigo = 0;
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> AplicarPago(Pago Entity, bool AplicarUnico)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_AplicarPagoPRO"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@idPago", DbType.Int32, Entity.IdPago);
                    db.AddInParameter(com, "@aplicarUnico", DbType.Boolean, AplicarUnico);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.Codigo = 0;
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> DesaplicarPago(Pago Entity)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_DesaplicarPagoPRO"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdPago", DbType.Int32, Entity.IdPago);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.Codigo = 0;
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        private Resultado<bool> ActualizarPago(String accion, Pago pago, int IdUsuario = 0)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCPagos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, accion);
                    db.AddInParameter(com, "@IdPago", DbType.Int32, pago.IdPago);
                    if(IdUsuario > 0)
                        db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.Codigo = 0;
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> CancelarPago(Pago pago)
        {
            return ActualizarPago("CANCELAR_PAGO", pago);
        }
        
        public Resultado<bool> DevolverPago(Pago pago)
        {
            return ActualizarPago("DEVOLVER_PAGO", pago);
        }

        public Resultado<bool> LiquidarPago(Pago pago)
        {
            return ActualizarPago("LIQUIDAR_PAGO", pago);
        }

        public Resultado<bool> RegistrarPago(Pago pago, int IdUsuario)
        {
            return ActualizarPago("REGISTRAR_PAGO", pago, IdUsuario);
        }

        public Resultado<bool> ActualizarCanalPago(Pago Entity)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCPagos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "UPD_PAGO_CANALPAGO"); // Actualizar el Canal de Pago a un Pago Registrado
                    db.AddInParameter(com, "@IdPago", DbType.Int32, Entity.IdPago);
                    db.AddInParameter(com, "@IdCanalPago", DbType.Int32, Entity.CanalPago.IdCanalPago);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.Codigo = 0;
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> RegistrarPagoVisaNet(Pago Entity)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCPagos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "INS_PAGO_VISANET"); // Inserta en Pago una Peticion Visanet Cobrada con Exito
                    db.AddInParameter(com, "@IdPeticionVisanet", DbType.Int32, Entity.IdPeticionVisaNet);                    

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.Codigo = 0;
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> RegistrarPagoKushki(Pago Entity)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCPagos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "INS_PAGO_KUSHKI"); // Inserta en Pago una Peticion Visanet Cobrada con Exito
                    db.AddInParameter(com, "@IdPeticionKushki", DbType.Int32, Entity.IdPeticionKushki);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.Codigo = 0;
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<BalanceCuenta> Balance_Filtrar(BalanceCuenta entity)
        {
            List<BalanceCuenta> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCPagos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "FLT_BALANCE"); // Filtrar Balance Cuentas
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, (entity.IdSolicitud > 0) ? entity.IdSolicitud : (object)DBNull.Value);
                    db.AddInParameter(com, "@NombreCliente", DbType.String, entity.ClienteNombre);
                    db.AddInParameter(com, "@FechaCreditoDesde", DbType.DateTime, (entity.FechaCredito.ValueIni != null) ? entity.FechaCredito.ValueIni : (object)DBNull.Value);
                    db.AddInParameter(com, "@FechaCreditoHasta", DbType.DateTime, (entity.FechaCredito.ValueEnd != null) ? entity.FechaCredito.ValueEnd : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, (entity.EstatusCredito.IdEstatus > 0) ? entity.EstatusCredito.IdEstatus : (object)DBNull.Value);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            BalanceCuenta objC = null;
                            objReturn = new List<BalanceCuenta>();
                            while (reader.Read())
                            {
                                objC = new BalanceCuenta();

                                //Lectura de los datos del ResultSet                                
                                objC.IdSolicitud = Utils.GetInt(reader, "IdSolicitud");
                                objC.IdCuenta = Utils.GetInt(reader, "IdCuenta");
                                objC.FechaCredito.Value = Utils.GetDateTime(reader, "FechaCredito");
                                objC.ClienteNombre = Utils.GetString(reader, "ClienteNombre");
                                objC.ClienteApPaterno = Utils.GetString(reader, "ClienteApPaterno");
                                objC.ClienteApMaterno = Utils.GetString(reader, "ClienteApMaterno");
                                objC.ProductoDesc = Utils.GetString(reader, "Producto");
                                objC.Capital = Utils.GetDecimal(reader, "Capital");
                                objC.SaldoCapital = Utils.GetDecimal(reader, "SaldoCapital");
                                objC.SaldoVencido = Utils.GetDecimal(reader, "SaldoVencido");                               

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public BalanceCuenta Balance_Obtener(BalanceCuenta entity)
        {
            BalanceCuenta objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCPagos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_BALANCE"); // Obtener Balance Cuenta
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, (entity.IdSolicitud > 0) ? entity.IdSolicitud : (object)DBNull.Value);                    

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {                                                        
                            if (reader.Read())
                            {
                                objReturn = new BalanceCuenta();

                                // Lectura de los datos del ResultSet                                
                                objReturn.IdSolicitud = Utils.GetInt(reader, "IdSolicitud");
                                objReturn.IdCuenta = Utils.GetInt(reader, "IdCuenta");
                                objReturn.IdTipoCredito = Utils.GetInt(reader, "IdTipoCredito");
                                objReturn.TipoCreditoDesc = Utils.GetString(reader, "TipoCreditoDesc");
                                objReturn.FechaCredito.Value = Utils.GetDateTime(reader, "FechaCredito");
                                objReturn.ClienteNombre = Utils.GetString(reader, "ClienteNombre");
                                objReturn.ClienteApPaterno = Utils.GetString(reader, "ClienteApPaterno");
                                objReturn.ClienteApMaterno = Utils.GetString(reader, "ClienteApMaterno");
                                objReturn.ProductoDesc = Utils.GetString(reader, "Producto");
                                objReturn.MontoCuota = Utils.GetDecimal(reader, "Cuota");
                                objReturn.CostoTotalCredito = Utils.GetDecimal(reader, "CostoTotalCredito");
                                objReturn.MontoGAT = Utils.GetDecimal(reader, "MontoGAT");
                                objReturn.MontoUsoCanal = Utils.GetDecimal(reader, "MontoUsoCanal");
                                objReturn.TasaSeguroDesgravamen = Utils.GetDecimal(reader, "TasaSeguroDesgravamen");
                                objReturn.Capital = Utils.GetDecimal(reader, "Capital");
                                objReturn.SaldoCapital = Utils.GetDecimal(reader, "SaldoCapital");
                                objReturn.SaldoVencido = Utils.GetDecimal(reader, "SaldoVencido");
                                objReturn.ComisionOrigen = Utils.GetDecimal(reader, "ComisionOrigen");

                                // Cargar datos del Estatus del Credito
                                objReturn.EstatusCredito.IdEstatus = Utils.GetInt(reader, "IdEstatusCredito");
                                objReturn.EstatusCredito.EstatusClave = Utils.GetString(reader, "ClaveEstatusCredito");
                                objReturn.EstatusCredito.EstatusDesc = Utils.GetString(reader, "DescEstatusCredito");
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<BalanceCuenta.Recibos> Balance_Obtener_Recibos(BalanceCuenta entity)
        {
            List<BalanceCuenta.Recibos> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCPagos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_BALANCE_RECIBOS"); // Obtener Recibos Balance Cuenta
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, (entity.IdSolicitud > 0) ? entity.IdSolicitud : (object)DBNull.Value);                    

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            BalanceCuenta.Recibos objC = null;
                            objReturn = new List<BalanceCuenta.Recibos>();
                            while (reader.Read())
                            {
                                objC = new BalanceCuenta.Recibos();

                                //Lectura de los datos del ResultSet                                
                                objC.IdCuenta = Utils.GetInt(reader, "IdCuenta");
                                objC.Recibo = Utils.GetInt(reader, "Recibo");
                                objC.FechaRecibo.Value = Utils.GetDateTime(reader, "FechaRecibo");
                                objC.IdEstatus = Utils.GetInt(reader, "IdEstatus");
                                objC.EstatusClave = Utils.GetString(reader, "EstatusClave");
                                objC.EstatusDesc = Utils.GetString(reader, "EstatusDesc");                                
                                objC.Capital = Utils.GetDecimal(reader, "Capital");
                                objC.Interes = Utils.GetDecimal(reader, "Interes");
                                objC.IGV = Utils.GetDecimal(reader, "IGV");
                                objC.Seguro = Utils.GetDecimal(reader, "Seguro");
                                objC.GAT = Utils.GetDecimal(reader, "GAT");
                                objC.CapitalInicial = Utils.GetDecimal(reader, "CapitalInicial");
                                objC.CapitalInsoluto = Utils.GetDecimal(reader, "CapitalInsoluto");
                                objC.SaldoCapital = Utils.GetDecimal(reader, "SaldoCapital");
                                objC.SaldoInteres = Utils.GetDecimal(reader, "SaldoInteres");
                                objC.SaldoIGV = Utils.GetDecimal(reader, "SaldoIGV");
                                objC.SaldoSeguro = Utils.GetDecimal(reader, "SaldoSeguro");
                                objC.SaldoGAT = Utils.GetDecimal(reader, "SaldoGAT");
                                objC.SaldoOtrosCargos = Utils.GetDecimal(reader, "SaldoOtrosCargos");
                                objC.SaldoIGVOtrosCargos = Utils.GetDecimal(reader, "SaldoIGVOtrosCargos");
                                objC.SaldoRecibo = Utils.GetDecimal(reader, "SaldoRecibo");
                                objC.TotalRecibo = Utils.GetDecimal(reader, "TotalRecibo");

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }        

        public List<Pagos.ChartCartera> ChartCartera(int Anio, int MesInicio, int MesFin)
        {
            List<Pagos.ChartCartera> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCPagos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "CHART_CARTERA"); // Grafica Cobranza Cartera
                    db.AddInParameter(com, "@Anio", DbType.Int32, Anio);
                    db.AddInParameter(com, "@MesInicio", DbType.Int32, MesInicio);
                    db.AddInParameter(com, "@MesFin", DbType.Int32, MesFin);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            Pagos.ChartCartera objC = null;
                            objReturn = new List<Pagos.ChartCartera>();
                            while (reader.Read())
                            {
                                objC = new Pagos.ChartCartera();

                                //Lectura de los datos del ResultSet
                                objC.AnioMes = Utils.GetString(reader, "AnioMes");
                                objC.CarteraVencida = Utils.GetDecimal(reader, "CarteraVencida");
                                objC.Cobros = Utils.GetDecimal(reader, "Cobros");
                                objC.PorAplicar = Utils.GetDecimal(reader, "PorAplicar");                               

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }
    }
}
