﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;

namespace SOPF.Core.DataAccess.Cobranza
{
    public class CanalPagoDAL
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public CanalPagoDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public List<CanalPago> ListarTodos()
        {
            List<CanalPago> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCCanalPago"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "FLT");

                    //Ejecucion de la Consulta                    
                    objReturn = ToList(db.ExecuteReader(com));                    

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<CanalPago> ListarActivos()
        {
            List<CanalPago> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCCanalPago"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "FLT");
                    db.AddInParameter(com, "@Activo", DbType.Boolean, true);

                    //Ejecucion de la Consulta                    
                    objReturn = ToList(db.ExecuteReader(com));

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<CanalPago> ListarInactivos()
        {
            List<CanalPago> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCCanalPago"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "FLT");
                    db.AddInParameter(com, "@Activo", DbType.Boolean, false);

                    //Ejecucion de la Consulta                    
                    objReturn = ToList(db.ExecuteReader(com));

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }        

        #region "Metodos Privados"

        private List<CanalPago> ToList(IDataReader reader)
        {
            List<CanalPago> objReturn = new List<CanalPago>();

            if (reader != null)
            {
                CanalPago objC = null;
                objReturn = new List<CanalPago>();

                while (reader.Read())
                {
                    objC = new CanalPago();

                    //Lectura de los datos del ResultSet
                    objC.IdCanalPago = Utils.GetInt(reader, "IdCanalPago");
                    objC.Nombre = Utils.GetString(reader, "Nombre");
                    objC.Clave = Utils.GetString(reader, "Clave");
                    objC.MontoComisionFijo = Utils.GetDecimal(reader, "MontoComisionFijo");
                    objC.PorcentajeComisionFijo = Utils.GetDecimal(reader, "PorcentajeComisionFijo");
                    objC.Activo = Utils.GetBool(reader, "Activo");

                    objReturn.Add(objC);
                }
            }

            reader.Dispose();

            return objReturn;
        }

        #endregion
    }
}
