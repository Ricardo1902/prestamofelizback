﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Poco.Hubble;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.DataAccess.Hubble
{
    public class AccountDAL
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public AccountDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public List<TB_EmailAccounts> GetTableAccounts(
            int idEmailAccount = 0,
            string cuenta = null,
            string host = null,
            bool? enableSSL = null,
            bool? activo = null,
            int usuarioCreacion = 0
        )
        {
            List<TB_EmailAccounts> tbEmailAccounts = new List<TB_EmailAccounts>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_EmailAccountCON"))
                {
                    if (idEmailAccount > 0)
                    {
                        db.AddInParameter(com, "@IdEmailAccount", DbType.Int32, idEmailAccount);
                    }
                    if (!String.IsNullOrEmpty(cuenta))
                    {
                        db.AddInParameter(com, "@Cuenta", DbType.String, cuenta);
                    }
                    if (!String.IsNullOrEmpty(host))
                    {
                        db.AddInParameter(com, "@Host", DbType.String, host);
                    }
                    if (enableSSL.HasValue)
                    {
                        db.AddInParameter(com, "@EnableSSL", DbType.Boolean, enableSSL.Value);
                    }
                    if (activo.HasValue)
                    {
                        db.AddInParameter(com, "@Activo", DbType.Boolean, activo.Value);
                    }
                    if (usuarioCreacion > 0)
                    {
                        db.AddInParameter(com, "@UsuarioCreacion", DbType.Int32, usuarioCreacion);
                    }

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                tbEmailAccounts.Add(new TB_EmailAccounts
                                {
                                    IdEmailAccount = Utils.GetInt(reader, "IdEmailAccount"),
                                    Cuenta = Utils.GetString(reader, "Cuenta"),
                                    Password = Utils.GetString(reader, "Password"),
                                    Host = Utils.GetString(reader, "Host"),
                                    Port = Utils.GetInt(reader, "Port"),
                                    Timeout = Utils.GetInt(reader, "Timeout"),
                                    EnableSSL = Utils.GetBool(reader, "EnableSSL"),
                                    Activo = Utils.GetBool(reader, "Activo"),
                                    FechaCreacion = Utils.GetDateTime(reader, "FechaCreacion"),
                                    UsuarioCreacion = Utils.GetInt(reader, "UsuarioCreacion"),
                                    FechaModificacion = Utils.GetDateTime(reader, "FechaModificacion"),
                                    UsuarioModificacion = Utils.GetInt(reader, "UsuarioModificacion")
                                });
                            }
                        }

                        reader.Dispose();
                    }

                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tbEmailAccounts;
        }
    }
}
