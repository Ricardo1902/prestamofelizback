﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Entities.Hubble;
using SOPF.Core.Poco.Hubble;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.DataAccess.Hubble
{
    public class TemplateDAL
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TemplateDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public List<TB_EmailTemplates> GetTableTemplates(
            int idEmailTemplate = 0,
            string template = null,
            bool? activo = null,
            int emailTemplateType_Id = 0,
            int usuarioCreacion = 0
        )
        {
            List<TB_EmailTemplates> tbEmailTemplates = new List<TB_EmailTemplates>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_EmailTemplateCON"))
                {
                    if (idEmailTemplate > 0)
                    {
                        db.AddInParameter(com, "@IdEmailTemplate", DbType.Int32, idEmailTemplate);
                    }
                    if (!String.IsNullOrEmpty(template))
                    {
                        db.AddInParameter(com, "@Template", DbType.String, template);
                    }
                    if (activo.HasValue)
                    {
                        db.AddInParameter(com, "@Activo", DbType.Boolean, activo.Value);
                    }
                    if (emailTemplateType_Id > 0)
                    {
                        db.AddInParameter(com, "@EmailTemplateType_Id", DbType.Int32, emailTemplateType_Id);
                    }
                    if (usuarioCreacion > 0)
                    {
                        db.AddInParameter(com, "@UsuarioCreacion", DbType.Int32, usuarioCreacion);
                    }

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                tbEmailTemplates.Add(new TB_EmailTemplates
                                {
                                    IdEmailTemplate = Utils.GetInt(reader, "IdEmailTemplate"),
                                    Template = Utils.GetString(reader, "Template"),
                                    Descripcion = Utils.GetString(reader, "Descripcion"),
                                    Activo = Utils.GetBool(reader, "Activo"),
                                    EmailTemplateType_Id = Utils.GetInt(reader, "EmailTemplateType_Id"),
                                    Tipo = Utils.GetString(reader, "Tipo"),
                                    FechaCreacion = Utils.GetDateTime(reader, "FechaCreacion"),
                                    UsuarioCreacion = Utils.GetInt(reader, "UsuarioCreacion"),
                                    FechaModificacion = Utils.GetDateTime(reader, "FechaModificacion"),
                                    UsuarioModificacion = Utils.GetInt(reader, "UsuarioModificacion")
                                });
                            }
                        }

                        reader.Dispose();
                    }

                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tbEmailTemplates;
        }

        public List<TB_EmailTemplateParams> GetTableTemplateParams(
            int idEmailTemplateParam = 0,
            string parametro = null,
	        string friendlyName = null,
	        bool? activo = null,
            int emailTemplate_Id = 0,
	        int emailTemplateParamType_Id = 0,
            int usuarioCreacion = 0
        )
        {
            List<TB_EmailTemplateParams> tbEmailTemplateParams = new List<TB_EmailTemplateParams>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_EmailTemplateParamCON"))
                {
                    if (idEmailTemplateParam > 0)
                    {
                        db.AddInParameter(com, "@IdEmailTemplateParam", DbType.Int32, idEmailTemplateParam);
                    }
                    if (!String.IsNullOrEmpty(parametro))
                    {
                        db.AddInParameter(com, "@Parametro", DbType.String, parametro);
                    }
                    if (!String.IsNullOrEmpty(friendlyName))
                    {
                        db.AddInParameter(com, "@FriendlyName", DbType.String, friendlyName);
                    }
                    if (activo.HasValue)
                    {
                        db.AddInParameter(com, "@Activo", DbType.Boolean, activo.Value);
                    }
                    if (emailTemplate_Id > 0)
                    {
                        db.AddInParameter(com, "@EmailTemplate_Id", DbType.Int32, emailTemplate_Id);
                    }
                    if (emailTemplateParamType_Id > 0)
                    {
                        db.AddInParameter(com, "@EmailTemplateParamType_Id", DbType.Int32, emailTemplateParamType_Id);
                    }
                    if (usuarioCreacion > 0)
                    {
                        db.AddInParameter(com, "@UsuarioCreacion", DbType.Int32, usuarioCreacion);
                    }

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                tbEmailTemplateParams.Add(new TB_EmailTemplateParams
                                {
                                    IdEmailTemplateParam = Utils.GetInt(reader, "IdEmailTemplateParam"),
                                    Parametro = Utils.GetString(reader, "Parametro"),
                                    Descripcion = Utils.GetString(reader, "Descripcion"),
                                    FriendlyName = Utils.GetString(reader, "FriendlyName"),
                                    Placeholder = Utils.GetString(reader, "Placeholder"),
                                    Help = Utils.GetString(reader, "Help"),
                                    Activo = Utils.GetBool(reader, "Activo"),
                                    EmailTemplate_Id = Utils.GetInt(reader, "EmailTemplate_Id"),
                                    EmailTemplateParamType_Id = Utils.GetInt(reader, "EmailTemplateParamType_Id"),
                                    FechaCreacion = Utils.GetDateTime(reader, "FechaCreacion"),
                                    UsuarioCreacion = Utils.GetInt(reader, "UsuarioCreacion"),
                                    FechaModificacion = Utils.GetDateTime(reader, "FechaModificacion"),
                                    UsuarioModificacion = Utils.GetInt(reader, "UsuarioModificacion")
                                });
                            }
                        }

                        reader.Dispose();
                    }

                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tbEmailTemplateParams;
        }

        public List<TB_TemplateParamInputTypes> GetTableTemplateParamInputTypes(
            int idEmailTemplateParamType = 0,
            string tipo = null,
            bool? activo = null,
            int usuarioCreacion = 0
        )
        {
            List<TB_TemplateParamInputTypes> tbTemplateParamInputTypes = new List<TB_TemplateParamInputTypes>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_TemplateParamInputTypeCON"))
                {
                    if (idEmailTemplateParamType > 0)
                    {
                        db.AddInParameter(com, "@IdEmailTemplateParamType", DbType.Int32, idEmailTemplateParamType);
                    }
                    if (!String.IsNullOrEmpty(tipo))
                    {
                        db.AddInParameter(com, "@Tipo", DbType.String, tipo);
                    }
                    if (activo.HasValue)
                    {
                        db.AddInParameter(com, "@Activo", DbType.Boolean, activo.Value);
                    }
                    if (usuarioCreacion > 0)
                    {
                        db.AddInParameter(com, "@UsuarioCreacion", DbType.Int32, usuarioCreacion);
                    }

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                tbTemplateParamInputTypes.Add(new TB_TemplateParamInputTypes
                                {
                                    IdEmailTemplateParamType = Utils.GetInt(reader, "IdEmailTemplateParamType"),
                                    Tipo = Utils.GetString(reader, "Tipo"),
                                    HtmlFragment = Utils.GetString(reader, "HtmlFragment"),
                                    Activo = Utils.GetBool(reader, "Activo"),
                                    FechaCreacion = Utils.GetDateTime(reader, "FechaCreacion"),
                                    UsuarioCreacion = Utils.GetInt(reader, "UsuarioCreacion"),
                                    FechaModificacion = Utils.GetDateTime(reader, "FechaModificacion"),
                                    UsuarioModificacion = Utils.GetInt(reader, "UsuarioModificacion")
                                });
                            }
                        }

                        reader.Dispose();
                    }

                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tbTemplateParamInputTypes;
        }

        public List<TB_SmartTypes> GetTableSmartTypes(
            int idSmartType = 0,
            string tipo = null,
            string friendlyName = null,
            bool? activo = null,
            int usuarioCreacion = 0
        )
        {
            List<TB_SmartTypes> tbSmartTypes = new List<TB_SmartTypes>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_SmartTypeCON"))
                {
                    if (idSmartType > 0)
                    {
                        db.AddInParameter(com, "@IdSmartType", DbType.Int32, idSmartType);
                    }
                    if (!String.IsNullOrEmpty(tipo))
                    {
                        db.AddInParameter(com, "@Tipo", DbType.String, tipo);
                    }
                    if (!String.IsNullOrEmpty(friendlyName))
                    {
                        db.AddInParameter(com, "@FriendlyName", DbType.String, friendlyName);
                    }
                    if (activo.HasValue)
                    {
                        db.AddInParameter(com, "@Activo", DbType.Boolean, activo.Value);
                    }
                    if (usuarioCreacion > 0)
                    {
                        db.AddInParameter(com, "@UsuarioCreacion", DbType.Int32, usuarioCreacion);
                    }

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                tbSmartTypes.Add(new TB_SmartTypes
                                {
                                    IdSmartType = Utils.GetInt(reader, "IdSmartType"),
                                    Tipo = Utils.GetString(reader, "Tipo"),
                                    Descripcion = Utils.GetString(reader, "Description"),
                                    FriendlyName = Utils.GetString(reader, "FriendlyName"),
                                    Indexed = Utils.GetBool(reader, "Indexed"),
                                    Activo = Utils.GetBool(reader, "Activo"),
                                    StoredCallback_Id = Utils.GetInt(reader, "StoredCallback_Id"),
                                    FechaCreacion = Utils.GetDateTime(reader, "FechaCreacion"),
                                    UsuarioCreacion = Utils.GetInt(reader, "UsuarioCreacion"),
                                    FechaModificacion = Utils.GetDateTime(reader, "FechaModificacion"),
                                    UsuarioModificacion = Utils.GetInt(reader, "UsuarioModificacion")
                                });
                            }
                        }

                        reader.Dispose();
                    }

                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tbSmartTypes;
        }
    }
}
