﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Entities.Hubble;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SOPF.Core.DataAccess.Hubble
{
    public class SmartEmailScheduleDAL
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public SmartEmailScheduleDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public bool CreateSmartEmailSchedule(
            string nombre,
            string descripcion,
            string asunto,
            DateTime horarioGeneracion,
            DateTime horarioEnvio,
            DateTime fechaUltimaGeneracion,
            bool archivosEnZip,
            int emailTemplateId,
            int emailAccountId,
            int smartType,
            int usuarioId,
            List<ReqTemplateParam> emailParams,
            List<string> attachments
        )
        {
            int smartEmailScheduleCreated = 0;
            bool created = false;

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_SmartEmailALT"))
                {
                    db.AddInParameter(com, "@Nombre", DbType.String, nombre);
                    db.AddInParameter(com, "@Descripcion", DbType.String, descripcion);
                    db.AddInParameter(com, "@Asunto", DbType.String, asunto);
                    db.AddInParameter(com, "@HorarioGeneracion", DbType.Time, horarioGeneracion.ToString("HH:mm"));
                    db.AddInParameter(com, "@HorarioEnvio", DbType.Time, horarioEnvio.ToString("HH:mm"));
                    db.AddInParameter(com, "@ArchivosEnZip", DbType.Boolean, archivosEnZip);
                    db.AddInParameter(com, "@FechaUltimaGeneracion", DbType.DateTime2, fechaUltimaGeneracion);
                    db.AddInParameter(com, "@SmartType_Id", DbType.Int32, smartType);
                    db.AddInParameter(com, "@EmailTemplate_Id", DbType.Int32, emailTemplateId);
                    db.AddInParameter(com, "@EmailAccount_Id", DbType.Int32, emailAccountId);
                    db.AddInParameter(com, "@Usuario", DbType.Int32, usuarioId);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            if (reader.Read())
                            {
                                smartEmailScheduleCreated = Utils.GetInt(reader, "Created");
                            }
                        }

                        reader.Dispose();
                    }

                    com.Dispose();
                }

                RegisterSmartEmailParam(emailParams, smartEmailScheduleCreated, usuarioId);
                RegisterEmailAttachments(attachments, smartEmailScheduleCreated, usuarioId);

                created = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return created;
        }

        private bool RegisterSmartEmailParam(List<ReqTemplateParam> emailParams, int smartEmailScheduleId, int userId)
        {
            Regex reg = new Regex(@"\[@[a-zA-Z0-9- ]+\]");
            Regex removeBraces = new Regex(@"[\[\]]");
            bool created = false;

            try
            {
                if (emailParams.Any())
                {
                    emailParams.ForEach(x => {
                        string finalValue = x.Value;
                        MatchCollection matches = reg.Matches(finalValue);
                        List<string> metadata = matches.Cast<Match>().Select(m => removeBraces.Replace(m.Value, String.Empty)).ToList();

                        if (metadata.Any())
                        {
                            foreach (string met in metadata)
                            {
                                string rawMetadata = "";
                                using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_TemplateMetadataCON"))
                                {
                                    db.AddInParameter(com, "@FriendlyName", DbType.String, met);

                                    using (IDataReader reader = db.ExecuteReader(com))
                                    {
                                        if (reader != null)
                                        {
                                            if (reader.Read())
                                            {
                                                rawMetadata = Utils.GetString(reader, "Metadata");
                                            }
                                        }

                                        reader.Dispose();
                                    }
                                    com.Dispose();
                                }

                                string replaceRegString = $@"\[{met}\]";

                                finalValue = Regex.Replace(finalValue, replaceRegString, @"\" + "{{" + rawMetadata + "}}");
                            }
                        }

                        using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_SmartEmailTemplateParamALT"))
                        {
                            db.AddInParameter(com, "@SmartEmail_Id", DbType.Int32, smartEmailScheduleId);
                            db.AddInParameter(com, "@EmailTemplateParam_Id", DbType.Int32, x.Id);
                            db.AddInParameter(com, "@Valor", DbType.String, finalValue);
                            db.AddInParameter(com, "@Usuario", DbType.Int32, userId);

                            db.ExecuteNonQuery(com);
                            com.Dispose();
                        }
                    });

                    created = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return created;
        }

        private bool RegisterEmailAttachments(List<string> attachments, int smartEmailScheduleId, int userId)
        {
            bool created = false;

            try
            {
                if (attachments.Any())
                {
                    attachments.ForEach(x => {
                        using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_SmartEmailAttachmentALT"))
                        {
                            db.AddInParameter(com, "@RutaArchivo", DbType.String, x);
                            db.AddInParameter(com, "@SmartEmail_Id", DbType.Int32, smartEmailScheduleId);
                            db.AddInParameter(com, "@Usuario", DbType.Int32, userId);

                            db.ExecuteNonQuery(com);
                            com.Dispose();
                        }
                    });

                    created = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return created;
        }
    }
}
