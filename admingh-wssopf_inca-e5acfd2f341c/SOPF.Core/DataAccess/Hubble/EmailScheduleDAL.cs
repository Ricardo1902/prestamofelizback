﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Entities.Hubble;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SOPF.Core.DataAccess.Hubble
{
    public class EmailScheduleDAL
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public EmailScheduleDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public bool CreateEmailSchedule(
            string nombre,
            string descripcion,
            string asunto,
            DateTime fechaProgramacion,
            bool emailUnico,
            bool archivosEnZip,
            int emailTemplateId,
            int emailAccountId,
            int usuarioId,
            List<string> To,
            List<string> CC,
            List<string> BCC,
            List<ReqTemplateParam> emailParams,
            List<string> attachments
        )
        {
            int emailScheduleCreated = 0;
            bool created = false;

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_EmailScheduleALT"))
                {
                    db.AddInParameter(com, "@Nombre", DbType.String, nombre);
                    db.AddInParameter(com, "@Descripcion", DbType.String, descripcion);
                    db.AddInParameter(com, "@Asunto", DbType.String, asunto);
                    db.AddInParameter(com, "@FechaProgramacion", DbType.DateTime2, fechaProgramacion);
                    db.AddInParameter(com, "@EmailUnico", DbType.Boolean, emailUnico);
                    db.AddInParameter(com, "@ArchivosEnZip", DbType.Boolean, archivosEnZip);
                    db.AddInParameter(com, "@EmailTemplate_Id", DbType.Int32, emailTemplateId);
                    db.AddInParameter(com, "@EmailAccount_Id", DbType.Int32, emailAccountId);
                    db.AddInParameter(com, "@Usuario", DbType.Int32, usuarioId);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            if (reader.Read())
                            {
                                emailScheduleCreated = Utils.GetInt(reader, "Created");
                            }
                        }

                        reader.Dispose();
                    }

                    com.Dispose();
                }

                RegisterEmailAddresses(To, emailScheduleCreated, usuarioId);
                RegisterEmailAddresses(CC, emailScheduleCreated, usuarioId, "CC");
                RegisterEmailAddresses(BCC, emailScheduleCreated, usuarioId, "BCC");
                RegisterEmailParam(emailParams, emailScheduleCreated, usuarioId);
                RegisterEmailAttachments(attachments, emailScheduleCreated, usuarioId);

                created = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return created;
        }

        public int UnsubscribeEmail(string email, string emailTemplate)
        {
            int IdCreated = 0;

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_UnsubscribeEmailALT"))
                {
                    db.AddInParameter(com, "@Email", DbType.String, email);
                    db.AddInParameter(com, "@EmailTemplate", DbType.String, emailTemplate);
                    db.AddOutParameter(com, "@IdCreated", DbType.Int32, IdCreated);
                    db.ExecuteNonQuery(com);

                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IdCreated;
        }

        private bool RegisterEmailAddresses(List<string> addresses, int emailScheduleId, int userId, string type = "To")
        {
            bool created = false;

            try
            {
                if (addresses.Any())
                {
                    addresses.ForEach(x => {
                        List<string> _smartEmailAddresses = new List<string>();

                        if (x.StartsWith("@"))
                        {
                            int idSmartType = 0;
                            using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_SmartTypeCON"))
                            {
                                db.AddInParameter(com, "@FriendlyName", DbType.String, x);

                                using (IDataReader reader = db.ExecuteReader(com))
                                {
                                    if (reader != null)
                                    {
                                        if (reader.Read())
                                        {
                                            idSmartType = Utils.GetInt(reader, "IdSmartType");
                                        }
                                    }

                                    reader.Dispose();
                                }

                                com.Dispose();
                            }

                            using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_GetSmarEmailAddressesPRO"))
                            {
                                db.AddInParameter(com, "@SmartType_Id", DbType.Int32, idSmartType);

                                using (IDataReader reader = db.ExecuteReader(com))
                                {
                                    if (reader != null)
                                    {
                                        while (reader.Read())
                                        {
                                            _smartEmailAddresses.Add(Utils.GetString(reader, "Email"));
                                        }
                                    }

                                    reader.Dispose();
                                }

                                com.Dispose();
                            }
                        }
                        
                        if (_smartEmailAddresses.Any())
                        {
                            _smartEmailAddresses.ForEach(se => {
                                using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_EmailAddressALT"))
                                {
                                    db.AddInParameter(com, "@Email", DbType.String, se);
                                    db.AddInParameter(com, "@EmailSchedule_Id", DbType.Int32, emailScheduleId);
                                    db.AddInParameter(com, "@Usuario", DbType.Int32, userId);

                                    switch (type)
                                    {
                                        case "CC":
                                            db.AddInParameter(com, "@Copia", DbType.Boolean, 1);
                                            break;
                                        case "BCC":
                                            db.AddInParameter(com, "@CopiaOculta", DbType.Boolean, 1);
                                            break;
                                        default:
                                            break;
                                    }

                                    db.ExecuteNonQuery(com);
                                    com.Dispose();
                                }
                            });
                        }
                        else
                        {
                            using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_EmailAddressALT"))
                            {
                                db.AddInParameter(com, "@Email", DbType.String, x);
                                db.AddInParameter(com, "@EmailSchedule_Id", DbType.Int32, emailScheduleId);
                                db.AddInParameter(com, "@Usuario", DbType.Int32, userId);

                                switch (type)
                                {
                                    case "CC":
                                        db.AddInParameter(com, "@Copia", DbType.Boolean, 1);
                                        break;
                                    case "BCC":
                                        db.AddInParameter(com, "@CopiaOculta", DbType.Boolean, 1);
                                        break;
                                    default:
                                        break;
                                }

                                db.ExecuteNonQuery(com);
                                com.Dispose();
                            }
                        }
                    });

                    created = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return created;
        }

        private bool RegisterEmailParam(List<ReqTemplateParam> emailParams, int emailScheduleId, int userId)
        {
            Regex reg = new Regex(@"\[@[a-zA-Z0-9- ]+\]");
            Regex removeBraces = new Regex(@"[\[\]]");
            bool created = false;

            try
            {
                if (emailParams.Any())
                {
                    emailParams.ForEach(x => {
                        string finalValue = x.Value;
                        MatchCollection matches = reg.Matches(finalValue);
                        List<string> metadata = matches.Cast<Match>().Select(m => removeBraces.Replace(m.Value, String.Empty)).ToList();

                        if (metadata.Any())
                        {
                            foreach (string met in metadata)
                            {
                                string rawMetadata = "";
                                using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_TemplateMetadataCON"))
                                {
                                    db.AddInParameter(com, "@FriendlyName", DbType.String, met);

                                    using (IDataReader reader = db.ExecuteReader(com))
                                    {
                                        if (reader != null)
                                        {
                                            if (reader.Read())
                                            {
                                                rawMetadata = Utils.GetString(reader, "Metadata");
                                            }
                                        }

                                        reader.Dispose();
                                    }
                                    com.Dispose();
                                }

                                string replaceRegString = $@"\[{met}\]";

                                finalValue = Regex.Replace(finalValue, replaceRegString, @"\" + "{{" + rawMetadata + "}}");
                            }
                        }

                        using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_EmailSchedulesParamALT"))
                        {
                            db.AddInParameter(com, "@EmailSchedule_Id", DbType.Int32, emailScheduleId);
                            db.AddInParameter(com, "@EmailTemplateParam_Id", DbType.Int32, x.Id);
                            db.AddInParameter(com, "@Valor", DbType.String, finalValue);
                            db.AddInParameter(com, "@Usuario", DbType.Int32, userId);

                            db.ExecuteNonQuery(com);
                            com.Dispose();
                        }
                    });

                    created = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return created;
        }

        private bool RegisterEmailAttachments(List<string> attachments, int emailScheduleId, int userId)
        {
            bool created = false;

            try
            {
                if (attachments.Any())
                {
                    attachments.ForEach(x => {
                        using (DbCommand com = db.GetStoredProcCommand("Hubble.SP_EmailAttachmentALT"))
                        {
                            db.AddInParameter(com, "@RutaArchivo", DbType.String, x);
                            db.AddInParameter(com, "@EmailSchedule_Id", DbType.Int32, emailScheduleId);
                            db.AddInParameter(com, "@Usuario", DbType.Int32, userId);

                            db.ExecuteNonQuery(com);
                            com.Dispose();
                        }
                    });

                    created = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return created;
        }
    }
}
