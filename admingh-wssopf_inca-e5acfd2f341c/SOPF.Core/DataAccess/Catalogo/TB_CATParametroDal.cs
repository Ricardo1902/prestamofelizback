﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Entities.Catalogo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.DataAccess.Catalogo
{
    public class TB_CATParametroDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TB_CATParametroDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public List<TB_CATParametro> ObtenerCATParametros()
        {
            List<TB_CATParametro> resultado = new List<TB_CATParametro>();
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_SelParametro"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 0);
                    db.AddInParameter(com, "@IdParametro", DbType.Int32, 0);
                    db.AddInParameter(com, "@Parametro", DbType.String, "");
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, 0);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TB_CATParametro parametro = new TB_CATParametro();

                                if (!reader.IsDBNull(0)) parametro.IdParametro = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) parametro.Parametro = reader[1].ToString();
                                if (!reader.IsDBNull(2)) parametro.Valor = reader[2].ToString();
                                if (!reader.IsDBNull(3)) parametro.Descripcion = reader[3].ToString();
                                if (!reader.IsDBNull(4)) parametro.IdEstatus = Convert.ToInt32(reader[4]);

                                resultado.Add(parametro);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public bool ActualizarCATParametros(int IdParametro, string Parametro, string Valor, string Descripcion, int IdEstatus)
        {
            bool resultado = false;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_CATParametroACT"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdParametro", DbType.Int32, IdParametro);
                    if (!String.IsNullOrEmpty(Parametro)) { db.AddInParameter(com, "@Parametro", DbType.String, Parametro); }
                    if (!String.IsNullOrEmpty(Valor)) { db.AddInParameter(com, "@Valor", DbType.String, Valor); }
                    if (!String.IsNullOrEmpty(Descripcion)) { db.AddInParameter(com, "@Descripcion", DbType.String, Descripcion); }
                    if (IdEstatus > 0) { db.AddInParameter(com, "@IdEstatus", DbType.Int32, IdEstatus); }
                    db.ExecuteNonQuery(com);

                    resultado = true;

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
    }
}
