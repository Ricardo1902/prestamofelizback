#pragma warning disable 141030
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATBancoDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATBanco
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-10-30 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess
{
    public class TBCATBancoDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCATBancoDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBCATBanco(TBCATBanco entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<TBCATBanco> ObtenerTBCATBanco(int accion, int idBanco)
        {
			List<TBCATBanco> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Catalogo.Sel_CatBanco"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, accion);
                    db.AddInParameter(com, "@Banco", DbType.Int32, idBanco);
                    
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<TBCATBanco>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATBanco Banco = new TBCATBanco();
                                if (!reader.IsDBNull(0)) Banco.IdBanco = Convert.ToInt16(reader[0]);
                                if (!reader.IsDBNull(1)) Banco.Banco = reader[1].ToString();
                                resultado.Add(Banco);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarTBCATBanco()
        {
        }

        public void EliminarTBCATBanco()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}