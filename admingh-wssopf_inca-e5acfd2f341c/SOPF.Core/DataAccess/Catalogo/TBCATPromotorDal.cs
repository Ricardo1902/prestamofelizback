#pragma warning disable 141229
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATPromotorDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATPromotor
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-12-29 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess
{
    public class TBCATPromotorDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCATPromotorDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBCATPromotor(int idSucursal, int Accion)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("catalogo.sel_promotor"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, idSucursal);
                    db.AddInParameter(com, "@idSucursal", DbType.Int32, Accion);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<TBCATPromotor> ObtenerTBCATPromotor(int Accion, int Sucursal)
        {
			List<TBCATPromotor> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("catalogo.sel_promotor"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@idSucursal", DbType.Int32, Sucursal);

					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<TBCATPromotor>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATPromotor promotor = new TBCATPromotor();
                                if (!reader.IsDBNull(0)) promotor.IdPromotor = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) promotor.Promotor = reader[1].ToString();
                                resultado.Add(promotor);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }

        public List<TBCATPromotor> ObtenerTBCATPromotorPorCanalVenta(int Sucursal, int IdCanalVenta)
        {
            List<TBCATPromotor> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Catalogo.SP_PromotorCanalVentaCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, Sucursal);
                    db.AddInParameter(com, "@IdCanalVenta", DbType.Int32, IdCanalVenta);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCATPromotor>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATPromotor promotor = new TBCATPromotor();
                                promotor.IdPromotor = Utils.GetInt(reader, "IdPromotor");
                                promotor.Promotor = Utils.GetString(reader, "Promotor");
                                resultado.Add(promotor);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public void ActualizarTBCATPromotor()
        {
        }

        public void EliminarTBCATPromotor()
        {
        }

        //ModificarSolicitud-LP
        public List<Combo> ObtenerCATCombo(int IDSucursal)
        {
          List<Combo> _lstCATCombo = null;
          try
          {
            using (DbCommand com = db.GetStoredProcCommand("catalogo.sel_promotor"))
            {
              db.AddInParameter(com, "@Accion", DbType.Int32, 999);
              db.AddInParameter(com, "@idSucursal", DbType.Int32, IDSucursal);

              using (IDataReader reader = db.ExecuteReader(com))
              {
                if (reader != null)
                {
                  _lstCATCombo = new List<Combo>();
                  while (reader.Read())
                  {
                    Combo _CATCombo = new Combo();
                    _CATCombo.Valor = Convert.ToInt32(reader[0]);
                    _CATCombo.Descripcion = Convert.ToString(reader[1]);
                    _lstCATCombo.Add(_CATCombo);
                  }
                }
                reader.Dispose();
              }
              com.Dispose();
            }
          }
          catch (Exception ex)
          {
            throw ex;
          }
          return _lstCATCombo;
        }
        
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}