#pragma warning disable 160330
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.Entities;

# region Copyright Dimex – 2016
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATConvenioDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATConvenio
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2016-03-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Catalogo
{
    public class TBCATConvenioDal 
    {

      public class CATCombo
      {
        public Int32 Valor { get; set; }
        public string Descripcion { get; set; }
      }

		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCATConvenioDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBCATConvenio(TBCATConvenio entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<TBCATConvenio> ObtenerTBCATConvenio(int Accion, int idconvenio, string convenio, int idEstatus)
        {
			List<TBCATConvenio> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("SP_SelConvenio"))
				{
					//Parametros
					db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdConvenio", DbType.Int32, idconvenio);
                    db.AddInParameter(com, "@Convenio", DbType.String, convenio);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, idEstatus);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<TBCATConvenio>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATConvenio Convenio = new TBCATConvenio();
                                if (!reader.IsDBNull(0)) Convenio.IdConvenio = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Convenio.Convenio = reader[1].ToString();
                                if (!reader.IsDBNull(2)) Convenio.IdEstatus = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) Convenio.QuincenasEnCamino = Convert.ToInt32(reader[3]);
                                if (!reader.IsDBNull(4)) Convenio.IdTipoConvenio = Convert.ToInt32(reader[4]);
                                if (!reader.IsDBNull(5)) Convenio.EsEmpleado = Convert.ToInt32(reader[5]);
                                if (!reader.IsDBNull(6)) Convenio.EsAutoFeliz = Convert.ToInt32(reader[6]);
                                if (!reader.IsDBNull(7)) Convenio.IdEstatus = Convert.ToInt32(reader[7]);
                                //if (!reader.IsDBNull(8)) Convenio.IdTipoEstatus = Convert.ToInt32(reader[8]);
                                //if (!reader.IsDBNull(9)) Convenio.Estatus = reader[9].ToString();
                                //if (!reader.IsDBNull(10)) Convenio.Descripcion = reader[10].ToString();
                                //if (!reader.IsDBNull(11)) Convenio.Ayuda = reader[11].ToString();
                                resultado.Add(Convenio);
                            }

						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarTBCATConvenio()
        {
        }

        public void EliminarTBCATConvenio()
        {
        }
        #endregion
        
        #region MISC Methods
        public List<Combo> Sistema_ObtenerCATCombo(int IDTipoConvenio)
        {
          List<Combo> _lstCATCombo = null;
          try
          {
            using (DbCommand com = db.GetStoredProcCommand("SP_SelConvenio"))
            {
              db.AddInParameter(com, "@Accion", DbType.Int32, 998);
              db.AddInParameter(com, "@IdConvenio", DbType.Int32, IDTipoConvenio);
              db.AddInParameter(com, "@Convenio", DbType.String, "");
              db.AddInParameter(com, "@IdEstatus", DbType.Int32, 0);

              using (IDataReader reader = db.ExecuteReader(com))
              {
                if (reader != null)
                {
                  _lstCATCombo = new List<Combo>();
                  while (reader.Read())
                  {
                    Combo _CATCombo = new Combo();
                    _CATCombo.Valor = Convert.ToInt32(reader[0]);
                    _CATCombo.Descripcion = Convert.ToString(reader[1]);
                    _lstCATCombo.Add(_CATCombo);
                  }
                }
                reader.Dispose();
              }
              com.Dispose();
            }
          }
          catch (Exception ex)
          {
            throw ex;
          }
          return _lstCATCombo;
        }

        public List<Combo> ObtenerCATCombo(int IDTipoConvenio)
        {
            List<Combo> _lstCATCombo = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("SP_SelConvenio"))
                {
                    db.AddInParameter(com, "@Accion", DbType.Int32, 999);
                    db.AddInParameter(com, "@IdConvenio", DbType.Int32, IDTipoConvenio);
                    db.AddInParameter(com, "@Convenio", DbType.String, "");
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, 0);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            _lstCATCombo = new List<Combo>();
                            while (reader.Read())
                            {
                                Combo _CATCombo = new Combo();
                                _CATCombo.Valor = Convert.ToInt32(reader[0]);
                                _CATCombo.Descripcion = Convert.ToString(reader[1]);
                                _lstCATCombo.Add(_CATCombo);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _lstCATCombo;
        }
        #endregion

        #region Private Methods
        #endregion
    }
}