#pragma warning disable 160330
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Catalogo;

# region Copyright Dimex – 2016
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATProductoDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATProducto
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2016-03-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Catalogo
{
    public class TBCATProductoDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCATProductoDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBCATProducto(TBCATProducto entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<TBCATProducto> ObtenerTBCATProducto(int Accion, int idProducto, string Producto, int idEstatus, int TipoReno, int IdTipoCredito)
        {
			List<TBCATProducto> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelProducto"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdProducto", DbType.Int32, idProducto);
                    db.AddInParameter(com, "@Producto", DbType.String, Producto);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, idEstatus);
                    db.AddInParameter(com, "@TipoReno", DbType.Int32, TipoReno);
                    db.AddInParameter(com, "@IdTipoCredito", DbType.Int32, IdTipoCredito);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<TBCATProducto>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATProducto producto = new TBCATProducto();
                                if (!reader.IsDBNull(0)) producto.IdProducto = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) producto.Producto = reader[1].ToString();
                                if (!reader.IsDBNull(2)) producto.Tasa = Convert.ToDecimal(reader[2]);
                                if (!reader.IsDBNull(3)) producto.Tasaequivalente = Convert.ToDecimal(reader[3]);
                                if (!reader.IsDBNull(4)) producto.TasaMora = Convert.ToDecimal(reader[4]);
                                if (!reader.IsDBNull(5)) producto.Pagos = Convert.ToDecimal(reader[5]);
                                if (!reader.IsDBNull(6)) producto.Meses = Convert.ToDecimal(reader[6]);
                                if (!reader.IsDBNull(7)) producto.Frecuencia = reader[7].ToString();
                                if (!reader.IsDBNull(8)) producto.Iva = Convert.ToDecimal(reader[8]);
                                if (!reader.IsDBNull(9)) producto.IdEstatus = Convert.ToInt32(reader[9]);                         
                                
                                if (!reader.IsDBNull(12)) producto.Otrocargo = Convert.ToDecimal(reader[12]);
                                if (!reader.IsDBNull(13)) producto.Moneda = reader[13].ToString();
                                if (!reader.IsDBNull(14)) producto.IdConvenio = Convert.ToInt32(reader[14]);
                                resultado.Add(producto);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarTBCATProducto()
        {
        }

        public void EliminarTBCATProducto()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}