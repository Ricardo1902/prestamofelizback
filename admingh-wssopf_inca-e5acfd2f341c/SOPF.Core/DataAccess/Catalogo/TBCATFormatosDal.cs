﻿#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Catalogo;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATFormatos.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATFormatos
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess
{
    public class TBCATFormatosDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TBCATFormatosDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods


        public List<TBCATFormatos> ConsultaFormatos(int Accion, int idFormato, string Formato, int idestatus, int idsucursal)
        {
            List<TBCATFormatos> resultado = null;
            
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelFormatos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdFormato", DbType.Int32, idFormato);
                    db.AddInParameter(com, "@Formato", DbType.String, Formato);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, idestatus);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, idsucursal);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCATFormatos>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATFormatos formato = new TBCATFormatos();
                                if (!reader.IsDBNull(0)) formato.IdFormato = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) formato.Formato = Convert.ToString(reader[1]);
                                if (!reader.IsDBNull(2)) formato.Ruta = Convert.ToString(reader[2]);
                                if (!reader.IsDBNull(3)) formato.IdEstatus = Convert.ToInt32(reader[3]);
                                if (!reader.IsDBNull(4)) formato.TipoProducto = Convert.ToInt32(reader[4]);
                                if (!reader.IsDBNull(5)) formato.NombrePlantilla = Convert.ToString(reader[5]);
                                if (!reader.IsDBNull(6)) formato.NombreDescarga = Convert.ToString(reader[6]);

                                resultado.Add(formato);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        #endregion

        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}