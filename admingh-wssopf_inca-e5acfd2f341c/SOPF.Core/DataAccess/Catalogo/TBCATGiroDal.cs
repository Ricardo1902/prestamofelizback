#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATGiroDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATGiro
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess
{
    public class TBCATGiroDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCATGiroDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBCATGiro(TBCATGiro entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<TBCATGiro> ObtenerTBCATGiro(int Accion, int idGiro, string Giro, int idestatus)
        {
			List<TBCATGiro> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelGiro"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdGiro", DbType.Int32, idGiro);
                    db.AddInParameter(com, "@Giro", DbType.String, Giro);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, idestatus);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<TBCATGiro>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATGiro giro = new TBCATGiro();
                                if (!reader.IsDBNull(0)) giro.IdGiro = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) giro.Giro = reader[1].ToString();
                                if (!reader.IsDBNull(2)) giro.IdEstatus = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) giro.IdEstatus = Convert.ToInt32(reader[3]);
                                resultado.Add(giro);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarTBCATGiro()
        {
        }

        public void EliminarTBCATGiro()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}