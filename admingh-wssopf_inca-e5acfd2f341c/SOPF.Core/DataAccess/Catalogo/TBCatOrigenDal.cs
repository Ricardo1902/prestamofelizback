﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Entities.Catalogo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCatOrigenDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCatOrigen
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2020-12-07 	Celeste Rodriguez   	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Catalogo
{
    public class TBCatOrigenDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TBCatOrigenDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public List<TBCatOrigen> ObtenerTBCatOrigen(int Accion)
        {
            List<TBCatOrigen> resultado = new List<TBCatOrigen>();
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("[Catalogo].[SP_CatOrigenCON]"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCatOrigen origen = new TBCatOrigen();
                                if (!reader.IsDBNull(0)) origen.IdOrigen = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) origen.Origen = reader[1].ToString();
                                if (Accion == 1 && !reader.IsDBNull(2)) origen.MontoComision = Convert.ToDouble(reader[2]);
                                resultado.Add(origen);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
    }
}
