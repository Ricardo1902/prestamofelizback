﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Catalogo;

# region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatCNTDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCobranza
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Catalogo
{
    public class CatCNTDal
    {

        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public CatCNTDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public List<CatCNT> ObtenerCatCNT(int Accion,string Descripcion)
        {
            List<CatCNT> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                
                using (DbCommand com = db.GetStoredProcCommand("catalogo.SP_CatCNTCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@Descripcion", DbType.String, Descripcion);                  

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<CatCNT>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                CatCNT CNT = new CatCNT();
                                if (!reader.IsDBNull(0)) CNT.id= Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) CNT.descripcion = reader[1].ToString();
                                
                                resultado.Add(CNT);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
    }
}
