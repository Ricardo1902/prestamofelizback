#pragma warning disable 160817
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Catalogo;

# region Copyright Dimex – 2016
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCatCatalogosDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCatCatalogos
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2016-08-17 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Catalogo
{
    public class TBCatCatalogosDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCatCatalogosDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBCatCatalogos(TBCatCatalogos entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<TBCatCatalogos> ObtenerTBCatCatalogos(int Accion, int IdTipo, string valor, int idestatus, string Tipo)
        {
			List<TBCatCatalogos> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelCatCatalogos"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdTipo", DbType.Int32, IdTipo);
                    db.AddInParameter(com, "@Valor", DbType.String , valor);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, idestatus);
                    db.AddInParameter(com, "@Tipo", DbType.String, Tipo);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<TBCatCatalogos>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCatCatalogos Catalogo = new TBCatCatalogos();
                                if (!reader.IsDBNull(0)) Catalogo.Valor = reader[0].ToString();
                                if (!reader.IsDBNull(1)) Catalogo.Descripcion = reader[1].ToString();
                                if (!reader.IsDBNull(2)) Catalogo.EsDefault = Convert.ToInt32(reader[2]);
                                resultado.Add(Catalogo);
                            }


						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarTBCatCatalogos()
        {
        }

        public void EliminarTBCatCatalogos()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}