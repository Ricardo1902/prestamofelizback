﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Catalogo;

namespace SOPF.Core.DataAccess.Catalogo
{
    public class TbCatFrecuenciaDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TbCatFrecuenciaDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
        #region CRUD Methods


        public List<TbCatFrecuencia> ObtenerTBCatFrecuencia(int usuarioSwitch, string frecs)
        {
            List<TbCatFrecuencia> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelFrecuencia_MIDE"))
                {
                    //Parametros
                    db.AddInParameter(com, "@usuarioSwitch", DbType.Int32, usuarioSwitch);
                    db.AddInParameter(com, "@frecs", DbType.String, frecs);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TbCatFrecuencia>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TbCatFrecuencia frecuencia = new TbCatFrecuencia();
                                if (!reader.IsDBNull(0)) frecuencia.frecuencia = reader[0].ToString();
                                if (!reader.IsDBNull(1)) frecuencia.frecDescripcion = reader[1].ToString();
                                resultado.Add(frecuencia);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public void ActualizarTBCATBanco()
        {
        }

        public void EliminarTBCATBanco()
        {
        }
        #endregion

    }
}
