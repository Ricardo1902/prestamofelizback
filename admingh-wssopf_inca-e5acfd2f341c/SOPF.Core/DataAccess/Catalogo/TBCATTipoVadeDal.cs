#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Catalogo;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATTipoVadeDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATTipoVade
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Catalogo
{
    public class TBCATTipoVadeDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCATTipoVadeDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBCATTipoVade(TBCATTipoVade entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<TBCATTipoVade> ObtenerTBCATTipoVade(int Tipo)
        {
			List<TBCATTipoVade> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelTipoVade"))
				{
					//Parametros
                    db.AddInParameter(com, "@Tipo", DbType.Int32, Tipo);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<TBCATTipoVade>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATTipoVade tipoVade = new TBCATTipoVade();
                                if (!reader.IsDBNull(0)) tipoVade.IdTipoVade = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) tipoVade.TipoVade = reader[1].ToString();
                                if (!reader.IsDBNull(2)) tipoVade.Tipo = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) tipoVade.Orden = Convert.ToDecimal(reader[3]);
                                if (!reader.IsDBNull(4)) tipoVade.IdEstatus = Convert.ToInt32(reader[4]);
                                resultado.Add(tipoVade);
                            }

						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarTBCATTipoVade()
        {
        }

        public void EliminarTBCATTipoVade()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}