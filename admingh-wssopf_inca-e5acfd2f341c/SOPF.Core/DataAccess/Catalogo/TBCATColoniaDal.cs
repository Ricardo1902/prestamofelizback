#pragma warning disable 141030
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;
using SOPF.Core.Model.Catalogo;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TBCATColoniaDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATColonia
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-10-30 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess
{
    public class TBCATColoniaDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TBCATColoniaDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods
        public void InsertarTBCATColonia(TBCATColonia entidad)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
                {
                    //Parametros
                    //db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TBCATColonia> ObtenerTBCATColonia(int Accion, int Ciudad, int idColonia)
        {
            List<TBCATColonia> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Catalogo.SelColonia"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@idCiudad", DbType.Int32, Ciudad);
                    db.AddInParameter(com, "@idColonia", DbType.Int32, idColonia);
                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCATColonia>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATColonia Colonia = new TBCATColonia();
                                if (!reader.IsDBNull(0)) Colonia.IdColonia = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Colonia.Colonia = reader[1].ToString();
                                if (!reader.IsDBNull(2)) Colonia.IdCiudad = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) Colonia.Cp = Convert.ToInt32(reader[3].ToString());
                                if (!reader.IsDBNull(4)) Colonia.ZEc = reader[4].ToString();
                                if (!reader.IsDBNull(5)) Colonia.Lada = reader[5].ToString();
                                if (!reader.IsDBNull(6)) Colonia.IdEstatus = Convert.ToInt32(reader[6].ToString());
                                resultado.Add(Colonia);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public void ActualizarTBCATColonia()
        {
        }

        public void EliminarTBCATColonia()
        {
        }
        #endregion

        #region MISC Methods

        public DetalleColoniaVM ObtenerDetalleColonia(int accion, int idColonia)
        {
            DetalleColoniaVM detalleColonia = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("clientes.SP_DetalleColoniaCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, accion);
                    db.AddInParameter(com, "@IdColonia", DbType.Int32, idColonia);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                            detalleColonia = new DetalleColoniaVM();
                        //Lectura de los datos del ResultSet
                        while (reader.Read())
                        {
                            {
                                if (!reader.IsDBNull(0)) detalleColonia.IdEstado = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) detalleColonia.Estado = reader[1].ToString();
                                if (!reader.IsDBNull(2)) detalleColonia.IdCiudad = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) detalleColonia.Ciudad = reader[3].ToString();
                                if (!reader.IsDBNull(2)) detalleColonia.IdColonia = Convert.ToInt32(reader[4]);
                                if (!reader.IsDBNull(3)) detalleColonia.Colonia = reader[5].ToString();
                            }

                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return detalleColonia;
        }
        #endregion

        #region Private Methods
        #endregion
    }
}