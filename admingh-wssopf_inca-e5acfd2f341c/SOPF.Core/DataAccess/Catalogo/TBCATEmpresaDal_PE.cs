﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Catalogo;
using System.Data.Common;
using System.Data;

namespace SOPF.Core.DataAccess.Catalogo
{
    public class TBCATEmpresaDal_PE
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        public TBCATEmpresaDal_PE()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }

        #region CRUD Methods
        
        public List<Combo> ObtenerCATCombo()
        {
            List<Combo> _lstCATCombo = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Catalogo.SP_TBCATEmpresaCON")) { 
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            _lstCATCombo = new List<Combo>();
                            while (reader.Read())
                            {
                                Combo _CATCombo = new Combo();
                                _CATCombo.Valor = Convert.ToInt32(reader[0]);
                                _CATCombo.Descripcion = Convert.ToString(reader[1]);
                                _CATCombo.Tipo = Convert.ToInt32(reader[2]);
                                _lstCATCombo.Add(_CATCombo);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _lstCATCombo;
        }

        public TB_CATEmpresa Obtener(int IdEmpresa)
        {
            TB_CATEmpresa objReturn = null;

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Catalogo.SP_TBCAT_EmpresaOBT"))
                {
                    // Parametros
                    db.AddInParameter(com, "@IdEmpresa", DbType.Int32, IdEmpresa);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new TB_CATEmpresa();
                            while (reader.Read())
                            {
                                objReturn.IdEmpresa = Utils.GetInt(reader, "IdEmpresa");
                                objReturn.Empresa = Utils.GetString(reader, "Empresa");
                                objReturn.RUC = Utils.GetString(reader, "RUC");
                                objReturn.Activo = Utils.GetBool(reader, "Activo");
                                objReturn.IdUsuarioRegistro = Utils.GetInt(reader, "IdUsuarioRegistro");
                                objReturn.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro").GetValueOrDefault();
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        #endregion
    }
}
