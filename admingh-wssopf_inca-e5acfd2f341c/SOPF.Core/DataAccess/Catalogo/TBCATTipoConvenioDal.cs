#pragma warning disable 150412
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATTipoConvenioDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATTipoConvenio
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-12 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess
{
    public class TBCATTipoConvenioDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCATTipoConvenioDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBCATTipoConvenio(TBCATTipoConvenio entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<TBCATTipoConvenio> ObtenerTBCATTipoConvenio(int Accion, int idTipoConvenio, string convenio, int IdEstatus, int producto)
        {
			List<TBCATTipoConvenio> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelTipoConvenio"))
				{
					//Parametros
					db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdTipoConvenio", DbType.Int32, idTipoConvenio);
                    db.AddInParameter(com, "@Convenio", DbType.String, "");
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, 0);
                    db.AddInParameter(com, "@Producto", DbType.Int32, producto);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<TBCATTipoConvenio>();
                            while (reader.Read())
                            {
                                TBCATTipoConvenio TipoConvenio = new TBCATTipoConvenio();
                                if (!reader.IsDBNull(0)) TipoConvenio.IdTipoConvenio = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) TipoConvenio.TipoConvenio = reader[1].ToString();
                                if (!reader.IsDBNull(2)) TipoConvenio.Descripcion = reader[2].ToString();
                                if (!reader.IsDBNull(3)) TipoConvenio.TipoProducto = Convert.ToInt32(reader[3]);
                                resultado.Add(TipoConvenio);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarTBCATTipoConvenio()
        {
        }

        public void EliminarTBCATTipoConvenio()
        {
        }

        //ModificarSolicitud-LP
        public List<Combo> ObtenerCATCombo()
        {
          List<Combo> _lstCATCombo = null;
          try
          {
            using (DbCommand com = db.GetStoredProcCommand("SP_SelTipoConvenio"))
            {
              db.AddInParameter(com, "@Accion", DbType.Int32, 999);
              db.AddInParameter(com, "@IdTipoConvenio", DbType.Int32, 0);
              db.AddInParameter(com, "@Convenio", DbType.String, "");
              db.AddInParameter(com, "@IdEstatus", DbType.Int32, 0);
              db.AddInParameter(com, "@Producto", DbType.Int32, 0);

              using (IDataReader reader = db.ExecuteReader(com))
              {
                if (reader != null)
                {
                  _lstCATCombo = new List<Combo>();
                  while (reader.Read())
                  {
                    Combo _CATCombo = new Combo();
                    _CATCombo.Valor = Convert.ToInt32(reader[0]);
                    _CATCombo.Descripcion = Convert.ToString(reader[1]);
                    _lstCATCombo.Add(_CATCombo);
                  }
                }
                reader.Dispose();
              }
              com.Dispose();
            }
          }
          catch (Exception ex)
          {
            throw ex;
          }
          return _lstCATCombo;
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}