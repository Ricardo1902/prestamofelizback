﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Entities;
using System.Data.Common;
using System.Data;

namespace SOPF.Core.DataAccess.Catalogo
{
    public class TBCATOrganoPagoaDal_PE
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        public TBCATOrganoPagoaDal_PE()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }

        #region CRUD Methods
        public List<Combo> ObtenerCATCombo()
        {
            List<Combo> _lstCATCombo = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Catalogo.SP_TBCATOrganoPagoCON"))
                {
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            _lstCATCombo = new List<Combo>();
                            while (reader.Read())
                            {
                                Combo _CATCombo = new Combo();
                                _CATCombo.Valor = Convert.ToInt32(reader[0]);
                                _CATCombo.Descripcion = Convert.ToString(reader[1]);
                                _lstCATCombo.Add(_CATCombo);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _lstCATCombo;
        }
        #endregion
    }
}
