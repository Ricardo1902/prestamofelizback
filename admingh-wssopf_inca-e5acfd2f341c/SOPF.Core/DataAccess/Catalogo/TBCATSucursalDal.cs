﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Catalogo;

# region Informacion General
//
// Archivo: TBCATSucursal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATSucursal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Catalogo
{
    public class TBCATSucursalDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TBCATSucursalDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods

        public List<TBCATSucursal> ObtenerSucursal(TBCATSucursal entidad)
        {
            List<TBCATSucursal> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelSucursal"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, entidad.Accion);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, entidad.IdSucursal);
                    db.AddInParameter(com, "@Sucursal", DbType.String, entidad.Sucursal);
                    db.AddInParameter(com, "@IdEstatus", DbType.String, entidad.IdEstatus);
                    db.AddInParameter(com, "@IdAfiliado", DbType.String, entidad.IdAfiliado);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCATSucursal>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATSucursal sucursal = new TBCATSucursal();
                                if (!reader.IsDBNull(0)) sucursal.IdSucursal = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) sucursal.Sucursal = Convert.ToString(reader[1]);
                                if (!reader.IsDBNull(2)) sucursal.RFC = Convert.ToString(reader[2]);
                                if (!reader.IsDBNull(3)) sucursal.Gerente = Convert.ToString(reader[3]);
                                if (!reader.IsDBNull(4)) sucursal.Direccion = Convert.ToString(reader[4]);
                                if (!reader.IsDBNull(5)) sucursal.IdColonia = Convert.ToInt32(reader[5]);
                                if (!reader.IsDBNull(6)) sucursal.Colonia = Convert.ToString(reader[6]);
                                if (!reader.IsDBNull(7)) sucursal.Ciudad = Convert.ToString(reader[7]);
                                if (!reader.IsDBNull(8)) sucursal.Estado = Convert.ToString(reader[8]);
                                if (!reader.IsDBNull(9)) sucursal.lada = Convert.ToString(reader[9]);
                                if (!reader.IsDBNull(10)) sucursal.telefono = Convert.ToString(reader[10]);
                                if (!reader.IsDBNull(11)) sucursal.fax = Convert.ToString(reader[11]);
                                if (!reader.IsDBNull(12)) sucursal.porce_comi = Convert.ToDecimal(reader[12]);
                                if (!reader.IsDBNull(13)) sucursal.IdEstatus = Convert.ToInt32(reader[13]);
                                if (!reader.IsDBNull(14)) sucursal.IdEstado = Convert.ToInt32(reader[14]);
                                if (!reader.IsDBNull(15)) sucursal.Estatus = Convert.ToString(reader[15]);
                                if (!reader.IsDBNull(16)) sucursal.descripcion = Convert.ToString(reader[16]);
                                if (!reader.IsDBNull(17)) sucursal.IdAfiliado = Convert.ToInt32(reader[17]);
                                if (!reader.IsDBNull(18)) sucursal.Afiliado = Convert.ToString(reader[18]);
                                if (!reader.IsDBNull(19)) sucursal.cp = Convert.ToInt32(reader[19]);
                                if (!reader.IsDBNull(20)) sucursal.cve_sucursal = Convert.ToString(reader[20]);
                                if (!reader.IsDBNull(21)) sucursal.RegionalId = Convert.ToInt32(reader[21]);

                                resultado.Add(sucursal);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return resultado;
        }

        //ModificarSolicitud-LP
        public List<Combo> ObtenerCATCombo(int IDUsuarioRegional)
        {
          List<Combo> _lstCATCombo = null;
          try
          {
            using (DbCommand com = db.GetStoredProcCommand("SP_SelSucursal"))
            {
              db.AddInParameter(com, "@Accion", DbType.Int32, 998);
              db.AddInParameter(com, "@IdSucursal", DbType.Int32, IDUsuarioRegional);
              db.AddInParameter(com, "@Sucursal", DbType.String, "");
              db.AddInParameter(com, "@IdEstatus", DbType.String, "");
              db.AddInParameter(com, "@IdAfiliado", DbType.String, "");
        
              using (IDataReader reader = db.ExecuteReader(com))
              {
                if (reader != null)
                {
                  _lstCATCombo = new List<Combo>();
                  while (reader.Read())
                  {
                    Combo _CATCombo = new Combo();
                    _CATCombo.Valor = Convert.ToInt32(reader[0]);
                    _CATCombo.Descripcion = Convert.ToString(reader[1]);
                    _lstCATCombo.Add(_CATCombo);
                  }
                }
                reader.Dispose();
              }
              com.Dispose();
            }
          }
          catch (Exception ex)
          {
            throw ex;
          }
          return _lstCATCombo;
        }        
        #endregion

    }
}
