﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Catalogo;
using SOPF.Core.Entities;


namespace SOPF.Core.DataAccess
{
    public class TBCATTipoCreditoDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        public TBCATTipoCreditoDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}

        public List<TBCATTipoCredito> ObtenerTBCATTipoCredito(int idCliente)
        {
            List<TBCATTipoCredito> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("solicitudes.Sel_TBCatTipoCredito"))
                {
                    //Parametros
                    db.AddInParameter(com, "@idCliente", DbType.Int32, idCliente);
                    

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCATTipoCredito>();
                            while (reader.Read())
                            {
                                TBCATTipoCredito TipoCredito = new TBCATTipoCredito();
                                if (!reader.IsDBNull(0)) TipoCredito.idTipoCredito = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) TipoCredito.TipoCredito = reader[1].ToString();

                                resultado.Add(TipoCredito);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
    }
}
