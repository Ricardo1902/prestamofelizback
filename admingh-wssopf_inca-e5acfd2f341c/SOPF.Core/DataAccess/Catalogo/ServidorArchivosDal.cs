﻿using SOPF.Core.Entities;
using SOPF.Core.Model.Catalogo;
using SOPF.Core.Poco.Catalogo;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace SOPF.Core.DataAccess.Catalogo
{
    public class ServidorArchivosDal
    {
        public CreditOrigContext con { get; set; }
        public ServidorArchivosDal(CreditOrigContext con)
        {
            this.con = con;
        }

        public ServidorArchivosVM ObtenerServidorArchivosPorId(int idServidorArchivos)
        {
            ServidorArchivosVM servidor = null;
            try
            {
                if (idServidorArchivos <= 0) throw new Exception("La petición es inválida");
                var encontrado = ConsultaServidorArchivosVM(sa => sa.IdServidorArchivos == idServidorArchivos).FirstOrDefault();
                if (encontrado != null)
                    servidor = encontrado;
            }
            catch (Exception)
            {
            }

            return servidor;
        }
        public List<ServidorArchivosVM> ObtenerServidoresArchivos()
        {
            List<ServidorArchivosVM> servidores = new List<ServidorArchivosVM>();
            try
            {
                var encontrado = ConsultaServidorArchivosVM(sa => true).ToList();
                if (encontrado != null)
                    servidores = encontrado;
            }
            catch (Exception ex)
            {
            }

            return servidores;
        }
        public Resultado<bool> InsertarServidorArchivos(ServidorArchivos archivoGestion)
        {
            if (archivoGestion == null) throw new Exception("La petición es inválida");
            Resultado<bool> respuesta = new Resultado<bool>();
            using (var tran = con.Database.BeginTransaction())
            {
                try
                {
                    //using (con)
                    //{
                    con.Catalogo_ServidorArchivos.Add(archivoGestion);
                    con.SaveChanges();

                    respuesta.Codigo = 1;
                    respuesta.Mensaje = "El archivo se inserto con exito";
                    respuesta.ResultObject = true;

                    tran.Commit();
                    //}
                }
                catch (Exception)
                {
                    tran.Rollback();
                    respuesta.Codigo = 2;
                    respuesta.Mensaje = "Ha ocurrido un error al insertar el archivo.";
                    respuesta.ResultObject = false;
                }
            }
            return respuesta;
        }
        public Resultado<bool> ActualizarServidorArchivos(ServidorArchivos archivoGestion)
        {
            if (archivoGestion == null) throw new Exception("La petición es inválida");
            if (archivoGestion.IdServidorArchivos <= 0) throw new Exception("La clave del archivo es incorrecta.");
            Resultado<bool> respuesta = new Resultado<bool>();
            using (var tran = con.Database.BeginTransaction())
            {
                try
                {
                    //using (con)
                    //{
                    var registro = ConsultaServidorArchivos(sa => sa.IdServidorArchivos == archivoGestion.IdServidorArchivos).FirstOrDefault();
                    if (registro != null)
                    {
                        registro.Nombre = archivoGestion.Nombre;

                        con.SaveChanges();

                        respuesta.Codigo = 1;
                        respuesta.Mensaje = "El archivo se actualizó con éxito";
                        respuesta.ResultObject = true;

                        tran.Commit();
                    }
                    //}
                }
                catch (Exception)
                {
                    tran.Rollback();
                }
            }
            return respuesta;
        }
        public Resultado<bool> EliminarServidorArchivos(int idServidorArchivos)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            try
            {
                if (idServidorArchivos <= 0) throw new Exception("Petición inválida. La clave del servidor de archivos es incorrecta.");
                {
                    var referencia = con.Catalogo_ServidorArchivos.SingleOrDefault(sa => sa.IdServidorArchivos == idServidorArchivos);
                    if (referencia != null && referencia.IdServidorArchivos > 0)
                    {
                        ServidorArchivos registro = referencia;
                        con.Catalogo_ServidorArchivos.Attach(registro);
                        con.Catalogo_ServidorArchivos.Remove(registro);
                        con.Entry(registro).State = EntityState.Deleted;

                        respuesta.Codigo = 1;
                        respuesta.ResultObject = true;
                        respuesta.Mensaje = "El servidor de archivos se eliminó con éxito.";

                        con.SaveChanges();

                    }

                }

            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.ResultObject = false;
                respuesta.Mensaje = ex.Message;
            }
            return respuesta;
        }

        private IQueryable<ServidorArchivos> ConsultaServidorArchivos(Expression<Func<ServidorArchivos, bool>> predicado)
        {
            var query = (from sa in con.Catalogo_ServidorArchivos
                         select sa).Where(predicado);

            return query;
        }
        private IQueryable<ServidorArchivosVM> ConsultaServidorArchivosVM(Expression<Func<ServidorArchivosVM, bool>> predicado)
        {
            var query = (from sa in con.Catalogo_ServidorArchivos
                         select new ServidorArchivosVM
                         {
                             IdServidorArchivos = sa.IdServidorArchivos,
                             Nombre = sa.Nombre

                         }).Where(predicado);

            return query;
        }
    }
}
