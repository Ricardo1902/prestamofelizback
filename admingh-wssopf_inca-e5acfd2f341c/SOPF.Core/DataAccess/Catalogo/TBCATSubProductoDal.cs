﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Entities.Catalogo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.DataAccess.Catalogo
{
    public class TBCATSubProductoDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TBCATSubProductoDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public List<TBCATSubProducto> ObtenerSubProductos(TBCATSubProducto entidad)
        {
            List<TBCATSubProducto> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_CATSubProductoCON"))
                {
                    //Parametros
                    if (entidad.IdSubProducto > 0)
                    {
                        db.AddInParameter(com, "@IdSubProducto", DbType.Int32, entidad.IdSubProducto);
                    }
                    if (!String.IsNullOrEmpty(entidad.SubProducto))
                    {
                        db.AddInParameter(com, "@SubProducto", DbType.String, entidad.SubProducto);
                    }

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCATSubProducto>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATSubProducto subProducto = new TBCATSubProducto();
                                subProducto.IdSubProducto = Utils.GetInt(reader, "IdSubProducto");
                                subProducto.Clave = Utils.GetString(reader, "Clave");
                                subProducto.SubProducto = Utils.GetString(reader, "SubProducto");
                                subProducto.Activo = Utils.GetBool(reader, "Activo");
                                subProducto.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro");

                                resultado.Add(subProducto);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resultado;
        }

        public List<TBCATSubProducto> ObtenerSubProductosPorConvenio(int IdConvenio)
        {
            List<TBCATSubProducto> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_SubProductoConvenioCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdConvenio", DbType.Int32, IdConvenio);                                        

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCATSubProducto>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATSubProducto subProducto = new TBCATSubProducto();
                                subProducto.IdSubProducto = Utils.GetInt(reader, "IdSubProducto");
                                subProducto.Clave = Utils.GetString(reader, "Clave");
                                subProducto.SubProducto = Utils.GetString(reader, "SubProducto");
                                subProducto.Activo = Utils.GetBool(reader, "Activo");
                                subProducto.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro");

                                resultado.Add(subProducto);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resultado;
        }
    }
}
