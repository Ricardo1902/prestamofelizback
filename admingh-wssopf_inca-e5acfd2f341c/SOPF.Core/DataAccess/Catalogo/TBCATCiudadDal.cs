#pragma warning disable 141030
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATCiudadDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATCiudad
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-10-30 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess
{
    public class TBCATCiudadDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCATCiudadDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBCATCiudad(TBCATCiudad entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }

        public List<TBCATCiudad> ObtenerTBCATCiudad(int accion, int idCiudad, string ciudad, int idEstatus, int idEstado)
        {
            List<TBCATCiudad> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelCiudad"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, accion);
                    db.AddInParameter(com, "@IdCiudad", DbType.Int32, idCiudad);
                    db.AddInParameter(com, "@Ciudad", DbType.String, ciudad);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, idEstatus);
                    db.AddInParameter(com, "@IdEstado", DbType.Int32, idEstado);
				
					//Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCATCiudad>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATCiudad Ciudad = new TBCATCiudad();
                                if (!reader.IsDBNull(0)) Ciudad.IdCiudad = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Ciudad.Ciudad = reader[1].ToString();
                                if (!reader.IsDBNull(2)) Ciudad.IdEstado = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) Ciudad.IdEstatus = Convert.ToInt32(reader[3]);
                                resultado.Add(Ciudad);
                            }
                        }

                        reader.Dispose();
                    }

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        public List<TBCATCiudad> ObtenerTBCATCiudadOrig(int accion, int idEstado, int idCiudad )
        {
            List<TBCATCiudad> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand( "catalogo.SelCiudad"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, accion);
                    db.AddInParameter(com, "@IdCiudad", DbType.Int32, idCiudad);                    
                    db.AddInParameter(com, "@IdEstado", DbType.Int32, idEstado);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCATCiudad>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATCiudad Ciudad = new TBCATCiudad();
                                if (!reader.IsDBNull(0)) Ciudad.IdCiudad = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Ciudad.Ciudad = reader[1].ToString();
                                if (!reader.IsDBNull(2)) Ciudad.IdEstado = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) Ciudad.IdEstatus = Convert.ToInt32(reader[3]);
                                resultado.Add(Ciudad);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        
        public void ActualizarTBCATCiudad()
        {
        }

        public void EliminarTBCATCiudad()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}