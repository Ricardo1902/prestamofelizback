﻿#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Catalogo;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATFormatos.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATFormatos
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess
{
    public class TBCATCampaniasDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TBCATCampaniasDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods


        public List<TBCATCampanias> ConsultaCampanias(int Accion, int idCampania, string Campania, int idestatus)
        {
            List<TBCATCampanias> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("catalogo.SP_SelCampanias"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdCampania", DbType.Int32, idCampania);
                    db.AddInParameter(com, "@Campania", DbType.String, Campania);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, idestatus);
                    

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCATCampanias>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATCampanias Campanias = new TBCATCampanias();
                                if (!reader.IsDBNull(0)) Campanias.IdCampania = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Campanias.Campania = Convert.ToString(reader[1]);
                                if (!reader.IsDBNull(2)) Campanias.TablaRelacion = Convert.ToString(reader[2]);
                                if (!reader.IsDBNull(3)) Campanias.IdEstatus = Convert.ToInt32(reader[3]);

                                resultado.Add(Campanias);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        #endregion

        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
