﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Entities;
using SOPF.Core.Model.Clientes;
using SOPF.Core.Poco.Clientes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using static SOPF.Core.Entities.Clientes.TBClientes;

namespace SOPF.Core.DataAccess.Clientes
{
    public class ReferenciaDal
    {
        Microsoft.Practices.EnterpriseLibrary.Data.Database db;
        private CreditOrigContext con { get; set; }
        public ReferenciaDal(CreditOrigContext con)
        {
            this.con = con;
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }

        public List<ClientesReferencia> ObtenerReferenciasCliente(int idCliente)
        {
            List<ClientesReferencia> referencias = new List<ClientesReferencia>();
            try
            {
                //Obtener solo las referencias activas
                var encontrados = (from r in con.Clientes_Referencia
                                   join p in con.Catalogo_TB_CATTipoVade on r.IdParentesco equals p.IdTipoVade
                                   where r.IdCliente == idCliente && r.Activo == true
                                   select new ClientesReferenciaVM
                                   {
                                       IdReferencia = r.IdReferencia,
                                       IdCliente = r.IdCliente,
                                       Nombres = r.Nombres,
                                       ApellidoPaterno = r.ApellidoPaterno,
                                       ApellidoMaterno = r.ApellidoMaterno,
                                       IdParentesco = r.IdParentesco,
                                       Parentesco = p.TipoVade,
                                       Calle = r.Calle,
                                       NumeroExterior = r.NumeroExterior,
                                       NumeroInterior = r.NumeroInterior,
                                       IdColonia = r.IdColonia,
                                       Lada = r.Lada,
                                       Telefono = r.Telefono,
                                       LadaCelular = r.LadaCelular,
                                       TelefonoCelular = r.TelefonoCelular,
                                       TiempoConocerlo = r.TiempoConocerlo,
                                       FechaRegistro = r.FechaRegistro,
                                       Activo = r.Activo
                                   }).ToList();

                if (referencias != null)
                {
                    foreach (var vm in encontrados)
                    {
                        referencias.Add(vm.ToClientesReferencia());
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return referencias;
        }
        public List<ClientesReferencia> ObtenerUltimasReferenciasCliente(int idCliente)
        {
            List<ClientesReferencia> ultimasReferencias = null;
            try
            {
                var encontradas = con.Clientes_Referencia.Where(r => r.IdCliente == idCliente && r.Activo == true).OrderByDescending(r => r.IdReferencia).Take(4);
                if (encontradas != null)
                {
                    ultimasReferencias = encontradas.ToList();
                }
            }
            catch (Exception)
            {
            }
            return ultimasReferencias;
        }
        public Resultado<int> InsertarReferencia(ClientesReferencia referencia)
        {
            Resultado<int> respuesta = new Resultado<int>();
            using (DbContextTransaction tran = con.Database.BeginTransaction())
            {
                try
                {
                    if (referencia == null) throw new Exception("Petición inválida.");
                    if (referencia.Validar())
                    {
                        referencia.Activo = true; //Asignar como activo
                        con.Clientes_Referencia.Add(referencia);
                        con.SaveChanges();

                        respuesta.Codigo = 1;
                        respuesta.Mensaje = "La referencia se insertó con éxito.";
                        respuesta.ResultObject = referencia.IdReferencia;

                        tran.Commit();
                    }
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    respuesta.Codigo = 0;
                    respuesta.Mensaje = ex.Message;
                    respuesta.ResultObject = 0;
                }
            }

            return respuesta;
        }
        public Resultado<ClientesReferencia> ActualizarReferencia(ClientesReferencia referencia)
        {
            Resultado<ClientesReferencia> respuesta = new Resultado<ClientesReferencia>();
            using (DbContextTransaction tran = con.Database.BeginTransaction())
            {
                try
                {
                    if (referencia == null) throw new Exception("Peticion inválida.");
                    if (referencia.ValidarClave())
                    {
                        var encontrada = con.Clientes_Referencia.SingleOrDefault(r => r.IdReferencia == referencia.IdReferencia);
                        if (encontrada != null && encontrada.IdReferencia > 0)
                        {
                            if (!encontrada.Activo) throw new Exception("La referencia es inválida.");

                            if (encontrada.CompareTo(referencia) > 0 && referencia.Validar())
                            {
                                var nuevaReferencia = new ClientesReferencia
                                {
                                    IdCliente = referencia.IdCliente,
                                    Nombres = referencia.Nombres,
                                    ApellidoPaterno = referencia.ApellidoPaterno,
                                    ApellidoMaterno = referencia.ApellidoMaterno,
                                    IdParentesco = referencia.IdParentesco,
                                    Calle = referencia.Calle,
                                    NumeroExterior = referencia.NumeroExterior,
                                    NumeroInterior = referencia.NumeroInterior,
                                    IdColonia = referencia.IdColonia,
                                    Lada = referencia.Lada,
                                    Telefono = referencia.Telefono,
                                    LadaCelular = referencia.LadaCelular,
                                    TelefonoCelular = referencia.TelefonoCelular,
                                    TiempoConocerlo = referencia.TiempoConocerlo,
                                    Activo = true
                                };
                                // Se inactiva la referencia actual, para dejar un historial
                                encontrada.Activo = false;
                                con.SaveChanges();

                                // Se agrega un nuevo registro con la información actualizada
                                con.Clientes_Referencia.Add(nuevaReferencia);
                                con.SaveChanges();

                            }

                            respuesta.Codigo = 1;
                            respuesta.ResultObject = referencia;
                            respuesta.Mensaje = "La referencia se actualizó con éxito.";

                            tran.Commit();
                        }

                    }
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    respuesta.Codigo = 0;
                    respuesta.Mensaje = ex.Message;
                }
                return respuesta;
            }
        }
        public Resultado<bool> EliminarReferencia(int idReferencia, int idCliente)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            using (DbContextTransaction tran = con.Database.BeginTransaction())
            {
                try
                {
                    if (idReferencia <= 0) throw new Exception("Peticion inválida. La clave de la referencia es inválida.");
                    var referencia = con.Clientes_Referencia.SingleOrDefault(r => r.IdReferencia == idReferencia && r.IdCliente == idCliente);
                    if (referencia != null && referencia.IdReferencia > 0)
                    {
                        if (referencia.Activo)
                        {
                            //Delete logico
                            referencia.Activo = false;
                            con.SaveChanges();

                            respuesta.Codigo = 1;
                            respuesta.ResultObject = true;
                            respuesta.Mensaje = "La referencia se eliminó con éxito.";

                            tran.Commit();
                        }
                    }

                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    respuesta.Codigo = 0;
                    respuesta.ResultObject = false;
                    respuesta.Mensaje = ex.Message;
                }
                return respuesta;
            }
        }
        public Resultado<int> ReactivarReferenia(int idReferencia, int idCliente)
        {
            Resultado<int> respuesta = new Resultado<int>();
            using (DbContextTransaction tran = con.Database.BeginTransaction())
            {
                try
                {
                    if (idReferencia <= 0) throw new Exception("La clave de referenica es inválida");
                    if (idCliente <= 0) throw new Exception("La clave del cliente es inválido");

                    var referencia = con.Clientes_Referencia.SingleOrDefault(r => r.IdReferencia == idReferencia && r.IdCliente == idCliente);
                    if (referencia != null && referencia.IdReferencia > 0)
                    {
                        //Se revierte delete logico
                        referencia.Activo = true;
                        con.SaveChanges();

                        respuesta.Codigo = 1;
                        respuesta.ResultObject = referencia.IdReferencia;
                        respuesta.Mensaje = "La referencia se reactivó.";

                        tran.Commit();
                    }
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    respuesta.Codigo = 0;
                    respuesta.Mensaje = ex.Message;
                }
            }
            return respuesta;
        }
        public Resultado<bool> EliminarReferenciaPermanente(int idReferencia, int idCliente)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            try
            {
                if (idReferencia != 0 && idCliente != 0)
                {
                    var referencia = con.Clientes_Referencia.SingleOrDefault(r => r.IdReferencia == idReferencia && r.IdCliente == idCliente);
                    if (referencia != null && referencia.IdReferencia > 0)
                    {
                        ClientesReferencia registro = referencia;
                        con.Clientes_Referencia.Attach(registro);
                        con.Clientes_Referencia.Remove(registro);
                        con.Entry(registro).State = EntityState.Deleted;

                        respuesta.Codigo = 1;
                        respuesta.ResultObject = true;
                        respuesta.Mensaje = "La referencia se eliminó con éxito.";

                        con.SaveChanges();

                    }

                }

            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.ResultObject = false;
                respuesta.Mensaje = ex.Message;
            }
            return respuesta;
        }
        public Resultado<bool> MigrarReferencias()
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            try
            {
                var countReferencias = (from r in con.Clientes_Referencia
                                        select r).Count();

                if (countReferencias == 0)
                {
                    List<ReferenciaCliente> referencias = ObtenerReferenciasTBClientes();
                    List<ClientesReferencia> referenciasCorregidas = null;

                    if (referencias != null && referencias.Count > 0)
                    {
                        referenciasCorregidas = referencias.ToListClientesReferencia();

                        if (referenciasCorregidas != null && referenciasCorregidas.Count > 0)
                        {
                            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["INCA_NUEVO"].ConnectionString))
                            {
                                connection.Open();
                                using (SqlTransaction tran = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                                {
                                    using (SqlBulkCopy bcopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, tran))
                                    {
                                        bcopy.BatchSize = 1000;
                                        bcopy.NotifyAfter = 500;
                                        bcopy.SqlRowsCopied += (sender, EventArgs) =>
                                        {
                                            Console.WriteLine(EventArgs.RowsCopied);
                                        };

                                        bcopy.DestinationTableName = "clientes.Referencia";
                                        bcopy.ColumnMappings.Add("IdCliente", "IdCliente");
                                        bcopy.ColumnMappings.Add("Nombres", "Nombres");
                                        bcopy.ColumnMappings.Add("ApellidoPaterno", "ApellidoPaterno");
                                        bcopy.ColumnMappings.Add("ApellidoMaterno", "ApellidoMaterno");
                                        bcopy.ColumnMappings.Add("IdParentesco", "IdParentesco");
                                        bcopy.ColumnMappings.Add("Calle", "Calle");
                                        bcopy.ColumnMappings.Add("NumeroExterior", "NumeroExterior");
                                        bcopy.ColumnMappings.Add("NumeroInterior", "NumeroInterior");
                                        bcopy.ColumnMappings.Add("IdColonia", "IdColonia");
                                        bcopy.ColumnMappings.Add("Lada", "Lada");
                                        bcopy.ColumnMappings.Add("Telefono", "Telefono");
                                        bcopy.ColumnMappings.Add("LadaCelular", "LadaCelular");
                                        bcopy.ColumnMappings.Add("TiempoConocerlo", "TiempoConocerlo");

                                        try
                                        {
                                            bcopy.WriteToServer(referenciasCorregidas.AsDataTable());
                                        }
                                        catch (Exception ex)
                                        {
                                            tran.Rollback();
                                            connection.Close();
                                        }
                                    }
                                    tran.Commit();


                                }
                            }
                        }

                        respuesta.Codigo = 1;
                        respuesta.ResultObject = true;
                        respuesta.Mensaje = "La migración se realizó con exito";
                    }
                }
                else
                {
                    respuesta.Codigo = 0;
                    respuesta.ResultObject = false;
                    respuesta.Mensaje = "La operacion solo se puede realizar una primera vez (si la tabla final no contiene registros).";
                }
            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.ResultObject = false;
                respuesta.Mensaje = ex.Message;
            }

            return respuesta;
        }
        private List<ReferenciaCliente> ObtenerReferenciasTBClientes(int accion = 0, int? idCliente = null)
        {
            //Obtener DbCommand para ejcutar el Store Procedure
            List<ReferenciaCliente> referencias = new List<ReferenciaCliente>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_ReferenciasClienteCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion ", DbType.Int32, accion);
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, idCliente);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                ReferenciaCliente referencia = new ReferenciaCliente();
                                if (accion > 0)
                                {
                                    if (!reader.IsDBNull(0)) referencia.IdCliente = Convert.ToInt32(reader["IdCliente"]);
                                    if (!reader.IsDBNull(1)) referencia.Referencia = reader["referencia" + accion].ToString();
                                    if (!reader.IsDBNull(2)) referencia.IdParentesco = Convert.ToInt32(reader["IdParentesco" + accion]);
                                    if (!reader.IsDBNull(3)) referencia.Direccion = reader["dire_ref" + accion].ToString();
                                    if (!reader.IsDBNull(4)) referencia.IdColonia = Convert.ToInt32(reader["IdCo_ref" + accion]);
                                    if (!reader.IsDBNull(5)) referencia.Lada = reader["lada_ref" + accion].ToString();
                                    if (!reader.IsDBNull(6)) referencia.Telefono = reader["tele_ref" + accion].ToString();
                                    if (!reader.IsDBNull(7)) referencia.Celular = reader["cel_ref" + accion].ToString();
                                    referencias.Add(referencia);
                                }
                                else
                                {
                                    //TODO: Agregar todos los demas campos
                                    int tempIdCliente = 0;
                                    if (!reader.IsDBNull(0)) tempIdCliente = Convert.ToInt32(reader["IdCliente"]);

                                    referencia.IdCliente = tempIdCliente;
                                    if (!reader.IsDBNull(1)) referencia.Referencia = reader["referencia1"].ToString();
                                    if (!reader.IsDBNull(2)) referencia.IdParentesco = Convert.ToInt32(reader["IdParentesco1"]);
                                    if (!reader.IsDBNull(3)) referencia.Direccion = reader["dire_ref1"].ToString();
                                    if (!reader.IsDBNull(4)) referencia.IdColonia = Convert.ToInt32(reader["IdCo_ref1"]);
                                    if (!reader.IsDBNull(6)) referencia.Telefono = reader["tele_ref1"].ToString();
                                    if (!reader.IsDBNull(7)) referencia.Celular = reader["cel_ref1"].ToString();
                                    referencias.Add(referencia);

                                    referencia = new ReferenciaCliente();
                                    referencia.IdCliente = tempIdCliente;
                                    if (!reader.IsDBNull(8)) referencia.Referencia = reader["referencia2"].ToString();
                                    if (!reader.IsDBNull(9)) referencia.IdParentesco = Convert.ToInt32(reader["IdParentesco2"]);
                                    if (!reader.IsDBNull(10)) referencia.Direccion = reader["dire_ref2"].ToString();
                                    if (!reader.IsDBNull(11)) referencia.IdColonia = Convert.ToInt32(reader["IdCo_ref2"]);
                                    if (!reader.IsDBNull(12)) referencia.Lada = reader["lada_ref2"].ToString();
                                    if (!reader.IsDBNull(13)) referencia.Telefono = reader["tele_ref2"].ToString();
                                    if (!reader.IsDBNull(14)) referencia.Celular = reader["cel_ref2"].ToString();
                                    referencias.Add(referencia);

                                    referencia = new ReferenciaCliente();
                                    referencia.IdCliente = tempIdCliente;
                                    if (!reader.IsDBNull(15)) referencia.Referencia = reader["referencia3"].ToString();
                                    if (!reader.IsDBNull(16)) referencia.IdParentesco = Convert.ToInt32(reader["IdParentesco3"]);
                                    if (!reader.IsDBNull(17)) referencia.Direccion = reader["dire_ref3"].ToString();
                                    if (!reader.IsDBNull(18)) referencia.IdColonia = Convert.ToInt32(reader["IdCo_ref3"]);
                                    if (!reader.IsDBNull(19)) referencia.Lada = reader["lada_ref3"].ToString();
                                    if (!reader.IsDBNull(20)) referencia.Telefono = reader["tele_ref3"].ToString();
                                    if (!reader.IsDBNull(21)) referencia.Celular = reader["cel_ref3"].ToString();
                                    referencias.Add(referencia);

                                    referencia = new ReferenciaCliente();
                                    referencia.IdCliente = tempIdCliente;
                                    if (!reader.IsDBNull(22)) referencia.Referencia = reader["referencia4"].ToString();
                                    if (!reader.IsDBNull(23)) referencia.IdParentesco = Convert.ToInt32(reader["IdParentesco4"]);
                                    if (!reader.IsDBNull(24)) referencia.Direccion = reader["dire_ref4"].ToString();
                                    if (!reader.IsDBNull(25)) referencia.IdColonia = Convert.ToInt32(reader["IdCo_ref4"]);
                                    if (!reader.IsDBNull(26)) referencia.Lada = reader["lada_ref4"].ToString();
                                    if (!reader.IsDBNull(27)) referencia.Telefono = reader["tele_ref4"].ToString();
                                    if (!reader.IsDBNull(28)) referencia.Celular = reader["cel_ref4"].ToString();
                                    referencias.Add(referencia);
                                }
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return referencias;
        }

    }
}
