#pragma warning disable 141030
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Clientes;
using SOPF.Core.Poco.Clientes;
using SOPF.Core.Model.Response.Catalogo;
using SOPF.Core.Model.Request.Catalogo;
using System.Data.SqlClient;

#region Copyright
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TBClientesDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBClientes
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-10-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Clientes
{
    public class TBClientesDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TBClientesDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods
        public Resultado<bool> InsertarTBClientes(TBClientes entidad)
        {
            Resultado<bool> resultado = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("sp_ABCCliente"))
                {
                    int Accion = 0;
                    if (entidad.IdCliente == 0)
                    {
                        Accion = 0;
                    }
                    else
                    {
                        Accion = 1;
                    }
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion); 		
                    db.AddInParameter(com, "@IdCliente", DbType.Decimal, entidad.IdCliente);
                    db.AddInParameter(com, "@Nombres", DbType.String, entidad.Nombres);
                    db.AddInParameter(com, "@ApellidoP", DbType.String, entidad.Apellidop);
                    db.AddInParameter(com, "@ApellidoM", DbType.String, entidad.Apellidom);
                    db.AddInParameter(com, "@fch_Nacimiento", DbType.String, entidad.FchNacimiento);
                    db.AddInParameter(com, "@rfc", DbType.String, entidad.Rfc);
                    db.AddInParameter(com, "@direccion", DbType.String, entidad.Direccion);
                    db.AddInParameter(com, "@IdColonia", DbType.Decimal, entidad.IdColonia);
                    db.AddInParameter(com, "@lada", DbType.String, entidad.Lada);
                    db.AddInParameter(com, "@telefono", DbType.String, entidad.Telefono);
                    db.AddInParameter(com, "@antig_dom", DbType.Double, entidad.AntigDom);
                    db.AddInParameter(com, "@num_emplea", DbType.String, entidad.NumEmplea);
                    db.AddInParameter(com, "@dire_ofic", DbType.String, entidad.DireOfic);
                    db.AddInParameter(com, "@IdCo_ofic", DbType.Decimal, entidad.IdCoOfic);
                    db.AddInParameter(com, "@DependenciaOfic", DbType.String, entidad.DependenciaOfic);
                    db.AddInParameter(com, "@UbicacionOfic", DbType.String, entidad.UbicacionOfic);
                    db.AddInParameter(com, "@lada_ofic", DbType.String, entidad.LadaOfic);
                    db.AddInParameter(com, "@tele_ofic", DbType.String, entidad.TeleOfic);
                    db.AddInParameter(com, "@exte_ofic", DbType.String, entidad.ExteOfic);
                    db.AddInParameter(com, "@antig_ofic", DbType.Double, entidad.AntigOfic);
                    db.AddInParameter(com, "@puesto", DbType.String, entidad.Puesto);
                    db.AddInParameter(com, "@IngresoBruto", DbType.Decimal, entidad.IngresoBruto);
                    db.AddInParameter(com, "@ingreso", DbType.Decimal, entidad.Ingreso);
                    db.AddInParameter(com, "@otros_ingr", DbType.Decimal, entidad.OtrosIngr);
                    db.AddInParameter(com, "@fuen_otroi", DbType.String, entidad.FuenOtroi);
                    db.AddInParameter(com, "@DescuentosLey", DbType.Decimal, entidad.DescuentosLey);
                    db.AddInParameter(com, "@OtrosDescuentos", DbType.Decimal, entidad.OtrosDescuentos);
                    db.AddInParameter(com, "@esta_civil", DbType.Int32, entidad.EstaCivil);
                    db.AddInParameter(com, "@sexo", DbType.Int32, entidad.Sexo);
                    db.AddInParameter(com, "@vive_casa", DbType.Int32, entidad.ViveCasa);
                    db.AddInParameter(com, "@nomb_arre", DbType.String, entidad.NombArre);
                    db.AddInParameter(com, "@lada_arre", DbType.String, entidad.LadaArre);
                    db.AddInParameter(com, "@tele_arre", DbType.String, entidad.TeleArre);
                    db.AddInParameter(com, "@renta_arre", DbType.Decimal, entidad.RentaArre);
                    db.AddInParameter(com, "@depe_econo", DbType.Int32, entidad.DepeEcono);
                    db.AddInParameter(com, "@conyuge ", DbType.String, entidad.Conyuge);
                    db.AddInParameter(com, "@cony_rfc", DbType.String, entidad.ConyRfc);
                    string fch_Cony = ("00" + DateTime.Now.Day.ToString()).Substring(("00" + DateTime.Now.Day.ToString()).Length - 2, 2) + "/" + ("00" + DateTime.Now.Month.ToString()).Substring(("00" + DateTime.Now.Month.ToString()).Length - 2, 2) + ("0000" + DateTime.Now.Year.ToString()).Substring(("0000" + DateTime.Now.Year.ToString()).Length - 4, 4);
                    db.AddInParameter(com, "@cony_fch_nac", DbType.String, fch_Cony);
                    db.AddInParameter(com, "@cony_compa", DbType.String, entidad.ConyCompa);
                    db.AddInParameter(com, "@cony_dire", DbType.String, entidad.ConyDire);
                    db.AddInParameter(com, "@cony_IdCo", DbType.Decimal, entidad.ConyIdCo);
                    db.AddInParameter(com, "@cony_lada", DbType.String, entidad.ConyLada);
                    db.AddInParameter(com, "@cony_tele", DbType.String, entidad.ConyTele);
                    db.AddInParameter(com, "@cony_exte", DbType.String, entidad.ConyExte);
                    db.AddInParameter(com, "@cony_puest", DbType.String, entidad.ConyPuest);
                    db.AddInParameter(com, "@cony_antig", DbType.Double, entidad.ConyAntig);
                    db.AddInParameter(com, "@cony_ingre", DbType.Decimal, entidad.ConyIngre);
                    db.AddInParameter(com, "@cony_IdGiro", DbType.Int32, entidad.ConyIdGiro);
                    db.AddInParameter(com, "@referencia1", DbType.String, entidad.Referencia1);
                    db.AddInParameter(com, "@IdParentesco1", DbType.Int32, entidad.IdParentesco1);
                    db.AddInParameter(com, "@dire_ref1", DbType.String, entidad.DireRef1);
                    db.AddInParameter(com, "@IdCo_ref1", DbType.Decimal, entidad.IdCoRef1);
                    db.AddInParameter(com, "@lada_ref1", DbType.String, entidad.LadaRef1);
                    db.AddInParameter(com, "@tele_ref1", DbType.String, entidad.TeleRef1);
                    db.AddInParameter(com, "@exte_ref1", DbType.String, entidad.ExteRef1);
                    db.AddInParameter(com, "@referencia2", DbType.String, entidad.Referencia2);
                    db.AddInParameter(com, "@IdParentesco2", DbType.Int32, entidad.IdParentesco2);
                    db.AddInParameter(com, "@dire_ref2", DbType.String, entidad.DireRef2);
                    db.AddInParameter(com, "@IdCo_ref2", DbType.Decimal, entidad.IdCoRef2);
                    db.AddInParameter(com, "@lada_ref2", DbType.String, entidad.LadaRef2);
                    db.AddInParameter(com, "@tele_ref2", DbType.String, entidad.TeleRef2);
                    db.AddInParameter(com, "@exte_ref2", DbType.String, entidad.ExteRef2);
                    db.AddInParameter(com, "@IdTipoCliente", DbType.Int32, entidad.IdTipoCliente);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, entidad.IdEstatus);
                    db.AddInParameter(com, "@IdNacionalidad", DbType.Int32, entidad.IdNacionalidad);
                    db.AddInParameter(com, "@EntreCalles_1", DbType.String, entidad.EntreCalles1);
                    db.AddInParameter(com, "@Celular", DbType.String, entidad.Celular);
                    db.AddInParameter(com, "@EntreCalles_2", DbType.String, entidad.EntreCalles2);
                    db.AddInParameter(com, "@NumInterior", DbType.String, entidad.NumInterior);
                    db.AddInParameter(com, "@NumExterior", DbType.String, entidad.NumExterior);
                    db.AddInParameter(com, "@TelMensaje", DbType.Int32, entidad.TelMensaje);
                    db.AddInParameter(com, "@IdDependencia", DbType.Int32, entidad.IdDependencia);
                    db.AddInParameter(com, "@IFE", DbType.String, entidad.IFE);
                    db.AddInParameter(com, "@IdTipoTransferencia", DbType.Int32, entidad.IdTipoTransferencia);
                    db.AddInParameter(com, "@Clabe", DbType.String, entidad.Clabe);
                    db.AddInParameter(com, "@curp", DbType.String, entidad.CURP);
                    db.AddInParameter(com, "@idpaisNacimiento", DbType.Int32, entidad.IdPaisNacimiento);
                    db.AddInParameter(com, "@idPaisRecidencia", DbType.Int32, entidad.IdPaisRecidencia);
                    db.AddInParameter(com, "@ocupacion", DbType.String, entidad.Ocupacion);
                    db.AddInParameter(com, "@claveActibidadEconomica", DbType.String, entidad.IdActividadEconomica);
                    db.AddInParameter(com, "@referencia3", DbType.String, entidad.Referencia3);
                    db.AddInParameter(com, "@IdParentesco3", DbType.Int32, entidad.IdParentesco3);
                    db.AddInParameter(com, "@lada_ref3", DbType.String, entidad.LadaRef3);
                    db.AddInParameter(com, "@tele_ref3", DbType.String, entidad.TeleRef3);
                    db.AddInParameter(com, "@referencia4", DbType.String, entidad.Referencia4);
                    db.AddInParameter(com, "@IdParentesco4", DbType.Int32, entidad.IdParentesco4);
                    db.AddInParameter(com, "@lada_ref4", DbType.String, entidad.LadaRef4);
                    db.AddInParameter(com, "@tele_ref4", DbType.String, entidad.TeleRef4);
                    db.AddInParameter(com, "@SituacionLaboral", DbType.Int32, entidad.SituacionLaboral);
                    db.AddInParameter(com, "@RegimenPension", DbType.Int32, (entidad.RegimenPension > 0) ? entidad.RegimenPension : (object)DBNull.Value);
                    db.AddInParameter(com, "@TipoDocumento", DbType.Int32, entidad.TipoDocumento);
                    db.AddInParameter(com, "@NumeroDocumento", DbType.String, entidad.NumeroDocumento);
                    db.AddInParameter(com, "@TipoDireccion", DbType.Int32, entidad.TipoDireccion);
                    db.AddInParameter(com, "@NombreVia", DbType.String, entidad.NombreVia);
                    db.AddInParameter(com, "@TipoZona", DbType.Int32, entidad.TipoZona);
                    db.AddInParameter(com, "@NombreZona", DbType.String, entidad.NombreZona);
                    db.AddInParameter(com, "@Manzana", DbType.String, entidad.Manzana);
                    db.AddInParameter(com, "@Lote", DbType.String, entidad.Lote);
                    db.AddInParameter(com, "@ReferenciaDomicilio", DbType.String, entidad.ReferenciaDomicilio);
                    db.AddInParameter(com, "@cel_ref1", DbType.String, entidad.CelRef1);
                    db.AddInParameter(com, "@cel_ref2", DbType.String, entidad.CelRef2);
                    db.AddInParameter(com, "@cel_ref3", DbType.String, entidad.CelRef3);
                    db.AddInParameter(com, "@cel_ref4", DbType.String, entidad.CelRef4);
                    db.AddInParameter(com, "@ConyTipoDocumento", DbType.Int32, entidad.ConyTipoDocumento);
                    db.AddInParameter(com, "@ConyNumeroDocumento", DbType.String, entidad.ConyNumeroDocumento);
                    db.AddInParameter(com, "@ConyDireccion", DbType.String, entidad.ConyDireccion);
                    db.AddInParameter(com, "@ConyRUC", DbType.String, entidad.ConyRUC);
                    db.AddInParameter(com, "@ConyDireccionEmpresa", DbType.String, entidad.ConyDireccionEmpresa);
                    db.AddInParameter(com, "@ConyTelefonoEmpresa", DbType.String, entidad.ConyTelefonoEmpresa);
                    //NO ESTABAN
                    db.AddInParameter(com, "@IDC_Nacimiento", DbType.Int32, entidad.IdCNacimiento);
                    db.AddInParameter(com, "@Correo", DbType.String, entidad.Correo);
                    db.AddInParameter(com, "@NumInteriorOfic", DbType.String, entidad.NumInteriorOfic);
                    db.AddInParameter(com, "@NumExteriorOfic", DbType.String, entidad.NumExteriorOfic);
                    db.AddInParameter(com, "@PEP", DbType.Int32, entidad.PEP);
                    db.AddInParameter(com, "@PEPNombre", DbType.String, entidad.PEPNombre);
                    db.AddInParameter(com, "@PEPPuesto", DbType.String, entidad.PEPPuesto);
                    db.AddInParameter(com, "@PEPParentesco", DbType.Int32, entidad.PEPParentesco);
                    db.AddInParameter(com, "@IdConfiguracionEmpresa", DbType.Int32, entidad.IdConfiguracionEmpresa);
                    db.AddInParameter(com, "@UsuarioPlanillaVirtual", DbType.String, entidad.UsuarioPlanillaVirtual);
                    db.AddInParameter(com, "@ClavePlanillaVirtual", DbType.String, entidad.ClavePlanillaVirtual);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, entidad.IdUsuarioActualiza);
                    //Nuevos Parametros
                    db.AddInParameter(com, "@vNomArrendatarioCasa", DbType.String, entidad.vNomArrendatarioCasa);
                    db.AddInParameter(com, "@iAniosViviendoCasa", DbType.Int32, entidad.iAniosViviendoCasa);
                    db.AddInParameter(com, "@vNomFamCasa", DbType.String, entidad.vNomFamCasa);
                    db.AddInParameter(com, "@vParentescoCasa", DbType.String, entidad.vParentescoCasa);
                    db.AddInParameter(com, "@iTipContrato", DbType.Int32, entidad.iTipContrato);
                    db.AddInParameter(com, "@iTipNegocio", DbType.Int32, entidad.iTipNegocio);
                    db.AddInParameter(com, "@vDNNomCliente", DbType.String, entidad.vDNNomCliente);
                    db.AddInParameter(com, "@vDNNomRazon", DbType.String, entidad.vDNNomRazon);
                    db.AddInParameter(com, "@iDNTipPersoneria", DbType.Int32, entidad.iDNTipPersoneria);
                    db.AddInParameter(com, "@vDNTipSociedad", DbType.String, entidad.vDNTipSociedad);
                    db.AddInParameter(com, "@iDNTipNegocio", DbType.Int32, entidad.iDNTipNegocio);
                    db.AddInParameter(com, "@iDNTipRegTributario", DbType.Int32, entidad.iDNTipRegTributario);
                    db.AddInParameter(com, "@vDNRUC", DbType.String, entidad.vDNRUC);
                    db.AddInParameter(com, "@iDNTipSector", DbType.Int32, entidad.iDNTipSector);
                    db.AddInParameter(com, "@iDNTipActividad", DbType.Int32, entidad.iDNTipActividad);
                    db.AddInParameter(com, "@iDNMesExperiencia", DbType.Int32, entidad.iDNMesExperiencia);
                    db.AddInParameter(com, "@iDNAnioExperiencia", DbType.Int32, entidad.iDNAnioExperiencia);
                    db.AddInParameter(com, "@iDNMesInicio", DbType.Int32, entidad.iDNMesInicio);
                    db.AddInParameter(com, "@iDNAnioInicio", DbType.Int32, entidad.iDNAnioInicio);
                    db.AddInParameter(com, "@iDNNumEmpleados", DbType.Int32, entidad.iDNNumEmpleados);
                    db.AddInParameter(com, "@iDNNumSucursales", DbType.Int32, entidad.iDNNumSucursales);
                    db.AddInParameter(com, "@iDNTipDireccion", DbType.Int32, entidad.iDNTipDireccion);
                    db.AddInParameter(com, "@vDNVia", DbType.String, entidad.vDNVia);
                    db.AddInParameter(com, "@iDNNumero", DbType.Int32, entidad.iDNNumero);
                    db.AddInParameter(com, "@vDNLote", DbType.String, entidad.vDNLote);
                    db.AddInParameter(com, "@vDNManzana", DbType.String, entidad.vDNManzana);
                    db.AddInParameter(com, "@vDNInterior", DbType.String, entidad.vDNInterior);
                    db.AddInParameter(com, "@iDNPiso", DbType.Int32, entidad.iDNPiso);
                    db.AddInParameter(com, "@vDNZona", DbType.String, entidad.vDNZona);
                    db.AddInParameter(com, "@iDNIdDepartamento", DbType.Int32, entidad.iDNIdDepartamento);
                    db.AddInParameter(com, "@iDNIdProvincia", DbType.Int32, entidad.iDNIdProvincia);
                    db.AddInParameter(com, "@iDNIdDistrito", DbType.Int32, entidad.iDNIdDistrito);
                    db.AddInParameter(com, "@vDNReferencia", DbType.String, entidad.vDNReferencia);
                    db.AddInParameter(com, "@iDNTipPropiedad", DbType.Int32, entidad.iDNTipPropiedad);
                    db.AddInParameter(com, "@iDNAnioOcupacion", DbType.Int32, entidad.iDNAnioOcupacion);
                    db.AddInParameter(com, "@vDNNomComercio", DbType.String, entidad.vDNNomComercio);
                    db.AddInParameter(com, "@iDNTelefono", DbType.Int32, entidad.iDNTelefono);
                    db.AddInParameter(com, "@iDNCelular", DbType.Int32, entidad.iDNCelular);
                    db.AddInParameter(com, "@vDNEmail", DbType.String, entidad.vDNEmail);
                    db.AddInParameter(com, "@vDNNomAccionista", DbType.String, entidad.vDNNomAccionista);
                    db.AddInParameter(com, "@iDNTipRelacion", DbType.Int32, entidad.iDNTipRelacion);
                    db.AddInParameter(com, "@iDNParticipacion", DbType.Int32, entidad.iDNParticipacion);
                    db.AddInParameter(com, "@iDNIdCargo", DbType.Int32, entidad.iDNIdCargo);


                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new Resultado<bool>();
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) resultado.CodigoStr = reader[0].ToString();
                                if (!reader.IsDBNull(1)) resultado.Mensaje = reader[1].ToString();
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                resultado = new Resultado<bool>();
                resultado.Codigo = 0;
                resultado.Mensaje = ex.Message;
                throw ex;
            }
            return resultado;
        }

        public List<TBClientes.BusquedaCliente> BusquedaCliente(int Accion, int IdSolicitud, int idCliente, string Cliente, DateTime FechaInicial, DateTime FechaFinal, int idSucursal)
        {
            List<TBClientes.BusquedaCliente> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelCliente"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, IdSolicitud);
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, idCliente);
                    db.AddInParameter(com, "@Cliente", DbType.String, Cliente);
                    db.AddInParameter(com, "@FechaInicial", DbType.DateTime, FechaInicial);
                    db.AddInParameter(com, "@FechaFinal", DbType.DateTime, FechaFinal);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, idSucursal);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBClientes.BusquedaCliente>();
                            while (reader.Read())
                            {
                                TBClientes.BusquedaCliente cliente = new TBClientes.BusquedaCliente();
                                if (!reader.IsDBNull(0)) cliente.clave = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) cliente.clavedesc = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) cliente.Cliente = reader[2].ToString();
                                if (!reader.IsDBNull(3)) cliente.Nombres = reader[3].ToString();
                                if (!reader.IsDBNull(4)) cliente.apellidop = reader[4].ToString();
                                if (!reader.IsDBNull(5)) cliente.fch_Nacimiento = Convert.ToDateTime(reader[5]);
                                if (!reader.IsDBNull(6)) cliente.apellidom = reader[6].ToString();
                                if (!reader.IsDBNull(7)) cliente.direccion = reader[7].ToString();
                                if (!reader.IsDBNull(8)) cliente.colonia = reader[8].ToString();
                                if (!reader.IsDBNull(9)) cliente.ciudad = reader[9].ToString();
                                if (!reader.IsDBNull(10)) cliente.estado = reader[10].ToString();
                                if (!reader.IsDBNull(11)) cliente.cp = Convert.ToInt32(reader[11]);
                                if (!reader.IsDBNull(12)) cliente.IdCliente = Convert.ToInt32(reader[12]);
                                if (!reader.IsDBNull(13)) cliente.dni = reader[13].ToString();
                                resultado.Add(cliente);
                            }

                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<TBClientes> ObtenerTBClientes(int Accion, int IdCliente)
        {
            List<TBClientes> resultado = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Creditos.Sel_Cliente"))
                {
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@idCliente", DbType.Int32, IdCliente);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBClientes>();
                            while (reader.Read())
                            {
                                TBClientes cliente = new TBClientes();
                                if (!reader.IsDBNull(0)) cliente.IdCliente = Convert.ToDecimal(reader[0]);
                                if (!reader.IsDBNull(1)) cliente.Cliente = reader[1].ToString();
                                if (!reader.IsDBNull(2)) cliente.Nombres = reader[2].ToString();
                                if (!reader.IsDBNull(3)) cliente.Apellidop = reader[3].ToString();
                                if (!reader.IsDBNull(4)) cliente.Apellidom = reader[4].ToString();
                                if (!reader.IsDBNull(5)) cliente.FchNacimiento = (reader[5].ToString());
                                if (!reader.IsDBNull(6)) cliente.Rfc = reader[6].ToString();
                                //if (!reader.IsDBNull(7)) IdGiro NO SE USA NO SE ELIMINA AUN JCGC 180520
                                if (!reader.IsDBNull(8)) cliente.Direccion = reader[8].ToString();
                                if (!reader.IsDBNull(9)) cliente.IdColonia = Convert.ToDecimal(reader[9]);
                                if (!reader.IsDBNull(10)) cliente.Lada = reader[10].ToString();
                                if (!reader.IsDBNull(11)) cliente.Telefono = reader[11].ToString();
                                if (!reader.IsDBNull(12)) cliente.AntigDom = Convert.ToDouble(reader[12]);
                                if (!reader.IsDBNull(13)) cliente.Compania = reader[13].ToString();
                                if (!reader.IsDBNull(14)) cliente.IdEscuela = Convert.ToInt32(reader[14]);
                                if (!reader.IsDBNull(15)) cliente.NumEmplea = reader[15].ToString();
                                if (!reader.IsDBNull(16)) cliente.DireOfic = reader[16].ToString();
                                if (!reader.IsDBNull(17)) cliente.IdCoOfic = Convert.ToDecimal(reader[17]);
                                if (!reader.IsDBNull(18)) cliente.LadaOfic = reader[18].ToString();
                                if (!reader.IsDBNull(19)) cliente.TeleOfic = reader[19].ToString();
                                if (!reader.IsDBNull(20)) cliente.ExteOfic = reader[20].ToString();
                                if (!reader.IsDBNull(21)) cliente.AntigOfic = Convert.ToDouble(reader[21]);
                                if (!reader.IsDBNull(22)) cliente.Puesto = reader[22].ToString();
                                if (!reader.IsDBNull(23)) cliente.Ingreso = Convert.ToDecimal(reader[23]);
                                if (!reader.IsDBNull(24)) cliente.OtrosIngr = Convert.ToDecimal(reader[24]);
                                if (!reader.IsDBNull(25)) cliente.FuenOtroi = reader[25].ToString();
                                if (!reader.IsDBNull(26)) cliente.EstaCivil = Convert.ToInt32(reader[26]);
                                if (!reader.IsDBNull(27)) cliente.Sexo = Convert.ToInt32(reader[27]);
                                if (!reader.IsDBNull(28)) cliente.ViveCasa = Convert.ToInt32(reader[28]);
                                if (!reader.IsDBNull(29)) cliente.NombArre = reader[29].ToString();
                                if (!reader.IsDBNull(30)) cliente.LadaArre = reader[30].ToString();
                                if (!reader.IsDBNull(31)) cliente.TeleArre = reader[31].ToString();
                                if (!reader.IsDBNull(32)) cliente.RentaArre = Convert.ToDecimal(reader[32]);
                                if (!reader.IsDBNull(33)) cliente.DepeEcono = Convert.ToInt32(reader[33]);
                                if (!reader.IsDBNull(34)) cliente.Conyuge = reader[34].ToString();
                                if (!reader.IsDBNull(35)) cliente.ConyRfc = reader[35].ToString();
                                if (!reader.IsDBNull(36)) cliente.ConyFchNac = Convert.ToDateTime(reader[36]);
                                if (!reader.IsDBNull(37)) cliente.ConyCompa = reader[37].ToString();
                                if (!reader.IsDBNull(38)) cliente.ConyDire = reader[38].ToString();
                                if (!reader.IsDBNull(39)) cliente.ConyIdCo = Convert.ToDecimal(reader[39]);
                                if (!reader.IsDBNull(40)) cliente.ConyLada = reader[40].ToString();
                                if (!reader.IsDBNull(41)) cliente.ConyTele = reader[41].ToString();
                                if (!reader.IsDBNull(42)) cliente.ConyExte = reader[42].ToString();
                                if (!reader.IsDBNull(43)) cliente.ConyPuest = reader[43].ToString();
                                if (!reader.IsDBNull(44)) cliente.ConyAntig = Convert.ToDouble(reader[44]);
                                if (!reader.IsDBNull(45)) cliente.ConyIngre = Convert.ToDecimal(reader[45]);
                                if (!reader.IsDBNull(46)) cliente.ConyIdGiro = Convert.ToInt32(reader[46]);
                                if (!reader.IsDBNull(47)) cliente.Referencia1 = reader[47].ToString();
                                if (!reader.IsDBNull(48)) cliente.IdParentesco1 = Convert.ToInt32(reader[48]);
                                if (!reader.IsDBNull(49)) cliente.DireRef1 = reader[49].ToString();
                                if (!reader.IsDBNull(50)) cliente.IdCoRef1 = Convert.ToDecimal(reader[50]);
                                if (!reader.IsDBNull(51)) cliente.LadaRef1 = reader[51].ToString();
                                if (!reader.IsDBNull(52)) cliente.TeleRef1 = reader[52].ToString();
                                if (!reader.IsDBNull(53)) cliente.ExteRef1 = reader[53].ToString();
                                if (!reader.IsDBNull(54)) cliente.Referencia2 = reader[54].ToString();
                                if (!reader.IsDBNull(55)) cliente.IdParentesco2 = Convert.ToInt32(reader[55]);
                                if (!reader.IsDBNull(56)) cliente.DireRef2 = reader[56].ToString();
                                if (!reader.IsDBNull(57)) cliente.IdCoRef2 = Convert.ToDecimal(reader[57]);
                                if (!reader.IsDBNull(58)) cliente.LadaRef2 = reader[58].ToString();
                                if (!reader.IsDBNull(59)) cliente.TeleRef2 = reader[59].ToString();
                                if (!reader.IsDBNull(60)) cliente.ExteRef2 = reader[60].ToString();
                                if (!reader.IsDBNull(61)) cliente.IdTipoCliente = Convert.ToInt32(reader[61]);
                                if (!reader.IsDBNull(62)) cliente.IdEstatus = Convert.ToInt32(reader[62]);
                                if (!reader.IsDBNull(63)) cliente.IdCNacimiento = Convert.ToInt32(reader[63]);
                                if (!reader.IsDBNull(64)) cliente.Celular = reader[64].ToString();
                                if (!reader.IsDBNull(65)) cliente.CURP = reader[65].ToString();
                                if (!reader.IsDBNull(66)) cliente.IdNacionalidad = Convert.ToInt32(reader[66]);
                                if (!reader.IsDBNull(67)) cliente.IFE = reader[67].ToString();
                                if (!reader.IsDBNull(68)) cliente.RFCGenerado = reader[68].ToString();
                                if (!reader.IsDBNull(69)) cliente.NumInterior = reader[69].ToString();
                                if (!reader.IsDBNull(70)) cliente.NumExterior = reader[70].ToString();
                                if (!reader.IsDBNull(71)) cliente.TelMensaje = Convert.ToInt32(reader[71]);
                                if (!reader.IsDBNull(72)) cliente.IdConvenio = Convert.ToInt32(reader[72]);
                                if (!reader.IsDBNull(73)) cliente.IdDependencia = Convert.ToInt32(reader[73]);
                                if (!reader.IsDBNull(74)) cliente.EntreCalles1 = reader[74].ToString();
                                if (!reader.IsDBNull(75)) cliente.EntreCalles2 = reader[75].ToString();
                                if (!reader.IsDBNull(76)) cliente.BndCliente = Convert.ToInt32(reader[76]);
                                if (!reader.IsDBNull(77)) cliente.IdTipoTransferencia = Convert.ToInt32(reader[77]);
                                if (!reader.IsDBNull(78)) cliente.Clabe = reader[78].ToString();
                                if (!reader.IsDBNull(79)) cliente.IdTipoPersona = Convert.ToInt32(reader[79]);
                                if (!reader.IsDBNull(80)) cliente.IdPaisNacimiento = Convert.ToInt32(reader[80]);
                                if (!reader.IsDBNull(81)) cliente.IdPaisRecidencia = Convert.ToInt32(reader[81]);
                                if (!reader.IsDBNull(82)) cliente.FchIngreso = (reader[82].ToString());
                                if (!reader.IsDBNull(83)) cliente.Ocupacion = reader[83].ToString();
                                if (!reader.IsDBNull(84)) cliente.IdActividadEconomica = reader[84].ToString();
                                int x = 85;
                                if (!reader.IsDBNull(x)) cliente.SituacionLaboral = Convert.ToInt32(reader[x].ToString()); x++;
                                if (!reader.IsDBNull(x)) cliente.RegimenPension = Convert.ToInt32(reader[x].ToString()); x++;
                                if (!reader.IsDBNull(x)) cliente.TipoDocumento = Convert.ToInt32(reader[x].ToString()); x++;
                                if (!reader.IsDBNull(x)) cliente.NumeroDocumento = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.TipoDireccion = Convert.ToInt32(reader[x].ToString()); x++;
                                if (!reader.IsDBNull(x)) cliente.NombreVia = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.TipoZona = Convert.ToInt32(reader[x].ToString()); x++;
                                if (!reader.IsDBNull(x)) cliente.NombreZona = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.CelRef1 = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.CelRef2 = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.CelRef3 = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.CelRef4 = reader[x].ToString(); x++;
                                //NO EXISTEN
                                if (!reader.IsDBNull(x)) cliente.Correo = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.RUC = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.NumInteriorOfic = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.NumExteriorOfic  = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.PEP = Convert.ToInt32(reader[x].ToString()); x++;
                                if (!reader.IsDBNull(x)) cliente.PEPNombre = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.PEPPuesto = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.PEPParentesco = Convert.ToInt32(reader[x].ToString()); x++;
                                //CONYUGE
                                if (!reader.IsDBNull(x)) cliente.ConyTipoDocumento = Convert.ToInt32(reader[x].ToString()); x++;
                                if (!reader.IsDBNull(x)) cliente.ConyNumeroDocumento = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.ConyDireccion = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.ConyRUC = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.ConyDireccionEmpresa = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.ConyTelefonoEmpresa = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.Manzana = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.Lote = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.ReferenciaDomicilio = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.IngresoBruto = Convert.ToDecimal(reader[x]); x++;
                                if (!reader.IsDBNull(x)) cliente.DescuentosLey = Convert.ToDecimal(reader[x]); x++;
                                if (!reader.IsDBNull(x)) cliente.OtrosDescuentos = Convert.ToDecimal(reader[x]); x++;
                                if (!reader.IsDBNull(x)) cliente.DependenciaOfic = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.UbicacionOfic = reader[x].ToString(); x++;
                                // JRVB 19/04/2018, No se estaban cargando los datos de las referencias 3 y 4
                                if (!reader.IsDBNull(x)) cliente.Referencia3 = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.IdParentesco3 = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) cliente.LadaRef3 = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.TeleRef3 = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.Referencia4 = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.IdParentesco4 = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) cliente.LadaRef4 = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) cliente.TeleRef4 = reader[x].ToString(); x++;

                                //Datos de ultima actualziacion
                                if (!reader.IsDBNull(127)) cliente.IdUsuarioActualiza = Convert.ToInt32(reader[127]);
                                if (!reader.IsDBNull(128)) cliente.FechaActualizacion = reader[128].ToString();

                                if (!reader.IsDBNull(129)) cliente.IdConfiguracionEmpresa = Convert.ToInt32(reader[129]);

                                cliente.UsuarioPlanillaVirtual = Utils.GetString(reader, "UsuarioPlanillaVirtual");
                                cliente.ClavePlanillaVirtual = Utils.GetString(reader, "ClavePlanillaVirtual");

                                resultado.Add(cliente);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<TBClientes.BusquedaClienteRecomienda> ObtenerTBClienteRecomendador(int Accion, int IdCliente)
        {
            List<TBClientes.BusquedaClienteRecomienda> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("creditos.Sel_Cliente"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@idCliente", DbType.Int32, IdCliente);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBClientes.BusquedaClienteRecomienda>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBClientes.BusquedaClienteRecomienda cliente = new TBClientes.BusquedaClienteRecomienda();
                                if (!reader.IsDBNull(0)) cliente.IdCampania = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) cliente.IdCliente = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) cliente.IdClienteRec = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) cliente.IdClabeRec = Convert.ToInt32(reader[3]);
                                
                                resultado.Add(cliente);
                            }

                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public string ObtenerCategoriaCliente(int idCliente)
        {
            string categoriaCliente = "";
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_CategoriaClienteCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, idCliente);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            if (reader.Read())
                            {
                                categoriaCliente = Utils.GetString(reader, "Categoria");
                            }

                        }
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return categoriaCliente;
        }

        public void ActualizarTBClientes()
        {
        }

        public void EliminarTBClientes()
        {
        }
        #endregion

        #region MISC Methods
        public Resultado<bool> InsertarClienteRecomendador(int Accion, int IdCliente, int IdClienteRec, int IdClabeRec, int IdCampania, int IdUsuario)
        {
            Resultado<bool> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_ABCClienteRecomendador"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, IdCliente);
                    db.AddInParameter(com, "@IdClienteRec", DbType.Int32, IdClienteRec);
                    db.AddInParameter(com, "@IdClabeRec", DbType.Int32, IdClabeRec);
                    db.AddInParameter(com, "@IdCampania", DbType.Int32, IdCampania);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new Resultado<bool>();
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) resultado.Codigo = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) resultado.Mensaje = reader[1].ToString();

                            }
                        }
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                resultado.Codigo = 0;
                resultado.Mensaje = ex.Message;
                throw ex;

            }
            return resultado;
        }

        public Resultado<int> ObtenerSolicitudActivaDeCliente(int idCliente)
        {
            Resultado<int> resultado = new Resultado<int>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("clientes.SP_ObtenerSolicitudActivaCON"))
                {
                    db.AddInParameter(com, "@idCliente", DbType.Int32, idCliente);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("idSolicitud"))) resultado.ResultObject = Convert.ToInt32(reader[reader.GetOrdinal("idSolicitud")].ToString());
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                resultado.Codigo = 1;
                resultado.CodigoStr = ex.Message;
            }

            return resultado;
        }

        public ObtenerConfiguracionEmpresaResponse ObtenerConfiguracionEmpresa(ObtenerConfiguracionEmpresaRequest peticion)
        {
            ObtenerConfiguracionEmpresaResponse resultado = new ObtenerConfiguracionEmpresaResponse();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("clientes.SP_ObtenerConfiguracionEmpresaCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, peticion.Accion);
                    db.AddInParameter(com, "@IdConfiguracionEmpresa", DbType.Int32, (peticion.IdConfiguracion > 0) ? peticion.IdConfiguracion : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdEmpresa", DbType.Int32, (peticion.IdEmpresa > 0) ? peticion.IdEmpresa : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdOrgano", DbType.Int32, (peticion.IdOrgano > 0) ? peticion.IdOrgano : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdSituacionLaboral", DbType.Int32, (peticion.IdSituacionLaboral > 0) ? peticion.IdSituacionLaboral : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdRegimenPension", DbType.Int32, (peticion.IdRegimenPension > 0) ? peticion.IdRegimenPension : (object)DBNull.Value);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            List<ConfiguracionEmpresaResultado> lst = new List<ConfiguracionEmpresaResultado>();
                            List<Combo> sl = new List<Combo>();
                            List<Combo> rp = new List<Combo>();
                            int IdConfiguracion = 0;
                            if (peticion.Accion == "OBTENER_POR_CAMPOS")
                            {
                                while (reader.Read())
                                {
                                    ConfiguracionEmpresaResultado config = new ConfiguracionEmpresaResultado();
                                    config.IdSituacionLaboral = Utils.GetInt(reader, "IdSituacionLaboral");
                                    config.IdRegimenPension = Utils.GetInt(reader, "IdRegimenPension");
                                    config.IdOrganoPago = Utils.GetInt(reader, "IdOrganoPago");
                                    config.NivelRiesgo = Utils.GetString(reader, "NivelRiesgo");
                                    lst.Add(config);
                                }
                                if (reader.NextResult())
                                {
                                    while (reader.Read())
                                    {
                                        Combo combo = new Combo();
                                        combo.Valor = Utils.GetInt(reader, "Valor");
                                        combo.Descripcion = Utils.GetString(reader, "Descripcion");
                                        sl.Add(combo);
                                    }
                                }
                                if (reader.NextResult())
                                {
                                    while (reader.Read())
                                    {
                                        Combo combo = new Combo();
                                        combo.Valor = Utils.GetInt(reader, "Valor");
                                        combo.Descripcion = Utils.GetString(reader, "Descripcion");
                                        rp.Add(combo);
                                    }
                                }
                            }
                            else if (peticion.Accion == "OBTENER_ID_POR_CAMPOS")
                            {
                                while (reader.Read())
                                {
                                    IdConfiguracion = Utils.GetInt(reader, "IdConfiguracion");
                                }
                            }
                            else if (peticion.Accion == "OBTENER_POR_ID")
                            {
                                while (reader.Read())
                                {
                                    ConfiguracionEmpresaResultado config = new ConfiguracionEmpresaResultado();
                                    config.IdEmpresa = Utils.GetInt(reader, "IdEmpresa");
                                    config.IdSituacionLaboral = Utils.GetInt(reader, "IdSituacionLaboral");
                                    config.IdRegimenPension = Utils.GetInt(reader, "IdRegimenPension");
                                    config.IdOrganoPago = Utils.GetInt(reader, "IdOrganoPago");
                                    config.NivelRiesgo = Utils.GetString(reader, "NivelRiesgo");
                                    lst.Add(config);
                                }
                                if (reader.NextResult())
                                {
                                    while (reader.Read())
                                    {
                                        Combo combo = new Combo();
                                        combo.Valor = Utils.GetInt(reader, "Valor");
                                        combo.Descripcion = Utils.GetString(reader, "Descripcion");
                                        sl.Add(combo);
                                    }
                                }
                                if (reader.NextResult())
                                {
                                    while (reader.Read())
                                    {
                                        Combo combo = new Combo();
                                        combo.Valor = Utils.GetInt(reader, "Valor");
                                        combo.Descripcion = Utils.GetString(reader, "Descripcion");
                                        rp.Add(combo);
                                    }
                                }
                            }
                            resultado.Resultado = lst;
                            resultado.ComboSituacionLaboral = sl;
                            resultado.ComboRegimenPension = rp;
                            resultado.IdConfiguracionEmpresa = IdConfiguracion;
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                resultado.Error = true;
                resultado.MensajeOperacion = ex.Message;
            }
            catch (Exception ex)
            {
                resultado.Error = true;
                resultado.MensajeOperacion = ex.Message;
            }
            return resultado;
        }

        public List<CapturaPlanillaVirtualConfig> ObtenerConfiguracionCapturaPlanillaVirtual(int IdEmpresa)
        {
            List<CapturaPlanillaVirtualConfig> objReturn = new List<CapturaPlanillaVirtualConfig>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Clientes.SP_PlanillaVirtualConfigCON"))
                {                    
                    db.AddInParameter(com, "@IdEmpresa", DbType.Int32, IdEmpresa);
                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            CapturaPlanillaVirtualConfig o = new CapturaPlanillaVirtualConfig();

                            while (reader.Read())
                            {
                                o = new CapturaPlanillaVirtualConfig();

                                o.IdPlanillaVirtualConfig = Utils.GetInt(reader, "IdPlanillaVirtualConfig");
                                o.IdEmpresa = Utils.GetInt(reader, "IdEmpresa");
                                o.DescripcionCampoUsuario = Utils.GetString(reader, "DescripcionCampoUsuario");
                                o.DescripcionCampoClave = Utils.GetString(reader, "DescripcionCampoClave");
                                o.VisibleCampoUsuario = Utils.GetBool(reader, "VisibleCampoUsuario");
                                o.VisibleCampoClave = Utils.GetBool(reader, "VisibleCampoClave");
                                o.Activo = Utils.GetBool(reader, "Activo");

                                objReturn.Add(o);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }            
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }
        #endregion

        #region Referencias 
        public Resultado<bool> ActualizarReferenciasCliente(int idCliente, int idUsuario, List<ClientesReferencia> referencias)
        {
            if (referencias == null) throw new Exception("Petición inválida.");
            if (idCliente <= 0) throw new Exception("La clave del cliente es inválida.");
            if (idUsuario <= 0) throw new Exception("La clave del usuario es inválida.");

            Resultado<bool> respuesta = new Resultado<bool>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("clientes.SP_ClienteReferenciasACT"))
                {
                    TBClientes.ReferenciaCliente referencia1 = null, referencia2 = null, referencia3 = null, referencia4 = null;

                    if (referencias != null)
                    {
                        if (referencias.Count > 0) referencia1 = referencias[0].ToReferenciaCliente();
                        if (referencias.Count > 1) referencia2 = referencias[1].ToReferenciaCliente();
                        if (referencias.Count > 2) referencia3 = referencias[2].ToReferenciaCliente();
                        if (referencias.Count > 3) referencia4 = referencias[3].ToReferenciaCliente();
                    }

                    db.AddInParameter(com, "@IdCliente", DbType.Int32, idCliente);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, idUsuario);

                    if (referencia1 != null)
                    {
                        db.AddInParameter(com, "@referencia1", DbType.String, referencia1.Referencia);
                        db.AddInParameter(com, "@IdParentesco1", DbType.Int32, referencia1.IdParentesco);
                        db.AddInParameter(com, "@dire_ref1", DbType.String, referencia1.Direccion);
                        db.AddInParameter(com, "@IdCo_ref1", DbType.Decimal, referencia1.IdColonia);
                        db.AddInParameter(com, "@lada_ref1", DbType.String, referencia1.Lada);
                        db.AddInParameter(com, "@tele_ref1", DbType.String, referencia1.Telefono);
                        db.AddInParameter(com, "@cel_ref1", DbType.String, referencia1.Celular);
                    }

                    if (referencia2 != null)
                    {
                        db.AddInParameter(com, "@referencia2", DbType.String, referencia2.Referencia);
                        db.AddInParameter(com, "@IdParentesco2", DbType.Int32, referencia2.IdParentesco);
                        db.AddInParameter(com, "@dire_ref2", DbType.String, referencia2.Direccion);
                        db.AddInParameter(com, "@IdCo_ref2", DbType.Decimal, referencia2.IdColonia);
                        db.AddInParameter(com, "@lada_ref2", DbType.String, referencia2.Lada);
                        db.AddInParameter(com, "@tele_ref2", DbType.String, referencia2.Telefono);
                        db.AddInParameter(com, "@cel_ref2", DbType.String, referencia2.Celular);
                    }

                    if (referencia3 != null)
                    {
                        db.AddInParameter(com, "@referencia3", DbType.String, referencia3.Referencia);
                        db.AddInParameter(com, "@IdParentesco3", DbType.Int32, referencia3.IdParentesco);
                        db.AddInParameter(com, "@dire_ref3", DbType.String, referencia3.Direccion);
                        db.AddInParameter(com, "@IdCo_ref3", DbType.Decimal, referencia3.IdColonia);
                        db.AddInParameter(com, "@lada_ref3", DbType.String, referencia3.Lada);
                        db.AddInParameter(com, "@tele_ref3", DbType.String, referencia3.Telefono);
                        db.AddInParameter(com, "@cel_ref3", DbType.String, referencia3.Celular);
                    }

                    if (referencia4 != null)
                    {
                        db.AddInParameter(com, "@referencia4", DbType.String, referencia4.Referencia);
                        db.AddInParameter(com, "@IdParentesco4", DbType.Int32, referencia4.IdParentesco);
                        db.AddInParameter(com, "@dire_ref4", DbType.String, referencia4.Direccion);
                        db.AddInParameter(com, "@IdCo_ref4", DbType.Decimal, referencia4.IdColonia);
                        db.AddInParameter(com, "@lada_ref4", DbType.String, referencia4.Lada);
                        db.AddInParameter(com, "@tele_ref4", DbType.String, referencia4.Telefono);
                        db.AddInParameter(com, "@cel_ref4", DbType.String, referencia4.Celular);
                    }

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) respuesta.Codigo = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) respuesta.Mensaje = reader[1].ToString();

                            }
                        }
                        respuesta.ResultObject = respuesta.Codigo > 0 ? true : false;
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria 
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.ResultObject = false;
                respuesta.CodigoStr = ex.Message;
            }

            return respuesta;
        }
        #endregion

        #region MISC Methods
        public Resultado<bool> ActualizarClienteGestiones(TBClientes.ActualizaClienteGestiones cliente)
        {
            var respuesta = new Resultado<bool>();
            try
            {
                if (cliente != null)
                {
                    using (var com = db.GetStoredProcCommand("dbo.SP_ClienteGestionesACT"))
                    {
                        db.AddInParameter(com, "@Accion", DbType.Int32, 0);
                        db.AlimentarParametrosDefault(com, cliente);

                        using (IDataReader reader = db.ExecuteReader(com))
                        {
                            if (reader != null)
                            {
                                while (reader.Read())
                                {
                                    if (!reader.IsDBNull(0)) respuesta.Codigo = Convert.ToInt32(reader[0]);
                                    if (!reader.IsDBNull(1)) respuesta.Mensaje = reader[1].ToString();

                                }
                            }
                            respuesta.ResultObject = respuesta.Codigo > 0 ? true : false;
                            reader.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return respuesta;
        }
        public List<DireccionCliente> ObtenerDireccionesCliente(int idCliente)
        {
            List<DireccionCliente> direccionesCliente = new List<DireccionCliente>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("clientes.SP_ClienteDireccionesCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.Int32, 1);
                    db.AddInParameter(com, "@idCliente", DbType.Int32, idCliente);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                DireccionCliente sd = new DireccionCliente();
                                if (!reader.IsDBNull(reader.GetOrdinal("idClienteDireccion"))) sd.idClienteDireccion = Convert.ToInt32(reader[reader.GetOrdinal("idClienteDireccion")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("idCliente"))) sd.idCliente = Convert.ToInt32(reader[reader.GetOrdinal("idCliente")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("idSolicitud"))) sd.idSolicitud = Convert.ToInt32(reader[reader.GetOrdinal("idSolicitud")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("direccion"))) sd.direccion = reader[reader.GetOrdinal("direccion")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("idTipoDireccion"))) sd.idTipoDireccion = Convert.ToInt32(reader[reader.GetOrdinal("idTipoDireccion")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("nombreVia"))) sd.nombreVia = reader[reader.GetOrdinal("nombreVia")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("numeroExterno"))) sd.numeroExterno = reader[reader.GetOrdinal("numeroExterno")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("numeroInterno"))) sd.numeroInterno = reader[reader.GetOrdinal("numeroInterno")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("manzana"))) sd.manzana = reader[reader.GetOrdinal("manzana")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("lote"))) sd.lote = reader[reader.GetOrdinal("lote")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("tipoZona"))) sd.tipoZona = Convert.ToInt32(reader[reader.GetOrdinal("tipoZona")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("nombreZona"))) sd.nombreZona = reader[reader.GetOrdinal("nombreZona")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("distrito"))) sd.idColonia = Convert.ToInt32(reader[reader.GetOrdinal("distrito")].ToString());
                                direccionesCliente.Add(sd);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return direccionesCliente;
        }
        #endregion

        #region Private Methods
        #endregion
    }
}