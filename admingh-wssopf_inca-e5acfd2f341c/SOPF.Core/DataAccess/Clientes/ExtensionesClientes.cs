﻿using SOPF.Core.Poco.Clientes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using static SOPF.Core.Entities.Clientes.TBClientes;

namespace SOPF.Core.DataAccess.Clientes
{
    public static class ExtensionesClientes
    {
        public static NombreSeparado SepararNombreCompleto(this string nombre)
        {
            NombreSeparado nombreSeparado = new NombreSeparado();
            try
            {
                if (nombre != null)
                {
                    string nombreSanitizado = nombre.ToUpper().Replace("  ", " ");
                    if (nombreSanitizado != null)
                    {
                        string[] palabras = nombreSanitizado.Split(' ');
                        string conjuntosFinales = string.Empty, subconjunto = string.Empty;
                        bool mismoConjunto = false;
                        for (var i = 0; i < palabras.Length; i++)
                        {
                            string palabra = palabras[i];
                            if (!String.IsNullOrWhiteSpace(palabra))
                            {
                                if (i == 0)
                                {
                                    mismoConjunto = true;
                                    conjuntosFinales += palabra;
                                }
                                else if (palabra == "DE" || palabra == "LA" || palabra == "DEL" || palabra == "LOS")
                                {
                                    mismoConjunto = true;
                                    subconjunto += " " + palabra;
                                    subconjunto = subconjunto.Trim();
                                    if (subconjunto == "DE LOS" || subconjunto == "DEL" || subconjunto == "DE LA")
                                    {
                                        conjuntosFinales += "|";
                                        subconjunto = string.Empty;
                                    }
                                    conjuntosFinales += palabra;
                                }
                                else
                                {
                                    subconjunto = string.Empty;
                                    if (!mismoConjunto)
                                        conjuntosFinales += "|" + palabra;
                                    else
                                        conjuntosFinales += palabra;
                                    mismoConjunto = false;
                                }
                                conjuntosFinales += " ";
                            }
                        }

                        if (!String.IsNullOrWhiteSpace(conjuntosFinales))
                        {
                            conjuntosFinales = conjuntosFinales.Trim();
                            string[] conjuntos = conjuntosFinales.Split('|');
                            if (conjuntos != null)
                            {
                                for (var i = 0; i < conjuntos.Length; i++)
                                {
                                    string conjNombre = conjuntos[i].Trim();
                                    switch (i)
                                    {
                                        case 0:
                                            nombreSeparado.Nombres = conjNombre;
                                            break;
                                        case 1:
                                            nombreSeparado.ApellidoPaterno = conjNombre;
                                            break;
                                        case 2:
                                            nombreSeparado.ApellidoMaterno = conjNombre;
                                            break;
                                        default:
                                            nombreSeparado.ApellidoMaterno += " " + conjNombre;
                                            break;
                                    }
                                }
                                if (nombreSeparado.ApellidoMaterno != null)
                                    nombreSeparado.ApellidoMaterno = nombreSeparado.ApellidoMaterno.Trim();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return nombreSeparado;
        }
        public static List<ClientesReferencia> ToListClientesReferencia(this List<ReferenciaCliente> referencias)
        {
            List<ClientesReferencia> clientesReferencias = new List<ClientesReferencia>();
            try
            {
                foreach (var referencia in referencias)
                {
                    var tempClientesReferencia = referencia.ToClientesReferencia();
                    if (tempClientesReferencia != null)
                        clientesReferencias.Add(tempClientesReferencia);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return clientesReferencias;
        }
        public static ClientesReferencia ToClientesReferencia(this ReferenciaCliente referencia)
        {
            ClientesReferencia tempReferencia = null;
            try
            {
                if (referencia.Referencia != null)
                {
                    NombreSeparado tempNombreSeparado = referencia.Referencia.SepararNombreCompleto();
                    tempReferencia = new ClientesReferencia();
                    tempReferencia.IdCliente = referencia.IdCliente;
                    tempReferencia.Nombres = tempNombreSeparado.Nombres == null ? "" : tempNombreSeparado.Nombres;
                    tempReferencia.ApellidoPaterno = tempNombreSeparado.ApellidoPaterno == null ? "" : tempNombreSeparado.ApellidoPaterno;
                    tempReferencia.ApellidoMaterno = tempNombreSeparado.ApellidoMaterno;
                    tempReferencia.IdParentesco = referencia.IdParentesco;
                    tempReferencia.Calle = referencia.Direccion == null ? "" : referencia.Direccion;
                    tempReferencia.NumeroExterior = "";
                    tempReferencia.IdColonia = (int)referencia.IdColonia;
                    tempReferencia.Lada = String.IsNullOrWhiteSpace(referencia.Lada) ? null : referencia.Lada;
                    tempReferencia.Telefono = String.IsNullOrWhiteSpace(referencia.Telefono) ? null : referencia.Telefono;
                    tempReferencia.TelefonoCelular = String.IsNullOrWhiteSpace(referencia.Celular) ? null : referencia.Celular;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return tempReferencia;
        }
        public static DataTable AsDataTable<T>(this IEnumerable<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
