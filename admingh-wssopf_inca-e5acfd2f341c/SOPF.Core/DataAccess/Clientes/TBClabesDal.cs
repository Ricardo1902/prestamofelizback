#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Clientes;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBClabesDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBClabes
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Clientes
{
    public class TBClabesDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBClabesDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public TBClabes.DataResultClabe InsertarTBClabes(int Accion, TBClabes entidad)
        {
            TBClabes.DataResultClabe resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_ABCClabeCliente"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@idClabe", DbType.Int32, entidad.IdClabe);
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, entidad.IdCliente);
                    db.AddInParameter(com, "@Clabe", DbType.String, entidad.Clabe);
                    db.AddInParameter(com, "@numPlastico", DbType.String, entidad.NroPlastico);
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, entidad.IdBanco);
                    db.AddInParameter(com, "@PagoMatico", DbType.Int32, entidad.IsPagoMatico);
				
					//Ejecucion de la Consulta
                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new TBClabes.DataResultClabe();
                            //Lectura de los datos del ResultSet

                            while (reader.Read())
                            {

                                if (!reader.IsDBNull(0)) resultado.desBandera = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) resultado.Bandera = reader[1].ToString();

                            

                            }

                        }
                        reader.Close();
                        reader.Dispose();
                    }

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
                return resultado;
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        

        

        public List<TBClabes.ClabeCliente> ObtenerClabesCliente(int Accion, int idcliente, int idsolicitud, string cclabe)
        {
            List<TBClabes.ClabeCliente> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("clientes.SelClabes"))
				{


					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@idcliente", DbType.Int32, idcliente);
                    db.AddInParameter(com, "@idsolicitud", DbType.Int32, idsolicitud);
                    db.AddInParameter(com, "@Clabe", DbType.String, cclabe);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
                            resultado = new List<TBClabes.ClabeCliente>();
							//Lectura de los datos del ResultSet

                            while (reader.Read())
                            {
                                TBClabes.ClabeCliente clabe = new TBClabes.ClabeCliente();
                                if (!reader.IsDBNull(0)) clabe.IdClabe = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) clabe.Clabe = reader[1].ToString();

                                resultado.Add(clabe);

                            }

						}
                        reader.Close();
						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarTBClabes()
        {
        }

        public void EliminarTBClabes()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}