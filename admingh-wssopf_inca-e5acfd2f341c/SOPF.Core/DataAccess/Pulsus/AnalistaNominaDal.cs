#pragma warning disable 150830
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Pulsus;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: AnalistaNominaDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla AnalistaNomina
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Pulsus
{
    public class AnalistaNominaDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public AnalistaNominaDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarAnalistaNomina(AnalistaNomina entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<AnalistaNomina> ObtenerAnalistaNomina(int Accion, int IdUsuario, int IdAnalista)
        {
			List<AnalistaNomina> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Pulsus.Sel_AnalistaNomina"))
				{
					//Parametros
                    db.AddInParameter(com, "@idAnalista", DbType.Int32, IdAnalista);
                    db.AddInParameter(com, "@idUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<AnalistaNomina>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                AnalistaNomina analista = new AnalistaNomina();
                                if (!reader.IsDBNull(0)) analista.IdAnalista = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) analista.Idusuario = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) analista.Email = reader[2].ToString();
                                if (!reader.IsDBNull(3)) analista.IdEstatus = Convert.ToBoolean(reader[3]);
                                resultado.Add(analista);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarAnalistaNomina()
        {
        }

        public void EliminarAnalistaNomina()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}