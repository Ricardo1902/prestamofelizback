#pragma warning disable 150817
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;

using SOPF.Core.Entities.Pulsus;
using SOPF.Core.Entities.Creditos;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: ArchivoDispersionDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla ArchivoDispersion
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-17 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Pulsus
{
    public class ArchivoDispersionDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public ArchivoDispersionDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarArchivoDispersion(ArchivoDispersion entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Pulsus.ABCArchivoDispersion"))
				{
					//Parametros
                    db.AddInParameter(com, "@Clave", DbType.String, entidad.Clave);
                    db.AddInParameter(com, "@Monto", DbType.Decimal, entidad.Monto);
                    db.AddInParameter(com, "@RFC", DbType.String, entidad.RFC);
                    db.AddInParameter(com, "@CuentaOrigen", DbType.String, entidad.CuentaOrigen);

				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public ArchivoDispersion ObtenerArchivoDispersion()
        {
			ArchivoDispersion resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new ArchivoDispersion();
							//Lectura de los datos del ResultSet
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        public List<CuentasDispersion> EmpleadosDispersion(int idbanco)
        {
            List<CuentasDispersion> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Pulsus.SelDispersiones"))
                {
                    //Parametros
                    db.AddInParameter(com, "@idBanco", DbType.Int32, idbanco);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<CuentasDispersion>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                CuentasDispersion archivo = new CuentasDispersion();
                                if (!reader.IsDBNull(0)) archivo.Operacion = reader[0].ToString();
                                if (!reader.IsDBNull(1)) archivo.ClaveId = reader[1].ToString();
                                if (!reader.IsDBNull(2)) archivo.CuentaOrigen = reader[2].ToString();
                                if (!reader.IsDBNull(3)) archivo.CuentaDestino = reader[3].ToString();
                                if (!reader.IsDBNull(4)) archivo.MontoTransferencia = Convert.ToString(Math.Round(Convert.ToDecimal(reader[4]),2));
                                if (!reader.IsDBNull(5)) archivo.Referencia = reader[5].ToString();
                                if (!reader.IsDBNull(6)) archivo.Descripcion = reader[6].ToString();
                                if (!reader.IsDBNull(7)) archivo.RFC = reader[7].ToString();
                                if (!reader.IsDBNull(8)) archivo.iva = Convert.ToInt32(reader[8]);
                                if (!reader.IsDBNull(9)) archivo.instruccion = reader[9].ToString();
                                if (!reader.IsDBNull(10)) archivo.TipoCambio = Convert.ToInt32(reader[10]);
                                if (!reader.IsDBNull(11)) archivo.fecha = reader[11].ToString();
                                
                                resultado.Add(archivo);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        
        public void ActualizarArchivoDispersion()
        {
        }

        public void EliminarArchivoDispersion()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}