#pragma warning disable 150830
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Pulsus;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: RegistrosDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla Registros
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Pulsus
{
    public class RegistrosDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public RegistrosDal()
		{
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarRegistros(Registros entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Pulsus.insertarRegistros"))
				{
					//Parametros
                    db.AddInParameter(com, "@campo1", DbType.String, entidad.Campo1);
                    db.AddInParameter(com, "@campo2", DbType.String, entidad.Campo2);
                    db.AddInParameter(com, "@campo3", DbType.String, entidad.Campo3);
                    db.AddInParameter(com, "@campo4", DbType.String, entidad.Campo4);
                    db.AddInParameter(com, "@campo5", DbType.String, entidad.Campo5);
                    db.AddInParameter(com, "@campo6", DbType.String, entidad.Campo6);
                    db.AddInParameter(com, "@campo7", DbType.String, entidad.Campo7);
                    db.AddInParameter(com, "@campo8", DbType.String, entidad.Campo8);
                    db.AddInParameter(com, "@campo9", DbType.String, entidad.Campo9);                    
                    db.AddInParameter(com, "@campo10", DbType.String, entidad.Campo10);
                    db.AddInParameter(com, "@campo11", DbType.String, entidad.Campo11);
                    db.AddInParameter(com, "@campo12", DbType.String, entidad.Campo12);
                    db.AddInParameter(com, "@campo13", DbType.String, entidad.Campo13);
                    db.AddInParameter(com, "@campo14", DbType.String, entidad.Campo14);
                    db.AddInParameter(com, "@campo15", DbType.String, entidad.Campo15);
                    db.AddInParameter(com, "@campo16", DbType.String, entidad.Campo16);
                    db.AddInParameter(com, "@campo17", DbType.String, entidad.Campo17);
                    db.AddInParameter(com, "@campo18", DbType.String, entidad.Campo18);
                    db.AddInParameter(com, "@campo19", DbType.String, entidad.Campo19);
                    db.AddInParameter(com, "@campo20", DbType.String, entidad.Campo20);
                    db.AddInParameter(com, "@campo21", DbType.String, entidad.Campo21);
                    db.AddInParameter(com, "@campo22", DbType.String, entidad.Campo22);
                    db.AddInParameter(com, "@campo23", DbType.String, entidad.Campo23);
                    db.AddInParameter(com, "@campo24", DbType.String, entidad.Campo24);
                    db.AddInParameter(com, "@campo25", DbType.String, entidad.Campo25);
                    db.AddInParameter(com, "@campo26", DbType.String, entidad.Campo26);
                    db.AddInParameter(com, "@campo27", DbType.String, entidad.Campo27);
                    db.AddInParameter(com, "@campo28", DbType.String, entidad.Campo28);
                    db.AddInParameter(com, "@campo29", DbType.String, entidad.Campo29);
                    db.AddInParameter(com, "@campo30", DbType.String, entidad.Campo30);
                    db.AddInParameter(com, "@campo31", DbType.String, entidad.Campo31);
                    db.AddInParameter(com, "@campo32", DbType.String, entidad.Campo32);
                    db.AddInParameter(com, "@campo33", DbType.String, entidad.Campo33);
                    db.AddInParameter(com, "@campo34", DbType.String, entidad.Campo34);
                    db.AddInParameter(com, "@campo35", DbType.String, entidad.Campo35);
                    db.AddInParameter(com, "@campo36", DbType.String, entidad.Campo36);
                    db.AddInParameter(com, "@campo37", DbType.String, entidad.Campo37);
                    
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public Registros ObtenerRegistros()
        {
			Registros resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new Registros();
							//Lectura de los datos del ResultSet
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarRegistros()
        {
        }

        public void EliminarRegistros()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}