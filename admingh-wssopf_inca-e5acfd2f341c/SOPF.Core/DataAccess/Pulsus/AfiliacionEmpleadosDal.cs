﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Pulsus;

namespace SOPF.Core.DataAccess.Pulsus
{
    public class AfiliacionEmpleadosDal
    {
         #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public AfiliacionEmpleadosDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
        public List<AfiliacionEmpleados> CuentasAfiliar(int Accion)
        {
            List<AfiliacionEmpleados> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Pulsus.SelAfiliacionEmpleados"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 1);
                    


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<AfiliacionEmpleados>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                AfiliacionEmpleados afiliacion = new AfiliacionEmpleados();
                                if (!reader.IsDBNull(0)) afiliacion.IdAfiliacion = reader[0].ToString();
                                if (!reader.IsDBNull(1)) afiliacion.ClaveId = reader[1].ToString();
                                if (!reader.IsDBNull(2)) afiliacion.Nombre = reader[2].ToString();
                                if (!reader.IsDBNull(3)) afiliacion.RFC = reader[3].ToString();
                                if (!reader.IsDBNull(4)) afiliacion.Telefono = reader[4].ToString();
                                if (!reader.IsDBNull(5)) afiliacion.Moneda = reader[5].ToString();
                                if (!reader.IsDBNull(6)) afiliacion.Banco = reader[6].ToString();
                                if (!reader.IsDBNull(7)) afiliacion.Clabe = reader[7].ToString();
                                
                                if (!reader.IsDBNull(8)) afiliacion.TipoCuenta = Convert.ToString(reader[8]);
                                //if (!reader.IsDBNull(11)) afiliacion.ClienteExistente = Convert.ToInt32(reader[11]);
                                resultado.Add(afiliacion);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        public void insertatEmpleado(AfiliacionEmpleados Empleado)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Pulsus.AltaAfiliacion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Clave", DbType.String, Empleado.ClaveId);
                    db.AddInParameter(com, "@Nombre", DbType.String, Empleado.Nombre);
                    db.AddInParameter(com, "@RFC", DbType.String, Empleado.RFC);
                    db.AddInParameter(com, "@Telefono", DbType.String, Empleado.Telefono);
                    db.AddInParameter(com, "@Clabe", DbType.String, Empleado.Clabe);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ActualizaEmpleado(int idAfiliacion)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Pulsus.UpdAfiliacion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@idAfiliacion", DbType.Int32, idAfiliacion);
                    

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
