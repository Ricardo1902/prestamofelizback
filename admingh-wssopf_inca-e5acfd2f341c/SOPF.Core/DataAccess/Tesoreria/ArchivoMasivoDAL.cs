﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Tesoreria;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.Entities.Catalogo;

namespace SOPF.Core.DataAccess.Tesoreria
{
    public class ArchivoMasivoDAL
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public ArchivoMasivoDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region "Domiciliacion"
        public List<ArchivoMasivo> Filtrar(ArchivoMasivo Entity)
        {
            List<ArchivoMasivo> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ABCArchivo"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "FLT"); // Filtrar
                    db.AddInParameter(com, "@IdArchivo", DbType.Int32, (Entity.IdArchivo > 0) ? Entity.IdArchivo : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdTipoArchivo", DbType.Int32, (Entity.TipoArchivo.IdTipoArchivo > 0) ? Entity.TipoArchivo.IdTipoArchivo : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdEstatusArchivo", DbType.Int32, (Entity.EstatusArchivo.IdEstatusArchivo > 0) ? Entity.EstatusArchivo.IdEstatusArchivo : (object)DBNull.Value);
                    db.AddInParameter(com, "@NombreArchivo", DbType.String, Entity.Nombre);                    

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            ArchivoMasivo objC = null;
                            objReturn = new List<ArchivoMasivo>();
                            while (reader.Read())
                            {
                                objC = new ArchivoMasivo();

                                //Lectura de los datos del ResultSet
                                objC.IdArchivo = Utils.GetInt(reader, "IdArchivo");
                                objC.TipoArchivo.IdTipoArchivo = Utils.GetInt(reader, "IdTipoArchivo");
                                objC.TipoArchivo.EntidadFinanciera = Utils.GetString(reader, "EntidadFinanciera");
                                objC.TipoArchivo.Nombre = Utils.GetString(reader, "TipoArchivo_Nombre");
                                objC.TipoArchivo.Descripcion = Utils.GetString(reader, "TipoArchivo_Descripcion");
                                objC.EstatusArchivo.IdEstatusArchivo = Utils.GetInt(reader, "IdEstatusArchivo");
                                objC.Nombre = Utils.GetString(reader, "Nombre");                                
                                objC.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro");

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> Registrar(ArchivoMasivo Entity)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ABCArchivo"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "INS_ARCHIVO"); // Registrar
                    db.AddInParameter(com, "@IdArchivo", DbType.Int32, (Entity.IdArchivo > 0) ? Entity.IdArchivo : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdTipoArchivo", DbType.Int32, (Entity.TipoArchivo.IdTipoArchivo > 0) ? Entity.TipoArchivo.IdTipoArchivo : (object)DBNull.Value);
                    db.AddInParameter(com, "@NombreArchivo", DbType.String, Entity.Nombre);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<ArchivoMasivo> ObtenerProcesados_Domiciliacion()
        {
            List<ArchivoMasivo> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ABCArchivo"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_DOMIC_CORRECTOS"); // Obtener Domiciliacion Procesados Correctamente

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            ArchivoMasivo objC = null;
                            objReturn = new List<ArchivoMasivo>();
                            while (reader.Read())
                            {
                                objC = new ArchivoMasivo();

                                //Lectura de los datos del ResultSet
                                objC.IdArchivo = Utils.GetInt(reader, "IdArchivo");
                                objC.TipoArchivo.IdTipoArchivo = Utils.GetInt(reader, "IdTipoArchivo");
                                objC.TipoArchivo.EntidadFinanciera = Utils.GetString(reader, "EntidadFinanciera");
                                objC.EstatusArchivo.IdEstatusArchivo = Utils.GetInt(reader, "IdEstatusArchivo");
                                objC.Nombre = Utils.GetString(reader, "Nombre");
                                objC.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro");
                                objC.TotalMonto = Utils.GetDecimal(reader, "TotalMonto");
                                objC.TotalRegistros = Utils.GetInt(reader, "TotalRegistros");

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<ArchivoMasivo> ObtenerRechazados_Domiciliacion()
        {
            List<ArchivoMasivo> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ABCArchivo"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_DOMIC_RECHAZADOS"); // Obtener Rechazados Domiciliacion

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            ArchivoMasivo objC = null;
                            objReturn = new List<ArchivoMasivo>();
                            while (reader.Read())
                            {
                                objC = new ArchivoMasivo();

                                //Lectura de los datos del ResultSet
                                objC.IdArchivo = Utils.GetInt(reader, "IdArchivo");
                                objC.TipoArchivo.IdTipoArchivo = Utils.GetInt(reader, "IdTipoArchivo");
                                objC.TipoArchivo.EntidadFinanciera = Utils.GetString(reader, "EntidadFinanciera");
                                objC.EstatusArchivo.IdEstatusArchivo = Utils.GetInt(reader, "IdEstatusArchivo");
                                objC.Nombre = Utils.GetString(reader, "Nombre");
                                objC.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro");
                                objC.TotalMonto = Utils.GetDecimal(reader, "TotalMonto");
                                objC.TotalRegistros = Utils.GetInt(reader, "TotalRegistros");

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<ArchivoMasivo> ObtenerConError_Domiciliacion()
        {
            List<ArchivoMasivo> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ABCArchivo"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_DOMIC_ERROR"); // Obtener Domiciliacion con Error

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            ArchivoMasivo objC = null;
                            objReturn = new List<ArchivoMasivo>();
                            while (reader.Read())
                            {
                                objC = new ArchivoMasivo();

                                //Lectura de los datos del ResultSet
                                objC.IdArchivo = Utils.GetInt(reader, "IdArchivo");
                                objC.TipoArchivo.IdTipoArchivo = Utils.GetInt(reader, "IdTipoArchivo");
                                objC.TipoArchivo.EntidadFinanciera = Utils.GetString(reader, "EntidadFinanciera");
                                objC.TipoArchivo.Nombre = Utils.GetString(reader, "TipoArchivo_Nombre");
                                objC.TipoArchivo.Descripcion = Utils.GetString(reader, "TipoArchivo_Descripcion");
                                objC.EstatusArchivo.IdEstatusArchivo = Utils.GetInt(reader, "IdEstatusArchivo");
                                objC.Nombre = Utils.GetString(reader, "Nombre");
                                objC.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro");                                

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<ArchivoMasivo> ObtenerProcesados_PagosDirectos()
        {
            List<ArchivoMasivo> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ABCArchivo"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_PAGOSDIRECTOS_CORRECTOS"); // Obtener Pagos Directos Procesados Correctamente

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            ArchivoMasivo objC = null;
                            objReturn = new List<ArchivoMasivo>();
                            while (reader.Read())
                            {
                                objC = new ArchivoMasivo();

                                //Lectura de los datos del ResultSet
                                objC.IdArchivo = Utils.GetInt(reader, "IdArchivo");
                                objC.TipoArchivo.IdTipoArchivo = Utils.GetInt(reader, "IdTipoArchivo");
                                objC.TipoArchivo.EntidadFinanciera = Utils.GetString(reader, "EntidadFinanciera");
                                objC.EstatusArchivo.IdEstatusArchivo = Utils.GetInt(reader, "IdEstatusArchivo");
                                objC.Nombre = Utils.GetString(reader, "Nombre");
                                objC.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro");
                                objC.TotalMonto = Utils.GetDecimal(reader, "TotalMonto");
                                objC.TotalRegistros = Utils.GetInt(reader, "TotalRegistros");

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<ArchivoMasivo> ObtenerConError_PagosDirectos()
        {
            List<ArchivoMasivo> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ABCArchivo"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_PAGOSDIRECTOS_ERROR"); // Obtener Pagos Directos con Error

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            ArchivoMasivo objC = null;
                            objReturn = new List<ArchivoMasivo>();
                            while (reader.Read())
                            {
                                objC = new ArchivoMasivo();

                                //Lectura de los datos del ResultSet
                                objC.IdArchivo = Utils.GetInt(reader, "IdArchivo");
                                objC.TipoArchivo.IdTipoArchivo = Utils.GetInt(reader, "IdTipoArchivo");
                                objC.TipoArchivo.EntidadFinanciera = Utils.GetString(reader, "EntidadFinanciera");
                                objC.TipoArchivo.Nombre = Utils.GetString(reader, "TipoArchivo_Nombre");
                                objC.TipoArchivo.Descripcion = Utils.GetString(reader, "TipoArchivo_Descripcion");
                                objC.EstatusArchivo.IdEstatusArchivo = Utils.GetInt(reader, "IdEstatusArchivo");
                                objC.Nombre = Utils.GetString(reader, "Nombre");
                                objC.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro");

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public ArchivoMasivo ObtenerDetalle(int IdArchivo)
        {
            ArchivoMasivo objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ABCArchivo"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_ARCHIVO_DETALLE"); // Obtener Detalle de Archivo
                    db.AddInParameter(com, "@IdArchivo", DbType.Int32, IdArchivo);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            ArchivoMasivoDetalle objC = null;
                            objReturn = Filtrar(new ArchivoMasivo() { IdArchivo = IdArchivo })?.FirstOrDefault();
                            while (reader.Read())
                            {
                                objC = new ArchivoMasivoDetalle();

                                //Lectura de los datos del ResultSet
                                objC.IdArchivo = Utils.GetInt(reader, "IdArchivo");
                                objC.Linea = Utils.GetInt(reader, "Linea");
                                objC.Descripcion = Utils.GetString(reader, "Descripcion");

                                objReturn.Detalle.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        #endregion

        #region "Domiciliacion Net Cash BBVA"

        public Resultado<bool> RegistrarEnvioDomiBBVA(ArchivoMasivo Entity)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ArchivoALT"))
                {
                    //Parametros
                    //db.AddInParameter(com, "@Accion", DbType.String, "INS_ARCHIVO"); // Registrar
                    db.AddInParameter(com, "@IdArchivo", DbType.Int32, (Entity.IdArchivo > 0) ? Entity.IdArchivo : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdTipoArchivo", DbType.Int32, (Entity.TipoArchivo.IdTipoArchivo > 0) ? Entity.TipoArchivo.IdTipoArchivo : (object)DBNull.Value);
                    db.AddInParameter(com, "@NombreArchivo", DbType.String, Entity.Nombre);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> RegistrarRespuestaDomiBBVA(string nombresRespuestas, string nombreArchivo, int numArchivos, int tipoPagosDirectos)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("cobranza.SP_EnvioDomiciliacionBBVAACT"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdTipoArchivo", DbType.Int32, tipoPagosDirectos);
                    db.AddInParameter(com, "@NombreArchivoDomi", DbType.String, nombreArchivo);
                    db.AddInParameter(com, "@nombresRespuestas", DbType.String, nombresRespuestas);
                    db.AddInParameter(com, "@numArchivos", DbType.String, numArchivos);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }

                //SqlException ex;

                objReturn.Codigo = 0;
                objReturn.Mensaje = "Proceso Terminado";
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<EnvioDomiBBVA> DescragaTXTDomiBBVA(string nombreArchivo)
        {
            List<EnvioDomiBBVA> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_EnvioDomiciliacionBBVACON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@NombreArchivo", DbType.String, nombreArchivo);
                    
                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<EnvioDomiBBVA>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                EnvioDomiBBVA Solicitudes = new EnvioDomiBBVA();
                                if (!reader.IsDBNull(0)) Solicitudes.RowInfo = reader[0].ToString();

                                resultado.Add(Solicitudes);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<FitrarRespuestaBBVA> FiltrarRespuestaDomiBBVA(string nombreArchivo)
        {
            List<FitrarRespuestaBBVA> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_RegistroEnviosDomiBBVACON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@NombreArchivo", DbType.String, nombreArchivo);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<FitrarRespuestaBBVA>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                FitrarRespuestaBBVA Solicitudes = new FitrarRespuestaBBVA();
                                if (!reader.IsDBNull(0)) Solicitudes.idArchivo = Convert.ToInt32(reader[0].ToString());
                                if (!reader.IsDBNull(1)) Solicitudes.TotalRegistros = Convert.ToInt32(reader[1].ToString());
                                if (!reader.IsDBNull(2)) Solicitudes.MontoCobrar = float.Parse(reader[2].ToString());
                                if (!reader.IsDBNull(3)) Solicitudes.TotalArchivos = Convert.ToInt32(reader[3].ToString());


                                resultado.Add(Solicitudes);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<CAT_Parametros> ObtenerParametros(int IdParametro, string ClaveParametro)
        {
            List<CAT_Parametros> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Catalogo.SP_CATParametrosCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdParametro", DbType.Int32, IdParametro);
                    db.AddInParameter(com, "@Clave", DbType.String, ClaveParametro);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            CAT_Parametros objC = null;
                            resultado = new List<CAT_Parametros>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                objC = new CAT_Parametros();

                                objC.IdParametro = Utils.GetInt(reader, "Id_Parametro");
                                objC.Clave = Utils.GetString(reader, "Clave"); 
                                objC.ValorInt = Utils.GetInt(reader, "Val_Int");

                                resultado.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<ArchivoMasivo> ObtenerArchivos_SubidosBBVA()
        {
            List<ArchivoMasivo> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ArchivoBBVACON"))
                {                   
                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            ArchivoMasivo objC = null;
                            objReturn = new List<ArchivoMasivo>();
                            while (reader.Read())
                            {
                                objC = new ArchivoMasivo();

                                //Lectura de los datos del ResultSet
                                objC.IdArchivo = Utils.GetInt(reader, "IdArchivo");
                                objC.TipoArchivo.IdTipoArchivo = Utils.GetInt(reader, "IdTipoArchivo");
                                objC.TipoArchivo.EntidadFinanciera = Utils.GetString(reader, "EntidadFinanciera");
                                objC.EstatusArchivo.IdEstatusArchivo = Utils.GetInt(reader, "IdEstatusArchivo");
                                objC.Nombre = Utils.GetString(reader, "Nombre");
                                objC.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro");
                                objC.TotalMonto = Utils.GetDecimal(reader, "TotalMonto");
                                objC.TotalRegistros = Utils.GetInt(reader, "TotalRegistros");

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<ArchivoMasivo> ObtenerArchivos_GeneradosBBVA()
        {
            List<ArchivoMasivo> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ArchivoGeneradoBBVACON"))
                {                   
                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            ArchivoMasivo objC = null;
                            objReturn = new List<ArchivoMasivo>();
                            while (reader.Read())
                            {
                                objC = new ArchivoMasivo();

                                //Lectura de los datos del ResultSet
                                objC.IdArchivo = Utils.GetInt(reader, "IdArchivo");
                                objC.TipoArchivo.IdTipoArchivo = Utils.GetInt(reader, "IdTipoArchivo");
                                objC.TipoArchivo.EntidadFinanciera = Utils.GetString(reader, "EntidadFinanciera");
                                objC.EstatusArchivo.IdEstatusArchivo = Utils.GetInt(reader, "IdEstatusArchivo");
                                objC.Nombre = Utils.GetString(reader, "Nombre");
                                objC.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro");
                                objC.TotalMonto = Utils.GetDecimal(reader, "TotalMonto");
                                objC.TotalRegistros = Utils.GetInt(reader, "TotalRegistros");

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<HistorialCobros> ObtenerHistorialSeguimientoDomiBBVA(int IdArchivo)
        {
            List<HistorialCobros> resultado = null;
            //List<TBCreditos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("cobranza.SP_HISRegistroEnviosDomiBBVACON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@id_archivo", DbType.String, IdArchivo);
                    //db.AddInParameter(com, "@IdSolicitud", DbType.Int32, cuenta);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            HistorialCobros historia = null;
                            resultado = new List<HistorialCobros>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                historia = new HistorialCobros();
                                //Lectura de los datos del ResultSet
                                historia.IdRegistro = Utils.GetInt(reader, "IdRegistro"); 
                                historia.Id_archivo = Utils.GetInt(reader, "Id_archivo"); 
                                historia.total_registros = Utils.GetInt(reader, "total_registros"); 
                                historia.monto_cobrar = Utils.GetDecimal(reader, "monto_cobrar"); 
                                historia.total_archivos = Utils.GetInt(reader, "total_archivos"); 
                                historia.fechaEnvio = Utils.GetDateTime(reader, "fechaEnvio"); 
                                historia.numero_registros_cobrados = Utils.GetInt(reader, "numero_registros_cobrados");  
                                historia.monto_cobrado = Utils.GetDecimal(reader, "monto_cobrado"); 
                                historia.fechaAplicado = Utils.GetString(reader, "fechaAplicado"); 

                                resultado.Add(historia);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        #endregion
    }
}
