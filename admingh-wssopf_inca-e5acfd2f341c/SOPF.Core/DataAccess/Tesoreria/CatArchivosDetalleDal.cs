#pragma warning disable 141124
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Tesoreria;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatArchivosDetalleDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla CatArchivosDetalle
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-11-24 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Tesoreria
{
    public class CatArchivosDetalleDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public CatArchivosDetalleDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarCatArchivosDetalle(CatArchivosDetalle entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<CatArchivosDetalle> ObtenerCatArchivosDetalle(int idArchivo, string seccion)
        {
			List<CatArchivosDetalle> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Tesoreria.Sel_CatArchivoDet"))
				{
					//Parametros
                    db.AddInParameter(com, "@IdArchivo", DbType.Int32, idArchivo);
                    db.AddInParameter(com, "@Seccion", DbType.String, seccion);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<CatArchivosDetalle>();
                            while (reader.Read())
                            {
                                CatArchivosDetalle archivoDetalle = new CatArchivosDetalle();
                                if (!reader.IsDBNull(0)) archivoDetalle.IdArchivo = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) archivoDetalle.Descripcion = reader[1].ToString();
                                if (!reader.IsDBNull(2)) archivoDetalle.IdCampo = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) archivoDetalle.IdTipoCampo = reader[3].ToString();
                                if (!reader.IsDBNull(4)) archivoDetalle.Longitud = Convert.ToInt32(reader[4]);
                                if (!reader.IsDBNull(5)) archivoDetalle.Enteros = Convert.ToInt32(reader[5]);
                                if (!reader.IsDBNull(6)) archivoDetalle.Decimales = Convert.ToInt32(reader[6]);
                                if (!reader.IsDBNull(7)) archivoDetalle.Incluyepunto = Convert.ToBoolean(reader[7]);
                                if (!reader.IsDBNull(8)) archivoDetalle.Alineacion = reader[8].ToString();
                                if (!reader.IsDBNull(9)) archivoDetalle.Relleno = reader[9].ToString();
                                if (!reader.IsDBNull(10)) archivoDetalle.TextiFijo = reader[10].ToString();
                                if (!reader.IsDBNull(11)) archivoDetalle.FormatoFecha = reader[11].ToString();
                                if (!reader.IsDBNull(12)) archivoDetalle.Orden = Convert.ToInt32(reader[12]);
                                resultado.Add(archivoDetalle);
                            }

						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarCatArchivosDetalle()
        {
        }

        public void EliminarCatArchivosDetalle()
        {
        }

        public void GeneraMoraConfig(int idGrupo, int idPlantilla)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("cobranzaAdministrativa.GeneraMoraConfig"))
                {
                    //Parametros
                    db.AddInParameter(com, "@idGrupo", DbType.Int32, idGrupo);
                    db.AddInParameter(com, "@idPlantilla", DbType.Int32, idPlantilla);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}