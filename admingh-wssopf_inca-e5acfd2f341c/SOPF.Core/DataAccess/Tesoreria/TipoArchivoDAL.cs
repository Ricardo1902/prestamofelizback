﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Tesoreria;

namespace SOPF.Core.DataAccess.Tesoreria
{
    public class TipoArchivoDAL
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TipoArchivoDAL()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public List<TipoArchivo> Filtrar(TipoArchivo Entity)
        {
            List<TipoArchivo> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ABCTipoArchivo"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 1); // Filtrar
                    db.AddInParameter(com, "@IdTipoArchivo", DbType.Int32, (Entity.IdTipoArchivo > 0) ? Entity.IdTipoArchivo : (object)DBNull.Value);
                    db.AddInParameter(com, "@Nombre", DbType.String, Entity.Nombre);
                    db.AddInParameter(com, "@Descripcion", DbType.String, Entity.Descripcion);
                    db.AddInParameter(com, "@EntidadFinanciera", DbType.String, Entity.EntidadFinanciera);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            TipoArchivo objC = null;
                            objReturn = new List<TipoArchivo>();
                            while (reader.Read())
                            {
                                objC = new TipoArchivo();

                                //Lectura de los datos del ResultSet
                                objC.IdTipoArchivo = Utils.GetInt(reader, "IdTipoArchivo");
                                objC.Nombre = Utils.GetString(reader, "Nombre");
                                objC.Descripcion = Utils.GetString(reader, "Descripcion");
                                objC.EntidadFinanciera = Utils.GetString(reader, "EntidadFinanciera");

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<TipoArchivo.LayoutArchivo> ObtenerLayoutArchivo(TipoArchivo Entity)
        {
            List<TipoArchivo.LayoutArchivo> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("ArchivoMasivo.SP_ABCLayoutArchivo"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_CONFIG"); // Obtener Configuracion del Tipo de Archivo
                    db.AddInParameter(com, "@IdTipoArchivo", DbType.Int32, (Entity.IdTipoArchivo > 0) ? Entity.IdTipoArchivo : (object)DBNull.Value);                    

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            TipoArchivo.LayoutArchivo objC = null;
                            objReturn = new List<TipoArchivo.LayoutArchivo>();
                            while (reader.Read())
                            {
                                objC = new TipoArchivo.LayoutArchivo();

                                //Lectura de los datos del ResultSet
                                objC.IdTipoArchivo = Utils.GetInt(reader, "IdTipoArchivo");
                                objC.Parametro = Utils.GetString(reader, "Parametro");
                                objC.IniciaPosicion = Utils.GetInt(reader, "IniciaPosicion");
                                objC.Longuitud = Utils.GetInt(reader, "Longuitud");
                                objC.Formato = Utils.GetString(reader, "Formato");

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }
    }
}
