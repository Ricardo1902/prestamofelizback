#pragma warning disable 150126
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Tesoreria;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: ArchivosDispersionDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla ArchivosDispersion
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-01-26 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Tesoreria
{
    public class ArchivosDispersionDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public ArchivosDispersionDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarArchivosDispersion(ArchivosDispersion entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Tesoreria.ABCArchivoDispersion"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 1);
                    db.AddInParameter(com, "@idCorte", DbType.Int32, entidad.IdCorte);
                    db.AddInParameter(com, "@Nombre", DbType.String, entidad.NombreArchivo);
                    db.AddInParameter(com, "@Estatus", DbType.Int32, entidad.Estatus);
                    db.AddInParameter(com, "@IdArchivo", DbType.Int32, entidad.IdArchivo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<ArchivosDispersion> ObtenerArchivosDispersion(int Accion, int idArchivo)
        {
			List<ArchivosDispersion> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Tesoreria.Sel_ArchivosDispersion"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32,Accion);
                    db.AddInParameter(com, "@idArchivo", DbType.Int32, idArchivo);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<ArchivosDispersion>();
                            while (reader.Read())
                            {
                                ArchivosDispersion archivo = new ArchivosDispersion();
                                if (!reader.IsDBNull(0)) archivo.IdArchivo = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) archivo.NombreArchivo = reader[1].ToString();
                                if (!reader.IsDBNull(2)) archivo.IdCorte = Convert.ToInt32(reader[2].ToString());
                                resultado.Add(archivo);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarArchivosDispersion(ArchivosDispersion entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Tesoreria.ABCArchivoDispersion"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 2);
                    db.AddInParameter(com, "@idCorte", DbType.Int32, entidad.IdCorte);
                    db.AddInParameter(com, "@Nombre", DbType.String, entidad.NombreArchivo);
                    db.AddInParameter(com, "@Estatus", DbType.Int32, entidad.Estatus);
                    db.AddInParameter(com, "@IdArchivo", DbType.Int32, entidad.IdArchivo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }

        public void EliminarArchivosDispersion()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}