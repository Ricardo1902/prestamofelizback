#pragma warning disable 141124
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Tesoreria;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatArchivosDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla CatArchivos
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-11-24 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Tesoreria
{
    public class CatArchivosDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public CatArchivosDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarCatArchivos(CatArchivos entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public CatArchivos ObtenerCatArchivos(int IdBanco,  string TipoTran, string Tipo)
        {
			CatArchivos resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Tesoreria.Sel_CatArchivoGral"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, IdBanco);
                    db.AddInParameter(com, "@TipoTran", DbType.String,TipoTran);
                    db.AddInParameter(com, "@Tipo", DbType.String,Tipo);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new CatArchivos();
                            //Lectura de los datos del ResultSet


                            if (reader != null)
                            {

                                while (reader.Read())
                                {

                                    if (!reader.IsDBNull(0)) resultado.IdArchivo = Convert.ToInt32(reader[0]);
                                    if (!reader.IsDBNull(1)) resultado.NombreArchivo = reader[1].ToString();
                                    if (!reader.IsDBNull(2)) resultado.TipoArchivo = reader[2].ToString();
                                    if (!reader.IsDBNull(3)) resultado.Delimitador = reader[3].ToString();
                                    if (!reader.IsDBNull(4)) resultado.FinRegistro = reader[4].ToString();
                                    if (!reader.IsDBNull(5)) resultado.Nombre = reader[5].ToString();
                                    if (!reader.IsDBNull(6)) resultado.Extension = reader[6].ToString();
                                    if (!reader.IsDBNull(7)) resultado.Tipo = reader[7].ToString();
                                    if (!reader.IsDBNull(8)) resultado.Consecutivo = Convert.ToInt32(reader[8]);

                                }


                            }

                            reader.Dispose();
                        }

                        //Cierre de la conexion y liberacion de memoria
                        com.Dispose();
                    }
                }
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public string ObtenerConsecutivo(int IdBanco, string TipoTran, string Tipo)
        {
            CatArchivosCon resultado = null;
            string Consecutivo = string.Empty;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Tesoreria.IncrementaConsecutivo"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, IdBanco);
                    db.AddInParameter(com, "@TipoTran", DbType.String, TipoTran);
                    db.AddInParameter(com, "@Tipo", DbType.String, Tipo);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new CatArchivosCon();
                            //Lectura de los datos del ResultSet


                            if (reader != null)
                            {

                                while (reader.Read())
                                {
                                    if (!reader.IsDBNull(0)) Consecutivo = "0" + reader[0].ToString();
                                }


                            }

                            reader.Dispose();
                        }

                        //Cierre de la conexion y liberacion de memoria
                        com.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Consecutivo.Substring(Consecutivo.Length - 2, 2);
        }
        
        public void ActualizarCatArchivos()
        {
        }

        public void EliminarCatArchivos()
        {
        }
        #endregion

        public void ABCCatParametro(int Accion, string Parametro, string valor, int idEstatus)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_ABCCatParametro"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@Parametro", DbType.String, Parametro);
                    db.AddInParameter(com, "@valor", DbType.String, valor);
                    db.AddInParameter(com, "@idEstatus", DbType.Int32, idEstatus);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AltaArchivosDomiciliacion(string NombreArchivo, int IdBanco, int IdGrupo, int IdPlantilla, string Convenio, string Emisora, int DiasVenIni, int DiasVenFin, int AplicarGastos, int IdUsuario)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("CobranzaAdministrativa.AltaArchivosDomiciliacion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@NombreArchivo", DbType.String, NombreArchivo);
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, IdBanco);
                    db.AddInParameter(com, "@IdGrupo", DbType.Int32, IdGrupo);
                    db.AddInParameter(com, "@IdPlantilla", DbType.Int32, IdPlantilla);
                    db.AddInParameter(com, "@Convenio", DbType.String, Convenio);
                    db.AddInParameter(com, "@Emisora", DbType.String, Emisora);
                    db.AddInParameter(com, "@DiasVenIni", DbType.Int32, DiasVenIni);
                    db.AddInParameter(com, "@DiasVenFin", DbType.Int32, DiasVenFin);
                    db.AddInParameter(com, "@AplicarGastos", DbType.Int32, AplicarGastos);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}