#pragma warning disable 160829
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Entities;

# region Copyright Dimex – 2016
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBBlogComentariosDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBBlogComentarios
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2016-08-29 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Solicitudes
{
    public class TBBlogComentariosDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBBlogComentariosDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public Resultado<bool> InsertarTBBlogComentarios(TBBlogComentarios entidad)
        {
            Resultado<bool> result = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_BlogComentarios_Alt"))
				{
					//Parametros
					db.AddInParameter(com, "@IdSolicitud", DbType.Int32, entidad.IdSolicitud);
                    db.AddInParameter(com, "@IdComentarioPadre", DbType.Int32, entidad.IdComentarioPadre);
                    db.AddInParameter(com, "@comentarios", DbType.String, entidad.Comentarios);
                    db.AddInParameter(com, "@UsuarioAlta", DbType.Int32, entidad.UsuarioAlta);
                    db.AddInParameter(com, "@RolUsuario", DbType.Int32, entidad.RolUsuario);
				
					//Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            result = new Resultado<bool>();
                            //Lectura de los datos del ResultSet                            
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) result.CodigoStr = reader[0].ToString();
                                if (!reader.IsDBNull(1)) result.Mensaje = reader[1].ToString();
                             
                            }
                        }

                        reader.Dispose();
                    }

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
             
			}
			catch (Exception ex)
			{

				throw ex;
			}
            return result;
        }
        
        public TBBlogComentarios ObtenerTBBlogComentarios()
        {
			TBBlogComentarios resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new TBBlogComentarios();
							//Lectura de los datos del ResultSet
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarTBBlogComentarios()
        {
        }

        public void EliminarTBBlogComentarios()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}