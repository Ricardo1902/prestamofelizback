﻿using SOPF.Core.Entities.Solicitudes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace SOPF.Core.DataAccess.Solicitudes
{
    public static class ExtensionesSolicitudes
    {
        public static string ToXML(this List<TBSolicitudes.CuentaDomiciliacionEmergente> lista)
        {
            string xml = string.Empty;

            using (var sw = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sw, new XmlWriterSettings { OmitXmlDeclaration = true }))
                {
                    new XmlSerializer(typeof(List<TBSolicitudes.CuentaDomiciliacionEmergente>), new XmlRootAttribute("Solicitud")).Serialize(writer, lista);
                }
                xml = sw.ToString();
            }

            return xml;
        }
        public static string ToXML<T>(this List<T> lista, string rootAttribute)
        {
            string xml = string.Empty;

            using (var sw = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sw, new XmlWriterSettings { OmitXmlDeclaration = true }))
                {
                    new XmlSerializer(typeof(List<T>), new XmlRootAttribute(rootAttribute)).Serialize(writer, lista);
                }
                xml = sw.ToString();
            }

            return xml;
        }
    }
}
