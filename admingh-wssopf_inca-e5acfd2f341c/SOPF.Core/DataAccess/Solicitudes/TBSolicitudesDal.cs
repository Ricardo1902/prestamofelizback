using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.BusinessLogic.Clientes;
using SOPF.Core.BusinessLogic.Solcitudes;
using SOPF.Core.BusinessLogic.Solicitudes;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Clientes;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Model.Request.Solicitudes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Xml;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TBSolicitudesDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBSolicitudes
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-22 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Solicitudes
{
    public class TBSolicitudesDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TBSolicitudesDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods

        public DataResultSolicitudes InsertarTBSolicitudes(int Accion, int idUsuario, TBSolicitudes entidad)
        {
            DataResultSolicitudes resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_ABCSolicitud"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, entidad.IdSolicitud);
                    string fch_solicitud = ("00" + Convert.ToString(entidad.FchSolicitud.Day)).Substring(("00" + Convert.ToString(entidad.FchSolicitud.Day)).Length - 2) + "/" + ("00" + Convert.ToString(entidad.FchSolicitud.Month)).Substring(("00" + Convert.ToString(entidad.FchSolicitud.Month)).Length - 2) + "/" + ("0000" + Convert.ToString(entidad.FchSolicitud.Year)).Substring(("0000" + Convert.ToString(entidad.FchSolicitud.Year)).Length - 4);
                    db.AddInParameter(com, "@fch_solicitud", DbType.String, fch_solicitud);
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, entidad.IdCliente);
                    db.AddInParameter(com, "@IdCliente_A", DbType.Int32, entidad.IdClienteA);
                    db.AddInParameter(com, "@IdConvenio", DbType.Int32, entidad.IdConvenio);
                    db.AddInParameter(com, "@IdPromotor", DbType.Int32, entidad.IdPromotor);
                    db.AddInParameter(com, "@IdLinea", DbType.Int32, entidad.IdLinea);
                    db.AddInParameter(com, "@erogacion", DbType.Decimal, entidad.Erogacion);
                    db.AddInParameter(com, "@capital", DbType.Decimal, entidad.Capital);
                    db.AddInParameter(com, "@interes", DbType.Decimal, entidad.Interes);
                    db.AddInParameter(com, "@iva_intere", DbType.Decimal, entidad.IvaIntere);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, entidad.IdSucursal);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, entidad.IdEstatus);
                    string fch_estatus = ("00" + Convert.ToString(entidad.FchEstatus.Day)).Substring(("00" + Convert.ToString(entidad.FchEstatus.Day)).Length - 2) + "/" + ("00" + Convert.ToString(entidad.FchEstatus.Month)).Substring(("00" + Convert.ToString(entidad.FchEstatus.Month)).Length - 2) + "/" + ("0000" + Convert.ToString(entidad.FchEstatus.Year)).Substring(("0000" + Convert.ToString(entidad.FchEstatus.Year)).Length - 4);
                    db.AddInParameter(com, "@fch_estatus", DbType.String, fch_estatus);
                    db.AddInParameter(com, "@ingr_liqui", DbType.Decimal, entidad.IngrLiqui);
                    db.AddInParameter(com, "@capa_pago", DbType.Decimal, entidad.CapaPago);
                    db.AddInParameter(com, "@IdAnalista", DbType.Int32, entidad.IdAnalista);
                    db.AddInParameter(com, "@articulos", DbType.Decimal, entidad.Articulos);
                    db.AddInParameter(com, "@apertura", DbType.Decimal, entidad.Apertura);
                    db.AddInParameter(com, "@otro_carg", DbType.Decimal, entidad.OtroCarg);
                    db.AddInParameter(com, "@enganche", DbType.Decimal, entidad.Enganche);
                    string fecha_enga = ("00" + Convert.ToString(entidad.FchEnganche.Day)).Substring(("00" + Convert.ToString(entidad.FchEnganche.Day)).Length - 2) + "/" + ("00" + Convert.ToString(entidad.FchEnganche.Month)).Substring(("00" + Convert.ToString(entidad.FchEnganche.Month)).Length - 2) + "/" + ("0000" + Convert.ToString(entidad.FchEnganche.Year)).Substring(("0000" + Convert.ToString(entidad.FchEnganche.Year)).Length - 4);
                    db.AddInParameter(com, "@fecha_enga", DbType.String, fecha_enga);
                    string fch_Arranq = ("00" + Convert.ToString(entidad.FchArranque.Day)).Substring(("00" + Convert.ToString(entidad.FchArranque.Day)).Length - 2) + "/" + ("00" + Convert.ToString(entidad.FchArranque.Month)).Substring(("00" + Convert.ToString(entidad.FchArranque.Month)).Length - 2) + "/" + ("0000" + Convert.ToString(entidad.FchArranque.Year)).Substring(("0000" + Convert.ToString(entidad.FchArranque.Year)).Length - 4);
                    db.AddInParameter(com, "@fch_Arranq", DbType.String, fch_Arranq);
                    db.AddInParameter(com, "@override", DbType.Decimal, entidad.Override);
                    db.AddInParameter(com, "@overpaid", DbType.Decimal, entidad.Overpaid);
                    db.AddInParameter(com, "@IdProducto", DbType.Int32, entidad.IdProducto);
                    db.AddInParameter(com, "@IdOpero", DbType.Int32, entidad.IdOpero);
                    db.AddInParameter(com, "@dg_articulo", DbType.String, entidad.DgArticulo);
                    db.AddInParameter(com, "@IdDestino", DbType.Int32, entidad.IdDestino);
                    db.AddInParameter(com, "@IdAsignado", DbType.Int32, entidad.IdAsignado);
                    string fch_Suscripcion = ("00" + Convert.ToString(entidad.FchSuscripcion.Day)).Substring(("00" + Convert.ToString(entidad.FchSuscripcion.Day)).Length - 2) + "/" + ("00" + Convert.ToString(entidad.FchSuscripcion.Month)).Substring(("00" + Convert.ToString(entidad.FchSuscripcion.Month)).Length - 2) + "/" + ("0000" + Convert.ToString(entidad.FchSuscripcion.Year)).Substring(("0000" + Convert.ToString(entidad.FchSuscripcion.Year)).Length - 4);
                    db.AddInParameter(com, "@fch_Suscripcion", DbType.String, fch_Suscripcion);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, idUsuario);
                    db.AddInParameter(com, "@idClabeCliente", DbType.Int32, entidad.IdClabeCliente);
                    db.AddInParameter(com, "@isReestructura", DbType.Int32, entidad.IsReestructura);
                    db.AddInParameter(com, "@otraInstitucion", DbType.Int32, entidad.LiqOtraInstitucion);
                    db.AddInParameter(com, "@IdCampania", DbType.Int32, entidad.IdCampania);
                    db.AddInParameter(com, "@IdCliRec", DbType.Int32, entidad.IdClienteRecomienda);
                    db.AddInParameter(com, "@IdClabeClienteRec", DbType.Int32, entidad.IdClabeClienteRec);
                    db.AddInParameter(com, "@IdTipoComision", DbType.Int32, entidad.IdTipoComision);
                    db.AddInParameter(com, "@Pedido", DbType.String, entidad.Pedido);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new DataResultSolicitudes();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) resultado.idMensaje = 1;

                                if (!reader.IsDBNull(0)) resultado.Mensaje = reader[0].ToString();
                            }
                        }
                        reader.Dispose();
                    }
                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
                return resultado;
            }
            catch (Exception ex)
            {
                resultado = new DataResultSolicitudes();
                resultado.idMensaje = 0;
                resultado.Mensaje = ex.ToString();
                return resultado;
            }
        }

        public int UpdEstProceso(int Accion, int idSolicitud, int EstProceso, int idUsuario)
        {
            int resultado = 0;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_UpdEstProcesoSolicitud_PE"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, idSolicitud);
                    db.AddInParameter(com, "@Est_Proceso", DbType.Int32, EstProceso);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, idUsuario);



                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {

                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {

                                if (!reader.IsDBNull(0)) resultado = Convert.ToInt32(reader[0].ToString());
                            }
                        }
                        reader.Dispose();
                    }
                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
                return resultado;
            }
            catch (Exception ex)
            {
                resultado = 1;
                return resultado;
            }
        }

        public List<TBSolicitudes> ObtenerTBSolicitudes(int Accion, int cuenta, int idcliente, string cliente, string fchInicial, string fchFinal, int idsucursal, int idconvenio, int idlinea)
        {
            List<TBSolicitudes> resultado = null;
            //List<TBCreditos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_SelSolicitud"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, cuenta);
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, idcliente);
                    db.AddInParameter(com, "@Cliente", DbType.String, cliente);
                    db.AddInParameter(com, "@FechaInicial", DbType.String, fchInicial);
                    db.AddInParameter(com, "@FechaFinal", DbType.String, fchFinal);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, idsucursal);
                    db.AddInParameter(com, "@IdConvenio", DbType.Int32, idconvenio);
                    db.AddInParameter(com, "@IdLinea", DbType.Int32, idlinea);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBSolicitudes>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBSolicitudes Solicitudes = new TBSolicitudes();
                                if (!reader.IsDBNull(0)) Solicitudes.IdCliente = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Solicitudes.Cliente = reader[1].ToString();
                                if (!reader.IsDBNull(2)) Solicitudes.IdSolicitud = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) Solicitudes.FchSolicitud = Convert.ToDateTime(reader[3]);
                                // if (!reader.IsDBNull(4)) Solicitudes.IdProducto = Convert.ToInt32(reader[4]);
                                //if (!reader.IsDBNull(5)) Solicitudes.Producto = reader[5].ToString();
                                if (!reader.IsDBNull(6)) Solicitudes.Estatus = reader[6].ToString();
                                // if (!reader.IsDBNull(7)) Solicitudes.Motivo = reader[7].ToString();
                                if (!reader.IsDBNull(8)) Solicitudes.Capital = Convert.ToDecimal(reader[8]);
                                //if (!reader.IsDBNull(9)) Solicitudes.Interes = Convert.ToDecimal(reader[9]);
                                //if (!reader.IsDBNull(10)) Solicitudes.iva_intere = Convert.ToDecimal(reader[10]);
                                //if (!reader.IsDBNull(11)) Solicitudes.IdSucursal = Convert.ToInt32(reader[11]);
                                //if (!reader.IsDBNull(12)) Solicitudes.Sucursal = reader[12].ToString();
                                //if (!reader.IsDBNull(13)) Solicitudes.EstProceso = Convert.ToInt32(reader[13]);
                                //if (!reader.IsDBNull(14)) Solicitudes.IdAnalista = Convert.ToInt32(reader[14]);
                                if (!reader.IsDBNull(15)) Solicitudes.Promotor = reader[15].ToString();
                                if (!reader.IsDBNull(16)) Solicitudes.IdConvenio = Convert.ToInt32(reader[16]);
                                if (!reader.IsDBNull(17)) Solicitudes.Convenio = reader[17].ToString();
                                //if (!reader.IsDBNull(18)) Solicitudes.IdLinea = Convert.ToInt32(reader[18]);
                                //if (!reader.IsDBNull(19)) Solicitudes.Linea = reader[19].ToString();
                                //if (!reader.IsDBNull(20)) Solicitudes.Analista = reader[20].ToString();
                                //if (!reader.IsDBNull(21)) Solicitudes.Opero = reader[21].ToString();
                                //if (!reader.IsDBNull(22)) Solicitudes.DescEstatusProceso = reader[22].ToString();
                                //if (!reader.IsDBNull(23)) Solicitudes.PendObse = Convert.ToInt32(reader[23]);
                                //if (!reader.IsDBNull(24)) Solicitudes.Pedido = reader[24].ToString();
                                if (!reader.IsDBNull(25)) Solicitudes.Asignado = reader[25].ToString();
                                //if (!reader.IsDBNull(26)) Solicitudes.AsignadoAnt = reader[26].ToString();
                                if (!reader.IsDBNull(27)) Solicitudes.Observacion = reader[27].ToString();
                                //if (!reader.IsDBNull(28)) Solicitudes.isReestructura = Convert.ToInt32(reader[28]);
                                //if (!reader.IsDBNull(29)) Solicitudes.nroQuitas = Convert.ToInt32(reader[29]);
                                //if (!reader.IsDBNull(30)) Solicitudes.fchUltCondicion = Convert.ToDateTime(reader[30]);
                                //if (!reader.IsDBNull(31)) Solicitudes.nroCondiones = Convert.ToInt32(reader[31]);
                                //if (!reader.IsDBNull(32)) Solicitudes.TotComentarios = Convert.ToInt32(reader[32]);
                                if (!reader.IsDBNull(32)) Solicitudes.Comentarios = reader[32].ToString();
                                if (!reader.IsDBNull(34)) Solicitudes.ConProcesoDigital = Convert.ToBoolean(reader[34].ToString());

                                resultado.Add(Solicitudes);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<TBSolicitudes.DatosSolicitud> ObtenerDatosSolicitud(int idsolicitud)
        {
            List<TBSolicitudes.DatosSolicitud> resultado = null;
            //List<TBCreditos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.Datossolicitud"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_DATOS");
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, idsolicitud);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBSolicitudes.DatosSolicitud>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBSolicitudes.DatosSolicitud Solicitudes = new TBSolicitudes.DatosSolicitud();

                                int x = 0;
                                if (!reader.IsDBNull(x)) Solicitudes.IDSolictud = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.FechaSolicitud = Convert.ToDateTime(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.idTipoConvenio = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.idConvenio = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.IDProducto = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.IDCredito = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.IDCanalVenta = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.TipoCliente = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.IDTipoEvaluacion = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.IDPromotor = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.ImporteCredito = Convert.ToDecimal(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.Plazo = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.TasaInteres = Convert.ToDecimal(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.TipoSeguroDesgravamen = Convert.ToInt32(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.SeguroDesgravamen = Convert.ToDecimal(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.MontoGAT = Convert.ToDecimal(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.MontoUsoCanal = Convert.ToDecimal(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.MontoTotalFinanciar = Convert.ToDecimal(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.CostoTotalCred = Convert.ToDecimal(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.MontoCuota = Convert.ToDecimal(reader[x]); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.Banco = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.CuentaDesembolso = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.CuentaDebito = reader[x].ToString(); x++;
                                if (!reader.IsDBNull(x)) Solicitudes.PrimerPago = Convert.ToDateTime(reader[x]); x++;
                                // JRVB - 10/04/2018, Obtener los datos por medio del nombre de columna.
                                Solicitudes.IdAsignado = Utils.GetInt(reader, "IdAsignado");
                                Solicitudes.IdEstatus = Utils.GetInt(reader, "IdEstatus");
                                Solicitudes.IdEstatusProceso = Utils.GetInt(reader, "Est_Proceso");
                                Solicitudes.FechaDeposito = Utils.GetDateTime(reader, "fch_deposito");
                                Solicitudes.pedido = Utils.GetString(reader, "Pedido");
                                Solicitudes.Deducciones = Utils.GetDecimal(reader, "Deducciones");
                                Solicitudes.NumeroPoliza = Utils.GetString(reader, "NumeroPoliza");
                                Solicitudes.ReferenciaExperian = Utils.GetString(reader, "ReferenciaExperian");
                                Solicitudes.ScoreExperian = Utils.GetDecimal(reader, "ScoreExperian");
                                Solicitudes.NumeroCertificado = Utils.GetString(reader, "NumeroCertificado");
                                Solicitudes.SaldoIniCta = Utils.GetDecimal(reader, "SaldoIniCta");
                                Solicitudes.SaldoIniCta2 = Utils.GetDecimal(reader, "SaldoIniCta2");
                                Solicitudes.SaldoIniCta3 = Utils.GetDecimal(reader, "SaldoIniCta3");
                                Solicitudes.SaldoFinCta = Utils.GetDecimal(reader, "SaldoFinCta");
                                Solicitudes.SaldoFinCta2 = Utils.GetDecimal(reader, "SaldoFinCta2");
                                Solicitudes.SaldoFinCta3 = Utils.GetDecimal(reader, "SaldoFinCta3");
                                Solicitudes.MonDepDia1 = Utils.GetDecimal(reader, "MontoDep1");
                                Solicitudes.MonDepDia2 = Utils.GetDecimal(reader, "MontoDep2");
                                Solicitudes.MonDepDia3 = Utils.GetDecimal(reader, "MontoDep3");
                                Solicitudes.MontoRetDia1 = Utils.GetDecimal(reader, "MontoRetiroDia1");
                                Solicitudes.MontoRetDia2 = Utils.GetDecimal(reader, "MontoRetiroDia2");
                                Solicitudes.MontoRetDia3 = Utils.GetDecimal(reader, "MontoRetiroDia3");
                                Solicitudes.CuentaCCI = Utils.GetString(reader, "CuentaCCI");
                                Solicitudes.TipoDomiciliacion = Utils.GetInt(reader, "TipoDomiciliacion");
                                Solicitudes.NumeroTarjetaVisa = Utils.GetString(reader, "NumeroTarjetaVisa");
                                Solicitudes.MesExpiraTarjetaVisa = Utils.GetInt(reader, "MesExpiraTarjetaVisa");
                                Solicitudes.AnioExpiraTarjetaVisa = Utils.GetInt(reader, "AnioExpiraTarjetaVisa");
                                Solicitudes.TMP_MontoLiquidacion = Utils.GetDecimal(reader, "TMP_MontoLiquidacion");
                                Solicitudes.idCliente = Utils.GetInt(reader, "idCliente");
                                Solicitudes.Origen_Id = Utils.GetInt(reader, "Origen_Id");

                                Solicitudes.IDSubProducto = Utils.GetInt(reader, "IdSubProducto");
                                Solicitudes.vFrecuenciaPago = Utils.GetString(reader, "vFrecuenciaPago");
                                Solicitudes.vMetDispersion = Utils.GetString(reader, "vMetDispersion");
                                Solicitudes.vCVV = Utils.GetString(reader, "vCVV");

                                Solicitudes.CuentasDomiciliacionEmergentes = ObtenerCuentasDomiciliacionEmergente(idsolicitud);
                                resultado.Add(Solicitudes);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public void ActualizarTBSolicitudes()
        {
        }

        public void EliminarTBSolicitudes()
        {
        }

        public int ActualizaComentariosSol(int IdSolicitud, int RolUsuario, int Accion)
        {
            int resultado = 0;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_BlogComentarios_Act"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, IdSolicitud);
                    db.AddInParameter(com, "@RolUsuario", DbType.Int32, RolUsuario);
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = 0;
                            ////Lectura de los datos del ResultSet
                            //while (reader.Read())
                            //{

                            //    //if (!reader.IsDBNull(0)) resultado = Convert.ToInt32(reader[0].ToString());
                            //    resultado = 0;
                            //}
                        }
                        reader.Dispose();
                    }
                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
                return resultado;
            }
            catch (Exception ex)
            {


                resultado = 1;
                return resultado;
            }
        }

        public int AltaComentariosSolicitud(int IdSolicitud, int IdComentarioPadre, string Comentarios, int UsuarioAlta, int RolUsuario)
        {
            int resultado = 0;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_BlogComentarios_Alt"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, IdSolicitud);
                    db.AddInParameter(com, "@IdComentarioPadre", DbType.Int32, IdComentarioPadre);
                    db.AddInParameter(com, "@comentarios", DbType.String, Comentarios);
                    db.AddInParameter(com, "@UsuarioAlta", DbType.Int32, UsuarioAlta);
                    db.AddInParameter(com, "@RolUsuario", DbType.Int32, RolUsuario);



                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = 0;
                            ////Lectura de los datos del ResultSet
                            //while (reader.Read())
                            //{

                            //    //if (!reader.IsDBNull(0)) resultado = Convert.ToInt32(reader[0].ToString());
                            //    resultado = 0;
                            //}
                        }
                        reader.Dispose();
                    }
                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
                return resultado;
            }
            catch (Exception ex)
            {
                resultado = 1;
                return resultado;
            }
        }

        public List<TBSolicitudes.ComentariosSolicitud> ConComentariosSolicitud(int IdSolicitud, int IdComentarioPadre, int TipoConsulta)
        {
            List<TBSolicitudes.ComentariosSolicitud> resultado = null;
            //List<TBCreditos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_BlogComentarios_Con"))
                {
                    //Parametros

                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, IdSolicitud);
                    db.AddInParameter(com, "@IdComentarioPadre", DbType.Int32, IdComentarioPadre);
                    db.AddInParameter(com, "@TipoConsulta", DbType.Int32, TipoConsulta);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBSolicitudes.ComentariosSolicitud>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBSolicitudes.ComentariosSolicitud Comentarios = new TBSolicitudes.ComentariosSolicitud();
                                if (!reader.IsDBNull(0)) Comentarios.IdComentario = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Comentarios.IdSolicitud = Convert.ToInt32(reader[1].ToString());
                                if (!reader.IsDBNull(2)) Comentarios.IdComentarioPadre = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) Comentarios.comentarios = reader[3].ToString();
                                if (!reader.IsDBNull(4)) Comentarios.Fec_Alta = reader[4].ToString();
                                if (!reader.IsDBNull(5)) Comentarios.UsuarioAlta = Convert.ToInt32(reader[5].ToString());
                                if (!reader.IsDBNull(6)) Comentarios.Nom_Usuario = reader[6].ToString();
                                if (!reader.IsDBNull(7)) Comentarios.No_Respuestas = Convert.ToInt32(reader[7].ToString());


                                resultado.Add(Comentarios);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        #region "Peru"
        public DataResultSolicitudes InsertarSolicitud_Peru(int Accion, int IDUsuario, TBSolicitudes.SolicitudPeru entidad)
        {
            DataResultSolicitudes resultado = null;
            int idSolicitud = 0;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("SP_ABCSolicitud")) //Usar el PS Original == Tabla Original
                {
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IDUsuario", DbType.Int32, IDUsuario);
                    db.AddInParameter(com, "@IDSolicitud", DbType.Int32, entidad.IDSolictud);
                    //db.AddInParameter(com, "@FechaSolicitud", DbType.String, entidad.FechaSolicitud);
                    db.AddInParameter(com, "@IDCliente", DbType.Int32, entidad.IDCliente);

                    db.AddInParameter(com, "@IDClienteA", DbType.Int32, entidad.IDClienteA);
                    db.AddInParameter(com, "@IdCliente_A", DbType.Int32, entidad.IDClienteA);

                    db.AddInParameter(com, "@IDProducto_PE", DbType.Int32, entidad.IDConvenio);
                    db.AddInParameter(com, "@IdSubProducto", DbType.Int32, entidad.IDSubProducto);
                    db.AddInParameter(com, "@IDCredito_PE", DbType.Int32, entidad.IDCredito);
                    db.AddInParameter(com, "@IDCanalVenta_PE", DbType.Int32, entidad.IDCanalVenta);
                    db.AddInParameter(com, "@TipoCliente", DbType.Int32, entidad.TipoCliente);
                    db.AddInParameter(com, "@TipoEvaluacion_PE", DbType.Int32, entidad.IDTipoEvaluacion);
                    db.AddInParameter(com, "@IDPromotor", DbType.Int32, entidad.IDPromotor);
                    db.AddInParameter(com, "@IdOrigen", DbType.Int32, entidad.IDOrigen);
                    db.AddInParameter(com, "@ImporteCredito", DbType.Decimal, entidad.ImporteCredito);
                    db.AddInParameter(com, "@Plazo", DbType.Int32, entidad.Plazo);
                    //db.AddInParameter(com, "@Comision", DbType.Decimal, entidad.Comision);
                    db.AddInParameter(com, "@TasaInteres", DbType.Decimal, entidad.TasaInteres);
                    db.AddInParameter(com, "@TipoSeguroDesgravamen", DbType.Decimal, entidad.TipoSeguroDesgravamen);
                    db.AddInParameter(com, "@SeguroDesgravamen", DbType.Decimal, entidad.SeguroDesgravamen);
                    db.AddInParameter(com, "@MontoGAT", DbType.Decimal, entidad.MontoGAT);
                    db.AddInParameter(com, "@MontoUsoCanal", DbType.Decimal, entidad.MontoUsoCanal);
                    db.AddInParameter(com, "@MontoTotalFinanciar", DbType.Decimal, entidad.MontoTotalFinanciar);
                    db.AddInParameter(com, "@CostoTotalCred", DbType.Decimal, entidad.CostoTotalCred);
                    db.AddInParameter(com, "@MontoCuota", DbType.Decimal, entidad.MontoCuota);
                    db.AddInParameter(com, "@Banco", DbType.String, entidad.Banco);
                    db.AddInParameter(com, "@CuentaDesembolso", DbType.String, entidad.CuentaDesembolso);
                    db.AddInParameter(com, "@CuentaDebito", DbType.String, entidad.CuentaDebito);
                    db.AddInParameter(com, "@CuentaCCI", DbType.String, entidad.CuentaCCI);
                    db.AddInParameter(com, "@PrimerPago", DbType.String, entidad.PrimerPago.ToString("yyyy/MM/dd"));

                    entidad.FechaSolicitud = DateTime.Now;
                    string fch_solicitud = ("00" + Convert.ToString(entidad.FechaSolicitud.Day)).Substring(("00" + Convert.ToString(entidad.FechaSolicitud.Day)).Length - 2) + "/" + ("00" + Convert.ToString(entidad.FechaSolicitud.Month)).Substring(("00" + Convert.ToString(entidad.FechaSolicitud.Month)).Length - 2) + "/" + ("0000" + Convert.ToString(entidad.FechaSolicitud.Year)).Substring(("0000" + Convert.ToString(entidad.FechaSolicitud.Year)).Length - 4);
                    db.AddInParameter(com, "@fch_solicitud", DbType.String, fch_solicitud);
                    db.AddInParameter(com, "@IdConvenio", DbType.Int32, entidad.IDConvenio);
                    db.AddInParameter(com, "@IdLinea", DbType.Int32, 1);
                    db.AddInParameter(com, "@erogacion", DbType.Decimal, 0);
                    db.AddInParameter(com, "@capital", DbType.Decimal, 0);
                    db.AddInParameter(com, "@interes", DbType.Decimal, 0);
                    db.AddInParameter(com, "@iva_intere", DbType.Decimal, 0);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, entidad.IDSucursal);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, 1);
                    //string fch_estatus = ("00" + Convert.ToString(entidad.FchEstatus.Day)).Substring(("00" + Convert.ToString(entidad.FchEstatus.Day)).Length - 2) + "/" + ("00" + Convert.ToString(entidad.FchEstatus.Month)).Substring(("00" + Convert.ToString(entidad.FchEstatus.Month)).Length - 2) + "/" + ("0000" + Convert.ToString(entidad.FchEstatus.Year)).Substring(("0000" + Convert.ToString(entidad.FchEstatus.Year)).Length - 4);
                    //db.AddInParameter(com, "@fch_estatus", DbType.String, fch_estatus);

                    db.AddInParameter(com, "@fch_estatus", DbType.String, fch_solicitud);
                    db.AddInParameter(com, "@ingr_liqui", DbType.Decimal, 0);
                    db.AddInParameter(com, "@capa_pago", DbType.Decimal, 0);
                    db.AddInParameter(com, "@IdAnalista", DbType.Int32, entidad.IDAnalista);
                    db.AddInParameter(com, "@articulos", DbType.Decimal, 0);
                    db.AddInParameter(com, "@apertura", DbType.Decimal, 0);
                    db.AddInParameter(com, "@otro_carg", DbType.Decimal, 0);
                    db.AddInParameter(com, "@enganche", DbType.Decimal, 0);

                    //string fecha_enga = ("00" + Convert.ToString(entidad.FchEnganche.Day)).Substring(("00" + Convert.ToString(entidad.FchEnganche.Day)).Length - 2) + "/" + ("00" + Convert.ToString(entidad.FchEnganche.Month)).Substring(("00" + Convert.ToString(entidad.FchEnganche.Month)).Length - 2) + "/" + ("0000" + Convert.ToString(entidad.FchEnganche.Year)).Substring(("0000" + Convert.ToString(entidad.FchEnganche.Year)).Length - 4);
                    db.AddInParameter(com, "@fecha_enga", DbType.String, fch_solicitud);
                    //string fch_Arranq = ("00" + Convert.ToString(entidad.FchArranque.Day)).Substring(("00" + Convert.ToString(entidad.FchArranque.Day)).Length - 2) + "/" + ("00" + Convert.ToString(entidad.FchArranque.Month)).Substring(("00" + Convert.ToString(entidad.FchArranque.Month)).Length - 2) + "/" + ("0000" + Convert.ToString(entidad.FchArranque.Year)).Substring(("0000" + Convert.ToString(entidad.FchArranque.Year)).Length - 4);
                    db.AddInParameter(com, "@fch_Arranq", DbType.String, entidad.PrimerPago.ToString("dd/MM/yyyy"));

                    db.AddInParameter(com, "@override", DbType.Decimal, 0);
                    db.AddInParameter(com, "@overpaid", DbType.Decimal, 0);
                    db.AddInParameter(com, "@IdProducto", DbType.Int32, entidad.IDProducto);
                    db.AddInParameter(com, "@IdOpero", DbType.Int32, 0);
                    db.AddInParameter(com, "@dg_articulo", DbType.String, "");
                    db.AddInParameter(com, "@IdDestino", DbType.Int32, 0);
                    db.AddInParameter(com, "@IdAsignado", DbType.Int32, 0);
                    //string fch_Suscripcion = ("00" + Convert.ToString(entidad.FchSuscripcion.Day)).Substring(("00" + Convert.ToString(entidad.FchSuscripcion.Day)).Length - 2) + "/" + ("00" + Convert.ToString(entidad.FchSuscripcion.Month)).Substring(("00" + Convert.ToString(entidad.FchSuscripcion.Month)).Length - 2) + "/" + ("0000" + Convert.ToString(entidad.FchSuscripcion.Year)).Substring(("0000" + Convert.ToString(entidad.FchSuscripcion.Year)).Length - 4);
                    db.AddInParameter(com, "@fch_Suscripcion", DbType.String, fch_solicitud);
                    db.AddInParameter(com, "@idClabeCliente", DbType.Int32, 0);
                    db.AddInParameter(com, "@isReestructura", DbType.Int32, 0);
                    db.AddInParameter(com, "@otraInstitucion", DbType.Int32, 0);
                    db.AddInParameter(com, "@IdCampania", DbType.Int32, 1);
                    db.AddInParameter(com, "@IdCliRec", DbType.Int32, 0);
                    db.AddInParameter(com, "@IdClabeClienteRec", DbType.Int32, 0);
                    db.AddInParameter(com, "@IdTipoComision", DbType.Int32, 0);
                    db.AddInParameter(com, "@Pedido", DbType.String, entidad.Pedido);

                    db.AddInParameter(com, "@NumeroPoliza", DbType.String, entidad.NumeroPoliza);
                    db.AddInParameter(com, "@NumeroCertificado", DbType.String, entidad.NumeroCertificado);
                    db.AddInParameter(com, "@Deducciones", DbType.Decimal, entidad.Deducciones);
                    db.AddInParameter(com, "@fch_deposito", DbType.String, (entidad.fchDeposito != null) ? entidad.fchDeposito.GetValueOrDefault().ToString("yyyy/MM/dd") : (object)DBNull.Value);
                    db.AddInParameter(com, "@MontoRetiroDia1", DbType.Decimal, entidad.MontoRetDia1);
                    db.AddInParameter(com, "@MontoRetiroDia2", DbType.Decimal, entidad.MontoRetDia2);
                    db.AddInParameter(com, "@MontoRetiroDia3", DbType.Decimal, entidad.MontoRetDia3);
                    db.AddInParameter(com, "@SaldoIniCta", DbType.Decimal, entidad.SaldoIniCta);
                    db.AddInParameter(com, "@SaldoFinCta", DbType.Decimal, entidad.SaldoFinCta);
                    db.AddInParameter(com, "@MontoDepDia1", DbType.Decimal, entidad.MonDepDia1);
                    db.AddInParameter(com, "@MontoDepDia2", DbType.Decimal, entidad.MonDepDia2);
                    db.AddInParameter(com, "@MontoDepDia3", DbType.Decimal, entidad.MonDepDia3);
                    db.AddInParameter(com, "@SaldoIniCta2", DbType.Decimal, entidad.SaldoIniCta2);
                    db.AddInParameter(com, "@SaldoFinCta2", DbType.Decimal, entidad.SaldoFinCta2);
                    db.AddInParameter(com, "@SaldoIniCta3", DbType.Decimal, entidad.SaldoIniCta3);
                    db.AddInParameter(com, "@SaldoFinCta3", DbType.Decimal, entidad.SaldoFinCta3);
                    db.AddInParameter(com, "@idTipoCredito", DbType.Decimal, entidad.TipoCredito);

                    db.AddInParameter(com, "@BCEntidad", DbType.String, entidad.BCEntidad);
                    db.AddInParameter(com, "@BCOficina", DbType.String, entidad.BCOficina);
                    db.AddInParameter(com, "@BCDC", DbType.String, entidad.BCDC);
                    db.AddInParameter(com, "@BCCuenta", DbType.String, entidad.BCCuenta);
                    db.AddInParameter(com, "@ReferenciaExperian", DbType.String, entidad.ReferenciaExperian);
                    db.AddInParameter(com, "@ScoreExperian", DbType.Decimal, entidad.ScoreExperian);
                    db.AddInParameter(com, "@TipoDomiciliacion", DbType.Int32, entidad.TipoDomiciliacion);
                    db.AddInParameter(com, "@NumeroTarjetaVisa", DbType.String, entidad.NumeroTarjetaVisa);
                    db.AddInParameter(com, "@MesExpiraTarjetaVisa", DbType.Int32, entidad.MesExpiraTarjetaVisa);
                    db.AddInParameter(com, "@AnioExpiraTarjetaVisa", DbType.Int32, entidad.AnioExpiraTarjetaVisa);
                    db.AddInParameter(com, "@vFrecuenciaPago", DbType.String, entidad.vFrecuenciaPago);
                    db.AddInParameter(com, "@vMetDispersion", DbType.String, entidad.vMetDispersion);
                    db.AddInParameter(com, "@vCVV", DbType.String, entidad.vCVV);
                    db.AddInParameter(com, "@XmlCuentasDomEme", DbType.Xml, new XmlTextReader(entidad.getCuentasEmergentesXML(), XmlNodeType.Document, null));

                    // Monto de Liquidacion MANUAL, es un dato temporal que se eliminara una vez que el modulo de LIQUIDACIONES/RENOVACIONES sea terminado
                    db.AddInParameter(com, "@TMP_MontoLiquidacion", DbType.Decimal, entidad.TMPMontoLiquidacion);

                    Resultado<int> res = (new Clientes.TBClientesDal()).ObtenerSolicitudActivaDeCliente(entidad.IDCliente);
                    int idSolicitudActiva = res.ResultObject;

                    if (idSolicitudActiva <= 0 && entidad.TipoCredito == 3)
                    {
                        new Exception("No se encontró una solicitud para ampliar");
                    }

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new DataResultSolicitudes();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) resultado.idMensaje = 1;
                                if (!reader.IsDBNull(0)) resultado.Mensaje = reader[0].ToString();

                                idSolicitud = Convert.ToInt32(reader[0].ToString());

                                //Verificar si ya hay un proceso iniciado para nueva solicitud (si lo hay debe haber un registro con el valor idSolicitud 0, para el cliente)
                                SolicitudDireccionCondiciones condiciones = new SolicitudDireccionCondiciones();

                                if (entidad.IDSolictud > 0)
                                {
                                    condiciones.idSolicitud = entidad.IDSolictud;
                                }
                                else
                                {
                                    condiciones.idSolicitud = 0;
                                }

                                condiciones.idCliente = entidad.IDCliente;
                                List<SolicitudDireccion> direcciones = ConsultarDireccionSolicitud(condiciones);

                                SolicitudDireccion direccion = new SolicitudDireccion();
                                direccion.idCliente = entidad.IDCliente;
                                direccion.idSolicitud = idSolicitud;

                                if (direcciones.Count <= 0)
                                {
                                    InsertarDireccionSolicitud(direccion, true);
                                }
                                else
                                {
                                    direccion.idClienteDireccion = direcciones[0].idClienteDireccion;
                                    ActualizarDireccionSolicitud(direccion);
                                }

                                if (idSolicitudActiva > 0)
                                {
                                    TBCreditos.Liquidacion liquidacion = new TBCreditos.Liquidacion();
                                    liquidacion.IdSolicitud = idSolicitudActiva;
                                    liquidacion.FechaLiquidar = new Utils.DateTimeR();
                                    liquidacion.FechaLiquidar.Value = DateTime.Now;

                                    SolicitudReestructura reestructura = new SolicitudReestructura();
                                    reestructura.idSolicitud = idSolicitud;
                                    reestructura.idSolicitudActiva = idSolicitudActiva;
                                    reestructura.fechaLiquidar = DateTime.Now;
                                    reestructura.idEstatus = 0;
                                    reestructura.comentarios = "Reestructura";

                                    if (entidad.IDSolictud == 0)
                                    {
                                        InsertarReestructura(reestructura);
                                    }
                                }
                            }
                        }
                        reader.Dispose();
                    }
                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }

                //Asociar documentos digiales y generacion de proceso digital.
                if (Accion == 1 && idSolicitud > 0)
                {
                    Poco.Solicitudes.TB_Proceso proceso = ProcesoBLL.Originacion();
                    if (proceso != null)
                    {
                        SolicitudProcesoDigitalBLL.Nuevo(new NuevoSolicitudProcesoDigitalRequest
                        {
                            IdSolicitud = idSolicitud,
                            IdUsuario = IDUsuario,
                            IdProceso = proceso.IdProceso
                        });
                    }
                }
            }
            catch (SqlException ex)
            {
                resultado = new DataResultSolicitudes();
                resultado.idMensaje = 0;
                resultado.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                resultado = new DataResultSolicitudes();
                resultado.idMensaje = 0;
                resultado.Mensaje = ex.Message;
            }
            return resultado;
        }

        public Resultado<bool> ValidaTarjetaVISADomiciliacion(string NumeroTarjeta)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_ValidaDatos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "BIN_VISA"); // Validar BIN de la Tarjeta VISA para Domiciliacion
                    db.AddInParameter(com, "@NumeroTarjeta", DbType.String, NumeroTarjeta);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                    objReturn.Codigo = 0;
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public CronogramaPagos ObtenerCronograma(int IdSolicitud, DateTime? FechaDesembolso)
        {
            CronogramaPagos objReturn = new CronogramaPagos();
            DataSet ds = new DataSet();

            try
            {
                objReturn.IdSolicitud = IdSolicitud;

                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_ABCPagos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_CRONOGRAMA"); // Obtener el Cronograma de Pagos de la Solicitud
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, IdSolicitud);
                    db.AddInParameter(com, "@FechaDesembolso", DbType.DateTime, FechaDesembolso);

                    //Ejecucion de la Consulta                    
                    ds = db.ExecuteDataSet(com);

                    // Obtener los datos del Cronograma
                    foreach (DataRow r in ds.Tables[0].Rows)
                    {
                        objReturn.DNICliente = r["DNICliente"].ToString();
                        objReturn.NombreCliente = r["NombreCliente"].ToString();
                        objReturn.ApPaternoCliente = r["ApellidoPaterno"].ToString();
                        objReturn.ApMaternoCliente = r["ApellidoMaterno"].ToString();
                        objReturn.DireccionCliente = r["DireccionCliente"].ToString();
                        objReturn.MontoDesembolsar = decimal.Parse(r["MontoCredito"].ToString());
                        objReturn.TasaAnual = decimal.Parse(r["TasaAnual"].ToString());
                        objReturn.TasaCostoAnual = decimal.Parse(r["TasaCostoAnual"].ToString());
                        objReturn.FechaDesembolso = Utils.GetDateTime(r, "FechaDesembolso").GetValueOrDefault();
                        objReturn.PrimerFechaPago = Utils.GetDateTime(r, "FechaPrimerPago").GetValueOrDefault();
                        objReturn.UltimaFechaPago = Utils.GetDateTime(r, "FechaUltimoPago").GetValueOrDefault();
                        objReturn.TotalInteres = decimal.Parse(r["TotalInteres"].ToString());
                        objReturn.Plazo = int.Parse(r["Plazo"].ToString());
                        objReturn.Condiciones = r["Condiciones"].ToString();
                        objReturn.TasaInteresMoratoria = decimal.Parse(r["TasaInteresMoratoria"].ToString());
                        objReturn.Cuota = decimal.Parse(r["Cuota"].ToString());
                        objReturn.NombreBancoDesembolso = r["NombreBancoDesembolso"].ToString();
                        objReturn.CuentaBancariaDesembolso = r["CuentaBancariaDesembolso"].ToString();
                        objReturn.NoTarjetaVisa = r["NoTarjetaVisa"].ToString();
                    }

                    // Obtener los Recibos del Cronograma
                    CronogramaPagos.Recibo recibo;
                    foreach (DataRow r in ds.Tables[1].Rows)
                    {
                        recibo = new CronogramaPagos.Recibo();

                        recibo.NoRecibo = r["Recibo"].ToString();
                        recibo.FechaRecibo = Utils.GetDateTime(r, "Fch_Recibo", "dd/MM/yyyy");
                        recibo.CapitalInicial = decimal.Parse(r["CapitalInicial"].ToString());
                        recibo.Capital = decimal.Parse(r["Capital"].ToString());
                        recibo.Interes = decimal.Parse(r["Interes"].ToString());
                        recibo.IGV = decimal.Parse(r["IGV_Total"].ToString());
                        recibo.Seguro = decimal.Parse(r["Seguro"].ToString());
                        recibo.GAT = decimal.Parse(r["GAT"].ToString());
                        recibo.OtrosCargos = decimal.Parse(r["OtrosCargos"].ToString());
                        recibo.IGVOtrosCargos = decimal.Parse(r["IGVOtrosCargos"].ToString());
                        recibo.Cuota = decimal.Parse(r["Cuota"].ToString());
                        recibo.SaldoCapital = decimal.Parse(r["SaldoCapital"].ToString());

                        objReturn.Recibos.Add(recibo);
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        #endregion

        #endregion

        #region MISC Methods

        public int ValidaClabeDal(string Clabe)
        {
            int resultado = 0;
            //List<TBCreditos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_CWD_ValidaCLABE"))
                {
                    //Parametros

                    db.AddInParameter(com, "@clabe", DbType.String, Clabe);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {

                                if (!reader.IsDBNull(0)) resultado = Convert.ToInt32(reader[0]);
                            }
                        }
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public int ValidaTipoRenovacionDal(int idCliente)
        {
            int resultado = 0;
            //List<TBCreditos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("VerificarRenovacionCliente"))
                {
                    //Parametros

                    db.AddInParameter(com, "@IdCliente", DbType.String, idCliente);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) resultado = Convert.ToInt32(reader[0]);
                            }
                        }
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public void InsertarDetSolicitud(int Accion, int idsolicitud, int secuencia, string descripcion, decimal cantidad, decimal p_unitario, decimal capital, decimal interes, decimal Iva, string Pedido, string FinancieraPedido, int IdUsuario)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_ABCDetSolicitud"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, idsolicitud);
                    db.AddInParameter(com, "@secuencia", DbType.Int32, secuencia);
                    db.AddInParameter(com, "@descripcion", DbType.String, descripcion);
                    db.AddInParameter(com, "@cantidad", DbType.Decimal, cantidad);
                    db.AddInParameter(com, "@p_unitario", DbType.Decimal, p_unitario);
                    db.AddInParameter(com, "@capital", DbType.Decimal, capital);
                    db.AddInParameter(com, "@interes", DbType.Decimal, interes);
                    db.AddInParameter(com, "@Iva", DbType.Decimal, Iva);
                    db.AddInParameter(com, "@Pedido", DbType.String, Pedido);
                    db.AddInParameter(com, "@FinancieraPedido", DbType.String, FinancieraPedido);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //ModificarSolicitud-LP
        public TBSolicitudes.ModificaSolicitud ObtenerSolicitud(int IDUsuario, int IDSolicitud)
        {
            TBSolicitudes.ModificaSolicitud _ModificaSolicitud = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_ModificaSolicitud_Consulta"))
                {
                    db.AddInParameter(com, "@IDUsuario", DbType.Int32, IDUsuario);
                    db.AddInParameter(com, "@IDSolicitud", DbType.Int32, IDSolicitud);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                _ModificaSolicitud = new TBSolicitudes.ModificaSolicitud();
                                _ModificaSolicitud.IDSolicitud = Convert.ToInt32(reader[0]);
                                _ModificaSolicitud.IDEstatus = Convert.ToInt32(reader[1]);
                                _ModificaSolicitud.IDTipoConvenio = Convert.ToInt32(reader[2]);
                                _ModificaSolicitud.IDConvenio = Convert.ToInt32(reader[3]);
                                _ModificaSolicitud.IDRegional = Convert.ToInt32(reader[4]);
                                _ModificaSolicitud.IDSucursal = Convert.ToInt32(reader[5]);
                                _ModificaSolicitud.IDTipoPromotor = Convert.ToInt32(reader[6]);
                                _ModificaSolicitud.IDPromotor = Convert.ToInt32(reader[7]);
                                _ModificaSolicitud.Reestructura = Convert.ToInt32(reader[8]);
                                _ModificaSolicitud.IDUsuario = Convert.ToInt32(reader[9]);
                                _ModificaSolicitud.IDAsignado = Convert.ToInt32(reader[10]);
                                _ModificaSolicitud.Error = reader[11].ToString();
                                _ModificaSolicitud.EST_Proceso = Convert.ToInt32(reader[12]);
                                _ModificaSolicitud.IDEstatus_Real = Convert.ToInt32(reader[13]);
                                _ModificaSolicitud.LiqInstitucion = Convert.ToInt32(reader[14]);
                                _ModificaSolicitud.IdCliente = Convert.ToInt32(reader[15]);
                                _ModificaSolicitud.Cliente = Convert.ToString(reader[16]);
                            } //while reader.read
                        } //if reader != null                
                        reader.Dispose();
                    } //using reader
                    com.Dispose();
                }//using com
            }
            catch (Exception ex)
            { throw ex; }
            return _ModificaSolicitud;
        }

        public Resultado<bool> ActualizaSolicitud(TBSolicitudes.ModificaSolicitud Solicitud, int IDUsuario_Modifica)
        {
            Resultado<bool> _Resultado = new Resultado<bool>();
            _Resultado.IdUsuario = IDUsuario_Modifica.ToString();
            _Resultado.Mensaje = "";
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_ModificaSolicitud_Actualiza"))
                {
                    db.AddInParameter(com, "@IDSolicitud", DbType.Int32, Solicitud.IDSolicitud);
                    db.AddInParameter(com, "@IDEstatus", DbType.Int32, Solicitud.IDEstatus);
                    db.AddInParameter(com, "@IDTipoConvenio", DbType.Int32, Solicitud.IDTipoConvenio);
                    db.AddInParameter(com, "@IDConvenio", DbType.Int32, Solicitud.IDConvenio);
                    db.AddInParameter(com, "@IDSucursal", DbType.Int32, Solicitud.IDSucursal);
                    db.AddInParameter(com, "@IDPromotor", DbType.Int32, Solicitud.IDPromotor);
                    db.AddInParameter(com, "@ISReestructura", DbType.Int32, Solicitud.Reestructura);
                    db.AddInParameter(com, "@UsuarioID", DbType.Int32, Solicitud.IDUsuario);
                    db.AddInParameter(com, "@Usuario_Modifica", DbType.Int32, IDUsuario_Modifica);
                    db.AddInParameter(com, "@EST_Proceso", DbType.Int32, Solicitud.EST_Proceso);
                    db.AddInParameter(com, "@LiqInstitucion", DbType.Int32, Solicitud.LiqInstitucion);
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, Solicitud.IdCliente);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                _Resultado.Codigo = Convert.ToInt32(reader[0]);
                                _Resultado.Mensaje = reader[1].ToString();
                            } //while reader.read
                        } //if reader != null                
                        reader.Dispose();
                    } //using reader
                    com.Dispose();
                }//using com
            }
            catch (Exception ex)
            {
                _Resultado.Mensaje = ex.Message;
            }
            return _Resultado;
        }
        public Resultado<bool> InsertarCuentaEmergente(TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente, int idUsuario)
        {

            // TODO: Llamar a SP para actualizar/insertar la informacion necesaria
            Resultado<bool> respuesta = new Resultado<bool>();
            try
            {
                if (cuentaEmergente == null) throw new Exception("La petición es inválida.");
                if (cuentaEmergente.IdSolicitud <= 0) throw new Exception("La solicitud es inválida.");
                if (idUsuario <= 0) throw new Exception("El usuario es incorrecto.");

                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_ABCCuentaDomiciliacionEmergente"))
                {
                    db.AddInParameter(com, "@Accion", DbType.Int32, 1);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, idUsuario);
                    db.AlimentarParametrosDefault(com, cuentaEmergente);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) respuesta.Codigo = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) respuesta.Mensaje = reader[1].ToString();

                            }
                        }
                        respuesta.ResultObject = respuesta.Codigo > 0 ? true : false;
                        reader.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.Mensaje = "Ha ocurrido un error al insertar la cuenta";
                respuesta.ResultObject = false;
            }
            return respuesta;
        }
        public Resultado<bool> ActualizarCuentaEmergente(TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente, int idUsuario)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            try
            {
                if (cuentaEmergente == null) throw new Exception("La petición es inválida.");
                if (cuentaEmergente.IdSolicitud <= 0) throw new Exception("La solicitud es inválida.");
                if (idUsuario <= 0) throw new Exception("El usuario es incorrecto.");

                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_ABCCuentaDomiciliacionEmergente"))
                {
                    db.AddInParameter(com, "@Accion", DbType.Int32, 2);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, idUsuario);
                    db.AlimentarParametrosDefault(com, cuentaEmergente);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) respuesta.Codigo = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) respuesta.Mensaje = reader[1].ToString();

                            }
                        }
                        respuesta.ResultObject = respuesta.Codigo > 0 ? true : false;
                        reader.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.Mensaje = "Ha ocurrido un error en la actualización de la cuenta";
                respuesta.ResultObject = false;
            }
            return respuesta;
        }

        public List<Combo> ObtenerCATEst_Proceso()
        {
            List<Combo> _lstCATCombo = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Catalogo.Est_Proceso"))
                {
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            _lstCATCombo = new List<Combo>();
                            while (reader.Read())
                            {
                                Combo _CATCombo = new Combo();
                                _CATCombo.Valor = Convert.ToInt32(reader[0]);
                                _CATCombo.Descripcion = Convert.ToString(reader[1]);
                                _lstCATCombo.Add(_CATCombo);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _lstCATCombo;
        }
        public Resultado<bool> ActualizarDatosSolicitudGestiones(int idUsuario, TBSolicitudes.ActualizarSolicitudGestiones solicitud)
        {
            var respuesta = new Resultado<bool>();
            try
            {
                if (solicitud == null)
                {
                    throw new Exception("La petición es inválida.");
                }
                else
                {
                    using (var com = db.GetStoredProcCommand("dbo.SP_SolicitudGestionesACT"))
                    {
                        db.AddInParameter(com, "@Accion", DbType.Int32, 0);
                        db.AddInParameter(com, "@IdUsuario", DbType.Int32, idUsuario);
                        db.AlimentarParametrosDefault(com, solicitud);

                        using (IDataReader reader = db.ExecuteReader(com))
                        {
                            if (reader != null)
                            {
                                while (reader.Read())
                                {
                                    if (!reader.IsDBNull(0)) respuesta.Codigo = Convert.ToInt32(reader[0]);
                                    if (!reader.IsDBNull(1)) respuesta.Mensaje = reader[1].ToString();

                                }
                            }
                            respuesta.ResultObject = respuesta.Codigo > 0 ? true : false;
                            reader.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.ResultObject = false;
                respuesta.Mensaje = ex.Message;
            }

            return respuesta;
        }

        #endregion

        #region MISC Methods

        public Resultado<bool> AsignaAnalista(int IDSolicitud, int IDUsuario, int Validacion)
        {
            Resultado<bool> _Resultado = new Resultado<bool>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_ModificaSolicitud_AsignaAnalista"))
                {
                    db.AddInParameter(com, "@IDSolicitud", DbType.Int32, IDSolicitud);
                    db.AddInParameter(com, "@IDUsuario", DbType.Int32, IDUsuario);
                    db.AddInParameter(com, "@Validacion", DbType.Int32, Validacion);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                _Resultado.Codigo = Convert.ToInt32(reader[0]);
                                _Resultado.Mensaje = reader[1].ToString();
                            } //while reader.read
                        } //if reader != null                
                        reader.Dispose();
                    } //using reader
                    com.Dispose();
                }//using com
            }
            catch (Exception ex)
            {
                _Resultado.Mensaje = ex.Message;
            }
            return _Resultado;
        }

        public DataResultSolicitudes ValidaDatosDal(int Accion, string Parametro, int idSolicitud, int TipoResultado)
        {
            DataResultSolicitudes resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("creditos.SP_ValidaDatos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@Parametro", DbType.String, Parametro);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, idSolicitud);
                    db.AddInParameter(com, "@TipoResultado", DbType.Int32, TipoResultado);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new DataResultSolicitudes();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) resultado.idMensaje = Convert.ToInt32(reader[0].ToString());
                                if (!reader.IsDBNull(1)) resultado.Mensaje = reader[0].ToString();
                            }
                        }
                        reader.Dispose();
                    }
                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
                return resultado;
            }
            catch (Exception ex)
            {
                resultado = new DataResultSolicitudes();
                resultado.idMensaje = 0;
                resultado.Mensaje = ex.ToString();
                return resultado;
            }
        }

        public Resultado<List<TBSolicitudes.ListaNegra>> ValidaListaNegra(TBSolicitudes.ListaNegra entidad)
        {
            Resultado<List<TBSolicitudes.ListaNegra>> objReturn = new Resultado<List<TBSolicitudes.ListaNegra>>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_ValidaListaNegra"))
                {
                    //Parametros
                    db.AddInParameter(com, "@DNI", DbType.String, entidad.DNI);
                    db.AddInParameter(com, "@Nombre", DbType.String, entidad.Nombre);
                    db.AddInParameter(com, "@ApellidoPaterno", DbType.String, entidad.ApellidoPaterno);
                    db.AddInParameter(com, "@ApellidoMaterno", DbType.String, entidad.ApellidoMaterno);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            //Lectura de los datos del ResultSet
                            TBSolicitudes.ListaNegra o;
                            while (reader.Read())
                            {
                                o = new TBSolicitudes.ListaNegra();

                                o.Bloqueo = Utils.GetBool(reader, "Bloqueo");
                                o.DNI = Utils.GetString(reader, "DNI");
                                o.Nombre = Utils.GetString(reader, "Nombre");
                                o.ApellidoPaterno = Utils.GetString(reader, "ApellidoPaterno");
                                o.ApellidoMaterno = Utils.GetString(reader, "ApellidoMaterno");
                                o.Origen = Utils.GetString(reader, "Origen");

                                objReturn.ResultObject.Add(o);
                            }
                        }
                        reader.Dispose();
                    }
                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }

            return objReturn;
        }

        #endregion

        #region Private Methods
        private List<TBSolicitudes.CuentaDomiciliacionEmergente> ObtenerCuentasDomiciliacionEmergente(int idsolicitud)
        {
            List<TBSolicitudes.CuentaDomiciliacionEmergente> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.Datossolicitud"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_CUENTAS_EMERGENTES");
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, idsolicitud);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBSolicitudes.CuentaDomiciliacionEmergente>();

                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                resultado.Add(new TBSolicitudes.CuentaDomiciliacionEmergente()
                                {
                                    IdSolicitud = Utils.GetInt(reader, "IdSolicitud"),
                                    TipoDomiciliacion = Utils.GetInt(reader, "TipoDomiciliacion"),
                                    Banco = Utils.GetString(reader, "Banco"),
                                    Cuenta = Utils.GetString(reader, "Cuenta"),
                                    CuentaCCI = Utils.GetString(reader, "CuentaCCI"),
                                    NumeroTarjeta = Utils.GetString(reader, "NumeroTarjeta"),
                                    MesExpiraTarjeta = Utils.GetInt(reader, "MesExpiraTarjeta"),
                                    AnioExpiraTarjeta = Utils.GetInt(reader, "AnioExpiraTarjeta"),
                                    Cvv = Utils.GetString(reader, "Cvv"),
                                });
                            }
                        }
                    }
                }
            }
            catch
            {
            }

            return resultado;
        }
        #endregion

        #region Expedientes

        public string GuardaExpediente(TBSolicitudes.Expediente _Expediente)
        {
            string strResultado = "";
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_ExpedientesALT"))
                {
                    db.AddInParameter(com, "@IDSolicitud", DbType.Int32, _Expediente.IDSolicitud);
                    db.AddInParameter(com, "@Fideicomiso", DbType.Int32, _Expediente.Fideicomiso);
                    db.AddInParameter(com, "@IDDocumento", DbType.Int32, _Expediente.IDDocumento);
                    db.AddInParameter(com, "@Fecha", DbType.String, _Expediente.Fecha);
                    db.AddInParameter(com, "@IDGoogleDrive", DbType.String, _Expediente.IDGoogleDrive);
                    db.AddInParameter(com, "@NombreArchivo", DbType.String, _Expediente.NombreArchivo);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {

                            }
                        } //if reader != null                
                        reader.Dispose();
                    } //using reader
                    com.Dispose();
                }//using com
            }
            catch (Exception ex)
            {
                strResultado = ex.Message;
            }
            return strResultado;
        }

        public int getIDSolicitud(int IDSolicitud)
        {
            int auxIDSolicitud = 0;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_getIDSolicitud"))
                {
                    db.AddInParameter(com, "@IDSolicitud", DbType.Int32, IDSolicitud);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) IDSolicitud = Convert.ToInt32(reader[0].ToString());
                            }
                        }
                        reader.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                IDSolicitud = -1;
            }
            return IDSolicitud;
        }

        public TBSolicitudes.Expediente ConsultaExpediente(TBSolicitudes.Expediente _Expediente)
        {
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_ExpedientesCON"))
                {
                    db.AddInParameter(com, "@IDSolicitud", DbType.Int32, _Expediente.IDSolicitud);
                    db.AddInParameter(com, "@IDDocumento", DbType.Int32, _Expediente.IDDocumento);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                _Expediente.IDGoogleDrive = reader[0].ToString();
                            }
                        } //if reader != null                
                        reader.Dispose();
                    } //using reader
                    com.Dispose();
                }//using com
            }
            catch (Exception ex)
            {
                _Expediente.IDSolicitud = -1;
                _Expediente.IDGoogleDrive = ex.Message;
            }
            return _Expediente;
        }

        #endregion

        public List<TBSolicitudes.Documentos> ObtenerDocumentos(int IDSolicitud)
        {
            List<TBSolicitudes.Documentos> resultado = null;
            //List<TBCreditos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SOLICITUDES.SEL_CATDOCUMENTOS"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IDSolicitud", DbType.Int32, IDSolicitud);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBSolicitudes.Documentos>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBSolicitudes.Documentos documento = new TBSolicitudes.Documentos();
                                if (!reader.IsDBNull(0)) documento.idDocumento = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) documento.Documento = reader[1].ToString();

                                resultado.Add(documento);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public SolicitudReestructura ObtenerReestructura(SolicitudReestructuraCondiciones condiciones)
        {
            SolicitudReestructura reestructura = new SolicitudReestructura();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("solicitudes.SP_SolicitudesReestructuraCON"))
                {
                    db.AddInParameter(com, "@idEstatus", DbType.Int32, condiciones.idEstatus);

                    if (condiciones.idSolicitud > 0)
                    {
                        db.AddInParameter(com, "@idSolicitud", DbType.Int32, condiciones.idSolicitud);
                    }

                    if (condiciones.idSolicitudReestructura > 0)
                    {
                        db.AddInParameter(com, "@idSolicitudReestructura", DbType.Int32, condiciones.idSolicitudReestructura);
                    }

                    if (condiciones.idCuenta > 0)
                    {
                        db.AddInParameter(com, "@idCuenta", DbType.Int32, condiciones.idCuenta);
                    }

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("idSolicitud"))) reestructura.idSolicitud = Convert.ToInt32(reader[reader.GetOrdinal("idSolicitud")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("idSolicitudReestructura"))) reestructura.idSolicitudReestructura = Convert.ToInt32(reader[reader.GetOrdinal("idSolicitudReestructura")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("idCuenta"))) reestructura.idCuenta = Convert.ToInt32(reader[reader.GetOrdinal("idCuenta")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("fechaLiquidar"))) reestructura.fechaLiquidar = Convert.ToDateTime(reader[reader.GetOrdinal("fechaLiquidar")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("capitalLiquidar"))) reestructura.capitalLiquidar = Convert.ToDecimal(reader[reader.GetOrdinal("capitalLiquidar")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("interesLiquidar"))) reestructura.interesLiquidar = Convert.ToDecimal(reader[reader.GetOrdinal("interesLiquidar")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("igvLiquidar"))) reestructura.igvLiquidar = Convert.ToDecimal(reader[reader.GetOrdinal("igvLiquidar")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("seguroLiquidar"))) reestructura.seguroLiquidar = Convert.ToDecimal(reader[reader.GetOrdinal("seguroLiquidar")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("gatLiquidar"))) reestructura.gatLiquidar = Convert.ToDecimal(reader[reader.GetOrdinal("gatLiquidar")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("totalLiquidar"))) reestructura.totalLiquidar = Convert.ToDecimal(reader[reader.GetOrdinal("totalLiquidar")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("idEstatus"))) reestructura.idEstatus = Convert.ToInt32(reader[reader.GetOrdinal("idEstatus")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("comentarios"))) reestructura.comentarios = reader[reader.GetOrdinal("comentarios")].ToString();
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reestructura;
        }

        public int InsertarReestructura(SolicitudReestructura reestructura)
        {
            int resultado = 0;

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("solicitudes.SP_SolicitudesReestructuraAL"))
                {
                    db.AddInParameter(com, "@idSolicitud", DbType.Int32, reestructura.idSolicitud);

                    if (reestructura.idSolicitudActiva > 0)
                    {
                        db.AddInParameter(com, "@idSolicitudActiva", DbType.Int32, reestructura.idSolicitudActiva);
                    }
                    else
                    {
                        db.AddInParameter(com, "@idCuenta", DbType.Int32, reestructura.idCuenta);
                        db.AddInParameter(com, "@capitalLiquidar", DbType.Decimal, reestructura.capitalLiquidar);
                        db.AddInParameter(com, "@interesLiquidar", DbType.Decimal, reestructura.interesLiquidar);
                        db.AddInParameter(com, "@igvLiquidar", DbType.Decimal, reestructura.igvLiquidar);
                        db.AddInParameter(com, "@seguroLiquidar", DbType.Decimal, reestructura.seguroLiquidar);
                        db.AddInParameter(com, "@gatLiquidar", DbType.Decimal, reestructura.gatLiquidar);
                        db.AddInParameter(com, "@totalLiquidar", DbType.Decimal, reestructura.totalLiquidar);
                    }

                    db.AddInParameter(com, "@fechaLiquidar", DbType.DateTime, reestructura.fechaLiquidar);
                    db.AddInParameter(com, "@idEstatus", DbType.Int32, reestructura.idEstatus);
                    db.AddInParameter(com, "@comentarios", DbType.String, reestructura.comentarios);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = 1;
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resultado;
        }

        public List<SolicitudDireccion> ConsultarDireccionSolicitud(SolicitudDireccionCondiciones condiciones)
        {
            List<SolicitudDireccion> solicitudDireccion = new List<SolicitudDireccion>();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("clientes.SP_ClienteDireccionesCON"))
                {
                    if (condiciones.idClienteDireccion > 0)
                    {
                        db.AddInParameter(com, "@idClienteDireccion", DbType.Int32, condiciones.idClienteDireccion);
                    }

                    db.AddInParameter(com, "@idCliente", DbType.Int32, condiciones.idCliente);
                    db.AddInParameter(com, "@idSolicitud", DbType.Int32, condiciones.idSolicitud);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                SolicitudDireccion sd = new SolicitudDireccion();
                                if (!reader.IsDBNull(reader.GetOrdinal("idClienteDireccion"))) sd.idClienteDireccion = Convert.ToInt32(reader[reader.GetOrdinal("idClienteDireccion")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("idCliente"))) sd.idCliente = Convert.ToInt32(reader[reader.GetOrdinal("idCliente")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("idSolicitud"))) sd.idSolicitud = Convert.ToInt32(reader[reader.GetOrdinal("idSolicitud")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("direccion"))) sd.direccion = reader[reader.GetOrdinal("direccion")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("idTipoDireccion"))) sd.idTipoDireccion = Convert.ToInt32(reader[reader.GetOrdinal("idTipoDireccion")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("nombreVia"))) sd.nombreVia = reader[reader.GetOrdinal("nombreVia")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("numeroExterno"))) sd.numeroExterno = reader[reader.GetOrdinal("numeroExterno")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("numeroInterno"))) sd.numeroInterno = reader[reader.GetOrdinal("numeroInterno")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("manzana"))) sd.manzana = reader[reader.GetOrdinal("manzana")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("lote"))) sd.lote = reader[reader.GetOrdinal("lote")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("tipoZona"))) sd.tipoZona = Convert.ToInt32(reader[reader.GetOrdinal("tipoZona")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("nombreZona"))) sd.nombreZona = reader[reader.GetOrdinal("nombreZona")].ToString();
                                if (!reader.IsDBNull(reader.GetOrdinal("distrito"))) sd.idColonia = Convert.ToInt32(reader[reader.GetOrdinal("distrito")].ToString());
                                solicitudDireccion.Add(sd);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return solicitudDireccion;
        }

        public PoliticaAmpliacion PoliticaAmplicacionParaCliente(int idCliente)
        {
            PoliticaAmpliacion politica = new PoliticaAmpliacion();

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("clientes.SP_ClientePoliticaAmpliacionPRO"))
                {
                    db.AddInParameter(com, "@idCliente", DbType.Int32, idCliente);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                SolicitudDireccion sd = new SolicitudDireccion();
                                if (!reader.IsDBNull(reader.GetOrdinal("idPolitica"))) politica.id = Convert.ToInt32(reader[reader.GetOrdinal("idPolitica")].ToString());
                                if (!reader.IsDBNull(reader.GetOrdinal("descripcion"))) politica.descripcion = reader[reader.GetOrdinal("descripcion")].ToString();
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return politica;
        }

        public Decimal ObtenerGAT()
        {
            decimal gat = 0;

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_ValorGatCON"))
                {
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                SolicitudDireccion sd = new SolicitudDireccion();
                                if (!reader.IsDBNull(reader.GetOrdinal("gat"))) gat = Convert.ToDecimal(reader[reader.GetOrdinal("gat")].ToString());
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return gat;
        }

        private int InsertarDireccionSolicitud(SolicitudDireccion direccion, bool datosCliente)
        {
            if (datosCliente)
            {
                TBClientes cliente = new TBClientes();
                cliente = TBClientesBll.ObtenerTBClientes(1, direccion.idCliente)[0];

                direccion.direccion = cliente.Direccion;
                direccion.idTipoDireccion = cliente.TipoDireccion;
                direccion.nombreVia = cliente.NombreVia;
                direccion.numeroExterno = cliente.NumExterior;
                direccion.numeroInterno = cliente.NumInterior;
                direccion.manzana = cliente.Manzana;
                direccion.lote = cliente.Lote;
                direccion.tipoZona = cliente.TipoZona;
                direccion.nombreZona = cliente.NombreZona;
                direccion.idColonia = Convert.ToInt32(cliente.IdColonia);
            }

            int resultado = 0;
            try
            {
                SolicitudDireccionCondiciones condiciones = new SolicitudDireccionCondiciones();
                condiciones.idSolicitud = direccion.idSolicitud;
                condiciones.idCliente = direccion.idCliente;
                List<SolicitudDireccion> direcciones = ConsultarDireccionSolicitud(condiciones);

                string sp = "clientes.SP_ClienteDireccionesAL";
                if (direcciones.Count > 0)
                {
                    sp = "clientes.SP_ClienteDireccionesACT";
                }

                using (DbCommand com = db.GetStoredProcCommand(sp))
                {
                    if (direcciones.Count > 0)
                    {
                        db.AddInParameter(com, "@idClienteDireccion", DbType.Int32, direcciones[0].idClienteDireccion);
                    }

                    db.AddInParameter(com, "@idCliente", DbType.Int32, direccion.idCliente);
                    db.AddInParameter(com, "@idSolicitud", DbType.Int32, direccion.idSolicitud);
                    db.AddInParameter(com, "@direccion", DbType.String, direccion.direccion);
                    db.AddInParameter(com, "@idTipoDireccion", DbType.String, direccion.idTipoDireccion);
                    db.AddInParameter(com, "@nombreVia", DbType.String, direccion.nombreVia);
                    db.AddInParameter(com, "@numeroExterno", DbType.String, direccion.numeroExterno);
                    db.AddInParameter(com, "@numeroInterno", DbType.String, direccion.numeroInterno);
                    db.AddInParameter(com, "@manzana", DbType.String, direccion.manzana);
                    db.AddInParameter(com, "@lote", DbType.String, direccion.lote);
                    db.AddInParameter(com, "@tipoZona", DbType.Int32, direccion.tipoZona);
                    db.AddInParameter(com, "@nombreZona", DbType.String, direccion.nombreZona);
                    db.AddInParameter(com, "@distrito", DbType.Int32, direccion.idColonia);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, direccion.IdUsuario);

                    IDataReader reader = db.ExecuteReader(com);

                    if (reader != null)
                    {
                        if (direcciones.Count > 0)
                        {
                            resultado = direcciones[0].idClienteDireccion;
                        }
                        else
                        {
                            while (reader.Read())
                            {
                                resultado = Convert.ToInt32(reader[reader.GetOrdinal("idClienteDireccion")].ToString());
                            }
                        }
                    }
                    reader.Dispose();

                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resultado;
        }

        public int InsertarDireccionSolicitud(SolicitudDireccion direccion)
        {
            return InsertarDireccionSolicitud(direccion, false);
        }

        public int ActualizarDireccionSolicitud(SolicitudDireccion direccion)
        {
            if (direccion.idClienteDireccion == 0 && direccion.idCliente > 0 && direccion.idSolicitud > 0)
            {
                SolicitudDireccionCondiciones condiciones = new SolicitudDireccionCondiciones();
                condiciones.idSolicitud = direccion.idSolicitud;
                condiciones.idCliente = direccion.idCliente;
                List<SolicitudDireccion> direcciones = ConsultarDireccionSolicitud(condiciones);

                if (direcciones.Count <= 0)
                {
                    InsertarDireccionSolicitud(direccion, false);
                    return 0;
                }
            }

            int resultado = 0;

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("clientes.SP_ClienteDireccionesACT"))
                {
                    if (direccion.idClienteDireccion > 0)
                    {
                        db.AddInParameter(com, "@idClienteDireccion", DbType.Int32, direccion.idClienteDireccion);
                    }

                    if (direccion.idCliente > 0)
                    {
                        db.AddInParameter(com, "@idCliente", DbType.Int32, direccion.idCliente);
                    }

                    if (direccion.idSolicitud > 0)
                    {
                        db.AddInParameter(com, "@idSolicitud", DbType.Int32, direccion.idSolicitud);
                    }

                    if (direccion.direccion != null)
                    {
                        db.AddInParameter(com, "@direccion", DbType.String, direccion.direccion);
                    }

                    if (direccion.idTipoDireccion > 0)
                    {
                        db.AddInParameter(com, "@idTipoDireccion", DbType.Int32, direccion.idTipoDireccion);
                    }

                    if (direccion.nombreVia != null)
                    {
                        db.AddInParameter(com, "@nombreVia", DbType.String, direccion.nombreVia);
                    }

                    if (direccion.numeroExterno != null)
                    {
                        db.AddInParameter(com, "@numeroExterno", DbType.String, direccion.numeroExterno);
                    }

                    if (direccion.numeroInterno != null)
                    {
                        db.AddInParameter(com, "@numeroInterno", DbType.String, direccion.numeroInterno);
                    }

                    if (direccion.manzana != null)
                    {
                        db.AddInParameter(com, "@manzana", DbType.String, direccion.manzana);
                    }

                    if (direccion.lote != null)
                    {
                        db.AddInParameter(com, "@lote", DbType.String, direccion.lote);
                    }

                    if (direccion.tipoZona > 0)
                    {
                        db.AddInParameter(com, "@tipoZona", DbType.Int32, direccion.tipoZona);
                    }

                    if (direccion.nombreZona != null)
                    {
                        db.AddInParameter(com, "@nombreZona", DbType.String, direccion.nombreZona);
                    }

                    if (direccion.idColonia > 0)
                    {
                        db.AddInParameter(com, "@distrito", DbType.Int32, direccion.idColonia);
                    }
                    if (direccion.IdUsuario > 0)
                    {
                        db.AddInParameter(com, "@IdUsuario", DbType.Int32, direccion.IdUsuario);
                    }

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                resultado = Convert.ToInt32(reader[reader.GetOrdinal("resultado")].ToString());
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resultado;
        }

        public SolicitudRPTPeru SolcitudRPT_Obtener(int IdSolicitud)
        {
            SolicitudRPTPeru objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_SolicitudRPT"))
                {
                    //Parametros
                    //db.AddInParameter(com, "@Accion", DbType.String, "GET_BALANCE"); // Obtener Balance Cuenta
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, (IdSolicitud > 0) ? IdSolicitud : (object)DBNull.Value);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            if (reader.Read())
                            {
                                objReturn = new SolicitudRPTPeru();

                                // Lectura de los datos del ResultSet                                
                                objReturn.IdSolicitud = Utils.GetInt(reader, "IdSolicitud");
                                objReturn.FechaSolicitud = reader.GetDateTime(1);
                                objReturn.ClienteNombre = Utils.GetString(reader, "Nombres");
                                objReturn.ClienteApePat = Utils.GetString(reader, "apellidop");
                                objReturn.ClienteApeMat = Utils.GetString(reader, "apellidom");
                                objReturn.FechaNacimiento = reader.GetDateTime(9);
                                objReturn.Edad = Utils.GetInt(reader, "Edad");
                                objReturn.EstadoCivil = Utils.GetString(reader, "EstCivil");
                                objReturn.Sexo = Utils.GetString(reader, "Sexo");
                                objReturn.Promotor = Utils.GetString(reader, "Promotor");
                                objReturn.CanalVenta = Utils.GetString(reader, "CanalVenta");
                                objReturn.ScoreExperian = Utils.GetDecimal(reader, "ScoreExperian");
                                objReturn.Origen = Utils.GetString(reader, "Origen");
                                objReturn.TipoDocumento = Utils.GetString(reader, "TipoDocumento");
                                objReturn.NumeroDocumento = Utils.GetString(reader, "NumeroDocumento");
                                objReturn.Celular = Utils.GetString(reader, "Celular");
                                objReturn.Email = Utils.GetString(reader, "Correo");
                                objReturn.UbicacionOfic = Utils.GetString(reader, "ubicacionOfic");
                                objReturn.IngresoBruto = Utils.GetDecimal(reader, "ingresoBruto");
                                objReturn.Otros_Ingr = Utils.GetDecimal(reader, "otros_ingr");
                                objReturn.DescuentosLey = Utils.GetDecimal(reader, "descuentosLey");
                                objReturn.SituacionLaboral = Utils.GetString(reader, "SituacionLaboral");

                                objReturn.Puesto = Utils.GetString(reader, "puesto");
                                objReturn.PEP = Utils.GetString(reader, "PEP");
                                objReturn.LugNac = Utils.GetString(reader, "LugNac");
                                objReturn.Nacion = Utils.GetString(reader, "Nacion");
                                objReturn.Distrito = Utils.GetString(reader, "Distrito");
                                objReturn.Provincia = Utils.GetString(reader, "Provincia");
                                objReturn.Departamento = Utils.GetString(reader, "Provincia");
                                objReturn.TipRes = Utils.GetString(reader, "TipRes");
                                objReturn.RUC = Utils.GetString(reader, "RUC");
                                objReturn.conyuge = Utils.GetString(reader, "ConyNom");
                                objReturn.cony_tele = Utils.GetString(reader, "ConyTel");
                                objReturn.Ref1Nom = Utils.GetString(reader, "Ref1Nom");
                                objReturn.Ref1Pare = Utils.GetString(reader, "Ref1Pare");
                                objReturn.Ref1Tel = Utils.GetString(reader, "Ref1Tel");
                                objReturn.Ref2Nom = Utils.GetString(reader, "Ref2Nom");
                                objReturn.Ref2Pare = Utils.GetString(reader, "Ref2Pare");
                                objReturn.Ref2Tel = Utils.GetString(reader, "Ref2Tel");
                                objReturn.NumDepen = Utils.GetInt(reader, "NumDepen");
                                objReturn.Direccion = Utils.GetString(reader, "Direccion");
                                objReturn.RefDom = Utils.GetString(reader, "RefDom");
                                objReturn.Empresa = Utils.GetString(reader, "Empresa");
                                objReturn.Dependencia = Utils.GetString(reader, "Dependencia");
                                objReturn.TelefonoLaboral = Utils.GetString(reader, "TelefonoLaboral");
                                objReturn.AnexoLaboral = Utils.GetString(reader, "AnexoLaboral");
                                objReturn.PuestoLaboral = Utils.GetString(reader, "PuestoLaboral");
                                objReturn.AntiLaboral = Utils.GetDecimal(reader, "AntiLaboral");
                                objReturn.OtrosDsctos = Utils.GetDecimal(reader, "OtrosDsctos");
                                objReturn.IngresoNeto = Utils.GetDecimal(reader, "IngresoNeto");
                                objReturn.ProOIng = Utils.GetString(reader, "ProOIng");
                                objReturn.ConyTDoc = Utils.GetString(reader, "ConyTDoc");
                                objReturn.ConyNDoc = Utils.GetString(reader, "ConyNDoc");
                                objReturn.ImpCred = Utils.GetDecimal(reader, "ImpCred");
                                objReturn.PlaCred = Utils.GetDecimal(reader, "PlaCred");
                                objReturn.MtoCuota = Utils.GetDecimal(reader, "MtoCuota");
                            }    
                        }
                        reader.Dispose();
                    }
                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> ActualizarDatosGestionSolicitud(TBSolicitudes.DatosSolicitud solicitud)
        {
            var respuesta = new Resultado<bool>();

            if (solicitud != null && solicitud.IDSolictud > 0)
            {
                // Actualizar los datos de la solicitud

            }

            return respuesta;
        }
    }
}