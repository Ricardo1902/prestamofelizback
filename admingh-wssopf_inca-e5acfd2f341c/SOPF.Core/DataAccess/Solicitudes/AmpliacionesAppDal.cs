using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Entities;

namespace SOPF.Core.DataAccess.Solicitudes
{
    public class AmpliacionesAppDal
    {
        #region Objetos Base de Datos

        Database db;

        #endregion

        #region Contructor

        public AmpliacionesAppDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public List<TBCATEstatus> RevisionDocs_CboEstatus()
        {
            List<TBCATEstatus> objReturn = new List<TBCATEstatus>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_RevDocs_CboEstatusCON"))
                {                    
                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new List<TBCATEstatus>();
                            TBCATEstatus o;

                            while (reader.Read())
                            {
                                o = new TBCATEstatus();

                                // Lectura de los datos del ResultSet                                
                                o.IdEstatus = Utils.GetInt(reader, "IdEstatus");
                                o.EstatusDesc = Utils.GetString(reader, "EstatusDesc");                               

                                objReturn.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<TB_SolicitudAmpliacionExpress> ObtenerSolicitudAmpliacion(int IdSolicitudAmpliacionExpress)
        {
            Resultado<TB_SolicitudAmpliacionExpress> objReturn = new Resultado<TB_SolicitudAmpliacionExpress>();

            try
            {
                Resultado<List<TB_SolicitudAmpliacionExpress>> res = FiltrarSolicitudes(new TB_SolicitudAmpliacionExpress() { IdSolicitudAmpliacionExpress = IdSolicitudAmpliacionExpress });

                if (res.Codigo > 0)
                {
                    objReturn.Codigo = res.Codigo;
                    objReturn.Mensaje = res.Mensaje;
                }
                else if (res.ResultObject.Count() <= 0)
                {
                    objReturn.Codigo = 1;
                    objReturn.Mensaje = "Error la Cargar ObtenerSolicitudAmpliacion";
                }

                objReturn.ResultObject = res.ResultObject.FirstOrDefault();
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<List<TB_SolicitudAmpliacionExpress>> FiltrarSolicitudes(TB_SolicitudAmpliacionExpress entity)
        {
            Resultado<List<TB_SolicitudAmpliacionExpress>> objReturn = new Resultado<List<TB_SolicitudAmpliacionExpress>>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_SolicitudAmpliacionExpressCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdSolicitudAmpliacionExpress", DbType.Int32, (entity.IdSolicitudAmpliacionExpress > 0) ? entity.IdSolicitudAmpliacionExpress : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdSolicitudAmpliacion", DbType.Int32, (entity.IdSolicitudAmpliacion > 0) ? entity.IdSolicitudAmpliacion : (object)DBNull.Value);                   
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, (entity.IdEstatus > 0) ? entity.IdEstatus : (object)DBNull.Value);
                    db.AddInParameter(com, "@IdPromotor", DbType.Int32, (entity.IdPromotor > 0) ? entity.IdPromotor : (object)DBNull.Value);
                    db.AddInParameter(com, "@NombreCliente", DbType.String, (entity.Nombre != null) ? entity.Nombre.Trim() : (object)DBNull.Value);
                    db.AddInParameter(com, "@FechaSolicitudDesde", DbType.Date, (entity.FechaAmpliacion.ValueIni != null) ? entity.FechaAmpliacion.ValueIni : (object)DBNull.Value);
                    db.AddInParameter(com, "@FechaSolicitudHasta", DbType.Date, (entity.FechaAmpliacion.ValueEnd != null) ? entity.FechaAmpliacion.ValueEnd : (object)DBNull.Value);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new Resultado<List<TB_SolicitudAmpliacionExpress>>();
                            TB_SolicitudAmpliacionExpress o;

                            while (reader.Read())
                            {
                                o = new TB_SolicitudAmpliacionExpress();

                                // Lectura de los datos del ResultSet
                                o.IdSolicitudAmpliacionExpress = Utils.GetInt(reader, "IdSolicitudAmpliacionExpress");
                                o.IdSolicitud  = Utils.GetInt(reader, "IdSolicitud");
                                o.DNI = Utils.GetString(reader, "DNI");
                                o.Nombre = Utils.GetString(reader, "Nombre");
                                o.ApellidoPaterno = Utils.GetString(reader, "ApellidoPaterno");
                                o.ApellidoMaterno = Utils.GetString(reader, "ApellidoMaterno");
                                o.MontoOtorgado = Utils.GetDecimal(reader, "MontoOtorgado");
                                o.PlazoOtorgado = Utils.GetInt(reader, "PlazoOtorgado");
                                o.FechaAmpliacion.Value = Utils.GetDateTime(reader, "FechaAmpliacion");
                                o.IdPromotor = Utils.GetInt(reader, "IdPromotor");
                                o.NombrePromotor = Utils.GetString(reader, "NombrePromotor");
                                o.Correo = Utils.GetString(reader, "Correo");
                                o.IdSolicitudAmpliacion = Utils.GetInt(reader, "IdSolicitudAmpliacion");
                                o.MontoLiquidacion = Utils.GetDecimal(reader, "MontoLiquidacion");
                                o.MontoDesembolsar = Utils.GetDecimal(reader, "MontoDesembolsar");
                                o.Telefono = Utils.GetString(reader, "Telefono");
                                o.Celular = Utils.GetString(reader, "Celular");
                                o.Mensaje = Utils.GetString(reader, "Mensaje");
                                o.IdEstatus = Utils.GetInt(reader, "IdEstatus");
                                o.Estatus = Utils.GetString(reader, "Estatus");
                                o.EstatusDesc = Utils.GetString(reader, "EstatusDesc");
                                o.FechaRegistro.Value = Utils.GetDateTime(reader, "FechaRegistro");
                                o.TasaAnual = Utils.GetDecimal(reader, "TasaAnual");
                                o.TasaCostoEfectiva = Utils.GetDecimal(reader, "TasaCostoEfectiva");
                                o.Cuota = Utils.GetDecimal(reader, "Cuota");

                                objReturn.ResultObject.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<List<TB_SolicitudAmpliacionExpress.ExpedienteDocumento>> ObtenerDocumentacion(int IdSolicitudAmpliacion)
        {
            Resultado<List<TB_SolicitudAmpliacionExpress.ExpedienteDocumento>> objReturn = new Resultado<List<TB_SolicitudAmpliacionExpress.ExpedienteDocumento>>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_TB_ExpedienteAmpliacionCON"))
                {
                    //Parametros                                        
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, IdSolicitudAmpliacion);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new Resultado<List<TB_SolicitudAmpliacionExpress.ExpedienteDocumento>>();
                            TB_SolicitudAmpliacionExpress.ExpedienteDocumento o;

                            while (reader.Read())
                            {
                                o = new TB_SolicitudAmpliacionExpress.ExpedienteDocumento();

                                // Lectura de los datos del ResultSet
                                o.IdExpedienteAmpliacion = Utils.GetInt(reader, "IdExpedienteAmpliacion");
                                o.IdDocumento = Utils.GetInt(reader, "IdDocumento");                                
                                o.Documento = Utils.GetString(reader, "Documento");                           
                                o.Orden = Utils.GetInt(reader, "Orden");                                
                                o.IdSolicitud = Utils.GetInt(reader, "IdSolicitud");                                
                                o.Actual = Utils.GetInt(reader, "Actual");                              
                                o.NombreArchivo = Utils.GetString(reader, "NombreArchivo");
                                o.GDIdArchivo = Utils.GetString(reader, "GDArchivo_Id");
                                o.FechaRegistro.Value = Utils.GetDateTime(reader, "FechaSys");

                                objReturn.ResultObject.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }
        
        public void EliminarDocumento(int IdExpedienteAmpliacion, int IdUsuario, string Comentario)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_TB_ExpedienteAmpliacionELI"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdExpedienteAmpliacion", DbType.Int32, IdExpedienteAmpliacion);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@Comentario", DbType.String, Comentario);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }          
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AutorizarCredito(int IdSolicitudAmpliacion, int IdUsuario)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_SolicitudAmpliacionExAutorizarPRO"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdSolicitudAmpliacion", DbType.Int32, IdSolicitudAmpliacion);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DenegarCredito(int IdSolicitudAmpliacion, int IdUsuario, string Comentario)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_SolicitudAmpliacionExDenegarPRO"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@IdSolicitudAmpliacion", DbType.Int32, IdSolicitudAmpliacion);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@Comentario", DbType.String, Comentario);

                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Resultado<List<TB_SolicitudAmpliacionExpress.EstatusHistorico>> ObtenerEstatusHistorico(int IdSolicitudAmpliacion)
        {
            Resultado<List<TB_SolicitudAmpliacionExpress.EstatusHistorico>> objReturn = new Resultado<List<TB_SolicitudAmpliacionExpress.EstatusHistorico>>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.TB_AMPEXEstatusHistorialCON"))
                {
                    //Parametros                                        
                    db.AddInParameter(com, "@IdSolicitudAmpliacion", DbType.Int32, IdSolicitudAmpliacion);

                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            objReturn = new Resultado<List<TB_SolicitudAmpliacionExpress.EstatusHistorico>>();
                            TB_SolicitudAmpliacionExpress.EstatusHistorico o;

                            while (reader.Read())
                            {
                                o = new TB_SolicitudAmpliacionExpress.EstatusHistorico();

                                // Lectura de los datos del ResultSet
                                o.IdSolicitudAmpliacion = Utils.GetInt(reader, "IdSolicitudAmpliacion");
                                o.IdEstatus = Utils.GetInt(reader, "IdEstatus");
                                o.Estatus = Utils.GetString(reader, "Estatus");
                                o.EstatusDesc = Utils.GetString(reader, "EstatusDesc");
                                o.IdUsuario = Utils.GetInt(reader, "IdUsuario");
                                o.NombreUsuario = Utils.GetString(reader, "NombreUsuario");
                                o.ApellidoPaternoUsuario = Utils.GetString(reader, "ApellidoPaternoUsuario");
                                o.ApellidoMaternoUsuario = Utils.GetString(reader, "ApellidoMaternoUsuario");
                                o.Comentario = Utils.GetString(reader, "Comentario");
                                o.FechaRegistro.Value = Utils.GetDateTime(reader, "FechaRegistro");

                                objReturn.ResultObject.Add(o);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }
    }
}