﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.IO;
using System.Linq;

namespace SOPF.Core.DataAccess
{
    public static class Extensiones
    {
        public static T ObtenerAtributoDe<T>(this object instancia, string nombrePropiedad) where T : Attribute
        {
            var tipoAtributo = typeof(T);
            var propiedad = instancia.GetType().GetProperty(nombrePropiedad);
            return (T)propiedad.GetCustomAttributes(tipoAtributo, false).FirstOrDefault();
        }
        public static string ObtenerPropiedadDe(this object instancia, string nombrePropiedad)
        {
            var propiedad = instancia.GetType().GetProperty(nombrePropiedad);
            return propiedad.Name;
        }

        public static void AlimentarParametrosDefault(this Database db, DbCommand com, object instancia, bool permitirNulos = false)
        {
            var propiedades = instancia.GetType().GetProperties();
            string nombreCampo = String.Empty;
            foreach (var prop in propiedades)
            {
                var atributoIgnorarPropiedad = instancia.ObtenerAtributoDe<NotMappedAttribute>(prop.Name);
                var valorAtributo = instancia.ObtenerAtributoDe<ColumnAttribute>(prop.Name);
                var valorPropiedad = prop.GetValue(instancia, null);

                if (!permitirNulos && valorPropiedad != null && atributoIgnorarPropiedad == null)
                {
                    nombreCampo = valorAtributo == null ? nombreCampo = instancia.ObtenerPropiedadDe(prop.Name) : valorAtributo.Name;
                    if (!String.IsNullOrEmpty(nombreCampo))
                    {
                        var tempType = prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? prop.PropertyType.GetGenericArguments()[0] : prop.PropertyType;
                        db.AddInParameter(com, "@" + nombreCampo, TypeConverter.ToDbType(tempType), prop.GetValue(instancia, null));
                    }
                }
            }
        }

    }
}
