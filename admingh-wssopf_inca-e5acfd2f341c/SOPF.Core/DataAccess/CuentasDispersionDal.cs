﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Creditos;

namespace SOPF.Core.DataAccess.Creditos
{
    public class CuentasDispersionDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public CuentasDispersionDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion

        public List<CuentasDispersion> CuentasDispersion(int Accion, int idBanco, int idCorte)
        {
            List<CuentasDispersion> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SelCuentas_Dispersion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, idBanco);
                    db.AddInParameter(com, "@IdCorte", DbType.Int32, idCorte);



                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<CuentasDispersion>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                CuentasDispersion dispersion = new CuentasDispersion();
                                if (!reader.IsDBNull(0)) dispersion.Operacion = reader[0].ToString();
                                if (!reader.IsDBNull(1)) dispersion.ClaveId = reader[1].ToString();
                                if (!reader.IsDBNull(2)) dispersion.CuentaOrigen = reader[2].ToString();
                                if (!reader.IsDBNull(3)) dispersion.CuentaDestino = reader[3].ToString();
                                if (!reader.IsDBNull(4)) dispersion.MontoTransferencia = reader[4].ToString();
                                if (!reader.IsDBNull(5)) dispersion.Referencia = reader[5].ToString();
                                if (!reader.IsDBNull(6)) dispersion.Descripcion = reader[6].ToString();
                                if (!reader.IsDBNull(7)) dispersion.RFC = reader[7].ToString();
                                if (!reader.IsDBNull(8)) dispersion.iva = Convert.ToInt32(reader[8]);
                                if (!reader.IsDBNull(9)) dispersion.instruccion = reader[9].ToString();
                                if (!reader.IsDBNull(10)) dispersion.TipoCambio = Convert.ToInt32(reader[10]);
                                if (!reader.IsDBNull(11)) dispersion.fecha = reader[11].ToString();
                                resultado.Add(dispersion);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
    }
}
