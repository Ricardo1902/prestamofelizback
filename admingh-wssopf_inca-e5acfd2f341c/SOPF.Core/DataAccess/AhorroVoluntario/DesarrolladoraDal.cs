#pragma warning disable 150527
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.AhorroVoluntario;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: DesarrolladoraDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla Desarrolladora
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-05-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.AhorroVoluntario
{
    public class DesarrolladoraDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public DesarrolladoraDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public int InsertarDesarrolladora(Desarrolladora entidad, int Accion)
        {
            int respuesta = 0;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.ACBDesarrolladora"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@NombreCorto", DbType.String, entidad.NombreCorto);
                    db.AddInParameter(com, "@RazonSocial", DbType.String, entidad.RazonSocial);
                    db.AddInParameter(com, "@RFC", DbType.String, entidad.RFC);
                    db.AddInParameter(com, "@Contacto", DbType.String, entidad.Contacto);
                    db.AddInParameter(com, "@email", DbType.String, entidad.Email);
                    db.AddInParameter(com, "@direccion", DbType.String, entidad.Direccion);
                    db.AddInParameter(com, "@NumExterior", DbType.String, entidad.NumExterior);
                    db.AddInParameter(com, "@NumInterior", DbType.String, entidad.NumInterior);
                    db.AddInParameter(com, "@idColonia", DbType.Int32, entidad.IdColonia);
                    db.AddInParameter(com, "@Telefono", DbType.String, entidad.Telefono);
                    db.AddInParameter(com, "@IdDesarrolladora", DbType.Int32, entidad.IdDesarrolladora);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {

                            

                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {


                                if (!reader.IsDBNull(0)) respuesta = Convert.ToInt32(reader[0]);


                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
            return respuesta;
        }
        
        public Desarrolladora ObtenerDesarrolladora()
        {
			Desarrolladora resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new Desarrolladora();
							//Lectura de los datos del ResultSet
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarDesarrolladora()
        {
        }

        public void EliminarDesarrolladora()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}