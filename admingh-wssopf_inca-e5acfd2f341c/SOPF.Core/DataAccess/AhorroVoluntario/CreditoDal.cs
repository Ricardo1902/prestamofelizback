#pragma warning disable 150408
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.AhorroVoluntario;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CreditoDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla Credito
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.AhorroVoluntario
{
    public class CreditoDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public CreditoDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public int InsertarCredito(Credito entidad, DateTime FechaPrimerPago)
        {

            
            DataResultAhorro resultado = null;
            int idcuenta = 0;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.InsertaCredito"))
				{
					//Parametros
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, entidad.IdCliente);
                    db.AddInParameter(com, "@Capital", DbType.Decimal, entidad.Capital);
                    db.AddInParameter(com, "@Frecuencia", DbType.String, entidad.Frecuencia);
                    db.AddInParameter(com, "@Plazo", DbType.Int32, entidad.Plazo);
                    db.AddInParameter(com, "@PrimerPago", DbType.DateTime, FechaPrimerPago);
                    db.AddInParameter(com, "@IdPromotor", DbType.Int32, entidad.IdPromotor);
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, entidad.idBanco);
                    db.AddInParameter(com, "@Clabe", DbType.String, entidad.Clabe);
                    db.AddInParameter(com, "@idTipoCred", DbType.Int32, entidad.idTipoCredito);
                    db.AddInParameter(com, "@IdSubTipo", DbType.Int32, entidad.SubTipo);
				
					//Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {

                            resultado = new DataResultAhorro();
                            
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                
                                
                                if (!reader.IsDBNull(1)) idcuenta = Convert.ToInt32(reader[1]);


                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
                    return idcuenta;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public Credito ObtenerCredito( int idcuenta)
        {
			Credito resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.Sel_DetalleCredito"))
				{
					//Parametros
                    db.AddInParameter(com, "@idcuenta", DbType.Int32, idcuenta);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new Credito();
                            while (reader.Read())
                            {
                                
                                if (!reader.IsDBNull(0)) resultado.Idcuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) resultado.Capital = Convert.ToDecimal(reader[1]);
                                if (!reader.IsDBNull(2)) resultado.Frecuencia = reader[2].ToString();
                                if (!reader.IsDBNull(3)) resultado.Plazo = Convert.ToInt32(reader[3]);
                                if (!reader.IsDBNull(4)) resultado.SdoCapital = Convert.ToDecimal(reader[4]);
                                if (!reader.IsDBNull(5)) resultado.IdEstatus = Convert.ToInt32(reader[5]);
                                if (!reader.IsDBNull(6)) resultado.IdCliente = Convert.ToInt32(reader[6]);
                                if (!reader.IsDBNull(7)) resultado.erogacion = Convert.ToDecimal(reader[7]);
                                if (!reader.IsDBNull(8)) resultado.FechaCredito = Convert.ToDateTime(reader[8]);
                                if (!reader.IsDBNull(9)) resultado.FechaEstatus = Convert.ToDateTime(reader[9]);
                                if (!reader.IsDBNull(10)) resultado.IdPromotor = Convert.ToInt32(reader[10]);
                                if (!reader.IsDBNull(11)) resultado.idBanco = Convert.ToInt32(reader[11]);
                                if (!reader.IsDBNull(12)) resultado.Clabe = reader[12].ToString();
                                if (!reader.IsDBNull(13)) resultado.idTipoCredito = Convert.ToInt32(reader[13]);
                                if (!reader.IsDBNull(14)) resultado.SubTipo = Convert.ToInt32(reader[14]);
                                if (!reader.IsDBNull(15)) resultado.fch_primerpago = Convert.ToDateTime(reader[15]);
                                if (!reader.IsDBNull(16)) resultado.Literal = reader[16].ToString();
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        public List<DataResultAhorro.SelCreditos> ObtenerCreditoPendientes(int accion, int idestatus, int credtio, int idDesarrolladora)
        {
            List<DataResultAhorro.SelCreditos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.Sel_Creditos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, accion);
                    db.AddInParameter(com, "@idEstatus", DbType.Int32, idestatus);
                    db.AddInParameter(com, "@Credito", DbType.Int32, credtio);
                    db.AddInParameter(com, "@idDesarrolladora", DbType.Int32, idDesarrolladora);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<DataResultAhorro.SelCreditos>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                DataResultAhorro.SelCreditos Pendientes = new DataResultAhorro.SelCreditos();
                                if (!reader.IsDBNull(0)) Pendientes.idcuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Pendientes.capital = Convert.ToDecimal(reader[1].ToString());
                                if (!reader.IsDBNull(2)) Pendientes.erogacion = Convert.ToDecimal(reader[2].ToString());
                                if (!reader.IsDBNull(3)) Pendientes.fecha = Convert.ToDateTime(reader[3].ToString());
                                if (!reader.IsDBNull(4)) Pendientes.cliente =(reader[4].ToString());
                                if (!reader.IsDBNull(5)) Pendientes.desarrolladora =(reader[5].ToString());

                                resultado.Add(Pendientes);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        
        public void ActualizarCredito()
        {
        }

        public void ActualizarEstatusCredito(int idcuenta, int idestatus)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.UpdEstatusCredito"))
                {
                    //Parametros
                    //db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);

                    db.AddInParameter(com, "@idcuenta", DbType.Int32, idcuenta);
                    db.AddInParameter(com, "@idestatus", DbType.Int32, idestatus);                  


                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void EliminarCredito()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}