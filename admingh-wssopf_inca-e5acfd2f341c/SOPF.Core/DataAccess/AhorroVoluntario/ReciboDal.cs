#pragma warning disable 150408
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.AhorroVoluntario;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: ReciboDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla Recibo
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.AhorroVoluntario
{
    public class ReciboDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public ReciboDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarRecibo(Recibo entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<Recibo> ObtenerRecibos(int idcuenta)
        {
			List<Recibo> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.SelTablaAmortizacion"))
				{
					//Parametros
                    db.AddInParameter(com, "@idcuenta", DbType.Int32, idcuenta);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
                            resultado = new List<Recibo>();
                            while (reader.Read())
                            {
                                Recibo recibo = new Recibo();
                                if (!reader.IsDBNull(0)) recibo.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) recibo.recibo = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) recibo.FchPago = Convert.ToDateTime(reader[2]);
                                if (!reader.IsDBNull(3)) recibo.Monto = Convert.ToDecimal(reader[3]);
                                if (!reader.IsDBNull(4)) recibo.SdoRecibo = Convert.ToDecimal(reader[4]);
                                if (!reader.IsDBNull(5)) recibo.IdEstatus =  Convert.ToInt32(reader[5].ToString());
                                resultado.Add(recibo);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        public List<DataResultAhorro.SelRecibos> ObtenerRecibosAhorro(int idcuenta)
        {
            List<DataResultAhorro.SelRecibos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.SelRecibos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@idcuenta", DbType.Int32, idcuenta);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<DataResultAhorro.SelRecibos>();
                            while (reader.Read())
                            {
                                DataResultAhorro.SelRecibos recibo = new DataResultAhorro.SelRecibos();
                                if (!reader.IsDBNull(0)) recibo.idcuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) recibo.recibo = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) recibo.fch_pago = Convert.ToDateTime(reader[2]);
                                if (!reader.IsDBNull(3)) recibo.sdo_insoluto = Convert.ToDecimal(reader[3]);
                                if (!reader.IsDBNull(4)) recibo.monto = Convert.ToDecimal(reader[4]);                                
                                resultado.Add(recibo);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        
        public void ActualizarRecibo()
        {
        }

        public void EliminarRecibo()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}