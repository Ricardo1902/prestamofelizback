#pragma warning disable 150807
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.AhorroVoluntario;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatTipoCreditoDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla CatTipoCredito
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-07 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.AhorroVoluntario
{
    public class CatTipoCreditoDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public CatTipoCreditoDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarCatTipoCredito(CatTipoCredito entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<CatTipoCredito> ObtenerCatTipoCredito(int Accion, int idTipo)
        {
			List<CatTipoCredito> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.SelTipoCredito"))
				{
					//Parametros
					db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdTipoCredito", DbType.Int32, idTipo);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
                            resultado = new List<CatTipoCredito>();
                            while (reader.Read())
                            {
                                CatTipoCredito TipoCredito = new CatTipoCredito();
                                if (!reader.IsDBNull(0)) TipoCredito.IdTipoCredito = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) TipoCredito.TipoCredito = reader[1].ToString();
                                resultado.Add(TipoCredito);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarCatTipoCredito()
        {
        }

        public void EliminarCatTipoCredito()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}