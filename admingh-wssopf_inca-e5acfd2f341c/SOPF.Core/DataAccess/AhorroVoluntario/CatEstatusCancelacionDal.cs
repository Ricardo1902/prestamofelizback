#pragma warning disable 150811
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.AhorroVoluntario;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CatEstatusCancelacionDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla CatEstatusCancelacion
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-11 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.AhorroVoluntario
{
    public class CatEstatusCancelacionDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public CatEstatusCancelacionDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods
        public void InsertarCatEstatusCancelacion(CatEstatusCancelacion entidad)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
                {
                    //Parametros
                    //db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CatEstatusCancelacion> ObtenerCatEstatusCancelacion(int Accion, int TipoEstatus)
        {
			List<CatEstatusCancelacion> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.SelEstatusCanc"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@TipoEstatus", DbType.Int32, TipoEstatus);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<CatEstatusCancelacion>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
	{
		CatEstatusCancelacion EstatusCan = new CatEstatusCancelacion();
		if (!reader.IsDBNull(0)) EstatusCan.IdEstatusCan = Convert.ToInt32(reader[0]);
		if (!reader.IsDBNull(1)) EstatusCan.Descripcion = reader[1].ToString();
		if (!reader.IsDBNull(2)) EstatusCan.Activo = Convert.ToBoolean(reader[2]);
		if (!reader.IsDBNull(3)) EstatusCan.TipoCancelacion = Convert.ToInt32(reader[3]);
		resultado.Add(EstatusCan);
	}
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }

        public void ActualizarCatEstatusCancelacion()
        {
        }

        public void EliminarCatEstatusCancelacion()
        {
        }
        #endregion

        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}