#pragma warning disable 150408
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.AhorroVoluntario;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: ClienteDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla Cliente
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.AhorroVoluntario
{
    public class ClienteDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public ClienteDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods
        public int InsertarCliente(Cliente entidad)
        {
            int resultado = 0;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.InsertarCliente"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Nombre", DbType.String, entidad.NombreCompleto);
                    db.AddInParameter(com, "@Domicilio", DbType.String, entidad.Domicilio);
                    db.AddInParameter(com, "@RFC", DbType.String, entidad.RFC);
                    db.AddInParameter(com, "@TelCasa", DbType.String, entidad.TelCasa);
                    db.AddInParameter(com, "@TelCelular", DbType.String, entidad.TelCelular);
                    db.AddInParameter(com, "@NSS", DbType.String, entidad.NSS);
                    db.AddInParameter(com, "@Nombres", DbType.String, entidad.Nombres);
                    db.AddInParameter(com, "@ApePat", DbType.String, entidad.ApePat);
                    db.AddInParameter(com, "@ApeMat", DbType.String, entidad.ApeMat);
                    db.AddInParameter(com, "@RegPatronal", DbType.String, entidad.RegistroPatronal);
                    db.AddInParameter(com, "@Email", DbType.String, entidad.EMail);
                    db.AddInParameter(com, "@NombrePatron", DbType.String, entidad.NombrePatron);
                    db.AddInParameter(com, "@DomNuevo", DbType.String, entidad.DomNuevo);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {

                                if (!reader.IsDBNull(0)) resultado = Convert.ToInt32(reader[0]);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return resultado;
        }

        public List<Cliente> ObtenerCliente(int Accion, int idCliente,  string Nombre)
        {
            List<Cliente> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.BuscarCliente"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, idCliente);
                    db.AddInParameter(com, "@Nombre", DbType.String, Nombre);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<Cliente>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                Cliente cliente = new Cliente();
                                if (!reader.IsDBNull(0)) cliente.Idcliente = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) cliente.NombreCompleto = reader[1].ToString();
                                if (!reader.IsDBNull(2)) cliente.Domicilio = reader[2].ToString();
                                if (!reader.IsDBNull(3)) cliente.RFC = reader[3].ToString();
                                if (!reader.IsDBNull(4)) cliente.TelCasa = reader[4].ToString();
                                if (!reader.IsDBNull(5)) cliente.TelCelular = reader[5].ToString();
                                if (!reader.IsDBNull(6)) cliente.NSS = reader[6].ToString();
                                if (!reader.IsDBNull(7)) cliente.Nombres = reader[7].ToString();
                                if (!reader.IsDBNull(8)) cliente.ApePat = reader[8].ToString();
                                if (!reader.IsDBNull(9)) cliente.ApeMat = reader[9].ToString();
                                if (!reader.IsDBNull(10)) cliente.RegistroPatronal = reader[10].ToString();
                                if (!reader.IsDBNull(11)) cliente.EMail = reader[11].ToString();
                                if (!reader.IsDBNull(12)) cliente.NombrePatron = reader[12].ToString();
                                if (!reader.IsDBNull(13)) cliente.Edad = Convert.ToInt32(reader[13].ToString());

                                resultado.Add(cliente);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public void ActualizarCliente()
        {
        }

        public void EliminarCliente()
        {
        }
        #endregion

        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}