#pragma warning disable 150920
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.AhorroVoluntario;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: ReferenciaDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla Referencia
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-09-20 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.AhorroVoluntario
{
    public class ReferenciaDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public ReferenciaDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public int InsertarReferencia(Referencia entidad)
        {
            int Bandera = 0;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.InsertarReferencia"))
				{
					//Parametros
                    db.AddInParameter(com, "@Nombre", DbType.String, entidad.Nombre);
                    db.AddInParameter(com, "@Telefono", DbType.String, entidad.Telefono);
                    db.AddInParameter(com, "@idcuenta", DbType.Int32, entidad.IdCuenta);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
                    Bandera = 0;
				}
			}
			catch (Exception ex)
			{
                Bandera = 1;
				throw ex;
			}
            return Bandera;
        }
        
        public List<Referencia> ObtenerReferencia(int accion,   int idcuenta)
        {
			List<Referencia> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.Sel_referencias"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, accion);
                    db.AddInParameter(com, "@idcuenta", DbType.Int32, idcuenta);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<Referencia>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                Referencia referencia = new Referencia();
                                if (!reader.IsDBNull(0)) referencia.IdReferencia = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) referencia.Nombre = reader[1].ToString();
                                if (!reader.IsDBNull(2)) referencia.Telefono = reader[2].ToString();
                                if (!reader.IsDBNull(3)) referencia.IdCuenta = Convert.ToInt32(reader[3]);
                                if (!reader.IsDBNull(4)) referencia.IdEstatus = Convert.ToBoolean(reader[4]);
                                resultado.Add(referencia);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarReferencia()
        {
        }

        public void EliminarReferencia()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}