#pragma warning disable 150622
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.AhorroVoluntario;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: PromotorDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla Promotor
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-06-22 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.AhorroVoluntario
{
    public class PromotorDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public PromotorDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarPromotor(Promotor entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<Promotor> ObtenerPromotor(int Accion, int IdDesarrolladora, int idPromotor, int idUsuario)
        {
			List<Promotor> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AhorroVoluntario.SelPromotor"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdDesarrolladora", DbType.Int32, IdDesarrolladora);
                    db.AddInParameter(com, "@IdPromotor", DbType.Int32, idPromotor);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, idUsuario);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<Promotor>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                Promotor promotor = new Promotor();
                                if (!reader.IsDBNull(0)) promotor.IdPromotor = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) promotor.Nombre = reader[1].ToString();
                                if (!reader.IsDBNull(2)) promotor.IdDesarrolladora = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) promotor.IdTipoPromotor = Convert.ToInt32(reader[3]);
                                if (!reader.IsDBNull(4)) promotor.IdEstatus = Convert.ToBoolean(reader[4]);
                                if (!reader.IsDBNull(5)) promotor.Idusuario = Convert.ToInt32(reader[5]);
                                resultado.Add(promotor);
                            }

						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarPromotor()
        {
        }

        public void EliminarPromotor()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}