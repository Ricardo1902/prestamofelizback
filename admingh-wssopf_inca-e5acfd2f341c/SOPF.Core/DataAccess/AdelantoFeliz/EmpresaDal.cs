#pragma warning disable 150412
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Adelantofeliz;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: EmpresaDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla Empresa
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-12 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Adelantofeliz
{
    public class EmpresaDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public EmpresaDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarEmpresa(Empresa entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<Empresa> ObtenerEmpresa(int Accion, int IdEmpresa)
        {
			List<Empresa> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AdelantoFeliz.SelCatalogoEmpresa"))
				{
					//Parametros
					db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdEmpresa", DbType.Int32, IdEmpresa);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<Empresa>();
                            while (reader.Read())
                            {
                                Empresa empresa = new Empresa();
                                if (!reader.IsDBNull(0)) empresa.IdEmpresa = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) empresa.Nombre = reader[1].ToString();
                                if (!reader.IsDBNull(2)) empresa.RFC = reader[2].ToString();
                                if (!reader.IsDBNull(3)) empresa.Direccion = reader[3].ToString();
                                resultado.Add(empresa);
                            }

						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarEmpresa()
        {
        }

        public void EliminarEmpresa()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}