#pragma warning disable 150408
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Adelantofeliz;

# region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: EmpleadosDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla Empleados
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Adelantofeliz
{
    public class EmpleadosDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public EmpleadosDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarEmpleados(Empleados entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("adelantofeliz.AltaEmpleados"))
                {
                    //Parametros
                    //db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);

                    db.AddInParameter(com, "@Nombre", DbType.String, entidad.Nombre);
                    db.AddInParameter(com, "@ApellidoPaterno", DbType.String, entidad.ApellidoPaterno);
                    db.AddInParameter(com, "@ApellidoMaterno", DbType.String, entidad.ApellidoMaterno);
                    db.AddInParameter(com, "@RFC", DbType.String, entidad.RFC);
                    db.AddInParameter(com, "@CURP", DbType.String, entidad.CURP);
                    db.AddInParameter(com, "@LugNacimiento", DbType.String, entidad.LugNacimiento);
                    db.AddInParameter(com, "@FechaNacimiento", DbType.DateTime, entidad.FechaNacimiento);
                    db.AddInParameter(com, "@Nacionalidad", DbType.String, entidad.Nacionalidad);
                    db.AddInParameter(com, "@Calle", DbType.String, entidad.Calle);
                    db.AddInParameter(com, "@NumeroExterior", DbType.String, entidad.NumeroExterior);
                    db.AddInParameter(com, "@NummeroInterior", DbType.String, entidad.NummeroInterior);
                    db.AddInParameter(com, "@Colonia", DbType.String, entidad.Colonia);
                    db.AddInParameter(com, "@Ciudad", DbType.String, entidad.Ciudad);
                    db.AddInParameter(com, "@CodigoPostal", DbType.String, entidad.CodigoPostal);
                    db.AddInParameter(com, "@Municipio", DbType.String, entidad.Municipio);
                    db.AddInParameter(com, "@Estado", DbType.String, entidad.Estado);
                    db.AddInParameter(com, "@TelefonoCasa", DbType.String, entidad.TelefonoCasa);
                    db.AddInParameter(com, "@TelefonoTrabajo", DbType.String, entidad.TelefonoTrabajo);
                    db.AddInParameter(com, "@Celular", DbType.String, entidad.Celular);
                    db.AddInParameter(com, "@CorreoElectronico", DbType.String, entidad.CorreoElectronico);
                    db.AddInParameter(com, "@IngresoNeto", DbType.String, entidad.IngresoNeto);
                    db.AddInParameter(com, "@NumEmpleado", DbType.String, entidad.NumEmpleado);
                    db.AddInParameter(com, "@IdEmpresa", DbType.Int32, entidad.IdEmpresa);
                    db.AddInParameter(com, "@Puesto", DbType.String, entidad.Puesto);
                    db.AddInParameter(com, "@Localidadtrabajo", DbType.String, entidad.Localidadtrabajo);
                    db.AddInParameter(com, "@FechaIngreso", DbType.String, entidad.FechaIngreso);
                    db.AddInParameter(com, "@DiaCorte", DbType.Int32, entidad.DiaCorte);
                    db.AddInParameter(com, "@CicloPago", DbType.String, entidad.CicloPago);
                    db.AddInParameter(com, "@Banco", DbType.String, entidad.Banco);
                    db.AddInParameter(com, "@Clabe", DbType.String, entidad.Clabe);
                    db.AddInParameter(com, "@Tarjeta", DbType.String, entidad.Tarjeta);


                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<Empleados> ObtenerEmpleados(int Accion, int IdEmpresa, int NumEmpleado, string Nombre, string RFC, string CURP, int Id)
        {
			List<Empleados> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("AdelantoFeliz.ConsultaEmpleado"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdEmpresa", DbType.Int32, IdEmpresa);
                    db.AddInParameter(com, "@NumEmpleado", DbType.Int32, NumEmpleado);
                    db.AddInParameter(com, "@Nombre", DbType.String, Nombre);
                    db.AddInParameter(com, "@RFC", DbType.String, RFC);
                    db.AddInParameter(com, "@CURP", DbType.String, CURP);
                    db.AddInParameter(com, "@Id", DbType.Int32, Id);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<Empleados>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                Empleados empleado = new Empleados();
                                if (!reader.IsDBNull(0)) empleado.Id = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) empleado.Nombre = reader[1].ToString();
                                if (!reader.IsDBNull(2)) empleado.ApellidoPaterno = reader[2].ToString();
                                if (!reader.IsDBNull(3)) empleado.ApellidoMaterno = reader[3].ToString();
                                if (!reader.IsDBNull(4)) empleado.RFC = reader[4].ToString();
                                if (!reader.IsDBNull(5)) empleado.CURP = reader[5].ToString();
                                if (!reader.IsDBNull(6)) empleado.LugNacimiento = reader[6].ToString();
                                if (!reader.IsDBNull(7)) empleado.FechaNacimiento = Convert.ToDateTime(reader[7]);
                                if (!reader.IsDBNull(8)) empleado.Nacionalidad = reader[8].ToString();
                                if (!reader.IsDBNull(9)) empleado.Calle = reader[9].ToString();
                                if (!reader.IsDBNull(10)) empleado.NumeroExterior = reader[10].ToString();
                                if (!reader.IsDBNull(11)) empleado.NummeroInterior = reader[11].ToString();
                                if (!reader.IsDBNull(12)) empleado.Colonia = reader[12].ToString();
                                if (!reader.IsDBNull(13)) empleado.Ciudad = reader[13].ToString();
                                if (!reader.IsDBNull(14)) empleado.CodigoPostal = reader[14].ToString();
                                if (!reader.IsDBNull(15)) empleado.Municipio = reader[15].ToString();
                                if (!reader.IsDBNull(16)) empleado.Estado = reader[16].ToString();
                                if (!reader.IsDBNull(17)) empleado.TelefonoCasa = reader[17].ToString();
                                if (!reader.IsDBNull(18)) empleado.TelefonoTrabajo = reader[18].ToString();
                                if (!reader.IsDBNull(19)) empleado.Celular = reader[19].ToString();
                                if (!reader.IsDBNull(20)) empleado.CorreoElectronico = reader[20].ToString();
                                if (!reader.IsDBNull(21)) empleado.IngresoNeto = reader[21].ToString();
                                if (!reader.IsDBNull(22)) empleado.NumEmpleado = reader[22].ToString();
                                if (!reader.IsDBNull(23)) empleado.IdEmpresa = Convert.ToInt32(reader[23]);
                                if (!reader.IsDBNull(24)) empleado.Puesto = reader[24].ToString();
                                if (!reader.IsDBNull(25)) empleado.Localidadtrabajo = reader[25].ToString();
                                if (!reader.IsDBNull(26)) empleado.FechaIngreso = Convert.ToDateTime(reader[26]);
                                if (!reader.IsDBNull(27)) empleado.DiaCorte = Convert.ToInt32(reader[27]);
                                if (!reader.IsDBNull(28)) empleado.CicloPago = reader[28].ToString();
                                if (!reader.IsDBNull(29)) empleado.Banco = reader[29].ToString();
                                if (!reader.IsDBNull(30)) empleado.Clabe = reader[30].ToString();
                                if (!reader.IsDBNull(31)) empleado.Tarjeta = reader[31].ToString();
                                if (!reader.IsDBNull(32)) empleado.Estatus = Convert.ToBoolean(reader[32]);
                                resultado.Add(empleado);
                            }

						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }

        public int ConsultarEmpresa(int idusuario)
        {
            int idEmpresa = 0;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Adelantofeliz.ConsultarEmpresa"))
                {
                    //Parametros
                    db.AddInParameter(com, "@usuarioid", DbType.Int32, idusuario);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {

                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {

                                if (!reader.IsDBNull(0)) idEmpresa = Convert.ToInt32(reader[0]);


                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return idEmpresa;
        }
        
        public void ActualizarEmpleados()
        {
        }

        public void EliminarEmpleados()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}