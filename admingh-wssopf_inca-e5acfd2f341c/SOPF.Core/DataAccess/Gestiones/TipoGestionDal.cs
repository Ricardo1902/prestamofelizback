﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace SOPF.Core.DataAccess.Gestiones
{
    public class TipoGestionDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TipoGestionDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods
        public List<TipoGestion> ObtenerTipoGestion(int accion)
        {
            List<TipoGestion> respuesta = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_TipoGestionCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.Int32, accion);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            var entidadCartera = Utils.DataReaderAEntidad<TipoGestion>(reader);
                            if (entidadCartera != null && entidadCartera.Count > 0)
                            {
                                respuesta = entidadCartera;
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return respuesta;
        }
        #endregion
    }
}
