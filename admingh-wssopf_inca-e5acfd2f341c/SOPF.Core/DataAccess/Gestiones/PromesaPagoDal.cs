#pragma warning disable 140827
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Gestiones;

# region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: PromesaPagoDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla PromesaPago
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Gestiones
{
    public class PromesaPagoDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public PromesaPagoDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarPromesaPago(PromesaPago entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_PromesaPagoALT"))
				{
					//Parametros
                    db.AddInParameter(com, "@IdGestion", DbType.Int32, entidad.IdGestion);
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, entidad.IdCuenta);
                    db.AddInParameter(com, "@fch_promoesa", DbType.DateTime, entidad.FchPromesa);
                    db.AddInParameter(com, "@Monto", DbType.Decimal, entidad.MontoPromesa);
                    db.AddInParameter(com, "@TipoCobro", DbType.Int32, entidad.idTipoCobro);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<PromesaPago> ObtenerPromesaPago(int idCuenta)
        {
			List<PromesaPago> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_PromesaPagoCON"))
				{
					//Parametros
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, idCuenta);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<PromesaPago>();
                            while (reader.Read())
                            {
                                PromesaPago promesa = new PromesaPago();
                                if (!reader.IsDBNull(0)) promesa.FchPromesa = Convert.ToDateTime(reader[0]);
                                if (!reader.IsDBNull(1)) promesa.MontoPromesa = Convert.ToDecimal(reader[1]);
                                if (!reader.IsDBNull(2)) promesa.TipoCobro = reader[2].ToString();
                                resultado.Add(promesa);
                            }
						}
                        reader.Close();
						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }

        public List<PromesaPago> ReportePromesaPago(int idcuenta, DateTime Fec_Inicial, DateTime Fec_Final, int Accion)
        {
            List<PromesaPago> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_RepPromesaPagoCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, idcuenta);
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, Fec_Inicial);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, Fec_Final);
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<PromesaPago>();
                            while (reader.Read())
                            {
                                PromesaPago promesa = new PromesaPago();
                                if (!reader.IsDBNull(0)) promesa.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) promesa.FchPromesa = Convert.ToDateTime(reader[1]);
                                if (!reader.IsDBNull(2)) promesa.MontoPromesa = Convert.ToDecimal(reader[2]);
                                if (!reader.IsDBNull(3)) promesa.TipoCobro = reader[3].ToString();
                                if (!reader.IsDBNull(4)) promesa.Cliente = reader[4].ToString();
                                resultado.Add(promesa);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        
        public void ActualizarPromesaPago()
        {
        }

        public void EliminarPromesaPago()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}