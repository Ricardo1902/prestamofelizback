﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.Entities.Creditos;

namespace SOPF.Core.DataAccess.Gestiones
{
    public class AvisosDal
    {
        #region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public AvisosDal()
		{
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public int InsertarAvisos(int IdCuenta, int Carteo, int sms, int wats, int mail, int circulo, int fisico)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                int resultado = 0;
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_AvisosGestionALT"))
				{

					//Parametros
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, IdCuenta);
                    db.AddInParameter(com, "@Carteo", DbType.Int32, Carteo);
                    db.AddInParameter(com, "@sms", DbType.String, sms);
                    db.AddInParameter(com, "@watsapp", DbType.Int32, wats);
                    db.AddInParameter(com, "@mailing", DbType.String, mail);
                    db.AddInParameter(com, "@circuloCredito", DbType.Int32, circulo);
                    db.AddInParameter(com, "@fisico", DbType.Int32, fisico);
                    

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
				}
                return resultado;
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }

        public Avisos ObtenerAvisos(int idCuenta)
        {
            Avisos resultado = new Avisos();
            //return dal.ObtenerAvisos(idCuenta);
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_ObtenerAvisosGestionCON"))
                {

                    //Parametros
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, idCuenta);
                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {

                            //Le    ctura de los datos del ResultSet
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) resultado.IdAvisos = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) resultado.IdCuenta = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) resultado.Carteo = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) resultado.sms = Convert.ToInt32(reader[3]);
                                if (!reader.IsDBNull(4)) resultado.watsapp = Convert.ToInt32(reader[4]);
                                if (!reader.IsDBNull(5)) resultado.mailing = Convert.ToInt32(reader[5]);
                                if (!reader.IsDBNull(6)) resultado.circuloCredito = Convert.ToInt32(reader[6]);
                                if (!reader.IsDBNull(7)) resultado.fisico = Convert.ToInt32(reader[7]);
                                //if (!reader.IsDBNull(6)) resultado.Rfc = Convert.ToInt32(reader[6]);
                              

                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public DetalleCuenta ObtenerDetalleCuenta(int idCuenta)
        //{
    }
}
        #endregion