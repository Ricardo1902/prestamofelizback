#pragma warning disable 150616
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Gestiones;

# region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBDevolverLlamadaDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBDevolverLlamada
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-06-16 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Gestiones
{
    public class TBDevolverLlamadaDal
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBDevolverLlamadaDal()
		{
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBDevolverLlamada(int IdGestion, int IdCuenta, DateTime FchDevLlamada, int IdCliente, int IdGestor)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_DevolverLlamadaALT"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
                    db.AddInParameter(com, "@IdGestion", DbType.Int32, IdGestion);
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, IdCuenta);
                    db.AddInParameter(com, "@Fch_DevLlamada", DbType.DateTime, FchDevLlamada);
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, IdCliente);
                    db.AddInParameter(com, "@IdGestor", DbType.Int32, IdGestor);

                    
                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }

        public List<TBDevolverLlamada> ObtenerTBDevolverLlamada(int idcuenta)
        {
            List<TBDevolverLlamada> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_DevolverLlamadaCON"))
                {
                    //Parametros

                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, idcuenta);               

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBDevolverLlamada>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBDevolverLlamada gestiones = new TBDevolverLlamada();
                                if (!reader.IsDBNull(0)) gestiones.IdDevLlamada = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) gestiones.IdGestion = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) gestiones.IdCuenta = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) gestiones.FchDevLlamada = Convert.ToDateTime(reader[3]);
                                if (!reader.IsDBNull(4)) gestiones.IdCliente = Convert.ToInt32(reader[4]);
                                if (!reader.IsDBNull(5)) gestiones.Cliente = reader[5].ToString();
                                if (!reader.IsDBNull(6)) gestiones.IdGestor = Convert.ToInt32(reader[6]);
                                if (!reader.IsDBNull(7)) gestiones.Gestor = reader[7].ToString();
                                if (!reader.IsDBNull(8)) gestiones.Comentario = reader[8].ToString();
                                if (!reader.IsDBNull(9)) gestiones.ResultadoGestion = reader[9].ToString();
                                resultado.Add(gestiones);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        
        public void ActualizarTBDevolverLlamada()
        {
        }

        public void EliminarTBDevolverLlamada()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}