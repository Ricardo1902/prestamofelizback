﻿using Newtonsoft.Json;
using SOPF.Core.BusinessLogic.Sistema;
using SOPF.Core.Entities;
using SOPF.Core.Model.Catalogo;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace SOPF.Core.DataAccess.Gestiones
{
    public class ArchivoGestionDal
    {
        public CreditOrigContext con { get; set; }
        public ArchivoGestionDal(CreditOrigContext con)
        {
            this.con = con;
        }

        public ArchivoGestionVM ObtenerArchivoGestionPorId(int idArchivoGestion)
        {
            ArchivoGestionVM archivo = null;
            try
            {
                if (idArchivoGestion <= 0) throw new Exception("La petición es inválida");
                var encontrado = ConsultaArchivoGestionVM(ag => ag.IdArchivoGestion == idArchivoGestion).FirstOrDefault();
                if (encontrado != null)
                    archivo = encontrado;
            }
            catch (Exception)
            {
            }

            return archivo;
        }
        public List<ArchivoGestionVM> ObtenerArchivoGestionPorSolicitud(int idSolicitud)
        {
            List<ArchivoGestionVM> archivosGestion = new List<ArchivoGestionVM>();
            try
            {
                if (idSolicitud <= 0) throw new Exception("La petición es inválida");
                var encontrados = ConsultaArchivoGestionVM(ag => ag.IdSolicitud == idSolicitud && ag.Activo == true).ToList();
                if (encontrados != null)
                    archivosGestion = encontrados;
            }
            catch (Exception ex)
            {
            }
            return archivosGestion;
        }
        public Resultado<bool> InsertarArchivoGestion(ArchivoGestion archivoGestion)
        {
            if (archivoGestion == null) throw new Exception("La petición es inválida");
            Resultado<bool> respuesta = new Resultado<bool>();
            using (var tran = con.Database.BeginTransaction())
            {
                try
                {
                    archivoGestion.Activo = true;
                    con.Gestiones_ArchivoGestion.Add(archivoGestion);
                    con.SaveChanges();

                    respuesta.Codigo = 1;
                    respuesta.Mensaje = "El archivo se inserto con exito";
                    respuesta.ResultObject = true;

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    respuesta.Codigo = 2;
                    respuesta.Mensaje = "Ha ocurrido un error al insertar el archivo.";
                    respuesta.ResultObject = false;
                }
            }
            return respuesta;
        }
        public Resultado<bool> ActualizarArchivoGestion(ArchivoGestion archivoGestion)
        {
            if (archivoGestion == null) throw new Exception("La petición es inválida");
            if (archivoGestion.IdArchivoGestion <= 0) throw new Exception("La clave del archivo es incorrecta.");
            Resultado<bool> respuesta = new Resultado<bool>();
            using (var tran = con.Database.BeginTransaction())
            {
                try
                {
                    var registro = ConsultaArchivoGestion(tag => tag.IdArchivoGestion == archivoGestion.IdArchivoGestion).FirstOrDefault();
                    if (registro != null)
                    {
                        if (!registro.Activo) throw new Exception("La referencia es inválida.");
                        if (registro.CompareTo(archivoGestion) > 0)
                        {
                            registro.Nombre = archivoGestion.Nombre;
                            registro.Comentario = archivoGestion.Comentario;
                            registro.IdTipoArchivo = archivoGestion.IdTipoArchivo;
                            registro.IdDocumentoDestino = archivoGestion.IdDocumentoDestino;
                            registro.IdServidorArchivos = archivoGestion.IdServidorArchivos;
                            registro.IdArchivoServidor = archivoGestion.IdArchivoServidor;
                            registro.UrlArchivo = archivoGestion.UrlArchivo;
                            registro.IdSolicitud = archivoGestion.IdSolicitud;
                            registro.IdUsuarioRegistro = archivoGestion.IdUsuarioRegistro;
                            registro.FechaRegistro = archivoGestion.FechaRegistro;
                            registro.Activo = true;

                            con.SaveChanges();

                        }

                        respuesta.Codigo = 1;
                        respuesta.Mensaje = "El archivo se actualizó con éxito";
                        respuesta.ResultObject = true;

                        tran.Commit();
                    }
                }
                catch (Exception)
                {
                    tran.Rollback();
                }
            }
            return respuesta;
        }
        public Resultado<bool> EliminarArchivoGestion(int idArchivoGestion, int idSolicitud)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            using (var tran = con.Database.BeginTransaction())
            {
                try
                {
                    if (idArchivoGestion <= 0) throw new Exception("Peticion inválida. La clave del archivo es inválida.");
                    var registro = con.Gestiones_ArchivoGestion.SingleOrDefault(ag => ag.IdArchivoGestion == idArchivoGestion && ag.IdSolicitud == idSolicitud);
                    if (registro != null && registro.IdArchivoGestion > 0)
                    {
                        if (registro.Activo)
                        {
                            //Delete logico
                            registro.Activo = false;
                            con.SaveChanges();

                            respuesta.Codigo = 1;
                            respuesta.ResultObject = true;
                            respuesta.Mensaje = "El archivo se eliminó con éxito.";

                            tran.Commit();
                        }
                    }

                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    respuesta.Codigo = 0;
                    respuesta.ResultObject = false;
                    respuesta.Mensaje = ex.Message;
                }
                return respuesta;
            }
        }

        public Resultado<bool> SubirArchivoGoogleDrive(int idSolicitud, int idUsuario, CargaArchivoGoogleDrive peticion)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            try
            {
                using (ServidorGoogleDrive googleDrive = new ServidorGoogleDrive())
                {
                    string idArchivoDrive = null, urlArchivoDrive = null;
                    var carpetaGestiones = System.Configuration.ConfigurationManager.AppSettings["GOOGLE_DRIVE_CARPETA_GESTIONES"];
                    if (String.IsNullOrEmpty(carpetaGestiones)) throw new Exception("No fue posible asignar una carpeta para el archivo, favor de revisar la configuracion");
                    peticion.IdCarpeta = carpetaGestiones;

                    var respuestaArchivo = googleDrive.SubirArchivo(peticion, ref idArchivoDrive, ref urlArchivoDrive);

                    if (respuestaArchivo != null && !String.IsNullOrEmpty(idArchivoDrive))
                    {
                        var respuestaInsertar = InsertarArchivoGestion(new ArchivoGestion
                        {
                            Nombre = peticion.NombreArchivo,
                            Comentario = peticion.Comentario,
                            IdTipoArchivo = peticion.IdTipoArchivo,
                            IdDocumentoDestino = peticion.IdDocumentoDestino,
                            IdServidorArchivos = peticion.IdServidorArchivos,
                            IdArchivoServidor = idArchivoDrive,
                            UrlArchivo = urlArchivoDrive,
                            IdSolicitud = idSolicitud,
                            IdUsuarioRegistro = idUsuario
                        });
                        if (respuestaInsertar != null && respuestaArchivo != null)
                        {
                            respuesta = respuestaArchivo;
                        }
                    }
                    else
                    {
                        respuesta.Codigo = 0;
                        respuesta.Mensaje = "Ocurrió un error al subir el archivo al servidor correspondiente.";
                        respuesta.ResultObject = false;
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.Mensaje = ex.Message;
                respuesta.ResultObject = false;

                peticion.Contenido = null; //Limpiar contenido de archivo para log.
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(peticion)}", JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        public Resultado<ArchivoGoogleDrive> DescargarArchivoGoogleDrive(string idArchivo)
        {
            Resultado<ArchivoGoogleDrive> respuesta = new Resultado<ArchivoGoogleDrive>();
            string error = "";
            try
            {
                using (ServidorGoogleDrive googleDrive = new ServidorGoogleDrive())
                {
                    var archivoDrive = googleDrive.DescargarArchivo(idArchivo, ref error);
                    if (archivoDrive != null)
                    {
                        if (String.IsNullOrWhiteSpace(error))
                        {
                            respuesta.Codigo = 1;
                            respuesta.Mensaje = "El archivo se decargo con exito.";
                            respuesta.ResultObject = archivoDrive;
                        }
                        else
                        {
                            respuesta.Codigo = 0;
                            respuesta.Mensaje = error;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.Mensaje = "Ocurrió un error al descargar el archivo.";

                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(idArchivo)}", JsonConvert.SerializeObject(ex));
            }

            return respuesta;
        }
        private IQueryable<ArchivoGestion> ConsultaArchivoGestion(Expression<Func<ArchivoGestion, bool>> predicado)
        {
            var query = (from ag in con.Gestiones_ArchivoGestion
                         select ag).Where(predicado);

            return query;
        }
        private IQueryable<ArchivoGestionVM> ConsultaArchivoGestionVM(Expression<Func<ArchivoGestionVM, bool>> predicado)
        {
            var query = (from ag in con.Gestiones_ArchivoGestion
                         join tag in con.Gestiones_TipoArchivoGestion on ag.IdTipoArchivo equals tag.IdTipoArchivoGestion
                         join dd in con.Gestiones_DocumentoDestino on ag.IdDocumentoDestino equals dd.IdDocumentoDestino
                         join n in con.Gestiones_TB_CATNotificaciones on dd.IdNotificacion equals n.IdNotificacion
                         join dn in con.Gestiones_TB_CATDestinoNotificacion on dd.IdDestinoNotificacion equals dn.IdDestinoNotificacion
                         select new ArchivoGestionVM
                         {
                             IdArchivoGestion = ag.IdArchivoGestion,
                             Nombre = ag.Nombre,
                             Comentario = ag.Comentario,
                             IdTipoArchivo = ag.IdTipoArchivo,
                             IdDocumentoDestino = ag.IdDocumentoDestino,
                             NombreDocumento = n.Notificacion + " " + dn.DestinoNotificacion,
                             TipoArchivo = tag.TipoArchivo,
                             Icono = tag.Icono,
                             IdServidorArchivos = ag.IdServidorArchivos,
                             IdArchivoServidor = ag.IdArchivoServidor,
                             UrlArchivo = ag.UrlArchivo,
                             IdSolicitud = ag.IdSolicitud,
                             TipoMime = tag.TipoMime,
                             IdUsuarioRegistro = ag.IdUsuarioRegistro,
                             FechaRegistro = ag.FechaRegistro,
                             Activo = ag.Activo,

                         }).Where(predicado);

            return query;
        }

    }
}
