﻿using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace SOPF.Core.DataAccess.Gestiones
{
    public class DocumentoDestinoDal
    {
        private CreditOrigContext con;
        public DocumentoDestinoDal(CreditOrigContext con)
        {
            this.con = con;
        }

        public ObtenerDocumentoDestinoResponse ObtenerDocumentoDestinoPorId(ObtenerDocumentoDestinoRequest peticion)
        {
            ObtenerDocumentoDestinoResponse respuesta = new ObtenerDocumentoDestinoResponse();
            try
            {
                var encontrados = ConsultaDocumentoDestinoVM(dd => dd.IdDocumentoDestino == peticion.IdDocumentoDestino).FirstOrDefault();
                if (encontrados != null)
                    respuesta.Resultado.DocumentoDestino = encontrados;
            }
            catch (Exception)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrio un error al obtener la informacion.";
            }
            return respuesta;
        }
        public ObtenerDocumentosDestinoResponse ObtenerDocumentosDestino()
        {
            ObtenerDocumentosDestinoResponse respuesta = new ObtenerDocumentosDestinoResponse();
            try
            {
                var encontrados = ConsultaDocumentoDestinoVM(dd => true).ToList();
                if (encontrados != null && encontrados.Count > 0)
                    respuesta.Resultado.DocumentosDestino = encontrados;
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrio un error al obtener la informacion.";
            }
            return respuesta;
        }

        // Para results de consultas
        private IQueryable<DocumentoDestinoVM> ConsultaDocumentoDestinoVM(Expression<Func<DocumentoDestinoVM, bool>> predicado)
        {
            var query = (from dd in con.Gestiones_DocumentoDestino
                         join dn in con.Gestiones_TB_CATDestinoNotificacion on dd.IdDestinoNotificacion equals dn.IdDestinoNotificacion
                         join n in con.Gestiones_TB_CATNotificaciones on dd.IdNotificacion equals n.IdNotificacion
                         select new DocumentoDestinoVM
                         {
                             IdDocumentoDestino = dd.IdDocumentoDestino,
                             Descripcion = n.Notificacion + " " + dn.DestinoNotificacion,
                             IdDestinoNotificacion = dd.IdDestinoNotificacion,
                             IdNotificacion = dd.IdNotificacion
                         }).Where(predicado);
            return query;
        }

        //Para transacciones a POCOs
        private IQueryable<DocumentoDestino> ConsultaDocumentoDestino(Expression<Func<DocumentoDestino, bool>> predicado)
        {
            var query = (from dd in con.Gestiones_DocumentoDestino
                         select dd).Where(predicado);
            return query;
        }
    }
}
