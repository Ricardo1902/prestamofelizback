#pragma warning disable 140822
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Gestiones;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: AsignarCuentasTMPDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla AsignarCuentasTMP
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-22 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Gestiones
{
    public class AsignarCuentasTMPDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public AsignarCuentasTMPDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarAsignarCuentasTMP(AsignarCuentasTMP entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.TB_SubirCuentasMasiva"))
				{
					//Parametros
					db.AddInParameter(com, "@IdCuenta", DbType.Int32, entidad.IdCuenta);
                    db.AddInParameter(com, "@IdGestor", DbType.Int32, entidad.IdGestor);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, entidad.IdUsuario);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public AsignarCuentasTMP ObtenerAsignarCuentasTMP()
        {
			AsignarCuentasTMP resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new AsignarCuentasTMP();
							//Lectura de los datos del ResultSet
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarAsignarCuentasTMP()
        {
        }

        public void EliminarAsignarCuentasTMP()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}