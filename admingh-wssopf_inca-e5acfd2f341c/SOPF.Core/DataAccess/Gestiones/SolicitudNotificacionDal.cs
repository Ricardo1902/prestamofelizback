﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Model.Response.Gestiones;
using SOPF.Core.Model.Request.Gestiones;

namespace SOPF.Core.DataAccess.Gestiones
{
    public class SolicitudNotificacionDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public SolicitudNotificacionDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region "CRUD Methods"
        public DatosAvisoCobranzaResponse DatosAvisoCobranza(DatosAvisoCobranzaRequest peticion)
        {
            DatosAvisoCobranzaResponse objReturn = new DatosAvisoCobranzaResponse();
            DataSet ds = new DataSet();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("gestiones.SP_DatosNotificacionCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, "CON_AVISO_COBRANZA");
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, peticion.IdUsuario);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, peticion.IdSolicitud);
                    db.AddInParameter(com, "@IdDestino", DbType.Int32, peticion.IdDestino);
                    db.AddInParameter(com, "@Actualizar", DbType.Boolean, peticion.Actualizar);
                    db.AddInParameter(com, "@Correo", DbType.Boolean, peticion.Correo);
                    db.AddInParameter(com, "@Courier", DbType.Boolean, peticion.Courier);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                objReturn.Resultado.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro").GetValueOrDefault();
                                objReturn.Resultado.NombreCliente = Utils.GetString(reader, "NombreCliente");
                                objReturn.Resultado.Direccion = Utils.GetString(reader, "Direccion");
                                objReturn.Resultado.ReferenciaDomicilio = Utils.GetString(reader, "ReferenciaDomicilio");
                                objReturn.Resultado.TotalDeuda = Utils.GetDecimal(reader, "TotalDeuda");
                                objReturn.Resultado.CuotasVencidas = Utils.GetInt(reader, "CuotasVencidas");
                                objReturn.Resultado.IdCliente = Utils.GetInt(reader, "IdCliente");
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                }
            }
            catch (SqlException ex)
            {
                objReturn.Error = true;
                objReturn.MensajeOperacion= ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public DatosAvisoPagoResponse DatosAvisoPago(DatosAvisoPagoRequest peticion)
        {
            DatosAvisoPagoResponse objReturn = new DatosAvisoPagoResponse();
            DataSet ds = new DataSet();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("gestiones.SP_DatosNotificacionCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, "CON_AVISO_PAGO");
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, peticion.IdUsuario);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, peticion.IdSolicitud);
                    db.AddInParameter(com, "@IdDestino", DbType.Int32, peticion.IdDestino);
                    db.AddInParameter(com, "@Actualizar", DbType.Boolean, peticion.Actualizar);
                    db.AddInParameter(com, "@Correo", DbType.Boolean, peticion.Correo);
                    db.AddInParameter(com, "@Courier", DbType.Boolean, peticion.Courier);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                objReturn.Resultado.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro").GetValueOrDefault();
                                objReturn.Resultado.NombreCliente = Utils.GetString(reader, "NombreCliente");
                                objReturn.Resultado.Direccion = Utils.GetString(reader, "Direccion");
                                objReturn.Resultado.ReferenciaDomicilio = Utils.GetString(reader, "ReferenciaDomicilio");
                                objReturn.Resultado.TotalDeuda = Utils.GetDecimal(reader, "TotalDeuda");
                                objReturn.Resultado.CuotasVencidas = Utils.GetInt(reader, "CuotasVencidas");
                                objReturn.Resultado.IdCliente = Utils.GetInt(reader, "IdCliente");
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                }
            }
            catch (SqlException ex)
            {
                objReturn.Error = true;
                objReturn.MensajeOperacion = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public DatosNotificacionPrejudicialResponse DatosNotificacionPrejudicial(DatosNotificacionPrejudicialRequest peticion)
        {
            DatosNotificacionPrejudicialResponse objReturn = new DatosNotificacionPrejudicialResponse();
            DataSet ds = new DataSet();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("gestiones.SP_DatosNotificacionCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, "CON_NOTIFICACION_PREJUDICIAL");
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, peticion.IdUsuario);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, peticion.IdSolicitud);
                    db.AddInParameter(com, "@IdDestino", DbType.Int32, peticion.IdDestino);
                    db.AddInParameter(com, "@Actualizar", DbType.Boolean, peticion.Actualizar);
                    db.AddInParameter(com, "@Correo", DbType.Boolean, peticion.Correo);
                    db.AddInParameter(com, "@Courier", DbType.Boolean, peticion.Courier);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                objReturn.Resultado.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro").GetValueOrDefault();
                                objReturn.Resultado.NombreCliente = Utils.GetString(reader, "NombreCliente");
                                objReturn.Resultado.Direccion = Utils.GetString(reader, "Direccion");
                                objReturn.Resultado.ReferenciaDomicilio = Utils.GetString(reader, "ReferenciaDomicilio");
                                objReturn.Resultado.TotalDeuda = Utils.GetDecimal(reader, "TotalDeuda");
                                objReturn.Resultado.CuotasVencidas = Utils.GetInt(reader, "CuotasVencidas");
                                objReturn.Resultado.IdCliente = Utils.GetInt(reader, "IdCliente");
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                }
            }
            catch (SqlException ex)
            {
                objReturn.Error = true;
                objReturn.MensajeOperacion = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public DatosUltimaNotificacionPrejudicialResponse DatosUltimaNotificacionPrejudicial(DatosUltimaNotificacionPrejudicialRequest peticion)
        {
            DatosUltimaNotificacionPrejudicialResponse objReturn = new DatosUltimaNotificacionPrejudicialResponse();
            DataSet ds = new DataSet();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("gestiones.SP_DatosNotificacionCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, "CON_ULTIMA_NOTIFICACION_PREJUDICIAL");
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, peticion.IdUsuario);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, peticion.IdSolicitud);
                    db.AddInParameter(com, "@IdDestino", DbType.Int32, peticion.IdDestino);
                    db.AddInParameter(com, "@Actualizar", DbType.Boolean, peticion.Actualizar);
                    db.AddInParameter(com, "@Correo", DbType.Boolean, peticion.Correo);
                    db.AddInParameter(com, "@Courier", DbType.Boolean, peticion.Courier);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                objReturn.Resultado.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro").GetValueOrDefault();
                                objReturn.Resultado.NombreCliente = Utils.GetString(reader, "NombreCliente");
                                objReturn.Resultado.Direccion = Utils.GetString(reader, "Direccion");
                                objReturn.Resultado.ReferenciaDomicilio = Utils.GetString(reader, "ReferenciaDomicilio");
                                objReturn.Resultado.TotalDeuda = Utils.GetDecimal(reader, "TotalDeuda");
                                objReturn.Resultado.CuotasVencidas = Utils.GetInt(reader, "CuotasVencidas");
                                objReturn.Resultado.IdCliente = Utils.GetInt(reader, "IdCliente");
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                }
            }
            catch (SqlException ex)
            {
                objReturn.Error = true;
                objReturn.MensajeOperacion = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public DatosCitacionPrejudicialResponse DatosCitacionPrejudicial(DatosCitacionPrejudicialRequest peticion)
        {
            DatosCitacionPrejudicialResponse objReturn = new DatosCitacionPrejudicialResponse();
            DataSet ds = new DataSet();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("gestiones.SP_DatosNotificacionCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, "CON_CITACION_PREJUDICIAL");
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, peticion.IdUsuario);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, peticion.IdSolicitud);
                    db.AddInParameter(com, "@IdDestino", DbType.Int32, peticion.IdDestino);
                    db.AddInParameter(com, "@FechaHoraCitacion", DbType.DateTime, peticion.FechaHoraCitacion);
                    db.AddInParameter(com, "@Actualizar", DbType.Boolean, peticion.Actualizar);
                    db.AddInParameter(com, "@Correo", DbType.Boolean, peticion.Correo);
                    db.AddInParameter(com, "@Courier", DbType.Boolean, peticion.Courier);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                objReturn.Resultado.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro").GetValueOrDefault();
                                objReturn.Resultado.NombreCliente = Utils.GetString(reader, "NombreCliente");
                                objReturn.Resultado.Direccion = Utils.GetString(reader, "Direccion");
                                objReturn.Resultado.ReferenciaDomicilio = Utils.GetString(reader, "ReferenciaDomicilio");
                                objReturn.Resultado.TotalDeuda = Utils.GetDecimal(reader, "TotalDeuda");
                                objReturn.Resultado.CuotasVencidas = Utils.GetInt(reader, "CuotasVencidas");
                                objReturn.Resultado.IdCliente = Utils.GetInt(reader, "IdCliente");
                                objReturn.Resultado.FechaHoraCitacion = Utils.GetDateTime(reader, "FechaHoraCitacion").GetValueOrDefault();
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                }
            }
            catch (SqlException ex)
            {
                objReturn.Error = true;
                objReturn.MensajeOperacion = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public DatosPromocionReduccionMoraResponse DatosPromocionReduccionMora(DatosPromocionReduccionMoraRequest peticion)
        {
            DatosPromocionReduccionMoraResponse objReturn = new DatosPromocionReduccionMoraResponse();
            DataSet ds = new DataSet();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("gestiones.SP_DatosNotificacionCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, "CON_PROMOCION_REDUCCION_MORA");
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, peticion.IdUsuario);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, peticion.IdSolicitud);
                    db.AddInParameter(com, "@IdDestino", DbType.Int32, peticion.IdDestino);
                    db.AddInParameter(com, "@DescuentoDeuda", DbType.Decimal, peticion.DescuentoDeuda);
                    db.AddInParameter(com, "@FechaVigencia", DbType.DateTime, peticion.FechaVigencia);
                    db.AddInParameter(com, "@Actualizar", DbType.Boolean, peticion.Actualizar);
                    db.AddInParameter(com, "@Correo", DbType.Boolean, peticion.Correo);
                    db.AddInParameter(com, "@Courier", DbType.Boolean, peticion.Courier);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                objReturn.Resultado.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro").GetValueOrDefault();
                                objReturn.Resultado.NombreCliente = Utils.GetString(reader, "NombreCliente");
                                objReturn.Resultado.Direccion = Utils.GetString(reader, "Direccion");
                                objReturn.Resultado.ReferenciaDomicilio = Utils.GetString(reader, "ReferenciaDomicilio");
                                objReturn.Resultado.TotalDeuda = Utils.GetDecimal(reader, "TotalDeuda");
                                objReturn.Resultado.CuotasVencidas = Utils.GetInt(reader, "CuotasVencidas");
                                objReturn.Resultado.IdCliente = Utils.GetInt(reader, "IdCliente");
                                objReturn.Resultado.DescuentoDeuda = Utils.GetDecimal(reader, "DescuentoDeuda");
                                objReturn.Resultado.FechaVigencia = Utils.GetDateTime(reader, "FechaVigencia").GetValueOrDefault();
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                }
            }
            catch (SqlException ex)
            {
                objReturn.Error = true;
                objReturn.MensajeOperacion = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }
        #endregion
    }
}
