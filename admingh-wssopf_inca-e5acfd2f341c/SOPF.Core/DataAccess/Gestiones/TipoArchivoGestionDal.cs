﻿using SOPF.Core.Entities;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SOPF.Core.DataAccess.Gestiones
{
    public class TipoArchivoGestionDal
    {
        private CreditOrigContext con { get; set; }
        public TipoArchivoGestionDal(CreditOrigContext con)
        {
            this.con = con;
        }
        public TipoArchivoGestionVM ObtenerTipoArchivoGestionPorId(int idTipoArchivoGestion)
        {
            TipoArchivoGestionVM tipoGestion = null;
            try
            {
                //using (con)
                //{
                var encontrado = ConsultaTipoArchivoGestionVM(tag => tag.IdTipoArchivoGestion == idTipoArchivoGestion).FirstOrDefault();
                if (encontrado != null)
                    tipoGestion = encontrado;
                //}
            }
            catch (Exception)
            {
            }
            return tipoGestion;
        }
        public TipoArchivoGestionVM ObtenerTipoArchivoGestionPorMime(string tipoMime)
        {
            TipoArchivoGestionVM tipoGestion = null;
            try
            {
                //using (con)
                //{
                var encontrado = ConsultaTipoArchivoGestionVM(tag => tag.TipoMime == tipoMime).FirstOrDefault();
                if (encontrado != null)
                    tipoGestion = encontrado;
                //}
            }
            catch (Exception)
            {
            }
            return tipoGestion;
        }
        public List<TipoArchivoGestionVM> ObtenerTiposArchivoGestion()
        {
            List<TipoArchivoGestionVM> tipos = new List<TipoArchivoGestionVM>();
            try
            {
                //using (con)
                //{
                var encontrados = ConsultaTipoArchivoGestionVM(tag => true).ToList();

                if (encontrados != null && encontrados.Count > 0)
                {
                    tipos = encontrados;
                }
                //}
            }
            catch (Exception)
            {
            }

            return tipos;
        }
        public Resultado<bool> InsertarTipoArchivoGestion(TipoArchivoGestion tipoArchivoGestion)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            if (tipoArchivoGestion == null) throw new Exception("La petición es inválida");
            using (var tran = con.Database.BeginTransaction())
            {
                try
                {
                    //using (con)
                    //{
                    con.Gestiones_TipoArchivoGestion.Add(tipoArchivoGestion);
                    con.SaveChanges();

                    respuesta.Codigo = 1;
                    respuesta.Mensaje = "Se insertó el registro con éxito";
                    respuesta.ResultObject = true;

                    tran.Commit();
                    //}
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    respuesta.Codigo = 0;
                    respuesta.Mensaje = ex.Message;
                    respuesta.ResultObject = false;
                }
            }
            return respuesta;
        }
        public Resultado<bool> ActualizarTipoArchivoGestion(TipoArchivoGestion tipoArchivoGestion)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            if (tipoArchivoGestion == null) throw new Exception("La petición es inválida");
            using (var tran = con.Database.BeginTransaction())
            {
                try
                {
                    TipoArchivoGestion registro = ConsultaTipoArchivoGestion(tag => tag.IdTipoArchivoGestion == tipoArchivoGestion.IdTipoArchivoGestion).FirstOrDefault();
                    if (registro != null)
                    {
                        if (registro.CompareTo(tipoArchivoGestion) > 0)
                        {
                            registro.TipoArchivo = tipoArchivoGestion.TipoArchivo;
                            registro.Extension = tipoArchivoGestion.Extension;
                            registro.TipoMime = tipoArchivoGestion.TipoMime;
                            registro.Icono = tipoArchivoGestion.Icono;

                            con.SaveChanges();
                        }

                        respuesta.Codigo = 1;
                        respuesta.Mensaje = "Se actualizó el registro con éxito";
                        respuesta.ResultObject = true;

                        tran.Commit();
                    }
                    //}
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    respuesta.Codigo = 0;
                    respuesta.Mensaje = ex.Message;
                    respuesta.ResultObject = false;
                }
            }
            return respuesta;
        }
        public IQueryable<TipoArchivoGestion> ConsultaTipoArchivoGestion(Expression<Func<TipoArchivoGestion, bool>> predicado)
        {
            var query = (from tag in con.Gestiones_TipoArchivoGestion
                         select tag).Where(predicado);
            return query;
        }
        public IQueryable<TipoArchivoGestionVM> ConsultaTipoArchivoGestionVM(Expression<Func<TipoArchivoGestionVM, bool>> predicado)
        {
            var query = (from tag in con.Gestiones_TipoArchivoGestion
                         select new TipoArchivoGestionVM
                         {
                             IdTipoArchivoGestion = tag.IdTipoArchivoGestion,
                             TipoArchivo = tag.TipoArchivo,
                             Extension = tag.Extension,
                             TipoMime = tag.TipoMime,
                             Icono = tag.Icono
                         }).Where(predicado);
            return query;
        }
    }
}
