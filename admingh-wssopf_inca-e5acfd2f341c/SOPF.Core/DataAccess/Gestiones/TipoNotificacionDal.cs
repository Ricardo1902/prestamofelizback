﻿using SOPF.Core.Entities;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SOPF.Core.DataAccess.Gestiones
{
    public class TipoNotificacionDal
    {
        private CreditOrigContext con;
        public TipoNotificacionDal(CreditOrigContext con)
        {
            this.con = con;
        }
        public List<TipoNotificacionVM> ObtenerTiposNotificacion()
        {
            List<TipoNotificacionVM> tipos = new List<TipoNotificacionVM>();
            try
            {
                var encontrados = ConsultaTipoNotificacionVM(tg => true).ToList();
                if (encontrados != null && encontrados.Count > 0)
                    tipos = encontrados;
            }
            catch (Exception)
            {
            }
            return tipos;
        }
        public IQueryable<TipoNotificacionVM> ConsultaTipoNotificacionVM(Expression<Func<TipoNotificacionVM, bool>> predicado)
        {
            var query = (from tn in con.Gestiones_TB_CATTipoNotificaciones
                         select new TipoNotificacionVM
                         {
                             IdTipoNotificacion = tn.IdTipoNotificacion,
                             Nombre = tn.TipoNotificacion,
                             Descripcion = tn.Descripcion,
                             IdUsuarioRegistro = tn.UsuarioRegistroId,
                             FechaRegistro = tn.FechaRegistro,
                             Actual = tn.Actual
                         }).Where(predicado);
            return query;
        }
    }
}
