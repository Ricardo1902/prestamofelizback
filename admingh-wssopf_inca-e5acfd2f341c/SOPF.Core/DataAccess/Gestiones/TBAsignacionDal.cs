#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Gestiones;

# region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBAsignacionDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBAsignacion
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Gestiones
{
    public class TBAsignacionDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBAsignacionDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBAsignacion(TBAsignacion entidad, int UsuarioIdAsigna)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.AsignarCuentas"))
				{
					//Parametros
                    db.AddInParameter(com, "@IdGestor", DbType.Int32, entidad.IdGestor);
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, entidad.IdCuenta);
                    db.AddInParameter(com, "@UsuarioIdAsigno", DbType.Int32, UsuarioIdAsigna);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<DataResult.Usp_ObtenerAsignacion> ObtenerTBAsignacion(int idGestor,int accion, int idCuenta, string cliente, int idSucursal, string DNI, string convenio)
        {
            List<DataResult.Usp_ObtenerAsignacion> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_SelAsignacionCON"))
				{
					//Parametros
                    db.AddInParameter(com, "@IdGestor", DbType.Int32, idGestor);
                    db.AddInParameter(com, "@Accion ", DbType.Int32, accion);
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, idCuenta);
                    db.AddInParameter(com, "@Cliente", DbType.String, cliente);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, idSucursal);
                    db.AddInParameter(com, "@DNI", DbType.String, DNI);
                    db.AddInParameter(com, "@Convenio", DbType.String, convenio);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
                            resultado = new List<DataResult.Usp_ObtenerAsignacion>();
							//Le    ctura de los datos del ResultSet
                            while (reader.Read())
                            {
                                DataResult.Usp_ObtenerAsignacion asignacion = new DataResult.Usp_ObtenerAsignacion();
                                if (!reader.IsDBNull(0)) asignacion.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) asignacion.Cliente = reader[1].ToString();
                                if (!reader.IsDBNull(2)) asignacion.Ciudad = reader[2].ToString();
                                if (!reader.IsDBNull(3)) asignacion.estado = reader[3].ToString();
                                if (!reader.IsDBNull(4)) asignacion.Sucursal = reader[4].ToString();
                                if (!reader.IsDBNull(5)) asignacion.FechaAsignacion = Convert.ToDateTime(reader[5]);
                                if (!reader.IsDBNull(6)) asignacion.Usuario = reader[6].ToString();
                                if (!reader.IsDBNull(7)) asignacion.Gestor = reader[7].ToString();
                                if (!reader.IsDBNull(8)) asignacion.IdSolicitud = Convert.ToDecimal(reader[8].ToString());
                                if (!reader.IsDBNull(8)) asignacion.Tipo = reader[9].ToString();
                                resultado.Add(asignacion);
                            }
						}
                        reader.Close();
						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarTBAsignacion(TBAsignacion entidad)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.DesasignarCuenta"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdGestor", DbType.Int32, entidad.IdGestor);
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, entidad.IdCuenta);
                    
                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarTBAsignacion()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}