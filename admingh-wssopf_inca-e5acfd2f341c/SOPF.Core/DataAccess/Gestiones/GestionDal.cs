#pragma warning disable 140825
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model.Response.Cobranza;
using SOPF.Core.Model.Response.Gestiones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: GestionDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla Gestion
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-25 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Gestiones
{
    public class GestionDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public GestionDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods
        public int InsertarGestion(Gestion entidad)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                int resultado = 0;
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_GestionALT"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, entidad.IdCuenta);
                    db.AddInParameter(com, "@IdGestor", DbType.Int32, entidad.IdGestor);
                    db.AddInParameter(com, "@Comentario", DbType.String, entidad.Comentario);
                    db.AddInParameter(com, "@IdResultado", DbType.Int32, entidad.IdResultadoGestion);
                    db.AddInParameter(com, "@TipoGestion", DbType.String, entidad.TipoGestion);
                    db.AddInParameter(com, "@idCNT", DbType.Int32, entidad.idCNT);
                    db.AddInParameter(com, "@IdCampana", DbType.Int32, entidad.idCampana);
                    db.AddInParameter(com, "@IdTipoGestion", DbType.Int32, entidad.IdTipoGestion);
                    db.AddInParameter(com, "@latitud", DbType.Double, entidad.latitud);
                    db.AddInParameter(com, "@longitud", DbType.Double, entidad.longitud);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) resultado = Convert.ToInt32(reader[0]);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SOPF.Core.Entities.Gestiones.DataResult.Usp_ObtenerGestion> ObtenerGestion(int Accion, int idCuenta, int idGestion, DateTime FecIni, DateTime FecFin, int idGestor)
        {
            List<SOPF.Core.Entities.Gestiones.DataResult.Usp_ObtenerGestion> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_GestionesCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, idCuenta);
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdGestion", DbType.Int32, idGestion);
                    db.AddInParameter(com, "@Fec_Inicial", DbType.DateTime, FecIni);
                    db.AddInParameter(com, "@Fec_Final", DbType.DateTime, FecFin);
                    db.AddInParameter(com, "@IdGestor", DbType.Int32, idGestor);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<SOPF.Core.Entities.Gestiones.DataResult.Usp_ObtenerGestion>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                SOPF.Core.Entities.Gestiones.DataResult.Usp_ObtenerGestion gestion = new SOPF.Core.Entities.Gestiones.DataResult.Usp_ObtenerGestion();
                                if (!reader.IsDBNull(1)) gestion.FchGestion = Convert.ToDateTime(reader[1]);
                                if (!reader.IsDBNull(4)) gestion.Nombre = reader[4].ToString();
                                if (!reader.IsDBNull(2)) gestion.Comentario = reader[2].ToString();
                                if (!reader.IsDBNull(3)) gestion.Descripcion = reader[3].ToString();
                                if (!reader.IsDBNull(5)) gestion.IdGestion = Convert.ToInt32(reader[5]);
                                if (!reader.IsDBNull(0)) gestion.IdGestor = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(6)) gestion.CNT = reader[6].ToString();
                                if (!reader.IsDBNull(7)) gestion.Campana = reader[7].ToString();
                                if (!reader.IsDBNull(8)) gestion.IdTipoGestion = Convert.ToInt32(reader[8]);
                                resultado.Add(gestion);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public ObtenerGestionPordIdGestionResponse ObtenerGestionPordIdGestion(ObtenerGestionRequest peticion)
        {
            ObtenerGestionPordIdGestionResponse gestiones = new ObtenerGestionPordIdGestionResponse();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_GestionesCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, peticion.Accion);
                    db.AddInParameter(com, "@IdGestion", DbType.Int32, peticion.IdGestion);
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, 0);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        var resulGestion = Utils.DataReaderAEntidad<Sel_GestionesResult>(reader);
                        if (resulGestion != null && resulGestion.Count > 0)
                        {
                            gestiones.Resultado.Gestion = resulGestion[0];
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                //TODO: Implementar log de error.
                //Utils.EscribirLog("ObtenerGestion (peticion) " + JsonConvert.SerializeObject(peticion) + ". " + ex.Message, ex.StackTrace);
            }
            return gestiones;
        }

        public int ActualizarGestion(Gestion entidad)
        {

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                int resultado = 0;
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_GestionACT"))
                {

                    //Parametros
                    db.AddInParameter(com, "@IdGestion", DbType.Int32, entidad.IdGestion);
                    db.AddInParameter(com, "@IdGestor", DbType.Int32, entidad.IdGestor);
                    db.AddInParameter(com, "@Comentario", DbType.String, entidad.Comentario);
                    db.AddInParameter(com, "@IdResultado", DbType.Int32, entidad.IdResultadoGestion);




                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {

                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {

                                if (!reader.IsDBNull(0)) resultado = Convert.ToInt32(reader[0]);


                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }



                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarGestion()
        {
        }
        #endregion

        #region MISC Methods
        public List<TBCreditos> ObtenerOtrasCuentas(int idCuenta)
        {
            List<TBCreditos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_OtrasCuentasCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, idCuenta);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCreditos>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCreditos credito = new TBCreditos();
                                if (!reader.IsDBNull(0)) credito.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) credito.Capital = Convert.ToDecimal(reader[1].ToString());
                                if (!reader.IsDBNull(2)) credito.FchCredito = Convert.ToDateTime(reader[2].ToString());
                                if (!reader.IsDBNull(3)) credito.Interes = Convert.ToDecimal(reader[3].ToString());
                                if (!reader.IsDBNull(4)) credito.IvaIntere = Convert.ToDecimal(reader[4].ToString());
                                if (!reader.IsDBNull(5)) credito.Erogacion = Convert.ToDecimal(reader[5].ToString());
                                if (!reader.IsDBNull(6)) credito.SdoVencido = Convert.ToDecimal(reader[6].ToString());
                                if (!reader.IsDBNull(7)) credito.SdoPorApl = Convert.ToDecimal(reader[7].ToString());
                                if (!reader.IsDBNull(8)) credito.IdSolicitud = Convert.ToDecimal(reader[8].ToString());
                                if (!reader.IsDBNull(9)) credito.Estatus = reader[9].ToString();
                                if (!reader.IsDBNull(10)) credito.Banco = reader[10].ToString();
                                if (!reader.IsDBNull(11)) credito.Clabe = reader[11].ToString();
                                resultado.Add(credito);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<DataResult.RepGestiones> ReporteGestiones(int Accion, DateTime Fec_Inicial, DateTime Fec_Final, int IdGestor)
        {
            List<DataResult.RepGestiones> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.RepGestiones"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@Fec_Inicial", DbType.DateTime, Fec_Inicial);
                    db.AddInParameter(com, "@Fec_Final", DbType.DateTime, Fec_Final);
                    db.AddInParameter(com, "@IdGestor", DbType.Int32, IdGestor);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<DataResult.RepGestiones>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                DataResult.RepGestiones gestiones = new DataResult.RepGestiones();
                                if (!reader.IsDBNull(0)) gestiones.IdGestion = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) gestiones.Idcuenta = Convert.ToDecimal(reader[1]);
                                if (!reader.IsDBNull(2)) gestiones.FchGestion = Convert.ToDateTime(reader[2]);
                                if (!reader.IsDBNull(3)) gestiones.Gestor = reader[3].ToString();
                                if (!reader.IsDBNull(4)) gestiones.Cliente = reader[4].ToString();
                                if (!reader.IsDBNull(5)) gestiones.Comentario = reader[5].ToString();
                                if (!reader.IsDBNull(6)) gestiones.Resultado = reader[6].ToString();
                                if (!reader.IsDBNull(7)) gestiones.Campana = reader[7].ToString();
                                if (!reader.IsDBNull(8)) gestiones.SaldoTotal = Convert.ToDecimal(reader[8]);
                                if (!reader.IsDBNull(9)) gestiones.Saldovencido = Convert.ToDecimal(reader[9]);
                                if (!reader.IsDBNull(10)) gestiones.Producto = reader[10].ToString();
                                if (!reader.IsDBNull(11)) gestiones.Convenio = reader[11].ToString();
                                if (!reader.IsDBNull(12)) gestiones.FchUltPago = Convert.ToDateTime(reader[12]);
                                if (!reader.IsDBNull(13)) gestiones.Sucursal = reader[13].ToString();
                                if (!reader.IsDBNull(14)) gestiones.Descripcion = reader[14].ToString();
                                if (!reader.IsDBNull(15)) gestiones.FlagMora = reader[15].ToString();
                                if (!reader.IsDBNull(16)) gestiones.GestorTelAsig = reader[16].ToString();
                                if (!reader.IsDBNull(17)) gestiones.idTelAsig = Convert.ToInt32(reader[17].ToString());
                                if (!reader.IsDBNull(18)) gestiones.GestorCamAsig = reader[18].ToString();
                                if (!reader.IsDBNull(19)) gestiones.idCamAsig = Convert.ToInt32(reader[19].ToString());
                                if (!reader.IsDBNull(20)) gestiones.GestorExtraAsig = reader[20].ToString();
                                if (!reader.IsDBNull(21)) gestiones.idExtraAsig = Convert.ToInt32(reader[21].ToString());
                                if (!reader.IsDBNull(22)) gestiones.GestorJudAsig = reader[22].ToString();
                                if (!reader.IsDBNull(23)) gestiones.idJudAsig = Convert.ToInt32(reader[23].ToString());
                                if (!reader.IsDBNull(24)) gestiones.AdminisAsig = reader[24].ToString();
                                if (!reader.IsDBNull(25)) gestiones.idAdminAsig = Convert.ToInt32(reader[25].ToString());
                                if (!reader.IsDBNull(26)) gestiones.FchPromesa = Convert.ToDateTime(reader[26]);
                                if (!reader.IsDBNull(27)) gestiones.Montopromesa = Convert.ToDecimal(reader[27]);
                                if (!reader.IsDBNull(28)) gestiones.Tipocobro = reader[28].ToString();
                                if (!reader.IsDBNull(29)) gestiones.idGestor = Convert.ToInt32(reader[29].ToString());

                                resultado.Add(gestiones);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        #endregion

        #region Private Methods
        #endregion

        #region GestionesNuevo
        public ObtenerDatosClienteGestionResponse ObtenerDatosClienteGestion(ObtenerDatosClienteGestionRequest peticion)
        {
            ObtenerDatosClienteGestionResponse response = new ObtenerDatosClienteGestionResponse();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_GestionClienteCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, peticion.Accion);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, peticion.IdSolicitud);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        var datos = Utils.DataReaderAEntidad<Sel_ObtenerDatosClienteGestionResult>(reader);
                        if (datos != null && datos.Count > 0)
                        {
                            response.Resultado.Datos = datos[0];
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                //TODO: Implementar log de error.
                //Utils.EscribirLog("ObtenerDatosClienteGestion (peticion) " + JsonConvert.SerializeObject(peticion) + ". " + ex.Message, ex.StackTrace);
            }
            return response;
        }
        public ObtenerCarteraAsignadaResponse ObtenerCarteraAsignada(ObtenerCarteraAsignadaRequest peticion)
        {
            ObtenerCarteraAsignadaResponse respuesta = new ObtenerCarteraAsignadaResponse();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_CarteraAsignadaCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.Int32, peticion.Accion);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, peticion.IdUsuario);
                    db.AddInParameter(com, "@IdGestor", DbType.Int32, peticion.IdGestor);
                    if (!string.IsNullOrWhiteSpace(peticion.Busqueda)) db.AddInParameter(com, "@Busqueda", DbType.String, peticion.Busqueda);
                    if (peticion.GrupoAsignaciones != null) db.AddInParameter(com, "@GrupoAsignaciones", DbType.Int32, peticion.GrupoAsignaciones);
                    if (peticion.Pagina > 0) db.AddInParameter(com, "@Pagina", DbType.Int32, peticion.Pagina);
                    if (peticion.RegistrosPagina > 0) db.AddInParameter(com, "@RegistrosPagina", DbType.Int32, peticion.RegistrosPagina);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            var entidadCartera = Utils.DataReaderAEntidad<Sel_ObtenerCarteraAsignadaResult>(reader);
                            if (entidadCartera != null && entidadCartera.Count > 0)
                            {
                                respuesta.Resultado.Cartera = entidadCartera;
                            }

                            reader.NextResult();

                            var entidadTotales = Utils.DataReaderAEntidad<ObtenerCarteraAsignadaTotales>(reader);
                            if (entidadTotales != null && entidadTotales.Count > 0)
                            {
                                respuesta.Resultado.Totales = entidadTotales[0];
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                //TODO: Implementar log de error
            }
            return respuesta;
        }
        public ObtenerPagosGestionesResponse ObtenerPagosGestiones(ObtenerPagosGestionesRequest peticion)
        {
            ObtenerPagosGestionesResponse resultado = new ObtenerPagosGestionesResponse() { };
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_PagosRealizadosCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, peticion.Accion);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, peticion.IdSolicitud);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            List<PagoGestionesVM> pagos = new List<PagoGestionesVM>();
                            PagoGestionesVM pago;
                            while (reader.Read())
                            {
                                pago = new PagoGestionesVM();
                                pago.Cuota = Utils.GetInt(reader, "Cuota");
                                pago.Fecha.Value = Utils.GetDateTime(reader, "Fecha");
                                pago.CapitalInicial = Utils.GetDecimal(reader, "CapitalInicial");
                                pago.Capital = Utils.GetDecimal(reader, "Capital");
                                pago.Interes = Utils.GetDecimal(reader, "Interes");
                                pago.IGV = Utils.GetDecimal(reader, "IGV");
                                pago.Seguro = Utils.GetDecimal(reader, "Seguro");
                                pago.GAT = Utils.GetDecimal(reader, "GAT");
                                pago.Estatus.IdEstatus = Utils.GetInt(reader, "IdEstatus");
                                pago.Estatus.EstatusClave = Utils.GetString(reader, "EstatusClave");
                                pago.Estatus.EstatusDesc = Utils.GetString(reader, "EstatusDesc");
                                pago.SaldoRecibo = Utils.GetDecimal(reader, "SaldoRecibo");
                                pago.TotalRecibo = Utils.GetDecimal(reader, "TotalRecibo");
                                pago.CanalPagoClave = Utils.GetString(reader, "CanalPagoClave");
                                pago.CanalPagoDesc = Utils.GetString(reader, "CanalPagoDesc");
                                pago.FechaPago.Value = Utils.GetDateTime(reader, "FechaPago");
                                pago.MontoPago = Utils.GetDecimal(reader, "MontoPago");
                                pagos.Add(pago);
                            }
                            resultado.TotalRegistros = pagos.Count;
                            resultado.Resultado.Pagos = pagos;
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                resultado.Error = true;
                resultado.MensajeErrorException = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        #endregion
    }
}