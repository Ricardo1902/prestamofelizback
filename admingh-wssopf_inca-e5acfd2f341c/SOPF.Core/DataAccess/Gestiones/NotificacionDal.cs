﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using SOPF.Core.Entities;
using SOPF.Core.Model.Gestiones;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Poco.Gestiones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SOPF.Core.DataAccess.Gestiones
{
    public class NotificacionDal
    {
        Database db;
        private CreditOrigContext con;
        public NotificacionDal(CreditOrigContext con)
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
            this.con = con;
        }

        public List<NotificacionVM> ObtenerNotificacionesPorTipo(int idTipoNotificacion)
        {
            List<NotificacionVM> notificaciones = null;
            try
            {
                if (idTipoNotificacion > 0)
                {
                    var encontrados = ConsultaNotificacionVM(n => n.IdTipoNotificacion == idTipoNotificacion).ToList();
                    if (encontrados != null && encontrados.Count > 0)
                        notificaciones = encontrados;
                }
            }
            catch (Exception)
            {
            }
            return notificaciones;
        }
        public SolicitudNotificacionVM ObtenerNotificacionActivaSolicitud(int idSolicitud, int idNotificacion, int idDestinoNotificacion)
        {
            SolicitudNotificacionVM notificacionSolicitud = null;
            try
            {
                var encontrado = ConsultaSolicitudNotificacionesVM(sn => sn.SolicitudId == idSolicitud && sn.NotificacionId == idNotificacion && sn.DestinoId == idDestinoNotificacion && sn.Actual == true).FirstOrDefault();
                if (encontrado != null)
                    notificacionSolicitud = encontrado;
            }
            catch (Exception ex)
            {
            }
            return notificacionSolicitud;
        }
        public Resultado<bool> ActualizarSolicitudNotificacion(ActualizarSolicitudNotificacionRequest peticion)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("gestiones.SP_SolicitudNotificacionesACT"))
                {
                    db.AddInParameter(com, "@IdSolicitudNotificacion", DbType.Int32, peticion.IdSolicitudNotificacion);
                    db.AddInParameter(com, "@IdUsuarioEscaneo", DbType.Int32, peticion.IdUsuarioEscaneo);
                    db.AddInParameter(com, "@IdDestino", DbType.Int32, peticion.IdDestino);
                    db.AddInParameter(com, "@IdSolNotTipoAccion", DbType.Int32, peticion.IdSolNotTipoAccion);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader == null)
                        {
                            throw new Exception("Ha ocurrido un error al actualziar la notificacion");
                        }
                        reader.Close();
                        reader.Dispose();

                        respuesta.Codigo = 1;
                        respuesta.Mensaje = "La actualización se realizó con éxito.";
                    }
                }
            }
            catch (SqlException ex)
            {
                respuesta.Codigo = 0;
                respuesta.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return respuesta;
        }

        private IQueryable<NotificacionVM> ConsultaNotificacionVM(Expression<Func<NotificacionVM, bool>> predicado)
        {
            var query = (from n in con.Gestiones_TB_CATNotificaciones
                         join tn in con.Gestiones_TB_CATTipoNotificaciones on n.TipoNotificacionId equals tn.IdTipoNotificacion
                         select new NotificacionVM
                         {
                             IdNotificacion = n.IdNotificacion,
                             IdTipoNotificacion = n.TipoNotificacionId,
                             TipoNotificacion = tn.TipoNotificacion,
                             DescripcionTipoNotificacion = tn.Descripcion,
                             Nombre = n.Notificacion,
                             Descripcion = n.Descripcion,
                             NombreArchivo = n.NombreArchivo,
                             Clave = n.Clave,
                             Orden = n.Orden,
                             IdUsuarioRegistro = n.UsuarioRegistroId,
                             FechaRegistro = n.FechaRegistro,
                             Actual = n.Actual
                         }).Where(predicado);

            return query;
        }
        private IQueryable<SolicitudNotificacionVM> ConsultaSolicitudNotificacionesVM(Expression<Func<SolicitudNotificacionVM, bool>> predicado)
        {
            var query = (from sn in con.Gestiones_SolicitudNotificaciones
                         select new SolicitudNotificacionVM
                         {
                             IdSolicitudNotificacion = sn.IdSolicitudNotificacion,
                             SolicitudId = (int)sn.SolicitudId,
                             NotificacionId = sn.NotificacionId,
                             DestinoId = sn.DestinoId,
                             UsuarioRegistroId = sn.UsuarioRegistroId,
                             FechaRegistro = sn.FechaRegistro,
                             UsuarioEscaneoId = sn.UsuarioEscaneoId,
                             FechaEscaneo = sn.FechaEscaneo,
                             Actual = sn.Actual
                         }).Where(predicado);

            return query;
        }
    }
}
