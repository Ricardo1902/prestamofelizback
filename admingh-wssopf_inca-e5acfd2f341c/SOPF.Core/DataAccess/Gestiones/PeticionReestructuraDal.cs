﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Solicitudes;
using SOPF.Core.Entities;
using System.Xml;
using SOPF.Core.Entities.Creditos;
using System.Data.SqlClient;
using SOPF.Core.Model.Request.Gestiones;
using SOPF.Core.Model;
using SOPF.Core.Entities.Gestiones;
using SOPF.Core.BusinessLogic.Gestiones;
using SOPF.Core.Poco.dbo;
using SOPF.Core.Poco.Gestiones;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using SOPF.Core.Model.Response.Gestiones;

namespace SOPF.Core.DataAccess.Gestiones
{
    public class PeticionReestructuraDal
    {
        #region Objetos Base de Datos
        Microsoft.Practices.EnterpriseLibrary.Data.Database db;
        #endregion

        #region Contructor
        public PeticionReestructuraDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods
        public List<PeticionReestructura.BusquedaClienteSolicitudPRRes> Clientes_Filtrar(string Accion, PeticionReestructura.BusquedaClienteSolicitudPRReq entity)
        {
            List<PeticionReestructura.BusquedaClienteSolicitudPRRes> resultado = new List<PeticionReestructura.BusquedaClienteSolicitudPRRes>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_PetResClientesCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, Accion);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, entity.IdSolicitud);
                    db.AddInParameter(com, "@NombreCliente", DbType.String, entity.NombreCliente);
                    db.AddInParameter(com, "@Bucket", DbType.Int32, entity.Bucket);
                    db.AddInParameter(com, "@RazonSocialEmpresa", DbType.String, entity.RazonSocialEmpresa);
                    db.AddInParameter(com, "@DependenciaEmpresa", DbType.String, entity.DependenciaEmpresa);
                    db.AddInParameter(com, "@UbicacionEmpresa", DbType.String, entity.UbicacionEmpresa);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                PeticionReestructura.BusquedaClienteSolicitudPRRes cliente = new PeticionReestructura.BusquedaClienteSolicitudPRRes();
                                cliente.IdSolicitud = Utils.GetInt(reader, "IdSolicitud");
                                cliente.NombreCliente = Utils.GetString(reader, "NombreCliente");
                                cliente.FechaNacimiento = Utils.GetString(reader, "FechaNacimiento");
                                cliente.SaldoVencido = Utils.GetDecimal(reader, "SaldoVencido");
                                cliente.DiasMora = Utils.GetInt(reader, "DiasMora");
                                cliente.Bucket = Utils.GetString(reader, "Bucket");
                                cliente.EstatusPeticion = Utils.GetString(reader, "EstatusPeticion");
                                cliente.IdPeticionReestructura = Utils.GetInt(reader, "IdPeticionReestructura");
                                resultado.Add(cliente);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public Resultado<bool> InsertarPeticionReestructura(string Accion, InsertarPeticionReestructuraRequest entidad)
        {
            Resultado<bool> resultado = new Resultado<bool>();
            try
            {
                bool exito = false;
                string mensaje = string.Empty;

                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_PeticionReestructuraPRO"))
                {
                    #region Parámetros
                    db.AddInParameter(com, "@Accion", DbType.String, "ALTA_PETICION_REESTRUCTURA_PRO");
                    db.AddInParameter(com, "@Solicitud_Id", DbType.Decimal, entidad.peticion.Solicitud_Id);
                    db.AddInParameter(com, "@TipoReestructura_Id", DbType.Int32, entidad.peticion.TipoReestructura_Id);
                    db.AddInParameter(com, "@MotivoPeticionReestructura_Id", DbType.Int32, entidad.peticion.MotivoPeticionReestructura_Id);
                    db.AddInParameter(com, "@Pagos", DbType.Int32, entidad.peticion.Pagos);
                    db.AddInParameter(com, "@CapturistaUsuario_Id", DbType.Int32, entidad.peticion.CapturistaUsuario_Id);
                    db.AddInParameter(com, "@fch_Credito", DbType.DateTime, entidad.peticion.FotoPeticionReestructura.fch_Credito);
                    db.AddInParameter(com, "@erogacion", DbType.Decimal, entidad.peticion.FotoPeticionReestructura.erogacion);
                    db.AddInParameter(com, "@NumeroDocumento", DbType.String, entidad.peticion.FotoPeticionReestructura.NumeroDocumento);
                    db.AddInParameter(com, "@Cliente", DbType.String, entidad.peticion.FotoPeticionReestructura.Cliente);
                    db.AddInParameter(com, "@Direccion", DbType.String, entidad.peticion.FotoPeticionReestructura.Direccion);
                    db.AddInParameter(com, "@capital", DbType.Decimal, entidad.peticion.FotoPeticionReestructura.capital);
                    db.AddInParameter(com, "@FechaPrimerPago", DbType.DateTime, entidad.peticion.FotoPeticionReestructura.FechaPrimerPago);
                    db.AddInParameter(com, "@TotalLiquidacion", DbType.Decimal, entidad.peticion.FotoPeticionReestructura.TotalLiquidacion);
                    db.AddInParameter(com, "@XmlFotoReciboPeticionReestructuras", DbType.Xml, new XmlTextReader(entidad.peticion.FotoPeticionReestructura.GetFotoReciboPeticionReestructurasXML(), XmlNodeType.Document, null));
                    db.AddInParameter(com, "@ComentarioCapturista", DbType.String, entidad.ComentarioCapturista);
                    db.AddOutParameter(com, "@Exito", DbType.Boolean, 2);
                    db.AddOutParameter(com, "@Resultado", DbType.Int32, Int32.MaxValue);
                    db.AddOutParameter(com, "@Mensaje", DbType.String, 1024);
                    #endregion

                    db.ExecuteScalar(com);

                    if (bool.TryParse(db.GetParameterValue(com, "@Exito").ToString(), out exito))
                    {
                        resultado.Codigo = (bool)db.GetParameterValue(com, "@Exito") ? 1 : 0;
                        resultado.CodigoStr = db.GetParameterValue(com, "@Resultado").ToString();
                        resultado.Mensaje = db.GetParameterValue(com, "@Mensaje").ToString();
                        resultado.ResultObject = exito;
                        if (exito)
                        {
                            string comentario = string.Empty;
                            comentario = $"Petición de Reestructura: se creó una petición de reestructura" +
                                $" con la solicitud {entidad.peticion.Solicitud_Id.ToString()}" +
                                $"; y el IdPeticion: {resultado.CodigoStr}.";
                            Respuesta respuestaGestion = InsertarGestionPeticionReestructura(entidad.peticion.CapturistaUsuario_Id, Convert.ToInt32(entidad.peticion.Solicitud_Id), comentario);
                            resultado.ResultObject = respuestaGestion.Error;
                            resultado.Codigo = respuestaGestion.Error ? 1 : 0;
                            if (respuestaGestion.Error)
                            {
                                resultado.CodigoStr = respuestaGestion.MensajeErrorException;
                                resultado.Mensaje = respuestaGestion.MensajeOperacion;
                            }
                        }
                    }
                    else
                    {
                        resultado.Codigo = 0;
                        resultado.CodigoStr = "0";
                        resultado.Mensaje = "No se pudo recuperar la información de la base de datos.";
                        resultado.ResultObject = false;
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                resultado.Codigo = 0;
                resultado.CodigoStr = "0";
                resultado.Mensaje = ex.Message != null ?
                    "Hubo un error inesperado en el servidor. Favor de contactar a sistemas. Mensaje:" + ex.Message
                    : "Hubo un error inesperado en el servidor. Favor de contactar a sistemas.";
                resultado.ResultObject = false;
                throw ex;
            }
            return resultado;
        }

        public Resultado<List<PeticionReestructura>> ObtenerPeticionReestructura(string Accion, PeticionReestructura entidad)
        {
            Resultado<List<PeticionReestructura>> resultado = new Resultado<List<PeticionReestructura>>();
            try
            {

                List<PeticionReestructura> resultObject = new List<PeticionReestructura>();

                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_PeticionReestructuraCON"))
                {
                    #region Parámetros
                    db.AddInParameter(com, "@Accion", DbType.String, Accion);
                    db.AddInParameter(com, "@IdPeticionReestructura", DbType.Int32, entidad.Id_PeticionReestructura);
                    #endregion

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                PeticionReestructura peticion = new PeticionReestructura();
                                switch (Accion)
                                {
                                    case "CONSULTAR_FOTO_PETICION_REESTRUCTURA_POR_ID":
                                        peticion.TipoReestructura_Id = Utils.GetInt(reader, "TipoReestructura_Id");
                                        peticion.MotivoPeticionReestructura_Id = Utils.GetInt(reader, "MotivoPeticionReestructura_Id");
                                        peticion.Pagos = Utils.GetInt(reader, "Pagos");
                                        peticion.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro").GetValueOrDefault();
                                        peticion.EstatusPeticionReestructuras_Id = Utils.GetInt(reader, "EstatusPeticionReestructuras_Id");
                                        peticion.Pagos = Utils.GetInt(reader, "Pagos");
                                        resultado.Mensaje = Utils.GetString(reader, "ComentarioAnalista");

                                        FotoPeticionReestructura fotopetres = new FotoPeticionReestructura();

                                        fotopetres.fch_Credito = Utils.GetDateTime(reader, "fch_Credito").GetValueOrDefault();
                                        fotopetres.erogacion = Utils.GetDecimal(reader, "erogacion");
                                        fotopetres.NumeroDocumento = Utils.GetString(reader, "NumeroDocumento");
                                        fotopetres.Cliente = Utils.GetString(reader, "Cliente");
                                        fotopetres.Direccion = Utils.GetString(reader, "Direccion");
                                        fotopetres.capital = Utils.GetDecimal(reader, "capital");
                                        fotopetres.FechaPrimerPago = Utils.GetDateTime(reader, "FechaPrimerPago").GetValueOrDefault();
                                        fotopetres.TotalLiquidacion = Utils.GetDecimal(reader, "TotalLiquidacion");

                                        if (reader.NextResult())
                                        {
                                            List<FotoPeticionReestructura.FotoReciboPeticionReestructura> listaRecibos = new List<FotoPeticionReestructura.FotoReciboPeticionReestructura>();
                                            while (reader.Read())
                                            {
                                                FotoPeticionReestructura.FotoReciboPeticionReestructura recibo = new FotoPeticionReestructura.FotoReciboPeticionReestructura();
                                                recibo.FotoTipoReciboPetRes_Id = Utils.GetInt(reader, "FotoTipoReciboPetRes_Id");
                                                recibo.Recibo = Utils.GetInt(reader, "Recibo");
                                                recibo.TipoRecibo = Utils.GetString(reader, "TipoRecibo");
                                                recibo.Fch_Recibo = Utils.GetDateTime(reader, "Fch_Recibo").GetValueOrDefault();
                                                recibo.Liquida_Capital = Utils.GetDecimal(reader, "Liquida_Capital");
                                                recibo.Liquida_Interes = Utils.GetDecimal(reader, "Liquida_Interes");
                                                recibo.Liquida_IGV = Utils.GetDecimal(reader, "Liquida_IGV");
                                                recibo.Liquida_Seguro = Utils.GetDecimal(reader, "Liquida_Seguro");
                                                recibo.Liquida_GAT = Utils.GetDecimal(reader, "Liquida_GAT");
                                                listaRecibos.Add(recibo);
                                            }

                                            fotopetres.FotoReciboPeticionReestructuras = listaRecibos;
                                        }
                                        peticion.FotoPeticionReestructura = fotopetres;
                                        break;
                                    default:
                                        break;
                                }
                                resultObject.Add(peticion);
                            }
                            if (resultObject.Count > 0)
                            {
                                resultado.Codigo = 1;
                                resultado.ResultObject = resultObject;
                                resultado.CodigoStr = "Éxito.";
                            }
                            else
                            {
                                resultado.Codigo = 0;
                                resultado.Mensaje = "No se pudo recuperar ningún registro con la información proporcionada.";
                                resultado.ResultObject = null;
                            }
                        }
                        else
                        {
                            resultado.Codigo = 0;
                            resultado.Mensaje = "No se pudo recuperar la información de la base de datos.";
                            resultado.ResultObject = null;
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                resultado.Codigo = 0;
                resultado.Mensaje = ex.Message != null ?
                    "Hubo un error inesperado en el servidor. Favor de contactar a sistemas. Mensaje:" + ex.Message
                    : "Hubo un error inesperado en el servidor. Favor de contactar a sistemas.";
                resultado.ResultObject = null;
                throw ex;
            }
            return resultado;
        }

        public Resultado<TBCreditos.CalculoLiquidacion> CalcularLiquidacionPeticionReestructura(TBCreditos.Liquidacion eOwner)
        {
            Resultado<TBCreditos.CalculoLiquidacion> objReturn = new Resultado<TBCreditos.CalculoLiquidacion>();
            DataSet ds = new DataSet();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_ABCSolicitud_Liquidacion"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, "CALC_LIQUIDACION_PETRES");
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, eOwner.IdSolicitud.GetValueOrDefault());

                    if (eOwner.FechaLiquidar.Value != null)
                    {
                        db.AddInParameter(com, "@FechaLiquidacion", DbType.DateTime, eOwner.FechaLiquidar.Value.GetValueOrDefault());
                    }

                    ds = db.ExecuteDataSet(com);

                    foreach (DataRow r in ds.Tables[0].Rows)
                    {
                        objReturn.ResultObject.IdSolicitud = int.Parse(r["IdSolicitud"].ToString());
                        objReturn.ResultObject.FechaCredito = Utils.GetDateTime(r, "Fch_Credito").GetValueOrDefault();
                        objReturn.ResultObject.Cuota = decimal.Parse(r["Erogacion"].ToString());
                        objReturn.ResultObject.DNICliente = r["DNICliente"].ToString();
                        objReturn.ResultObject.NombreCliente = r["NombreCliente"].ToString();
                        objReturn.ResultObject.ApPaternoCliente = r["ApellidoPaterno"].ToString();
                        objReturn.ResultObject.ApMaternoCliente = r["ApellidoMaterno"].ToString();
                        objReturn.ResultObject.MontoCredito = decimal.Parse(r["MontoCredito"].ToString());
                        objReturn.ResultObject.DireccionCliente = r["DireccionCliente"].ToString();
                        objReturn.ResultObject.TasaAnual = decimal.Parse(r["TasaAnual"].ToString());
                        objReturn.ResultObject.FechaLiquidacion.Value = Utils.GetDateTime(r, "FechaLiquidacion").GetValueOrDefault();
                        objReturn.ResultObject.FechaPrimerPago = Utils.GetDateTime(r, "FechaPrimerPago").GetValueOrDefault();
                        objReturn.ResultObject.FechaUltimoPago = Utils.GetDateTime(r, "FechaUltimoPago").GetValueOrDefault();
                        objReturn.ResultObject.TotalLiquidacion = decimal.Parse(r["TotalLiquidacion"].ToString());
                        objReturn.ResultObject.Plazo = int.Parse(r["Plazo"].ToString());
                    }
                    
                    TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle detalleLiquidacion;
                    foreach (DataRow r in ds.Tables[1].Rows)
                    {
                        detalleLiquidacion = new TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle();

                        detalleLiquidacion.Recibo = int.Parse(r["Recibo"].ToString());
                        detalleLiquidacion.FechaRecibo = Utils.GetDateTime(r, "Fch_Recibo").GetValueOrDefault();
                        detalleLiquidacion.TipoReciboCalculo = r["TipoRecibo"].ToString();
                        detalleLiquidacion.LiquidaCapital = decimal.Parse(r["Liquida_Capital"].ToString());
                        detalleLiquidacion.LiquidaInteres = decimal.Parse(r["Liquida_Interes"].ToString());
                        detalleLiquidacion.LiquidaIGV = decimal.Parse(r["Liquida_IGV"].ToString());
                        detalleLiquidacion.LiquidaSeguro = decimal.Parse(r["Liquida_Seguro"].ToString());
                        detalleLiquidacion.LiquidaGAT = decimal.Parse(r["Liquida_GAT"].ToString());
                        detalleLiquidacion.LiquidaTotal = decimal.Parse(r["Liquida_Total"].ToString());

                        objReturn.ResultObject.DetalleLiquidacion.Add(detalleLiquidacion);
                    }

                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public Resultado<bool> AutorizarPeticionReestructura(AutorizarPeticionReestructuraRequest entity)
        {
            Resultado<bool> resultado = new Resultado<bool>();
            try
            {
                bool exito = false;
                string mensaje = string.Empty;
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_PeticionReestructuraAutorizar"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, entity.Accion);
                    db.AddInParameter(com, "@IdPeticionReestructura", DbType.Int32, entity.Id_PeticionReestructura);
                    db.AddInParameter(com, "@AutorizadorUsuario_Id", DbType.Int32, entity.AutorizadorUsuario_Id);
                    db.AddInParameter(com, "@ComentarioAutorizador", DbType.String, entity.ComentarioAutorizador);
                    db.AddOutParameter(com, "@OperacionCorrecta", DbType.Boolean, 2);
                    db.AddOutParameter(com, "@MensajeOperacion", DbType.String, 1024);
                    db.AddOutParameter(com, "@ResultadoSolicitud", DbType.String, 1024);
                    db.ExecuteScalar(com);
                    if (bool.TryParse(db.GetParameterValue(com, "@OperacionCorrecta").ToString(), out exito))
                    {
                        resultado.Codigo = (bool)db.GetParameterValue(com, "@OperacionCorrecta") ? 0 : 1;
                        resultado.CodigoStr = db.GetParameterValue(com, "@MensajeOperacion").ToString();
                        resultado.Mensaje = db.GetParameterValue(com, "@ResultadoSolicitud").ToString();
                        resultado.ResultObject = exito;
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                resultado.Codigo = 1;
                resultado.Mensaje = ex.Message != null ?
                    "Hubo un error inesperado en el servidor. Favor de contactar a sistemas. Mensaje:" + ex.Message
                    : "Hubo un error inesperado en el servidor. Favor de contactar a sistemas.";
                resultado.ResultObject = false;
                throw ex;
            }
            return resultado;
        }

        public Resultado<bool> RechazarPeticionReestructura(RechazarPeticionReestructuraRequest entity)
        {
            Resultado<bool> resultado = new Resultado<bool>();
            try
            {
                bool exito = false;
                string mensaje = string.Empty;
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SP_PeticionReestructuraRechazar"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, entity.Accion);
                    db.AddInParameter(com, "@IdPeticionReestructura", DbType.Int32, entity.Id_PeticionReestructura);
                    db.AddInParameter(com, "@AutorizadorUsuario_Id", DbType.Int32, entity.AutorizadorUsuario_Id);
                    db.AddInParameter(com, "@ComentarioAutorizador", DbType.String, entity.ComentarioAutorizador);
                    db.AddOutParameter(com, "@OperacionCorrecta", DbType.Boolean, 2);
                    db.AddOutParameter(com, "@MensajeOperacion", DbType.String, 1024);
                    db.ExecuteScalar(com);
                    if (bool.TryParse(db.GetParameterValue(com, "@OperacionCorrecta").ToString(), out exito))
                    {
                        resultado.Codigo = (bool)db.GetParameterValue(com, "@OperacionCorrecta") ? 0 : 1;
                        resultado.CodigoStr = db.GetParameterValue(com, "@MensajeOperacion").ToString();
                        resultado.Mensaje = db.GetParameterValue(com, "@MensajeOperacion").ToString();
                        resultado.ResultObject = exito;
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                resultado.Codigo = 1;
                resultado.Mensaje = ex.Message != null ?
                    "Hubo un error inesperado en el servidor. Favor de contactar a sistemas. Mensaje:" + ex.Message
                    : "Hubo un error inesperado en el servidor. Favor de contactar a sistemas.";
                resultado.ResultObject = false;
                throw ex;
            }
            return resultado;
        }

        public List<Combo> ObtenerCombo_PlazosReestructura(ObtenerComboPlazosReestructuraRequest entity)
        {
            List<Combo> _lstCATCombo = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("catalogo.PlazosReestructuraCON"))
                {
                    #region Parámetros
                    db.AddInParameter(com, "@Accion", DbType.String, entity.Accion);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, entity.IdSolicitud);
                    #endregion

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            _lstCATCombo = new List<Combo>();
                            while (reader.Read())
                            {
                                Combo _CATCombo = new Combo();
                                _CATCombo.Valor = Convert.ToInt32(reader[0]);
                                _CATCombo.Descripcion = Convert.ToString(reader[1]);
                                _lstCATCombo.Add(_CATCombo);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _lstCATCombo;
        }

        public ObtenerProductoReestructuraResponse ObtenerProductoReestructura(ObtenerProductoReestructuraRequest entidad)
        {
            ObtenerProductoReestructuraResponse res = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.ProductoReestructuraCON"))
                {
                    db.AddInParameter(com, "@Accion", DbType.String, entidad.Accion);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, entidad.IdSolicitud);
                    db.AddInParameter(com, "@Plazo", DbType.Int32, entidad.Plazo);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            res = new ObtenerProductoReestructuraResponse();
                            while (reader.Read())
                            {
                                res.Resultado.IdProducto = Utils.GetInt(reader, "IdProducto");
                                res.Resultado.Tasa = Utils.GetDecimal(reader, "Tasa");
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (SqlException se)
            {
                res.Error = true;
                res.MensajeErrorException = se.Message;
            }
            catch (Exception ex)
            {
                res.Error = true;
                res.MensajeOperacion = "Ocurrió un error al momento de intentar obtener el producto reestructura asociado a los datos proporcionados.";
                res.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: " + entidad.Accion + entidad.IdSolicitud.ToString() + entidad.Plazo.ToString(), JsonConvert.SerializeObject(ex));
            }
            return res;
        }
        #endregion
        #region Métodos Privados
        private static Respuesta InsertarGestionPeticionReestructura(int idUsuario, int idSolicitud, string comentario)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                List<TBCatGestores> resulGestor = TBCatGestoresBll.ObtenerTBCatGestores(4, 0, 0, idUsuario);
                if (resulGestor == null || resulGestor.Count == 0) throw new Exception("No fue posible obtener datos del gestor.");
                int idGestor = resulGestor[0].IdGestor;
                TB_Creditos credito = new TB_Creditos();
                TipoGestion tipoGestion = new TipoGestion();
                TB_CATResultadoGestion resultadoGestion = new TB_CATResultadoGestion();
                using (CreditOrigContext con = new CreditOrigContext())
                {
                    credito = con.dbo_TB_Creditos.Where(c => c.IdSolicitud == idSolicitud && c.IdEstatus == 11).FirstOrDefault();
                    if (credito == null) throw new Exception("No se encontró la cuenta.");
                    tipoGestion = con.Gestiones_TipoGestion.Where(t => t.Clave == "RestPagos" && t.Activo).FirstOrDefault();
                    if (tipoGestion == null) throw new Exception("No se encontró el tipo gestión para dar de alta.");
                    resultadoGestion = con.Gestiones_TB_CATResultadoGestion.Where(r => r.TipoGestion.HasValue && r.TipoGestion.Value == tipoGestion.IdTipoGestion && r.Descripcion == "Actualización enviada" && r.Estatus.HasValue && r.Estatus.Value).FirstOrDefault();
                    if (resultadoGestion == null) throw new Exception("No se encontró el resultado de gestión para dar de alta.");
                }
                Gestion gestionDB = new Gestion
                {
                    IdCuenta = credito.IdCuenta,
                    IdGestor = idGestor,
                    Comentario = comentario,
                    IdResultadoGestion = resultadoGestion.IdResultado,
                    IdTipoGestion = tipoGestion.IdTipoGestion,
                    idCNT = 0
                };
                int idGestion = GestionBll.InsertarGestion(gestionDB);
                if (idGestion > 0)
                {
                    respuesta.MensajeOperacion = idGestion.ToString();
                    return respuesta;
                }
                else
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = "No se pudo insertar la gestión.";
                    respuesta.MensajeErrorException = "No se pudo insertar la gestión.";
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = "Ocurrió un error al momento de dar de alta la gestión.";
                respuesta.MensajeErrorException = ex.Message;
                Utils.EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: " + idUsuario.ToString() + idSolicitud.ToString() + comentario, JsonConvert.SerializeObject(ex));
            }
            return respuesta;
        }
        #endregion
    }
}