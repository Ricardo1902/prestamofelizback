﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Gestiones;

namespace SOPF.Core.DataAccess.Gestiones
{
    public class DetalleCuentaDal
    {
        #region Objetos Base de Datos
		Database db;
		#endregion

        #region Contructor
		public DetalleCuentaDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion


        public DetalleCuenta ObtenerDetalleCuenta(int idCuenta)
        {

            DetalleCuenta resultado = new DetalleCuenta();
            try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_DetalleCuentaCON"))
				{
					//Parametros
                   
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, idCuenta);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
                          
							//Le    ctura de los datos del ResultSet
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0)) resultado.Idcuenta = Convert.ToDecimal(reader[0]);
                                if (!reader.IsDBNull(1)) resultado.Cliente = reader[1].ToString();
                                if (!reader.IsDBNull(2)) resultado.Direccion = reader[2].ToString();
                                if (!reader.IsDBNull(3)) resultado.Colonia = reader[3].ToString();
                                if (!reader.IsDBNull(4)) resultado.Ciudad = reader[4].ToString();
                                if (!reader.IsDBNull(5)) resultado.Estado = reader[5].ToString();
                                if (!reader.IsDBNull(6)) resultado.Rfc = reader[6].ToString();
                                if (!reader.IsDBNull(7)) resultado.Telefono = reader[7].ToString();
                                if (!reader.IsDBNull(8)) resultado.Celular = reader[8].ToString();
                                if (!reader.IsDBNull(9)) resultado.Referencia1 = reader[9].ToString();
                                if (!reader.IsDBNull(10)) resultado.LadaRef1 = reader[10].ToString();
                                if (!reader.IsDBNull(11)) resultado.TeleRef1 = reader[11].ToString();
                                if (!reader.IsDBNull(12)) resultado.Referencia2 = reader[12].ToString();
                                if (!reader.IsDBNull(13)) resultado.LadaRef2 = reader[13].ToString();
                                if (!reader.IsDBNull(14)) resultado.TeleRef2 = reader[14].ToString();
                                if (!reader.IsDBNull(15)) resultado.FchCredito = Convert.ToDateTime(reader[15]);
                                if (!reader.IsDBNull(16)) resultado.Producto = reader[16].ToString();
                                if (!reader.IsDBNull(17)) resultado.Erogacion = Convert.ToDecimal(reader[17]);
                                if (!reader.IsDBNull(18)) resultado.Capital = Convert.ToDecimal(reader[18]);
                                if (!reader.IsDBNull(19)) resultado.SdoVencido = Convert.ToDecimal(reader[19]);
                                if (!reader.IsDBNull(20)) resultado.SdoVigente = Convert.ToDecimal(reader[20]);
                                if (!reader.IsDBNull(21)) resultado.Diasinac = Convert.ToInt32(reader[21]);
                                if (!reader.IsDBNull(22)) resultado.Diasven = Convert.ToInt32(reader[22]);
                                if (!reader.IsDBNull(23)) resultado.Saldocredi = Convert.ToDecimal(reader[23]);
                                if (!reader.IsDBNull(24)) resultado.Sigpag = reader[24].ToString();
                                if (!reader.IsDBNull(25)) resultado.Recexi = Convert.ToInt32(reader[25]);
                                if (!reader.IsDBNull(26)) resultado.Recven = Convert.ToInt32(reader[26]);
                                if (!reader.IsDBNull(27)) resultado.Recsal = Convert.ToInt32(reader[27]);
                                if (!reader.IsDBNull(28)) resultado.Rectot = Convert.ToInt32(reader[28]);
                                if (!reader.IsDBNull(29)) resultado.IdSolicitud = Convert.ToDecimal(reader[29]);
                                if (!reader.IsDBNull(30)) resultado.parantesco1 = reader[30].ToString();
                                if (!reader.IsDBNull(31)) resultado.parantesco2 = reader[31].ToString();
                                if (!reader.IsDBNull(32)) resultado.ReferenciaB = Convert.ToInt32(reader[32]);
                                if (!reader.IsDBNull(33)) resultado.frecuencia = reader[33].ToString();
                                if (!reader.IsDBNull(34)) resultado.BalanceTotal = Convert.ToInt32(reader[34]);
                                if (!reader.IsDBNull(35)) resultado.Banco = reader[35].ToString();
                                if (!reader.IsDBNull(36)) resultado.Clabe = reader[36].ToString();
                                if (!reader.IsDBNull(37)) resultado.EstatusBanco = reader[37].ToString();
                                if (!reader.IsDBNull(38)) resultado.FlagMoraInicioMes = reader[38].ToString();
                                if (!reader.IsDBNull(39)) resultado.FlagMoraActual = reader[39].ToString();
                                if (!reader.IsDBNull(40)) resultado.GestorAsigando = reader[40].ToString();
                                if (!reader.IsDBNull(41)) resultado.FechaUltimoPago = reader[41].ToString();
                                if (!reader.IsDBNull(42)) resultado.CelRef1 = reader[42].ToString();
                                if (!reader.IsDBNull(43)) resultado.CelRef2 = reader[43].ToString();
                                if (!reader.IsDBNull(44)) resultado.IdCliente = Convert.ToInt32(reader[44]);

                            }
                        }
                        reader.Close();
						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;

        }
       

    }
}
