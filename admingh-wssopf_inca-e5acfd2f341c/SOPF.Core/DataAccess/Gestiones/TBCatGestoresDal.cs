#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Gestiones;

# region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCatGestoresDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCatGestores
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Gestiones
{
    public class TBCatGestoresDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCatGestoresDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBCatGestores(int idTipo,int idUsuarioGes, int IdUsuario)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("gestiones.SP_CATGestoresALT"))
				{
					//Parametros
					db.AddInParameter(com, "@IdTipoGes", DbType.Int32, idTipo);
                    db.AddInParameter(com, "@IdUsuarioGes", DbType.Int32, idUsuarioGes);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<TBCatGestores> ObtenerTBCatGestores(int accion, int idGestor,   int idTipo, int Usuario)
        {
			List<TBCatGestores> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Gestiones.SP_CatGestoresCON"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, accion);
                    db.AddInParameter(com, "@IdGestor", DbType.Int32, idGestor);
                    db.AddInParameter(com, "@IdTipo", DbType.Int32, idTipo);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, Usuario);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<TBCatGestores>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCatGestores Gestor = new TBCatGestores();
                                if (!reader.IsDBNull(0)) Gestor.IdGestor = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Gestor.Nombre = reader[1].ToString();
                                if (!reader.IsDBNull(2)) Gestor.IdTipo = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) Gestor.Usuarioid = Convert.ToInt32(reader[3]);
                                if (!reader.IsDBNull(4)) Gestor.Estatus = Convert.ToBoolean(reader[4]);
                                if (!reader.IsDBNull(5)) Gestor.FechaAlta = Convert.ToDateTime(reader[5]);
                                if (!reader.IsDBNull(6)) Gestor.UsuarioIdAlta = Convert.ToInt32(reader[6]);
                                if (!reader.IsDBNull(7)) Gestor.FechaModifico = Convert.ToDateTime(reader[7]);
                                if (!reader.IsDBNull(8)) Gestor.UsuarioIdModifico = Convert.ToInt32(reader[8]);
                                resultado.Add(Gestor);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarTBCatGestores()
        {
        }

        public void EliminarTBCatGestores(int idUsuarioGes, int Usuario)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("gestiones.SP_CATGestoresBAJ"))
                {
                    //Parametros
                   
                    db.AddInParameter(com, "@IdUsuarioGes", DbType.Int32, idUsuarioGes);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, Usuario);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}