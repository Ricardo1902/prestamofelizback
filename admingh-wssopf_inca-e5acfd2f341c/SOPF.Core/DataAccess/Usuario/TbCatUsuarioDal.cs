#pragma warning disable 140811
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Usuario;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TbCatUsuarioDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TbCatUsuario
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-11 	Juan Carlos Garcia  	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Usuario
{
    public class TbCatUsuarioDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TbCatUsuarioDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region CRUD Methods
        public void InsertarTbCatUsuario(TBCATUsuario usuario)
        {

            //try
            //{
            //    //Obtener DbCommand para ejcutar el Store Procedure
            //    using (DbCommand com = db.GetStoredProcCommand("Usuario.Crear"))
            //    {
            //        //Parametros
            //        db.AddInParameter(com, "@Nombre", DbType.String, usuario.Nombre);
            //        db.AddInParameter(com, "@ApellidoPaterno", DbType.String, usuario.ApellidoPaterno);
            //        db.AddInParameter(com, "@ApellidoMaterno", DbType.String, usuario.ApellidoMaterno);
            //        db.AddInParameter(com, "@Contraseña", DbType.String, usuario.Contraseña);
            //        db.AddInParameter(com, "@IdUsuario", DbType.Int32, 1);				
            //        //Ejecucion de la Consulta
            //        db.ExecuteNonQuery(com);

            //        //Cierre de la conexion y liberacion de memoria
            //        com.Dispose();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }

        public List<TBCATUsuario> ObtenerTbCatUsuario(int Accion, int IdUsuario, string Usuario)
        {
            List<TBCATUsuario> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Usuario.SP_SelUsuario"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@usuario", DbType.String, Usuario);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCATUsuario>();

                            while (reader.Read())
                            {
                                TBCATUsuario usuario = new TBCATUsuario();
                                if (!reader.IsDBNull(0)) usuario.IdUsuario = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) usuario.IdDepartamento = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) usuario.NombreCompleto = reader[2].ToString();
                                if (!reader.IsDBNull(3)) usuario.Nombres = reader[3].ToString();
                                if (!reader.IsDBNull(4)) usuario.ApellidoP = reader[4].ToString();
                                if (!reader.IsDBNull(5)) usuario.ApellidoM = reader[5].ToString();
                                if (!reader.IsDBNull(6)) usuario.RFC = reader[6].ToString();
                                if (!reader.IsDBNull(7)) usuario.Lada = reader[7].ToString();
                                if (!reader.IsDBNull(8)) usuario.Telefono = reader[8].ToString();
                                if (!reader.IsDBNull(9)) usuario.Email = reader[9].ToString();
                                if (!reader.IsDBNull(10)) usuario.Usuario = reader[10].ToString();
                                if (!reader.IsDBNull(11)) usuario.IdTipoUsuario = Convert.ToInt32(reader[11]);
                                if (!reader.IsDBNull(12)) usuario.IdSucursal = Convert.ToInt32(reader[12]);
                                if (!reader.IsDBNull(13)) usuario.IdEstatus = Convert.ToInt32(reader[13]);
                                if (!reader.IsDBNull(14)) usuario.Exito = Convert.ToInt32(reader[14]);
                                resultado.Add(usuario);
                            }
                        }

                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<TBCATUsuario> BuscarUsuario(int Accion, int IdUsuario, string Usuario, int IdSucursal)
        {
            List<TBCATUsuario> objReturn = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Usuario.SP_SelUsuario"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@usuario", DbType.String, Usuario);
                    db.AddInParameter(com, "@IdSucursal", DbType.String, IdSucursal);
                    //Ejecucion de la Consulta

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            TBCATUsuario objC = null;
                            objReturn = new List<TBCATUsuario>();
                            while (reader.Read())
                            {
                                objC = new TBCATUsuario();

                                //Lectura de los datos del ResultSet                                
                                objC.IdUsuario = Utils.GetInt(reader, "IdUsuario");
                                objC.NombreCompleto = Utils.GetString(reader, "NombreCompleto");
                                objC.Usuario = Utils.GetString(reader, "Usuario");
                                objC.TipoUsuario = Utils.GetString(reader, "tipoUsuario");
                                objC.Sucursal = Utils.GetString(reader, "Sucursal");
                                objC.Estatus = Utils.GetString(reader, "Estatus");
                                objC.IdGestor = Utils.GetInt(reader, "IdGestor");

                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public void ActualizarTbCatUsuario()
        {
        }

        public void EliminarTbCatUsuario()
        {
        }

        //ModificarSolicitud-LP
        public List<Combo> ObtenerCATCombo()
        {
            List<Combo> _lstCATCombo = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Usuario.SP_SelUsuario"))
                {
                    db.AddInParameter(com, "@Accion", DbType.Int32, 998);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, 0);
                    db.AddInParameter(com, "@usuario", DbType.String, "");

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            _lstCATCombo = new List<Combo>();
                            while (reader.Read())
                            {
                                Combo _CATCombo = new Combo();
                                _CATCombo.Valor = Convert.ToInt32(reader[0]);
                                _CATCombo.Descripcion = Convert.ToString(reader[1]);
                                _lstCATCombo.Add(_CATCombo);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _lstCATCombo;
        }


        #endregion

        #region MISC Methods

        public Resultado<bool> validaUsuario(string usario, string contraseña, string IPAddress, string HostName, string MACAddress)
        {
            Resultado<bool> respuesta = new Resultado<bool>();
            try
            {

                using (DbCommand com = db.GetStoredProcCommand("usuario.SP_ValidaUsuario"))
                {
                    //Parametros
                    db.AddInParameter(com, "@parametro0", DbType.String, usario);
                    db.AddInParameter(com, "@parametro1", DbType.String, contraseña);
                    db.AddInParameter(com, "@parametro2", DbType.String, IPAddress);
                    db.AddInParameter(com, "@parametro3", DbType.String, HostName);
                    db.AddInParameter(com, "@parametro4", DbType.String, MACAddress);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            if (reader.Read())
                            {
                                //Lectura de los datos del ResultSet
                                respuesta.Codigo = Convert.ToInt32(reader[0]);
                                respuesta.Mensaje = reader[1].ToString();
                                respuesta.IdUsuario = reader[2].ToString();
                                respuesta.FechaEntro = Convert.ToDateTime(reader[3].ToString());
                            }
                        }

                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                respuesta.Codigo = 0;
                respuesta.Mensaje = "Ocurrio un error al validar el usuario favor de contactar a área de sistemas";
            }
            return respuesta;
        }
        public bool EsAdministrador(int accion, int idUsuario)
        {
            bool esAdmin = false;
            try
            {
                using (DbCommand com = db.GetSqlStringCommand("SELECT usuario.FN_EsUsuarioAdministrador(@Accion, @IdUsuario)"))
                {
                    db.AddInParameter(com, "@Accion", DbType.Int32, accion);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, idUsuario);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            if (reader.Read())
                            {
                                bool.TryParse(reader[0].ToString(), out esAdmin);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    com.Dispose();
                }

            }
            catch (Exception ex)
            {
            }

            return esAdmin;
        }

        #endregion


        #region Private Methods
        #endregion
    }
}