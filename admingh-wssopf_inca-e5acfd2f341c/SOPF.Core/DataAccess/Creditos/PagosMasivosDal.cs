﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Creditos;


namespace SOPF.Core.DataAccess.Creditos
{
    public class PagosMasivosDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public PagosMasivosDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion


        public void InsertarPago(int idsolicitud, decimal monto, DateTime Fecha, decimal Comision, int id, int Accion, int Origen, DateTime Quincena, int GrupoRelacion, string TipoPago, int resultHeaderId)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.PagosMasivos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@idsolicitud", DbType.Int32, idsolicitud);
                    db.AddInParameter(com, "@monto", DbType.Decimal, Convert.ToDecimal(monto));
                    db.AddInParameter(com, "@Fecha", DbType.DateTime, Fecha);
                    db.AddInParameter(com, "@Comision", DbType.Decimal, Comision);
                    db.AddInParameter(com, "@IdGrupo", DbType.Int32, id);
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@Origen", DbType.Int32, Origen);
                    db.AddInParameter(com, "@PagoAplicadoO", DbType.Int32, 0);
                    db.AddInParameter(com, "@Quincena", DbType.DateTime, Quincena);
                    db.AddInParameter(com, "@GrupoRelacion", DbType.Int32, GrupoRelacion);
                    db.AddInParameter(com, "@TipoPago", DbType.String, TipoPago);
                    db.AddInParameter(com, "@resultHeaderId", DbType.Int32, resultHeaderId);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GeneraId()
        {
            int id = 0;

            try {
               //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.InsertarPagosEfectivo"))
               {
                   //Parametros

                   //Ejecucion de la Consulta
                   using (IDataReader reader = db.ExecuteReader(com))
                   {
                       if (reader != null)
                       {
                           
                           //Lectura de los datos del ResultSet
                           while (reader.Read())
                           {

                               if (!reader.IsDBNull(0)) id = Convert.ToInt32(reader[0]);
                               
                           }
                       }
                       reader.Close();
                       reader.Dispose();
                   }
               }
           }
                catch (Exception ex)
           {
               throw ex;
           }

           return id;


        }

        public List<AutorizaPagosEfectivo> ConsultarPagoEfectivo(DateTime FchIni, DateTime FchFin, string Tip_Consul)
        {
            List<AutorizaPagosEfectivo> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.Sel_AutorizacionPagosEfectivo"))
                {
                    //Parametros
                    db.AddInParameter(com, "@FchIni", DbType.DateTime, FchIni);
                    db.AddInParameter(com, "@FchFin", DbType.DateTime, FchFin);
                    db.AddInParameter(com, "@Tip_Consul", DbType.String, Tip_Consul);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<AutorizaPagosEfectivo>();
                           
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                AutorizaPagosEfectivo Pagos = new AutorizaPagosEfectivo();
                               
                                if (!reader.IsDBNull(0)) Pagos.IdFolio = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Pagos.IdGrupo = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) Pagos.Fecha = Convert.ToDateTime(reader[2]);
                                if (!reader.IsDBNull(3)) Pagos.IdSolicitud = Convert.ToInt32(reader[3]);
                                if (!reader.IsDBNull(4)) Pagos.IdCuenta = Convert.ToInt32(reader[4]);
                                if (!reader.IsDBNull(5)) Pagos.Monto = Convert.ToDecimal(reader[5]);
                                if (!reader.IsDBNull(6)) Pagos.Estatus = Convert.ToString(reader[6]);
                                if (!reader.IsDBNull(7)) Pagos.DescOrigen  = Convert.ToString(reader[7]);

                                resultado.Add(Pagos);
                            }

                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                }
            }

            catch (Exception ex)

            {
                throw ex;
            }

            return resultado; 
        }

        public void AutorizarPagoEfectivo (int IdFolio, decimal MontoAplicado, int Accion, int idGpo, int IdUser, int IdUserAut, DateTime Fecha, int IdSolicitud)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.AutorizacionPagosEfectivo_PRO"))
                {
                    //Parametros
                    db.AddInParameter(com, "@idfolio", DbType.Int32, IdFolio);
                    db.AddInParameter(com, "@MontoAplicado", DbType.Decimal, MontoAplicado);
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdGpo", DbType.Int32, idGpo);
                    db.AddInParameter(com, "@IdUser", DbType.Int32, IdUser);
                    db.AddInParameter(com, "@IdUserAut", DbType.Int32, IdUserAut);
                    db.AddInParameter(com, "@Fecha", DbType.DateTime, Fecha);
                    db.AddInParameter(com, "@IdSolRef", DbType.Int32, IdSolicitud);


                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerGrupoRelacion(int Accion, int idCobranzaBanco, int idReciboCobranza)
        {
           // return dal.ObtenerGrupoRelacion(Accion, idCobranzaBanco, idReciboCobranza);

            int idGrelacion = 0;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_ABCCobranzaBanco"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@idCobranzaBanco", DbType.Int32, idCobranzaBanco);
                    db.AddInParameter(com, "@idReciboCobranza", DbType.Int32, idReciboCobranza);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {

                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {

                                if (!reader.IsDBNull(0)) idGrelacion = Convert.ToInt32(reader[0]);

                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return idGrelacion;
        }

        public List<PagosAplicados> ConsultarPagosAplicados(int tipoConsul, int IdGrupo, string fch_pago)
        {
            List<PagosAplicados> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_consultaAplicacionPagos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@TipoConsulta", DbType.Int32, tipoConsul);
                    db.AddInParameter(com, "@IdGrupo", DbType.Int32, IdGrupo);
                    db.AddInParameter(com, "@FechaPag", DbType.String, fch_pago);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<PagosAplicados>();

                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                PagosAplicados Pagos = new PagosAplicados();

                                if (!reader.IsDBNull(0)) Pagos.id_solicitud = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Pagos.monto_aplicado = Convert.ToDecimal(reader[1]);
                                if (!reader.IsDBNull(2)) Pagos.fecha_cobro = Convert.ToString(reader[2]);
                                if (!reader.IsDBNull(3)) Pagos.Comentario = Convert.ToString(reader[3]);
                                if (!reader.IsDBNull(4)) Pagos.IdGrupoAplicacion = Convert.ToInt32(reader[4]);
                                if (!reader.IsDBNull(5)) Pagos.Convenio = Convert.ToString(reader[5]);
                                if (!reader.IsDBNull(6)) Pagos.Sucursal = Convert.ToString(reader[6]);

                                resultado.Add(Pagos);
                            }

                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return resultado; 
        }
      
    }
}
