﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Creditos;

namespace SOPF.Core.DataAccess.Creditos
{
    public class EdoCtaDal
    {
        #region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
        public EdoCtaDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
        public EdoCuenta.RazonSocial DatosRazonSocial(int idcompania)
        {
            EdoCuenta.RazonSocial resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("creditos.SP_EstadoCuentaCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 1);
                    db.AddInParameter(com, "@idCompania", DbType.Int32, idcompania);
                    

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new EdoCuenta.RazonSocial();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                EdoCuenta.RazonSocial razonSocial = new EdoCuenta.RazonSocial();
                                if (!reader.IsDBNull(0)) razonSocial.razonSocial = reader[0].ToString();
                                if (!reader.IsDBNull(1)) razonSocial.Direccion = reader[1].ToString();
                                if (!reader.IsDBNull(2)) razonSocial.RFC = reader[2].ToString();
                                if (!reader.IsDBNull(3)) razonSocial.TelefonoLocal = reader[3].ToString();
                                if (!reader.IsDBNull(4)) razonSocial.TelefonoIntRep = reader[4].ToString();

                                resultado = razonSocial;
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        public EdoCuenta.InfoCreditos InfoCreditos(int idCuenta, DateTime Fecini, DateTime FecFin, double CAT)
        {
            EdoCuenta.InfoCreditos resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("creditos.SP_EstadoCuentaCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 2);
                    db.AddInParameter(com, "@idCuenta", DbType.Int32, idCuenta);
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, Fecini);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, FecFin);
                    db.AddInParameter(com, "@CAT", DbType.Double, CAT);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new EdoCuenta.InfoCreditos();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                EdoCuenta.InfoCreditos infoCredito = new EdoCuenta.InfoCreditos();
                                if (!reader.IsDBNull(0)) infoCredito.idCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) infoCredito.idSolicitud = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) infoCredito.FechaIni = Convert.ToDateTime(reader[2].ToString());
                                if (!reader.IsDBNull(3)) infoCredito.FechaFin = Convert.ToDateTime(reader[3].ToString());
                                if (!reader.IsDBNull(4)) infoCredito.Cliente = reader[4].ToString();
                                if (!reader.IsDBNull(5)) infoCredito.Direccion = reader[5].ToString();
                                if (!reader.IsDBNull(6)) infoCredito.RFC = reader[6].ToString();
                                if (!reader.IsDBNull(7)) infoCredito.Sucursal = reader[7].ToString();
                                if (!reader.IsDBNull(8)) infoCredito.capital = Convert.ToDecimal(reader[8].ToString());
                                if (!reader.IsDBNull(9)) infoCredito.interes = Convert.ToDecimal(reader[9].ToString());
                                if (!reader.IsDBNull(10)) infoCredito.iva_intere = Convert.ToDecimal(reader[10].ToString());
                                if (!reader.IsDBNull(11)) infoCredito.Moneda = reader[11].ToString();
                                if (!reader.IsDBNull(12)) infoCredito.Fch_Credito = Convert.ToDateTime(reader[12].ToString());
                                if (!reader.IsDBNull(13)) infoCredito.Cat = Convert.ToDecimal(reader[13].ToString());
                                if (!reader.IsDBNull(14)) infoCredito.ProtFallecimiento = Convert.ToDecimal(reader[14].ToString());
                                if (!reader.IsDBNull(15)) infoCredito.ivaProtFall = Convert.ToDecimal(reader[15].ToString());
                                if (!reader.IsDBNull(16)) infoCredito.apertura = Convert.ToDecimal(reader[16].ToString());
                                if (!reader.IsDBNull(17)) infoCredito.ivaapertura = Convert.ToDecimal(reader[17].ToString());
                                if (!reader.IsDBNull(18)) infoCredito.GastosCob = Convert.ToDecimal(reader[18].ToString());
                                if (!reader.IsDBNull(19)) infoCredito.iva_gascob = Convert.ToDecimal(reader[19].ToString());
                                if (!reader.IsDBNull(20)) infoCredito.sdo_capita = Convert.ToDecimal(reader[20].ToString());
                                if (!reader.IsDBNull(21)) infoCredito.sdo_intere = Convert.ToDecimal(reader[21].ToString());
                                if (!reader.IsDBNull(22)) infoCredito.sdo_iva_in = Convert.ToDecimal(reader[22].ToString());
                                if (!reader.IsDBNull(23)) infoCredito.tasa = Convert.ToDecimal(reader[23].ToString());
                                if (!reader.IsDBNull(24)) infoCredito.tasa_mora = Convert.ToDecimal(reader[24].ToString());
                                if (!reader.IsDBNull(25)) infoCredito.pagos = Convert.ToInt32(reader[25].ToString());
                                if (!reader.IsDBNull(26)) infoCredito.frecuencia = reader[26].ToString();
                                if (!reader.IsDBNull(27)) infoCredito.TotalRecPagados = Convert.ToInt32(reader[27].ToString());
                                resultado = infoCredito;
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        public EdoCuenta.totalAbonos TotalAbonos(int idCuenta, DateTime Fecini, DateTime FecFin, double CAT)
        {
            EdoCuenta.totalAbonos resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("creditos.SP_EstadoCuentaCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 3);
                    db.AddInParameter(com, "@idCuenta", DbType.Int32, idCuenta);
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, Fecini);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, FecFin);
                    db.AddInParameter(com, "@CAT", DbType.Double, CAT);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new EdoCuenta.totalAbonos();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {

                                EdoCuenta.totalAbonos AbonosT = new EdoCuenta.totalAbonos();

                                

                                if (!reader.IsDBNull(0)) AbonosT.idcuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) AbonosT.totalPagado = Convert.ToDecimal(reader[1]);
                                if (!reader.IsDBNull(2)) AbonosT.totalCondonado = Convert.ToDecimal(reader[2].ToString());
                                if (!reader.IsDBNull(3)) AbonosT.totalGAT = Convert.ToDecimal(reader[3].ToString());
                                if (!reader.IsDBNull(4)) AbonosT.totalSeguro = Convert.ToDecimal(reader[4].ToString());
                                if (!reader.IsDBNull(5)) AbonosT.totalIGV = Convert.ToDecimal(reader[5].ToString());

                                resultado = AbonosT;
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        public EdoCuenta.ResumenSaldos Saldos(int idCuenta, DateTime Fecini, DateTime FecFin, double CAT)
        {
            EdoCuenta.ResumenSaldos resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("creditos.SP_EstadoCuentaCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 4);
                    db.AddInParameter(com, "@idCuenta", DbType.Int32, idCuenta);
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, Fecini);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, FecFin);
                    db.AddInParameter(com, "@CAT", DbType.Double, CAT);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new EdoCuenta.ResumenSaldos();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {

                                EdoCuenta.ResumenSaldos saldos = new EdoCuenta.ResumenSaldos();



                                if (!reader.IsDBNull(0)) saldos.idcuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) saldos.saldoVigente = Convert.ToDecimal(reader[1]);
                                if (!reader.IsDBNull(2)) saldos.saldoVencido = Convert.ToDecimal(reader[2].ToString());
                                if (!reader.IsDBNull(3)) saldos.saldoExigible = Convert.ToDecimal(reader[3].ToString());
                                if (!reader.IsDBNull(4)) saldos.saldoGasCob = Convert.ToDecimal(reader[4].ToString());
                                if (!reader.IsDBNull(5)) saldos.saldoIvaGas = Convert.ToDecimal(reader[5].ToString());
                                if (!reader.IsDBNull(6)) saldos.saldoMora = Convert.ToDecimal(reader[6].ToString());

                                resultado = saldos;
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        public List<EdoCuenta.Pagos> Pagos(int idCuenta, DateTime Fecini, DateTime FecFin, double CAT)
        {
            List<EdoCuenta.Pagos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("creditos.SP_EstadoCuentaCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 5);
                    db.AddInParameter(com, "@idCuenta", DbType.Int32, idCuenta);
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, Fecini);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, FecFin);
                    db.AddInParameter(com, "@CAT", DbType.Double, CAT);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<EdoCuenta.Pagos>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {

                                EdoCuenta.Pagos pagos = new EdoCuenta.Pagos();



                                if (!reader.IsDBNull(0)) pagos.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) pagos.FchMvto  = Convert.ToDateTime(reader[1]);
                                if (!reader.IsDBNull(2)) pagos.concepto = reader[2].ToString();
                                if (!reader.IsDBNull(3)) pagos.Capital = Convert.ToDecimal(reader[3].ToString());
                                if (!reader.IsDBNull(4)) pagos.Interes = Convert.ToDecimal(reader[4].ToString());
                                if (!reader.IsDBNull(5)) pagos.IGV = Convert.ToDecimal(reader[5].ToString());
                                if (!reader.IsDBNull(6)) pagos.GAT = Convert.ToDecimal(reader[6].ToString());
                                if (!reader.IsDBNull(7)) pagos.Seguro = Convert.ToDecimal(reader[7].ToString());

                                resultado.Add(pagos);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        public List<EdoCuenta.Vencidos> RecibosVencidos(int idCuenta, DateTime Fecini, DateTime FecFin, double CAT)
        {
            List<EdoCuenta.Vencidos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("creditos.SP_EstadoCuentaCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 6);
                    db.AddInParameter(com, "@idCuenta", DbType.Int32, idCuenta);
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, Fecini);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, FecFin);
                    db.AddInParameter(com, "@CAT", DbType.Double, CAT);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<EdoCuenta.Vencidos>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {

                                EdoCuenta.Vencidos pagosVencidos = new EdoCuenta.Vencidos();



                                if (!reader.IsDBNull(0)) pagosVencidos.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) pagosVencidos.Recibo = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) pagosVencidos.FchRecibo = Convert.ToDateTime(reader[2].ToString());
                                if (!reader.IsDBNull(3)) pagosVencidos.Capital = Convert.ToDecimal(reader[3].ToString());
                                if (!reader.IsDBNull(4)) pagosVencidos.Interes = Convert.ToDecimal(reader[4].ToString());
                                if (!reader.IsDBNull(5)) pagosVencidos.IGV = Convert.ToDecimal(reader[5].ToString());
                                if (!reader.IsDBNull(6)) pagosVencidos.GAT = Convert.ToDecimal(reader[6].ToString());
                                if (!reader.IsDBNull(7)) pagosVencidos.Seguro = Convert.ToDecimal(reader[7].ToString());

                                resultado.Add(pagosVencidos);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        public List<EdoCuenta.ProxVencimientos> RecibosPorVencer(int idCuenta, DateTime Fecini, DateTime FecFin, double CAT)
        {
            List<EdoCuenta.ProxVencimientos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("creditos.SP_EstadoCuentaCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 7);
                    db.AddInParameter(com, "@idCuenta", DbType.Int32, idCuenta);
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, Fecini);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, FecFin);
                    db.AddInParameter(com, "@CAT", DbType.Double, CAT);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<EdoCuenta.ProxVencimientos>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {

                                EdoCuenta.ProxVencimientos pagosVigentes = new EdoCuenta.ProxVencimientos();



                                if (!reader.IsDBNull(0)) pagosVigentes.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) pagosVigentes.Recibo = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) pagosVigentes.FchRecibo = Convert.ToDateTime(reader[2].ToString());
                                if (!reader.IsDBNull(3)) pagosVigentes.Capital = Convert.ToDecimal(reader[3].ToString());
                                if (!reader.IsDBNull(4)) pagosVigentes.Interes = Convert.ToDecimal(reader[4].ToString());
                                if (!reader.IsDBNull(5)) pagosVigentes.IvaIntere = Convert.ToDecimal(reader[5].ToString());
                                if (!reader.IsDBNull(6)) pagosVigentes.InteresGtoCob = Convert.ToDecimal(reader[6].ToString());
                                if (!reader.IsDBNull(7)) pagosVigentes.IvaGtoCob = Convert.ToDecimal(reader[7].ToString());

                                resultado.Add(pagosVigentes);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
    }
    
}
