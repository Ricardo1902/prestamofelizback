﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Creditos;
namespace SOPF.Core.DataAccess.Creditos
{
    public class AfiliacionDom_BanorteDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

         #region Contructor
        public AfiliacionDom_BanorteDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion

        public List<AfiliacionDom_Banorte> CuentasAfiliar(int Accion)
        {
            List<AfiliacionDom_Banorte> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.AfiliacionDom_Banorte"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 1);



                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<AfiliacionDom_Banorte>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                AfiliacionDom_Banorte afiliacion = new AfiliacionDom_Banorte();
                                if (!reader.IsDBNull(0)) afiliacion.Fecha = Convert.ToDateTime(reader[0]);
                                if (!reader.IsDBNull(1)) afiliacion.NumeroAfiliaciones = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) afiliacion.ReferenciaServicio = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) afiliacion.BancoReceptor = reader[3].ToString();
                                if (!reader.IsDBNull(4)) afiliacion.nombreTitular = reader[4].ToString();
                                if (!reader.IsDBNull(5)) afiliacion.TipoCuenta = reader[5].ToString();
                                if (!reader.IsDBNull(6)) afiliacion.Clabe = reader[6].ToString();
                                if (!reader.IsDBNull(7)) afiliacion.NumeroId = reader[7].ToString();
                                if (!reader.IsDBNull(8)) afiliacion.RFC = reader[8].ToString();
                                if (!reader.IsDBNull(9)) afiliacion.idcuenta = Convert.ToInt32(reader[9]);

                                //if (!reader.IsDBNull(11)) afiliacion.ClienteExistente = Convert.ToInt32(reader[11]);
                                resultado.Add(afiliacion);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
    }
}
