#pragma warning disable 141229
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Creditos;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CorteDispersionDetalleDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla CorteDispersionDetalle
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-12-29 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Creditos
{
    public class CorteDispersionDetalleDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public CorteDispersionDetalleDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarCorteDispersionDetalle(CorteDispersionDetalle entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }

        public void ActCorteDispersionDetalle(int Accion, int idCorte, int idsolicitud)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.ABC_CortesDispersion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdCorte", DbType.Int32, idCorte);
                    db.AddInParameter(com, "@idsolicitud", DbType.Int32, idsolicitud);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        public List<DataResultCredito.Usp_SelDetalleCorteDispersion> ObtenerCorteDispersionDetalle(int Accion, int idCorte)
        {
            List<DataResultCredito.Usp_SelDetalleCorteDispersion> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("creditos.Sel_CorteDispersion_Detalle"))
				{
					//Parametros
					db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdCorte", DbType.Int32, idCorte);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
                            resultado = new List<DataResultCredito.Usp_SelDetalleCorteDispersion>();
                            while (reader.Read())
                            {
                                DataResultCredito.Usp_SelDetalleCorteDispersion detalleCorte = new DataResultCredito.Usp_SelDetalleCorteDispersion();
                                if (!reader.IsDBNull(0)) detalleCorte.Idcuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) detalleCorte.Idsolicitud = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) detalleCorte.Cliente = reader[2].ToString();
                                if (!reader.IsDBNull(3)) detalleCorte.Capital = Convert.ToDecimal(reader[3]);
                                if (!reader.IsDBNull(4)) detalleCorte.MontoTransferencia = Convert.ToDecimal(reader[4]);
                                if (!reader.IsDBNull(5)) detalleCorte.Reestructura = reader[5].ToString();
                                if (!reader.IsDBNull(6)) detalleCorte.Clabe = reader[6].ToString();
                                resultado.Add(detalleCorte);
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarCorteDispersionDetalle()
        {
        }

        public void EliminarCorteDispersionDetalle()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}