#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Model.Response.Creditos;
using SOPF.Core.Model.Request.Creditos;
using SOPF.Core.Entities.Solicitudes;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TBCreditosDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCreditos
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Creditos
{
    public class TBCreditosDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public TBCreditosDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        #region "CRUD Methods"
        public void InsertarTBCreditos(TBCreditos entidad)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
                {
                    //Parametros
                    //db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TBCreditos> ObtenerTBCreditos(int Accion, int Cuenta)
        {
            List<TBCreditos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_SelCreditoCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@Cuenta", DbType.Int32, Cuenta);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCreditos>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCreditos Credito = new TBCreditos();
                                if (!reader.IsDBNull(0)) Credito.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Credito.FchCredito = Convert.ToDateTime(reader[1]);
                                if (!reader.IsDBNull(2)) Credito.IdSolicitud = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) Credito.CveICobr = reader[3].ToString();
                                if (!reader.IsDBNull(4)) Credito.Erogacion = Convert.ToDecimal(reader[4]);
                                if (!reader.IsDBNull(5)) Credito.Capital = Convert.ToDecimal(reader[5]);
                                if (!reader.IsDBNull(6)) Credito.Interes = Convert.ToDecimal(reader[6]);
                                if (!reader.IsDBNull(7)) Credito.IvaIntere = Convert.ToDecimal(reader[7]);
                                if (!reader.IsDBNull(8)) Credito.SdoCapita = Convert.ToDecimal(reader[8]);
                                if (!reader.IsDBNull(9)) Credito.SdoIntere = Convert.ToDecimal(reader[9]);
                                if (!reader.IsDBNull(10)) Credito.SdoIvaIn = Convert.ToDecimal(reader[10]);
                                if (!reader.IsDBNull(11)) Credito.Moratorio = Convert.ToDecimal(reader[11]);
                                if (!reader.IsDBNull(12)) Credito.IvaMorato = Convert.ToDecimal(reader[12]);
                                if (!reader.IsDBNull(13)) Credito.SdoPorApl = Convert.ToDecimal(reader[13]);
                                if (!reader.IsDBNull(14)) Credito.SdoVencido = Convert.ToDecimal(reader[14]);
                                if (!reader.IsDBNull(15)) Credito.IdEstatus = Convert.ToInt32(reader[15]);
                                if (!reader.IsDBNull(16)) Credito.FchEstatus = Convert.ToDateTime(reader[16]);
                                if (!reader.IsDBNull(17)) Credito.IdMotivo = Convert.ToInt32(reader[17]);
                                if (!reader.IsDBNull(18)) Credito.PorcenIva = Convert.ToInt32(reader[18]);
                                if (!reader.IsDBNull(19)) Credito.NoRecPag = Convert.ToInt32(reader[19]);
                                if (!reader.IsDBNull(20)) Credito.DiasVenc = Convert.ToInt32(reader[20]);
                                if (!reader.IsDBNull(21)) Credito.DiasInac = Convert.ToInt32(reader[21]);
                                if (!reader.IsDBNull(22)) Credito.DiaMasVe = Convert.ToInt32(reader[22]);
                                if (!reader.IsDBNull(23)) Credito.EstaAdmin = reader[23].ToString();
                                if (!reader.IsDBNull(24)) Credito.FchUltPago = Convert.ToDateTime(reader[24]);
                                if (!reader.IsDBNull(25)) Credito.FchGenerado = Convert.ToDateTime(reader[25]);
                                if (!reader.IsDBNull(26)) Credito.Mientras = Convert.ToInt32(reader[26]);
                                if (!reader.IsDBNull(27)) Credito.Reposicion = Convert.ToInt32(reader[27]);
                                if (!reader.IsDBNull(28)) Credito.IdCuentaReposicion = Convert.ToInt32(reader[28]);
                                if (!reader.IsDBNull(29)) Credito.GenReposicion = Convert.ToInt32(reader[29]);
                                if (!reader.IsDBNull(30)) Credito.Fch1erPago = Convert.ToDateTime(reader[30]);
                                if (!reader.IsDBNull(31)) Credito.IdFondeo = Convert.ToInt32(reader[31]);
                                if (!reader.IsDBNull(32)) Credito.FchCobro = Convert.ToDateTime(reader[32]);
                                if (!reader.IsDBNull(33)) Credito.EstCobro = Convert.ToInt32(reader[33]);
                                if (!reader.IsDBNull(34)) Credito.PendObs = Convert.ToInt32(reader[34]);
                                if (!reader.IsDBNull(35)) Credito.IdEFondeador = Convert.ToInt32(reader[35]);
                                if (!reader.IsDBNull(36)) Credito.FchLiberado = Convert.ToDateTime(reader[36]);
                                if (!reader.IsDBNull(37)) Credito.IdTipoCobranza = Convert.ToInt32(reader[37]);
                                if (!reader.IsDBNull(38)) Credito.IdCuentaRest = Convert.ToDecimal(reader[38]);
                                if (!reader.IsDBNull(39)) Credito.ClaveTipoInstrumento = reader[39].ToString();
                                if (!reader.IsDBNull(40)) Credito.ControlUltimoPago = Convert.ToDateTime(reader[40]);
                                if (!reader.IsDBNull(41)) Credito.CodVerif = Convert.ToInt16(reader[41]);
                                if (!reader.IsDBNull(42)) Credito.idCliente = Convert.ToInt32(reader[42]);
                                resultado.Add(Credito);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<TBCreditos> ObtenerTBCreditosListado(int Accion, int IdSolicitud, int IdCuenta, int IdCliente, string Cliente, string FechaInicial, string FechaFinal, int IdSucursal, int IdUsuario, int comodin1, string comodin2)
        {
            List<TBCreditos> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.sp_SelCredito"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, IdCuenta);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, IdSolicitud);
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, IdCliente);
                    db.AddInParameter(com, "@Cliente", DbType.String, Cliente);
                    db.AddInParameter(com, "@FechaInicial", DbType.String, FechaInicial);
                    db.AddInParameter(com, "@FechaFinal", DbType.String, FechaFinal);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, IdSucursal);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCreditos>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCreditos Credito = new TBCreditos();
                                if (!reader.IsDBNull(0)) Credito.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Credito.IdSolicitud = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) Credito.FchCredito = Convert.ToDateTime(reader[2]);
                                if (!reader.IsDBNull(3)) Credito.Cliente = reader[3].ToString();
                                if (!reader.IsDBNull(4)) Credito.Convenio = reader[4].ToString();
                                if (!reader.IsDBNull(5)) Credito.Linea = reader[5].ToString();
                                if (!reader.IsDBNull(6)) Credito.Producto = reader[6].ToString();
                                if (!reader.IsDBNull(7)) Credito.Estatus = reader[7].ToString();
                                //if (!reader.IsDBNull(4)) Credito.Erogacion = Convert.ToDecimal(reader[4]);
                                if (!reader.IsDBNull(8)) Credito.Capital = Convert.ToDecimal(reader[8]);
                                if (!reader.IsDBNull(9)) Credito.Interes = Convert.ToDecimal(reader[9]);
                                if (!reader.IsDBNull(10)) Credito.IvaIntere = Convert.ToDecimal(reader[10]);
                                if (!reader.IsDBNull(11)) Credito.Promotor = reader[11].ToString();
                                if (!reader.IsDBNull(12)) Credito.Sucursal = reader[12].ToString();
                                if (!reader.IsDBNull(13)) Credito.Analista = reader[13].ToString();
                                if (!reader.IsDBNull(14)) Credito.Pedido = reader[14].ToString();
                                if (!reader.IsDBNull(15)) Credito.isReestructura = Convert.ToInt32(reader[15]);
                                if (!reader.IsDBNull(16)) Credito.FchEstatus = Convert.ToDateTime(reader[16]);
                                if (!reader.IsDBNull(17)) Credito.FchLiberado = Convert.ToDateTime(reader[17]);


                                //if (!reader.IsDBNull(8)) Credito.SdoCapita = Convert.ToDecimal(reader[8]);
                                //if (!reader.IsDBNull(9)) Credito.SdoIntere = Convert.ToDecimal(reader[9]);
                                //if (!reader.IsDBNull(10)) Credito.SdoIvaIn = Convert.ToDecimal(reader[10]);
                                //if (!reader.IsDBNull(11)) Credito.Moratorio = Convert.ToDecimal(reader[11]);
                                //if (!reader.IsDBNull(12)) Credito.IvaMorato = Convert.ToDecimal(reader[12]);
                                //if (!reader.IsDBNull(13)) Credito.SdoPorApl = Convert.ToDecimal(reader[13]);
                                //if (!reader.IsDBNull(14)) Credito.SdoVencido = Convert.ToDecimal(reader[14]);
                                //if (!reader.IsDBNull(15)) Credito.IdEstatus = Convert.ToInt32(reader[15]);

                                //if (!reader.IsDBNull(17)) Credito.IdMotivo = Convert.ToInt32(reader[17]);
                                //if (!reader.IsDBNull(18)) Credito.PorcenIva = Convert.ToInt32(reader[18]);
                                //if (!reader.IsDBNull(19)) Credito.NoRecPag = Convert.ToInt32(reader[19]);
                                //if (!reader.IsDBNull(20)) Credito.DiasVenc = Convert.ToInt32(reader[20]);
                                //if (!reader.IsDBNull(21)) Credito.DiasInac = Convert.ToInt32(reader[21]);
                                //if (!reader.IsDBNull(22)) Credito.DiaMasVe = Convert.ToInt32(reader[22]);
                                //if (!reader.IsDBNull(23)) Credito.EstaAdmin = reader[23].ToString();
                                //if (!reader.IsDBNull(24)) Credito.FchUltPago = Convert.ToDateTime(reader[24]);
                                //if (!reader.IsDBNull(25)) Credito.FchGenerado = Convert.ToDateTime(reader[25]);
                                //if (!reader.IsDBNull(26)) Credito.Mientras = Convert.ToInt32(reader[26]);
                                //if (!reader.IsDBNull(27)) Credito.Reposicion = Convert.ToInt32(reader[27]);
                                //if (!reader.IsDBNull(28)) Credito.IdCuentaReposicion = Convert.ToInt32(reader[28]);
                                //if (!reader.IsDBNull(29)) Credito.GenReposicion = Convert.ToInt32(reader[29]);
                                //if (!reader.IsDBNull(30)) Credito.Fch1erPago = Convert.ToDateTime(reader[30]);
                                //if (!reader.IsDBNull(31)) Credito.IdFondeo = Convert.ToInt32(reader[31]);
                                //if (!reader.IsDBNull(32)) Credito.FchCobro = Convert.ToDateTime(reader[32]);
                                //if (!reader.IsDBNull(33)) Credito.EstCobro = Convert.ToInt32(reader[33]);
                                //if (!reader.IsDBNull(34)) Credito.PendObs = Convert.ToInt32(reader[34]);
                                //if (!reader.IsDBNull(35)) Credito.IdEFondeador = Convert.ToInt32(reader[35]);
                                //if (!reader.IsDBNull(36)) Credito.FchLiberado = Convert.ToDateTime(reader[36]);
                                //if (!reader.IsDBNull(37)) Credito.IdTipoCobranza = Convert.ToInt32(reader[37]);
                                //if (!reader.IsDBNull(38)) Credito.IdCuentaRest = Convert.ToDecimal(reader[38]);
                                //if (!reader.IsDBNull(39)) Credito.ClaveTipoInstrumento = reader[39].ToString();
                                //if (!reader.IsDBNull(40)) Credito.ControlUltimoPago = Convert.ToDateTime(reader[40]);
                                //if (!reader.IsDBNull(41)) Credito.CodVerif = Convert.ToInt16(reader[41]);
                                //if (!reader.IsDBNull(42)) Credito.idCliente = Convert.ToInt32(reader[42]);
                                resultado.Add(Credito);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<DataResultCredito.Usp_SelCredito> ObtenerTBCreditosFecha(int Accion, DateTime fechaIni, DateTime fechaFin, int idPromotor)
        {
            List<DataResultCredito.Usp_SelCredito> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SelCreditosFecha"))
                {
                    //Parametros
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, fechaIni);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, fechaFin);
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdPromotor", DbType.Int32, idPromotor);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<DataResultCredito.Usp_SelCredito>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                DataResultCredito.Usp_SelCredito Credito = new DataResultCredito.Usp_SelCredito();
                                if (!reader.IsDBNull(0)) Credito.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Credito.IdSolicitud = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) Credito.FchCredito = Convert.ToDateTime(reader[2]);
                                if (!reader.IsDBNull(3)) Credito.Cliente = reader[3].ToString();
                                if (!reader.IsDBNull(4)) Credito.convenio = reader[4].ToString();
                                if (!reader.IsDBNull(5)) Credito.producto = reader[5].ToString();
                                if (!reader.IsDBNull(6)) Credito.estatus = reader[6].ToString();
                                if (!reader.IsDBNull(7)) Credito.Capital = Convert.ToDecimal(reader[7]);
                                if (!reader.IsDBNull(8)) Credito.Interes = Convert.ToDecimal(reader[8]);
                                if (!reader.IsDBNull(9)) Credito.IvaIntere = Convert.ToDecimal(reader[9]);
                                if (!reader.IsDBNull(10)) Credito.capitalD = Convert.ToDecimal(reader[10]);
                                if (!reader.IsDBNull(11)) Credito.interesD = Convert.ToDecimal(reader[11]);
                                if (!reader.IsDBNull(12)) Credito.ivaIntereD = Convert.ToDecimal(reader[12]);
                                if (!reader.IsDBNull(13)) Credito.promotor = reader[13].ToString();
                                if (!reader.IsDBNull(14)) Credito.sucursal = reader[14].ToString();
                                if (!reader.IsDBNull(15)) Credito.analista = reader[15].ToString();
                                if (!reader.IsDBNull(16)) Credito.pedido = reader[16].ToString();
                                if (!reader.IsDBNull(17)) Credito.isReestructura = Convert.ToInt32(reader[17].ToString());
                                resultado.Add(Credito);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<HistorialCobranza> ObtenerHistorialCobranza(int idBanco, int TipoConvenio, int Convenio, DateTime FecIni, DateTime Fecfin)
        {
            List<HistorialCobranza> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.HistorialCobranza"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, idBanco);
                    db.AddInParameter(com, "@IdTipoConvenio", DbType.Int32, TipoConvenio);
                    db.AddInParameter(com, "@IdConvneio", DbType.Int32, Convenio);
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, FecIni);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, Fecfin);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<HistorialCobranza>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                HistorialCobranza Historial = new HistorialCobranza();
                                if (!reader.IsDBNull(0)) Historial.Mes = reader[0].ToString();
                                if (!reader.IsDBNull(1)) Historial.Año = reader[1].ToString();
                                if (!reader.IsDBNull(2)) Historial.D1 = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) Historial.D2 = Convert.ToInt32(reader[3]);
                                if (!reader.IsDBNull(4)) Historial.D3 = Convert.ToInt32(reader[4]);
                                if (!reader.IsDBNull(5)) Historial.D4 = Convert.ToInt32(reader[5]);
                                if (!reader.IsDBNull(6)) Historial.D5 = Convert.ToInt32(reader[6]);
                                if (!reader.IsDBNull(7)) Historial.D6 = Convert.ToInt32(reader[7]);
                                if (!reader.IsDBNull(8)) Historial.D7 = Convert.ToInt32(reader[8]);
                                if (!reader.IsDBNull(9)) Historial.D8 = Convert.ToInt32(reader[9]);
                                if (!reader.IsDBNull(10)) Historial.D9 = Convert.ToInt32(reader[10]);
                                if (!reader.IsDBNull(11)) Historial.D10 = Convert.ToInt32(reader[11]);
                                if (!reader.IsDBNull(12)) Historial.D11 = Convert.ToInt32(reader[12]);
                                if (!reader.IsDBNull(13)) Historial.D12 = Convert.ToInt32(reader[13]);
                                if (!reader.IsDBNull(14)) Historial.D13 = Convert.ToInt32(reader[14]);
                                if (!reader.IsDBNull(15)) Historial.D14 = Convert.ToInt32(reader[15]);
                                if (!reader.IsDBNull(16)) Historial.D15 = Convert.ToInt32(reader[16]);
                                if (!reader.IsDBNull(17)) Historial.D16 = Convert.ToInt32(reader[17]);
                                if (!reader.IsDBNull(18)) Historial.D17 = Convert.ToInt32(reader[18]);
                                if (!reader.IsDBNull(19)) Historial.D18 = Convert.ToInt32(reader[19]);
                                if (!reader.IsDBNull(20)) Historial.D19 = Convert.ToInt32(reader[20]);
                                if (!reader.IsDBNull(21)) Historial.D20 = Convert.ToInt32(reader[21]);
                                if (!reader.IsDBNull(22)) Historial.D21 = Convert.ToInt32(reader[22]);
                                if (!reader.IsDBNull(23)) Historial.D22 = Convert.ToInt32(reader[23]);
                                if (!reader.IsDBNull(24)) Historial.D23 = Convert.ToInt32(reader[24]);
                                if (!reader.IsDBNull(25)) Historial.D24 = Convert.ToInt32(reader[25]);
                                if (!reader.IsDBNull(26)) Historial.D25 = Convert.ToInt32(reader[26]);
                                if (!reader.IsDBNull(27)) Historial.D26 = Convert.ToInt32(reader[27]);
                                if (!reader.IsDBNull(28)) Historial.D27 = Convert.ToInt32(reader[28]);
                                if (!reader.IsDBNull(29)) Historial.D28 = Convert.ToInt32(reader[29]);
                                if (!reader.IsDBNull(30)) Historial.D29 = Convert.ToInt32(reader[30]);
                                if (!reader.IsDBNull(31)) Historial.D30 = Convert.ToInt32(reader[31]);
                                if (!reader.IsDBNull(32)) Historial.D31 = Convert.ToInt32(reader[32]);
                                resultado.Add(Historial);
                            }

                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<HistorialCobranza> ObtenerHistorialCobranzaMontos(int idBanco, int TipoConvenio, int Convenio, DateTime FecIni, DateTime Fecfin)
        {
            List<HistorialCobranza> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.HistorialCobranzaMontos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, idBanco);
                    db.AddInParameter(com, "@IdTipoConvenio", DbType.Int32, TipoConvenio);
                    db.AddInParameter(com, "@IdConvneio", DbType.Int32, Convenio);
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, FecIni);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, Fecfin);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<HistorialCobranza>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                HistorialCobranza Historial = new HistorialCobranza();
                                if (!reader.IsDBNull(0)) Historial.Mes = reader[0].ToString();
                                if (!reader.IsDBNull(1)) Historial.Año = reader[1].ToString();
                                if (!reader.IsDBNull(2)) Historial.D1 = Convert.ToDecimal(reader[2]);
                                if (!reader.IsDBNull(3)) Historial.D2 = Convert.ToDecimal(reader[3]);
                                if (!reader.IsDBNull(4)) Historial.D3 = Convert.ToDecimal(reader[4]);
                                if (!reader.IsDBNull(5)) Historial.D4 = Convert.ToDecimal(reader[5]);
                                if (!reader.IsDBNull(6)) Historial.D5 = Convert.ToDecimal(reader[6]);
                                if (!reader.IsDBNull(7)) Historial.D6 = Convert.ToDecimal(reader[7]);
                                if (!reader.IsDBNull(8)) Historial.D7 = Convert.ToDecimal(reader[8]);
                                if (!reader.IsDBNull(9)) Historial.D8 = Convert.ToDecimal(reader[9]);
                                if (!reader.IsDBNull(10)) Historial.D9 = Convert.ToDecimal(reader[10]);
                                if (!reader.IsDBNull(11)) Historial.D10 = Convert.ToDecimal(reader[11]);
                                if (!reader.IsDBNull(12)) Historial.D11 = Convert.ToDecimal(reader[12]);
                                if (!reader.IsDBNull(13)) Historial.D12 = Convert.ToDecimal(reader[13]);
                                if (!reader.IsDBNull(14)) Historial.D13 = Convert.ToDecimal(reader[14]);
                                if (!reader.IsDBNull(15)) Historial.D14 = Convert.ToDecimal(reader[15]);
                                if (!reader.IsDBNull(16)) Historial.D15 = Convert.ToDecimal(reader[16]);
                                if (!reader.IsDBNull(17)) Historial.D16 = Convert.ToDecimal(reader[17]);
                                if (!reader.IsDBNull(18)) Historial.D17 = Convert.ToDecimal(reader[18]);
                                if (!reader.IsDBNull(19)) Historial.D18 = Convert.ToDecimal(reader[19]);
                                if (!reader.IsDBNull(20)) Historial.D19 = Convert.ToDecimal(reader[20]);
                                if (!reader.IsDBNull(21)) Historial.D20 = Convert.ToDecimal(reader[21]);
                                if (!reader.IsDBNull(22)) Historial.D21 = Convert.ToDecimal(reader[22]);
                                if (!reader.IsDBNull(23)) Historial.D22 = Convert.ToDecimal(reader[23]);
                                if (!reader.IsDBNull(24)) Historial.D23 = Convert.ToDecimal(reader[24]);
                                if (!reader.IsDBNull(25)) Historial.D24 = Convert.ToDecimal(reader[25]);
                                if (!reader.IsDBNull(26)) Historial.D25 = Convert.ToDecimal(reader[26]);
                                if (!reader.IsDBNull(27)) Historial.D26 = Convert.ToDecimal(reader[27]);
                                if (!reader.IsDBNull(28)) Historial.D27 = Convert.ToDecimal(reader[28]);
                                if (!reader.IsDBNull(29)) Historial.D28 = Convert.ToDecimal(reader[29]);
                                if (!reader.IsDBNull(30)) Historial.D29 = Convert.ToDecimal(reader[30]);
                                if (!reader.IsDBNull(31)) Historial.D30 = Convert.ToDecimal(reader[31]);
                                if (!reader.IsDBNull(32)) Historial.D31 = Convert.ToDecimal(reader[32]);
                                resultado.Add(Historial);
                            }

                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public TBCreditos.GeneraReferenciaCIE GeneraReferenciaCIE(DateTime FechaAutoriza, int idsolicitud, int idusuario, int origen)
        {
            TBCreditos.GeneraReferenciaCIE resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("creditos.GeneraMontoReferenciaCIE"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@FechaAutoriza", DbType.DateTime, FechaAutoriza);
                    db.AddInParameter(com, "@idsolicitud", DbType.Int32, idsolicitud);
                    db.AddInParameter(com, "@idusuario", DbType.Int32, idusuario);
                    db.AddInParameter(com, "@origen", DbType.Int32, origen);

                    //Ejecucion
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new TBCreditos.GeneraReferenciaCIE();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCreditos.GeneraReferenciaCIE RefCIE = new TBCreditos.GeneraReferenciaCIE();
                                if (!reader.IsDBNull(0)) RefCIE.ErrMensaje = Convert.ToString(reader[0]);
                                if (!reader.IsDBNull(1)) RefCIE.ReferenciaCompleta = Convert.ToString(reader[1]);
                                if (!reader.IsDBNull(2)) RefCIE.DigVerificador = Convert.ToString(reader[2]);

                                resultado = RefCIE;
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public Resultado<TBCreditos.AmpliacionEntity> ObtenerInfoAmpliacion(int IdSolicitud, int Accion)
        {
            Resultado<TBCreditos.AmpliacionEntity> objReturn = new Resultado<TBCreditos.AmpliacionEntity>();
            objReturn.ResultObject = null;

            try
            {
                // Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_SelCredito"))
                {
                    // Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, IdSolicitud);

                    // Ejecucion
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {                            
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                objReturn.ResultObject = new TBCreditos.AmpliacionEntity();

                                objReturn.ResultObject.IdSolicitud = Utils.GetInt(reader, "IdSolicitud");
                                objReturn.ResultObject.IdSolicitudAmplia = Utils.GetInt(reader, "IdSolicitudAmplia");
                                objReturn.ResultObject.IdCuenta = Utils.GetInt(reader, "IdCuenta");
                                objReturn.ResultObject.FechaLiquidar = Utils.GetDateTime(reader, "FechaLiquidar");
                                objReturn.ResultObject.CapitalLiquidar = Utils.GetDecimal(reader, "CapitalLiquidar");
                                objReturn.ResultObject.InteresLiquidar = Utils.GetDecimal(reader, "InteresLiquidar");
                                objReturn.ResultObject.IGVLiquidar = Utils.GetDecimal(reader, "IGVLiquidar");
                                objReturn.ResultObject.SeguroLiquidar = Utils.GetDecimal(reader, "SeguroLiquidar");
                                objReturn.ResultObject.GATLiquidar = Utils.GetDecimal(reader, "GATLiquidar");
                                objReturn.ResultObject.TotalLiquidar = Utils.GetDecimal(reader, "TotalLiquidar");                                
                                objReturn.ResultObject.ComentariosSistema = Utils.GetString(reader, "ComentariosSistema");
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public Resultado<TBCreditos.CalculoLiquidacion> CalcularLiquidacion(TBCreditos.Liquidacion eOwner)
        {
            Resultado<TBCreditos.CalculoLiquidacion> objReturn = new Resultado<TBCreditos.CalculoLiquidacion>();
            DataSet ds = new DataSet();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_ABCSolicitud_Liquidacion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "CALC_LIQUIDACION"); // Calcular la Liquidacion del Credito
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, eOwner.IdSolicitud.GetValueOrDefault());

                    if (eOwner.FechaLiquidar.Value != null)
                    {
                        db.AddInParameter(com, "@FechaLiquidacion", DbType.DateTime, eOwner.FechaLiquidar.Value.GetValueOrDefault());
                    }                    

                    //Ejecucion de la Consulta                              
                    ds = db.ExecuteDataSet(com);

                    // Obtener los datos de la Liquidacion
                    foreach (DataRow r in ds.Tables[0].Rows)
                    {
                        objReturn.ResultObject.IdSolicitud = int.Parse(r["IdSolicitud"].ToString());
                        objReturn.ResultObject.FechaCredito = Utils.GetDateTime(r, "Fch_Credito").GetValueOrDefault();
                        objReturn.ResultObject.Cuota = decimal.Parse(r["Erogacion"].ToString());
                        objReturn.ResultObject.DNICliente = r["DNICliente"].ToString();
                        objReturn.ResultObject.NombreCliente = r["NombreCliente"].ToString();
                        objReturn.ResultObject.ApPaternoCliente = r["ApellidoPaterno"].ToString();
                        objReturn.ResultObject.ApMaternoCliente = r["ApellidoMaterno"].ToString();
                        objReturn.ResultObject.MontoCredito = decimal.Parse(r["MontoCredito"].ToString());
                        objReturn.ResultObject.DireccionCliente = r["DireccionCliente"].ToString();
                        objReturn.ResultObject.TasaAnual = decimal.Parse(r["TasaAnual"].ToString());
                        objReturn.ResultObject.FechaLiquidacion.Value = Utils.GetDateTime(r, "FechaLiquidacion").GetValueOrDefault();
                        objReturn.ResultObject.FechaPrimerPago = Utils.GetDateTime(r, "FechaPrimerPago").GetValueOrDefault();
                        objReturn.ResultObject.FechaUltimoPago = Utils.GetDateTime(r, "FechaUltimoPago").GetValueOrDefault();
                        objReturn.ResultObject.TotalLiquidacion = decimal.Parse(r["TotalLiquidacion"].ToString());
                        objReturn.ResultObject.MontoComision = decimal.Parse(r["MontoComision"].ToString());
                        objReturn.ResultObject.Plazo = int.Parse(r["Plazo"].ToString());
                    }

                    // Obtener los Recibos de la Liquidacion
                    TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle detalleLiquidacion;
                    foreach (DataRow r in ds.Tables[1].Rows)
                    {
                        detalleLiquidacion = new TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle();

                        detalleLiquidacion.Recibo = int.Parse(r["Recibo"].ToString());
                        detalleLiquidacion.FechaRecibo = Utils.GetDateTime(r, "Fch_Recibo").GetValueOrDefault();
                        detalleLiquidacion.TipoReciboCalculo = r["TipoRecibo"].ToString();
                        detalleLiquidacion.LiquidaCapital = decimal.Parse(r["Liquida_Capital"].ToString());
                        detalleLiquidacion.LiquidaInteres = decimal.Parse(r["Liquida_Interes"].ToString());
                        detalleLiquidacion.LiquidaIGV = decimal.Parse(r["Liquida_IGV"].ToString());
                        detalleLiquidacion.LiquidaSeguro = decimal.Parse(r["Liquida_Seguro"].ToString());
                        detalleLiquidacion.LiquidaGAT = decimal.Parse(r["Liquida_GAT"].ToString());
                        detalleLiquidacion.LiquidaTotal = decimal.Parse(r["Liquida_Total"].ToString());
                        detalleLiquidacion.LiquidaOtrosCargos = decimal.Parse(r["Liquida_OtrosCargos"].ToString());
                        detalleLiquidacion.LiquidaIGVOtrosCargos = decimal.Parse(r["Liquida_IGVOtrosCargos"].ToString());

                        objReturn.ResultObject.DetalleLiquidacion.Add(detalleLiquidacion);
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public Resultado<TBCreditos.Liquidacion> RegistrarLiquidacion(TBCreditos.Liquidacion eOwner)
        {
            Resultado <TBCreditos.Liquidacion> objReturn = new Resultado<TBCreditos.Liquidacion>();
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_ABCSolicitud_Liquidacion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "INS_LIQUIDACION"); // Registrar la Liquidacion del Credito
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, eOwner.IdUsuarioRegistro);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, eOwner.IdSolicitud.GetValueOrDefault());
                    db.AddInParameter(com, "@FechaLiquidacion", DbType.DateTime, eOwner.FechaLiquidar.Value.GetValueOrDefault());
                    
                    //Ejecucion de la Consulta
                    objReturn.ResultObject = LiquidacionToObjFromDB(db.ExecuteDataSet(com));

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;                    
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public TBCreditos.Liquidacion ObtenerLiquidacionCredito(TBCreditos.Liquidacion eOwner)
        {
            TBCreditos.Liquidacion objReturn = null;
            
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_ABCSolicitud_Liquidacion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_LIQUIDACION_CREDITO"); // Obtener la Liquidacion con la que se liquido un Credito                    
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, eOwner.IdSolicitud.GetValueOrDefault());

                    //Ejecucion de la Consulta
                    objReturn = LiquidacionToObjFromDB(db.ExecuteDataSet(com));

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public TBCreditos.Liquidacion ObtenerLiquidacionActiva(TBCreditos.Liquidacion eOwner)
        {
            TBCreditos.Liquidacion objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_ABCSolicitud_Liquidacion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "GET_LIQUIDACION_ACTIVA"); // Obtener la Liquidacion Activa del Credito                    
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, eOwner.IdSolicitud.GetValueOrDefault());

                    //Ejecucion de la Consulta
                    objReturn = LiquidacionToObjFromDB(db.ExecuteDataSet(com));

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }            
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public Resultado<TBCreditos.Liquidacion> ConfirmarLiquidacion(TBCreditos.Liquidacion eOwner)
        {
            Resultado<TBCreditos.Liquidacion> objReturn = new Resultado<TBCreditos.Liquidacion>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_ABCSolicitud_Liquidacion"))
                {
                    //Parametros                                      
                    db.AddInParameter(com, "@Accion", DbType.String, "UPD_CONFIRMAR_LIQUIDACION"); // Confirmar la Liquidacion Activa del Credito                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, eOwner.IdUsuarioConfirma);
                    db.AddInParameter(com, "@IdLiquidacion", DbType.Int32, eOwner.IdLiquidacion.GetValueOrDefault());
                    db.AddInParameter(com, "@NumeroTransaccion", DbType.String, eOwner.NumeroTransaccion);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public Resultado<TBCreditos.Liquidacion> CancelarLiquidacion(TBCreditos.Liquidacion eOwner)
        {
            Resultado<TBCreditos.Liquidacion> objReturn = new Resultado<TBCreditos.Liquidacion>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_ABCSolicitud_Liquidacion"))
                {
                    //Parametros                                    
                    db.AddInParameter(com, "@Accion", DbType.String, "UPD_CANCELAR_LIQUIDACION"); // Cancelar la Liquidacion Activa del Credito                    
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, eOwner.IdUsuarioCancela);
                    db.AddInParameter(com, "@IdLiquidacion", DbType.Int32, eOwner.IdLiquidacion.GetValueOrDefault());

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public List<TBCreditos.Finiquitos> ObtenerTBCreditosFiniquitos(int Accion, int Cuenta, int Usuario)
        {
            List<TBCreditos.Finiquitos> resultado = null;
            
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.Sel_Credito"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@Cuenta", DbType.Int32, Cuenta);
                    db.AddInParameter(com, "@Usuario", DbType.Int32, Usuario);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCreditos.Finiquitos >();

                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCreditos.Finiquitos Finiquito = new TBCreditos.Finiquitos();
                                if (!reader.IsDBNull(0)) Finiquito.IdSolicitud = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Finiquito.IdCliente = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) Finiquito.IdCuenta = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) Finiquito.NombreCompleto = Convert.ToString(reader[3]);
                                if (!reader.IsDBNull(4)) Finiquito.MontoUltPago = Convert.ToDecimal(reader[4]);
                                if (!reader.IsDBNull(5)) Finiquito.FecUltPago = Convert.ToDateTime(reader[5]);
                                if (!reader.IsDBNull(6)) Finiquito.TipoLiqui = Convert.ToString(reader[6]);
                                if (!reader.IsDBNull(7)) Finiquito.IdEstatus = Convert.ToInt32(reader[7]);
                                if (!reader.IsDBNull(8)) Finiquito.CantLetraUltPago = Convert.ToString(reader[8]);
                                if (!reader.IsDBNull(9)) Finiquito.IdSolRees = Convert.ToInt32(reader[9]);
                                if (!reader.IsDBNull(10)) Finiquito.FecIniSolRees = Convert.ToDateTime(reader[10]);
                                if (!reader.IsDBNull(11)) Finiquito.MontoSolRees = Convert.ToDecimal(reader[11]);
                                if (!reader.IsDBNull(12)) Finiquito.SdoLiquiRees = Convert.ToDecimal(reader[12]);
                                if (!reader.IsDBNull(13)) Finiquito.CantLetraSdoLiquiRees = Convert.ToString(reader[13]);
                                
                                resultado.Add(Finiquito);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public DataResultCredito.Usp_SelDatosPago ObtenerDatosPago(int idProdutco,   decimal capital, int TipoCredito, decimal deducciones, decimal ingreso)
       {
           DataResultCredito.Usp_SelDatosPago resultado = null;

           try
           {
               //Obtener DbCommand para ejcutar el Store Procedure
               using (DbCommand com = db.GetStoredProcCommand("Creditos.CalPago"))
               {
                   //Parametros
                   db.AddInParameter(com, "@idproducto", DbType.Int32, idProdutco);
                   db.AddInParameter(com, "@capital", DbType.Decimal, capital);
                   db.AddInParameter(com, "@TipoCredito", DbType.Int32, TipoCredito);
                   db.AddInParameter(com, "@Ingreso", DbType.Decimal, ingreso);
                   db.AddInParameter(com, "@Deducciones", DbType.Decimal, deducciones);

                   //Ejecucion de la Consulta
                   using (IDataReader reader = db.ExecuteReader(com))
                   {
                       if (reader != null)
                       {
                           resultado = new DataResultCredito.Usp_SelDatosPago();

                           //Lectura de los datos del ResultSet
                           while (reader.Read())
                           {
                               if (!reader.IsDBNull(0)) resultado.Erogacion = Convert.ToDecimal(reader[0]);
                               if (!reader.IsDBNull(1)) resultado.TotalPagar = Convert.ToDecimal(reader[1]);
                               if (!reader.IsDBNull(2)) resultado.Fecha = Convert.ToDateTime(reader[2]);
                               if (!reader.IsDBNull(3)) resultado.MontoMaximo = Convert.ToDecimal(reader[3]); 
                           }
                       }

                       reader.Dispose();
                   }

                   //Cierre de la conexion y liberacion de memoria
                   com.Dispose();
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return resultado;
        }

        public void ActualizarTBCreditos()
        {
        }

        public void EliminarTBCreditos()
        {
        }

        public void ActualizarCreditosRenovacion(int Accion, int idsolicitud, int idcuenta, decimal LiqCapital, decimal InteresDev, int idcliente)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.ABC_CreditosRenovacion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@idsolicitud", DbType.Int32, idsolicitud);
                    db.AddInParameter(com, "@idcuenta", DbType.Int32, idcuenta);
                    db.AddInParameter(com, "@LiqCapita", DbType.Decimal, LiqCapital);
                    db.AddInParameter(com, "@InteresDev", DbType.Decimal, InteresDev);
                    db.AddInParameter(com, "@idcliente", DbType.Int32, idcliente);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Resultado<TBCreditos.Liquidacion> ConfirmarLiquidacionManual(TBCreditos.Liquidacion eOwner)
        {
            Resultado<TBCreditos.Liquidacion> objReturn = new Resultado<TBCreditos.Liquidacion>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_ABCSolicitud_Liquidacion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.String, "UPD_CONFIRMAR_LIQUIDACION_MANUAL"); // Confirmar la Liquidacion Activa del Credito
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, eOwner.IdUsuarioConfirma);
                    db.AddInParameter(com, "@IdPago", DbType.String, eOwner.IdPago.GetValueOrDefault());

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }
        #endregion

        #region MISC Methods
        public List<TBCreditos.CreditoRelacionado> ObtenerCreditosRelacionados(int Accion,  int idcliente)
        {
            List<TBCreditos.CreditoRelacionado> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SelCreditosRelacionados"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@idcliente", DbType.Int32, idcliente);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCreditos.CreditoRelacionado>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCreditos.CreditoRelacionado creditorelacinado = new TBCreditos.CreditoRelacionado();
                                if (!reader.IsDBNull(0)) creditorelacinado.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) creditorelacinado.IdSolicitud = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) creditorelacinado.capital = Convert.ToDecimal(reader[2]);
                                if (!reader.IsDBNull(3)) creditorelacinado.interes = Convert.ToDecimal(reader[3]);
                                if (!reader.IsDBNull(4)) creditorelacinado.iva = Convert.ToDecimal(reader[4]);
                                if (!reader.IsDBNull(5)) creditorelacinado.sdo_periodo = Convert.ToDecimal(reader[5]);
                                if (!reader.IsDBNull(6)) creditorelacinado.TotalRenovar = Convert.ToDecimal(reader[6]);
                                resultado.Add(creditorelacinado);
                            }

                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<TBCreditos.CreditoRelacionado> ObtenerCreditosRelacionadosBySol(int Accion, int idsolicitud)
        {
            List<TBCreditos.CreditoRelacionado> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_SelCreditoReesBySol"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdSolicitudRees", DbType.Int32, idsolicitud);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<TBCreditos.CreditoRelacionado>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCreditos.CreditoRelacionado creditorelacinado = new TBCreditos.CreditoRelacionado();
                                if (!reader.IsDBNull(0)) creditorelacinado.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) creditorelacinado.IdSolicitud = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) creditorelacinado.capital = Convert.ToDecimal(reader[3]);
                                if (!reader.IsDBNull(3)) creditorelacinado.interes = Convert.ToDecimal(reader[4]);
                                if (!reader.IsDBNull(4)) creditorelacinado.iva = Convert.ToDecimal(reader[5]);
                                if (!reader.IsDBNull(5)) creditorelacinado.sdo_periodo = Convert.ToDecimal(reader[7]);
                                if (!reader.IsDBNull(6)) creditorelacinado.TotalRenovar = Convert.ToDecimal(reader[8]);
                                resultado.Add(creditorelacinado);
                            }

                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public Resultado<ObtenerSimulacionCreditoResponse> ObtenerSimulacionCredito(ObtenerSimulacionCreditoRequest entity)
        {
            Resultado<ObtenerSimulacionCreditoResponse> res = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Solicitudes.SimuladorCredito"))
                {
                    db.AddInParameter(com, "@Producto", DbType.Int32, entity.IdProducto);
                    db.AddInParameter(com, "@Importe", DbType.Decimal, entity.Importe);
                    db.AddInParameter(com, "@Plazo", DbType.Int32, entity.Plazo);
                    db.AddInParameter(com, "@TipoSeguro", DbType.Int32, entity.TipoSeguro);
                    db.AddInParameter(com, "@Banco", DbType.Int32, entity.Banco);
                    db.AddInParameter(com, "@Interes", DbType.Decimal, entity.Interes);
                    db.AddInParameter(com, "@CalcularIntFechas", DbType.Int32, (entity.CalcularIntFechas > 0) ? entity.CalcularIntFechas : (object)DBNull.Value);
                    db.AddInParameter(com, "@FechaDesembolso", DbType.String, (!string.IsNullOrEmpty(entity.FechaDesembolso) ? entity.FechaDesembolso : (object)DBNull.Value));
                    db.ExecuteNonQuery(com);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            res = new Resultado<ObtenerSimulacionCreditoResponse>();
                            while (reader.Read())
                            {
                                CronogramaPagos.Recibo r = new CronogramaPagos.Recibo();
                                r.NoRecibo = Utils.GetString(reader, "Recibo");
                                r.FechaRecibo = Utils.GetDateTime(reader, "Fecha Pago", "dd/MM/yyyy");
                                r.CapitalInicial = Utils.GetDecimal(reader, "Capital Inicial");
                                r.Capital = Utils.GetDecimal(reader, "Capital");
                                r.Interes = Utils.GetDecimal(reader, "Interes");
                                r.IGV = Utils.GetDecimal(reader, "IGV");
                                r.Seguro = Utils.GetDecimal(reader, "Seguro");
                                r.GAT = Utils.GetDecimal(reader, "GAT");
                                r.Cuota = Utils.GetDecimal(reader, "Cuota");
                                r.SaldoCapital = Utils.GetDecimal(reader, "Saldo Capital");
                                res.ResultObject.Resultado.Add(r);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }

        public Resultado<List<ConsultaExpediente>> ConsultaExpediente(string Busqueda)
        {
            Resultado<List<ConsultaExpediente>> res = null;

            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_ConsultaExpedienteCON"))
                {
                    db.AddInParameter(com, "@Buscar", DbType.String, Busqueda);                    
                    db.ExecuteNonQuery(com);

                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            res = new Resultado<List<ConsultaExpediente>>();

                            while (reader.Read())
                            {
                                ConsultaExpediente ce = new ConsultaExpediente();

                                ce.IdSolicitud = Utils.GetInt(reader, "IdSolicitud");
                                ce.FechaSolicitud.Value = Utils.GetDateTime(reader, "FechaSolicitud", "dd/MM/yyyy");
                                ce.NombreCliente = Utils.GetString(reader, "NombreCliente");
                                ce.ApellidoPaterno = Utils.GetString(reader, "ApellidoPaterno");
                                ce.ApellidoMaterno = Utils.GetString(reader, "ApellidoMaterno");
                                ce.DNI = Utils.GetString(reader, "DNI");
                                ce.GDriveID = Utils.GetString(reader, "GDriveID");

                                res.ResultObject.Add(ce);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }

        public TipoCreditoCliente ObtenerTipoCreditoCliente(int idCliente)
        {
            TipoCreditoCliente resultado = new TipoCreditoCliente();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_TipoCreditoClientePRO"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, idCliente);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            if (reader.Read())
                            {
                                resultado.IdTipoCredito = Utils.GetInt(reader, "IdTipoCredito");
                                resultado.Credito = Utils.GetString(reader, "Credito");
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resultado;
        }
        #endregion

        #region "Private Methods"
        private TBCreditos.Liquidacion LiquidacionToObjFromDB(DataSet ds)
        {
            TBCreditos.Liquidacion objReturn = null;
            DataTable dtCurrent = new DataTable();

            // Cargar los datos principales de la Liquidacion
            if (ds.Tables.Count > 0)
            {                
                dtCurrent = ds.Tables[0];

                foreach (DataRow r in dtCurrent.Rows)
                {
                    objReturn = new TBCreditos.Liquidacion();

                    objReturn.IdLiquidacion = Utils.GetInt(r, "IdLiquidacion");
                    objReturn.IdSolicitud = Utils.GetInt(r, "IdSolicitud");
                    objReturn.IdPago = Utils.GetInt(r, "IdPago");
                    objReturn.NumeroTransaccion = Utils.GetString(r, "NumeroTransaccion");
                    objReturn.Estatus.IdEstatus = Utils.GetInt(r, "IdEstatus");
                    objReturn.Estatus.EstatusClave = Utils.GetString(r, "ClaveEstatus");
                    objReturn.Estatus.EstatusDesc = Utils.GetString(r, "EstatusDescripcion");
                    objReturn.FechaLiquidar.Value = Utils.GetDateTime(r, "FechaLiquidar");
                    objReturn.CapitalLiquidar = Utils.GetDecimal(r, "CapitalLiquidar");
                    objReturn.InteresLiquidar = Utils.GetDecimal(r, "InteresLiquidar");
                    objReturn.IGVLiquidar = Utils.GetDecimal(r, "IGVLiquidar");
                    objReturn.SeguroLiquidar = Utils.GetDecimal(r, "SeguroLiquidar");
                    objReturn.GATLiquidar = Utils.GetDecimal(r, "GATLiquidar");
                    objReturn.TotalLiquidar = Utils.GetDecimal(r, "TotalLiquidar");
                    objReturn.IdUsuarioRegistro = Utils.GetInt(r, "IdUsuarioRegistro");
                    objReturn.IdUsuarioConfirma = Utils.GetInt(r, "IdUsuarioConfirma");
                    objReturn.IdUsuarioCancela = Utils.GetInt(r, "IdUsuarioCancela");
                    objReturn.FechaConfirmacion.Value = Utils.GetDateTime(r, "FechaConfirmacion");
                    objReturn.FechaCancelacion.Value = Utils.GetDateTime(r, "FechaCancelacion");
                    objReturn.FechaRegistro.Value = Utils.GetDateTime(r, "FechaRegistro");
                }
            }

            // Cargar el Detalle de la Liquidacion
            if (ds.Tables.Count > 1)
            {
                TBCreditos.Liquidacion.LiquidacionDetalle c;
                dtCurrent = ds.Tables[1];

                foreach (DataRow r in dtCurrent.Rows)
                {
                    c = new TBCreditos.Liquidacion.LiquidacionDetalle();

                    c.IdLiquidacion = Utils.GetInt(r, "IdLiquidacion");
                    c.IdSolicitud = Utils.GetInt(r, "IdSolicitud");
                    c.IdCuenta = Utils.GetInt(r, "IdCuenta");
                    c.Recibo = Utils.GetInt(r, "Recibo");
                    c.TipoReciboCalculo = Utils.GetString(r, "TipoRecibo");
                    c.DayDiffVenc = Utils.GetInt(r, "ddDiffVenc");
                    c.EstatusRecibo.IdEstatus = Utils.GetInt(r, "IdEstatus");
                    c.FechaRecibo = Utils.GetDateTime(r, "Fch_Recibo");
                    c.CapitalInicial = Utils.GetDecimal(r, "CapitalInicial");
                    c.SaldoCapital = Utils.GetDecimal(r, "Sdo_Capital");
                    c.SaldoInteres = Utils.GetDecimal(r, "Sdo_Interes");
                    c.SaldoIGV = Utils.GetDecimal(r, "Sdo_IGV");
                    c.SaldoSeguro = Utils.GetDecimal(r, "Sdo_Seguro");
                    c.SaldoGAT = Utils.GetDecimal(r, "Sdo_GAT");
                    c.LiquidaCapital = Utils.GetDecimal(r, "Liquida_Capital");
                    c.LiquidaInteres = Utils.GetDecimal(r, "Liquida_Interes");
                    c.LiquidaIGV = Utils.GetDecimal(r, "Liquida_IGV");
                    c.LiquidaSeguro = Utils.GetDecimal(r, "Liquida_Seguro");
                    c.LiquidaGAT = Utils.GetDecimal(r, "Liquida_GAT");

                    objReturn.Detalle.Add(c);
                }
            }

            return objReturn;
        }
        #endregion
    }
}
