﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using  SOPF.Core.Entities.Creditos;
namespace SOPF.Core.DataAccess.Creditos
{
    public class CompDispersionDal
    {
        #region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
        public CompDispersionDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion

        public List<DataResultCredito.Usp_Error> VerificarDiespersion(int idCuenta, decimal monto)
        {
            List<DataResultCredito.Usp_Error> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.VerificaDispersion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, idCuenta);
                    db.AddInParameter(com, "@Monto", DbType.Int32, monto);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<DataResultCredito.Usp_Error>();
                            while (reader.Read())
                            {
                                DataResultCredito.Usp_Error error = new DataResultCredito.Usp_Error();
                                if (!reader.IsDBNull(0)) error.Error = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) error.Mensaje = reader[1].ToString();
                                
                                resultado.Add(error);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
    }
}
