﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Creditos;

namespace SOPF.Core.DataAccess.Creditos
{
    public class PromoRetencionesDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
        public PromoRetencionesDal()
		{
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods


        public List<PromoRetenciones> ObtenerPromoRetenciones(int Accion, int IdCliente, int IdSucursal, int IdCuenta, int IdUsuario)
        {
			List<PromoRetenciones> resultado = null;
			try
			{
				//Obtener DbCommand para ejecutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.ComportamientoCliente"))
				{
					//Parametros
					db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdCliente", DbType.Int32, IdCliente);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, IdSucursal);
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, IdCuenta);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    //db.AddInParameter(com, "@Validacion", DbType.Int32, Validacion);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<PromoRetenciones>();
                            while (reader.Read())
                            {
                                PromoRetenciones Promo = new PromoRetenciones();
                                if (!reader.IsDBNull(0)) Promo.Validacion = Convert.ToInt32(reader[0]); ;
                                if (!reader.IsDBNull(1)) Promo.Mensaje = reader[1].ToString();
                                resultado.Add(Promo);
                            }

						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        

        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}