﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Creditos;

namespace SOPF.Core.DataAccess.Creditos
{
    public class ClientesAfiliacionDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public ClientesAfiliacionDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
        public List<ClientesAfiliacion> CuentasAfiliar(int Accion)
        {
            List<ClientesAfiliacion> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.AfiliacionCuentas"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 1);
                    


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<ClientesAfiliacion>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                ClientesAfiliacion afiliacion = new ClientesAfiliacion();
                                if (!reader.IsDBNull(0)) afiliacion.ClaveId = reader[0].ToString();
                                if (!reader.IsDBNull(1)) afiliacion.Nombre = reader[1].ToString();
                                if (!reader.IsDBNull(2)) afiliacion.RFC = reader[2].ToString();
                                if (!reader.IsDBNull(3)) afiliacion.Telefono = reader[3].ToString();
                                if (!reader.IsDBNull(4)) afiliacion.Moneda = reader[4].ToString();
                                if (!reader.IsDBNull(5)) afiliacion.Banco = reader[5].ToString();
                                if (!reader.IsDBNull(6)) afiliacion.Clabe = reader[6].ToString();
                                if (!reader.IsDBNull(7)) afiliacion.Idcliente = Convert.ToInt32(reader[7]);
                                if (!reader.IsDBNull(8)) afiliacion.Idclabecliente = Convert.ToInt32(reader[8]);
                                if (!reader.IsDBNull(9)) afiliacion.Idbanco = Convert.ToInt32(reader[9]);
                                if (!reader.IsDBNull(10)) afiliacion.TipoCuenta = Convert.ToString(reader[10]);
                                //if (!reader.IsDBNull(11)) afiliacion.ClienteExistente = Convert.ToInt32(reader[11]);
                                resultado.Add(afiliacion);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        public List<ClientesAfiliacion> CuentasAfiliarBancomer(int Accion)
        {
            List<ClientesAfiliacion> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.AfiliacionCuentasBancomer"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 1);



                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<ClientesAfiliacion>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                ClientesAfiliacion afiliacion = new ClientesAfiliacion();
                                if (!reader.IsDBNull(0)) afiliacion.ClaveId = reader[0].ToString();
                                if (!reader.IsDBNull(1)) afiliacion.Nombre = reader[1].ToString();
                                if (!reader.IsDBNull(2)) afiliacion.RFC = reader[2].ToString();
                                if (!reader.IsDBNull(3)) afiliacion.Telefono = reader[3].ToString();
                                if (!reader.IsDBNull(4)) afiliacion.Moneda = reader[4].ToString();
                                if (!reader.IsDBNull(5)) afiliacion.Banco = reader[5].ToString();
                                if (!reader.IsDBNull(6)) afiliacion.Clabe = reader[6].ToString();
                                if (!reader.IsDBNull(7)) afiliacion.Idcliente = Convert.ToInt32(reader[7]);
                                if (!reader.IsDBNull(8)) afiliacion.Idclabecliente = Convert.ToInt32(reader[8]);
                                if (!reader.IsDBNull(9)) afiliacion.Idbanco = Convert.ToInt32(reader[9]);
                                if (!reader.IsDBNull(10)) afiliacion.TipoCuenta = Convert.ToString(reader[10]);
                                //if (!reader.IsDBNull(11)) afiliacion.ClienteExistente = Convert.ToInt32(reader[11]);
                                resultado.Add(afiliacion);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        public List<ClientesAfiliacion> CuentasAfiliarRec(int Accion)
        {
            List<ClientesAfiliacion> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.AfiliacionCuentasRec"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, 1);



                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<ClientesAfiliacion>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                ClientesAfiliacion afiliacion = new ClientesAfiliacion();
                                if (!reader.IsDBNull(0)) afiliacion.ClaveId = reader[0].ToString();
                                if (!reader.IsDBNull(1)) afiliacion.Nombre = reader[1].ToString();
                                if (!reader.IsDBNull(2)) afiliacion.RFC = reader[2].ToString();
                                if (!reader.IsDBNull(3)) afiliacion.Telefono = reader[3].ToString();
                                if (!reader.IsDBNull(4)) afiliacion.Moneda = reader[4].ToString();
                                if (!reader.IsDBNull(5)) afiliacion.Banco = reader[5].ToString();
                                if (!reader.IsDBNull(6)) afiliacion.Clabe = reader[6].ToString();
                                if (!reader.IsDBNull(7)) afiliacion.Idcliente = Convert.ToInt32(reader[7]);
                                if (!reader.IsDBNull(8)) afiliacion.Idclabecliente = Convert.ToInt32(reader[8]);
                                if (!reader.IsDBNull(9)) afiliacion.Idbanco = Convert.ToInt32(reader[9]);
                                if (!reader.IsDBNull(10)) afiliacion.TipoCuenta = Convert.ToString(reader[10]);
                                //if (!reader.IsDBNull(11)) afiliacion.ClienteExistente = Convert.ToInt32(reader[11]);
                                resultado.Add(afiliacion);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
    }

}
