﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Creditos;

namespace SOPF.Core.DataAccess.Creditos
{
    public class LotesImpresionDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public LotesImpresionDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public void InsertarLote(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdEstatus, int IdFolio)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_ABCLotesImpresion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@FechaAlta", DbType.DateTime, FechaAlta);
                    db.AddInParameter(com, "@IdUsuarioAlta", DbType.Int32, IdUsuarioAlta);
                    db.AddInParameter(com, "@IdFormato", DbType.Int32, IdFormato);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, IdSucursal);
                    db.AddInParameter(com, "@FolioInicial", DbType.Int32, FolioInicial);
                    db.AddInParameter(com, "@FolioFinal", DbType.Int32, FolioFinal);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, IdEstatus);
                    db.AddInParameter(com, "@IdFolio", DbType.Int32, IdFolio);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarLote(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdEstatus, int IdFolio)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_ABCLotesImpresion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@FechaAlta", DbType.DateTime, FechaAlta);
                    db.AddInParameter(com, "@IdUsuarioAlta", DbType.Int32, IdUsuarioAlta);
                    db.AddInParameter(com, "@IdFormato", DbType.Int32, IdFormato);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, IdSucursal);
                    db.AddInParameter(com, "@FolioInicial", DbType.Int32, FolioInicial);
                    db.AddInParameter(com, "@FolioFinal", DbType.Int32, FolioFinal);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, IdEstatus);
                    db.AddInParameter(com, "@IdFolio", DbType.Int32, IdFolio);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<LotesImpresion> ConsultarLotesImpresion (int Accion, int IdLote, DateTime Fecha, int IdSucursal, int IdUsuario, int FolioInicial, int FolioFinal, int IdEstatus, int IdFormato)
        {
            List<LotesImpresion> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelLotesImpresion"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdLote", DbType.Int32, IdLote);
                    db.AddInParameter(com, "@Fecha", DbType.DateTime, Fecha);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, IdSucursal);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@FolioInicial", DbType.Int32, FolioInicial);
                    db.AddInParameter(com, "@FolioFinal", DbType.Int32, FolioFinal);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, IdEstatus);
                    db.AddInParameter(com, "@IdFormato", DbType.Int32, IdFormato); 
                    

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<LotesImpresion>();

                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                LotesImpresion Lotes = new LotesImpresion();

                                if (Accion == 0)
                                {
                                    if (!reader.IsDBNull(0)) Lotes.IdLote = Convert.ToInt32(reader[0]);
                                    if (!reader.IsDBNull(1)) Lotes.Formato = Convert.ToString(reader[1]);
                                    if (!reader.IsDBNull(2)) Lotes.FolioInicial = Convert.ToInt32(reader[2]);
                                    if (!reader.IsDBNull(3)) Lotes.FolioFinal = Convert.ToInt32(reader[3]);
                                    if (!reader.IsDBNull(4)) Lotes.IdEstatus  = Convert.ToInt32(reader[4]);
                                    if (!reader.IsDBNull(5)) Lotes.EstatusLote = Convert.ToString(reader[5]);
                                }

                                if (Accion == 1)
                                {
                                    if (!reader.IsDBNull(0)) Lotes.FolioFinal = Convert.ToInt32(reader[0]);
                                    if (!reader.IsDBNull(1)) Lotes.Cantidad = Convert.ToInt32(reader[1]);
                                }

                                if (Accion == 2)
                                {
                                    if (!reader.IsDBNull(0)) Lotes.IdLote = Convert.ToInt32(reader[0]);
                                    if (!reader.IsDBNull(1)) Lotes.Formato = Convert.ToString(reader[1]);
                                    if (!reader.IsDBNull(2)) Lotes.Fecha = Convert.ToDateTime(reader[2]);
                                    if (!reader.IsDBNull(3)) Lotes.FolioInicial = Convert.ToInt32(reader[3]);
                                    if (!reader.IsDBNull(4)) Lotes.FolioFinal = Convert.ToInt32(reader[4]);
                                    if (!reader.IsDBNull(5)) Lotes.IdEstatus = Convert.ToInt32(reader[5]);
                                    if (!reader.IsDBNull(6)) Lotes.EstatusLote = Convert.ToString(reader[6]);
                                }

                                if (Accion == 3)
                                {
                                    if (!reader.IsDBNull(0)) Lotes.FolioMin = Convert.ToInt32(reader[0]);
                                    if (!reader.IsDBNull(1)) Lotes.FolioMax = Convert.ToInt32(reader[1]);
                                }

                                resultado.Add(Lotes);
                            }

                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return resultado;
        }
    }
}
