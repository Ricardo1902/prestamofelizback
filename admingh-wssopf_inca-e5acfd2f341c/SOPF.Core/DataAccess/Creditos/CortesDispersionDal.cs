#pragma warning disable 141229
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Creditos;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: CortesDispersionDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla CortesDispersion
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-12-29 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Creditos
{
    public class CortesDispersionDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public CortesDispersionDal()
		{
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarCortesDispersion(CortesDispersion entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<CortesDispersion> ObtenerCortesDispersion(int Accion,   int Idcorte)
        {
			List<CortesDispersion> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.Sel_CorteDispersion"))
				{
					//Parametros
					db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdCorte", DbType.Int32, Idcorte);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<CortesDispersion>();
                            while (reader.Read())
                            {
                                CortesDispersion Corte = new CortesDispersion();
                                if (!reader.IsDBNull(0)) Corte.IdCorte = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) Corte.Consecutivo = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) Corte.Fecha = Convert.ToDateTime(reader[2]);
                                if (!reader.IsDBNull(3)) Corte.Tipo = reader[3].ToString();
                                if (!reader.IsDBNull(4)) Corte.IdTipo = Convert.ToInt32(reader[4]);
                                if (!reader.IsDBNull(5)) Corte.total = Convert.ToDecimal(reader[5]);
                                resultado.Add(Corte);
                            }

						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarCortesDispersion()
        {
        }

        public void EliminarCortesDispersion()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}