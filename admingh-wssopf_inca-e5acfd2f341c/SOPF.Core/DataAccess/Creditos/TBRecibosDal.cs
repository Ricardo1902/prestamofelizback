#pragma warning disable 140827
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Creditos;

# region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBRecibosDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBRecibos
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Creditos
{
    public class TBRecibosDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBRecibosDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBRecibos(TBRecibos entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<DataResultCredito.Usp_ObtenerTablaAmortizacion> ObtenerTBRecibos(int idCuenta)
        {
            List<DataResultCredito.Usp_ObtenerTablaAmortizacion> objReturn = null;
            try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_TablaAmortizacionCON"))
				{
					//Parametros
					db.AddInParameter(com, "@IdCuenta", DbType.Int32, idCuenta);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
                            DataResultCredito.Usp_ObtenerTablaAmortizacion objC = null;
                            objReturn = new List<DataResultCredito.Usp_ObtenerTablaAmortizacion>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                               

                                objC = new DataResultCredito.Usp_ObtenerTablaAmortizacion();

                                //Lectura de los datos del ResultSet                                
                                objC.IdCuenta = Utils.GetInt(reader, "IdCuenta");
                                objC.Recibo = Utils.GetInt(reader, "Recibo");
                                objC.FchRecibo = Utils.GetDateTime(reader, "FechaRecibo", "dd/MM/yyyy hh:mm:ss tt");
                                objC.IdEstatus = Utils.GetInt(reader, "IdEstatus");
                                objC.Estatus = Utils.GetString(reader, "EstatusDesc");
                                objC.Capital = Utils.GetDecimal(reader, "Capital");
                                objC.Interes = Utils.GetDecimal(reader, "Interes");
                                objC.IGV = Utils.GetDecimal(reader, "IGV");
                                objC.Seguro = Utils.GetDecimal(reader, "Seguro");
                                objC.GAT = Utils.GetDecimal(reader, "GAT");
                                objC.CapitalInicial = Utils.GetDecimal(reader, "CapitalInicial");
                                objC.CapitalInsoluto = Utils.GetDecimal(reader, "CapitalInsoluto");
                                objC.SaldoCapital = Utils.GetDecimal(reader, "SaldoCapital");
                                objC.SaldoInteres = Utils.GetDecimal(reader, "SaldoInteres");
                                objC.SaldoIGV = Utils.GetDecimal(reader, "SaldoIGV");
                                objC.SaldoSeguro = Utils.GetDecimal(reader, "SaldoSeguro");
                                objC.SaldoGAT = Utils.GetDecimal(reader, "SaldoGAT");
                                objC.SaldoRecibo = Utils.GetDecimal(reader, "SaldoRecibo");
                                objC.TotalRecibo = Utils.GetDecimal(reader, "TotalRecibo");

                                objReturn.Add(objC);

                            }
						}
                        reader.Close();
						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			//return resultado;
            return objReturn;
        }
        
        public void ActualizarTBRecibos()
        {
        }

        public void EliminarTBRecibos()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}