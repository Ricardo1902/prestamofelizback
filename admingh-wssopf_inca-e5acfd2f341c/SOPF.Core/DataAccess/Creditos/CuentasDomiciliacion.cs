﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Creditos;
namespace SOPF.Core.DataAccess.Creditos
{
    public class CuentasDomiciliacionDal
    {
        #region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
        public CuentasDomiciliacionDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
        public List<CuentasDomiciliacion.Bancomer> CuentasDomiBancomer(int GpoEnvio, int DiasVenini,  int DiasVenFin, int IdBanco, decimal Porcent, int NumAmorti, int idMora, decimal monto, int AplicarGastos, string IdEmisora)
        {
            List<CuentasDomiciliacion.Bancomer> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("CobranzaAdministrativa.SelDomiBancomer"))
                {

                    com.CommandTimeout = 999999;
                    //Parametros
                    db.AddInParameter(com, "@IdGpoEnvio", DbType.Int32, GpoEnvio);
                    db.AddInParameter(com, "@DiasVenIni", DbType.Int32, DiasVenini);
                    db.AddInParameter(com, "@DiasVenFin", DbType.Int32, DiasVenFin);
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, IdBanco);
                    db.AddInParameter(com, "@Porcent", DbType.Decimal, Porcent);
                    db.AddInParameter(com, "@NumAmorti", DbType.Int32, NumAmorti);
                    db.AddInParameter(com, "@idMora", DbType.Int32, idMora);
                    db.AddInParameter(com, "@monto", DbType.Decimal, monto);
                    db.AddInParameter(com, "@AplaicarGastos", DbType.Int32, AplicarGastos);
                    db.AddInParameter(com, "@IdEmisora", DbType.String, IdEmisora);

                    
                    


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<CuentasDomiciliacion.Bancomer>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                CuentasDomiciliacion.Bancomer cuenta = new CuentasDomiciliacion.Bancomer();
                                if (!reader.IsDBNull(0)) cuenta.FechaPresentacion = reader[0].ToString();
                                if (!reader.IsDBNull(1)) cuenta.NumeroSecuencia = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) cuenta.Importe = Convert.ToDecimal(reader[2]);
                                if (!reader.IsDBNull(3)) cuenta.FechaLiquidacion = reader[3].ToString();
                                if (!reader.IsDBNull(4)) cuenta.FechaVencimiento = reader[4].ToString();
                                if (!reader.IsDBNull(5)) cuenta.Banco = reader[5].ToString();
                                if (!reader.IsDBNull(6)) cuenta.TipoCuenta = reader[6].ToString();
                                if (!reader.IsDBNull(7)) cuenta.Clabe = reader[7].ToString();
                                if (!reader.IsDBNull(8)) cuenta.Cliente = reader[8].ToString();
                                if (!reader.IsDBNull(9)) cuenta.referencia = reader[9].ToString();
                                if (!reader.IsDBNull(10)) cuenta.referencianum = reader[10].ToString();
                                if (!reader.IsDBNull(11)) cuenta.idcuenta = Convert.ToInt32(reader[11]);
                                if (!reader.IsDBNull(12)) cuenta.erogacion = Convert.ToDecimal(reader[12]);
                                if (!reader.IsDBNull(13)) cuenta.NumeroBloque= reader[13].ToString();
                                resultado.Add(cuenta);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        public List<CuentasDomiciliacion.Bancomer> CuentasDomiBancomerEspecifica(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, decimal Porcent, int NumAmorti, int idMora, decimal monto)
        {
            List<CuentasDomiciliacion.Bancomer> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("CobranzaAdministrativa.SelDomiBancomer_CobranzaEspecifica"))
                {

                    com.CommandTimeout = 999999;
                    //Parametros
                    db.AddInParameter(com, "@IdGpoEnvio", DbType.Int32, GpoEnvio);
                    db.AddInParameter(com, "@DiasVenIni", DbType.Int32, DiasVenini);
                    db.AddInParameter(com, "@DiasVenFin", DbType.Int32, DiasVenFin);
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, IdBanco);
                    db.AddInParameter(com, "@Porcent", DbType.Decimal, Porcent);
                    db.AddInParameter(com, "@NumAmorti", DbType.Int32, NumAmorti);
                    db.AddInParameter(com, "@idMora", DbType.Int32, idMora);
                    db.AddInParameter(com, "@monto", DbType.Decimal, monto);



                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<CuentasDomiciliacion.Bancomer>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                CuentasDomiciliacion.Bancomer cuenta = new CuentasDomiciliacion.Bancomer();
                                if (!reader.IsDBNull(0)) cuenta.FechaPresentacion = reader[0].ToString();
                                if (!reader.IsDBNull(1)) cuenta.NumeroSecuencia = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) cuenta.Importe = Convert.ToDecimal(reader[2]);
                                if (!reader.IsDBNull(3)) cuenta.FechaLiquidacion = reader[3].ToString();
                                if (!reader.IsDBNull(4)) cuenta.FechaVencimiento = reader[4].ToString();
                                if (!reader.IsDBNull(5)) cuenta.Banco = reader[5].ToString();
                                if (!reader.IsDBNull(6)) cuenta.TipoCuenta = reader[6].ToString();
                                if (!reader.IsDBNull(7)) cuenta.Clabe = reader[7].ToString();
                                if (!reader.IsDBNull(8)) cuenta.Cliente = reader[8].ToString();
                                if (!reader.IsDBNull(9)) cuenta.referencia = reader[9].ToString();
                                if (!reader.IsDBNull(10)) cuenta.referencianum = reader[10].ToString();
                                if (!reader.IsDBNull(11)) cuenta.idcuenta = Convert.ToInt32(reader[11]);
                                if (!reader.IsDBNull(12)) cuenta.erogacion = Convert.ToDecimal(reader[12]);
                                if (!reader.IsDBNull(13)) cuenta.NumeroBloque = reader[13].ToString();
                                resultado.Add(cuenta);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        public List<CuentasDomiciliacion.Banorte> CuentasDomiBanorte(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, decimal Porcent, int NumAmorti, int idMora, decimal monto, string idEmisora, int AplicarGastos)
        {
            List<CuentasDomiciliacion.Banorte> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("CobranzaAdministrativa.SelDomiBanorte"))
                {
                    com.CommandTimeout = 999999;
                    //Parametros
                    db.AddInParameter(com, "@IdGpoEnvio", DbType.Int32, GpoEnvio);
                    db.AddInParameter(com, "@DiasVenIni", DbType.Int32, DiasVenini);
                    db.AddInParameter(com, "@DiasVenFin", DbType.Int32, DiasVenFin);
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, IdBanco);
                    db.AddInParameter(com, "@Porcent", DbType.Decimal, Porcent);
                    db.AddInParameter(com, "@NumAmorti", DbType.Int32, NumAmorti);
                    db.AddInParameter(com, "@idMora", DbType.Int32, idMora);
                    db.AddInParameter(com, "@monto", DbType.Decimal, monto);
                    db.AddInParameter(com, "@IdEmisora", DbType.String, idEmisora);
                    db.AddInParameter(com, "@AplaicarGastos", DbType.Int32, AplicarGastos);


                    
                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<CuentasDomiciliacion.Banorte>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                CuentasDomiciliacion.Banorte cuenta = new CuentasDomiciliacion.Banorte();
                                if (!reader.IsDBNull(0)) cuenta.Emisora = reader[0].ToString();
                                if (!reader.IsDBNull(1)) cuenta.FechaProceso = (reader[1].ToString());
                                if (!reader.IsDBNull(2)) cuenta.consecutivo = Convert.ToInt32(reader[2]);
                                if (!reader.IsDBNull(3)) cuenta.TotalRegistros = Convert.ToInt32(reader[3].ToString());
                                if (!reader.IsDBNull(4)) cuenta.ImporteTotal = Convert.ToDecimal(reader[4].ToString());
                                if (!reader.IsDBNull(5)) cuenta.FechaAplicacion = reader[5].ToString();
                                if (!reader.IsDBNull(6)) cuenta.Referencia = Convert.ToInt32(reader[6].ToString());
                                if (!reader.IsDBNull(7)) cuenta.Importe = Convert.ToDecimal(reader[7].ToString());
                                if (!reader.IsDBNull(8)) cuenta.Banco = reader[8].ToString();
                                if (!reader.IsDBNull(9)) cuenta.TipoCuenta = reader[9].ToString();
                                if (!reader.IsDBNull(10)) cuenta.Clabe = reader[10].ToString();
                                
                                resultado.Add(cuenta);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
        public List<CuentasDomiciliacion.Banamex> CuentasDomiBanamex(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, decimal Porcent, int NumAmorti, int idMora, decimal monto, int AplicarGastos)
        {
            List<CuentasDomiciliacion.Banamex> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("CobranzaAdministrativa.SelDomiBanamex"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdGpoEnvio", DbType.Int32, GpoEnvio);
                    db.AddInParameter(com, "@DiasVenIni", DbType.Int32, DiasVenini);
                    db.AddInParameter(com, "@DiasVenFin", DbType.Int32, DiasVenFin);
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, IdBanco);
                    db.AddInParameter(com, "@Porcent", DbType.Decimal, Porcent);
                    db.AddInParameter(com, "@NumAmorti", DbType.Int32, NumAmorti);
                    db.AddInParameter(com, "@idMora", DbType.Int32, idMora);
                    db.AddInParameter(com, "@monto", DbType.Decimal, monto);
                    db.AddInParameter(com, "@AplaicarGastos", DbType.Int32, AplicarGastos);



                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<CuentasDomiciliacion.Banamex>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                CuentasDomiciliacion.Banamex cuenta = new CuentasDomiciliacion.Banamex();
                                if (!reader.IsDBNull(0)) cuenta.FechaPresentacion = reader[0].ToString();
                                if (!reader.IsDBNull(1)) cuenta.NumeroSecuencia = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) cuenta.Importe = Convert.ToDecimal(reader[2]);
                                if (!reader.IsDBNull(3)) cuenta.FechaLiquidacion = reader[3].ToString();
                                if (!reader.IsDBNull(4)) cuenta.FechaVencimiento = reader[4].ToString();
                                if (!reader.IsDBNull(5)) cuenta.Banco = reader[5].ToString();
                                if (!reader.IsDBNull(6)) cuenta.TipoCuenta = reader[6].ToString();
                                if (!reader.IsDBNull(7)) cuenta.Clabe = reader[7].ToString();
                                if (!reader.IsDBNull(8)) cuenta.Cliente = reader[8].ToString();
                                if (!reader.IsDBNull(9)) cuenta.referencia = Convert.ToInt32(reader[9]);
                                if (!reader.IsDBNull(10)) cuenta.referencianum = reader[10].ToString();
                                if (!reader.IsDBNull(11)) cuenta.idcuenta = Convert.ToInt32(reader[11]);
                                if (!reader.IsDBNull(12)) cuenta.erogacion = Convert.ToDecimal(reader[12]);
                                if (!reader.IsDBNull(13)) cuenta.NumeroBloque = reader[13].ToString();
                                if (!reader.IsDBNull(14)) cuenta.Secuencia = reader[13].ToString();
                                resultado.Add(cuenta);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }


        public List<CuentasDomiciliacion.Bancomer> CuentasDomiBancomerRemanente(int GpoEnvio, int DiasVenini, int DiasVenFin, int IdBanco, decimal Porcent, int NumAmorti, int idMora, decimal monto, int AplicarGastos, int idGrupoOrigen, string IdEmisora)
        {
            List<CuentasDomiciliacion.Bancomer> resultado = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("CobranzaAdministrativa.SelDomiBancomer_remanente"))
                {

                    com.CommandTimeout = 999999;
                    //Parametros
                    db.AddInParameter(com, "@IdGpoEnvio", DbType.Int32, GpoEnvio);
                    db.AddInParameter(com, "@DiasVenIni", DbType.Int32, DiasVenini);
                    db.AddInParameter(com, "@DiasVenFin", DbType.Int32, DiasVenFin);
                    db.AddInParameter(com, "@IdBanco", DbType.Int32, IdBanco);
                    db.AddInParameter(com, "@Porcent", DbType.Decimal, Porcent);
                    db.AddInParameter(com, "@NumAmorti", DbType.Int32, NumAmorti);
                    db.AddInParameter(com, "@idMora", DbType.Int32, idMora);
                    db.AddInParameter(com, "@monto", DbType.Decimal, monto);
                    db.AddInParameter(com, "@AplaicarGastos", DbType.Int32, AplicarGastos);
                    db.AddInParameter(com, "@IdGrupoOrigen", DbType.Int32, idGrupoOrigen);
                    db.AddInParameter(com, "@IdEmisora", DbType.String, IdEmisora);





                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<CuentasDomiciliacion.Bancomer>();
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                CuentasDomiciliacion.Bancomer cuenta = new CuentasDomiciliacion.Bancomer();
                                if (!reader.IsDBNull(0)) cuenta.FechaPresentacion = reader[0].ToString();
                                if (!reader.IsDBNull(1)) cuenta.NumeroSecuencia = Convert.ToInt32(reader[1]);
                                if (!reader.IsDBNull(2)) cuenta.Importe = Convert.ToDecimal(reader[2]);
                                if (!reader.IsDBNull(3)) cuenta.FechaLiquidacion = reader[3].ToString();
                                if (!reader.IsDBNull(4)) cuenta.FechaVencimiento = reader[4].ToString();
                                if (!reader.IsDBNull(5)) cuenta.Banco = reader[5].ToString();
                                if (!reader.IsDBNull(6)) cuenta.TipoCuenta = reader[6].ToString();
                                if (!reader.IsDBNull(7)) cuenta.Clabe = reader[7].ToString();
                                if (!reader.IsDBNull(8)) cuenta.Cliente = reader[8].ToString();
                                if (!reader.IsDBNull(9)) cuenta.referencia = reader[9].ToString();
                                if (!reader.IsDBNull(10)) cuenta.referencianum = reader[10].ToString();
                                if (!reader.IsDBNull(11)) cuenta.idcuenta = Convert.ToInt32(reader[11]);
                                if (!reader.IsDBNull(12)) cuenta.erogacion = Convert.ToDecimal(reader[12]);
                                if (!reader.IsDBNull(13)) cuenta.NumeroBloque = reader[13].ToString();
                                resultado.Add(cuenta);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

    }
}
