#pragma warning disable 140827
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Creditos;
using SOPF.Core.Entities.CobranzaAdministrativa;
using SOPF.Core.Entities;

#region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo: TBCobranzaDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCobranza
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Creditos
{
    public class TBCobranzaDal
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCobranzaDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods

        public void InsertarTBCobranza(TBCobranza entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public TBCobranza ObtenerTBCobranza()
        {
			TBCobranza resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new TBCobranza();
							//Lectura de los datos del ResultSet
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
              
        #endregion
        
        #region MISC Methods

        public List<DataResultCredito.Usp_HistorialPagos> ObtenerHistorialPagos(int idCuenta)
        {
            
            List<DataResultCredito.Usp_HistorialPagos> objReturn = null;
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Creditos.SP_HistorialPagosCON"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdCuenta", DbType.Int32, idCuenta);

                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            DataResultCredito.Usp_HistorialPagos objC = null;
                            objReturn = new List<DataResultCredito.Usp_HistorialPagos>();
                           
                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {

                                objC = new DataResultCredito.Usp_HistorialPagos();

                                //Lectura de los datos del ResultSet
                                objC.IdPago = Utils.GetInt(reader, "IdPago");
                                objC.IdDomiciliacion = Utils.GetInt(reader, "IdDomiciliacion");
                                objC.IdPagoDirecto = Utils.GetInt(reader, "IdPagoDirecto");
                                objC.IdSolicitud = Utils.GetInt(reader, "IdSolicitud");
                                objC.MontoCobrado = Utils.GetDecimal(reader, "MontoCobrado");
                                objC.MontoAplicado = Utils.GetDecimal(reader, "MontoAplicado");
                                objC.MontoPorAplicar = Utils.GetDecimal(reader, "MontoPorAplicar");
                                objC.CanalPagoClave = Utils.GetString(reader, "CanalPagoClave");
                                objC.CanalPago = Utils.GetString(reader, "CanalPagoDesc");                                
                                objC.Estatus = Utils.GetString(reader, "EstatusDesc");
                                objC.FechaPago = Utils.GetDateTime(reader, "FechaPago", "dd/MM/yyyy hh:mm:ss tt");
                                objC.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro", "dd/MM/yyyy hh:mm:ss tt");
                                objC.FechaAplicacion = Utils.GetDateTime(reader, "FechaAplicacion", "dd/MM/yyyy hh:mm:ss tt");

                                objReturn.Add(objC);
                            }
                        }
                        reader.Close();
                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturn;
        }

        public List<PagosNoIdentificados> ObtenerPagosNoIdentificados(DateTime? fchCreditoDesde, DateTime? fchCreditoHasta)
        {
            List<PagosNoIdentificados> objReturn = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PagosNoIdentificadosCON"))
                {
                    //Parametros                    
                    db.AddInParameter(com, "@FechaIni", DbType.DateTime, fchCreditoDesde);
                    db.AddInParameter(com, "@FechaFin", DbType.DateTime, fchCreditoHasta);
                    //Ejecucion de la Consulta                    
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            PagosNoIdentificados objC = null;
                            objReturn = new List<PagosNoIdentificados>();
                            while (reader.Read())
                            {
                                objC = new PagosNoIdentificados();

                                //Lectura de los datos del ResultSet
                                objC.IdPagoNoIdentificado = Utils.GetInt(reader, "IdPagoNoIdentificado");
                                objC.Fch_Pago = Utils.GetDateTime(reader, "Fch_Pago");
                                objC.Monto = Utils.GetDecimal(reader, "Monto");
                                objC.nombreCanal = Utils.GetString(reader, "Nombre");
                                objC.Observaciones = Utils.GetString(reader, "Observaciones");
                                objReturn.Add(objC);
                            }
                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public List<Devolucion> ObtenerDevoluciones(DevolucionCondiciones condiciones)
        {
            List<Devolucion> devoluciones = null;
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_DevolucionesCON"))
                {
                    if (condiciones.idSolicitud > 0)
                        db.AddInParameter(com, "@idSolicitud", DbType.Int32, condiciones.idSolicitud);
                    if (condiciones.fechaPagoInicio != DateTime.MinValue)
                        db.AddInParameter(com, "@fechaPagoInicio", DbType.DateTime, condiciones.fechaPagoInicio);
                    if (condiciones.fechaPagoFin != DateTime.MinValue)
                        db.AddInParameter(com, "@fechaPagoFin", DbType.DateTime, condiciones.fechaPagoFin);
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            devoluciones = new List<Devolucion>();
                            Devolucion dev = null;
                            while (reader.Read())
                            {
                                dev = new Devolucion();
                                dev.idDevolucion = Utils.GetInt(reader, "idDevolucion");
                                dev.idSolicitud = Utils.GetInt(reader, "idSolicitud");
                                dev.fechaPago = Utils.GetDateTime(reader, "fechaPago");
                                dev.montoPago = Utils.GetDecimal(reader, "montoPago");
                                dev.idEstatus = Utils.GetInt(reader, "idEstatus");
                                dev.comentarios = Utils.GetString(reader, "comentarios");
                                dev.resultado = Utils.GetString(reader, "resultado");
                                dev.idUsuario = Utils.GetInt(reader, "idUsuario");
                                dev.FechaRegistro = Utils.GetDateTime(reader, "FechaRegistro").Value;
                                devoluciones.Add(dev);
                            }
                        }
                        reader.Dispose();
                    }
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return devoluciones;
        }

        public Resultado<bool> GuardaPagoNoIdentificadoIndiv(PagosNoIdentificados pagos)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PagosNoIdentificadosALT"))
                {
                    //Parametros
                    db.AddInParameter(com, "@fchPago", DbType.DateTime, pagos.Fch_Pago);
                    db.AddInParameter(com, "@IdCanalpago", DbType.Int32, pagos.IdCanalPAgo);
                    db.AddInParameter(com, "@Monto", DbType.Decimal, pagos.Monto);
                    db.AddInParameter(com, "@Observaciones", DbType.String, pagos.Observaciones);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, pagos.IdUsuario);
                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
                objReturn.Codigo = 0;
                objReturn.Mensaje = "Datos Guardados";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> GuardaDevolucion(Devolucion devolucion)
        {
            Resultado<bool> objReturn = new Resultado<bool>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("cobranza.SP_DevolucionALT"))
                {
                    db.AddInParameter(com, "@idSolicitud", DbType.Int32, devolucion.idSolicitud);
                    db.AddInParameter(com, "@fecha", DbType.DateTime, devolucion.fechaPago);                    
                    db.AddInParameter(com, "@monto", DbType.Decimal, devolucion.montoPago);
                    db.AddInParameter(com, "@comentarios", DbType.String, devolucion.comentarios);
                    db.AddInParameter(com, "@idUsuario", DbType.Int32, devolucion.idUsuario);                    
                    db.ExecuteNonQuery(com);                    
                    com.Dispose();
                }
                objReturn.Codigo = 0;
                objReturn.Mensaje = "Datos Guardados";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> AplicarDevolucionesPendientes()
        {
            Resultado<bool> objReturn = new Resultado<bool>();
            try
            {
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_DevolverPagoPRO"))
                {
                    db.ExecuteNonQuery(com);
                    com.Dispose();
                }
                objReturn.Codigo = 0;
                objReturn.Mensaje = "Devoluciones aplicadas";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        public Resultado<bool> ActuPagoNoIdentificadoIndiv(PagosNoIdentificados pagos)
        {
            Resultado<bool> objReturn = new Resultado<bool>();

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Cobranza.SP_PagosNoIdentificadosACT"))
                {
                    //Parametros
                    db.AddInParameter(com, "@IdPagoNoIdentificado", DbType.Int32, pagos.IdPagoNoIdentificado);
                    db.AddInParameter(com, "@IdSolicitud", DbType.Int32, pagos.IdSolicitud);
                    
                    //Ejecucion de la Consulta                    
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
                objReturn.Codigo = 0;
                objReturn.Mensaje = "Datos Actualizados";
            }
            catch (SqlException ex)
            {
                objReturn.Codigo = 1;
                objReturn.Mensaje = ex.Message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objReturn;
        }

        #endregion        

        #region Private Methods
        #endregion
    }
}