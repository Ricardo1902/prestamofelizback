﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities;
using SOPF.Core.Entities.Creditos;

namespace SOPF.Core.DataAccess.Creditos
{
    public class ReimpresionFormatosDal
    {
        #region Objetos Base de Datos
        Database db;
        #endregion

        #region Contructor
        public ReimpresionFormatosDal()
        {
            db = DatabaseFactory.CreateDatabase(Utils.Conexion);
        }
        #endregion

        public void InsertarReimpresion(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdFolio, string Comentario, int IdEstatus)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_ABCReimpresionFormatos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@FechaAlta", DbType.DateTime, FechaAlta);
                    db.AddInParameter(com, "@IdUsuarioAlta", DbType.Int32, IdUsuarioAlta);
                    db.AddInParameter(com, "@IdFormato", DbType.Int32, IdFormato);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, IdSucursal);
                    db.AddInParameter(com, "@FolioInicial", DbType.Int32, FolioInicial);
                    db.AddInParameter(com, "@FolioFinal", DbType.Int32, FolioFinal);
                    db.AddInParameter(com, "@IdFolio", DbType.Int32, IdFolio);
                    db.AddInParameter(com, "@Comentario", DbType.String, Comentario);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, IdEstatus);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarReimpresion(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdFolio, string Comentario, int IdEstatus)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_ABCReimpresionFormatos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@FechaAlta", DbType.DateTime, FechaAlta);
                    db.AddInParameter(com, "@IdUsuarioAlta", DbType.Int32, IdUsuarioAlta);
                    db.AddInParameter(com, "@IdFormato", DbType.Int32, IdFormato);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, IdSucursal);
                    db.AddInParameter(com, "@FolioInicial", DbType.Int32, FolioInicial);
                    db.AddInParameter(com, "@FolioFinal", DbType.Int32, FolioFinal);
                    db.AddInParameter(com, "@IdFolio", DbType.Int32, IdFolio);
                    db.AddInParameter(com, "@Comentario", DbType.String, Comentario);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, IdEstatus);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarReimpresion(int Accion, DateTime FechaAlta, int IdUsuarioAlta, int IdFormato, int IdSucursal, int FolioInicial, int FolioFinal, int IdFolio, string Comentario, int IdEstatus)
        {
            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("dbo.SP_ABCReimpresionFormatos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@FechaAlta", DbType.DateTime, FechaAlta);
                    db.AddInParameter(com, "@IdUsuarioAlta", DbType.Int32, IdUsuarioAlta);
                    db.AddInParameter(com, "@IdFormato", DbType.Int32, IdFormato);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, IdSucursal);
                    db.AddInParameter(com, "@FolioInicial", DbType.Int32, FolioInicial);
                    db.AddInParameter(com, "@FolioFinal", DbType.Int32, FolioFinal);
                    db.AddInParameter(com, "@IdFolio", DbType.Int32, IdFolio);
                    db.AddInParameter(com, "@Comentario", DbType.String, Comentario);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, IdEstatus);

                    //Ejecucion de la Consulta
                    db.ExecuteNonQuery(com);

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ReimpresionFormatos> ConsultarReimpresion(int Accion, int IdFolio, DateTime Fecha, int IdSucursal, int IdUsuario, int FolioInicial, int FolioFinal, int IdEstatus, int IdFormato)
        {
            List<ReimpresionFormatos> resultado = null;

            try
            {
                //Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelReimpresionFormatos"))
                {
                    //Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@IdFolio", DbType.Int32, IdFolio);
                    db.AddInParameter(com, "@Fecha", DbType.DateTime, Fecha);
                    db.AddInParameter(com, "@IdSucursal", DbType.Int32, IdSucursal);
                    db.AddInParameter(com, "@IdUsuario", DbType.Int32, IdUsuario);
                    db.AddInParameter(com, "@FolioInicial", DbType.Int32, FolioInicial);
                    db.AddInParameter(com, "@FolioFinal", DbType.Int32, FolioFinal);
                    db.AddInParameter(com, "@IdEstatus", DbType.Int32, IdEstatus);
                    db.AddInParameter(com, "@IdFormato", DbType.Int32, IdFormato);


                    //Ejecucion de la Consulta
                    using (IDataReader reader = db.ExecuteReader(com))
                    {
                        if (reader != null)
                        {
                            resultado = new List<ReimpresionFormatos>();

                            //Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                ReimpresionFormatos Reimp = new ReimpresionFormatos();

                                if (Accion == 0)
                                {
                                    if (!reader.IsDBNull(0)) Reimp.IdReimp = Convert.ToInt32(reader[0]);
                                    if (!reader.IsDBNull(1)) Reimp.Formato = Convert.ToString(reader[1]);
                                    if (!reader.IsDBNull(2)) Reimp.FolioInicial = Convert.ToInt32(reader[2]);
                                    if (!reader.IsDBNull(3)) Reimp.FolioFinal = Convert.ToInt32(reader[3]);
                                    if (!reader.IsDBNull(4)) Reimp.Comentario = Convert.ToString(reader[4]);
                                    if (!reader.IsDBNull(5)) Reimp.Cantidad = Convert.ToInt32(reader[5]);

                                }

                                resultado.Add(Reimp);
                            }

                        }

                        reader.Dispose();
                    }

                    //Cierre de la conexion y liberacion de memoria
                    com.Dispose();

                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return resultado;
        }
    }
}

