#pragma warning disable 141208
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Creditos;

# region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: RepCirculoDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla RepCirculo
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-12-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Creditos
{
    public class RepCirculoDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public RepCirculoDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarRepCirculo(RepCirculo entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<RepCirculo> ObtenerRepCirculo(int Accion, DateTime Fecha)
        {
			List<RepCirculo> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("Reportes.BuroCredito"))
				{
					//Parametros
					db.AddInParameter(com, "@Accion", DbType.Int32, Accion);
                    db.AddInParameter(com, "@FechaCorte", DbType.DateTime, Fecha);
                    com.CommandTimeout = 9999999;

					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<RepCirculo>();
                            while (reader.Read())
                            {
                                RepCirculo repCirculo = new RepCirculo();
                                if (!reader.IsDBNull(0)) repCirculo.IdCuenta = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(1)) repCirculo.ClaveOtorgante = reader[1].ToString();
                                if (!reader.IsDBNull(2)) repCirculo.NombreOtorgante = reader[2].ToString();
                                if (!reader.IsDBNull(3)) repCirculo.FechaExtraccion = (reader[3].ToString());
                                if (!reader.IsDBNull(4)) repCirculo.Versi = reader[4].ToString();
                                if (!reader.IsDBNull(5)) repCirculo.ApellidPaterno = reader[5].ToString();
                                if (!reader.IsDBNull(6)) repCirculo.ApellidoMaterno = reader[6].ToString();
                                if (!reader.IsDBNull(7)) repCirculo.Nombres = reader[7].ToString();
                                if (!reader.IsDBNull(8)) repCirculo.Direccion = reader[8].ToString();
                                if (!reader.IsDBNull(9)) repCirculo.Ciudad = reader[9].ToString();
                                if (!reader.IsDBNull(10)) repCirculo.Estado = reader[10].ToString();
                                if (!reader.IsDBNull(11)) repCirculo.CP = reader[11].ToString();
                                if (!reader.IsDBNull(12)) repCirculo.CuentaActual = Convert.ToDecimal(reader[12]);
                                if (!reader.IsDBNull(13)) repCirculo.TipoResponsabilidad = reader[13].ToString();
                                if (!reader.IsDBNull(14)) repCirculo.TipoCuenta = reader[14].ToString();
                                if (!reader.IsDBNull(15)) repCirculo.TipoContrato = reader[15].ToString();
                                if (!reader.IsDBNull(16)) repCirculo.CveUnicaMoneda = reader[16].ToString();
                                if (!reader.IsDBNull(17)) repCirculo.FrecuenciaPagos = reader[17].ToString();
                                if (!reader.IsDBNull(18)) repCirculo.MontoPagar = Convert.ToDecimal(reader[18]);
                                if (!reader.IsDBNull(19)) repCirculo.FchApertura = reader[19].ToString();
                                if (!reader.IsDBNull(20)) repCirculo.FchUltimoPago = reader[20].ToString();;
                                if (!reader.IsDBNull(21)) repCirculo.FchUltimaCompra = reader[21].ToString();;
                                if (!reader.IsDBNull(22)) repCirculo.FechaCorte = reader[22].ToString();;
                                if (!reader.IsDBNull(23)) repCirculo.CreditoMaximo = Convert.ToDecimal(reader[23]);
                                if (!reader.IsDBNull(24)) repCirculo.SaldoActual = Convert.ToDecimal(reader[24]);
                                if (!reader.IsDBNull(25)) repCirculo.LimiteCredito = Convert.ToDecimal(reader[25]);
                                if (!reader.IsDBNull(26)) repCirculo.PagoActual = reader[26].ToString();
                                if (!reader.IsDBNull(27)) repCirculo.SaldoInsoluto = Convert.ToDecimal(reader[27]);
                                if (!reader.IsDBNull(28)) repCirculo.TotalSaldoAct = Convert.ToInt64(reader[28]);
                                if (!reader.IsDBNull(29)) repCirculo.TotalSaldoVen = Convert.ToInt64(reader[29]);
                                if (!reader.IsDBNull(30)) repCirculo.TotalNombRepo = Convert.ToInt32(reader[30]);
                                if (!reader.IsDBNull(31)) repCirculo.TotalDireRepo = Convert.ToInt32(reader[31]);
                                if (!reader.IsDBNull(32)) repCirculo.TotalEmplRepo = Convert.ToInt32(reader[32]);
                                if (!reader.IsDBNull(33)) repCirculo.TotalCuenRepo = Convert.ToInt32(reader[33]);
                                if (!reader.IsDBNull(34)) repCirculo.DomicilioDev = reader[34].ToString();
                                if (!reader.IsDBNull(35)) repCirculo.FechaCierre = reader[35].ToString();
                                if (!reader.IsDBNull(36)) repCirculo.NumeroPagos = Convert.ToInt32(reader[36].ToString());
                                if (!reader.IsDBNull(37)) repCirculo.Vencido = Convert.ToInt32(reader[37].ToString());
                                if (!reader.IsDBNull(38)) repCirculo.RecibosVencidos = Convert.ToInt32(reader[38].ToString());
                                if (!reader.IsDBNull(39)) repCirculo.FechaNacimiento = reader[39].ToString();
                                if (!reader.IsDBNull(40)) repCirculo.RFC = reader[40].ToString();
                                if (!reader.IsDBNull(41)) repCirculo.CURP = reader[41].ToString();
                                if (!reader.IsDBNull(42)) repCirculo.NACIONALIDAD = reader[42].ToString();                                

                                if (!reader.IsDBNull(44)) repCirculo.NombreEmpresa = reader[44].ToString();
                                if (!reader.IsDBNull(45)) repCirculo.DireccionEmpresa = reader[45].ToString();
                                if (!reader.IsDBNull(46)) repCirculo.ColoniaEmpresa = reader[46].ToString();
                                if (!reader.IsDBNull(47)) repCirculo.CiudadEmpresa = reader[47].ToString();
                                if (!reader.IsDBNull(48)) repCirculo.EstadoEmpresa = reader[48].ToString();
                                if (!reader.IsDBNull(49)) repCirculo.CPEmpresa = reader[49].ToString();
                                if (!reader.IsDBNull(50)) repCirculo.TelefonoEmpresa = reader[50].ToString();
                                if (!reader.IsDBNull(51)) repCirculo.Extension = reader[51].ToString();
                                if (!reader.IsDBNull(52)) repCirculo.Puesto = reader[52].ToString();
                                if (!reader.IsDBNull(53)) repCirculo.FechaContratacion = reader[53].ToString();
                                if (!reader.IsDBNull(54)) repCirculo.ClaveMonedaEmp = reader[54].ToString();
                                if (!reader.IsDBNull(55)) repCirculo.Salario = reader[55].ToString();
                                if (!reader.IsDBNull(56)) repCirculo.FechaVerificacion = reader[56].ToString();
                                
                                resultado.Add(repCirculo);
                            }

						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarRepCirculo()
        {
        }

        public void EliminarRepCirculo()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}