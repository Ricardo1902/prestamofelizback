#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using SOPF.Core.Entities.Menu;

# region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

# region Informacion General
//
// Archivo: TBCATMenuDal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATMenu
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.DataAccess.Menu
{
    public class TBCATMenuDal 
    {
		#region Objetos Base de Datos
		Database db;
		#endregion

		#region Contructor
		public TBCATMenuDal()
		{
			db = DatabaseFactory.CreateDatabase(Utils.Conexion);
		}
		#endregion
		
        #region CRUD Methods
        public void InsertarTBCATMenu(TBCATMenu entidad)
        {
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
				using (DbCommand com = db.GetStoredProcCommand("NombreDelStrore"))
				{
					//Parametros
					//db.AddInParameter(com, "@Parametro", DbType.Tipo, entidad.Atributo);
				
					//Ejecucion de la Consulta
					db.ExecuteNonQuery(com);

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }
        
        public List<TBCATMenu> ObtenerTBCATMenu(int accion,   int idTipoUsuario, int idMenu, int nivel, int idModulo, int tipo)
        {
			List<TBCATMenu> resultado = null;
			try
			{
				//Obtener DbCommand para ejcutar el Store Procedure
                using (DbCommand com = db.GetStoredProcCommand("SP_SelMenuIntranetPF"))
				{
					//Parametros
                    db.AddInParameter(com, "@Accion", DbType.Int32, accion);
                    db.AddInParameter(com, "@IdTipoUsuario", DbType.Int32, idTipoUsuario);
                    db.AddInParameter(com, "@IdMenu", DbType.Int32, idMenu);
                    db.AddInParameter(com, "@Nivel", DbType.Int32, nivel);
                    db.AddInParameter(com, "@IdModulo", DbType.Int32, idModulo);
                    db.AddInParameter(com, "@Tipo", DbType.Int32, tipo);                    
				
					//Ejecucion de la Consulta
					using (IDataReader reader = db.ExecuteReader(com))
					{
						if (reader != null)
						{
							resultado = new List<TBCATMenu>();
							//Lectura de los datos del ResultSet
                            while (reader.Read())
                            {
                                TBCATMenu Menu = new TBCATMenu();
                                if (!reader.IsDBNull(0)) Menu.IdMenu = Convert.ToInt32(reader[0]);
                                if (!reader.IsDBNull(2)) Menu.Menu = reader[2].ToString();
                                if (!reader.IsDBNull(3)) Menu.Descripcion = reader[3].ToString();
                                if (!reader.IsDBNull(4)) Menu.URL = reader[4].ToString();
                                if (!reader.IsDBNull(5)) Menu.Nivel = Convert.ToInt32(reader[5]);
                                if (!reader.IsDBNull(6)) Menu.Padre = Convert.ToInt32(reader[6]);
                                if (!reader.IsDBNull(7)) Menu.Hijo = reader[7].ToString();
                                if (!reader.IsDBNull(8)) Menu.Tipo = Convert.ToInt32(reader[8]);
                                if (!reader.IsDBNull(10)) Menu.IdModulo = Convert.ToInt32(reader[10]);
                                if (!reader.IsDBNull(11)) Menu.Carpeta = reader[11].ToString();
                                resultado.Add(Menu);
                               
                            }
						}

						reader.Dispose();
					}

					//Cierre de la conexion y liberacion de memoria
					com.Dispose();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return resultado;
        }
        
        public void ActualizarTBCATMenu()
        {
        }

        public void EliminarTBCATMenu()
        {
        }
        #endregion
        
        #region MISC Methods
        #endregion

        #region Private Methods
        #endregion
    }
}