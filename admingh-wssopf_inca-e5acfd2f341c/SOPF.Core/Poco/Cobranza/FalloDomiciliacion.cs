﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("FalloDomiciliacion", Schema = "Cobranza")]
    public class FalloDomiciliacion
    {
        [Key]
        public long IdFalloDomiciliacion { get; set; }
        public int IdSolicitud { get; set; }
        public int IdCuenta { get; set; }
        public int IdCanalPago { get; set; }
        public string DescripcionFallo { get; set; }
        public DateTime FechaValidacion { get; set; }
        public bool? RegistroActual { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}