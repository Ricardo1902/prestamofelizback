﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
	[Table("TB_PeticionDomiciliacionDetalle", Schema = "Cobranza")]
	public class TB_PeticionDomiciliacionDetalle
	{
		[Key]
		public int IdPeticionDomiciliacionDetalle { get; set; }
		public int IdPeticionDomiciliacion { get; set; }
		public int? IdCuenta { get; set; }
		public int? IdSolicitud { get; set; }
		public decimal MontoCobro { get; set; }
		public int? IdTipoDocumento { get; set; }
		public string ClaveTipoDocumento { get; set; }
		public string NumeroDocumento { get; set; }
		public string NombreCliente { get; set; }
		public string Banco { get; set; }
		public string NumeroCuenta { get; set; }
		public string NumeroTarjeta { get; set; }
		public int? MesExpiracion { get; set; }
		public int? AnioExpiracion { get; set; }
	}
}
