//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
	[Table("TB_PlantillaEnvioCobro", Schema = "Cobranza")]
	public class TB_PlantillaEnvioCobro
	{
		[Key]
		public int IdPlantillaEnvioCobro { get; set; }
		public int? IdPlantilla { get; set; }
		public int TotalRegistros { get; set; }
		public decimal MontoTotal { get; set; }
		public DateTime FechaRegistro { get; set; }
		public int IdUsuarioRegistro { get; set; }
		public bool ArchivoGenerado { get; set; }
		public int? VecesGenerado { get; set; }
	}
}
