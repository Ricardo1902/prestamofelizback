﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("TB_LayoutGeneradoPlantilla", Schema = "Cobranza")]
    public class TB_LayoutGeneradoPlantilla
    {
        [Key, Column(Order = 0)]
        public int? IdLayoutGenerado { get; set; }
        [Key, Column(Order = 1)]
        public int? IdPlantilla { get; set; }
        public int? IdPlantillaEnvioCobro { get; set; }
        public bool? OperacionCorrecta { get; set; }
        public string MensajeOperacion { get; set; }
        public int? DiasVencimiento { get; set; }
        public DateTime? FechaVencimientoProceso { get; set; }
        public DateTime? FechaRegistro { get; set; }
    }
}
