﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
	[Table("TB_TipoIngreso", Schema = "Cobranza")]
	public class TB_TipoIngreso
	{
		[Key]
		public int IdTipoIngreso { get; set; }
		public string Tipo { get; set; }
	}
}
