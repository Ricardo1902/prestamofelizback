//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("TB_LayoutArchivo", Schema = "Cobranza")]
    public class TB_LayoutArchivo
    {
        [Key]
        public int IdLayoutArchivo { get; set; }
        public int IdTipoLayout { get; set; }
        public string NombreLayout { get; set; }
        public int IdTipoArchivo { get; set; }
        public string NombreArchivo { get; set; }
        public string DelimitadorColumna { get; set; }
        public string Extension { get; set; }
        public string TipoContenido { get; set; }
        public int? IdBanco { get; set; }
        public int? Consecutivo { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public bool? Activo { get; set; }
        public string CaracteresRemplazo { get; set; }
    }
}
