//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("TB_PlantillaBucket", Schema = "Cobranza")]
    public class TB_PlantillaBucket
    {
        [Key]
        public int IdPlantillaBucket { get; set; }
        public int? IdPlantilla { get; set; }
        public int? IdConvenio { get; set; }
        public int? IdBucket { get; set; }
        public decimal? MontoIndivisible { get; set; }
    }
}
