﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("TB_PeticionDomiciliacion", Schema = "Cobranza")]
    public class TB_PeticionDomiciliacion
    {
        [Key]
        public int IdPeticionDomiciliacion { get; set; }
        public int IdOrigenDomiciliacion { get; set; }
        public int IdLayoutArchivo { get; set; }
        public int TotalRegistros { get; set; }
        public decimal? MontoTotal { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public string NombreArchivo { get; set; }
        public int VecesDescargado { get; set; }
    }
}
