﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("TB_ProgramacionDetProLayoutGenerado", Schema = "Cobranza")]
    public class TB_ProgramacionDetProLayoutGenerado
    {
        [Key]
        public int IdProgDetProLayoutGen { get; set; }
        public int IdProgramacionDetallePro { get; set; }
        public int IdLayoutGenerado { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int IdUsuarioRegistro { get; set; }
    }
}