//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
	[Table("TB_Plantilla", Schema = "Cobranza")]
	public class TB_Plantilla
	{
		[Key]
		public int IdPlantilla { get; set; }
		public int? IdTipoLayout { get; set; }
		public string Nombre { get; set; }
		public DateTime? FechaRegistro { get; set; }
		public int? IdUsuarioRegistro { get; set; }
		public bool? UsarCuentaEmergente { get; set; }
		public bool? OtroBancos { get; set; }
		public bool? Activo { get; set; }
	}
}
