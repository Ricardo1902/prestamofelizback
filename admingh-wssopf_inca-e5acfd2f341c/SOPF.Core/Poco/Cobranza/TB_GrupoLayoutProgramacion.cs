﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("TB_GrupoLayoutProgramacion", Schema = "Cobranza")]
    public class TB_GrupoLayoutProgramacion
    {
        [Key]
        public int IdProgramacion { get; set; }
        public int IdTipoProgramacion { get; set; }
        public int IdGrupoLayout { get; set; }
        public int IdUsuarioCreacion { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }
    }
}