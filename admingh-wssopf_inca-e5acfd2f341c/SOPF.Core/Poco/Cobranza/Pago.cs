﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("Pago", Schema = "Cobranza")]
    public class Pago
    {
        [Key]
        public int IdPago { get; set; }
        public int? IdDomiciliacion { get; set; }
        public int? IdPagoDirecto { get; set; }
        public int? IdSolicitud { get; set; }
        public decimal? MontoCobrado { get; set; }
        public decimal? MontoAplicado { get; set; }
        public int? IdEstatus { get; set; }
        public DateTime? FechaPago { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaAplicacion { get; set; }
        public int? IdCanalPago { get; set; }
        public decimal? Comision { get; set; }
        public int? PagoNoIdentificado_Id { get; set; }
        public int? IdPeticionVisanet { get; set; }
        public int? Intentos { get; set; }
    }
}
