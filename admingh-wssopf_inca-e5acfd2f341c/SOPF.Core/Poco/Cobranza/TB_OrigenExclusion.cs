﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
	[Table("TB_OrigenExclusion", Schema = "Cobranza")]
	public class TB_OrigenExclusion
	{
		[Key]
		public int IdOrigenExclusion { get; set; }
		public string Origen { get; set; }
	}
}