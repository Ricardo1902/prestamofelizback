﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
	[Table("TB_CuentaExcluyeDomiciliacion", Schema = "Cobranza")]
	public class TB_CuentaExcluyeDomiciliacion
	{
		[Key]
		public int IdCuentaExcluyeDomiciliacion { get; set; }
		public int IdTipoIngreso { get; set; }
		public int IdOrigenExclusion { get; set; }
		public int IdSolicitud { get; set; }
		public int? IdMotivoExclusionDomiciliacion { get; set; }
		public string Comentario { get; set; }
		public DateTime? FechaRegistro { get; set; }
		public int IdUsuarioRegistro { get; set; }
		public bool Activo { get; set; }
		public DateTime? FechaVigencia { get; set; }
		public DateTime? FechaActualizacion { get; set; }
		public int? IdUsuarioActualizo { get; set; }
	}
}