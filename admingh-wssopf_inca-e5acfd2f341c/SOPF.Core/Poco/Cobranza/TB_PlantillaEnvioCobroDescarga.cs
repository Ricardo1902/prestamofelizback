﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
	[Table("TB_PlantillaEnvioCobroDescarga", Schema = "Cobranza")]
	public class TB_PlantillaEnvioCobroDescarga
	{
		[Key]
		public int IdPlantillaEnvioCobroDescarga { get; set; }
		public int IdPlantillaEnvioCobro { get; set; }
		public int IdLayoutArchivo { get; set; }
		public int IdUsuarioDescarga { get; set; }
		public DateTime FechaDescarga { get; set; }
	}
}