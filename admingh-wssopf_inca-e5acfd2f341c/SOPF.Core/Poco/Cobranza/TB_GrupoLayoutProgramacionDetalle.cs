﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("TB_GrupoLayoutProgramacionDetalle", Schema = "Cobranza")]
    public class TB_GrupoLayoutProgramacionDetalle
    {
        [Key]
        public int IdProgramacionDetalle { get; set; }
        public int IdProgramacion { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaTermino { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraTermino { get; set; }
        public int IdIntervalo { get; set; }
        public decimal ValorIntervalo { get; set; }
        public int Intentos { get; set; }
        public bool EsGrupoMoroso { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }
        public int IdProgramacionEstatus { get; set; }
    }
}
