﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("TB_LayoutGenerado", Schema = "Cobranza")]
    public class TB_LayoutGenerado
    {
        [Key]
        public int IdLayoutGenerado { get; set; }
        public int? IdGrupoLayout { get; set; }
        public int? IdEstatus { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaTermino { get; set; }
        public string ResultadoMsg { get; set; }
        public int? IdUsuarioRegistro { get; set; }
        public bool? EsAutomatico { get; set; }
    }
}
