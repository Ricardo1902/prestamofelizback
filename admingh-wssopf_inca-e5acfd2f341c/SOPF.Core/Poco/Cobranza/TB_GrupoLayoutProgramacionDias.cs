﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("TB_GrupoLayoutProgramacionDias", Schema = "Cobranza")]
    public class TB_GrupoLayoutProgramacionDias
    {
        [Key]
        public int IdProgramacion { get; set; }
        public int Dia { get; set; }
    }
}
