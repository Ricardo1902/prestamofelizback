﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
	[Table("TB_MotivoExclusionDomiciliacion", Schema = "Cobranza")]
	public class TB_MotivoExclusionDomiciliacion
	{
		[Key]
		public int IdMotivoExclusionDomiciliacion { get; set; }
		public string MotivoExclusion { get; set; }
		public DateTime FechaRegistro { get; set; }
		public bool Activo { get; set; }
	}
}