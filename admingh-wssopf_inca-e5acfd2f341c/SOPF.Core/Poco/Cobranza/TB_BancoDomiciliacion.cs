﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
	[Table("TB_BancoDomiciliacion", Schema = "Cobranza")]
	public class TB_BancoDomiciliacion
	{
		[Key]
		public int IdBancoDomiciliacion { get; set; }
		public int IdBanco { get; set; }
		public int IdMedioDomiciliacion { get; set; }
		public bool Activo { get; set; }
		public DateTime FechaRegistro { get; set; }
	}
}
