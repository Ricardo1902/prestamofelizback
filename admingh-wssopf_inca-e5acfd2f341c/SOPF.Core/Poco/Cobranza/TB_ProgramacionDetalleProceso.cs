﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("TB_ProgramacionDetalleProceso", Schema = "Cobranza")]
    public class TB_ProgramacionDetalleProceso
    {
        [Key]
        public int IdProgramacionDetallePro { get; set; }
        public int IdProgramacionDetalle { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int IdIntervalo { get; set; }
        public int ValorIntervalo { get; set; }
        public DateTime FechaEjecucionMax { get; set; }
        public DateTime? FechaUltimoProceso { get; set; }
        public bool Activo { get; set; }
        public string Mensaje { get; set; }
        public int IdProgramacionEstatus { get; set; }
    }
}