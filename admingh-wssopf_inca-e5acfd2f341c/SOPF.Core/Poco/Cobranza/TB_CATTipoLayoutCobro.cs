﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("TB_CATTipoLayoutCobro", Schema = "Cobranza")]
    public class TB_CATTipoLayoutCobro
    {
        [Key]
        public int IdTipoLayoutCobro { get; set; }
        public string TipoLayout { get; set; }
        public bool? Activo { get; set; }
        public int? IdBanco { get; set; }
        public int? IdLayoutArchivo { get; set; }
    }
}