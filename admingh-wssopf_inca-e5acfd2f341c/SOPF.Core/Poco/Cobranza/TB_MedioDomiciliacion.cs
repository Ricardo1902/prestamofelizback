﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Cobranza
{
    [Table("TB_MedioDomiciliacion", Schema = "Cobranza")]
    public class TB_MedioDomiciliacion
    {
        [Key]
        public int IdMedioDomiciliacion { get; set; }
        public string Medio { get; set; }
        public bool? Activo { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}