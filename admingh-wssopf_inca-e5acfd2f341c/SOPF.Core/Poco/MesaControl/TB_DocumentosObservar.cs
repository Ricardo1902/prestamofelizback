﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.MesaControl
{
    [Table("TB_DocumentosObservar", Schema = "MesaControl")]
    public class TB_DocumentosObservar
    {
        [Key]
        public int IdDocumentoObservar { get; set; }
        public decimal Solicitud_Id { get; set; }
        public int Documento_Id { get; set; }
        public int Usuario_Id { get; set; }
        public string Comentarios { get; set; }
        public bool RegistroActual { get; set; }
        public DateTime? Fch_Registro { get; set; }
    }
}
