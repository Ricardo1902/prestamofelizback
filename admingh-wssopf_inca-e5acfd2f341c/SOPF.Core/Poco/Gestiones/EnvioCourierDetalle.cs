﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("EnvioCourierDetalle", Schema = "gestiones")]
    public class EnvioCourierDetalle
    {
        [Key]
        public int IdEnvioCourierDetalle { get; set; }
        public int IdEnvioCourier { get; set; }
        public int IdSolicitudNotificacion { get; set; }
        public bool Error { get; set; }
        public string Mensaje { get; set; }
        public bool Actual { get; set; }
    }
}
