﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_CATTipoNotificaciones", Schema = "Gestiones")]
    public class TB_CATTipoNotificaciones
    {
        [Key]
        public int IdTipoNotificacion { get; set; }
        public string TipoNotificacion { get; set; }
        public string Descripcion { get; set; }
        public int UsuarioRegistroId { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Actual { get; set; }
    }
}