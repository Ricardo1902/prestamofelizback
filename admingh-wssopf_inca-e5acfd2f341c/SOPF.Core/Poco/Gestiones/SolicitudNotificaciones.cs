﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("SolicitudNotificaciones", Schema = "Gestiones")]
    public class SolicitudNotificaciones
    {
        [Key]
        public int IdSolicitudNotificacion { get; set; }
        public decimal SolicitudId { get; set; }
        public int NotificacionId { get; set; }
        public int DestinoId { get; set; }
        public int UsuarioRegistroId { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int? UsuarioEscaneoId { get; set; }
        public DateTime? FechaEscaneo { get; set; }
        public bool Actual { get; set; }
        public string Correo { get; set; }
        public bool EsCourier { get; set; }
    }
}
