﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_Asignacion", Schema = "Gestiones")]
    public class TB_Asignacion
    {
        [Key]
        public int IdAsignacion { get; set; }
        public int? IdGestor { get; set; }
        public decimal? IdCuenta { get; set; }
        public DateTime? FechaAsignacion { get; set; }
        public int? UsuarioidAsigno { get; set; }
        public bool? EstatusAsignacion { get; set; }
        public DateTime? Fch_Modifi { get; set; }
        public int? IdTipoAsignacion { get; set; }
        public DateTime? VigenciaAsignacion { get; set; }
    }
}
