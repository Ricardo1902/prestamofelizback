﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_Gestion", Schema = "gestiones")]
    public class TB_Gestion
    {
        [Key]
        public int IdGestion { get; set; }
        public DateTime? fch_Gestion { get; set; }
        public int? idGestor { get; set; }
        public string Comentario { get; set; }
        public int? IdResultadoGestion { get; set; }
        public decimal? idCuenta { get; set; }
        public string TipoGestion { get; set; }
        public DateTime? fch_modificacion { get; set; }
        public int? idCNT { get; set; }
        public int? idCampana { get; set; }
        public int? IdTipoGestion { get; set; }
        public decimal? latitud { get; set; }
        public decimal? longitud { get; set; }
    }
}
