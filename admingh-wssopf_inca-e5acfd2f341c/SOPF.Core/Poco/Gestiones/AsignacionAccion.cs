﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("AsignacionAccion", Schema = "Gestiones")]
    public class AsignacionAccion
    {
        [Key]
        public int IdAsignacionAccion { get; set; }
        public int IdAsignacion { get; set; }
        public int IdTipoAccion { get; set; }
        public string DescipcionActividad { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}