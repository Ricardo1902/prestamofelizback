﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_DevolverLlamada", Schema = "gestiones")]
    public class TB_DevolverLlamada
    {
        [Key]
        public int IdDevLlamada { get; set; }
        public int? IdGestion { get; set; }
        public decimal? IdCuenta { get; set; }
        public DateTime? Fch_DevLlamada { get; set; }
        public int? IdCliente { get; set; }
        public int? IdGestor { get; set; }
        public string Telefono { get; set; }
        public string Tipo { get; set; }
        public string AnexoReferencia { get; set; }
        public string Comentario { get; set; }
    }
}
