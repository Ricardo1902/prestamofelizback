﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_TipoGestionPromesa", Schema = "gestiones")]
    public class TB_TipoGestionPromesa
    {
        [Key]
        public int IdTipoGestionPromesa { get; set; }
        public int IdTipoGestion { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
