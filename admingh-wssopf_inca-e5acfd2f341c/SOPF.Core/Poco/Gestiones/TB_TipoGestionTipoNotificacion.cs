﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_TipoGestionTipoNotificacion", Schema = "gestiones")]
    public class TB_TipoGestionTipoNotificacion
    {
        [Key]
        public int IdTipoGestTipoNot { get; set; }
        public int IdTipoGestion { get; set; }
        public int IdTipoNotificacion { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
