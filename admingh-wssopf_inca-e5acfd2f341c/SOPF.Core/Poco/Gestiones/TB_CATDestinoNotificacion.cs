﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_CATDestinoNotificacion", Schema = "Gestiones")]
    public class TB_CATDestinoNotificacion
    {
        [Key]
        public int IdDestinoNotificacion { get; set; }
        public string DestinoNotificacion { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }
    }
}
