﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("SolicitudNotificacionDetalle", Schema = "Gestiones")]
    public class SolicitudNotificacionDetalle
    {
        [Key]
        public int IdSolicitudNotificacionDetalle { get; set; }
        public int SolicitudNotificacionId { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Direccion { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public decimal TotalDeuda { get; set; }
        public int CuotasVencidas { get; set; }
        public int IdCliente { get; set; }
        public DateTime? FechaHoraCitacion { get; set; }
        public decimal? DescuentoDeuda { get; set; }
        public DateTime? FechaVigencia { get; set; }
    }
}