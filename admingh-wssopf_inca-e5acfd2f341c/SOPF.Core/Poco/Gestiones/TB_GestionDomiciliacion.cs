﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_GestionDomiciliacion", Schema = "gestiones")]
    public class TB_GestionDomiciliacion
    {
        [Key]
        public int IdGestionDomiciliacion { get; set; }
        public int IdSolicitud { get; set; }
        public int IdGestion { get; set; }
        public int IdTipoGestion { get; set; }
        public DateTime FechaDomiciliar { get; set; }
        public decimal Monto { get; set; }
        public bool? EnviadoDom { get; set; }
        public int? IdUsuarioEnviaDom { get; set; }
        public DateTime? FechaEnvioDom { get; set; }
        public int? IdPeticionDomiciliacion { get; set; }
        public string RespuestaDom { get; set; }
        public DateTime? FechaRespuestaDom { get; set; }
        public int? IdResultadoGestion { get; set; }
        public int IdEstatus { get; set; }
    }
}