﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_CATResultadoGestion", Schema = "gestiones")]
    public class TB_CATResultadoGestion
    {
        [Key]
        public int IdResultado { get; set; }
        public string Descripcion { get; set; }
        public bool? Estatus { get; set; }
        public int? TipoGestion { get; set; }
        public int? Color_Id { get; set; }
    }
}