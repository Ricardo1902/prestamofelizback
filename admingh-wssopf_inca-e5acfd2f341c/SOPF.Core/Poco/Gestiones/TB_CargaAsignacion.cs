﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_CargaAsignacion", Schema = "gestiones")]
    public class TB_CargaAsignacion
    {
        [Key]
        public int IdCargaAsignacion { get; set; }
        public string NombreArchivo { get; set; }
        public int? TotalRegistros { get; set; }
        public string Actividad { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int IdEstatus { get; set; }
        public bool Activo { get; set; }
    }
}
