﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TipoGestorContacto", Schema = "Gestiones")]
    public class TipoGestorContacto
    {
        [Key]
        public int IdTipoGestorContacto { get; set; }
        public string Contacto { get; set; }
        public bool Activo { get; set; }
    }
}
