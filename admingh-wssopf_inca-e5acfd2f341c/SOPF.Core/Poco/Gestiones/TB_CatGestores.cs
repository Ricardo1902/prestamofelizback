﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_CatGestores", Schema = "Gestiones")]
    public class TB_CatGestores
    {
        [Key]
        public int IdGestor { get; set; }
        public string Nombre { get; set; }
        public int? IdTipo { get; set; }
        public int? Usuarioid { get; set; }
        public bool? Estatus { get; set; }
        public DateTime? FechaAlta { get; set; }
        public int? UsuarioIdAlta { get; set; }
        public DateTime? FechaModifico { get; set; }
        public int? UsuarioIdModifico { get; set; }
        public DateTime? UltimaAsignacion { get; set; }
    }
}
