﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TipoArchivoGestion", Schema = "Gestiones")]
    public class TipoArchivoGestion: IComparable<TipoArchivoGestion>
    {
        [Key]
        public int IdTipoArchivoGestion { get; set; }
        [MaxLength(100)]
        public string TipoArchivo { get; set; }
        [MaxLength(10)]
        public string Extension { get; set; }
        [MaxLength(100)]
        public string TipoMime { get; set; }
        [MaxLength(50)]
        public string Icono { get; set; }
        public bool PermiteDescarga { get; set; }

        public int CompareTo(TipoArchivoGestion other)
        {
            int diferencias = 0;
            diferencias += Math.Abs(IdTipoArchivoGestion.CompareTo(other.IdTipoArchivoGestion));
            diferencias += Math.Abs((String.IsNullOrEmpty(TipoArchivo) ? "" : TipoArchivo).CompareTo(String.IsNullOrEmpty(other.TipoArchivo) ? "" : other.TipoArchivo));
            diferencias += Math.Abs((String.IsNullOrEmpty(Extension) ? "" : Extension).CompareTo(String.IsNullOrEmpty(other.Extension) ? "" : other.Extension));
            diferencias += Math.Abs((String.IsNullOrEmpty(TipoMime) ? "" : TipoMime).CompareTo(String.IsNullOrEmpty(other.TipoMime) ? "" : other.TipoMime));
            diferencias += Math.Abs((String.IsNullOrEmpty(Icono) ? "" : Icono).CompareTo(String.IsNullOrEmpty(other.Icono) ? "" : other.Icono));
            diferencias += Math.Abs(PermiteDescarga.CompareTo(other.PermiteDescarga));
            return diferencias;
        }
    }
}
