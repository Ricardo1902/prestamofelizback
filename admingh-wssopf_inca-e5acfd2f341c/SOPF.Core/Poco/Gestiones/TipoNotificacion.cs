﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_CATTipoNotificaciones", Schema = "Gestiones")]
    public class TipoNotificacion
    {
        [Key]
        public int IdTipoNotificacion { get; set; }
        [Column("TipoNotificacion")]
        [MaxLength(150)]
        public string Nombre { get; set; }
        [MaxLength(500)]
        public string Descripcion { get; set; }
        [Column("UsuarioRegistroId")]
        public int IdUsuarioRegistro { get; set; } //Default
        public DateTime FechaRegistro { get; set; } //Default
        public bool Actual { get; set; } //Default
    }
}
