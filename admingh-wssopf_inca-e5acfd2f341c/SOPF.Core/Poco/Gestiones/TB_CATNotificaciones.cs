﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_CATNotificaciones", Schema = "Gestiones")]
    public class TB_CATNotificaciones
    {
        [Key]
        public int IdNotificacion { get; set; }
        public int TipoNotificacionId { get; set; }
        public string Notificacion { get; set; }
        public string Descripcion { get; set; }
        public string NombreArchivo { get; set; }
        public string Clave { get; set; }
        public int Orden { get; set; }
        public int UsuarioRegistroId { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Actual { get; set; }
    }
}