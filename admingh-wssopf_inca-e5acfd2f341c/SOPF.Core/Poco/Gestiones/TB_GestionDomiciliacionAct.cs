﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_GestionDomiciliacionAct", Schema = "gestiones")]
	public class TB_GestionDomiciliacionAct
	{
		[Key]
		public int IdGestionDomiciliacionAct { get; set; }
		public int IdGestionDomiciliacion { get; set; }
		public int IdUsuario { get; set; }
		public DateTime FechaRegistro { get; set; }
		public int? IdEstatus { get; set; }
		public string Comentario { get; set; }
	}
}
