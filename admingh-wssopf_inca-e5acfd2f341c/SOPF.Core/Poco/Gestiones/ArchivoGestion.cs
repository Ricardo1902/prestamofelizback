﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("ArchivoGestion", Schema = "Gestiones")]
    public class ArchivoGestion: IComparable<ArchivoGestion>
    {
        [Key]
        public int IdArchivoGestion { get; set; }
        [MaxLength(250)]
        public string Nombre { get; set; }
        [MaxLength(250)]
        public string Comentario { get; set; }
        public int IdTipoArchivo { get; set; }
        public int IdDocumentoDestino { get; set; }
        public int IdServidorArchivos { get; set; }
        [MaxLength(50)]
        public string IdArchivoServidor { get; set; }
        public string UrlArchivo { get; set; }
        public int IdSolicitud { get; set; }
        public int IdUsuarioRegistro { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }

        public int CompareTo(ArchivoGestion other)
        {
            int diferencias = 0;
            diferencias += Math.Abs(IdArchivoGestion.CompareTo(other.IdArchivoGestion));
            diferencias += Math.Abs((String.IsNullOrEmpty(Nombre) ? "" : Nombre).CompareTo(String.IsNullOrEmpty(other.Nombre) ? "" : other.Nombre));
            diferencias += Math.Abs((String.IsNullOrEmpty(Comentario) ? "" : Comentario).CompareTo(String.IsNullOrEmpty(other.Comentario) ? "" : other.Comentario));
            diferencias += Math.Abs(IdTipoArchivo.CompareTo(other.IdTipoArchivo));
            diferencias += Math.Abs(IdDocumentoDestino.CompareTo(other.IdDocumentoDestino));
            diferencias += Math.Abs((String.IsNullOrEmpty(IdArchivoServidor) ? "" : IdArchivoServidor).CompareTo(String.IsNullOrEmpty(other.IdArchivoServidor) ? "" : other.IdArchivoServidor));
            diferencias += Math.Abs((String.IsNullOrEmpty(UrlArchivo) ? "" : UrlArchivo).CompareTo(String.IsNullOrEmpty(other.UrlArchivo) ? "" : other.UrlArchivo));
            diferencias += Math.Abs(IdSolicitud.CompareTo(other.IdSolicitud));
            diferencias += Math.Abs(IdUsuarioRegistro.CompareTo(other.IdUsuarioRegistro));
            return diferencias;
        }
    }
}
