﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_TipoGestionDomLayoutArchivo", Schema = "gestiones")]
    public class TB_TipoGestionDomLayoutArchivo
    {
        [Key]
        public int IdTipoGestionDomLayoutArchivo { get; set; }
        public int IdTipoGestionDomiciliacion { get; set; }
        public int IdLayoutArchivo { get; set; }
        public bool Activo { get; set; }
    }
}
