﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_CATSolNotTipoAcciones", Schema = "Gestiones")]
    public class TB_CATSolNotTipoAcciones
    {
        [Key]
        public int IdSolNotTipoAccion { get; set; }
        public string SolNotTipoAccion { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
    }
}
