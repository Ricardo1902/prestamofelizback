﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("Cattipocontaco", Schema = "Gestiones")]
    public class CatTipoContaco
    {
        [Key]
        public int id { get; set; }
        public string contacto { get; set; }
        public bool? estatus { get; set; }
        public int IdTipoGestion { get; set; }
    }
}
