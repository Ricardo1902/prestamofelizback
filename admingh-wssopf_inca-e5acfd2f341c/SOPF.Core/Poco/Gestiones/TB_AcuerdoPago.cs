﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_AcuerdoPago", Schema = "gestiones")]
    public class TB_AcuerdoPago
    {
        [Key]
        public int IdAcuerdoPago { get; set; }
        public int IdSolicitud { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public int TotalRegistros { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }
    }
}
