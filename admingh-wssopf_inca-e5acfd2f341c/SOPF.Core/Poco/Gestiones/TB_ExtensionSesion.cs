﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
	[Table("TB_ExtensionSesion", Schema = "gestiones")]
	public class TB_ExtensionSesion
	{
		[Key]
		public int IdExtensionSesion { get; set; }
		public int IdConexionSipExtension { get; set; }
		public int IdUsuario { get; set; }
		public DateTime FechaRegistro { get; set; }
		public DateTime? FechaFin { get; set; }
		public int? IdGestion { get; set; }
	}
}
