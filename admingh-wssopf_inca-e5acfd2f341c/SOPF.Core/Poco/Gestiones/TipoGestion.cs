﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TipoGestion", Schema = "gestiones")]
    public class TipoGestion
    {
        [Key]
        public int IdTipoGestion { get; set; }
        public string Tipo { get; set; }
        public bool Activo { get; set; }
        public int? IdTipoGestorContacto { get; set; }
        public string Clave { get; set; }
        public bool? GeneraRecordatorio { get; set; }
    }
}
