﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_CATNotificaciones", Schema = "Gestiones")]
    public class Notificacion
    {
        [Key]
        public int IdNotificacion { get; set; }
        [Column("TipoNotificacionId")]
        public int IdTipoNotificacion { get; set; }
        [Column("Notificacion")]
        [MaxLength(150)]
        public string Nombre { get; set; }
        [MaxLength(500)]
        public string Descripcion { get; set; }
        [MaxLength(100)]
        public string NombreArchivo { get; set; }
        [MaxLength(10)]
        public string Clave { get; set; }
        public int Orden { get; set; }
        [Column("UsuarioRegistroId")]
        public int IdUsuarioRegistro { get; set; } //Default
        public DateTime FechaRegistro { get; set; } //Default
        public bool Actual { get; set; } //Default
    }
}
