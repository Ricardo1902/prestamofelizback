﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_CargaAsignacionDetalle", Schema = "gestiones")]
    public class TB_CargaAsignacionDetalle
    {
        [Key]
        public int IdCargaAsignacionDetalle { get; set; }
        public int IdCargaAsignacion { get; set; }
        public int IdSolicitud { get; set; }
        public int? IdGestor { get; set; }
        public string Gestor { get; set; }
        public string Actividad { get; set; }
        public DateTime? FechaPago { get; set; }
        public int? IdAsignacion { get; set; }
        public bool? Error { get; set; }
        public string Mensaje { get; set; }
        public bool Activo { get; set; }
    }
}
