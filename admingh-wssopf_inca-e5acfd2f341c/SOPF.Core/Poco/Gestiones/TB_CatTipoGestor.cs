﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("TB_CatTipoGestor", Schema = "gestiones")]
    public class TB_CatTipoGestor
    {
        [Key]
        public int IdTipoGestor { get; set; }
        public string Descripcion { get; set; }
        public bool? Estatus { get; set; }
        public DateTime? FechaAlta { get; set; }
        public int? UsuarioidAlta { get; set; }
        public DateTime? FechaModificacon { get; set; }
        public int? UsuarioidModifica { get; set; }
    }
}
