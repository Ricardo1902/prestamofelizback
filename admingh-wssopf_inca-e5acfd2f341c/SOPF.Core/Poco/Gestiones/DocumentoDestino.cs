﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("DocumentoDestino", Schema = "Gestiones")]
    public class DocumentoDestino
    {
        [Key]
        public int IdDocumentoDestino { get; set; }
        public int IdNotificacion { get; set; }
        public int IdDestinoNotificacion { get; set; }
    }
}
