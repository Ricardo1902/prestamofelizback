﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
    [Table("SolicitudNotificacionAcciones", Schema = "Gestiones")]
    public class SolicitudNotificacionAcciones
    {
        [Key]
        public int IdSolNotAccion { get; set; }
        public int SolicitudNotificacionId { get; set; }
        public int SolNotTipoAccionId { get; set; }
        public int UsuarioId { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
