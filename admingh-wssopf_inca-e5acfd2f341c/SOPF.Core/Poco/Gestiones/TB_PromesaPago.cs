﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Gestiones
{
	[Table("TB_PromesaPago", Schema = "gestiones")]
	public class TB_PromesaPago
	{
		[Key]
		public int idPromesa { get; set; }
		public int? idGestion { get; set; }
		public decimal? idCuenta { get; set; }
		public DateTime? Fch_Promesa { get; set; }
		public decimal? MontoPromesa { get; set; }
		public int? Estatus { get; set; }
		public int? idTipoCobro { get; set; }
		public int? IdGestor { get; set; }
		public DateTime? FechaSis { get; set; }
		public int? Cumplio { get; set; }
		public int? IdGestorOri { get; set; }
	}
}