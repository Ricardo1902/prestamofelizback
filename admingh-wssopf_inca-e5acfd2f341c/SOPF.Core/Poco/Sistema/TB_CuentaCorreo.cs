﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Sistema
{
    [Table("TB_CuentaCorreo", Schema = "Sistema")]
    public class TB_CuentaCorreo
    {
        [Key]
        public int IdCuentaCorreo { get; set; }
        public string Servidor { get; set; }
        public int Puerto { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public bool UsaSSL { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}