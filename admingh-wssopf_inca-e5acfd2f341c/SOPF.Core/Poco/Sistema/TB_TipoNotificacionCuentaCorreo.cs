﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Sistema
{
    [Table("TB_TipoNotificacionCuentaCorreo", Schema = "Sistema")]
    public class TB_TipoNotificacionCuentaCorreo
    {
        [Key]
        public int IdTipoNotCuentaCo { get; set; }
        public int IdTipoNotificacion { get; set; }
        public int IdCuentaCorreo { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }
    }
}
