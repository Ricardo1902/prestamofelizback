﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Sistema
{
    [Table("TB_Notificacion", Schema = "Sistema")]
    public class TB_Notificacion
    {
        [Key]
        public int IdNotificacion { get; set; }
        public decimal? IdCliente { get; set; }
        public decimal? IdSolicitud { get; set; }
        public int IdTipoNotificacion { get; set; }
        public string Titulo { get; set; }
        public string CorreoElectronico { get; set; }
        public string NumeroTelefono { get; set; }
        public string NombreCliente { get; set; }
        public decimal? CapitalSolicitado { get; set; }
        public decimal? Erogacion { get; set; }
        public int? TotalRecibos { get; set; }
        public int Intentos { get; set; }
        public int MaxIntentos { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Enviado { get; set; }
        public DateTime? FechaEnvio { get; set; }
        public string Url { get; set; }
        public string Clave { get; set; }
        public DateTime? FechaHoraCitacion { get; set; }
        public decimal? DescuentoDeuda { get; set; }
        public DateTime? FechaVigencia { get; set; }
        public int? IdDestino { get; set; }
        public bool? Courier { get; set; }
        public bool? Actualizar { get; set; }
        public int? IdGestion { get; set; }
        public DateTime? FechaEnviar { get; set; }
        public bool? Programado { get; set; }
        public string CorreoCopia { get; set; }
    }
}