﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Sistema
{
    [Table("TB_PeticionOtpValidar", Schema = "Sistema")]
    public class TB_PeticionOtpValidar
    {
        [Key]
        public int IdPeticionOtpValidar { get; set; }
        public int IdPeticionOtp { get; set; }
        public string CodigoOtp { get; set; }
        public int IdUsuario { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Correcto { get; set; }
        public string Mensaje { get; set; }
    }
}