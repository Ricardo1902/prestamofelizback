﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Sistema
{
    [Table("TB_TipoNotificacionSMS", Schema = "Sistema")]
    public class TB_TipoNotificacionSMS
    {
        [Key]
        public int IdTipoNotificacionSMS { get; set; }
        public int IdTipoNotificacion { get; set; }
        public int? TiempoVigencia { get; set; }
        public string Formato { get; set; }
        public int Longitud { get; set; }
        public int MaxIntentos { get; set; }
        public string Certificar { get; set; }
        public string RazonSocialCertificado { get; set; }
        public string NumFiscalCertificado { get; set; }
        public string Mensaje { get; set; }
        public string Remitente { get; set; }
        public string AsuntoCorreo { get; set; }
        public bool Activo { get; set; }
    }
}
