﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Sistema
{
    [Table("TB_ConexionSipSdk", Schema = "Sistema")]
    public class TB_ConexionSipSdk
    {
        [Key]
        public int IdConexionSipSdk { get; set; }
        public int IdConexionSip { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }
    }
}
