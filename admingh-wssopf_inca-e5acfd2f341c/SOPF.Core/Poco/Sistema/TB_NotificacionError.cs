﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Sistema
{
    [Table("TB_NotificacionError", Schema = "Sistema")]
    public class TB_NotificacionError
    {
        [Key]
        public int IdNotificacionError { get; set; }
        public int IdNotificacion { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string MensajeError { get; set; }
    }
}