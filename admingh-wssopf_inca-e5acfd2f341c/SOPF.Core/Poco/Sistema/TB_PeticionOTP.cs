﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Sistema
{
    [Table("TB_PeticionOTP", Schema = "Sistema")]
    public class TB_PeticionOTP
    {
        [Key]
        public int IdPeticionOTP { get; set; }
        public int IdUsuario { get; set; }
        public int? IdSolicitud { get; set; }
        public string DNI { get; set; }
        public string Celular { get; set; }
        public string Correo { get; set; }
        public string MedioEnvio { get; set; }
        public string TipoNotificacion { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int? IdPeticion { get; set; }
        public bool Activo { get; set; }
        public string Mensaje { get; set; }
    }
}