﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Sistema
{
    [Table("TB_TipoNotificacion", Schema = "Sistema")]
    public class TB_TipoNotificacion
    {
        [Key]
        public int IdTipoNotificacion { get; set; }
        public string Clave { get; set; }
        public string Tipo { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string TituloCorreo { get; set; }
        public string CorreoCopia { get; set; }
        public bool EnvioProgramado { get; set; }
        public bool Activo { get; set; }
        public int MinutosRecurrencia { get; set; }
        public int? HorasRecurrencia { get; set; }
    }
}