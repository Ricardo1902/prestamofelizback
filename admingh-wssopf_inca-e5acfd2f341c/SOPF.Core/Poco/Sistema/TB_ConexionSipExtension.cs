﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Sistema
{
    [Table("TB_ConexionSipExtension", Schema = "Sistema")]
    public class TB_ConexionSipExtension
    {
        [Key]
        public int IdConexionSipExtension { get; set; }
        public int IdConexionSip { get; set; }
        public int Extension { get; set; }
        public bool Elegible { get; set; }
        public bool Activo { get; set; }
    }
}
