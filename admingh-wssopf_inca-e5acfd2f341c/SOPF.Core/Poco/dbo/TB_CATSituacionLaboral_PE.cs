﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATSituacionLaboral_PE", Schema = "dbo")]
    public class TB_CATSituacionLaboral_PE
    {
        [Key]
        public int IDSituacion { get; set; }
        public string Situacion { get; set; }
        public int? IDEstatus { get; set; }
    }
}
