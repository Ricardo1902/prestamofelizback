﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATFormatos", Schema = "dbo")]
    public class TB_CATFormatos
    {
        [Key]
        public int idFormato { get; set; }
        public string Formato { get; set; }
        public string Ruta { get; set; }
        public int? IdEstatus { get; set; }
        public int? TipoProducto { get; set; }
        public string NombrePlantilla { get; set; }
        public string NombreDescarga { get; set; }
        public DateTime Fec_IniUso { get; set; }
        public DateTime Fec_FinUso { get; set; }
        public DateTime Fec_IniImp { get; set; }
        public DateTime Fec_FinImp { get; set; }
        public int? ChecaHisCre { get; set; }
        public int? IdPolitica { get; set; }
        public int? IdPoliticaPenaliz { get; set; }
    }
}