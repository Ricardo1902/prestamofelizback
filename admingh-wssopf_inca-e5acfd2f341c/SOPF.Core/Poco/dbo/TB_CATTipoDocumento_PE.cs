﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATTipoDocumento_PE", Schema = "dbo")]
    public class TB_CATTipoDocumento_PE
    {
        [Key]
        public int IDTipoDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public int? IDEstatus { get; set; }
    }
}