﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATColonia", Schema = "dbo")]
    public class TB_CATColonia
    {
        [Key]
        public decimal IdColonia { get; set; }
        public string Colonia { get; set; }
        public int IdCiudad { get; set; }
        public int cp { get; set; }
        public string z_ec { get; set; }
        public string lada { get; set; }
        public int? IdEstatus { get; set; }
        public int? UDIS { get; set; }
    }
}
