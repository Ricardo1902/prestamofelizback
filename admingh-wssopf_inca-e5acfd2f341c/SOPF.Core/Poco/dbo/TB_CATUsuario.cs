﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATUsuario", Schema = "dbo")]
    public class TB_CATUsuario
    {
        [Key]
        public int IdUsuario { get; set; }
        public int IdDepartamento { get; set; }
        public String NombreCompleto { get; set; }
        public String Nombres { get; set; }
        public String ApellidoP { get; set; }
        public String ApellidoM { get; set; }
        public String RFC { get; set; }
        public String lada { get; set; }
        public String Telefono { get; set; }
        public String Email { get; set; }
        public String Usuario { get; set; }
        public Byte[] Clave { get; set; }
        public int? IdTipoUsuario { get; set; }
        public int? IdSucursal { get; set; }
        public int? IdEstatus { get; set; }
        public int? Bloquear { get; set; }
        public int? EnSession { get; set; }
        public DateTime? Fch_Session { get; set; }
        public DateTime? Fch_Bloqueo { get; set; }
        public Boolean? SoloOficina { get; set; }
        public Boolean? Inicializar { get; set; }
    }
}
