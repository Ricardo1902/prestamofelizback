//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATBanco", Schema = "dbo")]
    public class TB_CATBanco
    {
        [Key]
        public short IdBanco { get; set; }
        public string Banco { get; set; }
        public string Descripcion { get; set; }
        public string Clave { get; set; }
        public int IdEstatus { get; set; }
        public bool? Dispersion { get; set; }
    }
}