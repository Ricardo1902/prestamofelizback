﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("SI_Pais", Schema = "dbo")]
    public class SI_Pais
    {
        [Key]
        public int Clave { get; set; }
        public string Pais { get; set; }
    }
}