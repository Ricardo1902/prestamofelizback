﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATDireccion_PE", Schema = "dbo")]
    public class TB_CATDireccion_PE
    {
        [Key]
        public int IDDireccion { get; set; }
        public string Direccion { get; set; }
        public int? IDEstatus { get; set; }
        public string Clave { get; set; }
    }
}