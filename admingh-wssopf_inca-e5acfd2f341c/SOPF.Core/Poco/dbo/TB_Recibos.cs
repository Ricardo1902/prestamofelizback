﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_Recibos", Schema = "dbo")]
    public class TB_Recibos
    {
        [Key, Column(Order = 0)]
        public decimal IdCuenta { get; set; }
        [Key, Column(Order = 1)]
        public int recibo { get; set; }
        public DateTime? fch_recibo { get; set; }
        public DateTime fch_emision { get; set; }
        public decimal capital { get; set; }
        public decimal interes { get; set; }
        public decimal? iva_intere { get; set; }
        public decimal? sdo_insoluto { get; set; }
        public decimal? sdo_recibo { get; set; }
        public int IdEstatus { get; set; }
        public decimal? totalinteres { get; set; }
        public int? IdTipoRecibo { get; set; }
        public decimal? sdo_capita { get; set; }
        public decimal? sdo_intere { get; set; }
        public decimal? sdo_iva_in { get; set; }
        public decimal? interes_gto_cob { get; set; }
        public decimal? iva_gto_cob { get; set; }
        public DateTime? Fch_Inicio { get; set; }
        public DateTime? Fch_Vencimiento { get; set; }
        public decimal? otrosCargos { get; set; }
        public decimal? iva_OtrosCargos { get; set; }
        public DateTime? fch_Termino { get; set; }
        public decimal? IGV { get; set; }
        public decimal? seguro { get; set; }
        public decimal? GAT { get; set; }
        public decimal? sdo_igv { get; set; }
        public decimal? sdo_seguro { get; set; }
        public decimal? sdo_gat { get; set; }
    }
}
