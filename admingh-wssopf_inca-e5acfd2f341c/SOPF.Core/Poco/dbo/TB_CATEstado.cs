﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATEstado", Schema = "dbo")]
    public class TB_CATEstado
    {
        [Key]
        public int IdEstado { get; set; }
        public string Estado { get; set; }
        public int? IdEstatus { get; set; }
        public string ClaveCC { get; set; }
        public int? UDEP { get; set; }
    }
}