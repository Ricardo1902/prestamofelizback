﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATEstatus", Schema = "dbo")]
    public class TB_CATEstatus
    {
        [Key]
        public int IdEstatus { get; set; }        
        public int IdTipoEstatus { get; set; }
        public string Estatus { get; set; }
        public string Descripcion { get; set; }
        public string Ayuda { get; set; }
    }
}