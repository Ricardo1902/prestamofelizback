﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_Solicitudes", Schema = "dbo")]
    public class TB_Solicitudes
    {
        [Key]
        public decimal IdSolicitud { get; set; }
        public DateTime fch_Solicitud { get; set; }
        public decimal IdCliente { get; set; }
        public decimal IdCliente_A { get; set; }
        public int? IdConvenio { get; set; }
        public int? IdPromotor { get; set; }
        public int? IdLinea { get; set; }
        public decimal? erogacion { get; set; }
        public decimal? capital { get; set; }
        public decimal? interes { get; set; }
        public decimal? iva_intere { get; set; }
        public int? IdSucursal { get; set; }
        public int? IdEstatus { get; set; }
        public DateTime? fch_estatus { get; set; }
        public int? IdMotivo { get; set; }
        public string Observacion { get; set; }
        public int? Pend_obse { get; set; }
        public decimal? ingr_liqui { get; set; }
        public decimal? capa_pago { get; set; }
        public int? IdAnalista { get; set; }
        public decimal? articulos { get; set; }
        public decimal? apertura { get; set; }
        public decimal? otro_carg { get; set; }
        public decimal? enganche { get; set; }
        public DateTime? fch_enganche { get; set; }
        public DateTime? fch_arranque { get; set; }
        [Column("override")]
        public int? Override { get; set; }
        public decimal? overpaid { get; set; }
        public int? IdProducto { get; set; }
        public int? IdOpero { get; set; }
        public string cve_cont_bc { get; set; }
        public decimal? cve_Autorizacion { get; set; }
        public string dg_articulo { get; set; }
        public int? Est_Proceso { get; set; }
        public DateTime? fch_Proceso { get; set; }
        public Int16? Papeleria { get; set; }
        public int? IdDestino { get; set; }
        public DateTime? fch_Generado { get; set; }
        public int? Mientras { get; set; }
        public string Pedido { get; set; }
        public string FinancieraPedido { get; set; }
        public DateTime? fch_Suscripcion { get; set; }
        public int? idClabeCliente { get; set; }
        public int? IdAsignado { get; set; }
        public int? isReestructura { get; set; }
        public int? LiqOtraInstitucion { get; set; }
        public DateTime? fch_asignacion { get; set; }
        public int? ClabeAnterior { get; set; }
        public int? IdCampania { get; set; }
        public int? IdClienteRecomienda { get; set; }
        public int? IdClabeClienteRec { get; set; }
        public int? IdTipoComision { get; set; }
        public int? IdRespuestaCC { get; set; }
        public int? ChecaHisCre { get; set; }
        public int? IdSucursalOrigen { get; set; }
        public int? IdFormato { get; set; }
        public int? IsOrdenPago { get; set; }
        public int? IDProducto_PE { get; set; }
        public int? IDCredito_PE { get; set; }
        public int? IDCanalVenta_PE { get; set; }
        public int? TipoCliente { get; set; }
        public int? IDTipoEvaluacion_PE { get; set; }
        public decimal? ImporteCredito { get; set; }
        public int? Plazo { get; set; }
        public decimal? TasaInteres { get; set; }
        public int? TipoSeguroDesgravamen { get; set; }
        public decimal? SeguroDesgravamen { get; set; }
        public decimal? MontoGAT { get; set; }
        public decimal? MontoUsoCanal { get; set; }
        public decimal? MontoTotalFinanciar { get; set; }
        public decimal? CostoTotalCred { get; set; }
        public decimal? MontoCuota { get; set; }
        public string Banco { get; set; }
        public string CuentaDesembolso { get; set; }
        public string CuentaDebito { get; set; }
        public DateTime? fch_1er_pago { get; set; }
        public string NumeroPoliza { get; set; }
        public string NumeroCertificado { get; set; }
        public int? idTipoCredito { get; set; }
        public DateTime? fch_deposito { get; set; }
        public decimal? MontoRetiroDia1 { get; set; }
        public decimal? MontoRetiroDia2 { get; set; }
        public decimal? MontoRetiroDia3 { get; set; }
        public decimal? SaldoIniCta { get; set; }
        public decimal? SaldoFinCta { get; set; }
        public decimal? MontoDep1 { get; set; }
        public decimal? MontoDep2 { get; set; }
        public decimal? MontoDep3 { get; set; }
        public decimal? SaldoIniCta2 { get; set; }
        public decimal? SaldoIniCta3 { get; set; }
        public decimal? SaldoFinCta2 { get; set; }
        public decimal? SaldoFinCta3 { get; set; }
        public decimal? Deducciones { get; set; }
        public string BCEntidad { get; set; }
        public string BCOficina { get; set; }
        public string BCDC { get; set; }
        public string BCCuenta { get; set; }
        public string ReferenciaExperian { get; set; }
        public decimal? ScoreExperian { get; set; }
        public int? TipoDomiciliacion { get; set; }
        public string CuentaCCI { get; set; }
        public string NumeroTarjetaVisa { get; set; }
        public int? MesExpiraTarjetaVisa { get; set; }
        public int? AnioExpiraTarjetaVisa { get; set; }
        public decimal? TMP_MontoLiquidacion { get; set; }
        public bool? CoincidenciaListaNegra { get; set; }
        public bool? ValidacionContactabilidad { get; set; }
        public int? idPoliticaAmpliacion { get; set; }
        public int? Origen_Id { get; set; }
        public decimal? ComisionOrigen { get; set; }
    }
}
