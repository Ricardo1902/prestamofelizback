﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATTipoCredito_PE", Schema = "dbo")]
    public class TB_CATTipoCredito_PE
    {
        [Key]
        public int IDCredito { get; set; }
        public string Credito { get; set; }
        public int? IDEstatus { get; set; }
    }
}
