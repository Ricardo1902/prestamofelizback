﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATTipoZona_PE", Schema = "dbo")]
    public class TB_CATTipoZona_PE
    {
        [Key]
        public int IDTipoZona { get; set; }
        public string TipoZona { get; set; }
        public int? IDEstatus { get; set; }
        public string Clave { get; set; }
    }
}