﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_Solicitudes_Reestructura", Schema = "dbo")]
    public class TB_Solicitudes_Reestructura
    {
        [Key]
        public int IdSolicitud { get; set; }
        public int IdCuenta { get; set; }
        public DateTime? FechaLiquidar { get; set; }
        public decimal? CapitalLiquidar { get; set; }
        public decimal? InteresLiquidar { get; set; }
        public decimal? IGVLiquidar { get; set; }
        public decimal? SeguroLiquidar { get; set; }
        public decimal? GATLiquidar { get; set; }
        public decimal? TotalLiquidar { get; set; }
        public int IdEstatus { get; set; }
        public string ComentariosSistemas { get; set; }
    }
}
