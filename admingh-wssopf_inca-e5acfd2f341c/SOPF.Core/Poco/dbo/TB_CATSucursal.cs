﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATSucursal", Schema = "dbo")]
    public class TB_CATSucursal
    {
        [Key]
        public int IdSucursal { get; set; }
        public int IdAfiliado { get; set; }
        public string Sucursal { get; set; }
        public string RFC { get; set; }
        public string Gerente { get; set; }
        public string direccion { get; set; }
        public int IdColonia { get; set; }
        public string lada { get; set; }
        public string telefono { get; set; }
        public string fax { get; set; }
        public decimal porce_comi { get; set; }
        public int IdEstatus { get; set; }
        public int QuincenasEnCamino { get; set; }
        public string cve_sucursal { get; set; }
        public int Producto { get; set; }
        public int RegionalId { get; set; }
        public int IdSucursalPadre { get; set; }
    }
}
