﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATPromotor", Schema = "dbo")]
    public class TB_CATPromotor
    {
        [Key]
        public int IdPromotor { get; set; }
        public string Promotor { get; set; }
        public string direccion { get; set; }
        public int? IdColonia { get; set; }
        public string lada { get; set; }
        public string telefono { get; set; }
        public string celular { get; set; }
        public string email { get; set; }
        public decimal? porc_efvo { get; set; }
        public decimal? porc_defvo { get; set; }
        public int? IdEstatus { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdTipoPromotor { get; set; }
    }
}