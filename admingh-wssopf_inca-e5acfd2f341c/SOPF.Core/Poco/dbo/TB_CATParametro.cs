﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATParametro", Schema = "dbo")]
    public class TB_CATParametro
    {
        [Key]
        public int IdParametro { get; set; }
        public string Parametro { get; set; }
        public string valor { get; set; }
        public string descripcion { get; set; }
        public int IdEstatus { get; set; }
    }
}
