﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATTipoUsuario", Schema = "dbo")]
    public class TB_CATTipoUsuario
    {
        [Key]
        public int IdTipoUsuario { get; set; }
        public String TipoUsuario { get; set; }
        public String Descripcion { get; set; }
        public int IdEstatus { get; set; }
    }
}
