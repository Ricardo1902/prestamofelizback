﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATCiudad", Schema = "dbo")]
    public class TB_CATCiudad
    {
        [Key]
        public int IdCiudad { get; set; }
        public string Ciudad { get; set; }
        public int? IdEstado { get; set; }
        public int? IdEstatus { get; set; }
        public int? UPRO { get; set; }
    }
}
