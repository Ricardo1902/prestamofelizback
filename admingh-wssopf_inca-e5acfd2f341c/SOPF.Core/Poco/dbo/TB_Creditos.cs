﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_Creditos", Schema = "dbo")]
    public class TB_Creditos
    {
        [Key]
		public decimal IdCuenta { get; set; }
        public DateTime fch_Credito { get; set; }
        public decimal IdSolicitud { get; set; }
        public string cve_i_cobr { get; set; }
        public decimal? erogacion { get; set; }
        public decimal? capital { get; set; }
        public decimal? interes { get; set; }
        public decimal? iva_intere { get; set; }
        public decimal? sdo_capita { get; set; }
        public decimal? sdo_intere { get; set; }
        public decimal? sdo_iva_in { get; set; }
        public decimal? moratorio { get; set; }
        public decimal? iva_morato { get; set; }
        public decimal? sdo_porApl { get; set; }
        public decimal? sdo_vencido { get; set; }
        public int? IdEstatus { get; set; }
        public DateTime? fch_estatus { get; set; }
        public int? IdMotivo { get; set; }
        public int? porcen_iva { get; set; }
        public int? no_rec_pag { get; set; }
        public int? dias_venc { get; set; }
        public int? dias_inac { get; set; }
        public int? dia_mas_ve { get; set; }
        public string esta_admin { get; set; }
        public DateTime? fch_ult_pago { get; set; }
        public DateTime? fch_Generado { get; set; }
        public int? Mientras { get; set; }
        public int? Reposicion { get; set; }
        public int? IdCuentaReposicion { get; set; }
        public int? GenReposicion { get; set; }
        public DateTime? fch_1er_pago { get; set; }
        public int? IdFondeo { get; set; }
        public DateTime? fch_Cobro { get; set; }
        public int? Est_Cobro { get; set; }
        public int? Pend_Obs { get; set; }
        public int? IdE_Fondeador { get; set; }
        public DateTime? fch_Liberado { get; set; }
        public int? IdTipoCobranza { get; set; }
        public decimal? idCuentaRest { get; set; }
        public string claveTipoInstrumento { get; set; }
        public int? cod_verif { get; set; }
        public DateTime? ControlUltimoPago { get; set; }
        public int? verif_reestructura { get; set; }
        public DateTime? Fch_Termino { get; set; }
    }
}
