﻿namespace SOPF.Core.Poco
{
    interface IValidable
    {
        bool ValidarClave();
        bool Validar();
    }
}
