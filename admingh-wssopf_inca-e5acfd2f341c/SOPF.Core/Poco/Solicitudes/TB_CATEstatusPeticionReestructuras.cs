﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_CATEstatusPeticionReestructuras", Schema = "Solicitudes")]
    public class TB_CATEstatusPeticionReestructuras
    {
        [Key]
        public int Id_EstatusPeticionReestructuras { get; set; }
        public string EstatusPeticionReestructura { get; set; }
        public string Descripcion { get; set; }
        public int EtapaPeticionReestructura_Id { get; set; }
        public bool Activo { get; set; }
    }
}
