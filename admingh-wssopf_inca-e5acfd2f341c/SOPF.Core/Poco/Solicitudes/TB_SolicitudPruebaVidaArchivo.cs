﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_SolicitudPruebaVidaArchivo", Schema = "Solicitudes")]
    public class TB_SolicitudPruebaVidaArchivo
    {
        [Key]
        public int IdSolicitudPruebaVidaArchivo { get; set; }
        public int IdSolicitudPruebaVida { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string IdGDrive { get; set; }
        public string NombreArchivo { get; set; }
        public bool Actual { get; set; }
    }
}
