﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_DocumentoXSolicitud", Schema = "Solicitudes")]
	public class TB_DocumentoXSolicitud
	{
		[Key]
		public int IdDocumentoSolicitud { get; set; }
		public int IdOrigen { get; set; }
		public int IdTipoCredito { get; set; }
		public int IdDocumento { get; set; }
		public int? IdEstatus { get; set; }
		public bool Activo { get; set; }
		public bool EnvioDigital { get; set; }
	}
}
