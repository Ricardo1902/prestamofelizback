﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_FotoReciboPetRes", Schema = "Solicitudes")]
    public class TB_FotoReciboPetRes
    {
        [Key]
        public int Id_FotoReciboPetRes { get; set; }
        public int FotoPeticionReestructura_Id { get; set; }
        public int FotoTipoReciboPetRes_Id { get; set; }
        public int Recibo { get; set; }
        public string TipoRecibo { get; set; }
        public DateTime Fch_Recibo { get; set; }
        public decimal Liquida_Capital { get; set; }
        public decimal Liquida_Interes { get; set; }
        public decimal Liquida_IGV { get; set; }
        public decimal Liquida_Seguro { get; set; }
        public decimal Liquida_GAT { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
