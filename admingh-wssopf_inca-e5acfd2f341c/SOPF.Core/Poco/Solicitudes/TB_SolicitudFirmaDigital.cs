﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_SolicitudFirmaDigital", Schema = "Solicitudes")]
    public class TB_SolicitudFirmaDigital
    {
        [Key]
        public int IdSolicitudFirmaDigital { get; set; }
        public int IdSolicitudProcesoDigital { get; set; }
        public int IdSolicitud { get; set; }
        public string ClaveEnvio { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public int IdEstatus { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public int? IdPeticion { get; set; }
        public string SignatureId { get; set; }
        public int? CodigoEstado { get; set; }
        public string Mensaje { get; set; }
        public DateTime? FechaFirmado { get; set; }
        public bool Activo { get; set; }
    }
}