﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_CATMotivoReestructuras", Schema = "Solicitudes")]
    public class TB_CATMotivoReestructuras
    {
        [Key]
        public int Id_MotivoReestructura { get; set; }
        public string MotivoReestructura { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
    }
}
