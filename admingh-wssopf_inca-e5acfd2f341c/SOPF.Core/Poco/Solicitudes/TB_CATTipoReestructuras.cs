﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_CATTipoReestructuras", Schema = "Solicitudes")]
    public class TB_CATTipoReestructuras
    {
        [Key]
        public int Id_TipoReestructura { get; set; }
        public string TipoReestructura { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
        public int? IdDocumento { get; set; }
    }
}
