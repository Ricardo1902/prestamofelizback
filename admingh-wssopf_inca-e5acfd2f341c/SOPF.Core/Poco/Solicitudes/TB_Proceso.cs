﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
	[Table("TB_Proceso", Schema = "Solicitudes")]
	public class TB_Proceso
	{
		[Key]
		public int IdProceso { get; set; }
		public string NombreProceso { get; set; }
		public bool Activo { get; set; }
	}
}
