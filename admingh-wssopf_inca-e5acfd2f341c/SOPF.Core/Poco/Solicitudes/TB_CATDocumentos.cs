﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_CATDocumentos", Schema = "Solicitudes")]
    public class TB_CATDocumentos
    {
        [Key]
        public int IDDocumento { get; set; }
        public string Documento { get; set; }
        public bool? IDEstatus { get; set; }
        public int? Orden { get; set; }
        public int EnApp { get; set; }
        public string Descripcion { get; set; }
    }
}