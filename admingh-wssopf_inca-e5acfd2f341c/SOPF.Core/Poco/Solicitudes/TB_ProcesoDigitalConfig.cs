//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_ProcesoDigitalConfig", Schema = "Solicitudes")]
    public class TB_ProcesoDigitalConfig
    {
        [Key]
        public int IdProcesoDigitalConfig { get; set; }
        public int IdTipoCredito { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }
        public int? IdProceso { get; set; }
        public int IdOrigen { get; set; }
    }
}
