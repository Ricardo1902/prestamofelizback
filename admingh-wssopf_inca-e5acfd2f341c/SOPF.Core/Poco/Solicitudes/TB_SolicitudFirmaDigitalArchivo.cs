﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_SolicitudFirmaDigitalArchivo", Schema = "Solicitudes")]
    public class TB_SolicitudFirmaDigitalArchivo
    {
        [Key]
        public int IdSolicitudFirmaDigitalArchivo { get; set; }
        public int IdSolicitudFirmaDigital { get; set; }
        public int IdDocumento { get; set; }
        public int Orden { get; set; }
        public string NombreDocumentoDigital { get; set; }
    }
}