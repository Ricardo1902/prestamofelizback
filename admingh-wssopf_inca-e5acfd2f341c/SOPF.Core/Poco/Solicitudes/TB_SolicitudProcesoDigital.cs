//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
	[Table("TB_SolicitudProcesoDigital", Schema = "Solicitudes")]
	public class TB_SolicitudProcesoDigital
	{
		[Key]
		public int IdSolicitudProcesoDigital { get; set; }
		public int IdSolicitud { get; set; }
		public int IdProcesoDigitalConfigDet { get; set; }
		public int IdProcesoDigital { get; set; }
		public int Orden { get; set; }
		public DateTime FechaRegistro { get; set; }
		public int IdEstatus { get; set; }
		public string Comentario { get; set; }
		public int? IdUsuarioActualiza { get; set; }
		public DateTime? FechaActualizacion { get; set; }
		public bool Activo { get; set; }
		public bool ProcesoActual { get; set; }
		public int? IdProceso { get; set; }
	}
}
