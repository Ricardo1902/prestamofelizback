//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
	[Table("TB_ProcesoDigitalConfigDet", Schema = "Solicitudes")]
	public class TB_ProcesoDigitalConfigDet
	{
		[Key]
		public int IdProcesoDigitalConfigDet { get; set; }
		public int IdProcesoDigitalConfig { get; set; }
		public int IdProcesoDigital { get; set; }
		public int Orden { get; set; }
		public DateTime FechaRegistro { get; set; }
		public bool Activo { get; set; }
	}
}
