﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_PeticionReestructuras", Schema = "Solicitudes")]
    public class TB_PeticionReestructuras
    {
        [Key]
        public int Id_PeticionReestructura { get; set; }
        public decimal Solicitud_Id { get; set; }
        public decimal? IdSolicitudNuevo { get; set; }
        public decimal? TotalLiquidacionRestructura { get; set; }
        public int TipoReestructura_Id { get; set; }
        public int EstatusPeticionReestructuras_Id { get; set; }
        public int MotivoPeticionReestructura_Id { get; set; }
        public int Pagos { get; set; }
        public int CapturistaUsuario_Id { get; set; }
        public int? AnalistaUsuario_Id { get; set; }
        public int? AutorizadorUsuario_Id { get; set; }
        public bool Actual { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
