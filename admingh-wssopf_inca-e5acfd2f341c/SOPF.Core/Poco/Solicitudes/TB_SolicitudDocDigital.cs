﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_SolicitudDocDigital", Schema = "Solicitudes")]
    public class TB_SolicitudDocDigital
    {
        [Key]
        public int IdSolicitudDocDigital { get; set; }
        public int IdSolicitud { get; set; }
        public int IdDocumentoDigital { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Contenido { get; set; }
        public bool Activo { get; set; }
        public DateTime? FechaActualizacion { get; set; }
    }
}