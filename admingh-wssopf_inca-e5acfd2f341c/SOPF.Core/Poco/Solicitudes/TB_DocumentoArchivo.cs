﻿
//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_DocumentoArchivo", Schema = "Solicitudes")]
    public class TB_DocumentoArchivo
    {
        [Key]
        public int IdDocumentoArchivo { get; set; }
        public int IdSolicitud { get; set; }
        public int IdDocumento { get; set; }
        public string NombreArchivo { get; set; }
        public string TipoMedio { get; set; }
        public string IdArchivoDrive { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public bool Actual { get; set; }
        public bool EnLocal { get; set; }
        public int? IdUsuarioActualiza { get; set; }
        public DateTime? FechaActualizacion { get; set; }

    }
}
