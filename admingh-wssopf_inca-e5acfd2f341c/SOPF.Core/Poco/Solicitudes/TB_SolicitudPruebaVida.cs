//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_SolicitudPruebaVida", Schema = "Solicitudes")]
    public class TB_SolicitudPruebaVida
    {
        [Key]
        public int IdSolicitudPruebaVida { get; set; }
        public int IdSolicitudProcesoDigital { get; set; }
        public int IdSolicitud { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public string ClaveEnvio { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int IdPeticion { get; set; }
        public string UrlSesion { get; set; }
        public bool SesionRealizada { get; set; }
        public DateTime? FechaValidacion { get; set; }
        public bool ValidacionCorrecta { get; set; }
        public bool? Aprobado { get; set; }
        public string Mensaje { get; set; }
        public DateTime? FechaAprobacion { get; set; }
        public bool Activo { get; set; }
    }
}
