﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_SolicitudPruebaVidaEstatus", Schema = "Solicitudes")]
    public class TB_SolicitudPruebaVidaEstatus
    {
        [Key]
        public int IdSolicitudPruebaVidaEstatus { get; set; }
        public int IdSolicitudPruebaVida { get; set; }
        public int IdUsuario { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool? ValidacionCorrecta { get; set; }
        public bool? Aprobado { get; set; }
        public string Mensaje { get; set; }
    }
}
