﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_FotoPeticionReestructuras", Schema = "Solicitudes")]
    public class TB_FotoPeticionReestructuras
    {
        [Key]
        public int Id_FotoPeticionReestructura { get; set; }
        public int PeticionReestructura_Id { get; set; }
        public DateTime fch_Credito { get; set; }
        public decimal erogacion { get; set; }
        public string NumeroDocumento { get; set; }
        public string Cliente { get; set; }
        public string Direccion { get; set; }
        public decimal capital { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaPrimerPago { get; set; }
        public decimal TotalLiquidacion { get; set; }
    }
}
