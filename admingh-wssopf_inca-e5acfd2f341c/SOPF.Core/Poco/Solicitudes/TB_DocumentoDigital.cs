﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_DocumentoDigital", Schema = "Solicitudes")]
    public class TB_DocumentoDigital
    {
        [Key]
        public int IdDocumentoDigital { get; set; }
        public int IdDocumento { get; set; }
        public int Version { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }
        public int? IdProceso { get; set; }
        public DateTime? InicioVigencia { get; set; }
        public DateTime? FinVigencia { get; set; }
    }
}