﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("TB_SolicitudAmpliacionExpress", Schema = "Solicitudes")]
    public class TB_SolicitudAmpliacionExpress
    {
        [Key]
        public int IdSolicitudAmpliacionExpress { get; set; }
        public decimal IdSolicitud { get; set; }
        public string DNI { get; set; }
        public decimal MontoOtorgado { get; set; }
        public int PlazoOtorgado { get; set; }
        public DateTime FechaAmpliacion { get; set; }
        public string NombrePromotor { get; set; }
        public int IdPromotor { get; set; }
        public string Correo { get; set; }
        public decimal? IdSolicitudAmpliacion { get; set; }
        public decimal MontoLiquidacion { get; set; }
        public decimal MontoDesembolsar { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public int? DiaCobro { get; set; }
        public int? IdUsuario { get; set; }
        public string Mensaje { get; set; }
        public int IdEstatus { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}