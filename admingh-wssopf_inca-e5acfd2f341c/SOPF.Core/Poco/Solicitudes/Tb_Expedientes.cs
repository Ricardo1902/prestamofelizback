﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
    [Table("Tb_Expedientes", Schema = "Solicitudes")]
    public class Tb_Expedientes
    {
        [Key]
        public int Id_DExpediente { get; set; }
        public int? Solicitud_Id { get; set; }
        public bool? ExpedienteFideicomiso { get; set; }
        public int? Documento_Id { get; set; }
        public bool? Actual { get; set; }
        public DateTime? FechaSys { get; set; }
        public string GDArchivo_Id { get; set; }
        public string Nombre_Archivo { get; set; }
        public int? IdUsuarioRegistra { get; set; }
    }
}