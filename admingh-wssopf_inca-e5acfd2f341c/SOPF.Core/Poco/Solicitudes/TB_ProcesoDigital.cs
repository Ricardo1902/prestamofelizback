//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Solicitudes
{
	[Table("TB_ProcesoDigital", Schema = "Solicitudes")]
	public class TB_ProcesoDigital
	{
		[Key]
		public int IdProcesoDigital { get; set; }
		public string Proceso { get; set; }
		public string Descripcion { get; set; }
		public bool Activo { get; set; }
		public bool Visible { get; set; }
	}
}
