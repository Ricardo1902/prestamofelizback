﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.Poco.Hubble
{
    public class TB_SmartTypes
    {
        public int IdSmartType { get; set; }
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public string FriendlyName { get; set; }
        public bool Indexed { get; set; }
        public bool Activo { get; set; }
        public int StoredCallback_Id { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public int UsuarioCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public int UsuarioModificacion { get; set; }
    }
}
