﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.Poco.Hubble
{
    public class TB_EmailTemplateParams
    {
        public int IdEmailTemplateParam { get; set; }
        public string Parametro { get; set; }
        public string Descripcion { get; set; }
        public string FriendlyName { get; set; }
        public string Placeholder { get; set; }
        public string Help { get; set; }
        public bool Activo { get; set; }
        public int EmailTemplate_Id { get; set; }
        public int EmailTemplateParamType_Id { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public int UsuarioCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public int UsuarioModificacion { get; set; }
    }
}
