﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Reportes
{
    [Table("Accesos", Schema = "Reportes")]
    public class Accesos
    {
        [Key]
        public int Id_Acceso { get; set; }
        public int? Reporte_Id { get; set; }
        public int? UsuarioTipo_ID { get; set; }
        public int? TipoAcceso_Id { get; set; }
        public int? TipoAcc { get; set; }
    }
}