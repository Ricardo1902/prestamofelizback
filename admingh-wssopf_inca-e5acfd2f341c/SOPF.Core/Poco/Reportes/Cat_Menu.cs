﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Reportes
{
    [Table("Cat_Menu", Schema = "Reportes")]
    public class Cat_Menu
    {
        [Key]
        public int IdMenu { get; set; }
        public string Menu { get; set; }
        public string Descripcion { get; set; }
        public string Carpeta { get; set; }
        public string URL { get; set; }
        public int? Padre { get; set; }
        public int? Orden { get; set; }
        public int? IdEstatus { get; set; }
        public int? SistemaId { get; set; }
        public string TipoRedirect { get; set; }
    }
}