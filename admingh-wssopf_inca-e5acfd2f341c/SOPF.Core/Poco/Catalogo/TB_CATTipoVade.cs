﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Catalogo
{
    [Table("TB_CATTipoVade", Schema = "dbo")]
    public class TB_CATTipoVade
    {
        [Key]
        public int IdTipoVade { get; set; }
        [MaxLength(50)]
        public string TipoVade { get; set; }
        public int Tipo { get; set; }
        public decimal Orden { get; set; }
        public int IdEstatus { get; set; }
    }
}
