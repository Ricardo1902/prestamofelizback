﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATFormatoPaginas", Schema = "Catalogo")]
    public class TB_CATFormatoPaginas
    {
        [Key]
        public int IdFormatoPagina { get; set; }
        public int IdFormato { get; set; }
        public string NombrePagina { get; set; }
        public int PaginaInicial { get; set; }
        public int PaginaFinal { get; set; }
        public int CantidadCopias { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}