﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Catalogo
{
    [Table("TB_CATProgIntervalos", Schema = "Catalogo")]
    public class TB_CATProgIntervalos
    {
        [Key]
        public int IdIntervalo { get; set; }
        public string Intervalo { get; set; }
        public bool Activo { get; set; }
    }
}
