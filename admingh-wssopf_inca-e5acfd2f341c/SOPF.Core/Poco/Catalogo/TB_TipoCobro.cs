﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Catalogo
{
    [Table("TB_TipoCobro", Schema = "Catalogo")]
    public class TB_TipoCobro
    {
        [Key]
        public int id { get; set; }
        public string TipoCobro { get; set; }
    }
}
