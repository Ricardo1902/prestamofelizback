//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TipoPension", Schema = "Catalogo")]
    public class TipoPension
    {
        [Key]
        public int idTipoPension { get; set; }
        public string Descripcion { get; set; }
        public bool? Estatus { get; set; }
    }
}