﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Catalogo
{
    [Table("TB_ProgramacionEstatus", Schema = "Catalogo")]
    public class TB_ProgramacionEstatus
    {
        [Key]
        public int IdProgramacionEstatus { get; set; }
        public string ClaveProgramacion { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
