﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Catalogo
{
    [Table("TB_CATOrigenClienteEdicion", Schema = "Catalogo")]
    public class TB_CATOrigenClienteEdicion
    {
        [Key]
        public int IdOrigenClienteEdicion { get; set; }
        public string OrigenClienteEdicion { get; set; }
        public string Url { get; set; }
        public bool Activo { get; set; }
    }
}