﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Catalogo
{
    [Table("TB_CATEmpresa", Schema = "Catalogo")]
    public class TablaEmpresa
    {
        [Key]
        public int IdEmpresa { get; set; }
        public string Empresa { get; set; }
        public string RUC { get; set; }
        public bool Activo { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int iTipEmpresa { get; set; }
    }
}
