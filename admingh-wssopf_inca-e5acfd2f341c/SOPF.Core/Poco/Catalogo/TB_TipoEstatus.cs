﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Catalogo
{
    [Table("TB_TipoEstatus", Schema = "Catalogo")]
    public class TB_TipoEstatus
    {
        [Key]
        public int IdTipoEstatus { get; set; }
        public string Tipo { get; set; }
    }
}