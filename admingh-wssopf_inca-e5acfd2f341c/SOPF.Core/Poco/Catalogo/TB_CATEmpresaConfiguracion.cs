﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.dbo
{
    [Table("TB_CATEmpresaConfiguracion", Schema = "Catalogo")]
    public class TB_CATEmpresaConfiguracion
    {
        [Key]
        public int IdConfiguracion { get; set; }
        public int IdSituacionLaboral { get; set; }
        public int? IdRegimenPension { get; set; }
        public int IdEmpresa { get; set; }
        public int IdOrganoPago { get; set; }
        public int IdNivelRiesgo { get; set; }
        public int DiaPago { get; set; }
        public bool Activo { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}