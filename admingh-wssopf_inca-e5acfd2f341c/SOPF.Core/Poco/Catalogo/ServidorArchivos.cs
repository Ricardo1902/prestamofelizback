﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Catalogo
{
    [Table("ServidorArchivos", Schema = "Catalogo")]
    public class ServidorArchivos
    {
        [Key]
        public int IdServidorArchivos { get; set; }
        [MaxLength(100)]
        public string Nombre { get; set; }
    }
}
