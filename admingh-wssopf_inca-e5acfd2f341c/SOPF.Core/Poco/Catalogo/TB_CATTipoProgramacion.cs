﻿//Creado por: HefestoGenerator - SAVIED
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Catalogo
{
    [Table("TB_CATTipoProgramacion", Schema = "Catalogo")]
    public class TB_CATTipoProgramacion
    {
        [Key]
        public int IdTipoProgramacion { get; set; }
        public string TipoProgramacion { get; set; }
        public bool Activo { get; set; }
    }
}