﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Creditos
{
    [Table("TB_CATOrigen", Schema = "Creditos")]
    public class TB_CATOrigen
    {
        [Key]
        public int IdOrigen { get; set; }
        public string Origen { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool VisibleOriginacion { get; set; }
        public bool Activo { get; set; }
    }
}
