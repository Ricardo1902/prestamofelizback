﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Clientes
{
    [Table("TB_Proceso", Schema = "clientes")]
    public class TB_Proceso_Buzon
    {
        [Key]
        public int Id_Proceso { get; set; }
        public int TipoProceso_Id { get; set; }
        public int Usuario_Id { get; set; }
        public decimal Cliente_Id { get; set; }
        public decimal Solicitud_Id { get; set; }
        public int Estatus_Id { get; set; }
        public DateTime FecAlta { get; set; }
        public decimal SaldoVigente { get; set; }
        public int PagosRealizados { get; set; }
        public int? IdUsuarioAtendio { get; set; }
        public DateTime? FecAtendido { get; set; }
        public DateTime? FecLimitePago { get; set; }
        public string CodigoRecaudo { get; set; }
        public string Comentario { get; set; }
        public int? IdUsuarioRechazo { get; set; }
        public DateTime? FecRechazo { get; set; }
        public double? IngresoNeto { get; set; }
        public double? ImporteAmortizar { get; set; }
        public string TipoAmortizacion { get; set; }
        public DateTime? FecModificacion { get; set; }
        public bool Activo { get; set; }
    }
}
