﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Clientes
{
    [Table("TB_BuzonAcciones", Schema = "clientes")]
    public class TB_BuzonAcciones
    {
        [Key]
        public int IdBuzonAccion { get; set; }
        public int IdProceso { get; set; }
        public int EstatusOrigen { get; set; }
        public int EstatusDestino { get; set; }
        public string Comentario { get; set; }
        public int IdUsuario { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
