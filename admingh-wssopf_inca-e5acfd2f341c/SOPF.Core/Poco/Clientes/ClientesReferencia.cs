﻿using SOPF.Core.Entities.Clientes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Clientes
{
    [Table("Referencia", Schema = "Clientes")]
    public class ClientesReferencia : IValidable, IComparable<ClientesReferencia>
    {
        [Key]
        public int IdReferencia { get; set; }
        public int IdCliente { get; set; }
        [MaxLength(30)]
        public string Nombres { get; set; }
        [MaxLength(30)]
        public string ApellidoPaterno { get; set; }
        [MaxLength(30)]
        public string ApellidoMaterno { get; set; }
        public int IdParentesco { get; set; }
        [NotMapped]
        public string Parentesco { get; set; }
        [MaxLength(50)]
        public string Calle { get; set; }
        [MaxLength(30)]
        public string NumeroExterior { get; set; }
        [MaxLength(30)]
        public string NumeroInterior { get; set; }
        public int IdColonia { get; set; }
        [MaxLength(3)]
        public string Lada { get; set; }
        [MaxLength(10)]
        public string Telefono { get; set; }
        [MaxLength(3)]
        public string LadaCelular { get; set; }
        [MaxLength(10)]
        public string TelefonoCelular { get; set; }
        [MaxLength(200)]
        public string TiempoConocerlo { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }

        public bool ValidarClave()
        {
            if (IdReferencia <= 0) throw new Exception("La clave de la referencia es inválida.");

            return true;
        }

        public bool Validar()
        {
            if (IdCliente <= 0) throw new Exception("La clave del cliente no es válida.");
            if (String.IsNullOrEmpty(Nombres)) throw new Exception("El nombre(s) del cliente no es válido.");
            if (String.IsNullOrEmpty(ApellidoPaterno)) throw new Exception("El apellido paterno no es válido.");
            if (IdParentesco <= 0) throw new Exception("Especifique el parentesco de la referencia.");
            //if (String.IsNullOrEmpty(Calle)) throw new Exception("La Calle del domicilio no es válida.");
            //if (String.IsNullOrEmpty(NumeroExterior)) throw new Exception("Especifique el número exterior del domicilio.");
            //if (IdColonia <= 0) throw new Exception("Especifique la colonia del domicilio.");

            return true;
        }

        public TBClientes.ReferenciaCliente ToReferenciaCliente()
        {
            // Convertir Referencia a Campos de tabla TB_Clientes
            TBClientes.ReferenciaCliente tempReferenciaCliente = new TBClientes.ReferenciaCliente();
            string tempNombreCompleto = $"{Nombres} {ApellidoPaterno} {ApellidoMaterno}".Trim();
            tempNombreCompleto = tempNombreCompleto.Replace("  ", " ");

            tempReferenciaCliente.Referencia = tempNombreCompleto;
            tempReferenciaCliente.IdParentesco = IdParentesco;
            tempReferenciaCliente.Direccion = $"{Calle} {NumeroExterior}".Trim();
            tempReferenciaCliente.IdColonia = (decimal)IdColonia;
            tempReferenciaCliente.Lada = Lada;
            tempReferenciaCliente.Telefono = Telefono;
            tempReferenciaCliente.Celular = TelefonoCelular;

            return tempReferenciaCliente;
        }

        public int CompareTo(ClientesReferencia other)
        {
            int diferencias = 0;
            diferencias += Math.Abs(IdReferencia.CompareTo(other.IdReferencia));
            diferencias += Math.Abs(IdCliente.CompareTo(other.IdCliente));
            diferencias += Math.Abs((String.IsNullOrEmpty(Nombres) ? "" : Nombres).CompareTo(String.IsNullOrEmpty(other.Nombres) ? "" : other.Nombres));
            diferencias += Math.Abs((String.IsNullOrEmpty(ApellidoPaterno) ? "" : ApellidoPaterno).CompareTo(String.IsNullOrEmpty(other.ApellidoPaterno) ? "" : other.ApellidoPaterno));
            diferencias += Math.Abs((String.IsNullOrEmpty(ApellidoMaterno) ? "" : ApellidoMaterno).CompareTo(String.IsNullOrEmpty(other.ApellidoMaterno) ? "" : other.ApellidoMaterno));
            diferencias += Math.Abs(IdParentesco.CompareTo(other.IdParentesco));
            diferencias += Math.Abs((String.IsNullOrEmpty(Calle) ? "" : Calle).CompareTo(String.IsNullOrEmpty(other.Calle) ? "" : other.Calle));
            diferencias += Math.Abs((String.IsNullOrEmpty(NumeroExterior) ? "" : NumeroExterior).CompareTo(String.IsNullOrEmpty(other.NumeroExterior) ? "" : other.NumeroExterior));
            diferencias += Math.Abs((String.IsNullOrEmpty(NumeroInterior) ? "" : NumeroInterior).CompareTo(String.IsNullOrEmpty(other.NumeroInterior) ? "" : other.NumeroInterior));
            diferencias += Math.Abs(IdColonia.CompareTo(other.IdColonia));
            diferencias += Math.Abs((String.IsNullOrEmpty(Lada) ? "" : Lada).CompareTo(String.IsNullOrEmpty(other.Lada) ? "" : other.Lada));
            diferencias += Math.Abs(Telefono.CompareTo((String.IsNullOrEmpty(other.Telefono) ? "" : other.Telefono)));
            diferencias += Math.Abs((String.IsNullOrEmpty(LadaCelular) ? "" : LadaCelular).CompareTo((String.IsNullOrEmpty(other.LadaCelular) ? "" : other.LadaCelular)));
            diferencias += Math.Abs((String.IsNullOrEmpty(TelefonoCelular) ? "" : TelefonoCelular).CompareTo((String.IsNullOrEmpty(other.TelefonoCelular) ? "" : other.TelefonoCelular)));
            diferencias += Math.Abs((String.IsNullOrEmpty(TiempoConocerlo) ? "" : TiempoConocerlo).CompareTo((String.IsNullOrEmpty(other.TiempoConocerlo) ? "" : other.TiempoConocerlo)));

            return diferencias;
        }
    }
    public struct NombreSeparado
    {
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
    }
}
