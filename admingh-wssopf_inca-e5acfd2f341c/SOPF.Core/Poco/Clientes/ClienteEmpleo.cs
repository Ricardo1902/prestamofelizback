﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Clientes
{
    [Table("ClienteEmpleo", Schema = "clientes")]
    public class ClienteEmpleo
    {
        [Key]
        public int IdClienteEmpleo { get; set; }
        public int IdCliente { get; set; }
        public string Ocupacion { get; set; }
        public int? IdSituacionLaboral { get; set; }
        public int? IdRegimenPension { get; set; }
        public string NombreRazonSocial { get; set; }
        public string RUC { get; set; }
        public int? IdGiro { get; set; }
        public string DireccionOficina { get; set; }
        public string NumExterior { get; set; }
        public string NumInterior { get; set; }
        public string Dependencia { get; set; }
        public int? IdColonia { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public string Telefono { get; set; }
        public string Extension { get; set; }
        public string Puesto { get; set; }
        public double? Antiguedad { get; set; }
        public decimal? IngresoBruto { get; set; }
        public decimal? DescuentosLey { get; set; }
        public decimal? OtrosDescuentos { get; set; }
        public decimal? Ingreso { get; set; }
        public decimal? OtrosIngresos { get; set; }
        public bool Activo { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime? FechaRegistro { get; set; }
    }
}