﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Clientes
{
    [Table("TB_UsuarioFCMToken", Schema = "clientes")]
    public class TB_UsuarioFCMToken
    {
        [Key]
        public int Id_UsuarioFCMToken { get; set; }
        public int Usuario_Id { get; set; }
        public string Token { get; set; }
        public DateTime FecAlta { get; set; }
        public bool Activo { get; set; }
    }
}
