﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Clientes
{
	[Table("ClienteTelefono", Schema = "clientes")]
	public class ClienteTelefono
	{
		[Key]
		public int IdClienteTelefono { get; set; }
		public int IdCliente { get; set; }
		public int IdTipoTelefono { get; set; }
		public string Telefono { get; set; }
		public string Extension { get; set; }
		public string Nota { get; set; }
		public bool Activo { get; set; }
		public int IdUsuarioRegistro { get; set; }
		public DateTime? FechaRegistro { get; set; }
	}
}
