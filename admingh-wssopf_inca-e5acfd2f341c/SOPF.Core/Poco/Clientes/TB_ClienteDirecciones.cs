﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Clientes
{
    [Table("TB_ClienteDirecciones", Schema = "clientes")]
    public class TB_ClienteDirecciones
    {
        [Key]
        public int idClienteDireccion { get; set; }
        public int? idCliente { get; set; }
        public int? idSolicitud { get; set; }
        public string direccion { get; set; }
        public int? idTipoDireccion { get; set; }
        public string nombreVia { get; set; }
        public string numeroExterno { get; set; }
        public string numeroInterno { get; set; }
        public string manzana { get; set; }
        public string lote { get; set; }
        public int? tipoZona { get; set; }
        public string nombreZona { get; set; }
        public int? distrito { get; set; }
    }
}
