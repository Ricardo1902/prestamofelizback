﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Poco.Lleida
{
    [Table("TB_Plantilla", Schema = "Lleida")]
    public class Plantilla
    {
        [Key]
        public int IdPlantilla { get; set; }
        public int IdConfig { get; set; }
        public int IdUsuario { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
