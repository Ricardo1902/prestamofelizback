﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.Entities.Hubble
{
    public class ReqTemplateParam
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
