﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.Entities.Hubble
{
    public class TemplateParams
    {
        public int Id { get; set; }
        public string Parameter { get; set; }
        public string Description { get; set; }
        public string FriendlyName { get; set; }
        public string Placeholder { get; set; }
        public string Help { get; set; }
        public string InputType { get; set; }
        public string HtmlFragment { get; set; }
    }
}
