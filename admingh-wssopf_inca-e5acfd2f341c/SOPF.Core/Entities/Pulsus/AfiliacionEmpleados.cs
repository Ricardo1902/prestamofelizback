﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Pulsus
{
    public class AfiliacionEmpleados
    {
        public string IdAfiliacion { get; set; }
        public string ClaveId { get; set; }
        public string Nombre { get; set; }
        public string RFC { get; set; }
        public string Telefono { get; set; }
        public string Moneda { get; set; }
        public string Banco { get; set; }
        public string Clabe { get; set; }        
        public string TipoCuenta { get; set; }        
    }
}