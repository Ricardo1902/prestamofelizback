#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBCATUsuario.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Usuario
{
    public class TBCATUsuario
    {
		//Atributos de la clase 
		public Int32 IdUsuario { get; set; }
		public Int32 IdDepartamento { get; set; }
		public string NombreCompleto { get; set; }
		public string Nombres { get; set; }
		public string ApellidoP { get; set; }
		public string ApellidoM { get; set; }
		public string RFC { get; set; }
		public string Lada { get; set; }
		public string Telefono { get; set; }
		public string Email { get; set; }
		public string Usuario { get; set; }
		public Byte[] Clave { get; set; }
		public Int32 IdTipoUsuario { get; set; }
		public Int32 IdSucursal { get; set; }
		public Int32 IdEstatus { get; set; }
        public Int32 Exito { get; set; }
        public String TipoUsuario { get; set; }
        public string Sucursal { get; set; }
        public string Estatus { get; set; }
        public Int32 IdGestor { get; set; }
    }
}