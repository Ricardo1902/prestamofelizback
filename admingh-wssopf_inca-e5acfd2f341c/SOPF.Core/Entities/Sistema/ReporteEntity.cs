﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Sistema
{
    public class ReporteEntity
    {
        public int IdReporte { get; set; }        
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Sp { get; set; }        
        public int IdPagina { get; set; }
        public int Orden { get; set; }
        public bool Activo { get; set; }
        public int SistemaId { get; set; }        

        public ReporteEntity()
        {
        }
    }
}
