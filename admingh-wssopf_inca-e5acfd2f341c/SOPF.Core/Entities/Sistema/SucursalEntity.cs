﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Sistema
{
    public class SucursalEntity
    {
        public int IdSucursal { get; set; }
        public string Clave { get; set; }
        public string Sucursal { get; set; }
        public string Gerente { get; set; }
        public string Direccion { get; set; }
        public int IdEstatus { get; set; }       

        public SucursalEntity()
        {
        }
    }
}
