﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Sistema
{
    public class UsuarioEntity
    {
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string RFC { get; set; }
        public string Lada { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string Usuario { get; set; }
        public string Clave { get; set; }
        public int IdEstatus { get; set; }
        public bool Bloquear { get; set; }
        public bool EnSession { get; set; }
        public bool Inicializar { get; set; }

        private List<MenuEntity> _menuAcceso;
        public List<MenuEntity> MenuAcceso
        {
            get
            {
                return (_menuAcceso) ?? new List<MenuEntity>();
            }
            set
            {
                this._menuAcceso = value;
            }
        }

        private List<ReporteEntity> _reportesAcceso;
        public List<ReporteEntity> ReportesAcceso
        {
            get
            {
                return (_reportesAcceso) ?? new List<ReporteEntity>();
            }
            set
            {
                this._reportesAcceso = value;
            }
        }

        private List<AccionEntity> _accionesAcceso;
        public List<AccionEntity> AccionesAcceso
        {
            get
            {
                return (_accionesAcceso) ?? new List<AccionEntity>();
            }
            set
            {
                this._accionesAcceso = value;
            }
        }

        private TipoUsuarioEntity _tipoUsuario;
        public TipoUsuarioEntity TipoUsuario
        {
            get
            {
                return (this._tipoUsuario ?? new TipoUsuarioEntity());
            }
            set
            {
                this._tipoUsuario = value;
            }
        }

        private DepartamentoEntity _departamento;
        public DepartamentoEntity Departamento
        {
            get
            {
                return (this._departamento ?? new DepartamentoEntity());
            }
            set
            {
                this._departamento = value;
            }
        }

        private SucursalEntity _sucursal;
        public SucursalEntity Sucursal
        {
            get
            {
                return (this._sucursal ?? new SucursalEntity());
            }
            set
            {
                this._sucursal = value;
            }
        }

        public UsuarioEntity()
        {
            this.TipoUsuario = new TipoUsuarioEntity();
            this.Departamento = new DepartamentoEntity();
            this.Sucursal = new SucursalEntity();

            this.MenuAcceso = new List<MenuEntity>();
            this.ReportesAcceso = new List<ReporteEntity>();
            this.AccionesAcceso = new List<AccionEntity>();
        }
    }
}
