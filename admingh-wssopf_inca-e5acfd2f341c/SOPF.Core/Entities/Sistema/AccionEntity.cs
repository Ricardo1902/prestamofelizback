﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.Entities.Sistema
{
    public class AccionEntity
    {
        public int IdMenuAccion { get; set; }
        public string Menu { get; set; }
        public string Accion { get; set; }
        public bool Activo { get; set; }
    }
}
