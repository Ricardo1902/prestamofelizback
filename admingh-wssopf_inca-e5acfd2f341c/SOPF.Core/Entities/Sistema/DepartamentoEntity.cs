﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Sistema
{
    public class DepartamentoEntity
    {
        public int IdDepartamento { get; set; }
        public string Departamento { get; set; }
        public int IdEstatus { get; set; }

        public DepartamentoEntity()
        {
        }
    }
}
