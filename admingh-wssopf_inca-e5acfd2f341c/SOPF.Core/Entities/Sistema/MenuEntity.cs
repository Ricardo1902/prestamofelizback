﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Sistema
{
    public class MenuEntity
    {
        public int IdMenu { get; set; }
        public int IdPadre { get; set; }
        public string Menu { get; set; }
        public string Descripcion { get; set; }
        public string Carpeta { get; set; }
        public string Url { get; set; }
        public int Orden { get; set; }
        public bool Activo { get; set; }
        public int SistemaId { get; set; }
        public string TipoRedirect { get; set; }

        public List<MenuEntity> _subMenus;
        public List<MenuEntity> SubMenus
        {
            get
            {
                return (_subMenus) ?? new List<MenuEntity>();
            }
            set
            {
                _subMenus = value;
            }
        }

        public MenuEntity()
        {
            this.SubMenus = new List<MenuEntity>();
        }
    }
}
