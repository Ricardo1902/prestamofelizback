﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Sistema
{
    public class DatosAmpliacionEntity
    {
        public int IdSolicitud { get; set; }
        public int IdSolicitudAmplia { get; set; }
        public int IdCuenta { get; set; }    
        public DateTime? FechaLiquidar { get; set; }             
        public decimal CapitalLiquidar { get; set; }
        public decimal InteresLiquidar { get; set; }
        public decimal IGVLiquidar { get; set; }
        public decimal SeguroLiquidar { get; set; }
        public decimal GATLiquidar { get; set; }
        public decimal TotalLiquidar { get; set; }
        public string ComentariosSistema { get; set; }

        public DatosAmpliacionEntity()
        {                      
        }
    }
}
