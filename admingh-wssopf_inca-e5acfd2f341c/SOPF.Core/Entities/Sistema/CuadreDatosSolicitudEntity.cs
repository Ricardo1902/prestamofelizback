﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Sistema
{
    public class CuadreDatosSolicitudEntity
    {
        public int IdSolicitud { get; set; }
        public int IdCuenta { get; set; }
        public int IdSolicitudAmplio { get; set; }
        public int IdPromotor { get; set; }
        public int IdOrigen { get; set; }
        public int IdTipoCredito { get; set; }
        public TBCATEstatus EstatusSolicitud { get; set; }
        public TBCATEstatus EstatusCredito { get; set; }
        public DateTime? FechaSolicitud { get; set; }
        public DateTime? FechaCredito { get; set; }
        public string ClienteNombre { get; set; }
        public string ClienteApPaterno { get; set; }
        public string ClienteApMaterno { get; set; }                
        public int IdTipoConvenio { get; set; }
        public int IdConvenio { get; set; }
        public int IdProducto { get; set; }
        public decimal? TasaInteres { get; set; }
        public string ProductoDesc { get; set; }
        public decimal MontoCuota { get; set; }        
        public decimal MontoGAT { get; set; }
        public decimal MontoUsoCanal { get; set; }
        public int TipoSeguroDesgravamen { get; set; }
        public decimal SeguroDesgravamen { get; set; }
        public decimal Capital { get; set; }
        public decimal ImporteCredito { get; set; }
        public decimal CostoTotalCredito { get; set; }
        public decimal SaldoCapital { get; set; }
        public decimal SaldoVencido { get; set; }
        public DatosAmpliacionEntity DatosAmpliacion { get; set; }

        public CuadreDatosSolicitudEntity()
        {
            this.DatosAmpliacion = new DatosAmpliacionEntity();
            this.EstatusSolicitud = new TBCATEstatus();
            this.EstatusCredito = new TBCATEstatus();            
        }
    }
}
