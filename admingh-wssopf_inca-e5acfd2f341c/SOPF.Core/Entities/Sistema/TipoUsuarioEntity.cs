﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Sistema
{
    public class TipoUsuarioEntity
    {
        public int IdTipoUsuario { get; set; }        
        public string TipoUsuario { get; set; }
        public string Descripcion { get; set; }        
        public int IdEstatus { get; set; }       

        public TipoUsuarioEntity()
        {
        }
    }
}
