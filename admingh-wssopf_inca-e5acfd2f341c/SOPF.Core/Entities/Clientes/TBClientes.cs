#pragma warning disable 141030
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using SOPF.Core.Entities.Catalogo;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

#region Copyright
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBClientes.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-10-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Clientes
{
    public class TBClientes
    {
        public decimal IdCliente { get; set; }
        public string Cliente { get; set; }
        public string Nombres { get; set; }
        public string Apellidop { get; set; }
        public string Apellidom { get; set; }
        public string FchNacimiento { get; set; }
        public string Rfc { get; set; }
        public string Direccion { get; set; }
        public Decimal IdColonia { get; set; }
        public string Manzana { get; set; }
        public string Lote { get; set; }
        public string Lada { get; set; }
        public string Telefono { get; set; }
        public Double AntigDom { get; set; }
        public string Compania { get; set; }
        public Int32 IdEscuela { get; set; }
        public string NumEmplea { get; set; }
        public string DireOfic { get; set; }
        public Decimal IdCoOfic { get; set; }
        public string LadaOfic { get; set; }
        public string TeleOfic { get; set; }
        public string ExteOfic { get; set; }
        public string DependenciaOfic { get; set; }
        public string UbicacionOfic { get; set; }
        public Double AntigOfic { get; set; }
        public string Puesto { get; set; }
        public Decimal IngresoBruto { get; set; }
        public Decimal Ingreso { get; set; }
        public Decimal OtrosIngr { get; set; }
        public Decimal DescuentosLey { get; set; }
        public Decimal OtrosDescuentos { get; set; }
        public string FuenOtroi { get; set; }
        public Int32 EstaCivil { get; set; }
        public Int32 Sexo { get; set; }
        public Int32 ViveCasa { get; set; }
        public string NombArre { get; set; }
        public string LadaArre { get; set; }
        public string TeleArre { get; set; }
        public Decimal RentaArre { get; set; }
        public Int32 DepeEcono { get; set; }
        public string Conyuge { get; set; }
        public string ConyRfc { get; set; }
        public DateTime ConyFchNac { get; set; }
        public string ConyCompa { get; set; }
        public string ConyDire { get; set; }
        public Decimal ConyIdCo { get; set; }
        public string ConyLada { get; set; }
        public string ConyTele { get; set; }
        public string ConyExte { get; set; }
        public string ConyPuest { get; set; }
        public Double ConyAntig { get; set; }
        public Decimal ConyIngre { get; set; }
        public Int32 ConyIdGiro { get; set; }
        public string Referencia1 { get; set; }
        public Int32 IdParentesco1 { get; set; }
        public string DireRef1 { get; set; }
        public Decimal IdCoRef1 { get; set; }
        public string LadaRef1 { get; set; }
        public string TeleRef1 { get; set; }
        public string ExteRef1 { get; set; }
        public string Referencia2 { get; set; }
        public Int32 IdParentesco2 { get; set; }
        public string DireRef2 { get; set; }
        public Decimal IdCoRef2 { get; set; }
        public string LadaRef2 { get; set; }
        public string TeleRef2 { get; set; }
        public string ExteRef2 { get; set; }
        public Int32 IdTipoCliente { get; set; }
        public Int32 IdEstatus { get; set; }
        public Int32 IdCNacimiento { get; set; }
        public string Celular { get; set; }
        public string CURP { get; set; }
        public Int32 IdNacionalidad { get; set; }
        public string IFE { get; set; }
        public string RFCGenerado { get; set; }
        public string NumInterior { get; set; }
        public string NumExterior { get; set; }
        public Int32 TelMensaje { get; set; }
        public Int32 IdConvenio { get; set; }
        public Int32 IdDependencia { get; set; }
        public string EntreCalles1 { get; set; }
        public string EntreCalles2 { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public Int32 BndCliente { get; set; }
        public Int32 IdTipoTransferencia { get; set; }
        public string Clabe { get; set; }
        public Int32 IdTipoPersona { get; set; }
        public Int32 IdPaisNacimiento { get; set; }
        public Int32 IdPaisRecidencia { get; set; }
        public string FchIngreso { get; set; }
        public string Ocupacion { get; set; }
        public string IdActividadEconomica { get; set; }
        public string Referencia3 { get; set; }
        public Int32 IdParentesco3 { get; set; }
        public string DireRef3 { get; set; }
        public Decimal IdCoRef3 { get; set; }
        public string LadaRef3 { get; set; }
        public string TeleRef3 { get; set; }
        public string ExteRef3 { get; set; }
        public string Referencia4 { get; set; }
        public Int32 IdParentesco4 { get; set; }
        public string DireRef4 { get; set; }
        public Decimal IdCoRef4 { get; set; }
        public string LadaRef4 { get; set; }
        public string TeleRef4 { get; set; }
        public string ExteRef4 { get; set; }
        //##### ##### ##### ##### ##### PERU
        public int SituacionLaboral { get; set; }
        public int RegimenPension { get; set; }
        public int TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public int TipoDireccion { get; set; }
        public string NombreVia { get; set; }
        public int TipoZona { get; set; }
        public string NombreZona { get; set; }
        public string RUC { get; set; }
        public string CelRef1 { get; set; }
        public string CelRef2 { get; set; }
        public string CelRef3 { get; set; }
        public string CelRef4 { get; set; }
        public int ConyTipoDocumento { get; set; }
        public string ConyNumeroDocumento { get; set; }
        public string ConyDireccion { get; set; }
        public string ConyDireccionEmpresa { get; set; }
        public string ConyRUC { get; set; }
        public string ConyTelefonoEmpresa { get; set; }
        public int IdConfiguracionEmpresa { get; set; }
        public string UsuarioPlanillaVirtual { get; set; }
        public string ClavePlanillaVirtual { get; set; }

        public string vNomArrendatarioCasa { get; set; }
        public int iAniosViviendoCasa { get; set; }
        public string vNomFamCasa { get; set; }
        public string vParentescoCasa { get; set; }
        public int iTipContrato { get; set; }
        public int iTipNegocio { get; set; }
        public string vDNNomCliente { get; set; }
        public string vDNNomRazon { get; set; }
        public int iDNTipPersoneria { get; set; }
        public string vDNTipSociedad { get; set; }
        public int iDNTipNegocio { get; set; }
        public int iDNTipRegTributario { get; set; }
        public string vDNRUC { get; set; }
        public int iDNTipSector { get; set; }
        public int iDNTipActividad { get; set; }
        public int iDNMesExperiencia { get; set; }
        public int iDNAnioExperiencia { get; set; }
        public int iDNMesInicio { get; set; }
        public int iDNAnioInicio { get; set; }
        public int iDNNumEmpleados { get; set; }
        public int iDNNumSucursales { get; set; }
        public int iDNTipDireccion { get; set; }
        public string vDNVia { get; set; }
        public int iDNNumero { get; set; }
        public string vDNLote { get; set; }
        public string vDNManzana { get; set; }
        public string vDNInterior { get; set; }
        public int iDNPiso { get; set; }
        public string vDNZona { get; set; }
        public int iDNIdDepartamento { get; set; }
        public int iDNIdProvincia { get; set; }
        public int iDNIdDistrito { get; set; }
        public string vDNReferencia { get; set; }
        public int iDNTipPropiedad { get; set; }
        public int iDNAnioOcupacion { get; set; }
        public string vDNNomComercio { get; set; }
        public int iDNTelefono { get; set; }
        public int iDNCelular { get; set; }
        public string vDNEmail { get; set; }
        public string vDNNomAccionista { get; set; }
        public int iDNTipRelacion { get; set; }
        public int iDNParticipacion { get; set; }
        public int iDNIdCargo { get; set; }
        //##### ##### ##### ##### ##### PERU
        //NO EXISTEN
        public string Correo { get; set; }
        public string NumInteriorOfic { get; set; }
        public string NumExteriorOfic { get; set; }
        public int PEP { get; set; }
        public string PEPNombre { get; set; }
        public string PEPPuesto { get; set; }
        public int PEPParentesco { get; set; }
        public int IdUsuarioActualiza { get; set; }
        public string FechaActualizacion { get; set; }

        public partial class BusquedaCliente
        {
            public Int32 clave { get; set; }
            public Int32 clavedesc { get; set; }
            public string Cliente { get; set; }
            public string Nombres { get; set; }
            public string apellidop { get; set; }
            public string apellidom { get; set; }
            public DateTime fch_Nacimiento { get; set; }
            public string direccion { get; set; }
            public string colonia { get; set; }
            public string ciudad { get; set; }
            public string estado { get; set; }
            public Int32 cp { get; set; }
            public Int32 IdCliente { get; set; }
            public string dni { get; set; }
        }

        public partial class BusquedaClienteRecomienda
        {
            public Int32 IdCampania { get; set; }
            public Int32 IdCliente { get; set; }
            public Int32 IdClienteRec { get; set; }
            public Int32 IdClabeRec { get; set; }
        }

        public partial class ReferenciaCliente
        {
            public int IdCliente { get; set; }
            public string Referencia { get; set; }
            public int IdParentesco { get; set; }
            public string Direccion { get; set; }
            public decimal IdColonia { get; set; }
            public string Lada { get; set; }
            public string Telefono { get; set; }
            public string Celular { get; set; }
        }

        public partial class ActualizaClienteGestiones
        {
            public int IdUsuario { get; set; }
            public decimal IdCliente { get; set; }
            public int? IdGiro { get; set; }
            public decimal? IdColonia { get; set; }
            [Column("referenciaDomicilio")]
            public string ReferenciaDomicilio { get; set; }
            [Column("telefono")]
            public string Telefono { get; set; }
            public string Celular { get; set; }
            [Column("ocupacion")]
            public string Ocupacion { get; set; }
            public int? SituacionLaboral { get; set; }
            public int? RegimenPension { get; set; }
            //[Column("compania")]
            //public string Compania { get; set; }
            //public string RUC { get; set; }
            [Column("dire_ofic")]
            public string DireOfic { get; set; }
            [Column("IdCo_ofic")]
            public decimal IdCoOfic { get; set; }
            public string NumInteriorOfic { get; set; }
            public string NumExteriorOfic { get; set; }
            [Column("dependenciaOfic")]
            public string DependenciaOfic { get; set; }
            [Column("ubicacionOfic")]
            public string UbicacionOfic { get; set; }
            [Column("tele_ofic")]
            public string TeleOfic { get; set; }
            [Column("exte_ofic")]
            public string ExteOfic { get; set; }
            [Column("puesto")]
            public string Puesto { get; set; }
            [Column("antig_ofic")]
            public double? AntigOfic { get; set; }
            [Column("ingreso")]
            public decimal? Ingreso { get; set; }
            [Column("ingresoBruto")]
            public decimal? IngresoBruto { get; set; }
            [Column("descuentosLey")]
            public decimal? DescuentosLey { get; set; }
            [Column("otrosDescuentos")]
            public decimal? OtrosDescuentos { get; set; }
            [Column("fch_Ingreso")]
            public string FchIngreso { get; set; }
            [Column("otros_ingr")]
            public decimal? OtrosIngr { get; set; }
            public int? TipoDireccion { get; set; }
            public string NumInterior { get; set; }
            public string NumExterior { get; set; }
            public string NombreVia { get; set; }
            public int? TipoZona { get; set; }
            public string NombreZona { get; set; }
            public int? IdConfiguracionEmpresa { get; set; }
        }

    }
    public partial class DireccionCliente
    {
        public Int32 idClienteDireccion { get; set; }
        public Int32 idCliente { get; set; }
        public Int32 idSolicitud { get; set; }
        public string direccion { get; set; }
        public Int32 idTipoDireccion { get; set; }
        public string nombreVia { get; set; }
        public string numeroExterno { get; set; }
        public string numeroInterno { get; set; }
        public string manzana { get; set; }
        public string lote { get; set; }
        public Int32 tipoZona { get; set; }
        public string nombreZona { get; set; }
        public Int32 idColonia { get; set; } //distrito
    }
    public partial class ClientePeticionReestructura
    {
        public int Id_ClientePeticionReestructura { get; set; }
        public string Email { get; set; }
        public int TipoDireccion { get; set; }
        public string NombreVia { get; set; }
        public string NumExterior { get; set; }
        public string NumInterior { get; set; }
        public string manzana { get; set; }
        public string lote { get; set; }
        public int TipoZona { get; set; }
        public string NombreZona { get; set; }
        public Decimal IdColonia { get; set; }
        public string referenciaDomicilio { get; set; }
        public string lada { get; set; }
        public string telefono { get; set; }
        public string Celular { get; set; }
        public string ocupacion { get; set; }
        public int SituacionLaboral { get; set; }
        public int RegimenPension { get; set; }
        public string compania { get; set; }
        public string RUC { get; set; }
        public Decimal IdCo_ofic { get; set; }
        public string dire_ofic { get; set; }
        public string NumInteriorOfic { get; set; }
        public string NumExteriorOfic { get; set; }
        public string dependenciaOfic { get; set; }
        public string ubicacionOfic { get; set; }
        public string lada_ofic { get; set; }
        public string tele_ofic { get; set; }
        public string exte_ofic { get; set; }
        public string puesto { get; set; }
        public Double antig_ofic { get; set; }
        public Decimal ingresoBruto { get; set; }
        public Decimal descuentosLey { get; set; }
        public Decimal otrosDescuentos { get; set; }
        public DateTime FechaModificacion { get; set; }
        public Decimal ingreso { get; set; }
        public Decimal otros_ingr { get; set; }
    }

    public partial class CapturaPlanillaVirtualConfig
    {
        public int IdPlanillaVirtualConfig { get; set; }
        public int IdEmpresa { get; set; }
        public string DescripcionCampoUsuario { get; set; }
        public string DescripcionCampoClave { get; set; }
        public bool VisibleCampoUsuario { get; set; }
        public bool VisibleCampoClave { get; set; }
        public bool Activo { get; set; }
    }
}