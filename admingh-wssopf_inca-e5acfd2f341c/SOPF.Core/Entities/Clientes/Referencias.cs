﻿using System;

namespace SOPF.Core.Entities.Clientes
{
    public class Referencias
    {
        public string Referencia { get; set; }
        public Int32 IdParentesco { get; set; }
        public string DireRef { get; set; }
        public Decimal IdCoRef { get; set; }
        public string LadaRef { get; set; }
        public string TeleRef { get; set; }
        public string ExteRef { get; set; }
        public string Parentesco { get; set; }
        public string Celular { get; set; }

    }
}
