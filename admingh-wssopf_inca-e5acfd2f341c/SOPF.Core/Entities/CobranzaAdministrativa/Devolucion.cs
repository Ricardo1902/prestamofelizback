﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Entities.CobranzaAdministrativa
{    
    public class Devolucion
    {
        public int idDevolucion { get; set; }
        public int idSolicitud { get; set; }        
        public DateTime? fechaPago { get; set; }
        public decimal montoPago { get; set; }        
        public int idEstatus { get; set; }
        public string comentarios { get; set; }
        public string resultado { get; set; }
        public int idUsuario { get; set; }
        public bool? error { get; set; }
        public DateTime FechaRegistro { get; set; }
    }

    public class DevolucionCondiciones
    {
        public int idDevolucion { get; set; }
        public int idSolicitud { get; set; }
        public DateTime? fechaPagoInicio { get; set; }
        public DateTime? fechaPagoFin { get; set; }
        public int idEstatus { get; set; }
    }

    public class DevolucionMasivo
    {     
        public bool? error { get; set; }
        public string mensaje { get; set; }
    }
}
