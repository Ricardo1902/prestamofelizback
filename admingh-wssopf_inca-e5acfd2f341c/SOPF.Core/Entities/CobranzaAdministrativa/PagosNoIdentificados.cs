﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Entities.CobranzaAdministrativa
{    
    public class PagosNoIdentificados
    {
        public int IdPagoNoIdentificado { get; set; }
        public int IdSolicitud { get; set; }
        public int IdCanalPAgo { get; set; }
        public DateTime? Fch_Pago { get; set; }
        public decimal Monto { get; set; }
        public DateTime? Fch_Resgistro { get; set; }
        public int IdUsuario { get; set; }
        public bool? Error { get; set; }
        public string nombreCanal { get; set; }

        public string Observaciones { get; set; }
    }
    
    public class PagoNoIdentificadosMasivo
    {     
        public bool? Error { get; set; }
        public string Mensaje { get; set; }
    }
}
