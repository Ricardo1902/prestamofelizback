﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.CobranzaAdministrativa
{
    public class DataResultCobAdm
    {
        public partial class Resultado
        {
            public int Bandera { get; set; }
            public string Descripcion { get; set; }
        }
    }
}
