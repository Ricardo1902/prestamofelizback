﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.CobranzaAdministrativa
{
    public class GpoEnvio_Mora
    {
        public int IdGrupoMora { get; set; }
        public int IdGrupo { get; set; }
        public int IdMora { get; set; }
        public int IdEstatus { get; set; }
        public int NumAmortizaciones { get; set; }
        public int ConsecutivoReg { get; set; }
    }

}
