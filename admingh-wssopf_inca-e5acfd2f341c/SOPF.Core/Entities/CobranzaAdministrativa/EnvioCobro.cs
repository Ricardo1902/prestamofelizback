﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.CobranzaAdministrativa
{
    public class EnvioCobro
    {
        //Atributos de la clase 
        public int IdGrupo { get; set; }
        public int idBanco { get; set; }
        public DateTime Fecha { get; set; }
        public int TotalArchivos { get; set; }
        public string Particiones { get; set; }
        public int DiasVencido { get; set; }
        public int DiasIni { get; set; }
        public int TotParticiones { get; set; }
    }

    public class EnvioDomiBBVA
    {
        public string RowInfo { get; set; }
    }

    public class FitrarRespuestaBBVA
    {
        public int idArchivo { get; set; }
        public int TotalRegistros { get; set; }
        public float MontoCobrar { get; set; }
        public int TotalArchivos { get; set; }
    }

    public class EnvioRespuesta
    {
        public string NombreArchivo { get; set; }
        public string nombresRespuestas { get; set; }
        public int numArchivos { get; set; }
    }

    public class HistorialCobros
    {
        public int IdRegistro { get; set; }
        public int Id_archivo { get; set; }
        public int total_registros  { get; set; }
        public decimal  monto_cobrar { get; set; }
        public int total_archivos  { get; set; }
        public DateTime? fechaEnvio  { get; set; }
        public int numero_registros_cobrados  { get; set; }
		public decimal monto_cobrado  { get; set; }
        public string fechaAplicado  { get; set; }
    }

    public class LayoutSumatoria
    {
        public int NumSolicitudes { get; set; }
        public int NumeroRecibos { get; set; }
        public decimal CantidadCobrar { get; set; }

    }

    public class LayoutNetCash //por canal netcash 
    {
        public int SOLICITUD { get; set; } //por canal netcash y visanet
        public string TIPODOC { get; set; } //por canal netcash
        public string NUMDOC { get; set; }//por canal netcash
        public string NOMBRECLIENTE  { get; set; }//por canal netcash
        public decimal IMPORTE { get; set; } //por canal netcash y visanet (cantidad cobrar en recibo)
        public string CUENTA { get; set; }//por canal netcash
    }



    public class LayoutVisaNet
    {
        public int Solicitud { get; set; }
        public decimal Monto { get; set; }
        public string NoTarjeta { get; set; }
        public int MesExpiracion { get; set; }
        public int AnioExpiracion { get; set; }

    }

    public class LayoutInterbank
    {
        public string RowInfo { get; set; }
    }
}
