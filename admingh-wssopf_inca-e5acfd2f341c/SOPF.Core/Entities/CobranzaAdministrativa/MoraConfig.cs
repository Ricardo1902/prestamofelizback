﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SOPF.Core.Entities.CobranzaAdministrativa
{
    public class MoraConfig
    {
        public int IdConfig { get; set; }
        public int idGpoMora { get; set; }        
        public int NumAmorti { get; set; }
        public decimal Porcentaje { get; set; }
        public decimal monto { get; set; }
        
    }
}
