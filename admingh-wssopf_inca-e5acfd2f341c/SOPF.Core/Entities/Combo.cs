﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities
{
    public class Combo
    {
        public Int32 Valor { get; set; }
        public string Descripcion { get; set; }
        public Int32 Tipo { get; set; }
    }
}
