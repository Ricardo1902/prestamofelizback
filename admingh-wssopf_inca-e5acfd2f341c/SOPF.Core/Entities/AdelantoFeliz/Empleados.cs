#pragma warning disable 150408
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	Empleados.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Adelantofeliz
{
    public class Empleados
    {
        
		//Atributos de la clase 
		public Int32 Id { get; set; }
		public string Nombre { get; set; }
		public string ApellidoPaterno { get; set; }
		public string ApellidoMaterno { get; set; }
		public string RFC { get; set; }
		public string CURP { get; set; }
		public string LugNacimiento { get; set; }
		public DateTime FechaNacimiento { get; set; }
		public string Nacionalidad { get; set; }
		public string Calle { get; set; }
		public string NumeroExterior { get; set; }
		public string NummeroInterior { get; set; }
		public string Colonia { get; set; }
		public string Ciudad { get; set; }
		public string CodigoPostal { get; set; }
		public string Municipio { get; set; }
		public string Estado { get; set; }
		public string TelefonoCasa { get; set; }
		public string TelefonoTrabajo { get; set; }
		public string Celular { get; set; }
		public string CorreoElectronico { get; set; }
		public string IngresoNeto { get; set; }
		public string NumEmpleado { get; set; }
		public Int32 IdEmpresa { get; set; }
		public string Puesto { get; set; }
		public string Localidadtrabajo { get; set; }
		public DateTime FechaIngreso { get; set; }
		public Int32 DiaCorte { get; set; }
		public string CicloPago { get; set; }
		public string Banco { get; set; }
		public string Clabe { get; set; }
		public string Tarjeta { get; set; }
		public Boolean Estatus { get; set; }

    }
}