#pragma warning disable 140827
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	PromesaPago.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Gestiones
{
    public class PromesaPago
    {
		//Atributos de la clase 
		public Int32 IdPromesa { get; set; }
		public Int32 IdGestion { get; set; }
		public Decimal IdCuenta { get; set; }
		public DateTime FchPromesa { get; set; }
		public decimal MontoPromesa { get; set; }
		public Int32 Estatus { get; set; }
        public Int32 idTipoCobro { get; set; }
        public string TipoCobro { get; set; }
        public string Cliente { get; set; }
    }
}