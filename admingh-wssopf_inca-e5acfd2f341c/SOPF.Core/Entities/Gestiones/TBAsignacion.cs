#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBAsignacion.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García  	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Gestiones
{
    public class TBAsignacion
    {
		//Atributos de la clase 
		public Int32 IdAsignacion { get; set; }
		public Int32 IdGestor { get; set; }
		public Decimal IdCuenta { get; set; }
		public DateTime FechaAsignacion { get; set; }
		public Int32 UsuarioidAsigno { get; set; }
		public Boolean EstatusAsignacion { get; set; }
        public string Tipo { get; set; }
    }
}