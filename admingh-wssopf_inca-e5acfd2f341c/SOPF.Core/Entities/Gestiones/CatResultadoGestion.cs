#pragma warning disable 140825
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	CatResultadoGestion.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-25 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Gestiones
{
    public class CatResultadoGestion
    {
		//Atributos de la clase 
		public Int32 IdResultado { get; set; }
		public string Descripcion { get; set; }
		public Boolean Estatus { get; set; }

    }
}