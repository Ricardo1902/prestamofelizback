﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Gestiones
{
    public class Avisos
    {
        //Atributos de la clase 
        public Int32 IdAvisos { get; set; }
        public Int32 IdCuenta { get; set; }
        public Int32 Carteo {get; set;}
        public Int32 sms{get; set;}
        public Int32 watsapp{get; set;}
        public Int32 mailing{get; set;}
        public Int32 circuloCredito{get; set;}
        public Int32 fisico{get; set;}
        public DateTime FechaActualizado { get; set; }

    }
}
