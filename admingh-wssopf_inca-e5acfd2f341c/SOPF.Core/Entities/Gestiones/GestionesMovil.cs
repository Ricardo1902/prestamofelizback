﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Gestiones
{
    public class GestionesMovil
    {
        public int idsolicitud { get; set; }
        public string usuario { get; set; }
        public DateTime FechaGestion { get; set; }
        public double latitud { get; set; }
        public double longitud { get; set; }
        public string resultado { get; set; }
        public DateTime FechaPromesa { get; set; }
        public decimal montoPromesa { get; set; }
        public string  Tipocontacto { get; set; }
        public string  comentarios { get; set; }
        
    }
}
