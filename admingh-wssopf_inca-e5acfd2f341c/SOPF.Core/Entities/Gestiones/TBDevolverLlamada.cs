#pragma warning disable 150616
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBDevolverLlamada.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-06-16 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Gestiones
{
    public class TBDevolverLlamada
    {
		//Atributos de la clase 
		public Int32 IdDevLlamada { get; set; }
		public Int32 IdGestion { get; set; }
		public Decimal IdCuenta { get; set; }
		public DateTime FchDevLlamada { get; set; }
		public Int32 IdCliente { get; set; }
		public Int32 IdGestor { get; set; }
        public String Gestor { get; set; }
        public String Cliente { get; set; }
        public String ResultadoGestion { get; set;}
        public String Comentario { get; set; }

    }
}