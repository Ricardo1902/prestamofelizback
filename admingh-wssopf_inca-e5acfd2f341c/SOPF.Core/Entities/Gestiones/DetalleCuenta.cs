﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Gestiones
{
    public class DetalleCuenta
    {
        //Atributos de la clase

        public Decimal Idcuenta { get; set; }
        public String Cliente { get; set; }
        public String Direccion { get; set; }
        public String Colonia { get; set; }
        public String Ciudad { get; set; }
        public String Estado { get; set; }
        public String Rfc { get; set; }
        public String Telefono { get; set; }
        public String Celular { get; set; }
        public String Referencia1 { get; set; }
        public String LadaRef1 { get; set; }
        public String TeleRef1 { get; set; }
        public String CelRef1 { get; set; }
        public String Referencia2 { get; set; }
        public String LadaRef2 { get; set; }
        public String TeleRef2 { get; set; }
        public String CelRef2 { get; set; }
        public DateTime FchCredito { get; set; }
        public String Producto { get; set; }
        public Decimal Erogacion { get; set; }
        public Decimal Capital { get; set; }
        public Decimal SdoVencido { get; set; }
        public Decimal SdoVigente { get; set; }
        public Int32 Diasinac { get; set; }
        public Int32 Diasven { get; set; }
        public Decimal Saldocredi { get; set; }
        public String Sigpag { get; set; }
        public Int32 Recexi { get; set; }
        public Int32 Recven { get; set; }
        public Int32 Recsal { get; set; }
        public Int32 Rectot { get; set; }
        public Decimal IdSolicitud { get; set; }
        public String parantesco1 { get; set; }
        public String parantesco2 { get; set; }
        public Int32 ReferenciaB { get; set; }
        public string frecuencia { get; set; }
        public Int32 BalanceTotal { get; set; }
        public String Banco { get; set; }
        public String Clabe { get; set; }
        public String EstatusBanco { get; set; }
        public String FlagMoraInicioMes { get; set; }
        public String FlagMoraActual { get; set; }
        public String GestorAsigando { get; set; }
        public String FechaUltimoPago { get; set; }
        public Int32 IdCliente { get; set; }

    }
}
