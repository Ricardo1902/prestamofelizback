﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Gestiones
{
    public class DataResult
    {
        
        public partial class Usp_ObtenerAsignacion : TBAsignacion
        {
            public string Usuario { get; set; }
            public string Cliente { get; set; }
            public string Ciudad { get; set; }
            public string estado { get; set; }
            public string Sucursal { get; set; }
            public string Gestor { get; set; }
            public Decimal IdSolicitud { get; set; }
        }

        public partial class Usp_ObtenerGestion : Gestion
        {
            public string Nombre { get; set; }
            public string Descripcion { get; set; }
            public decimal MontoPromesa { get; set; }
            public DateTime FechaPromesa { get; set; }
            public string Campana { get; set; }
        }

        public partial class RepGestiones
        {
            //Atributos de la clase 
            public Int32 IdGestion { get; set; }
            public Decimal Idcuenta { get; set; }
            public DateTime FchGestion { get; set; }
            public string Gestor { get; set; }
            public string Cliente { get; set; }
            public string Comentario { get; set; }
            public string Resultado { get; set; }
            public string Campana { get; set; }
            public Decimal SaldoTotal { get; set; }
            public Decimal Saldovencido { get; set; }
            public string Producto { get; set; }
            public string Convenio { get; set; }
            public DateTime FchUltPago { get; set; }
            public string Sucursal { get; set; }
            public string Descripcion { get; set; }
            public string FlagMora { get; set; }
            public string GestorTelAsig { get; set; }
		    public int idTelAsig		{ get; set; }
		    public string GestorCamAsig { get; set; }
		    public int idCamAsig		{ get; set; }
		    public string GestorExtraAsig { get; set; }
		    public int idExtraAsig		{ get; set; }
		    public string GestorJudAsig { get; set; }
		    public int idJudAsig		{ get; set; }
		    public string AdminisAsig { get; set; }
            public int idAdminAsig { get; set; }
            public DateTime FchPromesa { get; set; }
            public decimal Montopromesa { get; set; }
            public string Tipocobro { get; set; }
            public int idGestor { get; set; }
            public int Nada { get; set; }

        }
      
    }
}
