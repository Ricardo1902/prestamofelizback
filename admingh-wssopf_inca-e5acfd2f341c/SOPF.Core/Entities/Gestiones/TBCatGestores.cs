//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     Juan Carlos García.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBCatGestores.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos Garcia	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Gestiones
{
    public class TBCatGestores
    {
		//Atributos de la clase 
		public Int32 IdGestor { get; set; }
		public string Nombre { get; set; }
		public Int32 IdTipo { get; set; }
		public Int32 Usuarioid { get; set; }
		public Boolean Estatus { get; set; }
		public DateTime FechaAlta { get; set; }
		public Int32 UsuarioIdAlta { get; set; }
		public DateTime FechaModifico { get; set; }
		public Int32 UsuarioIdModifico { get; set; }

    }
}