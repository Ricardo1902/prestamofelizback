#pragma warning disable 140825
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	Gestion.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-25 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Gestiones
{
    public class Gestion
    {
        //Atributos de la clase 
        public Int32 IdGestion { get; set; }
        public DateTime FchGestion { get; set; }
        public Int32 IdGestor { get; set; }
        public string Comentario { get; set; }
        public Int32 IdResultadoGestion { get; set; }
        public Decimal IdCuenta { get; set; }
        public string TipoGestion { get; set; }
        public Int32 idCNT { get; set; }
        public string CNT { get; set; }
        public int idCampana { get; set; }
        public double latitud { get; set; }
        public double longitud { get; set; }
        public int IdTipoGestion { get; set; }
        public string IdCamp { get; set; }
        public decimal? MontoDomiciliar { get; set; }
        public DateTime? FechaDomiciliar { get; set; }
    }
}