﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities
{
    public class Resultado<T> where T: new()
    {
        public Int32 Codigo { get; set; }
        public string CodigoStr { get; set; }
        public string Mensaje { get; set; }
        public string IdUsuario { get; set; }
        public DateTime FechaEntro { get; set; }
        public T ResultObject { get; set; }

        public Resultado()
        {
            this.ResultObject = new T();
        }
    }
}
