﻿using SOPF.Core.Entities.Clientes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using static SOPF.Core.Entities.Clientes.TBClientes;

namespace SOPF.Core.Entities.Solicitudes
{
    public class PeticionReestructura
    {
        public int Id_PeticionReestructura { get; set; }
        public Decimal Solicitud_Id { get; set; }
        public int TipoReestructura_Id { get; set; }
        public int EstatusPeticionReestructuras_Id { get; set; }
        public int ClientePeticionReestructura_Id { get; set; }
        public int SolicitudPeticionReestructura_Id { get; set; }
        public int MotivoPeticionReestructura_Id { get; set; }
        public int Pagos { get; set; }
        public int CapturistaUsuario_Id { get; set; }
        public int AnalistaUsuario_Id { get; set; }
        public int AutorizadorUsuario_Id { get; set; }
        public DateTime FechaRegistro { get; set; }
        public ClientePeticionReestructura ClientePeticionReestructura { get; set; }
        public SolicitudPeticionReestructura SolicitudPeticionReestructura { get; set; }
        public FotoPeticionReestructura FotoPeticionReestructura { get; set; }

        public partial class BusquedaClienteSolicitudPRReq
        {
            public int IdSolicitud { get; set; }
            public string NombreCliente { get; set; }
            public int Bucket { get; set; }
            public string RazonSocialEmpresa { get; set; }
            public string DependenciaEmpresa { get; set; }
            public string UbicacionEmpresa { get; set; }
        }

        public partial class BusquedaClienteSolicitudPRRes
        {
            public int IdSolicitud { get; set; }
            public string NombreCliente { get; set; }
            public string FechaNacimiento { get; set; }
            public decimal SaldoVencido { get; set; }
            public int DiasMora { get; set; }
            public string Bucket { get; set; }
            public string EstatusPeticion { get; set; }
            public int IdPeticionReestructura { get; set; }
        }

    }

    public partial class FotoPeticionReestructura
    {
        public int Id_FotoPeticionReestructura { get; set; }
        public int PeticionReestructura_Id { get; set; }
        public DateTime fch_Credito { get; set; }
        public decimal erogacion { get; set; }
        public string NumeroDocumento { get; set; }
        public string Cliente { get; set; }
        public string Direccion { get; set; }
        public decimal capital { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaPrimerPago { get; set; }
        public decimal TotalLiquidacion { get; set; }
        private List<FotoReciboPeticionReestructura> _fotoReciboPeticionReestructura;
        public List<FotoReciboPeticionReestructura> FotoReciboPeticionReestructuras
        {
            get
            {
                return _fotoReciboPeticionReestructura ?? new List<FotoReciboPeticionReestructura>();
            }
            set
            {
                this._fotoReciboPeticionReestructura = value;
            }
        }

        [XmlType("FotoReciboPeticionReestructura")]
        public class FotoReciboPeticionReestructura
        {
            [XmlAttribute("FotoTipoReciboPetRes_Id")]
            public int FotoTipoReciboPetRes_Id { get; set; }
            [XmlAttribute("Recibo")]
            public int Recibo { get; set; }
            [XmlAttribute("TipoRecibo")]
            public string TipoRecibo { get; set; }
            [XmlAttribute("Fch_Recibo")]
            public DateTime Fch_Recibo { get; set; }
            [XmlAttribute("Liquida_Capital")]
            public decimal Liquida_Capital { get; set; }
            [XmlAttribute("Liquida_Interes")]
            public decimal Liquida_Interes { get; set; }
            [XmlAttribute("Liquida_IGV")]
            public decimal Liquida_IGV { get; set; }
            [XmlAttribute("Liquida_Seguro")]
            public decimal Liquida_Seguro { get; set; }
            [XmlAttribute("Liquida_GAT")]
            public decimal Liquida_GAT { get; set; }
        }

        public string GetFotoReciboPeticionReestructurasXML()
        {
            string xml = string.Empty;
            using (var sw = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sw, new XmlWriterSettings { OmitXmlDeclaration = true }))
                {
                    new XmlSerializer(typeof(List<FotoReciboPeticionReestructura>), new XmlRootAttribute("Recibo")).Serialize(writer, this.FotoReciboPeticionReestructuras);
                }
                xml = sw.ToString();
            }
            return xml;
        }
    }
}
