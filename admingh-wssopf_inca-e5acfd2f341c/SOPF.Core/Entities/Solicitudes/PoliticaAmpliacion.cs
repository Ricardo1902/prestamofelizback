using System;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Collections.Generic;

namespace SOPF.Core.Entities.Solicitudes
{
    public class PoliticaAmpliacion
    {
        public int id { get; set; }
        public string descripcion { get; set; }
    }
}