#pragma warning disable 140822
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBSolicitudes.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-22 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Solicitudes
{
    public class TBSolicitudes
    {
        //Atributos de la clase 
        //Atributos de la clase 
        public Decimal IdSolicitud { get; set; }
        public string Cliente { get; set; }
        public DateTime FchSolicitud { get; set; }
        public Decimal IdCliente { get; set; }
        public Decimal IdClienteA { get; set; }
        public Int32 IdConvenio { get; set; }
        public string Convenio { get; set; }
        public Int32 IdPromotor { get; set; }
        public Int32 IdLinea { get; set; }
        public Decimal Erogacion { get; set; }
        public Decimal Capital { get; set; }
        public Decimal Interes { get; set; }
        public Decimal IvaIntere { get; set; }
        public Int32 IdSucursal { get; set; }
        public Int32 IdEstatus { get; set; }
        public string Estatus { get; set; }
        public DateTime FchEstatus { get; set; }
        public Int32 IdMotivo { get; set; }
        public string Observacion { get; set; }
        public Int32 PendObse { get; set; }
        public Decimal IngrLiqui { get; set; }
        public Decimal CapaPago { get; set; }
        public Int32 IdAnalista { get; set; }
        public Decimal Articulos { get; set; }
        public Decimal Apertura { get; set; }
        public Decimal OtroCarg { get; set; }
        public Decimal Enganche { get; set; }
        public DateTime FchEnganche { get; set; }
        public DateTime FchArranque { get; set; }
        public Int32 Override { get; set; }
        public Decimal Overpaid { get; set; }
        public Int32 IdProducto { get; set; }
        public Int32 IdOpero { get; set; }
        public string CveContBc { get; set; }
        public Decimal CveAutorizacion { get; set; }
        public string DgArticulo { get; set; }
        public Int32 EstProceso { get; set; }
        public DateTime FchProceso { get; set; }
        public Int16 Papeleria { get; set; }
        public Int32 IdDestino { get; set; }
        public DateTime FchGenerado { get; set; }
        public Int32 Mientras { get; set; }
        public string Pedido { get; set; }
        public string FinancieraPedido { get; set; }
        public DateTime FchSuscripcion { get; set; }
        public Int32 IdClabeCliente { get; set; }
        public Int32 IdAsignado { get; set; }
        public Int32 IsReestructura { get; set; }
        public Int32 LiqOtraInstitucion { get; set; }
        public DateTime FchAsignacion { get; set; }
        public Int32 ClabeAnterior { get; set; }
        public Int32 IdCampania { get; set; }
        public Int32 IdClienteRecomienda { get; set; }
        public Int32 IdClabeClienteRec { get; set; }
        public Int32 IdTipoComision { get; set; }
        public string Comentarios { get; set; }
        public string Asignado { get; set; }


        //La consula de Solicitudes requiere de estos dos campos pero no estaban en el código
        public string Promotor { get; set; }
        public string TipoComision { get; set; }
        public bool ConProcesoDigital { get; set; }


        public partial class DatosSolicitud
        {
            public Decimal idCliente { get; set; }
            public decimal Capital { get; set; }
            public decimal TotalPagar { get; set; }
            public int idTipoConvenio { get; set; }
            public int idConvenio { get; set; }
            public int idproducto { get; set; }
            public int idpromotor { get; set; }
            public string pedido { get; set; }
            public decimal erogacion { get; set; }
            public decimal Deducciones { get; set; }

            public Int32 IDSolictud { get; set; }
            public DateTime? FechaSolicitud { get; set; }

            public int IdEstatus { get; set; }
            public int IdEstatusProceso { get; set; }
            public Int32 IDProducto { get; set; }
            public Int32 IDSubProducto { get; set; }
            public Int32 IDCredito { get; set; }
            public Int32 IDCanalVenta { get; set; }
            public Int32 TipoCliente { get; set; }
            public int IDTipoEvaluacion { get; set; }
            public int IDPromotor { get; set; }
            public int IdAsignado { get; set; }
            public decimal ImporteCredito { get; set; }
            public Int32 Plazo { get; set; }
            public decimal TasaInteres { get; set; }
            public Int32 TipoSeguroDesgravamen { get; set; }
            public decimal SeguroDesgravamen { get; set; }
            public decimal MontoGAT { get; set; }
            public decimal MontoUsoCanal { get; set; }
            public decimal MontoTotalFinanciar { get; set; }
            public decimal CostoTotalCred { get; set; }
            public decimal MontoCuota { get; set; }
            public string Banco { get; set; }
            public string CuentaDesembolso { get; set; }
            public string CuentaDebito { get; set; }
            public string CuentaCCI { get; set; }
            public string ReferenciaExperian { get; set; }
            public decimal ScoreExperian { get; set; }
            public DateTime? PrimerPago { get; set; }
            public DateTime? FechaDeposito { get; set; }
            public string NumeroPoliza { get; set; }
            public string NumeroCertificado { get; set; }
            public decimal MontoRetDia1 { get; set; }
            public decimal MontoRetDia2 { get; set; }
            public decimal MontoRetDia3 { get; set; }
            public decimal MonDepDia1 { get; set; }
            public decimal MonDepDia2 { get; set; }
            public decimal MonDepDia3 { get; set; }
            public decimal SaldoIniCta { get; set; }
            public decimal SaldoIniCta2 { get; set; }
            public decimal SaldoIniCta3 { get; set; }
            public decimal SaldoFinCta { get; set; }
            public decimal SaldoFinCta2 { get; set; }
            public decimal SaldoFinCta3 { get; set; }
            public int TipoDomiciliacion { get; set; }
            public string NumeroTarjetaVisa { get; set; }
            public int MesExpiraTarjetaVisa { get; set; }
            public int AnioExpiraTarjetaVisa { get; set; }
            public decimal TMP_MontoLiquidacion { get; set; }
            public string vFrecuenciaPago { get; set; }
            public string vMetDispersion { get; set; }
            public string vCVV { get; set; }

            private List<CuentaDomiciliacionEmergente> _cuentasDomiciliacionEmergentes;
            public List<CuentaDomiciliacionEmergente> CuentasDomiciliacionEmergentes
            {
                get
                {
                    return _cuentasDomiciliacionEmergentes ?? new List<CuentaDomiciliacionEmergente>();
                }
                set
                {
                    this._cuentasDomiciliacionEmergentes = value;
                }
            }
            public int Origen_Id { get; set; }
        }
        public partial class ActualizarSolicitudGestiones
        {
            public int IdSolicitud { get; set; }
            public string Banco { get; set; }
            public int? TipoDomiciliacion { get; set; }
            public string BCEntidad { get; set; }
            public string BCOficina { get; set; }
            public string BCDC { get; set; }
            public string BCCuenta { get; set; }
            public string CuentaCCI { get; set; }
            public string CuentaDesembolso { get; set; }
            public string CuentaDebito { get; set; }
            [NotMapped]
            public bool TieneTarjetaVisa { get; set; }
            public string NumeroTarjetaVisa { get; set; }
            public int? MesExpiraTarjetaVisa { get; set; }
            public int? AnioExpiraTarjetaVisa { get; set; }
        }

        public partial class ComentariosSolicitud
        {
            public int IdComentario { get; set; }
            public int IdSolicitud { get; set; }
            public int IdComentarioPadre { get; set; }
            public string comentarios { get; set; }
            public string Fec_Alta { get; set; }
            public int UsuarioAlta { get; set; }
            public string Nom_Usuario { get; set; }
            public int No_Respuestas { get; set; }
        }
        //ModificarSolicitud-LP
        public partial class ModificaSolicitud
        {
            public int IDSolicitud { get; set; }
            public int IDEstatus { get; set; }
            public int IDTipoConvenio { get; set; }
            public int IDConvenio { get; set; }
            public int IDRegional { get; set; }
            public int IDSucursal { get; set; }
            public int IDTipoPromotor { get; set; }
            public int IDPromotor { get; set; }
            public int Reestructura { get; set; }
            public int IDUsuario { get; set; }
            public int IDAsignado { get; set; }
            public string Error { get; set; }
            public int IDEstatus_Real { get; set; }
            public int EST_Proceso { get; set; }
            public int LiqInstitucion { get; set; }
            public int IdCliente { get; set; }
            public string Cliente { get; set; }
        }

        public partial class SolicitudPeru
        {
            public Int32 IDSolictud { get; set; }
            public DateTime FechaSolicitud { get; set; }
            public Int32 IDCliente { get; set; }
            public string Cliente { get; set; }
            public Int32 IDClienteA { get; set; }
            public Int32 IDEstatus { get; set; }
            public string Estatus { get; set; }
            public DateTime fch_Estatus { get; set; }
            public Int32 est_proceso { get; set; }
            public DateTime fch_proceso { get; set; }
            public Int32 IDConvenio { get; set; }
            public Int32 IDProducto { get; set; }
            public Int32 IDSubProducto { get; set; }
            public string Producto { get; set; }
            public Int32 IDCredito { get; set; }
            public string Credito { get; set; }
            public Int32 IDCanalVenta { get; set; }
            public string CanalVenta { get; set; }
            public Int32 TipoCliente { get; set; }
            public int IDTipoEvaluacion { get; set; }
            public string TipoEvaluacion { get; set; }
            public int IDPromotor { get; set; }
            public string Promotor { get; set; }
            public int IDOrigen { get; set; }
            public decimal ImporteCredito { get; set; }
            public Int32 Plazo { get; set; }
            public decimal Comision { get; set; }
            public decimal TasaInteres { get; set; }
            public Int32 TipoSeguroDesgravamen { get; set; }
            public decimal SeguroDesgravamen { get; set; }
            public decimal MontoGAT { get; set; }
            public decimal MontoUsoCanal { get; set; }
            public decimal MontoTotalFinanciar { get; set; }
            public decimal CostoTotalCred { get; set; }
            public decimal MontoCuota { get; set; }
            public string Banco { get; set; }
            public string CuentaDesembolso { get; set; }
            public string CuentaDebito { get; set; }
            public string CuentaCCI { get; set; }
            public DateTime PrimerPago { get; set; }
            public Int32 IDSucursal { get; set; }
            public Int32 IDAnalista { get; set; }
            public string Pedido { get; set; }

            public string NumeroPoliza { get; set; }
            public string NumeroCertificado { get; set; }
            public DateTime? fchDeposito { get; set; }
            public decimal MontoRetDia1 { get; set; }
            public decimal MontoRetDia2 { get; set; }
            public decimal MontoRetDia3 { get; set; }
            public decimal SaldoIniCta { get; set; }
            public decimal SaldoFinCta { get; set; }
            public decimal MonDepDia1 { get; set; }
            public decimal MonDepDia2 { get; set; }
            public decimal MonDepDia3 { get; set; }
            public decimal SaldoIniCta2 { get; set; }
            public decimal SaldoFinCta2 { get; set; }
            public decimal SaldoIniCta3 { get; set; }
            public decimal SaldoFinCta3 { get; set; }
            public int TipoCredito { get; set; }
            public decimal Deducciones { get; set; }

            public string BCEntidad { get; set; }
            public string BCOficina { get; set; }
            public string BCDC { get; set; }
            public string BCCuenta { get; set; }

            public string ReferenciaExperian { get; set; }
            public decimal ScoreExperian { get; set; }

            public int TipoDomiciliacion { get; set; }
            public string NumeroTarjetaVisa { get; set; }
            public int MesExpiraTarjetaVisa { get; set; }
            public int AnioExpiraTarjetaVisa { get; set; }

            public decimal TMPMontoLiquidacion { get; set; }

            public string vFrecuenciaPago { get; set; }
            public string vMetDispersion { get; set; }
            public string vCVV { get; set; }

            private List<CuentaDomiciliacionEmergente> _cuentasDomiciliacionEmergentes;
            public List<CuentaDomiciliacionEmergente> CuentasDomiciliacionEmergentes
            {
                get
                {
                    return _cuentasDomiciliacionEmergentes ?? new List<CuentaDomiciliacionEmergente>();
                }
                set
                {
                    this._cuentasDomiciliacionEmergentes = value;
                }
            }

            public string getCuentasEmergentesXML()
            {
                string xml = string.Empty;

                using (var sw = new StringWriter())
                {
                    using (XmlWriter writer = XmlWriter.Create(sw, new XmlWriterSettings { OmitXmlDeclaration = true }))
                    {
                        new XmlSerializer(typeof(List<CuentaDomiciliacionEmergente>), new XmlRootAttribute("Solicitud")).Serialize(writer, this.CuentasDomiciliacionEmergentes);
                    }
                    xml = sw.ToString();
                }

                return xml;
            }
        }



        public partial class Expediente
        {
            public int IdExpediente { get; set; }
            public int IDSolicitud { get; set; }
            public int Fideicomiso { get; set; }
            public int IDDocumento { get; set; }
            public string Fecha { get; set; }
            public string IDGoogleDrive { get; set; }
            public string NombreArchivo { get; set; }
        }

        public partial class ExpedienteB64
        {
            public int IDResultado { get; set; }
            public string B64 { get; set; }
        }

        public partial class Documentos
        {
            public int idDocumento { get; set; }
            public string Documento { get; set; }
            public bool Condicionado { get; set; }
            public string MotivoCondicionado { get; set; }
        }

        public partial class ListaNegra
        {
            public bool Bloqueo { get; set; }
            public string DNI { get; set; }
            public string Nombre { get; set; }
            public string ApellidoPaterno { get; set; }
            public string ApellidoMaterno { get; set; }
            public string Origen { get; set; }
        }

        [XmlType("CuentaDomiciliacionEmergente")]
        public class CuentaDomiciliacionEmergente
        {
            [XmlAttribute("IdSolicitud")]
            public int IdSolicitud { get; set; }

            [XmlAttribute("TipoDomiciliacion")]
            public int TipoDomiciliacion { get; set; }

            [XmlAttribute("Banco")]
            public string Banco { get; set; }

            [XmlAttribute("Cuenta")]
            public string Cuenta { get; set; }

            [XmlAttribute("CuentaCCI")]
            public string CuentaCCI { get; set; }

            [XmlAttribute("NumeroTarjeta")]
            public string NumeroTarjeta { get; set; }

            [XmlAttribute("MesExpiraTarjeta")]
            public int MesExpiraTarjeta { get; set; }

            [XmlAttribute("AnioExpiraTarjeta")]
            public int AnioExpiraTarjeta { get; set; }

            [XmlAttribute("Cvv")]
            public string Cvv { get; set; }
        }
    }

    public class SolicitudReestructura
    {
        public Int32 idSolicitud { get; set; }
        public Int32 idSolicitudReestructura { get; set; }
        public Int32 idCuenta { get; set; }
        public DateTime fechaLiquidar { get; set; }
        public Decimal capitalLiquidar { get; set; }
        public Decimal interesLiquidar { get; set; }
        public Decimal igvLiquidar { get; set; }
        public Decimal seguroLiquidar { get; set; }
        public Decimal gatLiquidar { get; set; }
        public Decimal totalLiquidar { get; set; }
        public Int32 idEstatus { get; set; }
        public string comentarios { get; set; }
        public Int32 idSolicitudActiva { get; set; }
    }

    public class SolicitudRPTPeru
    {
        public Int32 IdSolicitud { get; set; }
        public DateTime FechaSolicitud { get; set; }
        public string ClienteNombre { get; set; }
        public string ClienteApePat { get; set; }
        public string ClienteApeMat { get; set; }
        public string Sexo { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public Int32 Edad { get; set; }
        public string EstadoCivil { get; set; }                
        public string Promotor { get; set; }
        public string CanalVenta { get; set; }
        public Decimal ScoreExperian { get; set; }
        public string Origen { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string UbicacionOfic { get; set; }
        public Decimal IngresoBruto { get; set; }
        public Decimal Otros_Ingr { get; set; }
        public Decimal DescuentosLey { get; set; }
        public string SituacionLaboral { get; set; }
        public string Puesto { get; set; }
        public string PEP { get; set; }
        public string LugNac { get; set; }
        public string Nacion { get; set; }
        public string Distrito { get; set; }
        public string Provincia { get; set; }
        public string Departamento { get; set; }
        public string TipRes { get; set; }
        public string RUC { get; set; }
        public string conyuge { get; set; }
        public string cony_tele { get; set; }
        public string Ref1Nom { get; set; }
        public string Ref1Pare { get; set; }
        public string Ref1Tel { get; set; }
        public string Ref2Nom { get; set; }
        public string Ref2Pare { get; set; }
        public string Ref2Tel { get; set; }
        public Int32 NumDepen { get; set; }
        public string Direccion { get; set; }
        public string RefDom { get; set; }
        public string Empresa { get; set; }
        public string Dependencia { get; set; }
        public string TelefonoLaboral { get; set; }
        public string AnexoLaboral { get; set; }
        public string PuestoLaboral { get; set; }
        public Decimal AntiLaboral { get; set; }
        public Decimal OtrosDsctos { get; set; }
        public Decimal IngresoNeto { get; set; }
        public string ProOIng { get; set; }
        public string ConyTDoc { get; set; }
        public string ConyNDoc { get; set; }
        public Decimal ImpCred { get; set; }
        public Decimal PlaCred { get; set; }
        public Decimal MtoCuota { get; set; }
    }

    public class SolicitudReestructuraCondiciones
    {
        public Int32 idSolicitud { get; set; }
        public Int32 idSolicitudReestructura { get; set; }
        public Int32 idCuenta { get; set; }
        public Int32 idEstatus { get; set; }
    }

    public class SolicitudDireccion
    {
        public Int32 idClienteDireccion { get; set; }
        public Int32 idCliente { get; set; }
        public Int32 idSolicitud { get; set; }
        public string direccion { get; set; }
        public Int32 idTipoDireccion { get; set; }
        public string nombreVia { get; set; }
        public string numeroExterno { get; set; }
        public string numeroInterno { get; set; }
        public string manzana { get; set; }
        public string lote { get; set; }
        public Int32 tipoZona { get; set; }
        public string nombreZona { get; set; }
        public Int32 idColonia { get; set; } //distrito
        public Int32 IdUsuario { get; set; }
    }

    public class SolicitudDireccionCondiciones
    {
        public Int32 idClienteDireccion { get; set; }
        public Int32 idCliente { get; set; }
        public Int32 idSolicitud { get; set; }
    }
    public class SolicitudPeticionReestructura
    {
        public int Id_SolicitudPeticionReestructura { get; set; }
        public int TipoDomiciliacion { get; set; }
        public string Banco { get; set; }
        public string BCEntidad { get; set; }
        public string BCOficina { get; set; }
        public string BCDC { get; set; }
        public string BCCuenta { get; set; }
        public string CuentaCCI { get; set; }
        public string CuentaDesembolso { get; set; }
        public string CuentaDebito { get; set; }
        public string NumeroTarjetaVisa { get; set; }
        public int MesExpiraTarjetaVisa { get; set; }
        public int AnioExpiraTarjetaVisa { get; set; }
        public decimal SaldoIniCta { get; set; }
        public DateTime? fch_deposito { get; set; }
        public decimal MontoRetiroDia1 { get; set; }
        public decimal MontoRetiroDia2 { get; set; }
        public decimal MontoRetiroDia3 { get; set; }
        public decimal SaldoFinCta { get; set; }
        public decimal MontoDep1 { get; set; }
        public decimal MontoDep2 { get; set; }
        public decimal MontoDep3 { get; set; }
        public decimal SaldoIniCta2 { get; set; }
        public decimal SaldoFinCta2 { get; set; }
        public decimal SaldoIniCta3 { get; set; }
        public decimal SaldoFinCta3 { get; set; }
        public DateTime FechaModificacion { get; set; }
        private List<CuentaDomiciliacionEmergenPetRes> _cuentasDomiciliacionEmergentes;
        public List<CuentaDomiciliacionEmergenPetRes> CuentasDomiciliacionEmergentes
        {
            get
            {
                return _cuentasDomiciliacionEmergentes ?? new List<CuentaDomiciliacionEmergenPetRes>();
            }
            set
            {
                this._cuentasDomiciliacionEmergentes = value;
            }
        }

        [XmlType("CuentaDomiciliacionEmergenPetRes")]
        public class CuentaDomiciliacionEmergenPetRes
        {
            [XmlAttribute("TipoDomiciliacion")]
            public int TipoDomiciliacion { get; set; }

            [XmlAttribute("Banco")]
            public string Banco { get; set; }

            [XmlAttribute("Cuenta")]
            public string Cuenta { get; set; }

            [XmlAttribute("CuentaCCI")]
            public string CuentaCCI { get; set; }

            [XmlAttribute("NumeroTarjeta")]
            public string NumeroTarjeta { get; set; }

            [XmlAttribute("MesExpiraTarjeta")]
            public int MesExpiraTarjeta { get; set; }

            [XmlAttribute("AnioExpiraTarjeta")]
            public int AnioExpiraTarjeta { get; set; }
        }
        public string getCuentasEmergentesXML()
        {
            string xml = string.Empty;
            using (var sw = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sw, new XmlWriterSettings { OmitXmlDeclaration = true }))
                {
                    new XmlSerializer(typeof(List<CuentaDomiciliacionEmergenPetRes>), new XmlRootAttribute("Solicitud")).Serialize(writer, this.CuentasDomiciliacionEmergentes);
                }
                xml = sw.ToString();
            }
            return xml;
        }
    }
}