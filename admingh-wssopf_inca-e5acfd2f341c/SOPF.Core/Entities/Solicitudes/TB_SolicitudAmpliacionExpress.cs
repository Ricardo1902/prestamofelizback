using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SOPF.Core.Entities.Solicitudes
{
    public class TB_SolicitudAmpliacionExpress
    {
        //Atributos de la clase 
        public int IdSolicitudAmpliacionExpress { get; set; }
        public int IdSolicitud { get; set; }
        public string DNI { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public decimal MontoOtorgado { get; set; }
        public int PlazoOtorgado { get; set; }
        public Utils.DateTimeR FechaAmpliacion { get; set; }
        public int IdPromotor { get; set; }
        public string NombrePromotor { get; set; }
        public string Correo { get; set; }
        public int IdSolicitudAmpliacion { get; set; }
        public decimal MontoLiquidacion { get; set; }
        public decimal MontoDesembolsar { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Mensaje { get; set; }
        public int IdEstatus { get; set; }
        public string Estatus { get; set; }
        public string EstatusDesc { get; set; }
        public Utils.DateTimeR FechaRegistro { get; set; }
        public decimal TasaAnual { get; set; }
        public decimal TasaCostoEfectiva { get; set; }
        public decimal Cuota { get; set; }

        public List<ExpedienteDocumento> ExpedienteDocumentos { get; set; }
        public List<EstatusHistorico> EstatusHistoricos { get; set; }

        public TB_SolicitudAmpliacionExpress()
        {
            this.FechaAmpliacion = new Utils.DateTimeR();
            this.FechaRegistro = new Utils.DateTimeR();

            this.ExpedienteDocumentos = new List<ExpedienteDocumento>();
        }

        public class ExpedienteDocumento
        {
            public int IdExpedienteAmpliacion { get; set; }
            public int IdDocumento { get; set; }
            public string Documento { get; set; }
            public int Orden { get; set; }
            public int IdSolicitud { get; set; }
            public int Actual { get; set; }
            public string NombreArchivo { get; set; }
            public string GDIdArchivo { get; set; }
            public string Archivo { get; set; }
            public Utils.DateTimeR FechaRegistro { get; set; }

            public ExpedienteDocumento()
            {
                this.FechaRegistro = new Utils.DateTimeR();
            }
        }

        public class EstatusHistorico
        {
            public int IdSolicitudAmpliacion { get; set; }
            public int IdEstatus { get; set; }
            public string Estatus { get; set; }
            public string EstatusDesc { get; set; }
            public int IdUsuario { get; set; }
            public string NombreUsuario { get; set; }
            public string ApellidoPaternoUsuario { get; set; }
            public string ApellidoMaternoUsuario { get; set; }
            public string Comentario { get; set; }
            public Utils.DateTimeR FechaRegistro { get; set; }

            public EstatusHistorico()
            {
                this.FechaRegistro = new Utils.DateTimeR();
            }
        }
    }
}