﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Solicitudes
{
    public class CronogramaPagos
    {
        public int IdSolicitud { get; set; }
        public string DNICliente { get; set; }
        public string NombreCliente { get; set; }
        public string ApPaternoCliente { get; set; }
        public string ApMaternoCliente { get; set; }
        public string DireccionCliente { get; set; }
        public decimal MontoDesembolsar { get; set; }
        public decimal TasaAnual { get; set; }
        public decimal TasaCostoAnual { get; set; }
        public DateTime FechaDesembolso { get; set; }
        public DateTime PrimerFechaPago { get; set; }
        public DateTime UltimaFechaPago { get; set; }
        public decimal TotalInteres { get; set; }
        public int Plazo { get; set; }
        public string Condiciones { get; set; }
        public decimal TasaInteresMoratoria { get; set; }
        public decimal Cuota { get; set; }
        public string NombreBancoDesembolso { get; set; }
        public string CuentaBancariaDesembolso { get; set; }
        public string NoTarjetaVisa { get; set; }

        public List<Recibo> Recibos { get; set; }

        public CronogramaPagos()
        {
            this.Recibos = new List<Recibo>();
        }

        public class Recibo
        {
            public string NoRecibo { get; set; }
            public DateTime FechaRecibo { get; set; }
            public decimal CapitalInicial { get; set; }
            public decimal Capital { get; set; }
            public decimal Interes { get; set; }
            public decimal IGV { get; set; }
            public decimal Seguro { get; set; }
            public decimal GAT { get; set; }
            public decimal OtrosCargos { get; set; }
            public decimal IGVOtrosCargos { get; set; }
            public decimal Cuota { get; set; }
            public decimal SaldoCapital { get; set; }
        }
    }
}
