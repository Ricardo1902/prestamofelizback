#pragma warning disable 160829
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Dimex – 2016
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBBlogComentarios.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2016-08-29 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Solicitudes
{
    public class TBBlogComentarios
    {
		//Atributos de la clase 
		public Int32 IdComentario { get; set; }
		public Decimal IdSolicitud { get; set; }
		public Int32 IdComentarioPadre { get; set; }
		public string Comentarios { get; set; }
		public Int32 UsuarioAlta { get; set; }
		public Int32 RolUsuario { get; set; }
		public DateTime FecAlta { get; set; }
		public Boolean Visto { get; set; }

    }
}