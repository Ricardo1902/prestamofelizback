﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Catalogo
{
    public class CAT_Parametros
    {
        public int IdParametro { get; set; }
        public string Descripcion { get; set; }
        public string Clave { get; set; }
        public int ValorInt { get; set; }
        public float ValorFloat { get; set; }
        public string ValorString { get; set; }
        public DateTime ValorFecha { get; set; }
    }
}
