﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities

{
    public class TBCATMedios
    {
        //Atributos de la clase 
        public Int32 IdMedio { get; set; }
        public string Medio { get; set; }
        public Int32 idestatus { get; set; }
    }
}
