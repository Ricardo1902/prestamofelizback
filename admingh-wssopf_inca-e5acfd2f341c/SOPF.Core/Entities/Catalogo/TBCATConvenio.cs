#pragma warning disable 160330
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Dimex – 2016
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBCATConvenio.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2016-03-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Catalogo
{
    public class TBCATConvenio
    {
		//Atributos de la clase 
		public Int32 IdConvenio { get; set; }
		public string Convenio { get; set; }
		public Int32 IdEstatus { get; set; }
		public Int32 QuincenasEnCamino { get; set; }
		public Int32 IdTipoConvenio { get; set; }
		public Int32 EsEmpleado { get; set; }
		public Int32 EsAutoFeliz { get; set; }

    }
}