#pragma warning disable 141229
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBCATPromotor.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-12-29 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities
{
    public class TBCATPromotor
    {
		//Atributos de la clase 
		public Int32 IdPromotor { get; set; }
		public string Promotor { get; set; }
		public string Direccion { get; set; }
		public Int32 IdColonia { get; set; }
		public string Lada { get; set; }
		public string Telefono { get; set; }
		public string Celular { get; set; }
		public string Email { get; set; }
		public Decimal PorcEfvo { get; set; }
		public Decimal PorcDefvo { get; set; }
		public Int32 IdEstatus { get; set; }
		public Int32 IdUsuario { get; set; }
		public Int32 IdTipoPromotor { get; set; }

    }
}