﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.Entities.Catalogo
{
    public class TBCATSubProducto
    {
        public int IdSubProducto { get; set; }
        public string Clave { get; set; }
        public string SubProducto { get; set; }
        public bool Activo { get; set; }
        public DateTime? FechaRegistro { get; set; }
    }
}
