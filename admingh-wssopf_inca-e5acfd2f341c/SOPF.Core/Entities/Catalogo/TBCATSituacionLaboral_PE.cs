﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Catalogo
{
    public class TBCATSituacionLaboral_PE
    {
        public Int32 IdSituacion { get; set; }
        public string Situacion { get; set; }
    }
}
