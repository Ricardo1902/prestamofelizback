﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.Entities.Catalogo
{
    public class TB_CATParametro
    {
        public int IdParametro { get; set; }
        public string Parametro { get; set; }
        public string Valor { get; set; }
        public string Descripcion { get; set; }
        public int IdEstatus { get; set; }
    }
}
