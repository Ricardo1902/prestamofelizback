﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Catalogo
{
  class TBCATDireccion_PE
  {
    public Int32 IDDireccion { get; set; }
    public string Direccion { get; set; }
  }
}
