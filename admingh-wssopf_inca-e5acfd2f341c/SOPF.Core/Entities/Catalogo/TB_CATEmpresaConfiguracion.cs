﻿using System;

namespace SOPF.Core.Entities.Catalogo
{
    public class TB_CATEmpresaConfiguracion
    {
        public int IdConfiguracion { get; set; }
        public int IdSituacionLaboral { get; set; }
        public int IdRegimenPension { get; set; }
        public int IdEmpresa { get; set; }
        public int IdOrganoPago { get; set; }
        public int IdNivelRiesgo { get; set; }
        public bool Activo { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
