﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Catalogo
{
    class TBCATTipoZona_PE
    {
        public Int32 IDTipoZona { get; set; }
        public string TipoZona { get; set; }
    }
}
