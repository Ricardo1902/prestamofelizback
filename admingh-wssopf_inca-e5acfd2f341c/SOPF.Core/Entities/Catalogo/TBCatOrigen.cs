﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Catalogo
{
    public class TBCatOrigen
    {
        public int IdOrigen { get; set; }
        public string Origen { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool VisibleOriginacion { get; set; }
        public bool Activo { get; set; }
        public double MontoComision { get; set; }
    }
}
