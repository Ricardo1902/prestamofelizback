﻿using System;

namespace SOPF.Core.Entities.Catalogo
{
    public class TB_CATEmpresa
    {        
        public int IdEmpresa { get; set; }
        public string Empresa { get; set; }
        public string RUC { get; set; }
        public bool Activo { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
