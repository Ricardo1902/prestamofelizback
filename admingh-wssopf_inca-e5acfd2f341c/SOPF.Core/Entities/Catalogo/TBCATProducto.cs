#pragma warning disable 160330
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Dimex – 2016
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBCATProducto.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2016-03-30 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Catalogo
{
    public class TBCATProducto
    {
		//Atributos de la clase 
		public Int32 IdProducto { get; set; }
		public string Producto { get; set; }
		public Decimal Tasa { get; set; }
		public Decimal Tasaequivalente { get; set; }
		public Decimal TasaMora { get; set; }
		public Decimal Pagos { get; set; }
		public Decimal Meses { get; set; }
		public string Frecuencia { get; set; }
		public Decimal Iva { get; set; }
		public Decimal IvaCap { get; set; }
		public Int32 IdTipoProducto { get; set; }
		public Int32 IdEstatus { get; set; }
		public Decimal Otrocargo { get; set; }
		public string Moneda { get; set; }
		public Int32 IdConvenio { get; set; }
		public Int32 AnioGtia { get; set; }

    }
}