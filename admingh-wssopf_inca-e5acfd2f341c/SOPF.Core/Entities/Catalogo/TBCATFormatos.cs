﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Catalogo

{
    public class TBCATFormatos
  
    {
        public Int32 IdFormato { get; set; }
        public string Formato { get; set; }
        public string Ruta { get; set; }
        public Int32 IdEstatus { get; set; }
        public Int32 TipoProducto { get; set; }
        public string NombrePlantilla { get; set; }
        public string NombreDescarga { get; set; }

    }

}


