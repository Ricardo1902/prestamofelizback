﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Catalogo
{
  class TBCATTipoDocumento_PE
  {
    Int32 IDTipoDocumento { get; set; }
    string TipoDocumento { get; set; }
  }
}
