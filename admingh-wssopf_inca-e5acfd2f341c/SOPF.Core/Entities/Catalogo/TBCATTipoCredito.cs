﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities
{
    public class TBCATTipoCredito
    {
        //Atributos de la clase 
        public Int32 idTipoCredito { get; set; }
        public string TipoCredito { get; set; }
        public Int32 IdEstatus { get; set; }
    }
}
