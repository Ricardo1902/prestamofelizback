﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



# region Informacion General
//
// Archivo: TBCATSucursal.cs
//
// Descripción:
// Clase para el acceso a datos a la tabla TBCATSucursal
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//

#endregion

namespace SOPF.Core.Entities.Catalogo
{
    public class TBCATSucursal
    {
        public Int32 Accion { get; set; }
        public Int32 IdSucursal { get; set; }
        public string Sucursal { get; set; }
        public string RFC { get; set; }
        public string Gerente { get; set; }
        public string Direccion { get; set; }
        public Int32 IdColonia { get; set; }
        public string Colonia { get; set; }
        public string Ciudad { get; set; }
        public string Estado { get; set; }
        public string lada { get; set; }
        public string telefono { get; set; }
        public string fax { get; set; }
        public decimal porce_comi { get; set; }
        public Int32 IdEstatus { get; set; }
        public Int32 IdEstado { get; set; }
        public string Estatus { get; set; }
        public string descripcion { get; set; }
        public Int32 IdAfiliado { get; set; }
        public string Afiliado { get; set; }
        public Int32 cp { get; set; }
        public String cve_sucursal { get; set; }
        public Int32 RegionalId { get; set; }

    }
}
