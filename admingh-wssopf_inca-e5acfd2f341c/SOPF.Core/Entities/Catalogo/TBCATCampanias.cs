﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Catalogo
{
    public class TBCATCampanias
    {
        public Int32 IdCampania { get; set; }
        public string Campania { get; set; }
        public string TablaRelacion { get; set; }
        public Int32 IdEstatus { get; set; }

    }
}
