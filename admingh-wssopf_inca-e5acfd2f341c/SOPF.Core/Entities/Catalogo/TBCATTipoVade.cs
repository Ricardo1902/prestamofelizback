#pragma warning disable 151228
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBCATTipoVade.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-12-28 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Catalogo
{
    public class TBCATTipoVade
    {
		//Atributos de la clase 
		public Int32 IdTipoVade { get; set; }
		public string TipoVade { get; set; }
		public Int32 Tipo { get; set; }
		public Decimal Orden { get; set; }
		public Int32 IdEstatus { get; set; }

    }
}