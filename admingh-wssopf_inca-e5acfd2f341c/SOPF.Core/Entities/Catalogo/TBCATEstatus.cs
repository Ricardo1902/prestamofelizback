﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities
{
    public class TBCATEstatus
    {
        public int IdEstatus { get; set; }
        public int IdTipoEstatus { get; set; }
        public string EstatusClave { get; set; }
        public string EstatusDesc { get; set; }
        public string Ayuda { get; set; }
    }
}
