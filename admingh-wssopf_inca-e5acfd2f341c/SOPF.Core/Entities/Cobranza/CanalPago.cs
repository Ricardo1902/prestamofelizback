﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;

namespace SOPF.Core.Entities.Cobranza
{
    public class CanalPago
    {
        // Propiedades escalares
        public int IdCanalPago { get; set; }
        public string Nombre { get; set; }
        public string Clave { get; set; }
        public decimal MontoComisionFijo { get; set; }
        public decimal PorcentajeComisionFijo { get; set; }
        public bool Activo { get; set; }          

        // Constructor
        public CanalPago()
        {
            
        }
    }    
}
