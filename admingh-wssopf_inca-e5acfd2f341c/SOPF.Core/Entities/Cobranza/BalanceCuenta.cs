﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core;

namespace SOPF.Core.Entities.Cobranza
{
    public class BalanceCuenta
    {
        public int IdSolicitud { get; set; }
        public int IdCuenta { get; set; }
        public int IdTipoCredito { get; set; }
        public string TipoCreditoDesc { get; set; }
        private TBCATEstatus _estatusCredito { get; set; }
        public TBCATEstatus EstatusCredito
        {
            set
            {
                this._estatusCredito = value;
            }
            get
            {
                return (this._estatusCredito != null) ? this._estatusCredito : new TBCATEstatus();
            }
        }
        public Utils.DateTimeR FechaSolicitud { get; set; }
        public Utils.DateTimeR FechaCredito { get; set; }
        public string ClienteNombre { get; set; }
        public string ClienteApPaterno { get; set; }
        public string ClienteApMaterno { get; set; }
        public int IdProducto { get; set; }
        public string ProductoDesc { get; set; }
        public decimal MontoCuota { get; set; }
        public decimal CostoTotalCredito { get; set; }
        public decimal MontoGAT { get; set; }
        public decimal MontoUsoCanal { get; set; }
        public decimal TasaSeguroDesgravamen { get; set; }
        public decimal Capital { get; set; }
        public decimal SaldoCapital { get; set; }
        public decimal SaldoVencido { get; set; }
        public decimal ComisionOrigen { get; set; }

        public BalanceCuenta()
        {
            this.EstatusCredito = new TBCATEstatus();
            this.FechaSolicitud = new Utils.DateTimeR();
            this.FechaCredito = new Utils.DateTimeR();
        }

        /* BalanceCuenta.Recibos */
        public class Recibos
        {
            public int IdCuenta { get; set; }
            public int Recibo { get; set; }            
            public Utils.DateTimeR FechaRecibo { get; set; }
            public int IdEstatus { get; set; }
            public string EstatusClave { get; set; }
            public string EstatusDesc { get; set; }
            public decimal Capital { get; set; }
            public decimal Interes { get; set; }
            public decimal IGV { get; set; }
            public decimal Seguro { get; set; }
            public decimal GAT { get; set; }
            public decimal CapitalInicial { get; set; }
            public decimal CapitalInsoluto { get; set; }
            public decimal SaldoCapital { get; set; }
            public decimal SaldoInteres { get; set; }
            public decimal SaldoIGV { get; set; }
            public decimal SaldoSeguro { get; set; }
            public decimal SaldoGAT { get; set; }
            public decimal SaldoOtrosCargos { get; set; }
            public decimal SaldoIGVOtrosCargos { get; set; }
            public decimal SaldoRecibo { get; set; }
            public decimal TotalRecibo { get; set; }

            public Recibos()
            {
                this.FechaRecibo = new Utils.DateTimeR();
            }
        }
    }
}
