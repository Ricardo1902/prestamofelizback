﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using SOPF.Core.Entities.Catalogo;

namespace SOPF.Core.Entities.Cobranza
{
    public class Plantilla
    {
        // Propiedades escalares
        public int? IdPlantilla { get; set; }
        public TipoLayoutCobro TipoLayout { get; set; }
        public string Nombre { get; set; }
        public int? IdUsuarioRegistro { get; set; }
        public Utils.DateTimeR FechaRegistro { get; set; }
        public bool? OtrosBancos { get; set; }
        public bool? UsarCuentaEmergente { get; set; }
        public bool? Activo { get; set; }  
        
        public List<TBCATSituacionLaboral_PE> SituacionLaboral { get; set; }
        public List<EmpresaConfiguracion> Empresas { get; set; }

        // Constructor
        public Plantilla()
        {
            this.TipoLayout = new TipoLayoutCobro();
            this.FechaRegistro = new Utils.DateTimeR();

            this.SituacionLaboral = new List<TBCATSituacionLaboral_PE>();
            this.Empresas = new List<EmpresaConfiguracion>();            
        }


        // Contiene la configuracion de cobranza por empresa
        public class EmpresaConfiguracion
        {
            public TB_CATEmpresa Empresa { get; set; }            
            public List<BucketConfiguracion> Buckets { get; set; }

            public EmpresaConfiguracion()
            {
                this.Empresa = new TB_CATEmpresa();                
                this.Buckets = new List<BucketConfiguracion>();
            }

            public class BucketConfiguracion
            {
                public Bucket Bucket { get; set; }
                public decimal? NoErogacionesMax { get; set; }
                public decimal? MontoIndivisible { get; set; }
                public List<PartidaConfiguracion> Partidas { get; set; }

                public BucketConfiguracion()
                {
                    this.Bucket = new Bucket();
                    this.Partidas = new List<PartidaConfiguracion>();
                }

                public string getPartidasXML()
                {
                    string xml = string.Empty;

                    using (var sw = new StringWriter())
                    {
                        using (XmlWriter writer = XmlWriter.Create(sw, new XmlWriterSettings { OmitXmlDeclaration = true }))
                        {
                            new XmlSerializer(typeof(List<PartidaConfiguracion>), new XmlRootAttribute("Bucket")).Serialize(writer, this.Partidas);
                        }
                        xml = sw.ToString();
                    }

                    return xml;
                }

                [XmlType("PartidaConfiguracion")]
                public class PartidaConfiguracion
                {
                    [XmlAttribute("NoPartida")]
                    public int NoPartida { get; set; }

                    [XmlAttribute("PorcentajePartida")]
                    public int PorcentajePartida { get; set; }
                }
            }
        }
    }   
}
