﻿using System;

namespace SOPF.Core.Entities.Cobranza
{
    public class ProgramacionDetalle
    {
        public int IdProgramacionDetalle { get; set; }
        public Programacion Programacion { get; set; }
        public DateTime FechaInicio { get; set; }
        public Utils.DateTimeR FechaTermino { get; set; }
        public Utils.DateTimeR HoraInicio { get; set; }
        public Utils.DateTimeR HoraTermino { get; set; }
        public ProgramacionIntervalo Intervalo { get; set; }
        public decimal ValorIntervalo { get; set; }
        public int Intentos { get; set; }
        public bool EsGrupoMoroso { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }

        public ProgramacionDetalle()
        {
            Programacion = new Programacion();
            FechaTermino = new Utils.DateTimeR();
            HoraInicio = new Utils.DateTimeR();
            HoraTermino = new Utils.DateTimeR();
            Intervalo = new ProgramacionIntervalo();
        }
    }
}
