﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System;

namespace SOPF.Core.Entities.Cobranza
{
    public class GrupoLayout
    {
        // Propiedades escalares
        public int? IdGrupoLayout { get; set; }
        public string Nombre { get; set; }
        public int? IdUsuarioRegistro { get; set; }
        public Utils.DateTimeR FechaRegistro { get; set; }
        public bool? UsarCuentaEmergente { get; set; }
        public bool? Activo { get; set; }

        public List<PlantillaGrupoLayout> Plantillas { get; set; }
        public List<ConfiguracionEjecucion> DiasEjecucion { get; set; }
        public List<ProgramacionGrupoLayout> Programaciones { get; set; }

        // Constructor
        public GrupoLayout()
        {
            this.FechaRegistro = new Utils.DateTimeR();
            this.DiasEjecucion = new List<ConfiguracionEjecucion>();
            this.Plantillas = new List<PlantillaGrupoLayout>();
            this.Programaciones = new List<ProgramacionGrupoLayout>();
        }

        public string getDiasEjecucionXML()
        {
            string xml = string.Empty;

            using (var sw = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sw, new XmlWriterSettings { OmitXmlDeclaration = true }))
                {
                    new XmlSerializer(typeof(List<ConfiguracionEjecucion>), new XmlRootAttribute("GrupoLayout")).Serialize(writer, this.DiasEjecucion);
                }
                xml = sw.ToString();
            }

            return xml;
        }

        // Contiene la configuracion de dias de ejecucion del grupo
        [XmlType("ConfiguracionEjecucion")]
        public class ConfiguracionEjecucion
        {
            [XmlAttribute("Dia")]
            public string Dia { get; set; }

            public ConfiguracionEjecucion()
            {
            }
        }

        public class PeticionProcesar
        {
            public PeticionProcesar()
            { }

            public int IdGrupoLayout { get; set; }
            public int IdUsuarioProcesa { get; set; }
            public bool EsAutomatico { get; set; }
        }

        public class PlantillaGrupoLayout
        {
            public Plantilla Plantilla { get; set; }
            public int DiasVencimiento { get; set; }
            public string TipoLayoutStr { get; set; }

            public PlantillaGrupoLayout()
            {
                this.Plantilla = new Plantilla();
            }
        }

        public class ProgramacionGrupoLayout
        {
            public int IdProgramacion { get; set; }
            public TipoProgramacion TipoProgramacion { get; set; }
            public int IdGrupoLayout { get; set; }
            public int IdUsuarioRegistro { get; set; }
            public DateTime FechaRegistro { get; set; }
            public bool Activo { get; set; }
            public DateTime FechaInicio { get; set; }
            public DateTime FechaTermino { get; set; }
            public DateTime HoraInicio { get; set; }
            public DateTime HoraTermino { get; set; }
            public ProgramacionIntervalo Intervalo { get; set; }
            public decimal ValorIntervalo { get; set; }
            public int Intentos { get; set; }
            public bool EsGrupoMoroso { get; set; }
            public List<ProgramacionDiasEjecucion> DiasEjecucion { get; set; }

            public ProgramacionGrupoLayout()
            {
                TipoProgramacion = new TipoProgramacion();
                Intervalo = new ProgramacionIntervalo();
                DiasEjecucion = new List<ProgramacionDiasEjecucion>();
            }

            public string getProgramacionDiasEjecucionXML()
            {
                string xml = string.Empty;

                using (var sw = new StringWriter())
                {
                    using (XmlWriter writer = XmlWriter.Create(sw, new XmlWriterSettings { OmitXmlDeclaration = true }))
                    {
                        new XmlSerializer(typeof(List<ProgramacionDiasEjecucion>), new XmlRootAttribute("ProgramacionGrupoLayout")).Serialize(writer, this.DiasEjecucion);
                    }
                    xml = sw.ToString();
                }

                return xml;
            }

            // Contiene la configuracion de dias de ejecucion de la programacion
            [XmlType("ProgramacionDiasEjecucion")]
            public class ProgramacionDiasEjecucion
            {
                [XmlAttribute("Dia")]
                public int Dia { get; set; }

                public ProgramacionDiasEjecucion()
                {
                }
            }
        }
    }
}
