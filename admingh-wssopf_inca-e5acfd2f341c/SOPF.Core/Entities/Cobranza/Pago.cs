﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;
using SOPF.Core;

namespace SOPF.Core.Entities.Cobranza
{
    public class ResultadoDetallePagoMasivo
    {
        public int total;
        public List<DetallePagoMasivo> pagos;
    }

    public class DetallePagoMasivo
    {
        public int idPago { get; set; }
        public int idDomiciliacion { get; set; }
        public int idPagoDirecto { get; set; }
        public int idSolicitud { get; set; }
        public decimal montoCobrado { get; set; }
        public decimal montoAplicado { get; set; }
        public decimal montoPorAplicar { get; set; }
        public string canalPagoClave { get; set; }
        public string canalPagoDesc { get; set; }
        public int idEstatus { get; set; }
        public string estatusClave { get; set; }
        public string estatusDesc { get; set; }
        public string fechaPago { get; set; }
        public string fechaRegistro { get; set; }
        public string fechaAplicacion { get; set; }
    }

    public class DetallePagoMasivoCondiciones
    {
        public int idSolicitud { get; set; }
        public int idDomiciliacion { get; set; }
        public int idPagoDirecto { get; set; }
        public int idCanalPago { get; set; }
        public DateTime fechaPagoDesde { get; set; }
        public DateTime fechaPagoHasta { get; set; }
        public DateTime fechaRegistroDesde { get; set; }
        public DateTime fechaRegistroHasta { get; set; }
        public DateTime fechaAplicacionDesde { get; set; }
        public DateTime fechaAplicacionHasta { get; set; }
        public List<int> idEstatus { get; set; }
        public int idTipoConvenio { get; set; }
        public int idConvenio { get; set; }
        public int idProducto { get; set; }
    }

    public class ProcesoPagosAplicacion
    {
        public int idProceso { get; set; }
        public int idUsuario { get; set; }
        public string usuarioNombre { get; set; }
        public int idEstatus { get; set; }
        public string estatus { get; set; }
        public bool aplicar { get; set; }
        public int totalPagos { get; set; }
        public int totalProcesados { get; set; }
        public string fechaCreacion { get; set; }
        public List<int> idPagos { get; set; }        
        public string tipo { get; set; }
        public int reversaIdProceso { get; set; }
    }

    public class ProcesoPagosAplicacionCondiciones
    {
        public int idProceso { get; set; }
        public int idUsuario { get; set; }
        public int idEstatus { get; set; }
        public string fechaCreacionDesde { get; set; }
        public string fechaCreacionHasta { get; set; }
    }

    public class ProcesoPagosAplicacionDetalle
    {
        public int idDetalle { get; set; }
        public int idProceso { get; set; }
        public int idPago { get; set; }
        public int idEstatus { get; set; }
        public string mensajeEstatus { get; set; }        
        public string fechaAplicacion { get; set; }
        public int idSolicitud { get; set; }
        public decimal montoAplicado { get; set; }
        public decimal montoPorAplicar { get; set; }
    }

    public class ProcesoPagosAplicacionDetalleCondiciones
    {
        public int idDetalle { get; set; }
        public int idProceso { get; set; }
        public int idPago { get; set; }
        public int idEstatus { get; set; }
        public string fechaAplicacionDesde { get; set; }
        public string fechaAplicacionHasta { get; set; }
    }

    public class Pago
    {
        // Propiedades escalares
        public int IdPago { get; set; }
        public int IdDomiciliacion { get; set; }
        public int IdPagoDirecto { get; set; }
        public int IdPeticionVisaNet { get; set; }
        public int IdPeticionKushki { get; set; }
        public int IdSolicitud { get; set; }
        private CanalPago _canalPago;
        public CanalPago CanalPago
        {
            get { return (_canalPago == null) ? new CanalPago() : _canalPago; }
            set { this._canalPago = value; }
        }
        public decimal MontoCobrado { get; set; }
        public decimal MontoAplicado { get; set; }
        public decimal MontoPorAplicar { get; set; }
        public string CanalPagoClave { get; set; }
        public string CanalPagoDesc { get; set; }
        private TBCATEstatus _estatus;
        public TBCATEstatus Estatus
        {
            get { return (_estatus == null) ? new TBCATEstatus() : _estatus; }
            set { this._estatus = value; }
        }
        private Utils.DateTimeR _fechaPago;
        public Utils.DateTimeR FechaPago
        {
            get { return (_fechaPago == null) ? new Utils.DateTimeR() : _fechaPago; }
            set { this._fechaPago = value; }
        }
        private Utils.DateTimeR _fechaRegistro;
        public Utils.DateTimeR FechaRegistro
        {
            get { return (_fechaRegistro == null) ? new Utils.DateTimeR() : _fechaRegistro; }
            set { this._fechaRegistro = value; }
        }
        private Utils.DateTimeR _fechaAplicacion;
        public Utils.DateTimeR FechaAplicacion
        {
            get { return (_fechaAplicacion == null) ? new Utils.DateTimeR() : _fechaAplicacion; }
            set { this._fechaAplicacion = value; }
        }

        // Constructor
        public Pago()
        {
            this.CanalPago = new CanalPago();
            this.Estatus = new TBCATEstatus();
            this.FechaPago = new Utils.DateTimeR();
            this.FechaRegistro = new Utils.DateTimeR();
            this.FechaAplicacion = new Utils.DateTimeR();
        }
    }

    public class PagoAplicado
    {
        //Atributos de la clase 
        public int IdPago { get; set; }
        public int Recibo { get; set; }        
        public decimal AplicadoCapital { get; set; }
        public decimal AplicadoInteres { get; set; }
        public decimal AplicadoIGV { get; set; }
        public decimal AplicadoSeguro { get; set; }
        public decimal AplicadoGAT { get; set; }
        public Utils.DateTimeR FechaAplicacion { get; set; }
    }    

    public class AplicarPagoParametros
    {
        public int idPago { get; set; }
        public bool aplicarUnico { get; set; }
        public bool recalcular { get; set; }
        public int idProceso { get; set; }
    }

    public class DesaplicarPagoParametros
    {
        public int idPago { get; set; }
        public bool aplicarUnico { get; set; }
        public bool recalcular { get; set; }
        public int idProceso { get; set; }
    }
}
