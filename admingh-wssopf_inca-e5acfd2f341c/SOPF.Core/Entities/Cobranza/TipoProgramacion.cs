﻿namespace SOPF.Core.Entities.Cobranza
{
    public class TipoProgramacion
    {
        public int IdTipoProgramacion { get; set; }
        public string TipoProgramacionDescripcion { get; set; }
        public bool Activo { get; set; }
    }
}
