﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Entities.Cobranza
{
    [Table("PagosTabKushkiTarjeta", Schema = "Cobranza")]
    public class KushkiTarjeta
    {
        [Key]
        public int iIdKushkiTarjeta { get; set; }
        public int iIdPeticionKushki { get; set; }
        public string vNumTarjeta { get; set; }
        public string vMesExpiracion { get; set; }
        public string vAnioExpiracion { get; set; }
        public string vCVV { get; set; }
        public string vNomTarjHabiente { get; set; }
        public string vTipDocumento { get; set; }
        public string vEmail { get; set; }
        public string vDocumento { get; set; }
        public string vApeTarjHabiente { get; set; }
        public string vTelefono { get; set; }
        public int iIdAccionKushki { get; set; }
    }
}
