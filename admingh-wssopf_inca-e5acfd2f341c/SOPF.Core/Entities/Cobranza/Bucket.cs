﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;

namespace SOPF.Core.Entities.Cobranza
{
    public class Bucket
    {
        // Propiedades escalares
        public int IdBucket { get; set; }
        public string Descripcion { get; set; }
        public int? DiasMin { get; set; }
        public int? DiasMax { get; set; }        

        // Constructor
        public Bucket()
        {
            
        }
    }    
}
