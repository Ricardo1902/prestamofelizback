﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Entities.Cobranza
{
    [Table("PagosTabKushkiOrden", Schema = "Cobranza")]
    public class KushkiOrden
    {
        [Key]
        public int iIdKushkiOrden { get; set; }
        public int iIdPeticionKushki { get; set; }
        public string vNumCompra { get; set; }
        public string vIdProducto { get; set; }
        public double Monto { get; set; }
        public int iIdAccionKushki { get; set; }

    }
}
