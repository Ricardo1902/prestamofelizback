﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Cobranza
{
    public class Pagos
    {
        public Pagos()
        {
        }

        public class ChartCartera
        {
            public string AnioMes { get; set; }
            public decimal CarteraVencida { get; set; }
            public decimal Cobros { get; set; }
            public decimal PorAplicar { get; set; }
        }        
    }
}
