﻿using System;

namespace SOPF.Core.Entities.Cobranza
{
    public class Programacion
    {
        public int IdProgramacion { get; set; }
        public TipoProgramacion TipoProgramacion { get; set; }
        public GrupoLayout GrupoLayout { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }
        
        public Programacion()
        {
            TipoProgramacion = new TipoProgramacion();
            GrupoLayout = new GrupoLayout();
        }
    }
}
