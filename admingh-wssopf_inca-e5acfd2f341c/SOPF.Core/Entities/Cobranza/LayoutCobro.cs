﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SOPF.Core.Entities;

namespace SOPF.Core.Entities.Cobranza
{
    public class TipoLayoutCobro
    {
        // Propiedades escalares
        public int? IdTipoLayoutCobro { get; set; }
        public string TipoLayout { get; set; }
        public bool Activo { get; set; }

        // Constructor
        public TipoLayoutCobro()
        {
        }
    }

    public class LayoutCobro
    {
        public class LayoutGenerado
        {
            public int? IdLayoutGenerado { get; set; }
            public GrupoLayout GrupoLayout { get; set; }
            public TBCATEstatus Estatus { get; set; }
            public Utils.DateTimeR FechaRegistro { get; set; }
            public Utils.DateTimeR FechaTermino { get; set; }
            public string ResultadoMsg { get; set; }
            public int IdUsuarioRegistro { get; set; }
            public bool? EsAutomatico { get; set; }

            public List<Plantilla> Plantillas { get; set; }

            public LayoutGenerado()
            {
                this.GrupoLayout = new GrupoLayout();
                this.Estatus = new TBCATEstatus();
                this.FechaRegistro = new Utils.DateTimeR();
                this.FechaTermino = new Utils.DateTimeR();

                this.Plantillas = new List<Plantilla>();
            }

            public class Plantilla
            {
                public Plantilla()
                {
                    this.TipoLayout = new TipoLayoutCobro();
                }

                public int IdPlantillaEnvioCobro { get; set; }
                public TipoLayoutCobro TipoLayout { get; set; }
                public string NombrePlantilla { get; set; }
                public bool OperacionCorrecta { get; set; }
                public string MensajeOperacion { get; set; }
                public int TotalRegistros { get; set; }
                public decimal MontoTotal { get; set; }
                public bool ArchivoGenerado { get; set; }
                public int VecesGenerado { get; set; }
            }
        }

        #region "Estructuras de Layouts(Archivos)"

        public class LayoutNetCash //por canal netcash 
        {
            public int SOLICITUD { get; set; } //por canal netcash y visanet
            public string TIPODOC { get; set; } //por canal netcash
            public string NUMDOC { get; set; }//por canal netcash
            public string NOMBRECLIENTE { get; set; }//por canal netcash
            public decimal IMPORTE { get; set; } //por canal netcash y visanet (cantidad cobrar en recibo)
            public string CUENTA { get; set; }//por canal netcash
        }

        public class LayoutVisaNet
        {
            public int Solicitud { get; set; }
            public decimal Monto { get; set; }
            public string NoTarjeta { get; set; }
            public int MesExpiracion { get; set; }
            public int AnioExpiracion { get; set; }
        }

        public class LayoutInterbank
        {
            public string RowInfo { get; set; }
        }

        public class LayoutSumatoria
        {
            public int NumSolicitudes { get; set; }
            public int NumeroRecibos { get; set; }
            public decimal CantidadCobrar { get; set; }
        }

        #endregion
    }   
}
