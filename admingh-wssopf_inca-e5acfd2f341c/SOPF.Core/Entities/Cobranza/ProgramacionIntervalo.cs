﻿namespace SOPF.Core.Entities.Cobranza
{
    public class ProgramacionIntervalo
    {
        public int IdIntervalo { get; set; }
        public string Intervalo { get; set; }
        public bool Activo { get; set; }
    }
}
