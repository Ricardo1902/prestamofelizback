﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Entities.Cobranza
{
    [Table(name: "PagosDetMasivosKushki", Schema = "Cobranza")]
    public class PagoMasivoDetalleKushki
    {
        [Key]
        public int iIdPagosDetMasivosKushki { get; set; }
        public int iIdPagosMasivosKushki { get; set; }
        public string vIdProducto { get; set; }
        public decimal Monto { get; set; }
        public string vNoTarjeta { get; set; }
        public string vNomTarjHabiente { get; set; }
        public string vTelefono { get; set; }
        public int? iIdPeticion { get; set; }
        public bool? bError { get; set; }
        public string vMensaje { get; set; }
        public DateTime sdFechaRegistro { get; set; }
    }
}