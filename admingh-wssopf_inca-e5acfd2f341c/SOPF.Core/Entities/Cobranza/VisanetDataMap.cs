﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Entities.Cobranza
{
    [Table("TB_VisanetDataMap", Schema = "Cobranza")]
    public class VisanetDataMap
    {
        [Key]
        public int IdVisanetDataMap { get; set; }
        public int IdPeticionVisanet { get; set; }
        public string Currency { get; set; }
        public string Transaction_date { get; set; }
        public string Terminal { get; set; }
        public string Action_Code { get; set; }
        public string Trace_Nummber { get; set; }
        public string Signature { get; set; }
        public string Brand { get; set; }
        public string Card { get; set; }
        public string Merchant { get; set; }
        public string Status { get; set; }
        public string Adquirente { get; set; }
        public string Action_Description { get; set; }
        public string Id_Unico { get; set; }
        public string Amount { get; set; }
        public string Process_Code { get; set; }
        public string Transaction_Id { get; set; }
        public string Authorization_Code { get; set; }
        public int IdAccionVisanet { get; set; }
    }   
}
