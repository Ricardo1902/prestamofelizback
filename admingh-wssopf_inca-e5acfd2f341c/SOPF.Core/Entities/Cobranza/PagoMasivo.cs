﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Entities.Cobranza
{
    [Table(name: "TB_PagoMasivo", Schema = "Cobranza")]
    public class PagoMasivo
    {
        [Key]
        public int IdPagoMasivo { get; set; }
        public string NombreArchivo { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int? TotalRegistros { get; set; }
        public int? TotalProcesado { get; set; }
        public decimal? MontoTotal { get; set; }
        public int IdEstatusProceso { get; set; }
        public DateTime? UltimaActualizacion { get; set; }
        public DateTime? FechaFin { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public bool? Error { get; set; }
        public string Mensaje { get; set; }
        public int? TotalAprobados { get; set; }
    }
}