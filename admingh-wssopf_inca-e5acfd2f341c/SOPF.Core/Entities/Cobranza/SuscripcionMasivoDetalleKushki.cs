﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Entities.Cobranza
{
    [Table(name: "SuscripcionDetMasivaKushki", Schema = "Cobranza")]
    public class SuscripcionMasivoDetalleKushki
    {
        [Key]
        public int iIdSuscripcionDetMasivaKushki { get; set; }
        public int iIdSuscripcionMasivaKushki { get; set; }
        public string vNoTarjeta { get; set; }
        public string vMesExpiracion { get; set; }
        public string vAnioExpiracion { get; set; }
        public string vCVV { get; set; }
        public string vNomTarjHabiente { get; set; }
        public string vTipDocumento { get; set; }
        public string vEmail { get; set; }
        public string vDocumento { get; set; }
        public string vApeTarjHabiente { get; set; }
        public string vTelefono { get; set; }
        public int? iIdPeticion { get; set; }
        public bool? bError { get; set; }
        public string vMensaje { get; set; }
        public DateTime sdFechaRegistro { get; set; }
    }
}