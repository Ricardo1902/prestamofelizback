﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Entities.Cobranza
{
    [Table(name: "PagosMasivosKushki", Schema = "Cobranza")]
    public class PagoMasivoKushki
    {
        [Key]
        public int iIdPagosMasivosKushki { get; set; }
        public string vNombreArchivo { get; set; }
        public DateTime sdFechaRegistro { get; set; }
        public int? iTotalRegistros { get; set; }
        public int? iTotalProcesado { get; set; }
        public int? iTotalNoProcesado { get; set; }
        public decimal? MontoTotal { get; set; }
        public decimal? MontoCobrado { get; set; }
        public decimal? dePorcentajeCobrado { get; set; }
        public int iIdEstatusProceso { get; set; }
        public DateTime? sdUltimaActualizacion { get; set; }
        public DateTime? sdFechaFin { get; set; }
        public int iIdUsuarioRegistro { get; set; }
        public bool? bError { get; set; }
        public string vMensaje { get; set; }
        public int? iTotalAprobados { get; set; }
    }
}