﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.Entities.Cobranza
{
    public class VisanetValidarTarjeta
    {
        public int IdSolicitud { get; set; }
        public string NombreCliente { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string NumeroTarjeta { get; set; }
        public string MesVencimientoTarjeta { get; set; }
        public string AnioVencimientoTarjeta { get; set; }
        public string Correo { get; set; }
        public string JsonEnvio { get; set; }
        public string JsonRespuesta { get; set; }
        public Utils.DateTimeR FechaValidado { get; set; }
        public bool Validado { get; set; }

        public VisanetValidarTarjeta()
        {
            this.FechaValidado = new Utils.DateTimeR();
        }
    }
}
