﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Entities.Cobranza
{
    [Table("PagosTabPeticionKushki", Schema = "Cobranza")]
    public class PeticionKushki
    {
        [Key]
        public int iIdPeticionKushki { get; set; }
        public int iIdUsuario { get; set; }
        public DateTime sdFechaRegistro { get; set; }
        public string vUrl { get; set; }
        public string vJsSuscripcionEnvio { get; set; }
        public string vJsPagoEnvio { get; set; }
        public string vJsSuscripcionRecep { get; set; }
        public string vJsPagoRecep { get; set; }
        public string vIdSuscripcion { get; set; }
        public string vIdPago { get; set; }
        public bool bExito { get; set; }
    }
}
