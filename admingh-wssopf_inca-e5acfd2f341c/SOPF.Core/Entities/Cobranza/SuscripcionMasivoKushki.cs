﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Entities.Cobranza
{
    [Table(name: "SuscripcionMasivaKushki", Schema = "Cobranza")]
    public class SuscripcionMasivoKushki
    {
        [Key]
        public int iIdSuscripcionMasivaKushki { get; set; }
        public string vNombreArchivo { get; set; }
        public DateTime sdFechaRegistro { get; set; }
        public int? iTotalRegistros { get; set; }
        public int? iTotalProcesado { get; set; }
        public int? iTotalNoProcesado { get; set; }
        public int iIdEstatusProceso { get; set; }
        public DateTime? sdUltimaActualizacion { get; set; }
        public DateTime? sdFechaFin { get; set; }
        public int iIdUsuarioRegistro { get; set; }
        public bool? bError { get; set; }
        public string vMensaje { get; set; }
        public int? iTotalAprobados { get; set; }
    }
}