﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SOPF.Core.Entities.Cobranza
{
    [Table(name: "TB_PagoMasivoDetalle", Schema = "Cobranza")]
    public class PagoMasivoDetalle
    {
        [Key]
        public int IdPagoMasivoDetalle { get; set; }
        public int IdPagoMasivo { get; set; }
        public int IdCuenta { get; set; }
        public decimal Monto { get; set; }
        public string NoTarjeta { get; set; }
        public int? MesExpiracion { get; set; }
        public int? AnioExpiracion { get; set; }
        public int? IdPeticion { get; set; }
        public bool? Error { get; set; }
        public string Mensaje { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}