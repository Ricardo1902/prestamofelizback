#pragma warning disable 150811
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     EdgarSV.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Dimex – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	CatEstatus.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-08-11 	Edgar Sánchez Vidales	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.AhorroVoluntario
{
    public class CatEstatus
    {
		//Atributos de la clase 
		public Int32 IdEstatus { get; set; }
		public string Descripcion { get; set; }
		public Int32 CveEstatus { get; set; }

    }
}