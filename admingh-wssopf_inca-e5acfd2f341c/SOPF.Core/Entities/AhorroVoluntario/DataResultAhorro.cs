﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.AhorroVoluntario
{
    public class DataResultAhorro
    {
        public string Mensaje { get; set; }
        public int Obejto  {get;set;}

        public partial class SelCreditos
        {
            public int idcuenta { get; set; }
            public decimal capital { get; set; }
            public decimal erogacion { get; set; }
            public DateTime fecha { get; set; }
            public string  cliente { get; set; }
            public string desarrolladora { get; set; }

        }

        public partial class SelRecibos
        {
            public int idcuenta { get; set; }
            public decimal recibo { get; set; }            
            public DateTime fch_pago { get; set; }
            public decimal sdo_insoluto { get; set; }
            public decimal monto { get; set; }     

        }
    }
}
