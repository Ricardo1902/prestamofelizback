#pragma warning disable 150527
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	Desarrolladora.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-05-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.AhorroVoluntario
{
    public class Desarrolladora
    {
		//Atributos de la clase 
		public Int32 IdDesarrolladora { get; set; }
		public string NombreCorto { get; set; }
		public string RazonSocial { get; set; }
		public string RFC { get; set; }
		public string Contacto { get; set; }
		public string Email { get; set; }
		public string Direccion { get; set; }
		public string NumExterior { get; set; }
		public string NumInterior { get; set; }
		public Int32 IdColonia { get; set; }
		public string Telefono { get; set; }
		public Boolean Estatus { get; set; }

    }
}