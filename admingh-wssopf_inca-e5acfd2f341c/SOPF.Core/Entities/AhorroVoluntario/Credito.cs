#pragma warning disable 150408
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	Credito.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.AhorroVoluntario
{
    public class Credito
    {
		//Atributos de la clase 
		public Int32 Idcuenta { get; set; }
		public decimal Capital { get; set; }
        public decimal erogacion { get; set; }
		public string Frecuencia { get; set; }
		public Int32 Plazo { get; set; }
		public decimal SdoCapital { get; set; }
		public Int32 IdEstatus { get; set; }
        public Int32 IdCliente { get; set; }
        public Int32 IdPromotor { get; set; }
        public DateTime FechaCredito { get; set; }
        public DateTime FechaEstatus { get; set; }
        public Int32 idPromotor { get; set; }
        public Int32 idBanco { get; set; }
        public string Clabe { get; set; }
        public Int32 idTipoCredito { get; set; }
        public Int32 SubTipo { get; set; }
        public DateTime fch_primerpago { get; set; }
        public string Literal { get; set; }
    }
}