#pragma warning disable 150408
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	Movimientos.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.AhorroVoluntario
{
    public class Movimientos
    {
		//Atributos de la clase 
		public Int32 IdCuenta { get; set; }
		public Int32 IdRecibo { get; set; }
		public Int32 TipoMovimiento { get; set; }
		public decimal Monto { get; set; }
		public DateTime FechaMovimiento { get; set; }
		public Int64 NumeroTransaccion { get; set; }
		public string Concepto { get; set; }

    }
}