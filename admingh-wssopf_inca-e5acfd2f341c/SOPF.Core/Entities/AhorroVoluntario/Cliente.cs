#pragma warning disable 150408
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	Cliente.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-04-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.AhorroVoluntario
{
    public class Cliente
    {
		//Atributos de la clase 
		public Int32 Idcliente { get; set; }
		public string NombreCompleto { get; set; }
		public string Domicilio { get; set; }
		public string RFC { get; set; }
		public string TelCasa { get; set; }
		public string TelCelular { get; set; }
		public string NSS { get; set; }
        public string Nombres { get; set; }
        public string ApePat { get; set; }
        public string ApeMat { get; set; }
        public string RegistroPatronal { get; set; }
        public string EMail { get; set; }
        public string NombrePatron { get; set; }
        public string DomNuevo { get; set; }
        public int Edad { get; set; }
    }
}