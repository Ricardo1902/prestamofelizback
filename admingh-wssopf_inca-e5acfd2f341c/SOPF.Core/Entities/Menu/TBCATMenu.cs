#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBCATMenu.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Menu
{
    public class TBCATMenu
    {
		//Atributos de la clase 
		public Int32 IdMenu { get; set; }
		public Int32 SistemaId { get; set; }
		public Int32 IdModulo { get; set; }
		public string Menu { get; set; }
		public string Descripcion { get; set; }
		public string Carpeta { get; set; }
		public string URL { get; set; }
		public Int32 Nivel { get; set; }
		public Int32 Padre { get; set; }
		public string Hijo { get; set; }
		public Int32 Tipo { get; set; }
		public Int32 Orden { get; set; }
		public Int32 IdEstatus { get; set; }

    }
}