﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Creditos
{
    public class DataResultCredito
    {
        public partial class Usp_ObtenerTablaAmortizacion : TBRecibos
        {
            public decimal pmt { get; set; }
            public string estatus { get; set; }
            public string FechaPago { get; set; }
        }
        public partial class Usp_HistorialPagos : TBCobranza
        {
            public decimal erogacion { get; set; }
            public decimal TotalPago { get; set; }
        }

        public partial class Usp_SelCredito 
        {
            public int IdCuenta { get; set; }
            public int IdSolicitud { get; set; }
            public DateTime FchCredito { get; set; }
            public string Cliente { get; set; }
            public string convenio { get; set; }
            public string producto { get; set; }
            public string estatus { get; set; }
            public decimal Capital { get; set; }
            public decimal Interes { get; set; }
            public decimal IvaIntere { get; set; }
            public decimal capitalD { get; set; }
            public decimal interesD { get; set; }
            public decimal ivaIntereD { get; set; }
            public string promotor { get; set; }
            public string sucursal { get; set; }
            public string analista { get; set; }
            public string pedido { get; set; }
            public int isReestructura { get; set; }
        }
        public partial class Usp_Error
        {
            public int Error { get; set; }
            public string Mensaje { get; set; }
        }

        public partial class Usp_SelDetalleCorteDispersion: CorteDispersionDetalle
        {
            public string Cliente { get; set; }
            public decimal Capital { get; set; }
            public string Reestructura { get; set; }
            public string Clabe { get; set; }

        }
        public partial class Usp_SelDatosPago 
        {
            
            public decimal Erogacion { get; set; }
            public decimal TotalPagar { get; set; }
            public DateTime Fecha { get; set; }
            public decimal MontoMaximo { get; set; }

        }
      
    }
}
