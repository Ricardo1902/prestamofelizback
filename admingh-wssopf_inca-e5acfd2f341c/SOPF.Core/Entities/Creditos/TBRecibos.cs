#pragma warning disable 140827
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBRecibos.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Creditos
{
    public class TBRecibos
    {
		//Atributos de la clase 
		public Decimal IdCuenta { get; set; }
		public Int32 Recibo { get; set; }
		public DateTime FchRecibo { get; set; }
		public DateTime FchEmision { get; set; }
		public Decimal Capital { get; set; }
		public Decimal Interes { get; set; }
		public Decimal IvaIntere { get; set; }
		public Decimal SdoInsoluto { get; set; }
		public Decimal SdoRecibo { get; set; }
		public Int32 IdEstatus { get; set; }
		public Decimal Totalinteres { get; set; }
		public Int32 IdTipoRecibo { get; set; }
		public Decimal SdoCapita { get; set; }
		public Decimal SdoIntere { get; set; }
		public Decimal SdoIvaIn { get; set; }
		public Decimal InteresGtoCob { get; set; }
		public Decimal IvaGtoCob { get; set; }

        
        public string Estatus { get; set; }
        public decimal IGV { get; set; }
        public decimal Seguro { get; set; }
        public decimal GAT { get; set; }
        public decimal CapitalInicial { get; set; }
        public decimal CapitalInsoluto { get; set; }
        public decimal SaldoCapital { get; set; }
        public decimal SaldoInteres { get; set; }
        public decimal SaldoIGV { get; set; }
        public decimal SaldoSeguro { get; set; }
        public decimal SaldoGAT { get; set; }
        public decimal SaldoRecibo { get; set; }
        public decimal TotalRecibo { get; set; }

    }
}