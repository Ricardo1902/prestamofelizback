﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOPF.Core.Entities.Creditos
{
    public class TipoCreditoCliente
    {
        public int IdTipoCredito { get; set; }
        public string Credito { get; set; }
    }
}
