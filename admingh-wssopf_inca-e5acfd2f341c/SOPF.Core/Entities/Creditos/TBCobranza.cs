#pragma warning disable 140827
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using SOPF.Core.Entities.Cobranza;
using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz– 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBCobranza.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-27 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Creditos
{
    public class TBCobranza
    {
		//Atributos de la clase 
		public Int32 Id { get; set; }
		public Decimal IdCuenta { get; set; }
		public Int32 Recibo { get; set; }
		public DateTime FchMvto { get; set; }
		public Decimal Capital { get; set; }
		public Decimal Interes { get; set; }
		public Decimal IvaIntere { get; set; }
		public string TipoMvto { get; set; }
		public string CveConcC { get; set; }
		public string CveCobrC { get; set; }
		public string ObservacC { get; set; }
		public string CveLPag { get; set; }
		public string ConsCobr { get; set; }
		public Int16 EstCred { get; set; }
		public DateTime FchReal { get; set; }
		public string ClaveTipoInstrumento { get; set; }
		public string ClaveTipoOperacion { get; set; }
		public Int32 IdEmicionGroup { get; set; }
		public Int32 IdPagoCapital { get; set; }
		public Decimal InteresGtoCob { get; set; }
		public Decimal IvaGtoCob { get; set; }

        public int IdPago { get; set; }
        public int IdDomiciliacion { get; set; }
        public int IdPagoDirecto { get; set; }
        public int IdPeticionVisaNet { get; set; }
        public int IdSolicitud { get; set; }

        public string CanalPago { get; set; }        
        public decimal MontoCobrado { get; set; }
        public decimal MontoAplicado { get; set; }
        public decimal MontoPorAplicar { get; set; }
        public string CanalPagoClave { get; set; }
        public string CanalPagoDesc { get; set; }
        public  string Estatus { get; set; }       
        public DateTime FechaPago { get; set; }
        //public DateTime? FechaPago2 { get; set; }
        public DateTime FechaRegistro { get; set; }     
        public DateTime FechaAplicacion { get; set; }
        public decimal IGV { get; set; }
        public decimal GAT { get; set; }
        public decimal Seguro { get; set; }
    }
}