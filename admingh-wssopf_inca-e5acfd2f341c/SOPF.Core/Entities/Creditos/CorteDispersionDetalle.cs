#pragma warning disable 141229
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	CorteDispersionDetalle.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-12-29 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Creditos
{
    public class CorteDispersionDetalle
    {
		//Atributos de la clase 
		public Int32 Id { get; set; }
		public Int32 IdCorte { get; set; }
		public Int32 Idcuenta { get; set; }
		public Int32 Idsolicitud { get; set; }
		public Boolean IdReestructura { get; set; }
		public decimal MontoTransferencia { get; set; }
		public Boolean Correcto { get; set; }

    }
}