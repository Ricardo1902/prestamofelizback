﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Creditos
{
    public class PagosAplicados
    {
       public int       id_solicitud{get;set;}
       public decimal monto_aplicado { get; set; }
       public decimal capital { get; set; }
       public decimal interes { get; set; }
       public decimal iva { get; set; }
       public decimal Gto_cob { get; set; }
       public decimal IVA_Gto_cob { get; set; }
       public string fecha_cobro { get; set; }
       public string Comentario { get; set; }
       public int IdGrupoAplicacion { get; set; }
       public string Convenio { get; set; }
       public string Sucursal { get; set; }
       public string Flag_mora { get; set; }
    }
}
