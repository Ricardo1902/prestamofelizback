﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Creditos
{
    public class CuentasDomiciliacion
    {

        public partial class Bancomer
        {
            public string NumeroBloque { get; set; }
            public string FechaPresentacion { get; set; }
            public int NumeroSecuencia { get; set; }
            public decimal Importe { get; set; }
            public string FechaLiquidacion { get; set; }
            public string FechaVencimiento { get; set; }
            public string Banco { get; set; }
            public string TipoCuenta { get; set; }
            public string Clabe { get; set; }
            public string Cliente { get; set; }
            public string referencia { get; set; }
            public string referencianum { get; set; }
            public int idcuenta { get; set; }
            public decimal erogacion { get; set; }
        }

        public partial class Banorte
        {
            public string Emisora { get; set; }
            public string FechaProceso { get; set; }
            public int consecutivo { get; set; }
            public int TotalRegistros { get; set; }
            public decimal ImporteTotal { get; set; }
            public string FechaAplicacion { get; set; }
            public int Referencia { get; set; }
            public decimal Importe { get; set; }
            public string Banco { get; set; }
            public string TipoCuenta { get; set; }
            public string Clabe { get; set; }
            public int idCuenta { get; set; }
            
        }

        public partial class Banamex
        {
            public string NumeroBloque { get; set; }
            public string FechaPresentacion { get; set; }
            public int NumeroSecuencia { get; set; }
            public decimal Importe { get; set; }
            public string FechaLiquidacion { get; set; }
            public string FechaVencimiento { get; set; }
            public string Banco { get; set; }
            public string TipoCuenta { get; set; }
            public string Clabe { get; set; }
            public string Cliente { get; set; }
            public int referencia { get; set; }
            public string referencianum { get; set; }
            public int idcuenta { get; set; }
            public decimal erogacion { get; set; }
            public string Secuencia { get; set; }
        }
    }
}
