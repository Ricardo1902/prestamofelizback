﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Creditos
{
    public class AutorizaPagosEfectivo
    {
        public Int32 IdFolio { get; set; }
        public Int32 IdGrupo { get; set; }
        public DateTime Fecha { get; set; }
        public Int32 IdSolicitud { get; set; }
        public Int32 IdCuenta { get; set; }
        public decimal Monto { get; set; }
        public string Estatus { get; set; }
        public string DescOrigen { get; set; }

    }
}
