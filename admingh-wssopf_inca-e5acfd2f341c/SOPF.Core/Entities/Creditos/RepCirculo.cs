#pragma warning disable 141208
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	RepCirculo.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-12-08 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Creditos
{
    public class RepCirculo
    {
		//Atributos de la clase 
		public int IdCuenta { get; set; }
		public string ClaveOtorgante { get; set; }
		public string NombreOtorgante { get; set; }
		public string FechaExtraccion { get; set; }
		public string Versi { get; set; }
		public string ApellidPaterno { get; set; }
		public string ApellidoMaterno { get; set; }
		public string Nombres { get; set; }
		public string Direccion { get; set; }
		public string Ciudad { get; set; }
		public string Estado { get; set; }
		public string CP { get; set; }
		public Decimal CuentaActual { get; set; }
		public string TipoResponsabilidad { get; set; }
		public string TipoCuenta { get; set; }
		public string TipoContrato { get; set; }
		public string CveUnicaMoneda { get; set; }
		public string FrecuenciaPagos { get; set; }
		public Decimal MontoPagar { get; set; }
		public string FchApertura { get; set; }
        public string FchUltimoPago { get; set; }
        public string FchUltimaCompra { get; set; }
        public string FechaCorte { get; set; }
		public Decimal CreditoMaximo { get; set; }
		public Decimal SaldoActual { get; set; }
		public Decimal LimiteCredito { get; set; }
		public string PagoActual { get; set; }
		public Decimal SaldoInsoluto { get; set; }
        public Int64 TotalSaldoAct { get; set; }
        public Int64 TotalSaldoVen { get; set; }
		public Int32 TotalNombRepo { get; set; }
		public Int32 TotalDireRepo { get; set; }
		public Int32 TotalEmplRepo { get; set; }
		public Int32 TotalCuenRepo { get; set; }
		public string DomicilioDev { get; set; }
        public string FechaCierre { get; set; }
        public Int32 NumeroPagos { get; set; }
        public Int32 Vencido { get; set; }
        public Int32 RecibosVencidos { get; set; }
        public string FechaNacimiento { get; set; }
        public string RFC { get; set; }
        public string CURP { get; set; }
        public string NACIONALIDAD { get; set; }
        public string NombreEmpresa { get; set; }
        public string DireccionEmpresa { get; set; }
        public string ColoniaEmpresa { get; set; }
        public string CiudadEmpresa { get; set; }
        public string EstadoEmpresa	 { get; set; }
        public string CPEmpresa	{ get; set; }
        public string TelefonoEmpresa	{ get; set; }
        public string Extension	{ get; set; }
        public string Puesto	{ get; set; }
        public string FechaContratacion	{ get; set; }
        public string ClaveMonedaEmp	{ get; set; }
        public string Salario	{ get; set; }
        public string FechaVerificacion { get; set; }
    }
}