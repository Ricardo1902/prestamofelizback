﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Creditos
{
    public class AfiliacionDom_Banorte
    {
        public DateTime Fecha { get; set; }
        public int NumeroAfiliaciones { get; set; }
        public int ReferenciaServicio { get; set; }
        public string BancoReceptor { get; set; }
        public string nombreTitular { get; set; }
        public string TipoCuenta { get; set; }
        public string Clabe { get; set; }
        public string NumeroId { get; set; }
        public string RFC { get; set; }
        public int idcuenta { get; set; }
        
    }
}
