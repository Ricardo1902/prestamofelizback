﻿using System;

namespace SOPF.Core.Entities.Creditos
{
    public class ConsultaExpediente
    {
        public int IdSolicitud { get; set; }
        public Utils.DateTimeR FechaSolicitud { get; set; }
        public string NombreCliente { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string DNI { get; set; }
        public string GDriveID { get; set; }

        public ConsultaExpediente()
        {
            this.FechaSolicitud = new Utils.DateTimeR();
        }
    }
}
