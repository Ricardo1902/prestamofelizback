﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Creditos
{
    public class CuentasDispersion
    {
        public string Operacion { get; set; }
        public string ClaveId { get; set; }
        public string CuentaOrigen { get; set; }
        public string CuentaDestino { get; set; }
        public string MontoTransferencia { get; set; }
        public string Referencia { get; set; }
        public string Descripcion { get; set; }
        public string RFC { get; set; }
        public Decimal iva { get; set; }
        public string instruccion { get; set; }
        public int TipoCambio { get; set; }
        public string fecha { get; set; }
    }
}
