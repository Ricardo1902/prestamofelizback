﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Creditos
{
    public class Liquidacion
    {
        public int idsolicitud { get; set; }
        public string cliente { get; set; }
        public int idcuenta { get; set; }
        public decimal TotSdoVen { get; set; }
        public decimal TotSdoCur { get; set; }
        public decimal TotSdoCap { get; set; }
        public decimal TotPenali { get; set; }
        public decimal TotLiqui { get; set; }
        public DateTime Fec_vigencia { get; set; }
    }
}
