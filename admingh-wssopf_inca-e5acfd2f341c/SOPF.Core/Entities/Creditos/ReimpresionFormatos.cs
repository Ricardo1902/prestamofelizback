﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Creditos
{
    public class ReimpresionFormatos
    {
        public Int32 IdReimp { get; set; }
        public string Formato { get; set; }
        public Int32 FolioInicial { get; set; }
        public Int32 FolioFinal { get; set; }
        public string Comentario { get; set; }
        public Int32 Cantidad { get; set; }

    }
}
