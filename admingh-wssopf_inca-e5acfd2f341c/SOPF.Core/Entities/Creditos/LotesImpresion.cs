﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Creditos
{
    public class LotesImpresion

        {
            public Int32 IdLote { get; set; }
            public DateTime Fecha { get; set; }
            public string Formato { get; set; }
            public Int32 FolioInicial { get; set; }
            public Int32 FolioFinal { get; set; }
            public Int32 IdEstatus { get; set; }
            public string EstatusLote { get; set; }
            public Int32 Cantidad { get; set; }
            public Int32 FolioMin { get; set; }
            public Int32 FolioMax { get; set; }

        }


}
