#pragma warning disable 140819
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;
using SOPF.Core;
using System.Collections.Generic;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	TBCreditos.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-08-19 	Juan Carlos García	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Creditos
{
    public class TBCreditos
    {
        //Atributos de la clase 
        public Decimal IdCuenta { get; set; }
        public DateTime FchCredito { get; set; }
        public Decimal IdSolicitud { get; set; }
        public string CveICobr { get; set; }
        public Decimal Erogacion { get; set; }
        public Decimal Capital { get; set; }
        public Decimal Interes { get; set; }
        public Decimal IvaIntere { get; set; }
        public Decimal SdoCapita { get; set; }
        public Decimal SdoIntere { get; set; }
        public Decimal SdoIvaIn { get; set; }
        public Decimal Moratorio { get; set; }
        public Decimal IvaMorato { get; set; }
        public Decimal SdoPorApl { get; set; }
        public Decimal SdoVencido { get; set; }
        public Int32 IdEstatus { get; set; }
        public DateTime FchEstatus { get; set; }
        public Int32 IdMotivo { get; set; }
        public Int32 PorcenIva { get; set; }
        public Int32 NoRecPag { get; set; }
        public Int32 DiasVenc { get; set; }
        public Int32 DiasInac { get; set; }
        public Int32 DiaMasVe { get; set; }
        public string EstaAdmin { get; set; }
        public DateTime FchUltPago { get; set; }
        public DateTime FchGenerado { get; set; }
        public Int32 Mientras { get; set; }
        public Int32 Reposicion { get; set; }
        public Int32 IdCuentaReposicion { get; set; }
        public Int32 GenReposicion { get; set; }
        public DateTime Fch1erPago { get; set; }
        public Int32 IdFondeo { get; set; }
        public DateTime FchCobro { get; set; }
        public Int32 EstCobro { get; set; }
        public Int32 PendObs { get; set; }
        public Int32 IdEFondeador { get; set; }
        public DateTime FchLiberado { get; set; }
        public Int32 IdTipoCobranza { get; set; }
        public Decimal IdCuentaRest { get; set; }
        public string ClaveTipoInstrumento { get; set; }
		public Int16 CodVerif { get; set; }
        public DateTime ControlUltimoPago { get; set; }
        public Int32 VerifReestructura { get; set; }
        public DateTime FchTermino { get; set; }  
		public Int32 idCliente { get; set; }
	     public string Estatus { get; set; }
        public String Banco { get; set; }
        public String Clabe { get; set; }
        public String Cliente { get; set; }
        public String Convenio { get; set; }
        public String Linea { get; set; }
        public String Producto { get; set; }
        // public String Estatus { get; set; }
        public String Promotor { get; set; }
        public String Sucursal { get; set; }
        public String Analista { get; set; }
        public String Pedido { get; set; }
        public Int32 isReestructura { get; set; }

        public partial class Liquidacion
        {
            public int? IdLiquidacion { get; set; }
            public int? IdSolicitud { get; set; }
            public int? IdPago { get; set; }
            public string NumeroTransaccion { get; set; }
            public TBCATEstatus Estatus { get; set; }
            public Utils.DateTimeR FechaLiquidar { get; set; }
            public decimal CapitalLiquidar { get; set; }
            public decimal InteresLiquidar { get; set; }
            public decimal IGVLiquidar { get; set; }
            public decimal SeguroLiquidar { get; set; }
            public decimal GATLiquidar { get; set; }
            public decimal TotalLiquidar { get; set; }
            public int IdUsuarioRegistro { get; set; }
            public int IdUsuarioConfirma { get; set; }
            public int IdUsuarioCancela { get; set; }
            public Utils.DateTimeR FechaConfirmacion { get; set; }
            public Utils.DateTimeR FechaCancelacion { get; set; }
            public Utils.DateTimeR FechaRegistro { get; set; }
            public List<LiquidacionDetalle> Detalle { get; set; }

            public Liquidacion()
            {
                this.FechaLiquidar = new Utils.DateTimeR();
                this.FechaConfirmacion = new Utils.DateTimeR();
                this.FechaCancelacion = new Utils.DateTimeR();
                this.FechaRegistro = new Utils.DateTimeR();

                this.Estatus = new TBCATEstatus();
                this.Detalle = new List<LiquidacionDetalle>();
            }

            public partial class LiquidacionDetalle
            {
                public int IdLiquidacion { get; set; }
                public int IdSolicitud { get; set; }
                public int IdCuenta { get; set; }
                public int Recibo { get; set; }
                public string TipoReciboCalculo { get; set; }
                public int DayDiffVenc { get; set; }
                public TBCATEstatus EstatusRecibo { get; set; }
                public DateTime? FechaRecibo { get; set; }
                public decimal CapitalInicial { get; set; }
                public decimal SaldoCapital { get; set; }
                public decimal SaldoInteres { get; set; }
                public decimal SaldoIGV { get; set; }
                public decimal SaldoSeguro { get; set; }
                public decimal SaldoGAT { get; set; }
                public decimal LiquidaCapital { get; set; }
                public decimal LiquidaInteres { get; set; }
                public decimal LiquidaIGV { get; set; }
                public decimal LiquidaSeguro { get; set; }
                public decimal LiquidaGAT { get; set; }
                public DateTime FechaRegistro { get; set; }

                public LiquidacionDetalle()
                {
                    this.EstatusRecibo = new TBCATEstatus();
                }
            }
        }

        public partial class CalculoLiquidacion
        {
            public int? IdSolicitud { get; set; }
            public DateTime FechaCredito { get; set; }
            public decimal Cuota { get; set; }
            public string DNICliente { get; set; }
            public string NombreCliente { get; set; }
            public string ApPaternoCliente { get; set; }
            public string ApMaternoCliente { get; set; }
            public decimal MontoCredito { get; set; }
            public string DireccionCliente { get; set; }
            public decimal TasaAnual { get; set; }
            public Utils.DateTimeR FechaLiquidacion { get; set; }
            public DateTime FechaPrimerPago { get; set; }
            public DateTime FechaUltimoPago { get; set; }
            public decimal TotalLiquidacion { get; set; }
            public decimal MontoComision { get; set; }
            public int Plazo { get; set; }
            public List<CalculoLiquidacionDetalle> DetalleLiquidacion { get; set; }

            public CalculoLiquidacion()
            {
                this.FechaLiquidacion = new Utils.DateTimeR();
                this.DetalleLiquidacion = new List<CalculoLiquidacionDetalle>();
            }

            public partial class CalculoLiquidacionDetalle
            {
                public int Recibo { get; set; }
                public DateTime FechaRecibo { get; set; }
                public string TipoReciboCalculo { get; set; }
                public decimal LiquidaCapital { get; set; }
                public decimal LiquidaInteres { get; set; }
                public decimal LiquidaIGV { get; set; }
                public decimal LiquidaSeguro { get; set; }
                public decimal LiquidaGAT { get; set; }
                public decimal LiquidaTotal { get; set; }
                public decimal LiquidaOtrosCargos { get; set; }
                public decimal LiquidaIGVOtrosCargos { get; set; }
            }
        }

        public partial class GeneraReferenciaCIE
        {
            public string ErrMensaje { get; set; }
            public string ReferenciaCompleta { get; set; }
            public string DigVerificador { get; set; }

        }

        public partial class LiquidacionRegistro
        {
            public int idLiqui { get; set; }
            public int idsolicitud { get; set; }
            public DateTime FechaRegistro { get; set; }
            public DateTime FechaDisponible { get; set; }
            public int idestatus { get; set; }
        }

        public partial class Finiquitos
        {
            //Atributos de la clase 
            public Int32 IdSolicitud { get; set; }
            public Int32 IdCliente { get; set; }
            public Int32 IdCuenta { get; set; }
            public string NombreCompleto { get; set; }
            public Decimal MontoUltPago { get; set; }
            public DateTime FecUltPago { get; set; }
            public string TipoLiqui { get; set; }
            public Int32 IdEstatus { get; set; }
            public string CantLetraUltPago { get; set; }
            public Int32 IdSolRees { get; set; }
            public DateTime FecIniSolRees { get; set; }
            public Decimal MontoSolRees { get; set; }
            public Decimal SdoLiquiRees { get; set; }
            public string CantLetraSdoLiquiRees { get; set; }

        }

        public partial class CreditoRelacionado
        {
            //Atributos de la clase 
            public Int32 IdCuenta { get; set; }
            public Int32 IdSolicitud { get; set; }
            public decimal capital { get; set; }
            public decimal interes { get; set; }
            public decimal iva { get; set; }
            public decimal sdo_periodo { get; set; }
            public decimal TotalRenovar { get; set; }
        }

        public class AmpliacionEntity
        {
            public int IdSolicitud { get; set; }
            public int IdSolicitudAmplia { get; set; }
            public int IdCuenta { get; set; }
            public DateTime? FechaLiquidar { get; set; }
            public decimal CapitalLiquidar { get; set; }
            public decimal InteresLiquidar { get; set; }
            public decimal IGVLiquidar { get; set; }
            public decimal SeguroLiquidar { get; set; }
            public decimal GATLiquidar { get; set; }
            public decimal TotalLiquidar { get; set; }
            public string ComentariosSistema { get; set; }

            public AmpliacionEntity()
            {
            }
        }
    }
}