﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Creditos
{
    public class ClientesAfiliacion
    {
        public string ClaveId { get; set; }
        public string Nombre { get; set; }
        public string RFC { get; set; }
        public string Telefono { get; set; }
        public string Moneda { get; set; }
        public string Banco { get; set; }
        public string Clabe { get; set; }
        public int Idcliente { get; set; }
        public int Idclabecliente { get; set; }
        public int Idbanco { get; set; }
        public string TipoCuenta { get; set; }
        public int ClienteExistente { get; set; }
    }
}
