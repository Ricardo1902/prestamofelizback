﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Creditos
{
    public class EdoCuenta
    {
        public partial class RazonSocial
        {
            public string razonSocial { get; set; }
            public string Direccion { get; set; }
            public string RFC { get; set; }
            public string TelefonoLocal { get; set; }
            public string TelefonoIntRep { get; set; }
        }

        public partial class InfoCreditos
        {
            public int idCuenta { get; set; }
            public int idSolicitud { get; set; }
			public DateTime FechaIni { get; set; }
			public DateTime FechaFin { get; set; }
			public string Cliente { get; set; }
			public string Direccion { get; set; }
			public string RFC { get; set; }
		    public string Sucursal { get; set; }	
            public decimal capital{ get; set; }	
            public decimal interes{ get; set; }
            public decimal iva_intere{ get; set; }
			public string Moneda{ get; set; }
			public DateTime Fch_Credito{ get; set; }				
			public decimal Cat{ get; set; }
			public decimal ProtFallecimiento{ get; set; }
			public decimal ivaProtFall{ get; set; }
			public decimal apertura	{ get; set; }
			public decimal ivaapertura	{ get; set; }
			public decimal GastosCob	{ get; set; }
			public decimal iva_gascob	{ get; set; }
			public decimal sdo_capita	{ get; set; }
            public decimal sdo_intere{ get; set; }
            public decimal sdo_iva_in{ get; set; }
            public decimal tasa{ get; set; }	
            public decimal tasa_mora{ get; set; }
			public int pagos{ get; set; }
            public string frecuencia { get; set; }
            public int TotalRecPagados { get; set; }
        }

        public partial class totalAbonos
        {
            public int idcuenta { get; set; }
            public decimal totalPagado { get; set; }
            public decimal totalCondonado { get; set; }
            public decimal totalGAT { get; set; }
            public decimal totalSeguro { get; set; }
            public decimal totalIGV { get; set; }
        }

        public partial class ResumenSaldos
        {
            public int idcuenta { get; set; }
            public decimal saldoVigente { get; set; }
            public decimal saldoVencido { get; set; }
            public decimal saldoExigible { get; set; }
            public decimal saldoGasCob { get; set; }
            public decimal saldoIvaGas { get; set; }
            public decimal saldoMora { get; set; }
        }

        public partial class Pagos : TBCobranza
        {
            public string concepto { get; set; }
        }

        public partial class Vencidos : TBRecibos
        {
        }

        public partial class ProxVencimientos : TBRecibos
        {
        }

    }
}
