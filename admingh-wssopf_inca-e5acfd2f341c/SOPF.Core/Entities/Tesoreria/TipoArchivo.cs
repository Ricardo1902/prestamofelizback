﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Tesoreria
{
    public class TipoArchivo
    {
        //Atributos de la clase 
        public int IdTipoArchivo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string EntidadFinanciera { get; set; }

        public class LayoutArchivo
        {
            public int IdTipoArchivo { get; set; }
            public string Parametro { get; set; }
            public int IniciaPosicion { get; set; }
            public int Longuitud { get; set; }
            public string Formato { get; set; }
        }
    }
}
