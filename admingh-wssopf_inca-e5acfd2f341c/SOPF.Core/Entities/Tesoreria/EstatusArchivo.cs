﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Tesoreria
{
    public class EstatusArchivo
    {
        //Atributos de la clase 
        public int IdEstatusArchivo { get; set; }        
        public string Descripcion { get; set; }        
    }
}
