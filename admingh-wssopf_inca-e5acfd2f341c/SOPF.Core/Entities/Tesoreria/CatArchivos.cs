#pragma warning disable 141124
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2014
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	CatArchivos.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-11-24 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Tesoreria
{
    public class CatArchivos
    {
		//Atributos de la clase 
		public Int32 IdArchivo { get; set; }
		public string NombreArchivo { get; set; }
		public string TipoArchivo { get; set; }
		public string Delimitador { get; set; }
		public string FinRegistro { get; set; }
		public string Nombre { get; set; }
		public string Extension { get; set; }
		public string Tipo { get; set; }
		public Boolean IdEstatus { get; set; }
		public Int32 IdBanco { get; set; }
		public Int32 Consecutivo { get; set; }

    }

    public class CatArchivosCon
    {
        public string Consecutivo { get; set; }

    }
}