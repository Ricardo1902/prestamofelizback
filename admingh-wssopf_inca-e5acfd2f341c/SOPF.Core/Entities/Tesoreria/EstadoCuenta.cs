#pragma warning disable 150317
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcia.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	EstadoCuenta.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2015-03-17 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Tesoreria
{
    public class EstadoCuenta
    {
		//Atributos de la clase 
		public Int32 Id { get; set; }
		public DateTime Fecha { get; set; }
		public Int32 Idsolicitud { get; set; }
		public decimal Cargo { get; set; }
		public decimal Abono { get; set; }
		public DateTime FechaReal { get; set; }
		public Int32 IdBanco { get; set; }
		public Boolean Procesado { get; set; }

    }
}