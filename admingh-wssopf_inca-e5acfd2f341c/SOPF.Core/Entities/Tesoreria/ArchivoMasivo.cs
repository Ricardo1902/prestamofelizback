﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Entities.Tesoreria
{
    public class ArchivoMasivo
    {
        // Propiedades escalares
        public int IdArchivo { get; set; }
        public string Nombre { get; set; }        
        public int TotalRegistros { get; set; }
        public decimal TotalMonto { get; set; }
        public DateTime? FechaRegistro { get; set; }

        // Propiedades tipo entidad
        private TipoArchivo _tipoArchivo;
        public TipoArchivo TipoArchivo
        {
            get
            {
                return (this._tipoArchivo != null) ? _tipoArchivo : new TipoArchivo();
            }
            set { this._tipoArchivo = value; }
        }
        private EstatusArchivo _estatusArchivo;
        public EstatusArchivo EstatusArchivo
        {
            get
            {
                return (this._estatusArchivo != null) ? _estatusArchivo : new EstatusArchivo();
            }
            set { this._estatusArchivo = value; }
        }
        private List<ArchivoMasivoDetalle> _detalle;
        public List<ArchivoMasivoDetalle> Detalle
        {
            get
            {
                return (_detalle != null) ? _detalle : new List<ArchivoMasivoDetalle>();
            }
            set { this._detalle = value; }
        }    

        public ArchivoMasivo()
        {
            this.TipoArchivo = new TipoArchivo();
            this.EstatusArchivo = new EstatusArchivo();
            this.Detalle = new List<ArchivoMasivoDetalle>();
        }
    }

    public class ArchivoMasivoDetalle
    {
        //Atributos de la clase 
        public int IdArchivo { get; set; }
        public int Linea { get; set; }        
        public string Descripcion { get; set; }
    }
}
