#pragma warning disable 141124
//=======================================================
//Autogenerado por:  HefestoGenerator
//Versión:   1.0
//Autor:     JCGarcía.
//=======================================================

using System;
using System.Linq;
using System.Text;

#region Copyright Prestamo Feliz – 2015
// Todos los derechos reservados. La repoducción o trasmisión en su
// totalidad o en parte, en cualquier forma o medio electrónico, mecánico
// o similar es prohibida sin autorización expresa y por escrito del
// propietario de este código.
#endregion

#region Informacion General
//
// Archivo:	CatArchivosDetalle.cs
//
// Descripción:
// Clase que hace el mapeado con la Entidad en Base de Datos.
//
// Bitacora de cambios
// Actividad	Fecha		Desarrollador				Descripción del cambio
// Creación		2014-11-24 	Juan Carlos García Obregón	    Creación de la clase
//
#endregion

namespace SOPF.Core.Entities.Tesoreria
{
    public class CatArchivosDetalle
    {
		//Atributos de la clase 
		public Int32 IdRegistro { get; set; }
		public Int32 IdArchivo { get; set; }
		public string Descripcion { get; set; }
		public string Seccion { get; set; }
		public Int32 IdCampo { get; set; }
		public string IdTipoCampo { get; set; }
		public Int32 Longitud { get; set; }
		public Int32 Enteros { get; set; }
		public Int32 Decimales { get; set; }
		public Boolean Incluyepunto { get; set; }
		public string Alineacion { get; set; }
		public string Relleno { get; set; }
		public string TextiFijo { get; set; }
		public string FormatoFecha { get; set; }
		public Boolean IdEstatus { get; set; }
		public Int32 Orden { get; set; }
        

    }
}