﻿using System.Runtime.Serialization;

namespace SOPF.Core.Model.WsPfLleida
{
    [DataContract]
    public class SignatoryVM
    {
        [DataMember(Name = "signatory_id", EmitDefaultValue = false)]
        public long SignatoryId { get; set; }
        [DataMember(Name = "phone", EmitDefaultValue = false)]
        public string Phone { get; set; }
        [DataMember(Name = "email", EmitDefaultValue = false)]
        public string Email { get; set; }
    }
}
