﻿using System.Collections.Generic;

namespace SOPF.Core.Model.WsPfLleida
{
    public class OrdenFirmaVM
    {
        public int Orden { get; set; }
        public int NoFirmasRequeridas { get; set; }
        public List<FirmanteVM> Firmantes { get; set; }
    }
}
