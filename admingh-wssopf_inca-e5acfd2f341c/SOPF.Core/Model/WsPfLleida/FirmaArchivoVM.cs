﻿namespace SOPF.Core.Model.WsPfLleida
{
    public class FirmaArchivoVM
    {
        public string FirmaNumeroFirmante { get; set; }
        public string Pagina { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public float Alto { get; set; }
        public float Ancho { get; set; }
    }
}
