﻿using System.Collections.Generic;

namespace SOPF.Core.Model.WsPfLleida
{
    public class ArchivoVM
    {
        public string NombreArchivo { get; set; }
        public string Contenido { get; set; }
        public string GrupoArchivo { get; set; }
        public List<FirmaArchivoVM> Firmas { get; set; }
    }
}
