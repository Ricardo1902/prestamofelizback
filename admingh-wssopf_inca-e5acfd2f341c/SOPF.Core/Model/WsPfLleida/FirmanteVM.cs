﻿namespace SOPF.Core.Model.WsPfLleida
{
    public class FirmanteVM
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string NumeroFirmante { get; set; }
    }
}
