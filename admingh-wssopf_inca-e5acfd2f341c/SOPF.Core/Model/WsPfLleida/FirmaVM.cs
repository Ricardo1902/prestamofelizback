﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SOPF.Core.Model.WsPfLleida
{
    [DataContract]
    public class FirmaVM
    {
        [DataMember(Name = "contract_id", EmitDefaultValue = false)]
        public string ContractId { get; set; }
        [DataMember(Name = "config_id", EmitDefaultValue = false)]
        public string ConfigId { get; set; }
        [DataMember(Name = "config_name", EmitDefaultValue = false)]
        public string ConfigName { get; set; }
        [DataMember(Name = "signature_id", EmitDefaultValue = false)]
        public string SignatureId { get; set; }
        [DataMember(Name = "signature_start_date", EmitDefaultValue = false)]
        public string SignatureStartDate { get; set; }
        [DataMember(Name = "signature_status", EmitDefaultValue = false)]
        public string SignatureStatus { get; set; }
        [DataMember(Name = "signature_status_date", EmitDefaultValue = false)]
        public string SignatureStatusDate { get; set; }
        [DataMember(Name = "signature_evidence_generated", EmitDefaultValue = false)]
        public string SignatureEvidenceGenerated { get; set; }
        [DataMember(Name = "signatories")]
        public List<SignatoryVM> Signatories { get; set; }
    }
}