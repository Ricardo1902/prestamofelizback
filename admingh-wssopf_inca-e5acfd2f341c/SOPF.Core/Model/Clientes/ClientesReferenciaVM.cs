﻿using SOPF.Core.Poco.Clientes;
using System;

namespace SOPF.Core.Model.Clientes
{
    public class ClientesReferenciaVM
    {
        public int IdReferencia { get; set; }
        public int IdCliente { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public int IdParentesco { get; set; }
        public string Parentesco { get; set; }
        public string Calle { get; set; }
        public string NumeroExterior { get; set; }
        public string NumeroInterior { get; set; }
        public int IdColonia { get; set; }
        public string Lada { get; set; }
        public string Telefono { get; set; }
        public string LadaCelular { get; set; }
        public string TelefonoCelular { get; set; }
        public string TiempoConocerlo { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }

        public ClientesReferencia ToClientesReferencia()
        {
            var tempClientesReferencia = new ClientesReferencia();

            try
            {
                tempClientesReferencia.IdReferencia = this.IdReferencia;
                tempClientesReferencia.IdCliente = this.IdCliente;
                tempClientesReferencia.Nombres = this.Nombres;
                tempClientesReferencia.ApellidoPaterno = this.ApellidoPaterno;
                tempClientesReferencia.ApellidoMaterno = this.ApellidoMaterno;
                tempClientesReferencia.IdParentesco = this.IdParentesco;
                tempClientesReferencia.Parentesco = this.Parentesco;
                tempClientesReferencia.Calle = this.Calle;
                tempClientesReferencia.NumeroExterior = this.NumeroExterior;
                tempClientesReferencia.NumeroInterior = this.NumeroInterior;
                tempClientesReferencia.IdColonia = this.IdColonia;
                tempClientesReferencia.Lada = this.Lada;
                tempClientesReferencia.Telefono = this.Telefono;
                tempClientesReferencia.LadaCelular = this.LadaCelular;
                tempClientesReferencia.TelefonoCelular = this.TelefonoCelular;
                tempClientesReferencia.TiempoConocerlo = this.TiempoConocerlo;
                tempClientesReferencia.FechaRegistro = this.FechaRegistro;
                tempClientesReferencia.Activo = this.Activo;
            }
            catch (Exception)
            {

            }

            return tempClientesReferencia;
        }
    }
}
