﻿namespace SOPF.Core.Model.Clientes
{
    public class DatosClienteVM
    {
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Apellidos { get => $"{ApellidoPaterno} {ApellidoMaterno }"; }
        public string Dni { get; set; }
        public string Estado { get; set; }
        public string Colonia { get; set; }
        public string Calle { get; set; }
        public string NumeroExterior { get; set; }
        public string NumeroInterior { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Direccion { get; set; }
    }
}

