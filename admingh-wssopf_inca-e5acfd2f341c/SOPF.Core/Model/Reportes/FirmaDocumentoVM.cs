﻿namespace SOPF.Core.Model.Reportes
{
    public class DatosFirmanteVM
    {
        public Firmante Firmante { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
    }

    public class PosicionFirmaVM
    {
        public Firmante Firmante { get; set; }
        public int Pagina { get; set; }
        public float PosX { get; set; }
        public float PosY { get; set; }
        public float Alto { get; set; }
        public float Ancho { get; set; }

        public PosicionFirmaVM()
        {
            Alto = 25;
            Ancho = 35;
        }
    }
}
