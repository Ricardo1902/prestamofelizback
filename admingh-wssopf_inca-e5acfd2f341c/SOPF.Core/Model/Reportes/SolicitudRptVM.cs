﻿using SOPF.Core.Entities.Cobranza;
using System;
using System.Collections.Generic;

namespace SOPF.Core.Model.Reportes
{
    public class SolicitudRptVM
    {
        //public string EstatusCredito { get; set; }
        public int IdSolicitud { get; set; }
        public string FechaSolicitud { get; set; }
        public string ClienteNombre { get; set; }
        public string ClienteApePat { get; set; }
        public string ClienteApeMat { get; set; }
        public string FechaNacimiento { get; set; }
        public int Edad { get; set; }
        public string EstadoCivil { get; set; }
        public string Sexo { get; set; }
        public string Promotor { get; set; }
        public string CanalVenta { get; set; }
        public decimal ScoreExperian { get; set; }
        public string Origen { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string UbicacionOfic { get; set; }
        public decimal IngresoBruto { get; set; }
        public decimal Otros_Ingr { get; set; }
        public decimal DescuentosLey { get; set; }
        public string SituacionLaboral { get; set; }
        public string Puesto { get; set; }
        public string PEP { get; set; }
        public string LugNac { get; set; }
        public string Nacion { get; set; }
        public string Distrito { get; set; }
        public string Provincia { get; set; }
        public string Departamento { get; set; }
        public string TipRes { get; set; }
        public string RUC { get; set; }
        public string conyuge { get; set; }
        public string cony_tele { get; set; }
        public string Ref1Nom { get; set; }
        public string Ref1Pare { get; set; }
        public string Ref1Tel { get; set; }
        public string Ref2Nom { get; set; }
        public string Ref2Pare { get; set; }
        public string Ref2Tel { get; set; }
        public Int32 NumDepen { get; set; }
        public string Direccion { get; set; }
        public string RefDom { get; set; }
        public string Empresa { get; set; }
        public string Dependencia { get; set; }
        public string TelefonoLaboral { get; set; }
        public string AnexoLaboral { get; set; }
        public string PuestoLaboral { get; set; }
        public Decimal AntiLaboral { get; set; }
        public Decimal OtrosDsctos { get; set; }
        public Decimal IngresoNeto { get; set; }
        public string ProOIng { get; set; }
        public string ConyTDoc { get; set; }
        public string ConyNDoc { get; set; }
        public Decimal ImpCred { get; set; }
        public Decimal PlaCred { get; set; }
        public Decimal MtoCuota { get; set; }

    }
}
