﻿using System;

namespace SOPF.Core.Model.Reportes
{
    public class CartaPoderVM
    {
        public string NombreCliente { get; set; }
        public string DNI { get; set; }
        public string Direccion { get; set; }
        public string NoCuentaDom { get; set; }
        public DateTime FechaSolicitud { get; set; }

    }
}
