﻿using System;

namespace SOPF.Core.Model.Reportes
{
    public class AcuerdoDomiciliacionVISAVM
    {
        public DateTime FechaSolicitud { get; set; }
        public string NombreCliente { get; set; }
        public string ApPaternoCliente { get; set; }
        public string ApMaternoCliente { get; set; }
        public string DNICliente { get; set; }
        public string TelefonoCliente { get; set; }
        public string CelularCliente { get; set; }
        public string CorreoCliente { get; set; }
        public string IdSolicitud { get; set; }
        public string NumeroTarjetaVisa { get; set; }
        public string MesExpiraTarjetaVisa { get; set; }
        public string AnioExpiraTarjetaVisa { get; set; }
    }
}
