﻿using System;
using System.Diagnostics;

namespace SOPF.Core.Model.Reportes
{
    public class AcuerdoPagareVM
    {
        public int IdSolicitud { get; set; }
        public string Cliente { get; set; }
        public int Folio { get; set; }
        public decimal Importe { get; set; }
        public string DNI { get; set; }
        public string Domicilio { get; set; }
        public string EstadoCivil { get; set; }
    }
}
