﻿using System;

namespace SOPF.Core.Model.Reportes
{
    public class AcuerdoDomiciliacionClienteVM
    {
        public DateTime FechaSolicitud { get; set; }
        public string CuentaDesembolso { get; set; }
        public string BCEntidad { get; set; }
        public string BCOficina { get; set; }
        public string BCCuenta { get; set; }
        public string BCDC { get; set; }
        public string NombreCliente { get; set; }
        public string ApPaternoCliente { get; set; }
        public string ApMaternoCliente { get; set; }
        public string DireccionCliente { get; set; }
        public string DNICliente { get; set; }
        public string CelularCliente { get; set; }
        public string EmpresaCliente { get; set; }
        public decimal IdSolicitud { get; set; }
        public int CantidadRecibos { get; set; }
        public decimal ImporteMaximoCuota { get; set; }
    }
}
