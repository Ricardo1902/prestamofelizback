﻿using SOPF.Core.Entities.Cobranza;
using System;
using System.Collections.Generic;

namespace SOPF.Core.Model.Reportes
{
    public class EstadoCuentaVM
    {
        public string EstatusCredito { get; set; }
        public int IdSolicitud { get; set; }
        public string Cliente { get; set; }
        public string FechaCredito { get; set; }
        public decimal MontoUsoCanal { get; set; }
        public decimal MontoGAT { get; set; }
        public decimal Cuota { get; set; }
        public decimal Capital { get; set; }
        public string Producto { get; set; }
        public decimal SaldoVencido { get; set; }
        public decimal CostoTotalCredito { get; set; }
        public string TipoCredito { get; set; }
        public decimal MontoComisionUsoCanal { get; set; }
        public List<BalanceCuenta.Recibos> Recibos { get; set; }
    }
}
