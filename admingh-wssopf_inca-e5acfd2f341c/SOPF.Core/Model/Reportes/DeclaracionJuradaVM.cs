﻿namespace SOPF.Core.Model.Reportes
{
    public class DeclaracionJuradaVM
    {
        public int IdSolicitud { get; set; }
        public string NombreCliente { get; set; }
        public string DNI { get; set; }
    }
}
