﻿using System;

namespace SOPF.Core.Model.Reportes
{
    public class PagareVM
    {
        public int Folio { get; set; }
        public decimal Importe { get; set; }
        public DateTime? Vencimiento { get; set; }
    }
}
