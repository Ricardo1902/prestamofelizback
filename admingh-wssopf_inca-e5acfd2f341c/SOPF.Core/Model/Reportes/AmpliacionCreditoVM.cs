﻿namespace SOPF.Core.Model.Reportes
{
    public class AmpliacionCreditoVM
    {
        public int IdSolicitud { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string NumeroDocumento { get; set; }
        public string CorreoElectronico { get; set; }
        public decimal MontoCredito { get; set; }
        public decimal TasaInteres { get; set; }
        public decimal TotalInteres { get; set; }
        public decimal TasaCostoEfectivoAnual { get; set; }
        public decimal Erogacion { get; set; }
        public decimal TotalRecibos { get; set; }
        public decimal MontoLiquidacion { get; set; }
        public decimal MontoDesembolso { get; set; }
        public decimal MontoAnterior { get; set; } //articulos idsolicitud ampliacionv2
        public decimal MontoPrestamoConAmpliacion { get; set; }//articulos idsolicitudampliacion ampliacionv2
    }
}
