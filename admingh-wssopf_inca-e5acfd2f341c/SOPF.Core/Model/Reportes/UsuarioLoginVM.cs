﻿namespace SOPF.Core.Model.Reportes
{
    public class UsuarioLoginVM
    {
        public int Exito { get; set; }
        public int Id_Usuario { get; set; }
        public int compania_Id { get; set; }
        public int Perfil_Id { get; set; }
        public string Nombre { get; set; }
        public int ID_Sistema { get; set; }
        public string Correo { get; set; }
        public int Inicializar { get; set; }
    }
}