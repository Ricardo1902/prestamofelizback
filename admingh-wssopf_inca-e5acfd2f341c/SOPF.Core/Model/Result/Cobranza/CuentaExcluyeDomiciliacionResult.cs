﻿namespace SOPF.Core.Model.Result.Cobranza
{
    public class CuentaExcluyeDomiciliacionResult
    {
        public int IdSolicitud { get; set; }
        public string Mensaje { get; set; }
    }
}
