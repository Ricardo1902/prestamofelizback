﻿using System;

namespace SOPF.Core.Model.Sistema
{
    public class NotificacionSolNotVM
    {
        public int IdNotificacion { get; set; }
        public decimal? IdSolicitud { get; set; }
        public string Titulo { get; set; }
        public string CorreoElectronico { get; set; }
        public string NumeroTelefono { get; set; }
        public string NombreCliente { get; set; }
        public decimal? CapitalSolicitado { get; set; }
        public decimal? Erogacion { get; set; }
        public int? TotalRecibos { get; set; }
        public int Intentos { get; set; }
        public int MaxIntentos { get; set; }
        public string Url { get; set; }
        public string Clave { get; set; }
        public DateTime? FechaHoraCitacion { get; set; }
        public decimal? DescuentoDeuda { get; set; }
        public DateTime? FechaVigencia { get; set; }
        public int IdDestino { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public bool Courier { get; set; }
        public bool Actualizar { get; set; }
    }
}
