﻿using System;

namespace SOPF.Core.Model.Sistema
{
    public class DevolucionLlamadaRequest : Peticion
    {
        public int? IdGestion { get; set; }
        public string CorreoElectronico { get; set; }
        public DateTime FechaEnviar { get; set; }
    }
}
