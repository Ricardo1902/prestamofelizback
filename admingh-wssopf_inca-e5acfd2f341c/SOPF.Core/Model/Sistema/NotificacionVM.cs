﻿//Creado por: HefestoGenerator - SAVIED
using SOPF.Core.Model.Gestiones;
using System;

namespace SOPF.Core.Model.Sistema
{
    public class NotificacionVM
    {
        public int IdNotificacion { get; set; }
        public int? IdCliente { get; set; }
        public decimal? IdSolicitud { get; set; }
        public string Titulo { get; set; }
        public string CorreoElectronico { get; set; }
        public string NumeroTelefono { get; set; }
        public string NombreCliente { get; set; }
        public decimal? CapitalSolicitado { get; set; }
        public decimal? Erogacion { get; set; }
        public int? TotalRecibos { get; set; }
        public int Intentos { get; set; }
        public int MaxIntentos { get; set; }
        public string Url { get; set; }
        public DateTime? FechaEnviar { get; set; }
        public int IdGestion { get; set; }
        public string CorreoCopia { get; set; }

        public DevolucionLlamadaVM DevolucionLlamada { get; set; }
        public GestionNotificacionVM GestionNotificacion { get; set; }
    }
}
