﻿using System.Collections.Generic;
using System.Net.Mail;

namespace SOPF.Core.Model.Sistema
{
    public class ContenidoCorreoVM
    {
        public string ContenidoHtml { get; set; }
        public List<AlternateView> ContenidoAlterno { get; set; }
        public List<Adjuntos> Adjuntos { get; set; }

        public ContenidoCorreoVM()
        {
            Adjuntos = new List<Adjuntos>();
            ContenidoAlterno = new List<AlternateView>();
        }
    }
}