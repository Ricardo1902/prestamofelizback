﻿namespace SOPF.Core.Model.Sistema
{
    public class TipoNotificacionVM
    {
        public int IdTipoNotificacion { get; set; }
        public string Clave { get; set; }
        public bool EnvioProgramado { get; set; }
        public int MinutosRecurrencia { get; set; }
        public int? HorasRecurrencia { get; set; }
    }
}
