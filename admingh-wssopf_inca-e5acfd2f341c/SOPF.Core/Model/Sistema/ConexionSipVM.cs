﻿namespace SOPF.Core.Model.Sistema
{
    public class ConexionSipVM
    {
        public int IdConexionSip { get; set; }
        public string Servidor { get; set; }
        public int? Puerto { get; set; }
        public string Password { get; set; }
        public ConexionSdkVM ConexionSdk { get; set; }
    }
}
