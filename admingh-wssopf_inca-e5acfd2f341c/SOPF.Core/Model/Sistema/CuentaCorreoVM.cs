﻿using System;

namespace SOPF.Core.Model.Sistema
{
    public class CuentaCorreoVM
    {
        public int IdCuentaCorreo { get; set; }
        public string Servidor { get; set; }
        public int Puerto { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public bool UsaSSL { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
