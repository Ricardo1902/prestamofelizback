﻿using Newtonsoft.Json;

namespace SOPF.Core.Model.Lleida.DosFA
{
    public class SmsVM
    {
        [JsonProperty("numDestinatario")]
        public string NumDestinatario { get; set; }
        [JsonProperty("mensaje")]
        public string Mensaje { get; set; }
    }
}
