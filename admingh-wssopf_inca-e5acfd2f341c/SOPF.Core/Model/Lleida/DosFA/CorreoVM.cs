﻿using Newtonsoft.Json;

namespace SOPF.Core.Model.Lleida.DosFA
{
    public class CorreoVM
    {
        [JsonProperty("remitente")]
        public string Remitente { get; set; }
        [JsonProperty("destinatario")]
        public string Destinatario { get; set; }
        [JsonProperty("nombreDestinatario", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string NombreDestinatario { get; set; }
        [JsonProperty("asunto")]
        public string Asunto { get; set; }
        [JsonProperty("mensaje")]
        public string Mensaje { get; set; }
    }
}
