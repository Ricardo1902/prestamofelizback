﻿using System.Runtime.Serialization;

namespace SOPF.Core.Model.Lleida
{
    [DataContract]
    public class ConfiguracionStatusVM
    {
        [DataMember(Name = "config_id", EmitDefaultValue = false)]
        public int ConfigId { get; set; }
        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }
        [DataMember(Name = "status", EmitDefaultValue = false)]
        public string Status { get; set; }
        [DataMember(Name = "Activo", EmitDefaultValue = false)]
        public bool Activo { get; set; }
    }
}
