﻿namespace SOPF.Core.Model.Lleida
{
    public class DocumentoFirmaVM
    {
        public string NombreDocumento { get; set; }
        public string Contenido { get; set; }
        public string Url { get; set; }
        public int IdDocumento { get; set; }
    }
}
