﻿using SOPF.Core.Lleida;
using System;
using System.Xml.Serialization;

namespace SOPF.Core.Model.Lleida
{
    [Serializable()]
    [XmlRoot("ArchivoDigital")]
    public class ArchivoDigitalVM
    {
        public string Status { get; set; }
        public long CallId { get; set; }
        public string ExternalId { get; set; }
        public string DataFormat { get; set; }
        public string Data { get; set; }
        public EkycMediaType MediaType { get; set; }
    }
}
