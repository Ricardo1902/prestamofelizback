﻿namespace SOPF.Core.Model.Dbo
{
    public class EstatusVM
    {
        public int IdEstatus { get; set; }
        public string Estatus { get; set; }
        public string Descripcion { get; set; }
    }
}
