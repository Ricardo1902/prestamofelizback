﻿namespace SOPF.Core.Model.Dbo
{
    public class TipoEstatusVM
    {
        public int IdTipoEstatus { get; set; }
        public string Tipo { get; set; }
    }
}
