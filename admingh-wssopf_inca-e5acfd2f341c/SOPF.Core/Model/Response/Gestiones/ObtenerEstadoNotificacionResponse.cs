﻿namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerEstadoNotificacionResponse : Respuesta
    {
        public ObtenerEstadoNotificacionResultado Resultado { get; set; }
        public ObtenerEstadoNotificacionResponse()
        {
            Resultado = new ObtenerEstadoNotificacionResultado();
        }
    }

    public class ObtenerEstadoNotificacionResultado
    {
        public Request.Gestiones.Sel_NotificacionResult Notificacion { get; set; }
    }
}
