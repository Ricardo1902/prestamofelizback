﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class GestoresResponse : Respuesta
    {
        public List<GestorVM> Gestores { get; set; }
    }
}
