﻿namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerProductoReestructuraResponse : Respuesta
    {
        public ObtenerProductoReestructuraResultado Resultado { get; set; }
        public ObtenerProductoReestructuraResponse()
        {
            Resultado = new ObtenerProductoReestructuraResultado();
        }
    }

    public class ObtenerProductoReestructuraResultado
    {
        public int IdProducto { get; set; }
        public decimal Tasa { get; set; }
    }
}