﻿namespace SOPF.Core.Model.Response.Gestiones
{
    public class InsertarGestionConResultadoResponse : Respuesta
    {
        public int IdGestion { get; set; }
    }
}
