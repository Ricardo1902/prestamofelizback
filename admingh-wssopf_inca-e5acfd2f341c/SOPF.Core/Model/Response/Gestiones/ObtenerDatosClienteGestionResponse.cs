﻿namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerDatosClienteGestionResponse : Respuesta
    {
        public ObtenerDatosClienteGestionResultado Resultado { get; set; }
        public ObtenerDatosClienteGestionResponse()
        {
            Resultado = new ObtenerDatosClienteGestionResultado();
        }
    }

    public class ObtenerDatosClienteGestionResultado
    {
        public Request.Gestiones.Sel_ObtenerDatosClienteGestionResult Datos { get; set; }
    }
}
