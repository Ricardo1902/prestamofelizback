﻿using System;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class DatosAvisoPagoResponse : Respuesta
    {
        public DatosAvisoPagoResultado Resultado { get; set; }
        public DatosAvisoPagoResponse()
        {
            Resultado = new DatosAvisoPagoResultado();
        }
    }

    public class DatosAvisoPagoResultado
    {
        public DateTime FechaRegistro { get; set; }
        public string NombreCliente { get; set; }
        public string Direccion { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public decimal TotalDeuda { get; set; }
        public int CuotasVencidas { get; set; }
        public int IdCliente { get; set; }

        public DatosAvisoPagoResultado()
        {
        }
    }
}