﻿using SOPF.Core.Poco.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerDestinosResponse : Respuesta
    {
        public ObtenerDestinosResultado Resultado { get; set; }
        public ObtenerDestinosResponse()
        {
            Resultado = new ObtenerDestinosResultado();
        }
    }

    public class ObtenerDestinosResultado
    {
        public List<TB_CATDestinoNotificacion> Destinos { get; set; }
        public ObtenerDestinosResultado()
        {
            Destinos = new List<TB_CATDestinoNotificacion>();
        }
    }
}
