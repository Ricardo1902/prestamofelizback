﻿using SOPF.Core.Poco.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerTipoContactosResponse : Respuesta
    {
        public ObtenerTipoContactosResultado Resultado { get; set; }
        public ObtenerTipoContactosResponse()
        {
            Resultado = new ObtenerTipoContactosResultado();
        }
    }

    public class ObtenerTipoContactosResultado
    {
        public List<CatTipoContaco> CatTipoContactos { get; set; }
        public ObtenerTipoContactosResultado()
        {
            CatTipoContactos = new List<CatTipoContaco>();
        }
    }
}
