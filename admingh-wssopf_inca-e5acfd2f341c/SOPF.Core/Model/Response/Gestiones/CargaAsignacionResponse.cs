﻿using SOPF.Core.Model.Gestiones;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class CargaAsignacionResponse : Respuesta
    {
        public CargaAsignacionVM CargaAsignacion { get; set; }
    }
}
