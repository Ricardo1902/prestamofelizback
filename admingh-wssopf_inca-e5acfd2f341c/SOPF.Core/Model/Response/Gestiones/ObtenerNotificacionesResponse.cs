﻿using SOPF.Core.Poco.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerNotificacionesResponse : Respuesta
    {
        public ObtenerNotificacionesResultado Resultado { get; set; }
        public ObtenerNotificacionesResponse()
        {
            Resultado = new ObtenerNotificacionesResultado();
        }
    }

    public class ObtenerNotificacionesResultado
    {
        public List<TB_CATNotificaciones> Notificaciones { get; set; }
        public ObtenerNotificacionesResultado()
        {
            Notificaciones = new List<TB_CATNotificaciones>();
        }
    }
}
