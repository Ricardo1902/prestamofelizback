﻿namespace SOPF.Core.Model.Response
{
    public class ImpresionNotificacionResponse : Respuesta
    {
        public ImpresionNotificacionResultado Resultado { get; set; }
        public ImpresionNotificacionResponse()
        {
            Resultado = new ImpresionNotificacionResultado();
        }
    }

    public class ImpresionNotificacionResultado
    {
        public byte[] ContenidoArchivo { get; set; }

        public ImpresionNotificacionResultado()
        {
        }
    }
}
