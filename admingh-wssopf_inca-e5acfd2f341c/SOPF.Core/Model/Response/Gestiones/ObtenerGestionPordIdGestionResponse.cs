﻿namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerGestionPordIdGestionResponse : Respuesta
    {
        public ObtenerGestionPordIdGestionResultado Resultado { get; set; }
        public ObtenerGestionPordIdGestionResponse()
        {
            Resultado = new ObtenerGestionPordIdGestionResultado();
        }
    }

    public class ObtenerGestionPordIdGestionResultado
    {
        public Request.Gestiones.Sel_GestionesResult Gestion { get; set; }
    }
}
