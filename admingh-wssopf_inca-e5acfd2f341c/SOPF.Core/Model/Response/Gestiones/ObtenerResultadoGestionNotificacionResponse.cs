﻿using SOPF.Core.Poco.dbo;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerResultadoGestionNotificacionResponse : Respuesta
    {
        public ObtenerResultadoGestionNotificacionResultado Resultado { get; set; }
        public ObtenerResultadoGestionNotificacionResponse()
        {
            Resultado = new ObtenerResultadoGestionNotificacionResultado();
        }
    }

    public class ObtenerResultadoGestionNotificacionResultado
    {
        public TB_CATParametro Parametro { get; set; }
        public ObtenerResultadoGestionNotificacionResultado()
        {
            Parametro = new TB_CATParametro();
        }
    }
}
