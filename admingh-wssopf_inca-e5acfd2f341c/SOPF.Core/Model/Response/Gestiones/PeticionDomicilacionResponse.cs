﻿using SOPF.Core.Model.Response.Cobranza;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class PeticionDomicilacionResponse : Respuesta
    {
        public GenerarArchivoDomiciliacionResultado ArchivoDomiciliacion { get; set; }
    }
}
