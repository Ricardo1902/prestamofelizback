﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class TelefonosTipoContactoResponse : Respuesta
    {
        public List<ContactoTelefonicoVM> Contactos { get; set; }
    }
}
