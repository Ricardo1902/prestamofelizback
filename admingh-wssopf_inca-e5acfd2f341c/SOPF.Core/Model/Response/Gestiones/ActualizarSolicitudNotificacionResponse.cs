﻿namespace SOPF.Core.Model.Response.Gestiones
{
    public class ActualizarSolicitudNotificacionResponse
    {
        public ActualizarSolicitudNotificacionResultado Resultado { get; set; }
        public ActualizarSolicitudNotificacionResponse()
        {
            Resultado = new ActualizarSolicitudNotificacionResultado();
        }
    }
    public class ActualizarSolicitudNotificacionResultado
    {

    }
}
