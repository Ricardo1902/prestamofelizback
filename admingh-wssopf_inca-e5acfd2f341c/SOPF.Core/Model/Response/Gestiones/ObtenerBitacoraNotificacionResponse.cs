﻿using SOPF.Core.Model.Request.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerBitacoraNotificacionResponse : Respuesta
    {
        public ObtenerBitacoraNotificacionResultado Resultado { get; set; }
        public ObtenerBitacoraNotificacionResponse()
        {
            Resultado = new ObtenerBitacoraNotificacionResultado();
        }
    }

    public class ObtenerBitacoraNotificacionResultado
    {
        public List<Sel_NotificacionBitacoraResult> Bitacora { get; set; }
    }
}
