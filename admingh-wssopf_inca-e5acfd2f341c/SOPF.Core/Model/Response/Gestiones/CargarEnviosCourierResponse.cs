﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class CargarEnviosCourierResponse : Respuesta
    {
        public CargarEnviosCourierResultado Resultado { get; set; }
        public CargarEnviosCourierResponse()
        {
            Resultado = new CargarEnviosCourierResultado();
        }
    }
    public class CargarEnviosCourierResultado
    {
        public List<CargarEnviosCourierVM> Envios { get; set; }
        public CargarEnviosCourierResultado()
        {
            Envios = new List<CargarEnviosCourierVM>();
        }
    }
}
