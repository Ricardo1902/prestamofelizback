﻿//Creado por: HefestoGenerator - SAVIED
using System;
using System.Xml.Linq;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class GestionDomiciliacionConRequest
    {
        public int IdUsuario { get; set; }
        public int IdTipoGestion { get; set; }
        public int? IdEstatus { get; set; }
        public DateTime? FechaDomciliar { get; set; }
        public int? IdBanco { get; set; }
        public XElement GestionDomiciliacion { get; set; }
        public int? Pagina { get; set; }
        public int? RegistrosPagina { get; set; }
    }
}