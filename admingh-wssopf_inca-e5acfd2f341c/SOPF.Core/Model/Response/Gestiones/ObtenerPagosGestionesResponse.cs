﻿using SOPF.Core.Model.Cobranza;
using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class ObtenerPagosGestionesResponse : Respuesta
    {
        public ObtenerPagosGestionesResultado Resultado = new ObtenerPagosGestionesResultado();
        public ObtenerPagosGestionesResponse()
        {
            Resultado = new ObtenerPagosGestionesResultado();
        }
    }

    public class ObtenerPagosGestionesResultado
    {
        public List<PagoGestionesVM> Pagos { get; set; }

        public ObtenerPagosGestionesResultado()
        {
            Pagos = new List<PagoGestionesVM>();
        }
    }
}
