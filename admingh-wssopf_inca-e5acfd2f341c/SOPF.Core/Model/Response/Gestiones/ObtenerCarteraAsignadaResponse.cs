﻿using SOPF.Core.Model.Request.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerCarteraAsignadaResponse : Respuesta
    {
        public ObtenerCarteraAsignadaResultado Resultado { get; set; }
        public ObtenerCarteraAsignadaResponse()
        {
            Resultado = new ObtenerCarteraAsignadaResultado();
        }
    }
    public class ObtenerCarteraAsignadaResultado
    {
        public List<Sel_ObtenerCarteraAsignadaResult> Cartera { get; set; }
        public ObtenerCarteraAsignadaTotales Totales { get; set; }

        public ObtenerCarteraAsignadaResultado()
        {
            Totales = new ObtenerCarteraAsignadaTotales();
        }
    }
    public class ObtenerCarteraAsignadaTotales
    {
        public int RegistrosPagina { get; set; }
        public int Pagina { get; set; }
        public int AsignacionesTotales { get; set; }
        public int AsignacionesTerminadas { get; set; }
        public int AsignacionesPendientes { get; set; }
        public int CuentasTotales { get; set; }
        public int RegistrosTotales { get; set; }
    }
}
