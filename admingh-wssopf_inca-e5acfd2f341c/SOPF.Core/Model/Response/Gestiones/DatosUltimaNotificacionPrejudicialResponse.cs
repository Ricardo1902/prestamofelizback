﻿using System;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class DatosUltimaNotificacionPrejudicialResponse : Respuesta
    {
        public DatosUltimaNotificacionPrejudicialResultado Resultado { get; set; }
        public DatosUltimaNotificacionPrejudicialResponse()
        {
            Resultado = new DatosUltimaNotificacionPrejudicialResultado();
        }
    }

    public class DatosUltimaNotificacionPrejudicialResultado
    {
        public DateTime FechaRegistro { get; set; }
        public string NombreCliente { get; set; }
        public string Direccion { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public decimal TotalDeuda { get; set; }
        public int CuotasVencidas { get; set; }
        public int IdCliente { get; set; }

        public DatosUltimaNotificacionPrejudicialResultado()
        {
        }
    }
}