﻿using System;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class DatosPromocionReduccionMoraResponse : Respuesta
    {
        public DatosPromocionReduccionMoraResultado Resultado { get; set; }
        public DatosPromocionReduccionMoraResponse()
        {
            Resultado = new DatosPromocionReduccionMoraResultado();
        }
    }

    public class DatosPromocionReduccionMoraResultado
    {
        public DateTime FechaRegistro { get; set; }
        public string NombreCliente { get; set; }
        public string Direccion { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public decimal TotalDeuda { get; set; }
        public int CuotasVencidas { get; set; }
        public int IdCliente { get; set; }
        public decimal? DescuentoDeuda { get; set; }
        public DateTime FechaVigencia { get; set; }

        public DatosPromocionReduccionMoraResultado()
        {
        }
    }
}