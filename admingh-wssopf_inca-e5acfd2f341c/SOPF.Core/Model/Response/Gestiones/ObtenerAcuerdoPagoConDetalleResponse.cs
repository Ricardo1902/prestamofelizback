﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerAcuerdoPagoConDetalleResponse : Respuesta
    {
        public AcuerdoPagoVM AcuerdoPago { get; set; }
        public List<AcuerdoPagoDetalleVM> Detalle { get; set; }
    }
}
