﻿using SOPF.Core.Poco.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerTipoGestorContactosResponse : Respuesta
    {
        public ObtenerTipoGestorContactosResultado Resultado { get; set; }
        public ObtenerTipoGestorContactosResponse()
        {
            Resultado = new ObtenerTipoGestorContactosResultado();
        }
    }

    public class ObtenerTipoGestorContactosResultado
    {
        public List<TipoGestorContacto> TipoGestorContactos { get; set; }
        public ObtenerTipoGestorContactosResultado()
        {
            TipoGestorContactos = new List<TipoGestorContacto>();
        }
    }
}
