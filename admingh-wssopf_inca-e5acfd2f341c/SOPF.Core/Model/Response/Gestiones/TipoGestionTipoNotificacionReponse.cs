﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class TipoGestionTipoNotificacionReponse: Respuesta
    {
        public List<TipoGestionTipoNotificacionVM> TipoGestionTipoNotificacion { get; set; }
    }
}
