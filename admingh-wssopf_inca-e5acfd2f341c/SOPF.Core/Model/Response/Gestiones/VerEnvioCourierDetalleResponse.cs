﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class VerEnvioCourierDetalleResponse : Respuesta
    {
        public VerEnvioCourierDetalleResultado Resultado { get; set; }
        public VerEnvioCourierDetalleResponse()
        {
            Resultado = new VerEnvioCourierDetalleResultado();
        }
    }
    public class VerEnvioCourierDetalleResultado
    {
        public List<VerEnvioCourierSolicitudNotificacionVM> Envios { get; set; }
        public VerEnvioCourierDetalleResultado()
        {
            Envios = new List<VerEnvioCourierSolicitudNotificacionVM>();
        }
    }
}
