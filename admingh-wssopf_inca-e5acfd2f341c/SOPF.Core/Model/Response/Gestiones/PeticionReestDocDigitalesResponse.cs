﻿using SOPF.Core.Model.Solicitudes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class PeticionReestDocDigitalesResponse : Respuesta
    {
        public List<PeticionReestDocDigitalVM> PeticionReestDocs { get; set; }
    }
}
