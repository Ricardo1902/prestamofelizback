﻿using SOPF.Core.Model.Gestiones;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ConexionTelefonicaResponse : Respuesta
    {
        public int IdExtensionSesion { get; set; }
        public ConexionTelefonicaVM ConexionTelefonica { get; set; }
    }
}
