﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class CargaAsignacionDetallesResponse : Respuesta
    {
        public List<CargaAsignacionDetalleVM> Asignaciones { get; set; }
    }
}
