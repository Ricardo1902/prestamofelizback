﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ResultadosGestionResponse : Respuesta
    {
        public List<ResultadoGestionVM> ResultadosGestion { get; set; }
    }
}
