﻿using System;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class DatosAvisoCobranzaResponse : Respuesta
    {
        public DatosAvisoCobranzaResultado Resultado { get; set; }
        public DatosAvisoCobranzaResponse()
        {
            Resultado = new DatosAvisoCobranzaResultado();
        }
    }

    public class DatosAvisoCobranzaResultado
    {
        public DateTime FechaRegistro { get; set; }
        public string NombreCliente { get; set; }
        public string Direccion { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public decimal TotalDeuda { get; set; }
        public int CuotasVencidas { get; set; }
        public int IdCliente { get; set; }

        public DatosAvisoCobranzaResultado()
        {
        }
    }
}