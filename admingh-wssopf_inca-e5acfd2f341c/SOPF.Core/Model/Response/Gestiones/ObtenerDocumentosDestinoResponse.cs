﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerDocumentosDestinoResponse: Respuesta
    {
        public ObtenerDocumentosDestinoResultado Resultado { get; set; }
        public ObtenerDocumentosDestinoResponse()
        {
            Resultado = new ObtenerDocumentosDestinoResultado();
        }
    }
    public class ObtenerDocumentosDestinoResultado
    {
        public List<DocumentoDestinoVM> DocumentosDestino { get; set; }
        public ObtenerDocumentosDestinoResultado()
        {
            DocumentosDestino = new List<DocumentoDestinoVM>();
        }
    }
}
