﻿using SOPF.Core.Model.Cobranza;
using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class TipoGestionDomiciliacionResponse : Respuesta
    {
        public List<TipoGestionVM> TipoGestionDomiciliacion { get; set; }
    }
}
