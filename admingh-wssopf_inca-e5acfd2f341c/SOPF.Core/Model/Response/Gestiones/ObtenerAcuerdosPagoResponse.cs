﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerAcuerdosPagoResponse: Respuesta
    {
        public List<AcuerdoPagoVM> Acuerdos { get; set; }
    }
}
