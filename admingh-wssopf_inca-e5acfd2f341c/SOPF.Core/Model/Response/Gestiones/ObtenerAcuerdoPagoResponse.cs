﻿using SOPF.Core.Model.Gestiones;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerAcuerdoPagoResponse: Respuesta
    {
        public AcuerdoPagoVM Acuerdo { get; set; }
    }
}
