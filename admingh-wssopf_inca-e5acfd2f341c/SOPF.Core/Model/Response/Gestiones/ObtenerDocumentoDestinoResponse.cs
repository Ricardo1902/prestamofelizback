﻿using SOPF.Core.Model.Gestiones;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerDocumentoDestinoResponse: Respuesta
    {
        public ObtenerDocumentoDestinoResultado Resultado { get; set; }
        public ObtenerDocumentoDestinoResponse()
        {
            Resultado = new ObtenerDocumentoDestinoResultado();
        }
    }
    public class ObtenerDocumentoDestinoResultado
    {
        public DocumentoDestinoVM DocumentoDestino { get; set; }
        public ObtenerDocumentoDestinoResultado()
        {
            DocumentoDestino = new DocumentoDestinoVM();
        }
    }
}
