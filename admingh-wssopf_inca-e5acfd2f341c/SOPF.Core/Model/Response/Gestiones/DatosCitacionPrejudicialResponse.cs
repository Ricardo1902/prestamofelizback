﻿using System;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class DatosCitacionPrejudicialResponse : Respuesta
    {
        public DatosCitacionPrejudicialResultado Resultado { get; set; }
        public DatosCitacionPrejudicialResponse()
        {
            Resultado = new DatosCitacionPrejudicialResultado();
        }
    }

    public class DatosCitacionPrejudicialResultado
    {
        public DateTime FechaRegistro { get; set; }
        public string NombreCliente { get; set; }
        public string Direccion { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public decimal TotalDeuda { get; set; }
        public int CuotasVencidas { get; set; }
        public int IdCliente { get; set; }
        public DateTime FechaHoraCitacion { get; set; }

        public DatosCitacionPrejudicialResultado()
        {
        }
    }
}