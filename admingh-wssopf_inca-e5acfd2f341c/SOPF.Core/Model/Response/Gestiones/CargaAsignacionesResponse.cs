﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class CargaAsignacionesResponse : Respuesta
    {
        public List<CargaAsignacionVM> CargaAsignaciones { get; set; }
    }
}
