﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class GestionDomiciacionesResponse : Respuesta
    {
        public List<GestionDomiciliacionVM> GestionDomiciliaciones { get; set; }
    }
}
