﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerSolicitudNotificacionesCourierResponse : Respuesta
    {
        public ObtenerSolicitudNotificacionesCourierResultado Resultado { get; set; }
        public ObtenerSolicitudNotificacionesCourierResponse()
        {
            Resultado = new ObtenerSolicitudNotificacionesCourierResultado();
        }
    }
    public class ObtenerSolicitudNotificacionesCourierResultado
    {
        public List<ObtenerSolicitudNotificacionesCourierVM> Notificaciones { get; set; }
        public ObtenerSolicitudNotificacionesCourierResultado()
        {
            Notificaciones = new List<ObtenerSolicitudNotificacionesCourierVM>();
        }
    }
}
