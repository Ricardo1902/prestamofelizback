﻿using System;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ConfiguracionNotificacionResponse : Respuesta
    {
        public DateTime? HoraMinAgenda { get; set; }
        public DateTime? HoraMaxAgenda { get; set; }
    }
}
