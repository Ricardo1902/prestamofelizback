﻿using SOPF.Core.Model.Gestiones;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class DevolucionLlamadaGestionResponse : Respuesta
    {
        public DevolucionLlamadaVM DevolucionLlamada { get; set; }
    }
}
