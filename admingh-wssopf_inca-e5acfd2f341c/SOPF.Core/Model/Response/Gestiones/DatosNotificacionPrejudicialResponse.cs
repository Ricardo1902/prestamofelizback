﻿using System;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class DatosNotificacionPrejudicialResponse : Respuesta
    {
        public DatosNotificacionPrejudicialResultado Resultado { get; set; }
        public DatosNotificacionPrejudicialResponse()
        {
            Resultado = new DatosNotificacionPrejudicialResultado();
        }
    }

    public class DatosNotificacionPrejudicialResultado
    {
        public DateTime FechaRegistro { get; set; }
        public string NombreCliente { get; set; }
        public string Direccion { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public decimal TotalDeuda { get; set; }
        public int CuotasVencidas { get; set; }
        public int IdCliente { get; set; }

        public DatosNotificacionPrejudicialResultado()
        {
        }
    }
}