﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class CargarAsignacionResponse : Respuesta
    {
        public CargaAsignacionVM CargaAsignacion { get; set; }
    }
}