﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerAcuerdoPagoDetalleResponse: Respuesta
    {
        public List<AcuerdoPagoDetalleVM> Datos { get; set; }
    }
}
