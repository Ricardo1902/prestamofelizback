﻿using SOPF.Core.Model.Gestiones;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class AltaAcuerdoPagoResponse: Respuesta
    {
        public AcuerdoPagoVM AcuerdoPago { get; set; }
    }
}
