﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class TipoGestionPromesaResponse: Respuesta
    {
        public List<TipoGestionVM> Tipos { get; set; }
    }
}
