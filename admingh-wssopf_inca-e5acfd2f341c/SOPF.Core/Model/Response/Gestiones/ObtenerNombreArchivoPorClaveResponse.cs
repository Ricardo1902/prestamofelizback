﻿namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerNombreArchivoPorClaveResponse : Respuesta
    {
        public ObtenerNombreArchivoPorClaveResultado Resultado { get; set; }
        public ObtenerNombreArchivoPorClaveResponse()
        {
            Resultado = new ObtenerNombreArchivoPorClaveResultado();
        }
    }

    public class ObtenerNombreArchivoPorClaveResultado
    {
        public Request.Gestiones.Sel_NotificacionResult Notificacion { get; set; }
    }
}
