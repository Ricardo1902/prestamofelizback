﻿using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerAsignacionAccionResponse : Respuesta
    {
        public ObtenerAsignacionAccionResultado Resultado { get; set; }
        public ObtenerAsignacionAccionResponse()
        {
            Resultado = new ObtenerAsignacionAccionResultado();
        }
    }

    public class ObtenerAsignacionAccionResultado
    {
        public List<ObtenerAsignacionAccionResult> AsignacionAccion { get; set; }
        public ObtenerAsignacionAccionResultado()
        {
            AsignacionAccion = new List<ObtenerAsignacionAccionResult>();
        }
    }

    public class ObtenerAsignacionAccionResult
    {
        public int IdAsignacion { get; set; }
        public int IdAsignacionAccion { get; set; }
        public string DescipcionActividad { get; set; }
        public string DescripcionFallo { get; set; }
    }
}
