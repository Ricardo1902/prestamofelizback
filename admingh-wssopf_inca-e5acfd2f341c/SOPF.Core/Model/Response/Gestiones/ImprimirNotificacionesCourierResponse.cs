﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ImprimirNotificacionesCourierResponse : Respuesta
    {
        public ImprimirNotificacionesCourierResultado Resultado { get; set; }
        public ImprimirNotificacionesCourierResponse()
        {
            Resultado = new ImprimirNotificacionesCourierResultado();
        }
    }
    public class ImprimirNotificacionesCourierResultado
    {
        public byte[] ContenidoArchivo { get; set; }

        public ImprimirNotificacionesCourierResultado()
        {
        }
    }
}
