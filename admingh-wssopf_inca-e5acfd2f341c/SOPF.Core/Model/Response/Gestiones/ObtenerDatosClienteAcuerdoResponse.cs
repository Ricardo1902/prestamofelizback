﻿using SOPF.Core.Model.Gestiones;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerDatosClienteAcuerdoResponse: Respuesta
    {
        public ClienteAcuerdoVM Datos { get; set; }
    }
}
