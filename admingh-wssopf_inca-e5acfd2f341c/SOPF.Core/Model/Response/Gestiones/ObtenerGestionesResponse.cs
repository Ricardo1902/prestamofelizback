﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Gestiones
{
    public class ObtenerGestionesResponse: Respuesta
    {
        public List<GestionVM> Gestiones { get; set; }
    }
}
