﻿using Newtonsoft.Json;

namespace SOPF.Core.Model.Response.WsPfLleida.DosFA
{
    public class VerificarResponse : BaseResponse
    {
        [JsonProperty("intentosRestantes")]
        public int IntentosRestantes { get; set; }
    }
}
