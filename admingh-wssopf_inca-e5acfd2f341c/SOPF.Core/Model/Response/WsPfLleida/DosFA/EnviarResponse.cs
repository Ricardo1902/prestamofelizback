﻿using Newtonsoft.Json;

namespace SOPF.Core.Model.Response.WsPfLleida.DosFA
{
    public class EnviarResponse : BaseResponse
    {
        [JsonProperty("idPeticion")]
        public int IdPeticion { get; set; }
        [JsonProperty("estatusSms", NullValueHandling = NullValueHandling.Ignore)]
        public string EstatusSms { get; set; }
        [JsonProperty("estatusEmail", NullValueHandling = NullValueHandling.Ignore)]
        public string EstatusEmail { get; set; }
    }
}
