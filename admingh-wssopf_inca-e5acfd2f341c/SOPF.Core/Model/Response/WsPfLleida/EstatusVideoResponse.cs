﻿using SOPF.Core.Model.Lleida;
using System;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.WsPfLleida
{
    public class EstatusVideoResponse : BaseResponse
    {
        public PeticionResponse Peticion { get; set; }
        public List<ArchivoDigitalVM> Archivos { get; set; }
    }
    public class PeticionResponse
    {
        public int IdPeticion { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }
        public DateTime? FechaUso { get; set; }
        public bool? Finalizado { get; set; }
        public bool? Correcto { get; set; }
    }
}
