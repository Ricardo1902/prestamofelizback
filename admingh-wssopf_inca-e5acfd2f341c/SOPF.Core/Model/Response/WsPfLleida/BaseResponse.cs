﻿namespace SOPF.Core.Model.Response.WsPfLleida
{
    public class BaseResponse
    {
        public bool Error { get; set; }
        public int CodigoEstado { get; set; }
        public string Mensaje { get; set; }
        public int TotalRegistros { get; set; }

        public BaseResponse()
        {
            Error = false;
            CodigoEstado = 200;
        }
    }
}
