﻿namespace SOPF.Core.Model.Response.WsPfLleida
{
    public class NuevoVideoResponse : BaseResponse
    {
        public int IdPeticion { get; set; }
        public string UrlSesion { get; set; }
    }
}
