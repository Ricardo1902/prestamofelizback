﻿using SOPF.Core.Model.WsPfLleida;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SOPF.Core.Model.Response.WsPfLleida
{
    [DataContract]
    public class FirmasResponse : BaseResponse
    {
        [DataMember(Name = "Signature", EmitDefaultValue = false)]
        public List<FirmaVM> Firmas { get; set; }
    }
}
