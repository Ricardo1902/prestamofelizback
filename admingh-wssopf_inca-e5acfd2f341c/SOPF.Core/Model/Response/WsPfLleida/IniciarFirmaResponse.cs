﻿using SOPF.Core.Model.WsPfLleida;

namespace SOPF.Core.Model.Response.WsPfLleida
{
    public class IniciarFirmaResponse : BaseResponse
    {
        public int IdPeticion { get; set; }
        public FirmaVM Firma { get; set; }
    }
}
