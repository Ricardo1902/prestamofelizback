﻿using SOPF.Core.Model.Reportes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response
{
    public class ImpresionDocumentoResponse : Respuesta
    {
        public ImpresionDocumentoResultado Resultado { get; set; }
        public ImpresionDocumentoResponse()
        {
            Resultado = new ImpresionDocumentoResultado();
        }
    }

    public class ImpresionDocumentoResultado
    {
        public byte[] ContenidoArchivo { get; set; }
        public string TipoContenido { get; set; }
        public string NombreArchivo { get; set; }
        public string Extension { get; set; }
        public List<PosicionFirmaVM> Firmas { get; set; }
        public string DatosDocumento { get; set; }
        public string Url { get; set; }

        public ImpresionDocumentoResultado()
        {
        }
    }
}
