﻿namespace SOPF.Core.Model.Response.Sistema
{
    public class VerificarOtpResponse : Respuesta
    {
        public int IntentosRestantes { get; set; }
    }
}
