﻿namespace SOPF.Core.Model.Response.Sistema
{
    public class EnviarOtpResponse : Respuesta
    {
        public int IdPeticion { get; set; }
    }
}
