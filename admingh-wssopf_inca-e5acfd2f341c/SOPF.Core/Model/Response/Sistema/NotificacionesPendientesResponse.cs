﻿using SOPF.Core.Model.Sistema;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Sistema
{
    public class NotificacionesPendientesResponse : Respuesta
    {
        public NotificacionesPendientesResultado Resultado { get; set; }

        public NotificacionesPendientesResponse()
        {
            Resultado = new NotificacionesPendientesResultado();
        }
    }

    public class NotificacionesPendientesResultado
    {
        public List<NotificacionVM> Notificaciones { get; set; }
        public NotificacionesPendientesResultado()
        {
            Notificaciones = new List<NotificacionVM>();
        }
    }
}
