﻿using SOPF.Core.Model.Sistema;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Sistema
{
    public class NotificacionesSolNotPendientesResponse : Respuesta
    {
        public NotificacionesSolNotPendientesResultado Resultado { get; set; }

        public NotificacionesSolNotPendientesResponse()
        {
            Resultado = new NotificacionesSolNotPendientesResultado();
        }
    }

    public class NotificacionesSolNotPendientesResultado
    {
        public List<NotificacionSolNotVM> Notificaciones { get; set; }
        public NotificacionesSolNotPendientesResultado()
        {
            Notificaciones = new List<NotificacionSolNotVM>();
        }
    }
}
