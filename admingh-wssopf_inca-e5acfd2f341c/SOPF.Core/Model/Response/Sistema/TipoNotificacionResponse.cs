﻿using SOPF.Core.Model.Sistema;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Sistema
{
    public class TipoNotificacionResponse : Respuesta
    {
        public TipoNotificacionVM TipoNotificacion { get; set; }
    }
}
