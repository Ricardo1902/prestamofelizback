﻿using SOPF.Core.Model.Sistema;

namespace SOPF.Core.Model.Response.Sistema
{
    public class ConexionSipGestionResponse : Respuesta
    {
        public ConexionSipVM Conexion { get; set; }
    }
}
