﻿using SOPF.Core.Model.Sistema;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Sistema
{
    public class CuentaCorreoResponse : Respuesta
    {
        public CuentaCorreoResultado Resultado { get; set; }
        public CuentaCorreoResponse()
        {
            Resultado = new CuentaCorreoResultado();
        }
    }

    public class CuentaCorreoResultado
    {
        public List<CuentaCorreoVM> CuentaCorreo { get; set; }
        public CuentaCorreoResultado()
        {
            CuentaCorreo = new List<CuentaCorreoVM>();
        }
    }
}
