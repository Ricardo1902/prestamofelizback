﻿using SOPF.Core.BusinessLogic.Sistema;

namespace SOPF.Core.Model.Response.Sistema
{
    public class DescargarArchivoGDResponse : Respuesta
    {
        public ArchivoGoogleDrive ArchivoGoogle { get; set; }
    }
}
