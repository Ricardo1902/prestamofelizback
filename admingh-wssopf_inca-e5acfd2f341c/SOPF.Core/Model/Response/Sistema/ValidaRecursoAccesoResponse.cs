﻿using SOPF.Core.Model.Sistema;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Sistema
{
    public class ValidaRecursoAccesoResponse : Respuesta
    {
        public List<RecursoAccionVM> RecursoAcciones { get; set; }
    }
}
