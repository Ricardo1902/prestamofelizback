﻿using SOPF.Core.Model.Catalogo;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Catalogo
{
    public class ObtenerTiposCobroResponse : Respuesta
    {
        public List<TipoCobroVM> Tipos { get; set; }
    }
}
