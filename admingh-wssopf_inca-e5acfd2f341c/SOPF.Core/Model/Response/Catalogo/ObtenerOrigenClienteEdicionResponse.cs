﻿using SOPF.Core.Model.Catalogo;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Catalogo
{
    public class ObtenerOrigenClienteEdicionResponse : Respuesta
    {
        public List<OrigenClienteEdicionVM> Origenes { get; set; }
    }
}
