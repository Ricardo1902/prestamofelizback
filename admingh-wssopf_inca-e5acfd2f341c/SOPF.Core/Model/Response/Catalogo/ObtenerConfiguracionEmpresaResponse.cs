﻿using SOPF.Core.Entities;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Catalogo
{
    public class ObtenerConfiguracionEmpresaResponse : Respuesta
    {
        public List<ConfiguracionEmpresaResultado> Resultado { get; set; }
        public List<Combo> ComboSituacionLaboral { get; set; }
        public List<Combo> ComboRegimenPension { get; set; }
        public int IdConfiguracionEmpresa { get; set; }

        public ObtenerConfiguracionEmpresaResponse()
        {
            Resultado = new List<ConfiguracionEmpresaResultado>();
            ComboSituacionLaboral = new List<Combo>();
            ComboRegimenPension = new List<Combo>();
            IdConfiguracionEmpresa = 0;
        }
    }

    public class ConfiguracionEmpresaResultado
    {
        public int IdEmpresa { get; set; }
        public int IdSituacionLaboral { get; set; }
        public int IdRegimenPension { get; set; }
        public int IdOrganoPago { get; set; }
        public string NivelRiesgo { get; set; }
    }
}
