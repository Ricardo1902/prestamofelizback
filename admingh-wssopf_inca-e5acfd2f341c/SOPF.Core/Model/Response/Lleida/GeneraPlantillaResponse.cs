﻿using System;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Lleida
{
    public class GeneraPlantillaResponse : Respuesta
    {
        public GeneraPlantillaResultado Resultado { get; set; }
        public GeneraPlantillaResponse()
        {
            Resultado = new GeneraPlantillaResultado();
        }
    }

    public class GeneraPlantillaResultado
    {
        public List<GeneraPlantillaResult> GeneraPlantilla { get; set; }
        public GeneraPlantillaResultado()
        {
            GeneraPlantilla = new List<GeneraPlantillaResult>();
        }
    }

    public class GeneraPlantillaResult
    {
        public int IdPlantilla { get; set; }
        public int IdConfig { get; set; }
        public int IdUsuario { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
