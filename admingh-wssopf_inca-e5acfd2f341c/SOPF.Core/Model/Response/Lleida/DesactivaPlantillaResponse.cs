﻿using System;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Lleida
{
    public class DesactivaPlantillaResponse : Respuesta
    {
        public DesactivaPlantillaResultado Resultado { get; set; }
        public DesactivaPlantillaResponse()
        {
            Resultado = new DesactivaPlantillaResultado();
        }
    }

    public class DesactivaPlantillaResultado
    {
        public List<DesactivaPlantillaResult> DesactivaPlantilla { get; set; }
        public DesactivaPlantillaResultado()
        {
            DesactivaPlantilla = new List<DesactivaPlantillaResult>();
        }
    }

    public class DesactivaPlantillaResult
    {
        public int IdPlantilla { get; set; }
        public int IdConfig { get; set; }
    }
}
