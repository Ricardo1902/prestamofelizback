﻿using iText.Layout.Element;
using SOPF.Core.Model.Lleida;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Lleida
{
    public class ConfiguracionesResponse : Respuesta
    {
        public ConfiguracionesResultado Resultado { get; set; }
    }
    public class ConfiguracionesResultado
    {
        public List<ConfiguracionStatusVM> Configuraciones { get; set; }
    }
}
