﻿using SOPF.Core.Model.WsPfLleida;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Lleida
{
    public class ObtenerFirmasResponse : Respuesta
    {
        public List<FirmaVM> Firmas { get; set; }
    }
}
