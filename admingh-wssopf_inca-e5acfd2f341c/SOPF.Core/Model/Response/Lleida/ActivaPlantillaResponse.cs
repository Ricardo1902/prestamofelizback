﻿using System;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Lleida
{
    public class ActivaPlantillaResponse : Respuesta
    {
        public ActivaPlantillaResultado Resultado { get; set; }
        public ActivaPlantillaResponse()
        {
            Resultado = new ActivaPlantillaResultado();
        }
    }

    public class ActivaPlantillaResultado
    {
        public List<ActivaPlantillaResult> ActivaPlantilla { get; set; }
        public ActivaPlantillaResultado()
        {
            ActivaPlantilla = new List<ActivaPlantillaResult>();
        }
    }

    public class ActivaPlantillaResult
    {
        public int IdPlantilla { get; set; }
        public int IdConfig { get; set; }
    }
}
