﻿namespace SOPF.Core.Model.Response.Lleida
{
    public class AltaFirmaResponse : Respuesta
    {
        public int IdPeticion { get; set; }
        public string SigantureId { get; set; }
    }
}
