﻿namespace SOPF.Core.Model.Response.Lleida
{
    public class NuevoVideoResponse : Respuesta
    {
        public int IdPeticion { get; set; }
        public string UrlSesion { get; set; }
    }
}
