﻿namespace SOPF.Core.Model.Response.Cobranza
{
    public class ProcesarArchivoPagoMasivoRespose : Respuesta
    {
        public int IdPagoMasivo { get; set; }
    }
}
