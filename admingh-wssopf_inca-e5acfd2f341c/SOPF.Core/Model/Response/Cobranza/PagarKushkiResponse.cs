﻿using System;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class PagarKushkiResponse
    {
        public string ticketNumber { get; set; }
        public string message { get; set; }
    }
}
