﻿namespace SOPF.Core.Model.Response.Cobranza
{
    public class GeneraPagoReponse : Respuesta
    {
        public PagoResultado Resultado { get; set; }

        public GeneraPagoReponse()
        {
            Resultado = new PagoResultado();
        }
    }
}
