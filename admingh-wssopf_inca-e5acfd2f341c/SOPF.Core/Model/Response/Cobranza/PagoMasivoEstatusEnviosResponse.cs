﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class PagoMasivoEstatusEnviosResponse : Respuesta
    {
        public List<PagoMasivoEstatusVM> PagosMasivosEstatus { get; set; }
    }
}
