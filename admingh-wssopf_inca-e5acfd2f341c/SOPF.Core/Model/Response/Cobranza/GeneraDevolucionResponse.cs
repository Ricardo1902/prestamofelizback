﻿namespace SOPF.Core.Model.Response.Cobranza
{
    public class GeneraDevolucionResponse : Respuesta
    {
        public GeneraDevolucionResultado resultado { get; set; }

        public GeneraDevolucionResponse()
        {
            resultado = new GeneraDevolucionResultado();
        }
    }

    public class GeneraDevolucionResultado
    {

    }
}
