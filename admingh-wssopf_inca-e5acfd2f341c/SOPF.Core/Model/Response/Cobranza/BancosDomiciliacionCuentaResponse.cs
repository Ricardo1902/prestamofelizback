﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class BancosDomiciliacionCuentaResponse : Respuesta
    {
        public List<BancoVM> Bancos { get; set; }
    }
}
