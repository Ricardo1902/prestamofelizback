﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class MotivosExclusionResponse : Respuesta
    {
        public MotivosExclusionResultado Resultado { get; set; }
        public MotivosExclusionResponse()
        {
            Resultado = new MotivosExclusionResultado();
        }
    }

    public class MotivosExclusionResultado
    {
        public List<MotivoExclusionDomiciliacionVM> MotivosExclusion { get; set; }
        public MotivosExclusionResultado()
        {
            MotivosExclusion = new List<MotivoExclusionDomiciliacionVM>();
        }
    }
}
