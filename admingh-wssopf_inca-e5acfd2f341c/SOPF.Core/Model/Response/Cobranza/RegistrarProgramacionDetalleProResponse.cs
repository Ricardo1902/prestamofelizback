﻿namespace SOPF.Core.Model.Response.Cobranza
{
    public class RegistrarProgramacionDetalleProResponse : Respuesta
    {
        public int IdProgramacionDetallePro { get; set; }
    }
}
