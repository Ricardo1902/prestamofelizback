﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class ProgramacionEstatusResponse : Respuesta
    {
        public List<ProgramacionEstatusVM> ProgramacionEstatus { get; set; }
    }
}
