﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class PagosMasivosDetalleResponse : Respuesta
    {
        public PagosMasivosDetalleResultado Resultado { get; set; }
        public PagosMasivosDetalleResponse()
        {
            Resultado = new PagosMasivosDetalleResultado();
        }
    }

    public class PagosMasivosDetalleResultado
    {
        public List<PagoMasivoDetalleVM> PagoMasivoDetalle { get; set; }

        public PagosMasivosDetalleResultado()
        {
            PagoMasivoDetalle = new List<PagoMasivoDetalleVM>();
        }
    }
}
