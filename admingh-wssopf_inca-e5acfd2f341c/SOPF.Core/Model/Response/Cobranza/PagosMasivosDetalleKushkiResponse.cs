﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class PagosMasivosDetalleKushkiResponse : Respuesta
    {
        public PagosMasivosDetalleKushkiResultado Resultado { get; set; }
        public PagosMasivosDetalleKushkiResponse()
        {
            Resultado = new PagosMasivosDetalleKushkiResultado();
        }
    }

    public class PagosMasivosDetalleKushkiResultado
    {
        public List<PagoMasivoDetalleKushkiVM> PagoMasivoDetalleKushki { get; set; }

        public PagosMasivosDetalleKushkiResultado()
        {
            PagoMasivoDetalleKushki = new List<PagoMasivoDetalleKushkiVM>();
        }
    }
}
