﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;
using System.Data;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class DescargarPagosFallidosVisanetResponse : Respuesta
    {
        public DescargarPagosFallidosVisanetResultado Resultado { get; set; }
        public DescargarPagosFallidosVisanetResponse()
        {
            Resultado = new DescargarPagosFallidosVisanetResultado();
        }
    }

    public class DescargarPagosFallidosVisanetResultado
    {
        public DataSet PagosFallidosVisanetVM { get; set; }

        public DescargarPagosFallidosVisanetResultado()
        {
            PagosFallidosVisanetVM = new DataSet();
        }
    }
}
