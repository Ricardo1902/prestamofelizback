﻿using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class ActualizarEstatusProgramacionResponse : Respuesta
    {
        public List<ActualizarEstatusProgramacionResult> Programaciones { get; set; }
    }

    public class ActualizarEstatusProgramacionResult
    {
        public int IdProgramacionDetalle { get; set; }
        public int IdProgramacionEstatus { get; set; }
        public bool Actualizado { get; set; }
    }
}
