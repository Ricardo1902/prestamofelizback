﻿namespace SOPF.Core.Model.Response.Cobranza
{
    public class SuscripcionKushkiResponse
    {
        public string subscriptionId { get; set; }
        public string message { get; set; }
    }
}
