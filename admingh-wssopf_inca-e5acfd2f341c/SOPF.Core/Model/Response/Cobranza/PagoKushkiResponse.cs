﻿using System;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class PagoKushkiResponse : Respuesta
    {
        public PagoKushkiResultado Resultado { get; set; }

        public PagoKushkiResponse()
        {
            Resultado = new PagoKushkiResultado();
        }
    }

    public class PagoKushkiResultado
    {
        public int IdPeticion { get; set; }
        public string IdTransaccion { get; set; }
        public DateTime FechaTransaccion { get; set; }
        public string Terminal { get; set; }
        public string NumeroRastreo { get; set; }
        public string Estatus { get; set; }
        public string CodigoAutorizacion { get; set; }
        public string DescripcionAcccion { get; set; }
    }
}
