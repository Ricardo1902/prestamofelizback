﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;
using System.Data;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class DescargarPagosFallidosKushkiResponse : Respuesta
    {
        public DescargarPagosFallidosKushkiResultado Resultado { get; set; }
        public DescargarPagosFallidosKushkiResponse()
        {
            Resultado = new DescargarPagosFallidosKushkiResultado();
        }
    }

    public class DescargarPagosFallidosKushkiResultado
    {
        public DataSet PagosFallidosKushkiVM { get; set; }

        public DescargarPagosFallidosKushkiResultado()
        {
            PagosFallidosKushkiVM = new DataSet();
        }
    }
}
