﻿namespace SOPF.Core.Model.Response.Cobranza
{
    public class DescargaArchivoDomiciliacionResponse : Respuesta
    {
        public GenerarArchivoDomiciliacionResultado Resultado { get; set; }
        public DescargaArchivoDomiciliacionResponse()
        {
            Resultado = new GenerarArchivoDomiciliacionResultado();
        }
    }
    public class GenerarArchivoDomiciliacionResultado
    {
        public byte[] ContenidoArchivo { get; set; }
        public string TipoContenido { get; set; }
        public string NombreArchivo { get; set; }
        public string Extension { get; set; }
        public GenerarArchivoDomiciliacionResultado()
        {
        }
    }
}
