﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class ProgramacionesProximasResponse : Respuesta
    {
        public List<ProgramacionDomiciliacionVM> Programaciones { get; set; }
    }
}
