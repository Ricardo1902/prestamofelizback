﻿namespace SOPF.Core.Model.Response.Cobranza
{
    public class GeneraPagoMavisoResponse : Respuesta
    {
        public GeneraPagoMavisoResultado Resultado { get; set; }

        public GeneraPagoMavisoResponse()
        {
            Resultado = new GeneraPagoMavisoResultado();
        }
    }

    public class GeneraPagoMavisoResultado
    {

    }
}
