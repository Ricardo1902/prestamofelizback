﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class CuentasExcluidasResponse : Respuesta
    {
        public CuentasExcluidasResultado Resultado { get; set; }
        public CuentasExcluidasResponse()
        {
            Resultado = new CuentasExcluidasResultado();
        }
    }

    public class CuentasExcluidasResultado
    {
        public List<CuentasExcluidasVM> CuentasExcluidas { get; set; }
        public CuentasExcluidasResultado()
        {
            CuentasExcluidas = new List<CuentasExcluidasVM>();
        }
    }
}
