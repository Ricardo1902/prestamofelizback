﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class PagosMasivosResponse : Respuesta
    {
        public PagosMasivosResultado Resultado = new PagosMasivosResultado();
        public PagosMasivosResponse()
        {
            Resultado = new PagosMasivosResultado();
        }
    }

    public class PagosMasivosResultado
    {
        public List<PagoMasivoVM> PagosMasivos { get; set; }

        public PagosMasivosResultado()
        {
            PagosMasivos = new List<PagoMasivoVM>();
        }
    }
}
