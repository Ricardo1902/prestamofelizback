﻿using SOPF.Core.Model.Cobranza;
using SOPF.Core.Model.Result.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class ExcluirCuentasResponse : Respuesta
    {
        public ExcluirCuentasResultado Resultado { get; set; }
        public ExcluirCuentasResponse()
        {
            Resultado = new ExcluirCuentasResultado();
        }
    }

    public class ExcluirCuentasResultado
    {
        public List<CuentaExcluyeDomiciliacionResult> ErrorCuentas { get; set; }
        public ExcluirCuentasResultado()
        {
        }
    }
}
