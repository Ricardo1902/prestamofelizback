﻿namespace SOPF.Core.Model.Response.Cobranza
{
    public class GenerarPlantillaEnvioCobroResponse : Respuesta
    {
        public GenerarPlantillaEnvioCobroResultado Resultado { get; set; }
        public GenerarPlantillaEnvioCobroResponse()
        {
            Resultado = new GenerarPlantillaEnvioCobroResultado();
        }
    }

    public class GenerarPlantillaEnvioCobroResultado
    {
        public int IdPlantillaEnvioCobro { get; set; }
        public GenerarPlantillaEnvioCobroResultado()
        {
        }
    }
}
