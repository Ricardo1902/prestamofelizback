﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class TipoProgramacionesResponse : Respuesta
    {
        public List<TipoProgramacionVM> TipoProgramacion { get; set; }
    }
}
