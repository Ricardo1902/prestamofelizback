﻿namespace SOPF.Core.Model.Response.Cobranza
{
    public class PeticionDomiciliacionResponse : Respuesta
    {
        public int IdPeticionDomiciliacion { get; set; }
    }
}
