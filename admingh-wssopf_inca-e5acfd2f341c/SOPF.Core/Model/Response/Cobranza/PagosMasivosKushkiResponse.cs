﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class PagosMasivosKushkiResponse : Respuesta
    {
        public PagosMasivosKushkiResultado Resultado = new PagosMasivosKushkiResultado();
        public PagosMasivosKushkiResponse()
        {
            Resultado = new PagosMasivosKushkiResultado();
        }
    }

    public class PagosMasivosKushkiResultado
    {
        public List<PagoMasivoKushkiVM> PagosMasivosKushki { get; set; }

        public PagosMasivosKushkiResultado()
        {
            PagosMasivosKushki = new List<PagoMasivoKushkiVM>();
        }
    }
}
