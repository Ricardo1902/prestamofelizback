﻿using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class ProgramacionGeneraMandoCobroResponse : Respuesta
    {
        public int? IdLayoutGenerado { get; set; }
        public List<int> IdPagosMasivos { get; set; }
        public int IdProgramacionEstatus { get; set; }
    }
}
