﻿using System;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class VerificacionRUCResponse :Respuesta
    {
        public VerificacionRUCResultado data { get; set; }
        public string message { get; set; }

        public VerificacionRUCResponse()
        {
            data = new VerificacionRUCResultado();
        }
    }

    public class VerificacionRUCResultado
    {
        public string nombre_o_razon_social { get; set; }
        public string estado { get; set; }
        // public string TipoEmp { get; set; }
    }
}
