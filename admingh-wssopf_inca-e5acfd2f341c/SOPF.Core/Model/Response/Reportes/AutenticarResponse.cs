﻿namespace SOPF.Core.Model.Response.Reportes
{
    public class AutenticarResponse : Respuesta
    {
        public int IdUsuario { get; set; }
        public int IdSucursal { get; set; }
        public int IdTipoUsuario { get; set; }
        public string NombreUsuario { get; set; }
    }
}
