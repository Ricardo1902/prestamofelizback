﻿using SOPF.Core.Model.Reportes;

namespace SOPF.Core.Model.Response.Reportes
{
    public class UsuariosResponse
    {
        public UsuarioLoginVM UsuarioLogin { get; set; }
    }
}
