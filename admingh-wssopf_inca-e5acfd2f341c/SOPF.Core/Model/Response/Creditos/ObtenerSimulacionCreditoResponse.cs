﻿using System.Collections.Generic;
using static SOPF.Core.Entities.Solicitudes.CronogramaPagos;

namespace SOPF.Core.Model.Response.Creditos
{
    public class ObtenerSimulacionCreditoResponse : Respuesta
    {
        public List<Recibo> Resultado { get; set; }

        public ObtenerSimulacionCreditoResponse()
        {
            Resultado = new List<Recibo>();
        }
    }
}
