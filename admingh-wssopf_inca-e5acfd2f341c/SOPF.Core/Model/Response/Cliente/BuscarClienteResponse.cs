﻿using SOPF.Core.Model.Clientes;

namespace SOPF.Core.Model.Response.Cliente
{
    public class BuscarClienteResponse : Respuesta
    {
        public DatosClienteVM Cliente { get; set; }
    }
}
