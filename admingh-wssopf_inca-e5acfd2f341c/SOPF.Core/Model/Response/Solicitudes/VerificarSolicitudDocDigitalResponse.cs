﻿using Org.BouncyCastle.Utilities.IO.Pem;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class VerificarSolicitudDocDigitalResponse : Respuesta
    {
        public bool EnviarProcesoFirmaDigital { get; set; }
        public bool ProcesoIniciado { get; set; }
    }
}
