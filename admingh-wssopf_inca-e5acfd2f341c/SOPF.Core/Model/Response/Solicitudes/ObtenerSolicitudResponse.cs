﻿using SOPF.Core.Model.Solicitudes;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class ObtenerSolicitudResponse : Respuesta
    {
        public SolicitudVM Solicitud { get; set; }
    }
}
