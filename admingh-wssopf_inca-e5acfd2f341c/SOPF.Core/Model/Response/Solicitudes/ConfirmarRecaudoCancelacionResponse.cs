﻿namespace SOPF.Core.Model.Response.Solicitudes
{
    public class ConfirmarRecaudoCancelacionResponse : Respuesta
    {
        public string token { get; set; }
    }
}
