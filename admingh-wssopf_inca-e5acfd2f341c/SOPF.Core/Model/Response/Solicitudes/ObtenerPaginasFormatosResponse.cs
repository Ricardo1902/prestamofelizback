﻿using SOPF.Core.Model.Solicitudes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class ObtenerPaginasFormatosResponse : Respuesta
    {
        public List<FormatoPaginaVM> formatos { get; set; }
    }
}
