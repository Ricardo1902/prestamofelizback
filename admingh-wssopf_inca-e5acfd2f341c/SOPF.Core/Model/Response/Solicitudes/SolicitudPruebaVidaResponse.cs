﻿using iText.Layout.Element;
using SOPF.Core.Model.Solicitudes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class SolicitudPruebaVidaResponse : Respuesta
    {
        public List<SolicitudPruebaVidaVM> PruebasVida { get; set; }
    }
}
