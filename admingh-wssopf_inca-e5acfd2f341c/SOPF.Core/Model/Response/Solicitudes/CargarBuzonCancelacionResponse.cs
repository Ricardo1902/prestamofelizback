﻿using SOPF.Core.Model.Solicitudes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class CargarBuzonCancelacionResponse : Respuesta
    {
        public List<ProcesoBuzonCancelacionVM> Procesos { get; set; }
    }
}
