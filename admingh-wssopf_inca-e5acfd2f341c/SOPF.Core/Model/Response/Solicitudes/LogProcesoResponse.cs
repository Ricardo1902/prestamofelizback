﻿using SOPF.Core.Model.Solicitudes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class LogProcesoResponse : Respuesta
    {
        public List<LogProcesoVM> LogProcesos { get; set; }
    }
}
