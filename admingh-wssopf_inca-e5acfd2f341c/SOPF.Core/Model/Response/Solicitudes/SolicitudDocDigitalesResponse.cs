﻿using SOPF.Core.Model.Solicitudes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class SolicitudDocDigitalesResponse : Respuesta
    {
        public List<SolicitudDocDigitalVM> Documentos { get; set; }
    }
}
