﻿namespace SOPF.Core.Model.Response.Solicitudes
{
    public class AltaSolicitudFirmaDigitalResponse : Respuesta
    {
        public int IdSolicitudFirmaDigital { get; set; }
        public int IdProceso { get; set; }
    }
}
