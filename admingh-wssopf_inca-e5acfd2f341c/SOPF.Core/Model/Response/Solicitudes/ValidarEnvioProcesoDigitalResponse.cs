﻿namespace SOPF.Core.Model.Response.Solicitudes
{
    public class ValidarEnvioProcesoDigitalResponse : Respuesta
    {
        public bool ProcesoIniciado { get; set; }
    }
}
