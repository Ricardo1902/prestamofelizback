﻿namespace SOPF.Core.Model.Response.Solicitudes
{
    public class InicarSolicitudProcesoDigitalResponse : Respuesta
    {
        public bool EnviaProcesoDigital { get; set; }
        public bool ProcesoIniciado { get; set; }
    }
}
