﻿using SOPF.Core.Model.Solicitudes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class SolicitudFirmaDigitalResponse : Respuesta
    {
        public List<SolicitudFirmaDigitalVM> Firmas { get; set; }
    }
}
