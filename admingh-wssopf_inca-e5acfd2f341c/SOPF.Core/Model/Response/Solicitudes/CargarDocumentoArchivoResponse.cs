﻿using SOPF.Core.Model.Solicitudes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class CargarDocumentoArchivoResponse : Respuesta
    {
        public List<DocumentoArchivoVM> DocumentoArchivo { get; set; }
    }
}
