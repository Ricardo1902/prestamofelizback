﻿using SOPF.Core.Model.Solicitudes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class DocumentosArchivosResponse : Respuesta
    {
        public List<DocumentoVM> Documentos { get; set; }
    }
}
