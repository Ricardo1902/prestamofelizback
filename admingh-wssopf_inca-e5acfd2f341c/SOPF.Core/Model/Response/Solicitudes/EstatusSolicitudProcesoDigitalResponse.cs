﻿using SOPF.Core.Model.Solicitudes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class EstatusSolicitudProcesoDigitalResponse : Respuesta
    {
        public bool ExisteProceso { get; set; }        
        public List<SolicitudProcesoDigitalVM> ProcesosDigital { get; set; }
    }
}
