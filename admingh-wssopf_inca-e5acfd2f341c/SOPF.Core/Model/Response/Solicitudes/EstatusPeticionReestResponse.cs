﻿using SOPF.Core.Model.Solicitudes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class EstatusPeticionReestResponse : Respuesta
    {
        public List<EstatusPeticionReestructuraVM> EstatusPeticion { get; set; }
    }
}
