﻿namespace SOPF.Core.Model.Response.Solicitudes
{
    public class ObtenerEstatusBuzonCancelacionResponse : Respuesta
    {
        public int IdEstatus { get; set; }
    }
}
