﻿using SOPF.Core.Model.Solicitudes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Solicitudes
{
    public class ExpedienteResponse : Respuesta
    {
        public List<DocumentoVM> Documentos { get; set; }
    }
}
