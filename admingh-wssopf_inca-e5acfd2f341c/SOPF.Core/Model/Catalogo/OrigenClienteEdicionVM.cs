﻿namespace SOPF.Core.Model.Catalogo
{
    public class OrigenClienteEdicionVM
    {
        public int IdOrigenClienteEdicion { get; set; }
        public string Url { get; set; }
    }
}
