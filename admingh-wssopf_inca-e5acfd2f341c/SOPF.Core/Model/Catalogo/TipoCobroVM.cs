﻿namespace SOPF.Core.Model.Catalogo
{
    public class TipoCobroVM
    {
        public int IdTipoCobro { get; set; }
        public string TipoCobro { get; set; }
    }
}
