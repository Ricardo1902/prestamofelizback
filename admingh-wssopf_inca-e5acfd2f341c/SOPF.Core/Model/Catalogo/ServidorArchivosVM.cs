﻿namespace SOPF.Core.Model.Catalogo
{
    public class ServidorArchivosVM
    {
        public int IdServidorArchivos { get; set; }
        public string Nombre { get; set; }
    }
}
