﻿namespace SOPF.Core.Model.Catalogo
{
    public class DetalleColoniaVM
    {
        public int IdEstado { get; set; }
        public string Estado { get; set; }
        public int IdCiudad { get; set; }
        public string Ciudad { get; set; }
        public int IdColonia { get; set; }
        public string Colonia { get; set; }
    }
}
