﻿using System;

namespace SOPF.Core.Model.Solicitudes
{
    public class TransaccionExtrajudicialVM
    {
        public string NombreCliente { get; set; }
        public string DNI { get; set; }
        public string Calle { get; set; }
        public string Distrito { get; set; }
        public DateTime FechaCredito{ get; set; }
        public decimal ImporteCredito { get; set; }
        public int PlazoActual { get; set; }
        public DateTime FechaPrimerRecibo { get; set; }
        public decimal CuotaActual { get; set; }
    }
}
