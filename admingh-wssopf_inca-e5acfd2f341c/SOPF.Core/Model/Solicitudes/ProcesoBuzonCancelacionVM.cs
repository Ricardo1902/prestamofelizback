﻿using System;

namespace SOPF.Core.Model.Solicitudes
{
    public class ProcesoBuzonCancelacionVM
    {
        public int IdProceso { get; set; }
        public decimal Solicitud { get; set; }
        public string TipoCredito { get; set; }
        public string Cliente { get; set; }
        public decimal SaldoVigente { get; set; }
        public int PagosRealizados { get; set; }
        public DateTime Registro { get; set; }
    }
}
