﻿using System;

namespace SOPF.Core.Model.Solicitudes
{
    public class ContratoVM
    {
        public int Folio { get; set; }
        public string Lugar { get; set; }
        public DateTime Fecha { get; set; }
        public string NombreCliente { get; set; }
    }
}
