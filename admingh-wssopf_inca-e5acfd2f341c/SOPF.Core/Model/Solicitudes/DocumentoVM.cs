﻿namespace SOPF.Core.Model.Solicitudes
{
    public class DocumentoVM
    {
        public int IdDocumentoArchivo { get; set; }
        public int IdDocumento { get; set; }
        public string NombreDocumento { get; set; }
        public string Contenido { get; set; }
        public string TipoMedio { get; set; }
    }
}
