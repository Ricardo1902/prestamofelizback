﻿using System;

namespace SOPF.Core.Model.Solicitudes
{
    public class SolicitudFirmaDigitalVM
    {
        public int IdSolicitudFirmaDigital { get; set; }
        public int IdSolicitud { get; set; }
        public string ClaveEnvio { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public string EstatusFirma { get; set; }
        public string EstatusDescripcion { get; set; }
        public string ClaveFirma { get; set; }
        public string Mensaje { get; set; }
        public string Cliente { get; set; }
        public bool CargaEnCurso { get; set; }
        public bool Reenviar { get; set; }
    }
}
