﻿namespace SOPF.Core.Model.Solicitudes
{
    public class SolicitudDocDigitalVM
    {
        public int IdSolicitudDocDigital { get; set; }
        public int IdDocumento { get; set; }
        public string Documento { get; set; }
        public string Contenido { get; set; }
        public string ContenidoB64 { get; set; }
        public int Version { get; set; }
        public int Orden { get; set; }
    }
}
