﻿namespace SOPF.Core.Model.Solicitudes
{
    public class SolicitudFirmaDigitalArchivoVM
    {
        public int IdSolicitudFirmaDigital { get; set; }
        public int IdDocumento { get; set; }
        public int Orden { get; set; }
        public string NombreDocumentoDigital { get; set; }
    }
}
