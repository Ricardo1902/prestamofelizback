﻿namespace SOPF.Core.Model.Solicitudes
{
    public class FormatoPaginaVM
    {
        public int IdFormatoPagina { get; set; }
        public int IdFormato { get; set; }
        public string NombrePagina { get; set; }
        public int PaginaInicial { get; set; }
        public int PaginaFinal { get; set; }
        public int CantidadCopias { get; set; }
        public string NombreDescarga { get; set; }
        public string NombrePlantilla { get; set; }
    }
}
