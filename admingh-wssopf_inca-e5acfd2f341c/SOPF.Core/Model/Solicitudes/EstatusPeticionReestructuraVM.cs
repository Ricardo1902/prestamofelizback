﻿namespace SOPF.Core.Model.Solicitudes
{
    public class EstatusPeticionReestructuraVM
    {
        public int IdEstatusPeticionReestructura { get; set; }
        public string EstatusPeticionReestructura { get; set; }
    }
}
