﻿using System;

namespace SOPF.Core.Model.Solicitudes
{
    public class LogProcesoVM
    {
        public string Proceso { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Accion { get; set; }
    }
}
