﻿using System;

namespace SOPF.Core.Model.Solicitudes
{
    public class SolicitudProcesoDigitalVM
    {
        public int IdSolicitudProcesoDigital { get; set; }
        public int IdSolicitud { get; set; }
        public int IdProcesoDigitalConfigDet { get; set; }
        public int IdProcesoDigital { get; set; }
        public int Orden { get; set; }
        public bool ProcesoActual { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int IdEstatus { get; set; }
        public string ProcesoDigital { get; set; }
        public string Estatus { get; set; }
        public string Mensaje { get; set; }
        public int? IdProceso { get; set; }
    }
}
