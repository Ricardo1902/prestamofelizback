﻿namespace SOPF.Core.Model.Solicitudes
{
    public class DocumentoArchivoVM
    {
        public int IdDocumentoArchivo { get; set; }
        public int IdDocumento { get; set; }
        public string NombreArchivo { get; set; }
    }
}
