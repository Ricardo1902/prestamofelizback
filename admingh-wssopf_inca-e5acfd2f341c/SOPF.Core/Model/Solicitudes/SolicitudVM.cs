﻿using System;

namespace SOPF.Core.Model.Solicitudes
{
    public class SolicitudVM
    {
        public int IdSolicitud { get; set; }
        public int IdCliente { get; set; }
        public DateTime FechaSolicitud { get; set; }
        public int IdTipoCredito { get; set; }
        public int IdEstatus { get; set; }
        public int EstProceso { get; set; }
        public int IdAsignado { get; set; }
        public string Banco { get; set; }
        public BancoDomicilacion BancoDomiciliacion
        {
            get
            {
                if (string.IsNullOrEmpty(Banco)) return BancoDomicilacion.Otro;
                string banco = Banco.ToUpper();

                if (banco.Contains("CONTINENTAL")) return BancoDomicilacion.Continental;
                else if (banco.Contains("INTERBANK")) return BancoDomicilacion.Interbank;
                else return BancoDomicilacion.Otro;
            }
        }
    }
}
