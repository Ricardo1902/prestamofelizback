﻿using System;

namespace SOPF.Core.Model.Solicitudes
{
    public class SolicitudPruebaVidaVM
    {
        public int IdSolicitudPruebaVida { get; set; }
        public int IdPeticion { get; set; }
        public int IdSolicitud { get; set; }
        public string ClaveEnvio { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Cliente { get; set; }
        public bool SesionRealizada { get; set; }
        public bool ValidacionCorrecta { get; set; }
        public bool? Aprobado { get; set; }
        public string Mensaje { get; set; }
        public string Archivos { get; set; }
    }
}
