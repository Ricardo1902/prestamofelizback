﻿using System;

namespace SOPF.Core.Model.Solicitudes
{
    public class PeticionReestDocDigitalVM
    {
        public int Registro { get; set; }
        public int IdPeticionReestructura { get; set; }
        public int NoSolicitud { get; set; }
        public int NoSolicitudNuevo { get; set; }
        public string Cliente { get; set; }
        public DateTime FechaReestructura { get; set; }
        public int? IdSolicitudPruebaVida { get; set; }
        public bool? ConErrorPV { get; set; }
        public string MensajePV { get; set; }
        public string ArchivosPV { get; set; }
        public int? IdSolicitudFirmaDigital { get; set; }
        public int? IdEstatusFirma { get; set; }
        public string EstatusFirma { get; set; }
        public bool? ConErrorFD { get; set; }
        public string MensajeFD { get; set; }
        public string ArchivosFD { get; set; }
        public string Estatus { get; set; }
    }
}
