﻿using System;

namespace SOPF.Core.Model.Solicitudes
{
    public class SolicitudFormatoVM
    {
        public int IdSolicitud { get; set; }
        public string Vendedor { get; set; }
        public string NombresCliente { get; set; }
        public string ApellidoPaternoCliente { get; set; }
        public string ApellidoMaternoCliente { get; set; }
        public string Sexo { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string LugarNacimiento { get; set; }
        public string PaisNacimiento { get; set; }
        public string Ocupacion { get; set; }
        public string SituacionLaboral { get; set; }
        public string RegimenPension { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string Correo { get; set; }
        public string TipoDireccion { get; set; }
        public string NombreVia { get; set; }
        public string NumeroExterior { get; set; }
        public string NumeroInterior { get; set; }
        public string Lote { get; set; }
        public string Manzana { get; set; }
        public string TipoZona { get; set; }
        public string NombreZona { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }

        public decimal ImporteCredito { get; set; }
        public int Plazo { get; set; }
        public DateTime FechaSolicitud { get; set; }
    }
}
