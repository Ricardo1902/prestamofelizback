﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class GestionVM
    {
        public int IdGestion { get; set; }
        public DateTime? fch_Gestion { get; set; }
        public int IdGestor { get; set; }
        public string Comentario { get; set; }
        public int IdResultadoGestion { get; set; }
        public string ResultadoGestion { get; set; }
        public int idCuenta { get; set; }
        public string TipoGestion { get; set; } //Utilizado para campo de tabla gestiones.TB_Gestion
        public DateTime? fch_modificacion { get; set; }
        public int idCNT { get; set; }
        public string CNT { get; set; }
        public int idCampana { get; set; }
        public int idTipoGestion { get; set; }
        public decimal latitud { get; set; }
        public decimal longitud { get; set; }
        public string Tipo { get; set; } //Utilizado para catalogo de gestiones.TipoGestion
        public decimal? Monto { get; set; }
        public bool? GeneraRecordatorio { get; set; }
        public int? EstatusPromesa { get; set; }
        public DateTime? FechaUltimaNotificacion { get; set; }
        public DateTime? FechaProgUltNotificacion { get; set; }
    }
}
