﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class VerEnvioCourierSolicitudNotificacionVM
    {
        public int IdEnvioCourier { get; set; }
        public int IdSolicitudNotificacion { get; set; }
        public int IdSolicitud { get; set; }
        public string Notificacion { get; set; }
        public string Destino { get; set; }
        public string UsuarioRegistroNotificacion { get; set; }
        public DateTime FechaRegistroNotificacion { get; set; }
        public bool Error { get; set; }
        public string Mensaje { get; set; }
    }
}
