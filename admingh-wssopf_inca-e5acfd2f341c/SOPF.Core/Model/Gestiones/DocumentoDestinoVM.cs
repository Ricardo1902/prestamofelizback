﻿namespace SOPF.Core.Model.Gestiones
{
    public class DocumentoDestinoVM
    {
        public int IdDocumentoDestino { get; set; }
        public string Descripcion { get; set; }
        public int IdNotificacion { get; set; }
        public int IdDestinoNotificacion { get; set; }
    }
}
