﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class CargaAsignacionVM
    {
        public int IdCargaAsignacion { get; set; }
        public string NombreArchivo { get; set; }
        public int? TotalRegistros { get; set; }
        public string Actividad { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int IdEstatus { get; set; }
        public string Estatus { get; set; }
    }
}
