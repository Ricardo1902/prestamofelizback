﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class AcuerdoPagoVM
    {
        public int IdAcuerdoPago { get; set; }
        public int IdSolicitud { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public string UsuarioRegistro { get; set; }
        public string Gestor { get; set; }
        public int? TotalRegistros { get; set; }
        public string Actividad { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }
        public string Estatus { get; set; }
        public bool? Incumplido { get; set; }
    }
}
