﻿namespace SOPF.Core.Model.Gestiones
{
    public class CargaAsignacionDetalleVM
    {
        public int IdCargaAsignacionDetalle { get; set; }
        public int IdSolicitud { get; set; }
        public int? IdGestor { get; set; }
        public string Gestor { get; set; }
        public string Actividad { get; set; }
        public bool? Error { get; set; }
        public string Mensaje { get; set; }
        public int? IdAsignacion { get; set; }
    }
}
