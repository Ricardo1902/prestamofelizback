﻿using SOPF.Core.Entities;
using SOPF.Core.Entities.Cobranza;

namespace SOPF.Core.Model.Gestiones
{
    public class PagoGestionesVM
    {
        public int Cuota { get; set; }
        private Utils.DateTimeR _fecha;
        public Utils.DateTimeR Fecha
        {
            get { return (_fecha == null) ? new Utils.DateTimeR() : _fecha; }
            set { this._fecha = value; }
        }
        public decimal CapitalInicial { get; set; }
        public decimal Capital { get; set; }
        public decimal Interes { get; set; }
        public decimal IGV { get; set; }
        public decimal Seguro { get; set; }
        public decimal GAT { get; set; }
        private TBCATEstatus _estatus;
        public TBCATEstatus Estatus
        {
            get { return (_estatus == null) ? new TBCATEstatus() : _estatus; }
            set { this._estatus = value; }
        }
        public decimal SaldoRecibo { get; set; }
        public decimal TotalRecibo { get; set; }
        private CanalPago _canalPago;
        public CanalPago CanalPago
        {
            get { return (_canalPago == null) ? new CanalPago() : _canalPago; }
            set { this._canalPago = value; }
        }
        public string CanalPagoClave { get; set; }
        public string CanalPagoDesc { get; set; }
        private Utils.DateTimeR _fechaPago;
        public Utils.DateTimeR FechaPago
        {
            get { return (_fechaPago == null) ? new Utils.DateTimeR() : _fechaPago; }
            set { this._fechaPago = value; }
        }
        public decimal MontoPago { get; set; }

        public PagoGestionesVM()
        {
            this.Fecha = new Utils.DateTimeR();
            this.Estatus = new TBCATEstatus();
            this.CanalPago = new CanalPago();
            this.FechaPago = new Utils.DateTimeR();
        }
    }
}
