﻿namespace SOPF.Core.Model.Gestiones
{
    public class GestionLayoutArchivoVM
    {
        public int IdTipoGestion { get; set; }
        public int IdLayoutArchivo { get; set; }
        public string NombreLayout { get; set; }
        public int IdBanco { get; set; }
    }
}
