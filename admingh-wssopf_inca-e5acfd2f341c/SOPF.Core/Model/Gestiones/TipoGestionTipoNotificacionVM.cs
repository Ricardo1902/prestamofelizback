﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class TipoGestionTipoNotificacionVM
    {
        public int IdTipoGestTipoNot { get; set; }
        public int IdTipoGestion { get; set; }
        public int IdTipoNotificacion { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
