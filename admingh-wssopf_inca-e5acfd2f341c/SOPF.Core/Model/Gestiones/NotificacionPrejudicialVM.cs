﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class NotificacionPrejudicialVM
    {
        public DateTime FechaRegistro { get; set; }
        public string NombreCliente { get; set; }
        public string Direccion { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public decimal TotalDeuda { get; set; }
        public int CuotasVencidas { get; set; }
        public int IdCliente { get; set; }
        public int IdDestino { get; set; }
        public int IdSolicitud { get; set; }
    }
}
