﻿namespace SOPF.Core.Model.Gestiones
{
    public class DevolucionLlamadaVM
    {
        public int IdNotificacion { get; set; }
        public int IdSolicitud { get; set; }
        public string NombreUsuario { get; set; }
        public string Telefono { get; set; }
        public string Tipo { get; set; }
        public string AnexoReferencia { get; set; }
        public string Comentario { get; set; }
    }
}
