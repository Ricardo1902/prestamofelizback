﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class GestionDomiciliacionVM
    {
        public int IdGestionDomiciliacion { get; set; }
        public int IdSolicitud { get; set; }
        public string Cliente { get; set; }
        public decimal Monto { get; set; }
        public int IdTipoGestion { get; set; }
        public DateTime FechaDomiciliar { get; set; }
        public string Banco { get; set; }
        public string CuentaBancaria { get; set; }
        public string NumeroTarjeta { get; set; }
    }
}
