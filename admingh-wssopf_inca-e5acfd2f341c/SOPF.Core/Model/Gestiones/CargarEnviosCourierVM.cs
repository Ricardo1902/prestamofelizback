﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class CargarEnviosCourierVM
    {
        public int IdEnvioCourier { get; set; }
        public string UsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
