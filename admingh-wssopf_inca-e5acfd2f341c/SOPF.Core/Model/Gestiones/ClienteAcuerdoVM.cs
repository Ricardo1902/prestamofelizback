﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class ClienteAcuerdoVM
    {
        public int IdSolicitud { get; set; }
        public string NombreCliente { get; set; }
        public DateTime? FechaCredito { get; set; }
        public decimal MontoCredito { get; set; }
        public int Plazo { get; set; }
        public string Frecuencia { get; set; }
        public decimal SaldoCapital { get; set; }
        public decimal CostoTotalCredito { get; set; }
        public decimal SaldoVencido { get; set; }
        public string TipoCredito { get; set; }
        public int RecibosVencidos { get; set; }
        public int DiasMora { get; set; }
        public DateTime? FechaUltimoPago { get; set; }
        public decimal MontoCuota { get; set; }
        public decimal MontoLiquidacion { get; set; }
        public int DiaPago { get; set; }
    }
}
