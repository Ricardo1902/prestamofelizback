﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class TipoNotificacionVM
    {
        public int IdTipoNotificacion { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Actual { get; set; }
    }
}
