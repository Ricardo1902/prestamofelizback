﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class AcuerdoPagoDetalleVM
    {
        public int IdAcuerdoPagoDetalle { get; set; }
        public int IdAcuerdoPago { get; set; }
        public decimal Monto { get; set; }
        public decimal GastoAdministrativo { get; set; }
        public decimal MontoTotal { get; set; }
        public int IdEstatus { get; set; }
        public DateTime? FechaAcuerdo { get; set; }
        public DateTime? FechaEnvioDomiciliacion { get; set; }
        public DateTime? FechaCumplimiento { get; set; }
        public bool Activo { get; set; }
    }
}
