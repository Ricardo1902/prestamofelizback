﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class ArchivoGestionVM
    {
        public int IdArchivoGestion { get; set; }
        public string Nombre { get; set; }
        public string Comentario { get; set; }
        public int IdTipoArchivo { get; set; }
        public int IdDocumentoDestino { get; set; }
        public string NombreDocumento { get; set; }
        public string TipoArchivo { get; set; }
        public string Icono { get; set; }
        public int IdServidorArchivos { get; set; }
        public string IdArchivoServidor { get; set; }
        public string UrlArchivo { get; set; }
        public int IdSolicitud { get; set; }
        public string TipoMime { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Activo { get; set; }
    }
}
