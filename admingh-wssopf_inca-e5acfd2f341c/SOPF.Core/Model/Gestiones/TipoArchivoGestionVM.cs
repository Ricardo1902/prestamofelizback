﻿namespace SOPF.Core.Model.Gestiones
{
    public class TipoArchivoGestionVM
    {
        public int IdTipoArchivoGestion { get; set; }
        public string TipoArchivo { get; set; }
        public string Extension { get; set; }
        public string TipoMime { get; set; }
        public string Icono { get; set; }
        public bool PermiteDescarga { get; set; }
    }
}
