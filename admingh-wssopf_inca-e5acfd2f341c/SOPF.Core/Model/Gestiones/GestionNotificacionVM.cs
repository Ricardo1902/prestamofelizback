﻿namespace SOPF.Core.Model.Gestiones
{
    public class GestionNotificacionVM
    {
        public int IdNotificacion { get; set; }
        public int IdSolicitud { get; set; }
        public int IdCuenta { get; set; }
        public string NombreCliente { get; set; }
        public string Dni { get; set; }
        public decimal Monto { get; set; }
    }
}
