﻿namespace SOPF.Core.Model.Gestiones
{
    public class ResultadoGestionVM
    {
        public int IdResultado { get; set; }
        public string Descripcion { get; set; }
    }
}
