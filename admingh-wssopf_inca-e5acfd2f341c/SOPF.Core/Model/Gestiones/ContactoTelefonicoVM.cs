﻿namespace SOPF.Core.Model.Gestiones
{
    public class ContactoTelefonicoVM
    {
        public string TipoContacto { get; set; }
        public string Parentesco { get; set; }
        public string NombreContacto { get; set; }
        public string TipoTelefono { get; set; }
        public string Telefono { get; set; }
        public string Extension { get; set; }
    }
}
