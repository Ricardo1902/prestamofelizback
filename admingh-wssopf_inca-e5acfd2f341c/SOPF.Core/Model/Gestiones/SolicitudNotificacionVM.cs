﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class SolicitudNotificacionVM
    {
        public int IdSolicitudNotificacion { get; set; }
        public int SolicitudId { get; set; }
        public int NotificacionId { get; set; }
        public int DestinoId { get; set; }
        public int UsuarioRegistroId { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int? UsuarioEscaneoId { get; set; }
        public DateTime? FechaEscaneo { get; set; }
        public bool Actual { get; set; }
    }
}
