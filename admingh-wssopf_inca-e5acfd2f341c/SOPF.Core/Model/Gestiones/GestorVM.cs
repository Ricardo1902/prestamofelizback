﻿namespace SOPF.Core.Model.Gestiones
{
    public class GestorVM
    {
        public int IdGestor { get; set; }
        public string Nombre { get; set; }
        public string Usuario { get; set; }
        public string TipoGestor { get; set; }
    }
}
