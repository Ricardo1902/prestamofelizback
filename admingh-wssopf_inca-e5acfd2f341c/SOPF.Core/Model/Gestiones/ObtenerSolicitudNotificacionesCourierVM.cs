﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class ObtenerSolicitudNotificacionesCourierVM
    {
        public int IdSolicitudNotificacion { get; set; }
        public int SolicitudId { get; set; }
        public string NombreCliente { get; set; }
        public string Notificacion { get; set; }
        public string Destino { get; set; }
        public string UsuarioRegistroNotificacion { get; set; }
        public DateTime FechaRegistroNotificacion { get; set; }
        public bool Error { get; set; }
        public string Mensaje { get; set; }
    }
}
