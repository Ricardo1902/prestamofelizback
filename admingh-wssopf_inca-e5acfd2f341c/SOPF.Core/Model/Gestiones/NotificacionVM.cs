﻿using System;

namespace SOPF.Core.Model.Gestiones
{
    public class NotificacionVM
    {
        public int IdNotificacion { get; set; }
        public int IdTipoNotificacion { get; set; }
        public string TipoNotificacion { get; set; }
        public string DescripcionTipoNotificacion { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string NombreArchivo { get; set; }
        public string Clave { get; set; }
        public int Orden { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Actual { get; set; }
    }
}
