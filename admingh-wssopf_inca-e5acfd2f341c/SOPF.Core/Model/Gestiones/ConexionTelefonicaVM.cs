﻿using SOPF.Core.Model.Sistema;

namespace SOPF.Core.Model.Gestiones
{
    public class ConexionTelefonicaVM
    {
        public int IdConexionSip { get; set; }
        public string Servidor { get; set; }
        public int Puerto { get; set; }
        public int Extension { get; set; }
        public string Password { get; set; }

        public ConexionSdkVM ConexionSdk { get; set; }
    }
}
