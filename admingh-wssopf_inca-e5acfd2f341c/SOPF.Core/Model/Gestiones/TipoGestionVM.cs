﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Gestiones
{
    public class TipoGestionVM
    {
        public int IdTipoGestion { get; set; }
        public string Tipo { get; set; }
        public string Clave { get; set; }
        public bool? GeneraRecordatorio { get; set; }

        public List<GestionLayoutArchivoVM> LayoutDomiciliacion { get; set; }
    }
}
