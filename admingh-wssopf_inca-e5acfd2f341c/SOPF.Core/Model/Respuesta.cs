﻿namespace SOPF.Core.Model
{
    public class Respuesta
    {
        public bool Error { get; set; }
        public int StatusCode { get; set; }
        public string CodigoError { get; set; }
        public string MensajeOperacion { get; set; }
        public string MensajeErrorException { get; set; }
        public int TotalRegistros { get; set; }

        public Respuesta()
        {
            Error = false;
            StatusCode = 200;
        }
    }
}
