﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class Sel_DestinoResult
    {
        public int IdDestino { get; set; }
        public string DestinoNotificacion { get; set; }
    }
}
