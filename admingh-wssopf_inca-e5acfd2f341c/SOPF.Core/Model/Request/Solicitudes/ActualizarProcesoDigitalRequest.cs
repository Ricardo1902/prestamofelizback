﻿using SOPF.Core.Solicitudes;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ActualizarProcesoDigitalRequest
    {
        public int IdSolicitudProcesoDigital { get; set; }
        //public int IdEstatusValidacion { get; set; }
        public EstatusProcesoDigital EstatusProcesoDigital { get; set; }
    }
}
