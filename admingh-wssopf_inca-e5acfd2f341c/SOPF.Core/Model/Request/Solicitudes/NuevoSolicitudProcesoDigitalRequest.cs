﻿using SOPF.Core.Solicitudes;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class NuevoSolicitudProcesoDigitalRequest
    {
        public int IdSolicitud { get; set; }
        public int IdUsuario { get; set; }
        public int IdProceso { get; set; }
        public Proceso Proceso { get; set; }
    }
}
