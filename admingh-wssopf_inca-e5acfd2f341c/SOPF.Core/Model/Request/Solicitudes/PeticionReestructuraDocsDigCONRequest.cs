﻿using System.Xml.Linq;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class PeticionReestructuraDocsDigCONRequest
    {
        public XElement IdEstatus { get; set; }
        public int? IdSolicitud { get; set; }
        public int? Pagina { get; set; }
        public int? RegistrosPagina { get; set; }
    }
}
