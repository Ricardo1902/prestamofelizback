﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ValidarPeticionReestructuraRequest : Peticion
    {
        public string Accion { get; set; }
        public int IdPeticionReestructura { get; set; }
        public int IdSolicitudNuevo { get; set; }
        public string Comentario { get; set; }
    }
}
