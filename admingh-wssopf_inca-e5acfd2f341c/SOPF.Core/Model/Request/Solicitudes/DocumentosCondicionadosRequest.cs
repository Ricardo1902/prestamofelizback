﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class DocumentosCondicionadosRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
    }
}
