﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class CargarBuzonCancelacionRequest
    {
        public int IdEstatus { get; set; }
        public int IdUsuario { get; set; }
    }
}
