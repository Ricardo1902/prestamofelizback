﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class AltaSolicitudFirmaDigitalRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitudProcesoDigital { get; set; }
        public int IdSolicitud { get; set; }
        public string ClaveEnvio { get; set; }
    }
}
