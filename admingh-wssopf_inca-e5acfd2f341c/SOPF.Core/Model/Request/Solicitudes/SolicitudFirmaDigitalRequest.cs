﻿using System;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class SolicitudFirmaDigitalRequest
    {
        public int IdUsuario { get; set; }
        public int? IdSolicitud { get; set; }
        public int? IdEstatus { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public int? Pagina { get; set; }
        public int? RegistrosPagina { get; set; }
        public int? TotalRegistros { get; set; }
    }
}
