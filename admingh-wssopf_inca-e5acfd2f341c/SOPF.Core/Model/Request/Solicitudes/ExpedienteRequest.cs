﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ExpedienteRequest : Peticion
    {
        public int IdSolicitud { get; set; }
        public int? IdDocumento { get; set; }
        public bool ConArchivos { get; set; }
    }
}
