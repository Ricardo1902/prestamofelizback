﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ValidarPruebaVidaRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
        public int IdPeticion { get; set; }
        public bool Aprobado { get; set; }
        public string Mensaje { get; set; }
    }
}
