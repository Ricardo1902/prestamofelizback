﻿using SOPF.Core.Solicitudes;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ReiniciarProcesoRequest : Peticion
    {
        public int IdSolicitudProcesoDigital { get; set; }
        public int IdSolicitud { get; set; }
        public string Comentario { get; set; }
        public bool InicioAutomatico { get; set; }
        public bool ReinicioManual { get; set; }
        public bool ReinicioInvidual { get; set; }
    }
}
