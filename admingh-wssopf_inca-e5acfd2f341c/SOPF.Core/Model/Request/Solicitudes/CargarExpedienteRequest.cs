﻿using SOPF.Core.Model.Solicitudes;
using System.Collections.Generic;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class CargarExpedienteRequest : Peticion
    {
        public int IdSolicitud { get; set; }
        public List<DocumentoVM> Documentos { get; set; }
    }
}
