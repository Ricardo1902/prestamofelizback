﻿namespace SOPF.Core.Model.Solicitudes
{
    public class ReestructuraVM
    {
        public string NombreCliente { get; set; }
        public string TipoCredito { get; set; }
        public int IdSolicitud { get; set; }
        public decimal ImporteCredito { get; set; }
        public int PlazoActual { get; set; }
        public decimal Condonado { get; set; }
        public int Vencidas { get; set; }
        public int NuevoPlazo { get; set; }
        public decimal NuevaCuota { get; set; }
    }
}
