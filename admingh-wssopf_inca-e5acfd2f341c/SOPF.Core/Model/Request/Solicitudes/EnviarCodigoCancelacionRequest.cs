﻿using System;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class EnviarCodigoCancelacionRequest
    {
        public int IdUsuario { get; set; }
        public int IdProceso { get; set; }
        public string Codigo { get; set; }
        public DateTime FechaLimitePago { get; set; }
    }
}
