﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class VerificarSolicitudDocDigitalRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
    }
}
