﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class SolicitudDocDigitalesRequest
    {
        public int IdSolicitud { get; set; }
        public bool? Activo { get; set; }
        public int IdProceso { get; set; }
    }
}
