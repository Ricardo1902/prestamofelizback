﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class EliminarDocumentoArchivoRequest
    {
        public int IdUsuario { get; set; }
        public int IdDocumentoArchivo { get; set; }
        public string Comentario { get; set; }
    }
}
