﻿using SOPF.Core.Solicitudes;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class EnviarCronogramaRequest : Peticion
    {
        public OrigenEnvioCronograma Origen { get; set; }
        public int IdSolicitud { get; set; }
        public int IdCuenta { get; set; }
        public string CopiaCC { get; set; }
    }
}
