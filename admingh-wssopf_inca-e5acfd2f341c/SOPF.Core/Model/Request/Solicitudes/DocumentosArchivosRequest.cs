﻿using System.Collections.Generic;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class DocumentosArchivosRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
        public List<int> IdsDocumento { get; set; }
        public List<int> IdsDocumentoArchivo { get; set; }
        public bool ConArchivo { get; set; }
    }
}
