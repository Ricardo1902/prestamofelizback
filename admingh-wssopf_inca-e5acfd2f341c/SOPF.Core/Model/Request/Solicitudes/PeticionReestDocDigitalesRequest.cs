﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class PeticionReestDocDigitalesRequest : Peticion
    {
        public int[] EstatusPeticionReest { get; set; }
        public int? IdSolicitud { get; set; }
        public int? Pagina { get; set; }
        public int? RegistrosPagina { get; set; }
    }
}
