﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ReiniciarProcesoDigitalPetReestRequest : Peticion
    {
        public int IdSolicitud { get; set; }
        public int IdSolicitudPruebaVida { get; set; }
        public int IdSolicitudFirmaDigital { get; set; }
    }
}
