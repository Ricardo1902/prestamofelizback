﻿using SOPF.Core.Model.Request.Lleida;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class CargarDocumentosRequest : ActualizarFirmaRequest
    {
        public int IdSolicitud { get; set; }
        public int IdSolicitudFirmaDigital { get; set; }
    }
}
