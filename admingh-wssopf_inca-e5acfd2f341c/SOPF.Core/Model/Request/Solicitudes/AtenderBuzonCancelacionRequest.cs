﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class AtenderBuzonCancelacionRequest
    {
        public int IdUsuario { get; set; }
        public int IdProceso { get; set; }
    }
}
