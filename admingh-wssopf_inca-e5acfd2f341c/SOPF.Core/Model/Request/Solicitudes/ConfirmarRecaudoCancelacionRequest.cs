﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ConfirmarRecaudoCancelacionRequest
    {
        public int IdUsuario { get; set; }
        public int IdProceso { get; set; }
        public string Comentario { get; set; }
    }
}
