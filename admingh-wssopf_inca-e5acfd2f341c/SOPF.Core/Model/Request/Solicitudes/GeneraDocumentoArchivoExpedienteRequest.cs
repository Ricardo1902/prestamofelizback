﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class GeneraDocumentoArchivoExpedienteRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
    }
}
