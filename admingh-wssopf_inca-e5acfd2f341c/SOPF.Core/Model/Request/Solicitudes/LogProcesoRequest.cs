﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class LogProcesoRequest : Peticion
    {
        public int IdSolicitud { get; set; }
        public int IdProceso { get; set; }
    }
}
