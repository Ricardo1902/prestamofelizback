﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ReiniciarPruebaVidaRequest
    {
        public int IdUsuario { get; set; }
        public string Comentario { get; set; }
        public int IdSolicitudPruebaVida { get; set; }
    }
}
