﻿using System;
using System.Security.Policy;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ActualizarSolicitudFirmaDigitalRequest
    {
        public int IdSolicitudFirmaDigital { get; set; }
        public int IdEstatus { get; set; }
        public int CodigoEstado { get; set; }
        public string Mensaje { get; set; }
        public int IdPeticion { get; set; }
        public string SignatureId { get; set; }
        public DateTime? FechaFirmado { get; set; }
    }
}
