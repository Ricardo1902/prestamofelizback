﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ExisteCambiosDatosDocumentosRequest
    {
        public int IdSolicitud { get; set; }
        public int IdUsuario { get; set; }
        public int IdProceso { get; set; }
    }
}
