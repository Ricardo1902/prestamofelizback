﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class GenerarSolicitudFirmaDigitalRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
        public int IdSolicitudProcesoDigital { get; set; }
        public int IdSolicitudFirmaDigital { get; set; }
        public string ClaveEnvio { get; set; }
        public bool ValidarCambios { get; set; }
    }
}
