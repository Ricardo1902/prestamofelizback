﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class InicarSolicitudProcesoDigitalRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
        public bool Reactivar { get; set; }
        public int IdProceso { get; set; }
    }
}
