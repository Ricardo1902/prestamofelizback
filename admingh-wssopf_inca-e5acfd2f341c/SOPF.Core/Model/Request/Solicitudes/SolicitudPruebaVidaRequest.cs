﻿using System;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class SolicitudPruebaVidaRequest
    {
        public int IdUsuario { get; set; }
        public int? IdSolicitud { get; set; }
        public bool? SesionRealizada { get; set; }
        public bool? ValidacionCorrecta { get; set; }
        public bool? Aprobado { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public int? Pagina { get; set; }
        public int? RegistrosPagina { get; set; }
        public int? TotalRegistros { get; set; }
    }
}
