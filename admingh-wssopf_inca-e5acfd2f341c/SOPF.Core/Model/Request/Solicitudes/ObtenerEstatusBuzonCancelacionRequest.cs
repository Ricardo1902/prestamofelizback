﻿namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ObtenerEstatusBuzonCancelacionRequest
    {
        public string Estatus { get; set; }
    }
}
