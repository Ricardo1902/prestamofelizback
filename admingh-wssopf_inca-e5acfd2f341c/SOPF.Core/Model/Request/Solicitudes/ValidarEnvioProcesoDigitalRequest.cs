﻿using SOPF.Core.Model.Solicitudes;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ValidarEnvioProcesoDigitalRequest
    {
        public int IdUsuario { get; set; }
        public string ClaveEnvio { get; set; }
        public SolicitudVM Solicitud { get; set; }
        public SolicitudProcesoDigitalVM ProcesoDigital { get; set; }
    }
}
