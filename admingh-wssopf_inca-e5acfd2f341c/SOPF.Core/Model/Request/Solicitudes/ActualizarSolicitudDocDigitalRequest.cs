﻿using iText.Layout.Element;

namespace SOPF.Core.Model.Request.Solicitudes
{
    public class ActualizarSolicitudDocDigitalRequest
    {
        public int IdSolicitudDocDigital { get; set; }
        public string Contenido { get; set; }
    }
}
