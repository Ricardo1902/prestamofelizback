﻿using Newtonsoft.Json;
using SOPF.Core.Model.Lleida.DosFA;

namespace SOPF.Core.Model.Request.WsPfLleida.DosFA
{
    public class EnviarRequest
    {
        [JsonProperty("clavePeticion", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ClavePeticion { get; set; }
        [JsonProperty("tiempoVigencia")]
        public int TiempoVigencia { get; set; }
        [JsonProperty("formato")]
        public string Formato { get; set; }
        [JsonProperty("longitud")]
        public int Longitud { get; set; }
        [JsonProperty("maxIntentos")]
        public int MaxIntentos { get; set; }
        [JsonProperty("certificar")]
        public string Certificar { get; set; }
        [JsonProperty("razonSocialCertificar")]
        public string RazonSocialCertificar { get; set; }
        [JsonProperty("numFiscalCertificar")]
        public string NumFiscalCertificar { get; set; }
        [JsonProperty("correo")]
        public CorreoVM Correo { get; set; }
        [JsonProperty("sms")]
        public SmsVM Sms { get; set; }
    }
}
