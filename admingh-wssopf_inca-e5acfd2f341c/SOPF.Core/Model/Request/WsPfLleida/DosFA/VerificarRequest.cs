﻿using Newtonsoft.Json;

namespace SOPF.Core.Model.Request.WsPfLleida.DosFA
{
    public class VerificarRequest
    {
        [JsonProperty("idPeticion")]
        public int IdPeticion { get; set; }
        [JsonProperty("otp")]
        public string Otp { get; set; }
    }
}
