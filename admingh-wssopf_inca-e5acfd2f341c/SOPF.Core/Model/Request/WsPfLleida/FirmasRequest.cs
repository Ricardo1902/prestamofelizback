﻿using System.Runtime.Serialization;

namespace SOPF.Core.Model.Request.WsPfLleida
{
    [DataContract]
    public class FirmasRequest
    {
        [DataMember(Name = "start_date", EmitDefaultValue = false)]
        public long StartDate { get; set; }
        [DataMember(Name = "end_date", EmitDefaultValue = false)]
        public long EndDate { get; set; }
        [DataMember(Name = "signature_id", EmitDefaultValue = false)]
        public long SignatureId { get; set; }
        [DataMember(Name = "signature_status", EmitDefaultValue = false)]
        public string SignatureStatus { get; set; }
    }
}
