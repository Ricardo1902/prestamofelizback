﻿using System.Runtime.Serialization;

namespace SOPF.Core.Model.Request.WsPfLleida
{
    [DataContract]
    public class NotificacionRequest
    {
        [DataMember(Name = "signature_id", EmitDefaultValue = false)]
        public long SignatureId { get; set; }
        [DataMember(Name = "signatory_id", EmitDefaultValue = false)]
        public long SignatoryId { get; set; }
        [DataMember(Name = "contract_id", EmitDefaultValue = false)]
        public string ContractId { get; set; }
        [DataMember(Name = "status", EmitDefaultValue = false)]
        public string Status { get; set; }
        [DataMember(Name = "status_date", EmitDefaultValue = false)]
        public long StatusDate { get; set; }
    }
}
