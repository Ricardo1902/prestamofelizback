﻿namespace SOPF.Core.Model.Request.WsPfLleida
{
    public class EstatusVideoRequest
    {
        public int IdPeticion { get; set; }
        public string ClavePeticion { get; set; }
        public bool IncluirArchivos { get; set; }
    }
}
