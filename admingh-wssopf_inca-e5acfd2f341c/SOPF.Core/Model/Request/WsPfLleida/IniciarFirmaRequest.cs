﻿using SOPF.Core.Model.WsPfLleida;
using System.Collections.Generic;

namespace SOPF.Core.Model.Request.WsPfLleida
{
    public class IniciarFirmaRequest
    {
        public int IdPlantilla { get; set; }
        public string ClaveDocumento { get; set; }
        public List<OrdenFirmaVM> OrdenFirma { get; set; }
        public List<ArchivoVM> Archivos { get; set; }
    }
}
