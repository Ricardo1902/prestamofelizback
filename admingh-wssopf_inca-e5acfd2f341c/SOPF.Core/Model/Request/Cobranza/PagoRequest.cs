﻿using System.Runtime.Serialization;
namespace SOPF.Core.Model.Request.Cobranza
{
    public class PagoRequest
    {
        public enum Moneda
        {

            /// <summary>
            /// Enum PEN for value: PEN
            /// </summary>
            [EnumMember(Value = "PEN")]
            PEN = 1,

            /// <summary>
            /// Enum USD for value: USD
            /// </summary>
            [EnumMember(Value = "USD")]
            USD = 2
        }

        
        public int IdPagoMasivoDetalle { get; set; }
        public int IdUsuario { get; set; }
        public string IdProducto { get; set; }
        public decimal Monto { get; set; }
        public Moneda TipoMonena { get; set; }
        public string NumeroTarjeta { get; set; }
        public int MesExpiracion { get; set; }
        public int AnioExpiracion { get; set; }
        public string NombreTarjetahabiente { get; set; }
        public bool ValidaCampoStatusCodeBlacklist { get; set; }
        public bool ValidaCampoVisaAuthEnab { get; set; }
        public int IdUsuarioSistemaActualiza { get; set; }
    }
}
