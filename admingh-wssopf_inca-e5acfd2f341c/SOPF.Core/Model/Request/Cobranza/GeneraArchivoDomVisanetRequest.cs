﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class GeneraArchivoDomVisanetRequest
    {
        public int IdUsuario { get; set; }
        public List<LayoutVisanetVM> MandoCobro { get; set; }
    }
}
