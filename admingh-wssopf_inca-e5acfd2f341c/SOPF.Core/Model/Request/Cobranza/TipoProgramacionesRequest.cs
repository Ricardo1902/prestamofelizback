﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class TipoProgramacionesRequest
    {
        public int IdUsuario { get; set; }
        public string TipoProgramacion { get; set; }
        public bool? Activo { get; set; }
    }
}
