﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class ExcluirCuentasRequest
    {
        public int IdUsuario { get; set; }
        public List<CuentaExcluyeDomiciliacionVM> CuentasExcluir { get; set; }
    }
}
