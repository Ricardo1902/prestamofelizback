﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class MotivosExclusionRequest
    {
        public int IdUsuario { get; set; }
        public bool Activo { get; set; }
    }
}
