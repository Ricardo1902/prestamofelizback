﻿using System.Collections.Generic;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class ProgramacionReintentarMandoCobroRequest
    {
        public int IdUsuario { get; set; }
        public int IdProgramacionDetallePro { get; set; }
        public List<int> IdPagosMasivos { get; set; }
    }
}
