﻿using SOPF.Core.DataAccess;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class Cobranza_SP_PlantillaEnvioCobroPROResponse : BaseResponseSotreFunction
    {
        public int IdPlantillaEnvioCobro { get; set; }
    }
}
