﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class SuscripcionKushkiRequest
    {
        public string token { get; set; }
        public string planName { get; set; }
        public string fullResponse { get; set; }
        public string periodicity { get; set; }
        public Montos amount { get; set; }
        public string startDate { get; set; }
        public string language { get; set; }
        public Detalles contactDetails { get; set; }
        public SuscripcionKushkiRequest()
        {
            amount = new Montos();
            contactDetails = new Detalles();
        }
        public class Montos
        {
            public decimal subtotalIva { get; set; }
            public decimal subtotalIva0 { get; set; }
            public decimal iva { get; set; }
            public decimal ice { get; set; }
            public string currency { get; set; }
        }
        public class Detalles
        {
            public string documentType { get; set; }
            public string documentNumber { get; set; }
            public string email { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string phoneNumber { get; set; }
        }
    }
}
