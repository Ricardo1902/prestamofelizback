﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class GeneraDevolucionRequest
    {
        public int idUsuario { get; set; }
        public string nombreArchivo { get; set; }
        public byte[] contenidoArchivo { get; set; }
    }
}
