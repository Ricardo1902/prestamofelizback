﻿//Creado por: HefestoGenerator - SAVIED
namespace SOPF.Core.Model.Request.Cobranza
{
    public class SP_ProcesarGrupoPRORequest
    {
        public int IdGrupoLayout { get; set; }
        public int IdUsuario { get; set; }
        public bool EsAutomatico { get; set; }
        public int IdLayoutGenerado { get; set; }
    }
}
