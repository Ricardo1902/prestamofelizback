﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class ProgramacionesProximasRequest
    {
        public int IdUsuario { get; set; }
        public int MinutosProximos { get; set; }
        public int HorasProximas { get; set; }
    }
}
