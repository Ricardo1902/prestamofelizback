﻿using System.Runtime.Serialization;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class GeneraPagoMavisokushkiRequest
    {

        public int IdUsuario { get; set; }
        public string cvv { get; set; }
        public string IdProducto { get; set; }
        public double Monto { get; set; }
        public int TipoMoneda { get; set; }
        public string Moneda { get; set; }
        public string NumeroTarjeta { get; set; }
        public string MesExpiracion { get; set; }
        public string AnioExpiracion { get; set; }
        public string NombreTarjetahabiente { get; set; }
        public string fecha { get; set; }
        public string TipoDocumento { get; set; }
        public string Documento { get; set; }
        public string email { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Token { get; set; }
        public string subscriptionId { get; set; }
        public int IdPagoMasivo { get; set; }

        public string mensaje { get; set; }
    }
}
