﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class ProgramacionGeneraMandoCobroRequest
    {
        public int IdProgramacionDetallePro { get; set; }
        public int IdUsuario { get; set; }
    }
}
