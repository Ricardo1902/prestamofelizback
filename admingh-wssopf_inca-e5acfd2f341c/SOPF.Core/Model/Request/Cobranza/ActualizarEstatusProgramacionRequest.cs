﻿using SOPF.Core.Model.Cobranza;
using System;
using System.Collections.Generic;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class ActualizarEstatusProgramacionRequest
    {
        public int IdUsuario { get; set; }
        public List<ProgramacionDomiciliacionVM> Programaciones { get; set; }
    }
}
