﻿using System.Collections.Generic;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class PagoMasivoEstatusEnviosRequest
    {
        public int IdUsuario { get; set; }
        public List<int> IdPagosMasivos { get; set; }
    }
}
