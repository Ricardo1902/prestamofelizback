﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class ActualizaProgramacionDetalleProRequest
    {
        public int IdUsuario { get; set; }
        public int IdProgramacionDetallePro { get; set; }
        public int IdProgramacionEstatus { get; set; }
        public string Mensaje { get; set; }
    }
}
