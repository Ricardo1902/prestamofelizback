﻿using System;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class PagosMasivosKushkiRequest
    {
        public string NombreArchivo { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
    }
}
