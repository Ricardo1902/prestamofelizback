﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class DescargaPeticionDomiciliacionRequest : Peticion
    {
        public int IdPeticionDomiciliacion { get; set; }
    }
}
