﻿using System;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class RegistrarProgramacionDetalleProRequest
    {
        public int IdProgramacionDetalle { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public int IdIntervalo { get; set; }
        public int ValorIntervalo { get; set; }
        public DateTime FechaEjecucionMax { get; set; }
        public int IdProgramacionEstatus { get; set; }
    }
}
