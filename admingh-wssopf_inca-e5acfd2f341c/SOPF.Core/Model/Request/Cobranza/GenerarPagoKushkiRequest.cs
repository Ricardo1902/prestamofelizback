﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class GenerarPagoKushkiRequest 
    {
        public int IdUsuario { get; set; }
        public int NumeroDatos { get; set; }
        public int NumeroDatosSE { get; set; }
        public int NumeroDatosCE { get; set; }
        public string NombreDocumento { get; set; }
        public string[][] pagos { get; set; }
        public string[][] pagosErr { get; set; }
    }
}
