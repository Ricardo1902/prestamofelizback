﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class PagarKushkiRequest
    {
        public string fullResponse { get; set; }
        public Montos amount { get; set; }
        public string language { get; set; }

        public PagarKushkiRequest()
        {
            amount = new Montos();
        }
        public class Montos
        {
            public double subtotalIva { get; set; }
            public double subtotalIva0 { get; set; }
            public double iva { get; set; }
            public string currency { get; set; }
        }
    }
}
