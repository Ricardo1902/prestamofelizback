﻿using System.Runtime.Serialization;
namespace SOPF.Core.Model.Request.Cobranza
{
    public class VerificacionRUCRequest
    {
              
        public string RUC { get; set; }
        public int IdUsuario { get; set; }
        public string NombreEmp { get; set; }
        public string Estado { get; set; }
        public string TipoEmp { get; set; }
    }
}
