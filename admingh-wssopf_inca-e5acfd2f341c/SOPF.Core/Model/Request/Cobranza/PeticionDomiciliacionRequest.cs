﻿using SOPF.Core.Model.Cobranza;
using System.Collections.Generic;

namespace SOPF.Core.Model.Response.Cobranza
{
    public class PeticionDomiciliacionRequest : Peticion
    {
        public string OrigenDomiciliacion { get; set; }
        public int IdLayuotArchivo { get; set; }
        public List<PeticionDomiciliacionVM> SolicitudesDomiciliar { get; set; }
    }
}
