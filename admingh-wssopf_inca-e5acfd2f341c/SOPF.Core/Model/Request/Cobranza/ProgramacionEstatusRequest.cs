﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class ProgramacionEstatusRequest
    {
        public int IdUsuario { get; set; }
        public string ClaveProgramacion { get; set; }
    }
}
