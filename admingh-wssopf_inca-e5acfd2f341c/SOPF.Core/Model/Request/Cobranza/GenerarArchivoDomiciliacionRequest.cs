﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class GenerarArchivoDomiciliacionRequest
    {
        public int IdUsuario { get; set; }
        public int IdPlantillaEnvioCobro { get; set; }
    }
}
