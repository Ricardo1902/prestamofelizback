﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class CuentasExcluidasRequest
    {
        public int IdUsuario { get; set; }
        public int? IdSolicitud { get; set; }
        public int? Pagina { get; set; }
        public int? RegistrosPagina { get; set; }
    }
}
