﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class GeneraPagoMavisoRequest
    {
        public int IdUsuario { get; set; }
        public string NombreArchivo { get; set; }
        public byte[] ContenidoArchivo { get; set; }
    }
}
