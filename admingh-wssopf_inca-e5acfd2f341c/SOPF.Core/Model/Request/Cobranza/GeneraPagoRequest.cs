﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class GeneraPagoRequest
    {
        public int IdUsuario { get; set; }
        public string Cuenta { get; set; }
        public decimal Monto { get; set; }
        public string NumeroTarjeta { get; set; }
        public int MesExpiracion { get; set; }
        public int AnioExpiracion { get; set; }
    }
}
