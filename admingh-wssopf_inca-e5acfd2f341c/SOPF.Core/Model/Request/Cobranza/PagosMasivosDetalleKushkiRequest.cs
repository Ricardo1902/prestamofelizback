﻿using System;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class PagosMasivosDetalleKushkiRequest
    {
        public int IdPagoMasivo { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
    }
}
