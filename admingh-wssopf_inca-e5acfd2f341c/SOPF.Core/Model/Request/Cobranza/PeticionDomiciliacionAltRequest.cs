﻿//Creado por: HefestoGenerator - SAVIED
using System.Xml.Linq;

namespace SOPF.Core.Model.Request.Cobranza
{
    public class PeticionDomiciliacionAltRequest
    {
        public int IdUsuario { get; set; }
        public string OrigenDomiciliacion { get; set; }
        public int IdLayuotArchivo { get; set; }
        public XElement Solicitudes { get; set; }
    }
}
