﻿namespace SOPF.Core.Model.Request.Cobranza
{
    public class GenerarPlantillaEnvioCobroRequest
    {
        public int IdUsuario { get; set; }
        public int IdPlantilla { get; set; }
    }
}
