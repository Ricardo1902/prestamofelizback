﻿//=================== Class MesaControl.SP_DocumentosObservarMCACTRequest ===================
//Creado por: HefestoGenerator - SAVIED
namespace SOPF.Core.Model.Request.MesaControl
{
    public class SP_DocumentosObservarMCACTRequest
    {
        public int IdSolicitud { get; set; }
        public long? NumeroTransaccion { get; set; }
        public long? Transaccion { get; set; }
        public int? Sy_Paso { get; set; }
        public int? UsuarioId { get; set; }
        public string IP { get; set; }
        public int? ObjPadreId { get; set; }
    }
}