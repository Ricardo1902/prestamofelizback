﻿namespace SOPF.Core.Model.Request.Sistema
{
    public class TipoNotificacionRequest : Peticion
    {
        public bool? Activo { get; set; }
        public string Clave { get; set; }
    }
}
