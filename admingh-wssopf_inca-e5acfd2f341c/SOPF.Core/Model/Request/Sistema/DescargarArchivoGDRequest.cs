﻿namespace SOPF.Core.Model.Request.Sistema
{
    public class DescargarArchivoGDRequest
    {
        public string IdArchivoGD { get; set; }
        public int IdUsuario { get; set; }
        public bool RegresaURL { get; set; }
    }
}
