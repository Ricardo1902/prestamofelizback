﻿namespace SOPF.Core.Model.Request.Sistema
{
    public class AltaNotificacionRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
        public int IdTipoNotificacion { get; set; }
    }
}
