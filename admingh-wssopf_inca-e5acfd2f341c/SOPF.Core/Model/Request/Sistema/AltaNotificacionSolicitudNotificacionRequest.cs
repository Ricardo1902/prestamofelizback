﻿using System;

namespace SOPF.Core.Model.Request.Sistema
{
    public class AltaNotificacionSolicitudNotificacionRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
        public int IdTipoNotificacion { get; set; }
        public string Clave { get; set; }
        public DateTime? FechaHoraCitacion { get; set; }
        public decimal? DescuentoDeuda { get; set; }
        public DateTime? FechaVigencia { get; set; }
        public int IdDestino { get; set; }
        public bool Courier { get; set; }
        public bool Actualizar { get; set; }
    }
}
