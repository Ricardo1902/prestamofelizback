﻿using System;

namespace SOPF.Core.Model.Request.Sistema
{
    public class NuevoRequest : Peticion
    {
        public decimal? IdCliente { get; set; }
        public decimal? IdSolicitud { get; set; }
        public int IdTipoNotificacion { get; set; }
        public string Titulo { get; set; }
        public string CorreoElectronico { get; set; }
        public string NumeroTelefono { get; set; }
        public string NombreCliente { get; set; }
        public int Intentos { get; set; }
        public int MaxIntentos { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public bool Enviado { get; set; }
        public string Url { get; set; }
        public DateTime? FechaEnviar { get; set; }
        public bool? Programado { get; set; }
        public int? IdGestion { get; set; }
        public string CorreoCopia { get; set; }
    }
}
