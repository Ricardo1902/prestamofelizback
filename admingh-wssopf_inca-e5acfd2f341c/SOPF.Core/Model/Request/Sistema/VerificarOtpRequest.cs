﻿namespace SOPF.Core.Model.Request.Sistema
{
    public class VerificarOtpRequest
    {
        public int IdUsuario { get; set; }
        public int IdPeticion { get; set; }
        public string Otp { get; set; }
    }
}
