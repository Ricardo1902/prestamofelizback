﻿namespace SOPF.Core.Model.Request.Sistema
{
    public class ValidaRecursoAccesoRequest : Peticion
    {
        public string Recurso { get; set; }
    }
}
