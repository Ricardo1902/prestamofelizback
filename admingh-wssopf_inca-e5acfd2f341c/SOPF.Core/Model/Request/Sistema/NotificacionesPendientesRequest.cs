﻿namespace SOPF.Core.Model.Request.Sistema
{
    public class NotificacionesPendientesRequest
    {
        public int IdUsuario { get; set; }
        public string TipoNotificacion { get; set; }
        public int IdTipoNotificacion { get; set; }
        public int RegistrosPorPagina { get; set; }
    }
}
