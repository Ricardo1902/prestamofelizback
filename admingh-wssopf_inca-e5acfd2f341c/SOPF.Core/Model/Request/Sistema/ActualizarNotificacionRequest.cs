﻿using System.Data.Entity.Migrations.Model;

namespace SOPF.Core.Model.Request.Sistema
{
    public class ActualizarNotificacionRequest
    {
        public int IdUsuario { get; set; }
        public int IdNotificacion { get; set; }
        public bool Enviado { get; set; }
        public string MensajeError { get; set; }
        public bool? Programado { get; set; }
    }
}
