﻿//Creado por: HefestoGenerator - SAVIED
namespace SOPF.Core.Model.Request.Sistema
{

    public class ValidaRecursoAccesoConRequest
    {
        public int IdUsuario { get; set; }
        public string Recurso { get; set; }
    }
}