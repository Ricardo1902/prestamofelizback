﻿namespace SOPF.Core.Model.Request.Sistema
{
    public class CuentaCorreoRequest
    {
        public int IdUsuario { get; set; }
        public int? IdTipoNotificacion { get; set; }
        public string ClaveTipoNotificacion { get; set; }
        public int? IdCuentaCorreo { get; set; }
        public string Usuario { get; set; }
        public bool? Activo { get; set; }
    }
}
