﻿namespace SOPF.Core.Model.Request.Sistema
{
    public class EnviarOtpRequest
    {
        public int IdUsuario { get; set; }
        public string DNI { get; set; }
        public int IdSolicitud { get; set; }
        public string Celular { get; set; }
        public string Correo { get; set; }
        public MedioEnvioOTP MedioEnvio { get; set; }
        public TipoOTP Tipo { get; set; }
    }
}
