﻿namespace SOPF.Core.Model.Request.Creditos
{
    public class ObtenerSimulacionCreditoRequest
    {
        public int IdSolicitud { get; set; }
        public int IdTipoReestructura { get; set; }
        public int IdProducto { get; set; }
        public decimal Importe { get; set; }
        public int Plazo { get; set; }
        public int TipoSeguro { get; set; }
        public int Banco { get; set; }
        public decimal Interes { get; set; }
        public int? CalcularIntFechas { get; set; }
        public string FechaDesembolso { get; set; }
    }
}
