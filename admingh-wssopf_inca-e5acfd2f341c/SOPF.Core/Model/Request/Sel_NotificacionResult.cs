﻿using System;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class Sel_NotificacionResult
    {
        public int IdNotificacion { get; set; }
        public string Notificacion { get; set; }
        public string NombreArchivo { get; set; }
        public string Clave { get; set; }
        public int Orden { get; set; }
        public DateTime? FechaHoraCitacion { get; set; }
        public decimal? DescuentoDeuda { get; set; }
        public DateTime? FechaVigencia { get; set; }
        public int Destino { get; set; }
        public DateTime? FechaEscaneo { get; set; }
        public string Correo { get; set; }
        public bool EsCourier{ get; set; }
    }
}
