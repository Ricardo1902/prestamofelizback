﻿namespace SOPF.Core.Model.Request.Reportes
{
    public class ImpresionDocumentoRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
        public int Version { get; set; }
        public bool RegresaUrl { get; set; }
    }
}
