﻿using System;

namespace SOPF.Core.Model.Request.Reportes
{
    public class ImpresionTablaAmortizacionRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
        public DateTime? FechaDesembolso { get; set; }
        public int Version { get; set; }
        public bool? EsReimpresion { get; set; }
        public bool? EsSimulado { get; set; }
    }
}
