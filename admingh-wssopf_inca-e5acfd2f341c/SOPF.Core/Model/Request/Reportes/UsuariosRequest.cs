﻿//Creado por: HefestoGenerator - SAVIED
namespace SOPF.Core.Model.Request.Reportes
{
    public class UsuariosRequest
    {
        public int? Id_Usuario { get; set; }
        public int? Perfil_Id { get; set; }
        public int Compania_Id { get; set; }
        public string Usuario { get; set; }
        public string Pass { get; set; }
        public string Nombre { get; set; }
        public int? Accion { get; set; }
        public string Correo { get; set; }
        public int? Token { get; set; }
    }
}
