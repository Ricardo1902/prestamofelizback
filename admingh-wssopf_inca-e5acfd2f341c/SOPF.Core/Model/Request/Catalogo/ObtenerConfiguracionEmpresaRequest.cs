﻿namespace SOPF.Core.Model.Request.Catalogo
{
    public class ObtenerConfiguracionEmpresaRequest
    {
        public string Accion { get; set; }
        public int IdConfiguracion { get; set; }
        public int IdEmpresa { get; set; }
        public int IdOrgano { get; set; }
        public int IdSituacionLaboral { get; set; }
        public int IdRegimenPension { get; set; }
    }
}
