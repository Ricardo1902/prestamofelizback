﻿namespace SOPF.Core.Model.Request.Lleida
{
    public class GeneraPlantillaRequest
    {
        public int IdUsuario { get; set; }
        public int IdPlantilla { get; set; }
    }
}
