﻿namespace SOPF.Core.Model.Request.Lleida
{
    public class CancelarFirmaRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitudFirmaDigital { get; set; }
        public int Accion { get; set; }
    }
}
