﻿namespace SOPF.Core.Model.Request.Lleida
{
    public class ActualizarPeticionEstatusRequest
    {
        public int IdPeticion { get; set; }
        public string ClavePeticion { get; set; }
        public bool IncluirArchivos { get; set; }
        public int IdUsuario { get; set; }
    }
}
