﻿namespace SOPF.Core.Model.Request.Lleida
{
    public class ActivaPlantillaRequest
    {
        public int IdUsuario { get; set; }
        public int IdPlantilla { get; set; }
    }
}
