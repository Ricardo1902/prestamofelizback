﻿namespace SOPF.Core.Model.Request.Lleida
{
    public class ObtenerFirmasRequest
    {
        public long StartDate { get; set; }
        public long EndDate { get; set; }
        public long SignatureId { get; set; }
        public string SignatureStatus { get; set; }
    }
}
