﻿using SOPF.Core.Model.Lleida;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SOPF.Core.Model.Request.Lleida
{
    [XmlRoot("EstatusPruebaVida")]
    public class ActualizarEstatusPruebaVidaRequest
    {
        public int IdPeticion { get; set; }
        public bool Aprobado { get; set; }
        public string Mensaje { get; set; }
        public bool EsperarResultado { get; set; }
        public int IdUsuario { get; set; }
        public List<ArchivoDigitalVM> Archivos { get; set; }
    }
}
