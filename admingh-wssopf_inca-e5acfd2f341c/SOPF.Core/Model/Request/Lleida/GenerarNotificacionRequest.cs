﻿namespace SOPF.Core.Model.Request.Lleida
{
    public class GenerarNotificacionRequest
    {
        public int IdSolicitudFirmaDigital { get; set; }
        public string Estatus { get; set; }
    }
}
