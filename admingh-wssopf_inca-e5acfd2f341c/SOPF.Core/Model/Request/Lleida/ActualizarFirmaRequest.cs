﻿using SOPF.Core.Model.Lleida;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SOPF.Core.Model.Request.Lleida
{
    [Serializable()]
    [XmlRoot("PeticionFirmaVM")]
    public class ActualizarFirmaRequest
    {
        public int IdPeticion { get; set; }
        public string IdSignature { get; set; }
        public int IdEstatus { get; set; }
        public List<DocumentoFirmaVM> Archivos { get; set; }
    }
}
