﻿namespace SOPF.Core.Model.Request.Lleida
{
    public class DesactivaPlantillaRequest
    {
        public int IdUsuario { get; set; }
        public int IdPlantilla { get; set; }
    }
}
