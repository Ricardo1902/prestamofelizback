﻿using System;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class Sel_NotificacionBitacoraResult
    {
        public string Notificacion { get; set; }
        public string Destino { get; set; }
        public string Accion { get; set; }
        public string Usuario { get; set; }
        public DateTime Fecha { get; set; }
    }
}