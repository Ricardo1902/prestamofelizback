﻿namespace SOPF.Core.Model.Request.Cliente
{
    public class BuscarClienteRequest : Peticion
    {
        public int IdCliente { get; set; }
        public int IdSolicitud { get; set; }
        public int IdCuenta { get; set; }
        public string DNI { get; set; }
    }
}
