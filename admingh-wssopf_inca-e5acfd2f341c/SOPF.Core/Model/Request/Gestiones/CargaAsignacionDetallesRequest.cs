﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class CargaAsignacionDetallesRequest : Peticion
    {
        public int IdCargaAsignacion { get; set; }
        public bool? Error { get; set; }
        public int? Pagina { get; set; }
        public int? RegistrosPagina { get; set; }
    }
}
