﻿//Creado por: HefestoGenerator - SAVIED
using System.Xml.Linq;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class CargarAsignacionActRequest
    {
        public int IdUsuario { get; set; }
        public int Accion { get; set; }
        public int IdCargaAsignacion { get; set; }
        public bool? Error { get; set; }
        public string Actividad { get; set; }
        public XElement XmlCarga { get; set; }
    }
}