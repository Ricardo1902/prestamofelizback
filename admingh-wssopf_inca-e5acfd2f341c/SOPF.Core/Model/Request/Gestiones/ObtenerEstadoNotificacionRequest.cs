﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerEstadoNotificacionRequest
    {
        public string Accion { get; set; }
        public int IdSolicitud { get; set; }
        public int IdNotificacion { get; set; }
        public int IdDestino { get; set; }
    }
}
