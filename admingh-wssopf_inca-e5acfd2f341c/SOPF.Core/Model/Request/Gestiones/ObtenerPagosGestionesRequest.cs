﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerPagosGestionesRequest
    {
        public string Accion { get; set; }
        public int IdSolicitud { get; set; }
    }
}