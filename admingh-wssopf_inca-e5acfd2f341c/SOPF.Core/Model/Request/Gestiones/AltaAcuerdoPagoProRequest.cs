﻿using System.Xml.Linq;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class AltaAcuerdoPagoProRequest
    {
        public int IdSolicitud { get; set; }
        public int IdUsuario { get; set; }
        public XElement XmlAcuerdos { get; set; }
        public bool? OperacionCorrecta { get; set; }
        public string MensajeOperacion { get; set; }
        public int? IdAcuerdoPago { get; set; }
    }
}
