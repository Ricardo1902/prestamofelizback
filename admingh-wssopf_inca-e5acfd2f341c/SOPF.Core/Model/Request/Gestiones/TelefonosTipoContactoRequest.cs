﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class TelefonosTipoContactoRequest : Peticion
    {
        public int IdSolicitud { get; set; }
        public int IdTipoContacto { get; set; }
    }
}
