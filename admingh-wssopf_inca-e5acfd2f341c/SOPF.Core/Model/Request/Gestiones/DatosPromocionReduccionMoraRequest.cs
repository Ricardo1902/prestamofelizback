﻿using System;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class DatosPromocionReduccionMoraRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
        public string Clave { get; set; }
        public int IdDestino { get; set; }
        public decimal? DescuentoDeuda { get; set; }
        public DateTime FechaVigencia { get; set; }
        public bool Actualizar { get; set; }
        public bool Correo { get; set; }
        public bool Courier { get; set; }
    }
}