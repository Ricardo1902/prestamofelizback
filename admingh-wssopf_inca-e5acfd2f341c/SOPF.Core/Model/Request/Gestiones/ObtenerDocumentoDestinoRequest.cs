﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerDocumentoDestinoRequest
    {
        public int IdDocumentoDestino { get; set; }
    }
}
