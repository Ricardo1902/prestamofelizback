﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class CargaAsignacionAsignarProRequest
    {
        public int IdUsuario { get; set; }
        public int IdCargaAsignacion { get; set; }
    }
}