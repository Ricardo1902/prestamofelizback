﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class CargaAsignacionRequest : Peticion
    {
        public int IdCargaAsignacion { get; set; }
    }
}
