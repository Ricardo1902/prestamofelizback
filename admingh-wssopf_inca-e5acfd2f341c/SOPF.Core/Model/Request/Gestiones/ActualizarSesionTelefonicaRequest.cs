﻿using System;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class ActualizarSesionTelefonicaRequest : Peticion
    {
        public int IdExtensionSesion { get; set; }
        public int IdGestion { get; set; }
    }
}
