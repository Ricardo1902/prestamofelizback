﻿using iText.Layout.Element;
using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class ActualizarCargaAsignacionRequest : Peticion
    {
        public int Accion { get; set; }
        public int IdCargaAsignacion { get; set; }
        public bool? Error { get; set; }
        public string Actividad { get; set; }
        public List<CargaAsignacionDetalleVM> Asignaciones { get; set; }
    }
}
