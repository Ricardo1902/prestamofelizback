﻿using System.Collections.Generic;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class ImprimirNotificacionesCourierRequest
    {
        public List<int> Notificaciones { get; set; }
        public int IdUsuario { get; set; }
    }
}