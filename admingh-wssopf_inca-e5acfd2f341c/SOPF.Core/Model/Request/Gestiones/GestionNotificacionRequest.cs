﻿using System;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class GestionNotificacionRequest : Peticion
    {
        public int IdTipoGestion { get; set; }
        public int IdTipoNotificacion { get; set; }
        public int IdGestion { get; set; }
        public DateTime FechaEnviar { get; set; }
        public bool EnviarCliente { get; set; }
    }
}
