﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerDatosClienteGestionRequest
    {
        public string Accion { get; set; }
        public int IdSolicitud { get; set; }
    }
}
