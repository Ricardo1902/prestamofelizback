﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class DatosAvisoPagoRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
        public string Clave { get; set; }
        public int IdDestino { get; set; }
        public bool Actualizar { get; set; }
        public bool Correo { get; set; }
        public bool Courier { get; set; }
    }
}