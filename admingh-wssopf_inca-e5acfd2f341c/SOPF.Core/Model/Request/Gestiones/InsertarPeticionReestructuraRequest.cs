﻿using SOPF.Core.Entities.Solicitudes;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class InsertarPeticionReestructuraRequest
    {
        public PeticionReestructura peticion { get; set; }
        public string ComentarioCapturista { get; set; }
    }
}
