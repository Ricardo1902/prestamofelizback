﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class AltaAcuerdoPagoRequest
    {
        public int IdSolicitud { get; set; }
        public int IdUsuario { get; set; }
        public bool esNuevo { get; set; }
        public List<AcuerdoPagoDetalleVM> Acuerdos { get; set; }
        //public XElement XmlAcuerdos { get; set; }
    }
}
