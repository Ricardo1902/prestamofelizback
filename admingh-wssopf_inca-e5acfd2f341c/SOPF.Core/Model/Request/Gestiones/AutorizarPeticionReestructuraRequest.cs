﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class AutorizarPeticionReestructuraRequest
    {
        public string Accion { get; set; }
        public int Id_PeticionReestructura { get; set; }
        public int AutorizadorUsuario_Id { get; set; }
        public string ComentarioAutorizador { get; set; }
    }
}
