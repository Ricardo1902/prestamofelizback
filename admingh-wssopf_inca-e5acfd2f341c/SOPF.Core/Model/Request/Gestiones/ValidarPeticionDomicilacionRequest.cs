﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ValidarPeticionDomicilacionRequest : Peticion
    {
        public string Origen { get; set; }
        public int IdTipoGestion { get; set; }
        public bool Todo { get; set; }
        public int IdBanco { get; set; }
        public string ClaveEstatus { get; set; }
        public int[] IdGestionDomiciliacion { get; set; }
    }
}
