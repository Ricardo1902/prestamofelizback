﻿//Creado por: HefestoGenerator - SAVIED
using System;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class DevolverLlamadaAltRequest
    {
        public int IdGestion { get; set; }
        public decimal IdCuenta { get; set; }
        public DateTime Fch_DevLlamada { get; set; }
        public decimal IdCliente { get; set; }
        public int IdGestor { get; set; }
        public string Telefono { get; set; }
        public string Tipo { get; set; }
        public string AnexoReferencia { get; set; }
        public string Comentario { get; set; }
    }
}
