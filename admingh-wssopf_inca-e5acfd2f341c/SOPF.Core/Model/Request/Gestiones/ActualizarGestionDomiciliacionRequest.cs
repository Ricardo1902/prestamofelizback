﻿using SOPF.Core.Model.Gestiones;
using System.Collections.Generic;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class ActualizarGestionDomiciliacionRequest : Peticion
    {
        public bool EnviadoDom { get; set; }
        public int IdPeticionDomiciliacion { get; set; }
        public int IdEstatus { get; set; }
        public List<int> IdGestionDomiciliacion { get; set; }
    }
}
