﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerAcuerdoPagoRequest
    {
        public int Accion { get; set; }
        public int IdAcuerdoPago { get; set; }
        public int IdSolicitud { get; set; }
        public int? Pagina { get; set; }
        public int? RegistrosPagina { get; set; }
    }
}
