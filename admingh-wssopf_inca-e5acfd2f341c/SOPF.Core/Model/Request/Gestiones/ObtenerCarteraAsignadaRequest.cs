﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerCarteraAsignadaRequest
    {
        public int Accion { get; set; }
        public int IdUsuario { get; set; }
        public int IdGestor { get; set; }
        public string Busqueda { get; set; }
        public int? GrupoAsignaciones { get; set; }
        public int Pagina { get; set; }
        public int RegistrosPagina { get; set; }
        public bool OrdenAscendente { get; set; }
    }
}
