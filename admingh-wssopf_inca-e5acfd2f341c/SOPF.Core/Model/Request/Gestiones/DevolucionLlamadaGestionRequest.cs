﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class DevolucionLlamadaGestionRequest : Peticion
    {
        public int IdNotificacion { get; set; }
    }
}
