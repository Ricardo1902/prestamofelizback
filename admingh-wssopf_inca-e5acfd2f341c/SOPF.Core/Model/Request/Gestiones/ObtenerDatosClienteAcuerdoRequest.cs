﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerDatosClienteAcuerdoRequest
    {
        public int Accion { get; set; }
        public int IdSolicitud { get; set; }
    }
}
