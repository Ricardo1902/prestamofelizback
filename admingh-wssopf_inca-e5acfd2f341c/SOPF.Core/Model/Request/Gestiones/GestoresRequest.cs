﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class GestoresRequest : Peticion
    {
        public bool? Activo { get; set; }
        public string Tipo { get; set; }
    }
}
