﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ResultadosGestionPeticion : Peticion
    {
        public int IdTipoGestion { get; set; }
        public bool? Estatus { get; set; }
    }
}
