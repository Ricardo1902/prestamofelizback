﻿using System;

namespace SOPF.Core.Model.Request.Reportes
{
    public class ImpresionCitacionPrejudicialRequest
    {
        public int IdUsuario { get; set; }
        public int IdSolicitud { get; set; }
        public string Clave { get; set; }
        public int IdDestino { get; set; }
        public int Version { get; set; }
        public DateTime FechaHoraCitacion { get; set; }
        public bool Actualizar { get; set; }
        public bool Correo { get; set; }
        public bool Courier { get; set; }
    }
}
