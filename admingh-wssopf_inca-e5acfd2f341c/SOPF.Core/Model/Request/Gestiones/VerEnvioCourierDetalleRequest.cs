﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class VerEnvioCourierDetalleRequest
    {
        public int IdEnvioCourier { get; set; }
    }
}
