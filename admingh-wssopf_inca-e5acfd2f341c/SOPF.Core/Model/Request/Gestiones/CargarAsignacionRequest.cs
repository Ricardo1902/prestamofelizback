﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class CargarAsignacionRequest : Peticion
    {
        public string Actividad { get; set; }
        public string NombreArchivo { get; set; }
        public string TipoArchivo { get; set; }
        public string Contenido { get; set; }
        public int Tamanio { get; set; }
    }
}
