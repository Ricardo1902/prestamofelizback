﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerGestionRequest
    {
        public int Accion { get; set; }
        public int IdGestion { get; set; }
        public int IdCuenta { get; set; }
        public int Pagina { get; set; }
        public int RegistrosPagina { get; set; }
    }
}
