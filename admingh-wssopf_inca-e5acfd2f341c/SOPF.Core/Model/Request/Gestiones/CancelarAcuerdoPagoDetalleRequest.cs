﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class CancelarAcuerdoPagoDetalleRequest
    {
        public int Accion { get; set; }
        public int IdAcuerdoPagoDetalle { get; set; }
    }
}
