﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class CargaAsignacionConRequest
    {
        public int IdUsuario { get; set; }
        public int? IdEstatus { get; set; }
        public int? Pagina { get; set; }
        public int? RegistrosPagina { get; set; }
    }
}
