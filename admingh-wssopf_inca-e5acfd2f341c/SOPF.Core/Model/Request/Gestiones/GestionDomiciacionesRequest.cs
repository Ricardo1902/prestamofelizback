﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class GestionDomiciacionesRequest : Peticion
    {
        public int IdTipoGestion { get; set; }
        public int? IdEstatus { get; set; }
        public int? IdBanco { get; set; }
        public int? Pagina { get; set; }
        public int? RegistrosPagina { get; set; }
    }
}
