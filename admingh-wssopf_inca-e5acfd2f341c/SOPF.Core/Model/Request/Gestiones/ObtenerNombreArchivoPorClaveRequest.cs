﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerNombreArchivoPorClaveRequest
    {
        public string Accion { get; set; }
        public string Clave { get; set; }
    }
}
