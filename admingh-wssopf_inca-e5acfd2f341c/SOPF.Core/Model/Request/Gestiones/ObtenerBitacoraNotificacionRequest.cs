﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerBitacoraNotificacionRequest
    {
        public string Accion { get; set; }
        public int IdSolicitud { get; set; }
    }
}
