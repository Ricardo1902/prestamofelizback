﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class PeticionDomicilacionRequest : Peticion
    {
        public int IdTipoGestion { get; set; }
        public bool Todo { get; set; }
        public int IdLayoutArchivo { get; set; }
        public int IdBanco { get; set; }
        public int? IdEstatus { get; set; }
        public int[] IdGestionDomiciliacion { get; set; }
    }
}
