﻿//Creado por: HefestoGenerator - SAVIED
using System.Xml.Linq;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class CargarAsignacionProRequest
    {
        public int IdUsuario { get; set; }
        public XElement XmlCarga { get; set; }
        public bool? OperacionCorrecta { get; set; }
        public string MensajeOperacion { get; set; }
        public int? IdCargaAsignacion { get; set; }
    }
}
