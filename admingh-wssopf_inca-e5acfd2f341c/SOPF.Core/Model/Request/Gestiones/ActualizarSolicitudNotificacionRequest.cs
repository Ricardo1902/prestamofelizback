﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ActualizarSolicitudNotificacionRequest
    {
        public int  IdSolicitudNotificacion { get; set; }
        public int IdUsuarioEscaneo { get; set; }
        public int IdDestino { get; set; }
        public int IdSolNotTipoAccion { get; set; }
    }
}
