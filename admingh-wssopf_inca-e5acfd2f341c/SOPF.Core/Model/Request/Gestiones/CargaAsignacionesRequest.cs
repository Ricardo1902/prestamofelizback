﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class CargaAsignacionesRequest : Peticion
    {
        public int? IdEstatus { get; set; }
        public int? Pagina { get; set; }
        public int? RegistrosPagina { get; set; }
        public int? TotalRegistros { get; set; }
    }
}
