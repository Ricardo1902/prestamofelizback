﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class AsignarCargaAsignacionRequest : Peticion
    {
        public int IdCargaAsignacion { get; set; }
    }
}
