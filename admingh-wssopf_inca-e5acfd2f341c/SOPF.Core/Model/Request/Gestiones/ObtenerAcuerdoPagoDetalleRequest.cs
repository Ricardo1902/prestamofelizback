﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerAcuerdoPagoDetalleRequest
    {
        public int Accion { get; set; }
        public int IdAcuerdoPago { get; set; }
        public int IdAcuerdoPagoDetalle { get; set; }
    }
}
