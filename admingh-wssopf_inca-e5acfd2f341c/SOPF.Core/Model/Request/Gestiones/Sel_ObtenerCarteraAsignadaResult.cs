﻿using System;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class Sel_ObtenerCarteraAsignadaResult
    {
        public int IdCuenta { get; set; }
        public int IdSolicitud { get; set; }
        public int IdAsignacion { get; set; }
        public int IdCliente { get; set; }
        public DateTime? FechaCredito { get; set; }
        public decimal MontoCredito { get; set; }
        public decimal Cuota { get; set; }
        public string Empresa { get; set; }
        public string Cliente { get; set; }
        public string DNI { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Correo { get; set; }
        public string TelefonoOficina { get; set; }
        public string ExtensionOficina { get; set; }
        public string DireccionOficina { get; set; }
        public string NumeroExteriorOficina { get; set; }
        public string NumeroInteriorOficina { get; set; }
        public string DependenciaOficina { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public DateTime? FechaProximoPago { get; set; }
        public int RecibosVencidos { get; set; }
        public decimal SaldoVencido { get; set; }
        public int DiasVencidos { get; set; }
        public string BucketMora { get; set; }
        public string TipoCredito { get; set; }
        public DateTime? FechaUltPago { get; set; }
        public DateTime? FechaUltGestion { get; set; }
        public string ResultadoGestion { get; set; }
        public DateTime? FechaAsignacion { get; set; }
        public string Gestor { get; set; }
        public bool? TieneAcuerdo { get; set; }
        public bool? TienePromesaPago { get; set; }
    }
}
