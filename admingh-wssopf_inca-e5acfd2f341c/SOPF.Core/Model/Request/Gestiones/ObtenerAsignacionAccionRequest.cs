﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerAsignacionAccionRequest
    {
        public int IdUsuario { get; set; }
        public int IdCuenta { get; set; }
    }
}
