﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class ObtenerProductoReestructuraRequest
    {
        public string Accion { get; set; }
        public int IdSolicitud { get; set; }
        public int Plazo { get; set; }
    }
}