﻿using System;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class Sel_ObtenerDatosClienteGestionResult
    {
        public string NombreCliente { get; set; }
        public string DNICliente { get; set; }
        public string DomicilioLegal { get; set; }
        public string Distrito { get; set; }
        public string ReferenciaDomiciliaria { get; set; }
        public string RazonSocialEmpresa { get; set; }
        public string SituacionLaboral { get; set; }
        public string Producto { get; set; }
        public string EjecutivoVenta { get; set; }
        public decimal MontoCredito { get; set; }
        public int Plazo { get; set; }
        public decimal Cuota { get; set; }
        public int? RecibosVencidos { get; set; }
        public int? DiasMora { get; set; }
        public string FlagMora { get; set; }
        public decimal? SaldoVencido { get; set; }
        public string EstatusCliente { get; set; }
        public DateTime? FechaUltimoPago { get; set; }
        public int DiaPago { get; set; }
        public string Banco { get; set; }
        public string CuentaBancaria { get; set; }
        public string NumeroTarjeta { get; set; }
        public int? MesVencimiento { get; set; }
        public int? AnioVencimiento { get; set; }
        public DateTime? FechaUltimoProceso { get; set; }
        public string ProcesoCobro { get; set; }
        public string MensajeBanco { get; set; }
        public int TarjetaActAuto { get; set; }
    }
}
