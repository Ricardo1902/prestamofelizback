﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class CargaAsignacionDetalleConRequest
    {
        public int IdUsuario { get; set; }
        public int IdCargaAsignacion { get; set; }
        public bool? Error { get; set; }
        public int? Pagina { get; set; }
        public int? RegistrosPagina { get; set; }
    }
}
