﻿namespace SOPF.Core.Model.Request.Gestiones
{
    public class CancelarAcuerdoPagoRequest
    {
        public int Accion { get; set; }
        public int IdUsuario { get; set; }
        public int IdAcuerdoPago { get; set; }
        public int IdSolicitud { get; set; }
    }
}
