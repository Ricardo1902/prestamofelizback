﻿using System;

namespace SOPF.Core.Model.Request.Gestiones
{
    public class DevolverLlamadaRequest : Peticion
    {
        public int? IdGestion { get; set; }
        public decimal? IdCuenta { get; set; }
        public DateTime? Fch_DevLlamada { get; set; }
        public int? IdCliente { get; set; }
        public int? IdGestor { get; set; }
        public string Telefono { get; set; }
        public string Tipo { get; set; }
        public string AnexoReferencia { get; set; }
        public string Comentario { get; set; }
        public int[] IdGestoresNotificar { get; set; }
    }
}
