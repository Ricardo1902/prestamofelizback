﻿namespace SOPF.Core.Model.Cobranza
{
    public class LayoutArchivoCampoVM
    {
        public int IdLayoutArchivoCampo { get; set; }
        public int IdLayoutArchivo { get; set; }
        public string DescripcionCampo { get; set; }
        public int IdLayoutArchivoSeccion { get; set; }
        public int IdTipoCampo { get; set; }
        public string EntidadValor { get; set; }
        public int? Longitud { get; set; }
        public int? Enteros { get; set; }
        public int? Decimales { get; set; }
        public bool? IncluyePunto { get; set; }
        public int IdLayoutArchivoAlineacion { get; set; }
        public string Relleno { get; set; }
        public string TextoFijo { get; set; }
        public string FormatoFecha { get; set; }
        public int Orden { get; set; }
        public bool Activo { get; set; }
    }
}
