﻿namespace SOPF.Core.Model.Cobranza
{
    public class PagoFallidoVisanetVM
    {
        public string Solicitud { get; set; }
        public decimal Monto { get; set; }
        public string NoTarjeta { get; set; }
        public int MesExpiracion { get; set; }
        public int AnioExpiracion { get; set; }
    }
}
