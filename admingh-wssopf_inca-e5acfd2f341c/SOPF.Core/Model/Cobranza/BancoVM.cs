﻿namespace SOPF.Core.Model.Cobranza
{
    public class BancoVM
    {
        public short IdBanco { get; set; }
        public string Banco { get; set; }
    }
}
