﻿namespace SOPF.Core.Model.Cobranza
{
    public class TipoProgramacionVM
    {
        public int IdTipoProgramacion { get; set; }
        public string TipoProgramacion { get; set; }
    }
}
