﻿namespace SOPF.Core.Model.Cobranza
{
    public class CuentaExcluyeDomiciliacionVM
    {
        public int IdTipoIngreso { get; set; }
        public int IdOrigenExclusion { get; set; }
        public int IdSolicitud { get; set; }
        public int? IdMotivoExclusionDomiciliacion { get; set; }
        public string Comentario { get; set; }
    }
}
