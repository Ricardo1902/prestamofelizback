﻿using System;

namespace SOPF.Core.Model.Cobranza
{
    public class ProgramacionDomiciliacionVM
    {
        public int IdProgramacionDetalle { get; set; }
        public int IdProgramacion { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaTermino { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraTermino { get; set; }
        public DateTime FechaProcesar { get; set; }
        public int IdIntervalo { get; set; }
        public decimal ValorIntervalo { get; set; }
        public int Intento { get; set; }
        public int Dia { get; set; }
        public int IdEstatusProgramacion { get; set; }
    }
}