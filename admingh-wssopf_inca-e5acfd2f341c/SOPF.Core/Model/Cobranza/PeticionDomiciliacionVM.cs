﻿namespace SOPF.Core.Model.Cobranza
{
    public class PeticionDomiciliacionVM
    {
        public int IdGestionDomiciliacion { get; set; }
        public int IdSolicitud { get; set; }
        public decimal Monto { get; set; }
    }
}
