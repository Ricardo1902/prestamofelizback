﻿namespace SOPF.Core.Model.Cobranza
{
    public class PagoMasivoEstatusVM
    {
        public int IdPagoMasivo { get; set; }
        public int TotalRegistros { get; set; }
        public int TotalProcesados { get; set; }
        public int TotalAprobados { get; set; }
        public int IdEstatusProceso { get; set; }
    }
}
