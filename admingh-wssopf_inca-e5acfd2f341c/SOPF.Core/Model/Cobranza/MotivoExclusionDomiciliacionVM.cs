﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core.Model.Cobranza
{
    public class MotivoExclusionDomiciliacionVM
    {
        public int IdMotivoExclusionDomiciliacion { get; set; }
        public string MotivoExclusion { get; set; }
        public bool Activo { get; set; }
    }
}
