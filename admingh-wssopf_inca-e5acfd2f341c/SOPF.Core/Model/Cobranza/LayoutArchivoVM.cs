﻿using System;
using System.Collections.Generic;

namespace SOPF.Core.Model.Cobranza
{
    public class LayoutArchivoVM
    {
        public int IdLayoutArchivo { get; set; }
        public int IdTipoLayout { get; set; }
        public string NombreLayout { get; set; }
        public int IdTipoArchivo { get; set; }
        public string NombreArchivo { get; set; }
        public string DelimitadorColumna { get; set; }
        public string Extension { get; set; }
        public string TipoContenido { get; set; }
        public int? IdBanco { get; set; }
        public int? Consecutivo { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public bool? Activo { get; set; }
        public string CaracteresRemplazo { get; set; }

        public List<LayoutArchivoCampoVM> Campos { get; set; }

        public LayoutArchivoVM()
        {
            Campos = new List<LayoutArchivoCampoVM>();
        }
    }
}