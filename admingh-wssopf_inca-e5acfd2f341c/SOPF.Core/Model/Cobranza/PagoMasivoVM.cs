﻿using System;

namespace SOPF.Core.Model.Cobranza
{
    public class PagoMasivoVM
    {
        public int IdPagoMasivo { get; set; }
        public string NombreArchivo { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int? TotalRegistros { get; set; }
        public int? TotalProcesado { get; set; }
        public int? TotalAprobados { get; set; }
        public decimal? MontoTotal { get; set; }
        public int IdEstatusProceso { get; set; }
        public DateTime? UltimaActualizacion { get; set; }
        public DateTime? FechaFin { get; set; }
        public int IdUsuarioRegistro { get; set; }
        public bool? Error { get; set; }
        public string Mensaje { get; set; }
        public string EstatusProceso { get; set; }
        public string UsuarioRegistro { get; set; }
    }
}
