﻿namespace SOPF.Core.Model.Cobranza
{
    public class PagoMasivoDetalleVM
    {
        public int IdPagoMasivoDetalle { get; set; }
        public int IdCuenta { get; set; }
        public decimal Monto { get; set; }
        public string NoTarjeta { get; set; }
        public string Estatus { get; set; }
        public string DescripcionAccion { get; set; }
        public string IdTransaccion { get; set; }
        public string CodigoAutorizacion { get; set; }
        public string NumeroRastreo { get; set; }
        public string MontoOperacion { get; set; }
    }
}
