﻿namespace SOPF.Core.Model.Cobranza
{
    public class LayoutVisanetVM
    {
        public int IdSolicitud { get; set; }
        public decimal MontoCobro { get; set; }
        public string NumeroTarjeta { get; set; }
        public int MesExpiracion { get; set; }
        public int AnioExpiracion { get; set; }
    }
}
