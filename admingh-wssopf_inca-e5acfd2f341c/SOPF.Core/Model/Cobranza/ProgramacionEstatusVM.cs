﻿namespace SOPF.Core.Model.Cobranza
{
    public class ProgramacionEstatusVM
    {
        public int IdProgramacionEstatus { get; set; }
        public string ClaveProgramacion { get; set; }
        public string Descripcion { get; set; }
    }
}
