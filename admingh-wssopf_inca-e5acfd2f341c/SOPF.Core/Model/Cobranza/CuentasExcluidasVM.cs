﻿using System;

namespace SOPF.Core.Model.Cobranza
{
    public class CuentasExcluidasVM : CuentaExcluyeDomiciliacionVM
    {
        public int IdCuentaExcluyeDomiciliacion { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public string MotivoExclusionDomiciliacion { get; set; }
    }
}
