﻿namespace SOPF.Core.Model.Cobranza
{
    public class PagoMasivoDetalleKushkiVM
    {
        public int IdPagoMasivoDetalleKushki { get; set; }
        public string IdProducto { get; set; }
        public decimal Monto { get; set; }
        public string NoTarjeta { get; set; }
        public string MesExpiracion { get; set; }
        public string AnioExpiracion { get; set; }
        public string NombreTarjetahabiente { get; set; }
        public string cvv { get; set; }
        public string TipoDocumento { get; set; }
        public string Documento { get; set; }
        public string Estatus { get; set; }
        public string DescripcionAccion { get; set; }
        public string IdTransaccion { get; set; }
        public string email  { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string CodigoAutorizacion { get; set; }
        public string NumeroRastreo { get; set; }
        public string MontoOperacion { get; set; }
    }
}
