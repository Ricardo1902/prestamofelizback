﻿using iText.Kernel.Colors;
using iText.Kernel.Font;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;

namespace SOPF.Core
{
    public static class PdfCelda
    {
        public enum Alineacion { Izquierda, Derecha, Centro, Justificado };
        public enum Color { BLACK, RED, WHITE, PERUBLUE };
        public enum Colorb { BLACK, RED, WHITE, GRAY, PERUBLUE };
        private static float borde = 0.5f;
        private static float bordeS = 0.0f;

        public static Cell NuevaCelda(string valor, PdfFont fuente, float tamaño)
        {
            Cell celda = new Cell();
            if (string.IsNullOrEmpty(valor)) valor = " ";
            Paragraph contenido = new Paragraph(valor);
            contenido.SetFont(fuente);
            contenido.SetFontSize(tamaño);
            celda.Add(contenido);
            return celda;
        }

        public static Cell NuevaCelda(string valor, PdfFont fuente, float tamaño, Alineacion alineacion)
        {
            Cell celda = new Cell();
            if (string.IsNullOrEmpty(valor)) valor = " ";
            Paragraph contenido = new Paragraph(valor);
            contenido.SetFont(fuente);
            contenido.SetFontSize(tamaño);
            if (alineacion == Alineacion.Derecha) contenido.SetTextAlignment(TextAlignment.RIGHT);
            if (alineacion == Alineacion.Izquierda) contenido.SetTextAlignment(TextAlignment.LEFT);
            if (alineacion == Alineacion.Centro) contenido.SetTextAlignment(TextAlignment.CENTER);
            if (alineacion == Alineacion.Justificado) contenido.SetTextAlignment(TextAlignment.JUSTIFIED);
            celda.Add(contenido);
            return celda;
        }

        public static Cell NuevaCelda(string valor, PdfFont fuente, float tamaño, int rowSpan, int colSpan)
        {
            Cell celda = new Cell((rowSpan > 0) ? rowSpan : 0, (colSpan > 0) ? colSpan : 0);
            if (string.IsNullOrEmpty(valor)) valor = " ";
            Paragraph contenido = new Paragraph(valor);
            contenido.SetFont(fuente);
            contenido.SetFontSize(tamaño);
            celda.Add(contenido);
            return celda;
        }

        public static Cell NuevaCelda(string valor, PdfFont fuente, float tamaño, int rowSpan, int colSpan, Alineacion alineacion)
        {
            Cell celda = new Cell((rowSpan > 0) ? rowSpan : 0, (colSpan > 0) ? colSpan : 0);
            if (string.IsNullOrEmpty(valor)) valor = " ";
            Paragraph contenido = new Paragraph(valor);
            contenido.SetFont(fuente);
            contenido.SetFontSize(tamaño);
            if (alineacion == Alineacion.Derecha) contenido.SetTextAlignment(TextAlignment.RIGHT);
            if (alineacion == Alineacion.Izquierda) contenido.SetTextAlignment(TextAlignment.LEFT);
            if (alineacion == Alineacion.Centro) contenido.SetTextAlignment(TextAlignment.CENTER);
            if (alineacion == Alineacion.Justificado) contenido.SetTextAlignment(TextAlignment.JUSTIFIED);
            celda.Add(contenido);
            return celda;
        }

        public static Cell NuevaCelda(string valor, PdfFont fuente, float tamaño,
            bool bordeArriba, bool bordeAbajo, bool bordeIzquiedo, bool bordeDerecho)
        {
            Cell celda = new Cell();
            if (string.IsNullOrEmpty(valor)) valor = " ";
            celda.SetBorder(Border.NO_BORDER);
            if (bordeArriba) celda.SetBorderTop(new SolidBorder(borde));
            if (bordeAbajo) celda.SetBorderBottom(new SolidBorder(borde));
            if (bordeIzquiedo) celda.SetBorderLeft(new SolidBorder(borde));
            if (bordeDerecho) celda.SetBorderRight(new SolidBorder(borde));
            Paragraph contenido = new Paragraph(valor);
            contenido.SetFont(fuente);
            contenido.SetFontSize(tamaño);
            celda.Add(contenido);
            return celda;
        }

        public static Cell NuevaCelda(string valor, PdfFont fuente, float tamaño,
            bool bordeArriba, bool bordeAbajo, bool bordeIzquiedo, bool bordeDerecho, Alineacion alineacion)
        {
            Cell celda = new Cell();
            celda.SetBorder(Border.NO_BORDER);
            if (string.IsNullOrEmpty(valor)) valor = " ";
            if (bordeArriba) celda.SetBorderTop(new SolidBorder(borde));
            if (bordeAbajo) celda.SetBorderBottom(new SolidBorder(borde));
            if (bordeIzquiedo) celda.SetBorderLeft(new SolidBorder(borde));
            if (bordeDerecho) celda.SetBorderRight(new SolidBorder(borde));
            Paragraph contenido = new Paragraph(valor);
            contenido.SetFont(fuente);
            contenido.SetFontSize(tamaño);
            if (alineacion == Alineacion.Derecha) contenido.SetTextAlignment(TextAlignment.RIGHT);
            if (alineacion == Alineacion.Izquierda) contenido.SetTextAlignment(TextAlignment.LEFT);
            if (alineacion == Alineacion.Centro) contenido.SetTextAlignment(TextAlignment.CENTER);
            if (alineacion == Alineacion.Justificado) contenido.SetTextAlignment(TextAlignment.JUSTIFIED);
            celda.Add(contenido);
            return celda;
        }

        public static Cell NuevaCelda(string valor, PdfFont fuente, float tamaño,
            bool bordeArriba, bool bordeAbajo, bool bordeIzquiedo, bool bordeDerecho, int rowSpan, int colSpan)
        {
            Cell celda = new Cell((rowSpan > 0) ? rowSpan : 0, (colSpan > 0) ? colSpan : 0);
            celda.SetBorder(Border.NO_BORDER);
            if (string.IsNullOrEmpty(valor)) valor = " ";
            if (bordeArriba) celda.SetBorderTop(new SolidBorder(borde));
            if (bordeAbajo) celda.SetBorderBottom(new SolidBorder(borde));
            if (bordeIzquiedo) celda.SetBorderLeft(new SolidBorder(borde));
            if (bordeDerecho) celda.SetBorderRight(new SolidBorder(borde));
            Paragraph contenido = new Paragraph(valor);
            contenido.SetFont(fuente);
            contenido.SetFontSize(tamaño);
            celda.Add(contenido);
            return celda;
        }

        public static Cell NuevaCelda(string valor, PdfFont fuente, float tamaño,
            bool bordeArriba, bool bordeAbajo, bool bordeIzquiedo, bool bordeDerecho, int rowSpan, int colSpan, Alineacion alineacion)
        {
            Cell celda = new Cell((rowSpan > 0) ? rowSpan : 0, (colSpan > 0) ? colSpan : 0);
            celda.SetBorder(Border.NO_BORDER);
            if (string.IsNullOrEmpty(valor)) valor = " ";
            if (bordeArriba) celda.SetBorderTop(new SolidBorder(borde));
            if (bordeAbajo) celda.SetBorderBottom(new SolidBorder(borde));
            if (bordeIzquiedo) celda.SetBorderLeft(new SolidBorder(borde));
            if (bordeDerecho) celda.SetBorderRight(new SolidBorder(borde));
            Paragraph contenido = new Paragraph(valor);
            contenido.SetFont(fuente);
            contenido.SetFontSize(tamaño);
            if (alineacion == Alineacion.Derecha) contenido.SetTextAlignment(TextAlignment.RIGHT);
            if (alineacion == Alineacion.Izquierda) contenido.SetTextAlignment(TextAlignment.LEFT);
            if (alineacion == Alineacion.Centro) contenido.SetTextAlignment(TextAlignment.CENTER);
            if (alineacion == Alineacion.Justificado) contenido.SetTextAlignment(TextAlignment.JUSTIFIED);
            celda.Add(contenido);
            return celda;
        }

        public static Cell NuevaCelda(string valor, PdfFont fuente, float tamaño, bool bordeArriba, bool bordeAbajo,
            bool bordeIzquiedo, bool bordeDerecho, int rowSpan, int colSpan, Alineacion alineacion, Color color)
        {
            Cell celda = new Cell((rowSpan > 0) ? rowSpan : 0, (colSpan > 0) ? colSpan : 0);
            celda.SetBorder(Border.NO_BORDER);
            if (string.IsNullOrEmpty(valor)) valor = " ";
            if (bordeArriba) celda.SetBorderTop(new SolidBorder(borde));
            if (bordeAbajo) celda.SetBorderBottom(new SolidBorder(borde));
            if (bordeIzquiedo) celda.SetBorderLeft(new SolidBorder(borde));
            if (bordeDerecho) celda.SetBorderRight(new SolidBorder(borde));
            Paragraph contenido = new Paragraph(valor);
            contenido.SetFont(fuente);
            contenido.SetFontSize(tamaño);
            if (alineacion == Alineacion.Derecha) contenido.SetTextAlignment(TextAlignment.RIGHT);
            if (alineacion == Alineacion.Izquierda) contenido.SetTextAlignment(TextAlignment.LEFT);
            if (alineacion == Alineacion.Centro) contenido.SetTextAlignment(TextAlignment.CENTER);
            if (alineacion == Alineacion.Justificado) contenido.SetTextAlignment(TextAlignment.JUSTIFIED);
            if (color == Color.BLACK) contenido.SetFontColor(ColorConstants.BLACK);
            if (color == Color.RED) contenido.SetFontColor(ColorConstants.RED);
            if (color == Color.PERUBLUE) contenido.SetFontColor(new DeviceRgb(16, 38, 79));
            celda.Add(contenido);
            return celda;
        }

        public static Cell NuevaCelda(string valor, PdfFont fuente, float tamaño, bool bordeArriba, bool bordeAbajo,
             bool bordeIzquiedo, bool bordeDerecho, int rowSpan, int colSpan, Alineacion alineacion, Color color, Colorb colorb)
        {

            Cell celda = new Cell((rowSpan > 0) ? rowSpan : 0, (colSpan > 0) ? colSpan : 0);
            celda.SetBorder(Border.NO_BORDER);
            if (string.IsNullOrEmpty(valor)) valor = " ";
            if (bordeArriba) celda.SetBorderTop(new SolidBorder(bordeS));
            if (bordeAbajo) celda.SetBorderBottom(new SolidBorder(bordeS));
            if (bordeIzquiedo) celda.SetBorderLeft(new SolidBorder(bordeS));
            if (bordeDerecho) celda.SetBorderRight(new SolidBorder(bordeS));

            Paragraph contenido = new Paragraph(valor);
            contenido.SetFont(fuente);
            contenido.SetFontSize(tamaño);
            if (alineacion == Alineacion.Derecha) contenido.SetTextAlignment(TextAlignment.RIGHT);
            if (alineacion == Alineacion.Izquierda) contenido.SetTextAlignment(TextAlignment.LEFT);
            if (alineacion == Alineacion.Centro) contenido.SetTextAlignment(TextAlignment.CENTER);
            if (alineacion == Alineacion.Justificado) contenido.SetTextAlignment(TextAlignment.JUSTIFIED);
            if (color == Color.BLACK) contenido.SetFontColor(ColorConstants.BLACK);
            if (color == Color.RED) contenido.SetFontColor(ColorConstants.RED);
            if (color == Color.WHITE) contenido.SetFontColor(ColorConstants.WHITE);
            if (color == Color.PERUBLUE) contenido.SetFontColor(new DeviceRgb(16, 38, 79));
            if (colorb == Colorb.BLACK) contenido.SetBackgroundColor(ColorConstants.BLACK);
            if (colorb == Colorb.RED) contenido.SetBackgroundColor(ColorConstants.RED);
            if (colorb == Colorb.WHITE) contenido.SetBackgroundColor(ColorConstants.WHITE);
            if (colorb == Colorb.GRAY) contenido.SetBackgroundColor(new DeviceRgb(0, 0, 128));
            celda.Add(contenido);

            return celda;
        }

        public static void AgregarCelda(Table tabla, Cell celda, bool esTitulo)
        {
            if (esTitulo) tabla.AddHeaderCell(celda);
            else tabla.AddCell(celda);
        }

        public static void AgregarCelda(Table tabla, string valor, PdfFont fuente, float tamaño, bool esTitulo)
        {
            Cell celda = new Cell();
            if (string.IsNullOrEmpty(valor)) valor = " ";
            Paragraph contenido = new Paragraph(valor);
            contenido.SetFont(fuente);
            contenido.SetFontSize(tamaño);
            celda.Add(contenido);
            if (esTitulo) tabla.AddHeaderCell(celda);
            else tabla.AddCell(celda);
        }

        public static void AgregarCelda(Table tabla, int rowSpan, int colSpan, string valor, PdfFont fuente, float tamaño, bool esTitulo)
        {
            Cell celda = new Cell((rowSpan > 0) ? rowSpan : 0, (colSpan > 0) ? colSpan : 0);
            if (string.IsNullOrEmpty(valor)) valor = " ";
            Paragraph contenido = new Paragraph(valor);
            contenido.SetFont(fuente);
            contenido.SetFontSize(tamaño);
            celda.Add(contenido);
            if (esTitulo) tabla.AddHeaderCell(celda);
            else tabla.AddCell(celda);
        }
    }
}