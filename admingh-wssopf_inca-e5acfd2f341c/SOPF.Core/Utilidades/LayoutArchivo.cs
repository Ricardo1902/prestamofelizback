﻿namespace SOPF.Core
{
    public static class LayoutArchivo
    {
        public enum TipoLayot
        {
            Domiciliacion = 1,
            AltaClientes = 2
        }

        public enum TipoArchivo
        {
            Entrada = 1,
            Salida = 2
        }

        public enum ArchivoSeccion
        {
            Encabezado = 1,
            Detalle = 2,
            Subdetalle = 3,
            Resumen = 4
        }

        public enum TipoCampo
        {
            Texto = 1,
            Numerico = 2,
            SaltoLinea = 3
        }

        public enum Alineacion
        {
            Izquierda = 1,
            Derecha = 2,
            Centrado = 3,
            Justificado = 4
        }
    }
}
