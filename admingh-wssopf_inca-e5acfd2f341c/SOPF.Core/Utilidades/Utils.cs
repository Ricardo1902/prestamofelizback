﻿using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Layout;
using iText.Layout.Renderer;
using Newtonsoft.Json;
using SOPF.Core.Model.Cobranza;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static SOPF.Core.LayoutArchivo;

namespace SOPF.Core
{
    public static class Utils
    {
        public static string Conexion
        {
            get { return ConfigurationManager.AppSettings.Get("CADENACONEXIONSQL"); }
        }

        public static string UrlWsPFVisanet
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings.Get("WsPfVisanet");
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        #region "DAO Reader"

        public static object GetVal(IDataReader reader, string ColumnName)
        {
            bool columnExists = reader.GetSchemaTable().Rows
                                .Cast<DataRow>()
                                .Select(x => (string)x["ColumnName"])
                                .Contains(ColumnName, StringComparer.OrdinalIgnoreCase);

            if (columnExists)
                return (reader[ColumnName] != DBNull.Value) ? reader[ColumnName] : null;
            else
                return null;

        }

        public static string GetString(IDataReader reader, string ColumnName)
        {
            object o = GetVal(reader, ColumnName);

            return (o != null) ? o.ToString() : string.Empty;
        }

        public static DateTime? GetDateTime(IDataReader reader, string ColumnName)
        {
            object o = GetVal(reader, ColumnName);

            if (o != null)
                return Convert.ToDateTime(o);
            else
                return null;
        }

        public static DateTime GetDateTime(IDataReader reader, string ColumnName, string Format)
        {
            object o = GetVal(reader, ColumnName);

            return (o != null) ? DateTime.ParseExact(o.ToString(), Format, null) : DateTime.MinValue;
        }

        public static int GetInt(IDataReader reader, string ColumnName)
        {
            object o = GetVal(reader, ColumnName);

            return (o != null) ? Convert.ToInt32(o) : 0;
        }

        public static decimal GetDecimal(IDataReader reader, string ColumnName)
        {
            object o = GetVal(reader, ColumnName);

            return (o != null) ? Convert.ToDecimal(o) : 0;
        }

        public static bool GetBool(IDataReader reader, string ColumnName)
        {
            object o = GetVal(reader, ColumnName);

            return (o != null) ? Convert.ToBoolean(o) : false;
        }

        #endregion

        #region "DAO DataTable"

        public static object GetVal(DataRow dr, string ColumnName)
        {
            DataColumnCollection columns = dr.Table.Columns;
            bool columnExists = columns.Contains(ColumnName);

            if (columnExists)
                return (!DBNull.Value.Equals(dr[ColumnName])) ? dr[ColumnName] : null;
            else
                return null;

        }

        public static string GetString(DataRow dr, string ColumnName)
        {
            object o = GetVal(dr, ColumnName);

            return (o != null) ? o.ToString() : string.Empty;
        }

        public static DateTime? GetDateTime(DataRow dr, string ColumnName)
        {
            object o = GetVal(dr, ColumnName);

            if (o != null)
                return Convert.ToDateTime(o);
            else
                return null;
        }

        public static DateTime GetDateTime(DataRow dr, string ColumnName, string Format)
        {
            object o = GetVal(dr, ColumnName);

            return (o != null) ? DateTime.ParseExact(o.ToString(), Format, null) : DateTime.MinValue;
        }

        public static int GetInt(DataRow dr, string ColumnName)
        {
            object o = GetVal(dr, ColumnName);

            return (o != null) ? Convert.ToInt32(o) : 0;
        }

        public static decimal GetDecimal(DataRow dr, string ColumnName)
        {
            object o = GetVal(dr, ColumnName);

            return (o != null) ? Convert.ToDecimal(o) : 0;
        }

        public static bool GetBool(DataRow dr, string ColumnName)
        {
            object o = GetVal(dr, ColumnName);

            return (o != null) ? Convert.ToBoolean(o) : false;
        }

        #endregion


        /// <summary>
        /// Convierte un DataReader en una lista de objetos
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static List<T> DataReaderAEntidad<T>(IDataReader reader)
        {
            List<T> resultado = null;
            try
            {
                if (reader == null) return resultado;

                resultado = new List<T>();
                T registro = default(T);
                List<string> columnas = new List<string>();

                //Obtenemos el nombre de las columnas del resultset
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    columnas.Add(reader.GetName(i));
                }

                //Recorremos todos los registros del resultset actual
                while (reader.Read())
                {
                    registro = Activator.CreateInstance<T>();

                    //Obtenemos el valor de cada columna en el registro actual
                    foreach (string columna in columnas)
                    {
                        var propiedad = registro.GetType().GetProperty(columna);
                        if (propiedad != null && !Equals(reader[propiedad.Name], DBNull.Value))
                        {
                            propiedad.SetValue(registro, reader[propiedad.Name], null);
                        }
                    }
                    resultado.Add(registro);
                }
            }
            catch (Exception ex)
            {
                resultado = null;
                //EscribirLog($"Error en {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                //   $"{Environment.NewLine}Datos: {""}", JsonConvert.SerializeObject(ex));
            }
            return resultado;
        }

        /// <summary>
        /// Escribe un log de la aplicación en un archivo de texto
        /// </summary>
        /// <param name="mensaje"></param>
        /// <param name="stackTrace"></param>
        public static void EscribirLog(string mensaje, string stackTrace)
        {
            string rutaLog = Path.Combine(AppSettings.DirectorioLog, "WsSOPF_Inca");
            string nombreArchivo = $"WsSOPF_Inca_{DateTime.Now.ToString("yyyyMMdd")}.txt";

            if (!string.IsNullOrEmpty(Path.GetExtension(rutaLog)))
                rutaLog = Path.GetDirectoryName(rutaLog);

            if (!string.IsNullOrEmpty(rutaLog))
            {
                var strFecha = $"{DateTime.Now.Year}\\{DateTime.Now.Month.ToString("00")}";
                rutaLog = string.Format("{0}\\{1}\\{2}", rutaLog, strFecha, nombreArchivo);
            }

            if (!Directory.Exists(Path.GetDirectoryName(rutaLog)))
                Directory.CreateDirectory(Path.GetDirectoryName(rutaLog));
            try
            {
                using (StreamWriter w = File.AppendText(rutaLog))
                {
                    if (!string.IsNullOrEmpty(mensaje) || !string.IsNullOrEmpty(stackTrace))
                    {
                        w.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}]");
                        w.WriteLine($"Mensaje: {mensaje}");
                        w.WriteLine($"StackTrace: {stackTrace}");
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Obtiene el valor de una propiedad en el objeto
        /// </summary>
        /// <param name="objeto"></param>
        /// <param name="campo"></param>
        /// <returns></returns>
        public static object ObtenerValorPropiedad(object objeto, string campo)
        {
            object valorPropiedad = null;
            try
            {
                if (objeto != null)
                {
                    var propiedadRes = objeto.GetType().GetProperty(campo);
                    if (propiedadRes != null)
                    {
                        valorPropiedad = propiedadRes.GetValue(objeto, null);
                    }
                }
            }
            catch (Exception ex)
            {
                valorPropiedad = null;
            }
            return valorPropiedad;
        }

        public static string FomatearCampo(LayoutArchivoCampoVM campo, object entidadValor)
        {
            //string valorCampo, TipoCampo tipoCampo, int maxLongitud, string relleno, Alineacion alineacion, int decimales, bool incluyePunto;
            string valorCampo, relleno;
            string campoFormateado;
            int longitudCampo, maxLongitud;
            try
            {
                valorCampo = string.Empty;
                campoFormateado = string.Empty;
                relleno = string.Empty;
                maxLongitud = 0;

                if (string.IsNullOrEmpty(campo.EntidadValor))
                {
                    valorCampo = campo.TextoFijo;
                }
                else
                {
                    var valorPropiedad = ObtenerValorPropiedad(entidadValor, campo.EntidadValor);
                    if (valorPropiedad != null)
                    {
                        valorCampo = valorPropiedad.ToString();
                    }
                    else { valorCampo = campo.EntidadValor; };
                }
                relleno = campo.Relleno;
                if (string.IsNullOrEmpty(campo.Relleno)) relleno = string.Empty;
                //if ((campo.Longitud ?? 0) < 0) maxLongitud = 0;
                maxLongitud = (campo.Longitud ?? 0) < 0 ? 0 : (campo.Longitud ?? 0);
                if (!string.IsNullOrEmpty(valorCampo)) longitudCampo = valorCampo.Length;
                if (!string.IsNullOrEmpty(campo.FormatoFecha) && DateTime.TryParse(valorCampo, out DateTime valorFecha))
                {
                    valorCampo = valorFecha.ToString(campo.FormatoFecha);
                }

                switch ((TipoCampo)campo.IdTipoCampo)
                {
                    case TipoCampo.Numerico:
                        decimal.TryParse(valorCampo, out decimal valorDecimal);
                        campoFormateado = valorDecimal.ToString();
                        string[] valor = campoFormateado.Split('.');
                        if ((campo.Decimales ?? 0) > 0 && valor.Length > 1)
                        {
                            campoFormateado = valor[0] + ".";
                            char[] valorFracc = valor[1].ToCharArray();
                            for (int i = 0; i < campo.Decimales && i < valorFracc.Length; i++)
                            {
                                campoFormateado += valorFracc[i];
                            }
                        }
                        if (!(campo.IncluyePunto ?? false)) campoFormateado = campoFormateado.Replace(".", "");
                        break;
                    case TipoCampo.Texto:
                        if (string.IsNullOrEmpty(valorCampo)) campoFormateado = string.Empty;
                        else campoFormateado = valorCampo;
                        break;
                    case TipoCampo.SaltoLinea:
                        campoFormateado = Environment.NewLine;
                        break;
                }
                if (campoFormateado.Length < maxLongitud && relleno.Length == 0) relleno = " ";
                switch ((Alineacion)campo.IdLayoutArchivoAlineacion)
                {
                    case Alineacion.Izquierda:
                        while (campoFormateado.Length < maxLongitud)
                        {
                            campoFormateado += relleno;
                        }
                        break;
                    case Alineacion.Derecha:
                        while (campoFormateado.Length < maxLongitud)
                        {
                            campoFormateado = relleno + campoFormateado;
                        }
                        break;
                }
                longitudCampo = campoFormateado.Length;
                if (maxLongitud > 0 && longitudCampo > maxLongitud)
                {
                    campoFormateado = campoFormateado.Substring(0, maxLongitud);
                }
            }
            catch (Exception ex)
            {
                campoFormateado = string.Empty;
                EscribirLog($"Error: {ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(campo)}, {JsonConvert.SerializeObject(entidadValor)}",
                    JsonConvert.SerializeObject(ex));
            }
            return campoFormateado;
        }

        /// <summary>
        /// Devuelve la instrucción exec del procedimiento con los parametros indicados
        /// </summary>
        /// <param name="procedimiento"></param>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public static string SqlQueryProcedureString(string procedimiento, List<SqlParameter> parametros)
        {
            string sqlCommand = string.Empty;
            if (string.IsNullOrEmpty(procedimiento)) return sqlCommand;
            sqlCommand = $"EXEC {procedimiento} ";
            if (parametros == null || parametros.Count == 0) return sqlCommand.Trim();
            int noParametro = 0;
            foreach (SqlParameter parametro in parametros)
            {
                if (!parametro.ParameterName.Contains("@")) parametro.ParameterName = $"@{parametro.ParameterName}";
                sqlCommand += $"{parametro.ParameterName} = {parametro.ParameterName}";
                if (parametro.Direction == ParameterDirection.Output || parametro.Direction == ParameterDirection.InputOutput) sqlCommand += " OUTPUT";
                if (noParametro < parametros.Count - 1) sqlCommand += ", ";
                noParametro++;
            }
            return sqlCommand;
        }

        public class DateTimeR
        {
            public DateTime? Value { get; set; }
            public DateTime? ValueIni { get; set; }
            public DateTime? ValueEnd { get; set; }

            public DateTimeR()
            {
                this.Value = null;
                this.ValueIni = null;
                this.ValueEnd = null;
            }
        }

        public class AppSettings
        {
            /// <summary>
            /// Obtiene el valor de las llaves DirectorioLog que se encuentran el archivo de configuración, separados por comas.
            /// </summary>
            public static string DirectorioLog
            {
                get
                {
                    string direcorioLog = @"C:\Logs";
                    try
                    {
                        string temp = ConfigurationManager.AppSettings.Get("DirectorioLog");
                        if (temp != null) direcorioLog = temp;
                    }
                    catch (Exception)
                    {
                    }
                    return direcorioLog;
                }
            }
        }

        public static float AltoElemento(Document documento, IBlockElement elemento)
        {
            IRenderer pRenderer = elemento.CreateRendererSubTree().SetParent(documento.GetRenderer());
            LayoutResult pLayoutResult = pRenderer.Layout(new LayoutContext(new LayoutArea(0, new iText.Kernel.Geom.Rectangle(10, 10))));
            float y = 0;
            if (pRenderer.GetOccupiedArea() != null) y = pRenderer.GetOccupiedArea().GetBBox().GetHeight();
            return y;

            //IRenderer pRenderer = p.createRendererSubTree().setParent(doc.getRenderer());
            //LayoutResult pLayoutResult = pRenderer.layout(new LayoutContext(new LayoutArea(0, new Rectangle(595 - 72, 842 - 72))));

            //float y = pLayoutResult.getOccupiedArea().getBBox().getY();
            //float x = pLayoutResult.getOccupiedArea().getBBox().getX();
        }

        public static float AltoAreaRestante(Document documento)
        {
            return documento.GetRenderer().GetCurrentArea().GetBBox().GetHeight();
        }

        public static void GuardarArchivo(string nombreArchivo, byte[] contenido, string ruta = "")
        {
            _ = Task.Factory.StartNew(() =>
            {
                try
                {
                    if (!string.IsNullOrEmpty(nombreArchivo) && contenido != null && contenido.Length > 0)
                    {
                        string rutaArchivo = string.IsNullOrEmpty(ruta) ?
                            Path.Combine(AppSettings.DirectorioLog, "Archivos", nombreArchivo)
                            : Path.Combine(ruta, nombreArchivo);
                        if (!Directory.Exists(Path.GetDirectoryName(rutaArchivo))) Directory.CreateDirectory(Path.GetDirectoryName(rutaArchivo));
                        File.WriteAllBytes(rutaArchivo, contenido);
                    }
                }
                catch (Exception e)
                {
                    EscribirLog("No existe el directorio", e.StackTrace);
                }
            });
        }

        public static byte[] CombinarPdfs(List<byte[]> bytePdf)
        {
            byte[] byteDocumento = null;
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms).SetSmartMode(true)))
                    {
                        foreach (byte[] archivo in bytePdf)
                        {
                            using (MemoryStream mem = new MemoryStream(archivo))
                            {
                                using (PdfReader reader = new PdfReader(mem))
                                {
                                    PdfDocument doc = new PdfDocument(reader);
                                    doc.CopyPagesTo(1, doc.GetNumberOfPages(), pdfArchivo);
                                }
                            }

                        }
                        pdfArchivo.Close();
                    }
                    byteDocumento = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                byteDocumento = null;
                EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(bytePdf)}", JsonConvert.SerializeObject(ex));
            }
            return byteDocumento;
        }

        /// <summary>
        /// Obtiene el nombre del mes de una fecha
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public static string MesFecha(DateTime fecha)
        {
            string nombreMes = string.Empty;
            if (!fecha.Equals(new DateTime()))
            {
                int noMes = fecha.Month - 1;
                string[] meses = new string[] {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
                    "Septiembre", "Octubre", "Noviembre", "Diciembre" };
                nombreMes = meses[noMes];
            }
            return nombreMes;
        }

        /// <summary>
        /// Crea una entidad con las propiedades contenidas en la entidad origen de tipo T
        /// </summary>
        /// <typeparam name="T">Entidad Destino</typeparam>
        /// <param name="entidad">Entidad Origen</param>
        /// <returns></returns>
        public static T Convertir<T>(this Object entidad)
        {
            //if (entidad is null) return default;
            Type entidadType = entidad.GetType();
            Type destinoType = typeof(T);
            var entidadDestino = Activator.CreateInstance(destinoType, false);

            IEnumerable<MemberInfo> propEntidad = from p in entidadType.GetMembers().ToList()
                                                  where p.MemberType == MemberTypes.Property
                                                  select p;
            IEnumerable<MemberInfo> propDestino = from p in destinoType.GetMembers().ToList()
                                                  where p.MemberType == MemberTypes.Property
                                                  select p;
            List<MemberInfo> propiedades = propEntidad
                .Where(p => propDestino
                    .Select(p1 => p1.Name).ToList()
                    .Contains(p.Name))
                .ToList();

            PropertyInfo propiedad;
            object valor;
            propiedades.ForEach(p =>
            {
                propiedad = typeof(T).GetProperty(p.Name);
                valor = entidad.GetType().GetProperty(p.Name).GetValue(entidad, null);
                propiedad.SetValue(entidadDestino, valor, null);
            });
            return (T)entidadDestino;
        }

        /// <summary>
        /// Metodo que recibe un arreglo de imagenes en base64 y lo convierte a un documento pdf
        /// </summary>
        /// <param name="imagenes"></param>
        /// <returns></returns>
        public static byte[] ImagenB64PdfByte(List<string> imagenes)
        {
            //NOTA: Cree un documento por imágen para conserva el tamaño original de la imagen, no encontré el modo de hacerlo en sola apertura de archivo
            //Probé algunos pero se ajustaba a la primera página. Para ajustar las imágenes en un formato específico si es posible.

            byte[] byteDocumento = null;
            List<byte[]> pdfImagenes = new List<byte[]>();
            try
            {
                if (imagenes == null || imagenes.Count == 0 || imagenes.Any(im => string.IsNullOrEmpty(im))) return byteDocumento;
                foreach (string imgString in imagenes)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (PdfDocument pdfArchivo = new PdfDocument(new PdfWriter(ms).SetSmartMode(true)))
                        {
                            byte[] imgByte = Convert.FromBase64String(imgString);
                            if (imgByte == null) throw new Exception("No fue posible decodificar la imagen.");
                            iText.IO.Image.ImageData imagenData = iText.IO.Image.ImageDataFactory.Create(imgByte, false);
                            Image imagen = new Image(imagenData);

                            Document documento = new Document(pdfArchivo, new iText.Kernel.Geom.PageSize(imagen.GetImageWidth(), imagen.GetImageHeight()));
                            documento.Add(imagen);
                            documento.Close();
                        }
                        byteDocumento = ms.ToArray();
                    }

                    if (byteDocumento == null || byteDocumento.Length == 0) throw new Exception("No fue posible crear el documento para la imagen.");
                    pdfImagenes.Add(byteDocumento);
                }
                byteDocumento = CombinarPdfs(pdfImagenes);
            }
            catch (Exception ex)
            {
                byteDocumento = null;
                EscribirLog($"{ex.Message}{Environment.NewLine}Método: {MethodBase.GetCurrentMethod().DeclaringType.Name}\\{MethodBase.GetCurrentMethod().Name}" +
                    $"{Environment.NewLine}Datos: {JsonConvert.SerializeObject(imagenes)}", JsonConvert.SerializeObject(ex));
            }
            return byteDocumento;
        }

        /// https://docs.microsoft.com/en-us/dotnet/standard/base-types/how-to-verify-that-strings-are-in-valid-email-format
        /// <summary>
        /// Valida si una cadena es una dirección de correo valido
        /// </summary>
        /// <param name="correo"></param>
        /// <returns></returns>
        public static bool esCorreoValido(string correo)
        {
            if (string.IsNullOrWhiteSpace(correo))
                return false;

            try
            {
                // Normalize the domain
                correo = Regex.Replace(correo, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    string domainName = idn.GetAscii(match.Groups[2].Value);
                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(correo, @"^[^@\s]+@[^@\s]+\.[^@\s]+$", RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}
