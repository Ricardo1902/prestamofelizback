﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace SOPF.Core
{
    public static class EnvioCorreo
    {
        public static EnvioCorreoResponse Enviar(SmtpParametros smtpParametros)
        {
            EnvioCorreoResponse respuesta = new EnvioCorreoResponse();
            MailMessage mailMessage = new MailMessage();
            try
            {
                if (!string.IsNullOrEmpty(smtpParametros.De))
                {
                    mailMessage.From = new MailAddress(smtpParametros.De);
                }

                foreach (string to in smtpParametros.Para)
                {
                    mailMessage.To.Add(new MailAddress(to));
                }

                if (smtpParametros.CCO != null)
                {
                    foreach (string bcc in smtpParametros.CCO)
                    {
                        mailMessage.Bcc.Add(new MailAddress(bcc));
                    }
                }

                if (smtpParametros.CC != null)
                {
                    foreach (string cc in smtpParametros.CC)
                    {
                        mailMessage.CC.Add(new MailAddress(cc));
                    }
                }

                if (!string.IsNullOrEmpty(smtpParametros.Asunto))
                {
                    mailMessage.Subject = smtpParametros.Asunto;
                }

                if (!string.IsNullOrEmpty(smtpParametros.Contenido))
                {
                    mailMessage.Body = smtpParametros.Contenido;
                }

                if (smtpParametros.ContenidoAlterno != null)
                {
                    foreach (AlternateView cont in smtpParametros.ContenidoAlterno)
                    {
                        mailMessage.AlternateViews.Add(cont);
                    }
                }

                mailMessage.IsBodyHtml = smtpParametros.ContenidoHtml;
                mailMessage.Priority = MailPriority.Normal;

                if (smtpParametros.Adjuntos != null)
                {
                    foreach (var attachmentPath in smtpParametros.Adjuntos)
                    {
                        Attachment mailAttachment = new Attachment(attachmentPath);
                        mailMessage.Attachments.Add(mailAttachment);
                    }
                }

                if (smtpParametros.AdjuntosByte != null)
                {
                    foreach (Adjuntos item in smtpParametros.AdjuntosByte)
                    {
                        if (item != null && item.Contenido != null && item.Contenido.Length > 0)
                        {
                            Attachment mailAttachment = new Attachment(new MemoryStream(item.Contenido), item.NombreArchivo);
                            mailMessage.Attachments.Add(mailAttachment);
                        }
                    }
                }

                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Host = smtpParametros.Servidor;
                smtpClient.Port = smtpParametros.Puerto;
                smtpClient.EnableSsl = smtpParametros.Ssl;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.Credentials = new NetworkCredential(smtpParametros.De, smtpParametros.Password);

                smtpClient.Send(mailMessage);
                respuesta.Enviado = true;
                respuesta.Mensaje = "Enviado con éxito";
            }
            catch (Exception ex)
            {
                respuesta.Enviado = false;
                respuesta.Mensaje = ex.Message;
                if (ex.InnerException != null) respuesta.Mensaje += $" {ex.InnerException}";
            }
            return respuesta;
        }
    }

    public class SmtpParametros
    {
        public string Servidor { get; set; }
        public string Password { get; set; }
        public int Puerto { get; set; }
        public bool Ssl { get; set; }
        public List<string> Para { get; set; }
        public string De { get; set; }
        public List<string> CCO { get; set; }
        public List<string> CC { get; set; }
        public string Asunto { get; set; }
        public string Contenido { get; set; }
        public bool ContenidoHtml { get; set; }
        public List<string> Adjuntos { get; set; }
        public List<Adjuntos> AdjuntosByte { get; set; }
        public List<AlternateView> ContenidoAlterno { get; set; }
    }

    public class Adjuntos
    {
        public string NombreArchivo { get; set; }
        public byte[] Contenido { get; set; }
    }

    public class EnvioCorreoResponse
    {
        public bool Enviado { get; set; }
        public string Mensaje { get; set; }
    }
}