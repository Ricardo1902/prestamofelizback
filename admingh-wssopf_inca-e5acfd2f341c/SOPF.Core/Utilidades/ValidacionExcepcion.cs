﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOPF.Core
{
    /// <summary>
    /// Descripción breve de ValidacionExcepcion
    /// </summary>
    public class ValidacionExcepcion : Exception
    {
        public ValidacionExcepcion()
        {
        }
        public ValidacionExcepcion(string mensaje) : base(mensaje) { }
        public ValidacionExcepcion(string mensaje, Exception inner) : base(mensaje, inner) { }
    }
}
